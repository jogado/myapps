
#ifndef _MAIN_H
#define _MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>


extern void Draw_rectangle  ( int x , int y ,int width , int height ,int r, int g , int b) ;
extern int  main            ( int argc, char **argv );
extern void Show_help       ( void );


#endif
