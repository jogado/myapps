#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/input.h>
#include <string.h>

#include <malloc.h>


#include <stdint.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <string.h>
#include <errno.h>


#define DDR_1MB  1*1024*1024
#define SIZE_1_MB    (   1 * 1024 * 1024 )
#define DRAM_START_ADDR                 0x80000000 // DRAM start address





extern int main(void) ;
extern void read_memory(uintptr_t addr, void *buf, size_t size);
extern void check_ddr_size();


void read_memory(uintptr_t addr, void *buf, size_t size) {
    memcpy_fromio(buf, (void *)addr, size);
}



void check_ddr_size() {
    ulong dram_start = DRAM_START_ADDR;
    ulong i;
    uint8_t *buf;
    ulong dram_read;
    ulong value;
    ulong value2;



    buf = malloc(10);
    if (buf == NULL) {
        printf("DDR: Failed to allocate memory \n" );
        return;
    }


    dram_read=dram_start + ( 960 * SIZE_1_MB ) ;

    //----------------------------------------------------------
    read_memory( dram_read , buf, 4);
    value2 = buf[0] ;
    value2 += (ulong) ( buf[1] << 8  );
    value2 += (ulong) ( buf[2] << 16 );
    value2 += (ulong) ( buf[3] << 24 );
    printf("    malloc : 0x%08lX \n\r" ,value2  );
    usleep(100000);
    //----------------------------------------------------------
    value = readl(dram_read);
    printf("    readl  : 0x%08lX \n\r",value );
    usleep(100000);
    //----------------------------------------------------------

    if(value == value2)
    {
        puts("DDR S: 1024(MB)\n");
    }
    else
    {
        puts("DDR S: 512(MB)\n");
    }
    usleep(200000);
    free(buf);
    return;
}

int main(void)
{
    check_ddr_size();
    return 0;
}


