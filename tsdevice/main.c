#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

//   struct timeval {
//      long tv_sec;    /* seconds */
//      long tv_usec;   /* microseconds */
//   };





//   struct input_event {
//   struct timeval time;
//   __u16 type;
//   __u16 code;
//   __s32 value;
//   };





/*
 * Event types

#define EV_SYN                        0x00
#define EV_KEY                        0x01
#define EV_REL                        0x02
#define EV_ABS                        0x03
#define EV_MSC                        0x04
#define EV_SW                        0x05
#define EV_LED                        0x11
#define EV_SND                        0x12
#define EV_REP                        0x14
#define EV_FF                        0x15
#define EV_PWR                        0x16
#define EV_FF_STATUS                0x17
#define EV_MAX                        0x1f
#define EV_CNT

 */



/*
 * Synchronization events.


#define SYN_REPORT                0
#define SYN_CONFIG                1
#define SYN_MT_REPORT                2


 */


/*
 * Absolute axes
https://sites.uclouvain.be/SystInfo/usr/include/linux/input.h.html

#define ABS_X                        0x00
#define ABS_Y                        0x01
#define ABS_Z                        0x02
#define ABS_RX                        0x03
#define ABS_RY                        0x04
#define ABS_RZ                        0x05
#define ABS_THROTTLE                0x06
#define ABS_RUDDER                0x07
#define ABS_WHEEL                0x08
#define ABS_GAS                        0x09
#define ABS_BRAKE                0x0a
#define ABS_HAT0X                0x10
#define ABS_HAT0Y                0x11
#define ABS_HAT1X                0x12
#define ABS_HAT1Y                0x13
#define ABS_HAT2X                0x14
#define ABS_HAT2Y                0x15
#define ABS_HAT3X                0x16
#define ABS_HAT3Y                0x17
#define ABS_PRESSURE                0x18
#define ABS_DISTANCE                0x19
#define ABS_TILT_X                0x1a
#define ABS_TILT_Y                0x1b
#define ABS_TOOL_WIDTH                0x1c
#define ABS_VOLUME                0x20
#define ABS_MISC                0x28

#define ABS_MT_TOUCH_MAJOR        0x30
#define ABS_MT_TOUCH_MINOR        0x31
#define ABS_MT_WIDTH_MAJOR        0x32
#define ABS_MT_WIDTH_MINOR        0x33
#define ABS_MT_ORIENTATION        0x34
#define ABS_MT_POSITION_X        0x35
#define ABS_MT_POSITION_Y        0x36
#define ABS_MT_TOOL_TYPE        0x37
#define ABS_MT_BLOB_ID                0x38
#define ABS_MT_TRACKING_ID        0x39

#define ABS_MAX                        0x3f
#define ABS_CNT                        (ABS_MAX+1)
*/




int hh=0;
int mm=0;
int ss=0;
char aType[80];
int ret=0;

extern  int main(void ) ;

int main(void )
{

    printf("Touchscreen Decoder\n\r");

    // Open the input device file for the touchscreen (replace /dev/input/event0 with the actual device file)
    int fd = open("/dev/input/touchscreen0", O_RDONLY);
    if (fd == -1)
    {
        perror("Error opening input device");
        exit(1);
    }

    // Set up variables to hold touchscreen data

    // Read and translate input events
    struct input_event ev;


    while (1)
    {
        ret=(int) read(fd, &ev, sizeof(struct input_event));

        if (ret < (int)sizeof(struct input_event) )
        {
            perror("Error reading input event");
            exit(1);
        }

        int ss=ev.time.tv_sec%60;
        int mm=(ev.time.tv_sec/60)%60;
        int hh=(ev.time.tv_sec/3600)%24;


        switch(ev.type)
        {
            case EV_ABS:
                        switch (ev.code )
                        {
                                case ABS_X :
                                                   // sprintf(aType,"EV_ABS - ABS_X" );
                                                   // printf("time = %02d:%02d:%02d   type %s: %d\n\r",hh,mm,ss, aType ,ev.value );
                                                    printf("X:%03d  ",ev.value );

                                                    break;

                                case ABS_Y :
//                                                    sprintf(aType,"EV_ABS - ABS_Y" );
//                                                    printf("time = %02d:%02d:%02d   type %s: %d\n\r",hh,mm,ss, aType ,ev.value );

                                                    printf("Y:%03d  ",ev.value );
                                                    break;

                                case ABS_MT_POSITION_X :
                                                   // sprintf(aType,"EV_ABS - ABS_MT_POSITION_X" );
                                                    //printf("time = %02d:%02d:%02d   type %s: %d\n\r",hh,mm,ss, aType ,ev.value );
                                                    printf("XX:%03d  ",ev.value );
                                                    break;

                                case ABS_MT_POSITION_Y :
                                                    //sprintf(aType,"EV_ABS - ABS_MT_POSITION_Y" );
                                                    //printf("time = %02d:%02d:%02d   type %s: %d\n\r",hh,mm,ss, aType ,ev.value );
                                                    printf("YY:%03d  ",ev.value );
                                                    break;

                                case ABS_MT_TRACKING_ID :

                                                    //sprintf(aType,"EV_ABS - ABS_MT_TRACKING_ID" );
                                                    //printf("time = %02d:%02d:%02d   type %s: %d\n\r",hh,mm,ss, aType ,ev.value );

                                                    if( ev.value != -1 )
                                                        printf("ID: %04d  ",ev.value );

                                                    break;

                                default:
                                                    sprintf(aType,"EV_ABS - code(0x%02x)",ev.code );
                                                    printf("time = %02d:%02d:%02d   type %s value:%d \n\r",hh,mm,ss, aType,ev.value );
                                                    break;
                        }
                        break;

            case EV_KEY:
                        switch (ev.code )
                        {
                            case BTN_TOUCH :

                                    if( ev.value == 1)
                                    {
                                        //sprintf(aType,"EV_KEY - BTN_TOUCH DOWN" );
                                        //printf("time = %02d:%02d:%02d   type %s  value:%d \n\r",hh,mm,ss, aType ,ev.value );
                                        printf("[ DOWN ]\n\r");
                                    }
                                    else
                                    {
                                        //sprintf(aType,"EV_KEY - BTN_TOUCH UP" );
                                        //printf("time = %02d:%02d:%02d   type %s  value:%d \n\r",hh,mm,ss, aType ,ev.value );
                                        printf("[ UP ]\n\r");
                                    }
                                    break;

                           default:

                                    sprintf(aType,"EV_KEY - code(0x%02x)",ev.code );
                                    printf("time = %02d:%02d:%02d   type %s value:%d \n\r",hh,mm,ss, aType ,ev.value );
                                    break;
                        }


                        break;




            case EV_SYN:

                        switch (ev.code )
                        {
                            case SYN_REPORT :
                                                sprintf(aType,"EV_SYN - SYN_REPORT" );
                                                break;
                            case SYN_CONFIG :
                                                sprintf(aType,"EV_SYN - SYN_CONFIG" );
                                                break;
                            case SYN_MT_REPORT :
                                                sprintf(aType,"EV_SYN - SYN_MT_REPORT" );
                                                break;
                            case SYN_DROPPED :
                                                sprintf(aType,"EV_SYN - SYN_DROPPED" );
                                                break;
                            default:
                                                sprintf(aType,"EV_SYN - ev.code (%d)",ev.code );
                                                break;
                        }
                        //printf("time = %02d:%02d:%02d   type %s value:%d \n\r",hh,mm,ss, aType ,ev.value );
                        break;

            case EV_REL:
                        sprintf(aType,"%-10.10s","EV_REL" );
                        printf("time = %02d:%02d:%02d   type %s code:%04x value:%d \n\r",hh,mm,ss, aType ,ev.code,ev.value );
                        break;
            case EV_MSC:
                        sprintf(aType,"%-10.10s","EV_MSC" );
                        printf("time = %02d:%02d:%02d   type %s code:%04x value:%d \n\r",hh,mm,ss, aType ,ev.code,ev.value );
                        break;

            case EV_SW:     sprintf(aType,"%-10.10s","EV_SW" );
                        printf("time = %02d:%02d:%02d   type %s code:%04x value:%d \n\r",hh,mm,ss, aType ,ev.code,ev.value );
                            break;

            default:

                        sprintf(aType,"ev.type(%d)",ev.type );
                        printf("time = %02d:%02d:%02d   type %s code:%04x value:%d \n\r",hh,mm,ss, aType ,ev.code,ev.value );
                        break;
        }



    }

    // Close the input device file
    close(fd);

    return 0;
}
