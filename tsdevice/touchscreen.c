#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <linux/input.h>



//   struct timeval {
//      long tv_sec;    /* seconds */
//      long tv_usec;   /* microseconds */
//   };





//   struct input_event {
//   struct timeval time;
//   __u16 type;
//   __u16 code;
//   __s32 value;
//   };



int main()
{

    printf("Touchscreen Decoder\n\r");

    // Open the input device file for the touchscreen (replace /dev/input/event0 with the actual device file)
    int fd = open("/dev/input/touchscreen0", O_RDONLY);
    if (fd == -1)
    {
        perror("Error opening input device");
        exit(1);
    }


    
    // Set up variables to hold touchscreen data
    int x_pos = 0;
    int y_pos = 0;
    int touching = 0;

    // Read and translate input events
    struct input_event ev;
    while (1)
    {
        if (read(fd, &ev, sizeof(struct input_event)) < sizeof(struct input_event))
        {
            perror("Error reading input event");
            exit(1);
        }

        printf("time = %ld %ld \n\r",ev.time.tv_sec,ev.time.tv_usec    );



        if (ev.type == EV_SYN && ev.code == SYN_REPORT)
        {
            // Output the current touchscreen data and event type
            printf("x_pos: %d, y_pos: %d, touching: %d, event: ", x_pos, y_pos, touching);
            switch (ev.code)
            {
                case BTN_TOUCH:
                    printf("push\n");
                    break;
                case BTN_TOOL_FINGER:
                    printf("click\n");
                    break;
                default:
                    printf("unknown\n");
                    break;
            }
        }
        else if (ev.type == EV_ABS)
        {
            if (ev.code == ABS_X)
            {
                x_pos = ev.value;
            }
            else if (ev.code == ABS_Y)
            {
                y_pos = ev.value;
            }
            else if (ev.code == BTN_TOUCH)
            {
                touching = ev.value;
            }
        }
    }

    // Close the input device file
    close(fd);

    return 0;
}
