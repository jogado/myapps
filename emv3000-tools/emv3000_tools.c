#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>


#include "struct.h"
#include "decode_oti.h"


#define MOTI_DATA_PATH		"/tmp/moti.data"
#define MOTI_DATA_PRODTEST	"/tmp/moti.tmp"
#define MOTI_ERROR_LOG_PATH 	"/etc/moti/moti.error.log"

char aError[256];



typedef enum {
   HUNT_STX = 1   ,
   HUNT_LEN       ,
   HUNT_UNIT      ,
   HUNT_OPCODE    ,
   HUNT_DATA      ,
   HUNT_LRC       ,
   HUNT_ETX       ,
} HUNT_STATES;


OTI_REC ri;
OTI_REC r_old;


HEAD_REC h;



#define STX 0x02
#define ETX 0x03

#define EMV300_SET	0x3c
#define EMV300_GET	0x3d
#define EMV300_DO	0x3e


#define ms_1 	1000
#define ms_100 	100000
#define ms_200 	200000
#define ms_500 	500000
#define ms_50  	50000




extern void store_record	(OTI_REC *r,OTI_REC *r_old);
extern int same_record		(OTI_REC *r,OTI_REC *r_old);
extern void clear_record	(OTI_REC *rec);


extern int  main 		( int argc, char **argv ) ;
extern void Decode_Head		(unsigned char *p);
extern void ShowData		(HEAD_REC *h);
extern void ProdTestData_file	(char * s);
extern int  usb_reset		(int bus, int device);
extern int  reset_emv3000	(void);
extern void RxLoop		(void);
extern void treat_data		(OTI_REC *r);
extern void SendCommand		(unsigned char *s);

extern void errorfile		(char * s);
extern void system_call 	(const char *s);


char devname[] = "/dev/ttyACM0";
int  devfd;



int PollingRequested=1;
int PollPending = 0;
int Ack_Received=0;




int CARD_M=0;
int CARD_B=0;
int CARD_F=0;
int CARD_X=0;
int DoBeep=0;
int Debug=0;
int DebugTXRX=0;



int nothing=0;
int Done = 0;
int Toggle=0;



//============================================================================================
void store_record(OTI_REC *r,OTI_REC *r_old)
{
	memcpy(r_old,r,sizeof(OTI_REC));
}
//============================================================================================
int same_record(OTI_REC *r,OTI_REC *r_old)
{
	return( memcmp(r,r_old,sizeof(OTI_REC)) ) ;
}
//============================================================================================
void clear_record(OTI_REC *rec)
{
	 memset(rec,0,sizeof(OTI_REC));
}
//============================================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}
//============================================================================================





//============================================================================================
void treat_data(OTI_REC *r)
{
	int len = r->REC_LEN -6;
	unsigned char *p = &r->DATA[0] ;
	int r_type;
	int r_len;
	
	if(Debug) printf("Treat_data(%d)\n\r", len);

	nothing=0;
	Done=0;

	if( (r->REC_LEN==8) && (r->DATA[0]==0x00) && (r->DATA[1]==0x00) )
	{
		if(Debug) printf("	PDC ACK\n\r");
		Ack_Received=1;
		return;
	}

	if( (r->REC_LEN==7) && (r->OPCODE==0x15) )
	{
		printf("	PDC NACK (error: 0x%02X)\n\r",r->DATA[0]);
		PollingRequested =1;
		PollPending     = 0 ;
		return;
	}
	

	if( same_record(r,&r_old)==0 )
	{
		if(Debug) printf("	Same record, data discarted\n\r");
//		printf("resend requested - Duplicate record \n\r");
		PollingRequested = 1;
		PollPending      = 0;
		return;
	}


	while (len > 0)
	{
		r_type= *(p+0);
//		r_id  = *(p+1);
		r_len = *(p+2);

	
		if( r_type == 0xDF )
		{
			treat_df ( p );
			p   += r_len +3 ;
			len -= r_len +3 ;
			continue;
		}

		else if( r_type == 0xFF ) // PDC Response templates
		{
			treat_ff ( p );
			p+=3 ;
			len -= 3;	// ADDED by LUC !!!!! but it still blocks :-(
			continue;
		}
		else if( r_type == 0xFC ) // Result Data Template
		{
			treat_fc ( p );
			p+=2 ;
			len -= 2;	// ADDED by LUC !!!!! but it still blocks :-(
			continue;
		}

		else if( r_type == 0x84 ) // Dedicated File (DF) Name (EMV Cards)
		{
			//r_type= *(p+0);
			//r_id  = *(p+1);
			//r_len = *(p+2);

			treat_84 ( p );
			p   += r_len +2 ;
			len -= r_len +2 ;
			continue;
		}
		else	// ADDED by LUC
		{
			printf("Unknown type: %X\r\n", r_type);
		}

		p++;
		len--;
	}
}
//============================================================================================







unsigned char Buffer[256];
int Buffer_len=0;
int Buffer_offset=0;

void RxLoop(void)
{
	static unsigned char    State = HUNT_STX;
	static int		Offset;
	static unsigned char	LRC;
	OTI_REC			*r=&ri;
	int 			len;
	int 			TheChar;


	while (1)
	{
		if(Buffer_len)
		{
			TheChar = (int)Buffer[Buffer_offset++];
			if(Buffer_offset >= Buffer_len )
			{
				Buffer_len    = 0;
				Buffer_offset = 0;
				//usleep(100000); // System give hand, could help ?
			}
		}
		else
		{
			len = read(devfd,&Buffer,200);
			if(len < 0)
			{
				printf("read error : (%d) %s\n\r", len , strerror(errno));
				return;
			}
			if(len==0)
			{
				return;
			}
			Buffer_len    = len;
			Buffer_offset = 0;
			continue;
		}

		nothing=0;
		TheChar = TheChar & 0x00ff;

		switch (State)
		{
			case HUNT_STX:
				LRC = 0;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LEN:
			case HUNT_UNIT:
			case HUNT_OPCODE:
			case HUNT_DATA:
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LRC:
			case HUNT_ETX:
				break;
			default:
				break;
		}

		if( State == HUNT_LEN )
		{
			if(Debug||DebugTXRX) printf("RX [%03d]: 02",TheChar);
		}

		if( State != HUNT_STX )
		{
			if(Debug||DebugTXRX) printf(" %02X",TheChar);
		}

		if( State == HUNT_ETX && TheChar == ETX )
		{
			if(Debug||DebugTXRX) printf("\n\r");
		}

		switch (State)
		{
			case HUNT_STX:
				if( TheChar == STX )
				{
					memset(r,0x00,sizeof(OTI_REC));
					Offset = 0;
					State  = HUNT_LEN;
					r->START_CHR = TheChar;
				}
				break;
			case HUNT_LEN:
				r->REC_LEN =  TheChar ;
				State = HUNT_UNIT;
				break;
			case HUNT_UNIT:
				r->UNIT = TheChar ;
				State = HUNT_OPCODE;
				break;
			case HUNT_OPCODE:
				r->OPCODE =  TheChar ;
				State = HUNT_DATA;
				break;
			case HUNT_DATA:
	
				if(Offset >100)
				{
					State=HUNT_STX;
					printf("************************\n\r");
					printf("************************\n\r");
					printf("***** Offset error *****\n\r");
					printf("************************\n\r");
					printf("************************\n\r");
					system_call("beep &");
					exit (0);
					break;
				}

				r->DATA[Offset++] = TheChar;
				if (Offset >= r->REC_LEN -6)
				{
					State = HUNT_LRC;
				}
				break;
			case HUNT_LRC:
				r->LRC =  TheChar ;
				if(r->LRC != LRC )
				{
					printf("LRC = 0x%02X  / Calc = %02X \r\n",r->LRC,LRC);
					State = HUNT_STX;
					PollingRequested=1;
					//printf("resend requested - Error LRC\n\r");

				}
				else
				{
					State = HUNT_ETX;
				}
				break;
			case HUNT_ETX:
				if( TheChar == ETX )
				{
					r->END_CHR =  TheChar ;
					treat_data(r);
				}
				State = HUNT_STX;
				break;
			default:
				printf("Default case in Rxloop\r\n");
				State = HUNT_STX;
				PollingRequested=1;
				printf("resend requested - Default State\n\r");
				break;
		}
	}
}
//===============================================================================================
char aProd[256];
void ProdTestData_file(char * s)
{
	FILE *f;
	int len = strlen(s);

	f= fopen (MOTI_DATA_PRODTEST,"w");
	fwrite(s , 1 , len , f );
	fclose(f);

	if(DoBeep) system_call("beep &");
}
//================================================================================================
void errorfile(char * s)
{
	FILE *f;
	int len = strlen(s);

	f= fopen (MOTI_ERROR_LOG_PATH,"a");
	fwrite(s , 1 , len , f );
	fclose(f);
}
//================================================================================================







//================================================================================================
void ShowData(HEAD_REC *h)
{
	int i;

	if(h->Len )
	{
		printf(" [ ");
		for(i=0; i <h->Len ;i++)
		{
			printf("%02X ",h->data[i]);
		}
		printf("]");
	}
	printf("\n\r");

}
//==============================================================================
void Decode_Head(unsigned char *p)
{
   h.Type  = *(p+0);
   h.Id    = *(p+1);
   h.Len   = *(p+2);
   h.Val   = *(p+3);

   h.data  = p+3;
}
//=====================================================================================================================





















#define aSendBuffer_LEN 100
char aSendBuffer [100];

void SendCommand(unsigned char *s)
{
	int i=0;
	int ret;
	int len = s[1] ;
	
	if(len >= aSendBuffer_LEN )
	{
		printf("SendCommand() : Incorrect Command length \n\r");
		return;
	}
	
	memcpy(aSendBuffer,s,len);


	ret = write(devfd, aSendBuffer, len);
	if(ret < 0 )
	{
		printf("write() error (%d) \r\n", ret);
	}

	if(Debug||DebugTXRX) 
	{
		printf("(%d)\n\r",Toggle++);
		printf("TX [%03d]:",len);
		for(i=0;i< len ; i++)
		{
			printf(" %02X",aSendBuffer[i] ) ;
		}
		printf("\n\r");
	}
}


//================================================================================================
char filename[100];
int usb_reset(int bus, int device)
{
	int fd;
	int rc;

	sprintf(filename,"/dev/bus/usb/%03d/%03d",bus,device);

	printf("Resetting USB device [%s]\r\n", filename);
	usleep(100000);

	fd = open(filename, O_WRONLY);
	if (fd < 0) {
		printf("Error opening usb file\n\r");
		return 1;
	}
	printf("open %s OK\r\n", filename);

	rc = ioctl(fd, USBDEVFS_RESET, 0);
	if (rc < 0) {
		printf("Error in ioctl:%d\n\r",rc);
		close(fd);
		return 1;
	}

	close(fd);
	printf("Reset successful\n");
	return 0;
}
//================================================================================================
int reset_emv3000(void)
{
	printf("Resetting emv3000 ....\n\r");
	system_call("echo  1 > /sys/class/leds/emv-reset/brightness");
	sleep(1);
	system_call("echo  0 > /sys/class/leds/emv-reset/brightness");
	return 0;
}
//================================================================================================








typedef struct {
	unsigned int baud;
	unsigned int cbaud;
} BaudDefines;

static BaudDefines baudDefines[] = { 
	{9600, B9600},
	{19200, B19200},
	{38400, B38400},
	{57600, B57600},
	{115200, B115200},
	{230400, B230400}, 
	{460800, B460800}
};

int RTS_flag ;
int DTR_flag ;


static void closePort(void)
{
	printf("Clear DTR/RTS\n\r");
	ioctl(devfd,TIOCMBIC,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIC,&RTS_flag);//Set DTR pin
	sleep(1);

	printf("Closing device ....\n\r");
	close(devfd);
	printf("Closed....\n\r");
}



int TheSelectedBaudrate = B115200 ;


static int openPort(char *name,unsigned int baud)
{
	unsigned int i,cbaud=TheSelectedBaudrate;
	int r;
	int Flags=0;

	

	struct termios ntio, otio;
	for( i = 0 ; i < sizeof(baudDefines)/sizeof(BaudDefines) ; i++ )
	{
		if(baudDefines[i].baud == baud) 
		{
			cbaud =  baudDefines[i].cbaud; 
			break;
		}
	}

	Flags |= O_RDWR ;
//	Flags |= O_NONBLOCK;
//	Flags |= O_NOCTTY;

	devfd = open(name, Flags);

	if( devfd < 0 ) 
	{
		printf("open [%s] failed %d %s\n", name, devfd, strerror(errno));
		exit(-1);
	}


	DTR_flag = TIOCM_DTR;
	RTS_flag = TIOCM_RTS;


	printf("DTR/RTS Set \n\r");
	ioctl(devfd,TIOCMBIS,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIS,&RTS_flag);//Set DTR pin
	sleep(1);


	if( ( r = tcgetattr( devfd, & otio ) ) < 0 ){
		printf("openPort [%s] error tcgetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}


	//=================================================
	bzero( & ntio, sizeof( struct termios ) );

	cfmakeraw( &ntio );

	ntio.c_lflag 		= 0;			/* c_lflag : local mode flags   */
	ntio.c_oflag 		= 0;			/* c_oflag : output mode flags  */
	ntio.c_cc[VMIN] 	= 0;
	ntio.c_cc[VTIME] 	= 0;

	ntio.c_cflag = CS8 | CLOCAL | CREAD;	/* c_oflag; control mode flags */
	ntio.c_cflag &= ~HUPCL;
	ntio.c_cflag &= ~PARENB;		/* CLEAR Parity Bit PARENB*/
	ntio.c_cflag &= ~CSTOPB;		/* Stop bits = 1 */
	ntio.c_cflag &= ~CRTSCTS;		/* Turn off hardware based flow control (RTS/CTS). */

	cfsetispeed(&ntio, cbaud);
	cfsetospeed(&ntio, cbaud);

	if( ( r = tcsetattr( devfd, TCSANOW, & ntio ) ) < 0 ){
		printf("open_port [%s] error tcsetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}

	if( ( r = tcflush( devfd, TCIOFLUSH ) ) < 0 ){
		printf("tcflush [%s] error  %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}
	//=================================================

	return devfd;  
}




/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/



unsigned char Do_Poll_1        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x01,0x97,0x03}; /* Pending Auto Polling Continuously for supported Application Transaction */
unsigned char Do_Poll_2        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x02,0x94,0x03}; /* Pending Auto Polling once for supported Application Transaction  */
unsigned char Do_Poll         		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x03,0x95,0x03}; /* Pending Polling once and ONLY Activate PICC */
unsigned char Do_Poll_4        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x04,0x92,0x03}; /* Immediate Polling once and ONLY Activate PICC */
unsigned char Do_Poll_stop 		[] = {0x02,0x09,0x00,0x3E,0xDF,0x7D,0x00,0x97,0x03}; 		// Pending Polling once and ONLY Activate PICC 
unsigned char Get_RF_param_MaxBitRate	[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};
unsigned char Do_RF_param_FWTI		[] = {02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};
unsigned char do_RF_max_bitrate		[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};		// RF Parameter MAX bit rate
unsigned char Do_RF_Parameter		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};	// RF Parameter


unsigned char Do_RF_OFF			[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x00 ,0xEE ,0x03};
unsigned char Do_RF_ON	          	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x01 ,0xEF ,0x03};
unsigned char Do_OUTPORT_BUZZER_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x01 ,0x96 ,0x03};
unsigned char Do_OUTPORT_LED1_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x02 ,0x95 ,0x03};
unsigned char Do_OUTPORT_LED2_ON	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x04 ,0x93 ,0x03};
unsigned char Do_OUTPORT_LED3_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x08 ,0x9F ,0x03};
unsigned char Do_OUTPORT_LED4_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x10 ,0x87 ,0x03};
unsigned char Do_OUTPORT_ALL_OFF  	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x00 ,0x97 ,0x03};
unsigned char Get_FW_Version		[] = {0x02 ,0x0A ,0x00 ,0x3D ,0xDF ,0x4E ,0x01 ,0x01 ,0xA4 ,0x03};
unsigned char Get_PCD_serial_number	[] = {0x02 ,0x09 ,0x00 ,0x3D ,0xDF ,0x4D ,0x00 ,0xA4 ,0x03};  		
unsigned char Get_SN_FW			[] = {0x02 ,0x0D ,0x00 ,0x3D ,0xDF ,0x4E ,0x01 ,0x01 ,0xDF ,0x4D ,0x00 ,0x31 ,0x03 };
          



int main ( int argc, char **argv ) 
{
	FILE *f;
	int i;
	char * p;
	time_t t = time(NULL);
	int step = 0;

	for(i=0 ; i < argc ; i++)
	{
		p=argv[i];

		if(memcmp(p,"debug"  ,5)==0){ Debug     = 1; }
		if(memcmp(p,"txrx"   ,4)==0){ DebugTXRX = 1; }
		if(memcmp(p,"beep"   ,4)==0){ DoBeep    = 1; }


		if(memcmp(p,"-h" ,2)==0)
		{ 
			printf("\n\remv3000_tools  v2019.10.07\n\r");
			printf("usage moti <options> <...>\n\r");
			printf("Options:\n\r");
			printf("    -h     : This info\n\r");
			printf("    debug  : Debug on\n\r");

			return(0);
		};
	}
	//==================================================================================	
	f= fopen (MOTI_DATA_PATH,"w");
	fwrite("READY" , 1 , 5 , f );
   	fclose(f); 
	//==================================================================================	
	sprintf(aError,"\n\r\n\rMoti started : %s",ctime(&t) ) ;
	errorfile(aError);
	//==================================================================================	


	devfd=openPort(devname,TheSelectedBaudrate);
	if(devfd == -1)
	{
		printf("\n open() failed with error [%s]\n",strerror(errno));
		return -1;
	}

	printf("\n\rREADY\n\r");


	nothing = 0;
	step    = 0;

	while(1)
	{
		RxLoop();

		if(nothing > 1000000 )
		{

			switch(step++)
			{
				case 0: 
					printf("Sending RF ON \n\r");
					SendCommand(Do_RF_ON );
					nothing = 0;
					break;
				case 1: 
					printf("Sending RF OFF \n\r");
					SendCommand(Do_RF_OFF );
					break;
				case 2: 
					printf("Do_OUTPORT_BUZZER_ON \n\r");
					SendCommand(Do_OUTPORT_BUZZER_ON );
					break;
				case 3: 
					printf("Do_OUTPORT_LED1_ON \n\r");
					SendCommand(Do_OUTPORT_LED1_ON );
					break;
				case 4: 
					printf("Do_OUTPORT_LED2_ON \n\r");
					SendCommand(Do_OUTPORT_LED2_ON );
					break;
				case 5: 
					printf("Do_OUTPORT_LED3_ON \n\r");
					SendCommand(Do_OUTPORT_LED3_ON );
					break;
				case 6: 
					printf("Do_OUTPORT_LED4_ON \n\r");
					SendCommand(Do_OUTPORT_LED4_ON );
					break;
				case 7: 
					printf("Do_OUTPORT_ALL_OFF \n\r");
					SendCommand(Do_OUTPORT_ALL_OFF );
					break;
				case 8: 
					printf("Get_PCD_serial_number \n\r");
					SendCommand(Get_PCD_serial_number );
					break;
				case 9: 
					printf("Get_FW_Version \n\r");
					SendCommand(Get_FW_Version );
					break;
				case 10:
					printf("Get_SN_FW \n\r");
					SendCommand(Get_SN_FW );

				default:
					step=0;
					continue;
			}
			nothing = 0;
		}
		
		nothing+=1000;
		usleep(1000);
	}
	printf("exited from the loop ...\n");
	closePort();
	return 0;
}




