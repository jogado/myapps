

typedef struct
{
   unsigned char START_CHR;
   unsigned char REC_LEN;
   unsigned char UNIT;
   unsigned char OPCODE;
   unsigned char DATA[128];
   unsigned char LRC;
   unsigned char END_CHR;
}
OTI_REC;


typedef struct
{
	int Type;
	int Id;
	int Len;
	int Val;
	unsigned char *data;
}
HEAD_REC;



#define MOTI_DATA_PATH		"/tmp/moti.data"
#define MOTI_DATA_PRODTEST	"/tmp/moti.tmp"
#define MOTI_ERROR_LOG_PATH 	"/etc/moti/moti.error.log"
