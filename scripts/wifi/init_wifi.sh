#!/bin/sh
#===============================================================================
# Set Technexion wifi
#===============================================================================





SSID="ESC-2.4"
PSK="STX-Esc540Solutions-ETX"
WPA_SUPPLICANT_FILE="/etc/wpa_supplicant.conf"



update_wpa_file()
{
	echo "ctrl_interface=/var/run/wpa_supplicant" 	 > $WPA_SUPPLICANT_FILE
	echo "ctrl_interface_group=0"					>> $WPA_SUPPLICANT_FILE
	echo "update_config=1" 							>> $WPA_SUPPLICANT_FILE
	echo " " 										>> $WPA_SUPPLICANT_FILE
	wpa_passphrase "$SSID" "$PSK"             		>> $WPA_SUPPLICANT_FILE
}



killall NetworkManager
killall wpa_supplicant

beep;sleep 1

update_wpa_file

wpa_supplicant -D nl80211,wext -i wlan0 -c $WPA_SUPPLICANT_FILE


#wpa_cli -i wlan0 reconnect
#iw wlan0 link 


udhcpc -i wlan0


