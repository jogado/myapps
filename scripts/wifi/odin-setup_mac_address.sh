#!/bin/sh 

CONFIG_FILE=/etc/odin/odin.config
ODIN_OUT_FILE=/tmp/odin.out




clean_up()
{
    echo "Cleaning up"
    exit
}
trap clean_up SIGHUP SIGINT SIGTERM SIGKILL




#===============================================================
# Clean up
#================================================================
if [ -f  /tmp/odin.txt ]; 
then
    rm /tmp/odin.txt
fi  

if [ -f  /tmp/odin.mac ]; 
then
    rm /tmp/odin.mac
fi  



send_command()
{
    count=0

    echo "    TX:$1"

    echo "" > $ODIN_OUT_FILE
    echo $1 > /dev/odin 
    

    while [ ! -f $ODIN_OUT_FILE ]   
    do
        echo $count
        
        if [ $count  -gt 3 ] 
        then
            echo "TIMEOUT"
            return
        fi
        sleep 1
        let count=count+1
    done        
    
    for i in `seq 0 $2`;  
    do 
        if grep -q "$3"  "$ODIN_OUT_FILE" 
        then 
            #echo "    $4  ..... OK"
            echo "    RX:$3"
            return
        fi
        #echo "T$i" 
        sleep 1
    done    

    echo "RX:TIMEOUT"
   
}









#-------------------------------------------------------------------------------------------------
Setup_comms_with_odin()
{
    echo "Setup comms"
    stty -F /dev/odin 115200 -echo -raw    
    cat /dev/odin > $ODIN_OUT_FILE  &
    sleep 2
    send_command "ATE1"       5      "OK"  "ECHO ON        "    
}
#-------------------------------------------------------------------------------------------------
Search_mac_address()
{
    echo "Search mac address"
    send_command "AT+UMLA=2"  5    "OK"  "Search Odin mac address      "    
}
#-------------------------------------------------------------------------------------------------

MAC_RAW=""
MAC_ADDRESS=""

#-------------------------------------------------------------------------------------------------
Format_mac_address()
{
    echo "Format mac address"
    
    echo `cat $ODIN_OUT_FILE | grep -i UMLA: |cut -c7-18` > /tmp/odin.tmp 
    
    MAC_RAW=`cat $ODIN_OUT_FILE | grep -i UMLA: |cut -c7-18`        
    #echo "MAC_RAW=$MAC_RAW"

    MAC_ADDRESS=$(printf "%2s:%2s:%2s:%2s:%2s:%2s" \
                             ` echo $MAC_RAW |cut -c1-2` \
                             ` echo $MAC_RAW |cut -c3-4` \
                             ` echo $MAC_RAW |cut -c5-6` \
                             ` echo $MAC_RAW |cut -c7-8` \
                             ` echo $MAC_RAW |cut -c9-10` \
                             ` echo $MAC_RAW |cut -c11-12` )
    echo "    mac address : $MAC_ADDRESS"  
}
#-------------------------------------------------------------------------------------------------
Check_mac_address()
{
    FILESIZE="${#MAC_ADDRESS}" 
    
    echo "Check mac address"    

    if [ $FILESIZE = '17' ];
    then
        echo "    Mac address size = $FILESIZE bytes. OK"
    else
       echo "    Error: BAD size := $FILESIZE bytes"
       exit
    fi
    
}
#-------------------------------------------------------------------------------------------------
Update_eth1_mac_address()
{
    echo "Updating ETH1 mac address"
    ETH1_MAC=`ifconfig | grep -i eth1 | cut -c39-55`
    echo "    mac-address before : $ETH1_MAC"
    
    ip link set eth1 address $MAC_ADDRESS
    
    ETH1_MAC=`ifconfig | grep -i ETH1 | cut -c39-55`
    echo "    mac-address after  : $ETH1_MAC"
    
    if [ $ETH1_MAC == $MAC_ADDRESS ]
    then
        echo "    Matching OK"
    else
        echo "    Matching error"
        exit
    fi
    
}
#-------------------------------------------------------------------------------------------------
Reboot_Module()
{
    echo "Rebooting module"
    send_command "AT+CPWROFF"  20    "+UUWLE"  "    Rebooting module"         
}
#-------------------------------------------------------------------------------------------------
Set_eth1_ip_address()
{
    
    ETH1_IP=`esc-read-dallas-ip` 

    echo "Setting ip address to :" $ETH1_IP

    ifconfig eth1 $ETH1_IP

    if ifconfig | grep -i $ETH1_IP  
    then 
            echo "    Check OK"
    else
            echo "    Check Error"
    fi
}

Reset_interface()
{
    echo "Reset interface"
    ifdown eth0
    ifdown eth1
    ifup eth1
}


Check_Wifi_enabled()
{
    RET=`cat $CONFIG_FILE | grep -i WIFI: |cut -c6-` 

    
    if [ "$RET" == "YES" ]
    then
         echo "WIFI Enabled ($RET)"
    else
        echo "WIFI Disabled ($RET)"
        exit
    fi
}



Check_Wifi_enabled
Setup_comms_with_odin
Search_mac_address
Format_mac_address
Check_mac_address
Update_eth1_mac_address
Reboot_Module
Reset_interface
Set_eth1_ip_address


echo "finished done"


sync
killall cat  > /dev/null


