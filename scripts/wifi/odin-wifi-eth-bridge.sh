#!/bin/sh

#==============================================================================
# ODIN-wifi-eth-bridge
#---------------------------------------------------------------------------------
# echo 1 > /sys/class/leds/wlan-reset/brightness
# sleep 1
# echo 0 > /sys/class/leds/wlan-reset/brightness
#
# ABT3000 :
#   ODIN-W26 Wifi-Bluethood
#==============================================================================


#==============================================================================
log() {
	echo -e "$*" >&2
}
#==============================================================================
CONFIG_FILE=/etc/odin/odin.config
SSID=`cat $CONFIG_FILE | grep -i SSID: |cut -c6-` 
PSK=`cat $CONFIG_FILE | grep -i PWD: |cut -c5-` 
#==============================================================================


mac=$(cat /sys/class/net/eth1/address | sed 's/://g')
chat_mac="\ AT OK
	  AT+UMLA=2 OK
	  AT+UMLA=2,$mac OK
	  AT&W OK
	  AT+CPWROFF OK
	  \c +STARTUP
	  AT OK"

chat_bridge="\ AT OK
	     AT+UBRGC=0,1,1,3 OK
	     AT+UBRGCA=0,3 OK"

chat_eth="\ AT OK
	  AT+UETHC=1,0 OK
	  AT+UETHC=2,0 OK
	  AT+UETHC=3,0 OK
	  AT+UETHC=4,0 OK
	  AT+UETHCA=3 OK
	  \c +UUETHLU"

chat_wifi="\ AT OK
	   AT+UWSC=0,0 OK
	   AT+UWSC=0,0,1 OK
	   AT+UWSC=0,2,$SSID OK
	   AT+UWSC=0,5,2 OK
	   AT+UWSC=0,8,$PSK OK
	   AT+UWSCA=0,3 OK
	   \c +UUWLE:0"

chat_factory_reset="\ AT OK
        AT+UFACTORY OK
        AT+CPWROFF +STARTUP  
    " 
#===========================================================


#===========================================================
Check_config_file_exist()
{
    echo "Check odin.config file"
   
    if [ ! -f $CONFIG_FILE ] 
    then
        echo "ERROR : Config file don't exist <$CONFIG_FILE>"
        exit
    else
        echo "    OK"
    fi
}
#===========================================================
Check_Wifi_enabled()
{

    RET=`cat $CONFIG_FILE | grep -i WIFI: |cut -c6-` 

    
    if [ "$RET" == "YES" ]
    then
         echo "WIFI Enabled ($RET)"
    else
        echo "WIFI Disabled ($RET)"
        exit
    fi
}
#===========================================================




Check_config_file_exist
Check_Wifi_enabled
    
stty -F /dev/odin 115200 raw

log "\nFactory reset..."
chat -V $chat_factory_reset </dev/odin >/dev/odin

log "\nSetup mac address..."
chat -V $chat_mac </dev/odin >/dev/odin

log "\nSetup bridge..."
chat -V $chat_bridge </dev/odin >/dev/odin

log "\nSetup ethernet..."
chat -V $chat_eth </dev/odin >/dev/odin

log "\nSetup wifi..."
chat -V $chat_wifi </dev/odin >/dev/odin

ifconfig eth0 down

udhcpc -i eth1



EXTERNAL_IP=$(esc-dallas-ip |grep "Ip" | cut -d ':' -f2  2>/dev/null)
if [ ! $? -eq 0 ]; then
        return
fi
ifconfig eth1 down
ifconfig eth1 $EXTERNAL_IP 
ifup eth1



