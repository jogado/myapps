#!/bin/sh 

CONFIG_FILE=/etc/odin/odin.config
ODIN_OUT_FILE=/tmp/odin.out


#------------------------------------------------------------------------------
clean_up()
{
    echo "Cleaning up"

    if [ -f  $ODIN_OUT_FILE ]; 
    then
#        rm $ODIN_OUT_FILE
        echo ""
    fi  

    killall cat  > /dev/null
    sleep 1
    exit
}
trap clean_up EXIT SIGHUP SIGINT SIGTERM SIGKILL
#------------------------------------------------------------------------------
send_command()
{
    count=0

    echo "    TX:$1"

    echo "" > $ODIN_OUT_FILE
    echo $1 > /dev/odin 
    

    while [ ! -f $ODIN_OUT_FILE ]   
    do
        echo $count
        
        if [ $count  -gt 3 ] 
        then
            echo "TIMEOUT"
            return
        fi
        sleep 1
        let count=count+1
    done        
    
    for i in `seq 0 $2`;  
    do 
        if grep -q "$3"  "$ODIN_OUT_FILE" 
        then 
            #echo "    $4  ..... OK"
            
            if [ $i -gt 0 ]
            then
                    echo ""
            fi            
            
            echo "    RX:$3"
            echo ""
            return
        fi
        #echo "T$i" 
        echo -n -e "." 
        sleep 1
    done    

   
    echo "RX:TIMEOUT"
   
}
#------------------------------------------------------------------------------



#------------------------------------------------------------------------------
Check_config_file_exist()
{
    echo "Check if config file exist"
    
    if [ ! -f $CONFIG_FILE ] 
    then
        echo "ERROR : Config file don't exist <$CONFIG_FILE>"
        exit
    else
        echo "    OK"
    fi

    
    if [  -f $CONFIG_FILE.done ]
    then
        cat $CONFIG_FILE.done
        exit
    fi
}
#-------------------------------------------------------------------------------------------------
Setup_comms_with_odin()
{
    echo "Setup comms"
    stty -F /dev/odin 115200 -echo -raw    
    cat /dev/odin > $ODIN_OUT_FILE  &
    sleep 2
    send_command "ATE1"       5      "OK"  "    ECHO ON        "    
}
#-------------------------------------------------------------------------------------------------
Factory_reset()
{
    echo "Factory reset"
    send_command "AT+UFACTORY"  20    "OK"    "    Factory reset   "            
}
#-------------------------------------------------------------------------------------------------
Reboot_Module()
{
    echo "Rebooting module"
    send_command "AT+CPWROFF"  20    "+STARTUP"  "    Rebooting module"         
}
Reboot_Module2()
{
    echo "Rebooting module"
   send_command "AT+CPWROFF"  20    "+UUWLE"    "    Rebooting module"         
}
#-------------------------------------------------------------------------------------------------
Stop_process()
{
    echo "Stop modules process"

    echo "  Deactivate the bridge"
    send_command "AT+UBRGCA=0,4"  3    "OK" "   Deactivate the bridge"         
    
    echo "  Deactivate the Ethernet"
    send_command "AT+UETHCA=4"  3    "OK"  "    Deactivate the Ethernet"         
       
    echo "  Deactivate the Wi-Fi Station"
    send_command "AT+UWSCA=0,4"  3    "OK" "    Deactivate the Wi-Fi Station"         
    
}
#-------------------------------------------------------------------------------------------------
Bridge_configuration()
{
    echo "Bridge_configuration" 

    echo "  Enable bridging between 1:Wi-Fi / 3:Ethernet"
    send_command "AT+UBRGC=0,1,1,3"  3    "OK" "    Enable bridging"         

    echo "  Active on startup"
    send_command "AT+UBRGC=0,0,1"  3    "OK" "    Active on startup"         

    echo "  Store configuration"
    send_command "AT+UBRGCA=0,1"  3    "OK" "    Store configuration"         
    sleep 1
}
#-------------------------------------------------------------------------------------------------
Activate_bridge()
{
    echo "Activate_bridge"
    
    echo "  Activate the bridge configuration"
    send_command "AT+UBRGCA=0,3"  3    "OK" "Activate the bridge configuration"         
    
    echo "  Use RMII"
    send_command "AT+UETHC=1,0"  3    "OK" "Use RMII"         
}
#-------------------------------------------------------------------------------------------------
Ethernet_configuration()
{
    echo "Ethernet_configuration" 

    echo "  Active on startup"
    send_command "AT+UETHC=0,1"  3    "OK" "Use RMII"         
    
    echo "  Use RMII"
    send_command "AT+UETHC=1,0"  3    "OK" "Use RMII"         
    
    echo "  DHCP"
    send_command "AT+UETHC=100,2"  3    "OK" "Use RMII"         
    
    echo "  Auto negotiation = 0"
    send_command "AT+UETHC=4,0"  3    "OK" "Use RMII"         
    
    echo "  Store configuration"
    send_command "AT+UETHCA=1"  3    "OK" "Use RMII"         
}
#-------------------------------------------------------------------------------------------------
Ethernet_activation()
{
    echo "Ethernet_activation"  
    echo "  Activate the Ethernet configuration"   
    echo "  Connect Ethernet cable and wait for interface to go up (+UUETHLU)"   
    send_command "AT+UETHCA=3"  10    "+UUETHLU" "Use RMII"     
}
#-------------------------------------------------------------------------------------------------
Wifi_configuration()
{
    echo "Wifi_configuration" 

    SSID=`cat $CONFIG_FILE | grep -i SSID: |cut -c6-` 
    PWD=`cat $CONFIG_FILE | grep -i PWD: |cut -c5-` 
    AUTHENTICATION=`cat $CONFIG_FILE | grep -i AUTHENTICATION: |cut -c16`      
    
    case $AUTHENTICATION in
        1)
            echo "    Authentication : Open"
            ;;
        2)
            echo "    Authentication : WPA/WPA2 PSK"
            ;;
        3)
            echo "    Authentication : LEAP"
            ;;
        4)
            echo "    Authentication : PEAP"
            ;;
        5)
            echo "    Authentication : EAP-TLS"
            ;;
        *)
              echo "    ERROR : Authentication : ??????"
              exit
            ;;
    esac    

    echo "    Set the Network SSID"
    send_command "AT+UWSC=0,2,$SSID"  10    "OK"    "Set the Network SSID"     

    echo "    Use WPA2"
    send_command "AT+UWSC=0,5,$AUTHENTICATION"  10    "OK"    "Use WPA2"     
    
    echo "    Set Password"
    send_command "AT+UWSC=0,8,$PWD"  10    "OK"    "Set Password"     
    

    echo "    Activate on startup"
    send_command "AT+UWSC=0,0,1"  10    "OK"    "Activate on startup"     

    echo "    Store configuration"
    send_command "AT+UWSCA=0,1"  10    "OK"    "Store configuration"     
}
#-------------------------------------------------------------------------------------------------
Activate_Wifi_station()
{

    echo "Activate_Wifi_station" 
    
    echo "  Activate the Wi-Fi Station"
    echo "  Wait for the Wi-Fi interface to be connected.: +UUWLE:0,112233445566,11 "
    send_command "AT+UWSCA=0,3"  10    "+UUWLE:0"    "Activate the Wi-Fi Station"     

    echo "  Show WiFi status"
    send_command "AT+UWSSTAT"  10    "OK"    "Activate the Wi-Fi Station"   

    cat  $ODIN_OUT_FILE
}
#-------------------------------------------------------------------------------------------------

































Show_status()
{
    echo "AT+UNSTAT" > /dev/odin
    echo "AT+UETHC" > /dev/odin
    
    #Wi-Fi Status        
    echo "AT+UWSSTAT" > /dev/odin  
}






Restart_eth1()
{
    echo "Restarting ETH1 ============================================="
    ifdown eth1
    sleep 1 
    ifup eth1
    sleep 1 
    ifconfig
    echo "Restarting ETH1 Done ========================================"
}

Show_mac_address()
{
    echo "MAC ADDRESS ================================================="
    echo "AT+UMLA=2" > /dev/odin
    sleep 2
    echo "============================================================="
}

Set_jgd_mac_address()
{
    echo "AT+UMLA=2,D4CA6E7D3582" > /dev/odin
    sleep 1
    echo "AT&W" > /dev/odin
    Reboot_Module
    Show_mac_address
}

Set_mac_address_old()
{
    echo "AT+UMLA=2,D4CA6E900492" > /dev/odin
    sleep 1
    echo "AT&W" > /dev/odin
    Reboot_Module
    Show_mac_address
}

Check_Wifi_enabled()
{
    RET=`cat $CONFIG_FILE | grep -i WIFI: |cut -c6-` 

    
    if [ "$RET" == "YES" ]
    then
         echo "WIFI Enabled ($RET)"
    else
        echo "WIFI Disabled ($RET)"
        exit
    fi
}






#============================================================================================
#    START HERE
#============================================================================================
Check_config_file_exist 
Check_Wifi_enabled
Setup_comms_with_odin
Factory_reset
Reboot_Module
Stop_process
Bridge_configuration
Activate_bridge
Ethernet_configuration
Ethernet_activation
Wifi_configuration
Activate_Wifi_station
Reboot_Module2

echo "WARNING : Config already done : `date '+%d/%m/%Y_%H:%M:%S'` " >> $CONFIG_FILE.done
   
clean_up    

