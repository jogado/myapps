#!/bin/sh

FILENAME=$(basename $0)
OUT="/dev/stdout"
I2C_ADDR="4-0050"
I2Cbus=4



PowerOff()
{
    echo 0 > /sys/class/leds/output3/brightness
    echo 0 > /sys/class/leds/LED0/brightness
}


#----------------------------------------------------------------------------------------------------------------------
clean_up()
{
    echo  "clean_up()" >> $OUT
    echo "Power Off" >> $OUT
    PowerOff
    sleep 1
    exit
}
trap clean_up SIGTERM SIGINT EXIT
#----------------------------------------------------------------------------------------------------------------------







show_pwm_val()
{
    VAL=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/$1`
    echo "    $1   :$VAL"
}

show_pwm_point()
{
    PWM=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1_auto_point$1_pwm`
    TEMP=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1_auto_point$1_temp`
    TEMP_HYST=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1_auto_point$1_temp_hyst`

    #echo "$1 PWM: $PWM    TEMP: $TEMP   HYST;$TEMP_HYST"

    printf "%s TEMP: %03d - %02d      PWM: %03d  \n\r" $1 $TEMP $TEMP_HYST $PWM

}

set_pwm_points_registers()
{
  #  echo "    set_pwm_points_registers :  $1  $2  $3"
    `echo $2 > /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1_auto_point$1_pwm`
}

set_pwm_points()
{
    echo "set_pwm_points()"
    set_pwm_points_registers  "1" "64"  "18"
    set_pwm_points_registers  "2" "68"  "20"
    set_pwm_points_registers  "3" "72"  "22"
    set_pwm_points_registers  "4" "76"  "24"
    set_pwm_points_registers  "5" "78"  "26"
    set_pwm_points_registers  "6" "82"  "28"
    set_pwm_points_registers  "7" "86"  "30"
    set_pwm_points_registers  "8" "90"  "32"
    set_pwm_points_registers  "9" "94"  "34"
    set_pwm_points_registers  "10" "98"  "36"
    set_pwm_points_registers  "11" "102" "38"
    set_pwm_points_registers  "12" "106" "40"
    set_pwm_points_registers  "13" "108" "42"
    set_pwm_points_registers  "14" "112" "44"
    set_pwm_points_registers  "15" "116" "46"
    set_pwm_points_registers  "16" "120" "48"

    set_pwm_points_registers  "17" "124" "50"
    set_pwm_points_registers  "18" "128" "52"
    set_pwm_points_registers  "19" "132" "54"
    set_pwm_points_registers  "20" "140" "56"
    set_pwm_points_registers  "21" "144" "68"
    set_pwm_points_registers  "22" "148" "60"
    set_pwm_points_registers  "23" "152" "62"
    set_pwm_points_registers  "24" "156" "64"
    set_pwm_points_registers  "25" "160" "66"
    set_pwm_points_registers  "26" "164" "78"
    set_pwm_points_registers  "27" "168" "70"
    set_pwm_points_registers  "28" "172" "72"
    set_pwm_points_registers  "29" "176" "74"
    set_pwm_points_registers  "30" "180" "76"
    set_pwm_points_registers  "31" "184" "78"
    set_pwm_points_registers  "32" "188" "80"

}


show_lut_registers()
{
    echo "show_lut_registers()"
    s1="0x20 0x21 0x22 0x23 0x24 0x25 0x26 0x27 0x28 0x29 0x2A 0x2B 0x2C 0x2D 0x2E 0x2F"
    s2="0x30 0x31 0x32 0x33 0x34 0x35 0x36 0x37 0x38 0x39 0x3A 0x3B 0x3C 0x3D 0x3E 0x3F"
    s3="0x40 0x41 0x42 0x43 0x44 0x45 0x46 0x47 0x48 0x49 0x4A 0x4B 0x4C 0x4D 0x4E 0x4F"


    #--------------------------------------------------------
    echo -n -e  "    Lut PWM  = "
    for n in $s1
    do
        VAL=`i2cget -f -y 4 0x50 $n`
        printf "%3d " "$(($VAL))"
    done
    echo -n -e "\n\r"

    echo -n -e  "    Lut PWM  = "
    for n in $s2
    do
        VAL=`i2cget -f -y 4 0x50 $n`
        printf "%3d " "$(($VAL))"
    done
    echo -n -e "\n\r"
    #--------------------------------------------------------

    #--------------------------------------------------------
    echo -n -e  "    Lut TEMP = "
    for (( i=16; i < 48; i+=2 ))
    do
        printf "%3d " "$(($i))"
    done
    echo ""
    echo -n -e  "    Lut TEMP = "
    for (( i=48; i < 80; i+=2 ))
    do
        printf "%3d " "$(($i))"
    done
    echo ""
    #--------------------------------------------------------

    #--------------------------------------------------------
    # echo -n -e  "    Lut TEMP = "
    # for (( i=80; i < 112; i+=2 ))
    # do
    #     printf "%3d " "$(($i))"
    # done
    # echo ""
    #
    #
    # echo -n -e  "    Lut PWM  = "
    # for n in $s3
    # do
    #     VAL=`i2cget -f -y 4 0x50 $n`
    #     printf "%3d " "$(($VAL))"
    # done
    # echo -n -e "\n\r\n\r"
    #--------------------------------------------------------
}



show_reg_infos()
{
    echo "show_reg_infos()"
    #-------------------------------------
    CR1=`i2cget -f -y 4 0x50 0x00`
    CR2=`i2cget -f -y 4 0x50 0x01`
    CR3=`i2cget -f -y 4 0x50 0x02`
    echo "    CR1 = $CR1"
    echo "    CR2 = $CR2"
    echo "    CR3 = $CR3"
    #-------------------------------------

    #-------------------------------------
    TC1H=`i2cget -f -y 4 0x50 0x52 | cut -c3-4`
    TC1L=`i2cget -f -y 4 0x50 0x53 | cut -c3-4`
    TC2H=`i2cget -f -y 4 0x50 0x54 | cut -c3-4`
    TC2L=`i2cget -f -y 4 0x50 0x55 | cut -c3-4`
    echo "    TC1 = $TC1H$TC1L"
    echo "    TC2 = $TC2H$TC2L"
    #-------------------------------------

    #-------------------------------------
    RTH=`i2cget -f -y 4 0x50 0x56`
    RTL=`i2cget -f -y 4 0x50 0x57`
    echo "    Remote Temp =" $(($RTH)).$(($RTL))

    LTH=`i2cget -f -y 4 0x50 0x58`
    LTL=`i2cget -f -y 4 0x50 0x59`
    echo "    Local  Temp =" $(($LTH)).$(($LTL))
    #-------------------------------------

    show_pwm_val "pwm1            "
    show_pwm_val "pwm1_enable     "
    show_pwm_val "pwm1_freq       "

    show_pwm_val "fan1_enable     "
    show_pwm_val "fan1_fault      "
    show_pwm_val "fan1_input      "

    show_pwm_val "fan2_enable     "
    show_pwm_val "fan2_fault      "
    show_pwm_val "fan2_input      "
}

first_init()
{
    echo "first_init()"

#    echo "delete_device()"
#    echo max31760 0x50 >/sys/class/i2c-dev/i2c-4/device/delete_device
#    `i2cset -f -y 4 0x50 0x00 0x18`    # 25000 KHZ Hist:2*C
#    `i2cset -f -y 4 0x50 0x01 0x72`    # auto pwm


 #   echo "new_device()"
 #   echo max31760 0x50 >/sys/class/i2c-dev/i2c-4/device/new_device
    `i2cset -f -y 4 0x50 0x00 0x18`    # 25000 KHZ Hist:2*C
    `i2cset -f -y 4 0x50 0x01 0x72`    # Direct fan control enabled
    sleep 3

}



#================================================================================
#================================================================================
#================================================================================
#================================================================================
#================================================================================
first_init
#show_reg_infos
show_lut_registers
sleep 2

VAL1=`i2cget -f -y 4 0x50 0x20`
#echo "VAL1=$VAL1"

if [ "$VAL1" = "0xff" ]
then
    set_pwm_points
    show_lut_registers
fi

echo "Power ON" >> $OUT
echo 1 > /sys/class/leds/output3/brightness


while [ 1 ]
do
    PWM=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1`
    TEMP=`cat /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/temp2_input`


    LTH=`i2cget -f -y 4 0x50 0x58`
    LTL=`i2cget -f -y 4 0x50 0x59`
    LTH1=`echo $(($LTH))`
    LTL1=`echo $(($LTL))`


    TCTH=`i2cget -f -y 4 0x50 0x0E | cut -c3-4`
    TCTL=`i2cget -f -y 4 0x50 0x0F | cut -c3-4`
    TCTH1=`echo $(($TCTH))`
    TCTL1=`echo $(($TCTL))`
    SR=`i2cget -f -y 4 0x50 0x5A`


    MUX=$((PWM/32))
    echo $MUX > /sys/class/leds/LED0/brightness

    echo -n -e  "Temp: $TEMP   pwm:$PWM  LOCAL:$LTH1.$LTL1  TACO:$TCTH$TCTL    STATUS:$SR     \r"
    sleep 1
done
