#!/bin/sh


i2cset -f -y 4 0x50 0x00 0x38       # Set 2C , 25KHZ
i2cset -f -y 4 0x50 0x01 0x03       # No Spin-up , direct access control
i2cset -f -y 4 0x50 0x02 0x30       # Ramp fast

#----------------------------------------------
# Direct fan control enabled
# set PWM to 33
#----------------------------------------------
sleep 3
i2cset -f -y 4 0x50 0x01 0x71
echo 33  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1

#----------------------------------------------
# Direct fan control disabled
#----------------------------------------------
sleep 3
i2cset -f -y 4 0x50 0x01 0x72





R0=`i2cget -f -y 4 0x50 0x00`
R1=`i2cget -f -y 4 0x50 0x01`
R2=`i2cget -f -y 4 0x50 0x02`

echo "MAX31760: register <0>=$R0" > /dev/kmsg
echo "MAX31760: register <1>=$R1" > /dev/kmsg
echo "MAX31760: register <2>=$R2" > /dev/kmsg

~

