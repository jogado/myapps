#!/bin/sh

DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`

# Create and set XDG_RUNTIME_DIR
mkdir -p /run/user/0
chmod 700 /run/user/0
export XDG_RUNTIME_DIR=/run/user/0

# Set debugging environment variables for EGL
export LIBGL_DEBUG=verbose
export EGL_LOG_LEVEL=debug


CONFIG_NAME="xorg-$DEVICE_TYPE.conf.glxgears"

echo "============================================"
echo "Xorg config file: $CONFIG_NAME"
echo "============================================"

killall X

sleep 2

# Run the application
export DISPLAY=:0.0

#X -config $CONFIG_NAME &
X&

sleep 2

kmscube

