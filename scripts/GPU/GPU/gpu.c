#include <EGL/egl.h>
//#include "GLES2/gl2.h"
#include <drm.h>
#include <drmMode.h>

int main() {
    // Open DRM device
    int fd = drmOpen("etnaviv", NULL);

    // Set up EGL display
    EGLDisplay eglDisplay = eglGetDisplay((EGLNativeDisplayType)fd);
    eglInitialize(eglDisplay, NULL, NULL);

    // Create an EGL context and surface (omitting detailed setup code)
    // ...

    // Use OpenGL ES for rendering
    glClearColor(1.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    eglSwapBuffers(eglDisplay, eglSurface);

    // Clean up
    eglTerminate(eglDisplay);
    drmClose(fd);

    return 0;
}
