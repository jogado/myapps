#include <EGL/egl.h>
#include <stdio.h>

int main() {
    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (display == EGL_NO_DISPLAY) {
        fprintf(stderr, "Failed to get EGL display\n");
        return -1;
    }

    if (!eglInitialize(display, NULL, NULL)) {
        fprintf(stderr, "Failed to initialize EGL\n");
        return -1;
    }

    const char *extensions = eglQueryString(display, EGL_EXTENSIONS);
    if (extensions == NULL) {
        fprintf(stderr, "Failed to query EGL extensions\n");
        return -1;
    }

    printf("EGL Extensions: %s\n", extensions);

    eglTerminate(display);
    return 0;
}

