#ifndef _BUILD_VERSION_H
#define _BUILD_VERSION_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>



extern char completeVersion[];
extern char *Build_dateTime(void);

#endif
