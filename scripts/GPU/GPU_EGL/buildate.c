/*****************************************************************************
*
*      Copyright (C) 2021 ... Emsyscon
*
* All rights reserved.
* 
* Permission to use, copy, modify, and distribute this software for any purpose
* with or without fee is hereby granted, provided that the above copyright
* notice and this permission notice appear in all copies.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
* NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
* OR OTHER DEALINGS IN THE SOFTWARE.
* 
* Except as contained in this notice, the name of a copyright holder shall not
* be used in advertising or otherwise to promote the sale, use or other dealings
* in this Software without prior written authorization of the copyright holder.
*
*******************************************************************************/

/******************************************************************************
* Generate build date
* maintainer: Jose Garcia
*******************************************************************************/

#include "buildate.h"


//=============================================================================================================================
char completeVersion[50];

char *Build_dateTime(void)
{
	char aTmp[100];
	int Month = 0;
 	int Year  = 0;
 	int Day   = 0;

	sprintf(aTmp,"%s",__DATE__);

	if(memcmp(aTmp,"Jan",3)==0) Month=1;
	if(memcmp(aTmp,"F"  ,1)==0) Month=2;
	if(memcmp(aTmp,"Mar",3)==0) Month=3;
	if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
	if(memcmp(aTmp,"May",3)==0) Month=5;
	if(memcmp(aTmp,"Jun",3)==0) Month=6;
	if(memcmp(aTmp,"Jul",3)==0) Month=7;
	if(memcmp(aTmp,"Au" ,2)==0) Month=8;
	if(memcmp(aTmp,"S"  ,1)==0) Month=9;
	if(memcmp(aTmp,"O"  ,1)==0) Month=10;
	if(memcmp(aTmp,"N"  ,1)==0) Month=11;
	if(memcmp(aTmp,"D"  ,1)==0) Month=13;
	

	aTmp[6]=0;
	Day  = atoi(&aTmp[4]);
	Year = atoi(&aTmp[7]);

	sprintf(completeVersion,"%02d/%02d/%04d %s"
		,Day
		,Month
		,Year
		,__TIME__);

	return(completeVersion);
}

//===========================================================================================================

