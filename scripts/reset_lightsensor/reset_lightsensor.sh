#!/bin/sh



#	pinctrl_i2c3: i2c3grp {
#		fsl,pins = <
#			MX6QDL_PAD_GPIO_3__I2C3_SCL		0x4001b8b1
#			MX6QDL_PAD_GPIO_6__I2C3_SDA		0x4001b8b1
#		>;
#	};
#       define MX6QDL_PAD_GPIO_3__GPIO1_IO03
#       define MX6QDL_PAD_GPIO_6__GPIO1_IO06   


echo 3   > /sys/class/gpio/export     
echo 6   > /sys/class/gpio/export     
echo out > /sys/class/gpio/gpio3/direction
echo out > /sys/class/gpio/gpio6/direction

echo 0   > /sys/class/gpio/gpio3/value
echo 0   > /sys/class/gpio/gpio6/value
usleep 500000
cat /sys/class/i2c-dev/i2c-0/device/0-0044/lux


echo 1   > /sys/class/gpio/gpio3/value
echo 1   > /sys/class/gpio/gpio6/value

echo 3   > /sys/class/gpio/unexport     
echo 6   > /sys/class/gpio/unexport     

echo 0 > /sys/class/i2c-dev/i2c-0/device/0-0044/power_state
usleep 200000
echo 1 > /sys/class/i2c-dev/i2c-0/device/0-0044/power_state
usleep 200000

cat /sys/class/i2c-dev/i2c-0/device/0-0044/lux

