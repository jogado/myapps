#!/bin/sh


#=========================================================================================
ODD="0"
EVENT="0"

IsEven()
{
	INPUT=$1
	LEN=${#INPUT}

	let OFFSET=LEN-5

	VAL=`echo $1 | cut -c$OFFSET`

	even_array=("0" "2" "4" "6" "8" "A" "C" "E" )

	for i in  "${even_array[@]}"
	do
		if [ "$i" = "$VAL" ];then
			echo "$VAL is even number"
			ODD="0"
			EVENT="1"
			return
		fi
	done
	echo "$VAL could be odd number"
	ODD="1"
	EVENT="0"
}

IsOdd()
{
	INPUT=$1
	LEN=${#INPUT}

	let OFFSET=LEN-5

	VAL=`echo $1 | cut -c$OFFSET`

	odd_array=("1" "3" "5" "7" "9" "B" "D" "F" )

	for i in  "${odd_array[@]}"
	do
		if [ "$i" = "$VAL" ];then
			echo "$VAL is odd number"
			ODD="1"
			EVENT="0"
			return
		fi
	done
	echo "$VAL could be even number"
	ODD="0"
	EVENT="1"
}
#=========================================================================================


echo "input $1"

IsOdd  "$1"
IsEven "$1"

