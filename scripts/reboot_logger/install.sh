#!/bin/sh



TARGET=$1

install_badblocks()
{

    echo "=================================================="
    echo "install_badblocks ...."
    scp ./e2fsprogs-badblocks_1.45.4-r0_cortexa7t2hf-neon.ipk  root@$TARGET:/home/root

    ssh root@$TARGET "cd /home/root"
    ssh root@$TARGET "opkg install e2fsprogs-badblocks_1.45.4-r0_cortexa7t2hf-neon.ipk"
    ssh root@$TARGET "which badblocks"
    echo "=================================================="
    echo ""
    echo ""

}


if echo $TARGET | grep -q "192.168"; then

# Clean up
ssh root@$TARGET "rm /home/root/reboot_logger.txt"
ssh root@$TARGET "rm /etc/init.d/reboot_logger.sh"
ssh root@$TARGET "rm /etc/rc0.d/K20reboot_logger.sh"
ssh root@$TARGET "rm /etc/rc5.d/S99reboot_logger.sh"

ssh root@$TARGET "killall alive.sh"



scp ./reboot_logger.sh  root@$TARGET:/etc/init.d/
ssh root@$TARGET "chmod 777 /etc/init.d/reboot_logger.sh"
ssh root@$TARGET "ls -la /etc/init.d/reboot_logger.sh"



scp ./alive.sh          root@$TARGET:/home/root/
ssh root@$TARGET "chmod 777 /home/root/alive.sh"


ssh root@$TARGET "ln -sf /etc/init.d/reboot_logger.sh /etc/rc5.d/S99reboot_logger.sh"
ssh root@$TARGET "ls -la /etc/rc5.d/S99reboot_logger.sh"



ssh root@$TARGET "cd /etc/rc0.d"
ssh root@$TARGET "ln -sf /etc/init.d/reboot_logger.sh /etc/rc0.d/K20reboot_logger.sh"
ssh root@$TARGET "ls -la /etc/rc0.d/K20reboot_logger.sh"



install_badblocks


ssh root@$TARGET "sync"
ssh root@$TARGET "reboot"




exit 0
fi

echo ""
echo "============================\n\r"
echo "Error No target defined\n\r"
echo "============================\n\r"
exit

