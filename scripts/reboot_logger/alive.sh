#!/bin/sh

LOGFILE="/home/root/alive.txt"

#==============================================================================
LOGIT()
{
    echo "$1" >> $LOGFILE
    sync
}
#==============================================================================


udhcpc


time_convert()
{
    TSEC=$1
    SEC=`expr $TSEC % 60`
    MIN=`expr $TSEC / 60 % 60`
    HOUR=`expr $TSEC / 3600 % 24`
    DAY=`expr $TSEC / 3600 / 24`
    ELAPSED=`printf '%d days %02d:%02d:%02d \n' "$DAY" "$HOUR" "$MIN" "$SEC"`
#    ELAPSED=`printf '%02d:%02d:%02d \n' "$HOUR" "$MIN" "$SEC"`
}



start_time="$(date -u +%s)"
#==============================================================================
while [ 1 ]
do
    end_time="$(date -u +%s)"
    elapsed="$(($end_time-$start_time))"
    time_convert $elapsed


    cur_time="$(date -u +%s)"
    S=$((cur_time%60))
    M=$((cur_time/60%60))
    H=$((cur_time/60/60%24))
    CURTIME="$(printf "%02d:%02d:%02d" $H $M $S)"

    if [ "$S" == "0" ]
    then
        LOGIT "ALIVE    : $CURTIME : ELAPSED: $elapsed ($ELAPSED)"
    else
        echo -n -e "ALIVE    : $CURTIME : ELAPSED: $elapsed ($ELAPSED) \r"
    fi

    if [ "$elapsed" == "3600" ]
    then
        break
    fi


    usleep 500000
done
#==============================================================================
beep
beep
beep

