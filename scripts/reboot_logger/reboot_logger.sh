#!/bin/sh

udhcpc

DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `


CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

if [ ${#CPU_TEMP} -gt 5 ]  # temp > 99°
then
        CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
        CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4-5`"
else

        CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
        CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3-4`"
fi





LOGFILE="/home/root/reboot_logger.txt"

cur_time="$(date -u +%s)"
S=$((cur_time%60))
M=$((cur_time/60%60))
H=$((cur_time/60/60%24))
BOOTED="$(printf "BOOT TIME : %02d:%02d:%02d" $H $M $S)"

CURRENT_IP=`ifconfig |grep -m 1 inet |cut -d ':' -f2 | cut -d ' ' -f1 | tr -d '\r'`

DEVICE=`dallas-esc |grep  Product|cut -d ':' -f 2`

KERNEL=`opkg list | grep uimage | cut -d '-' -f 6`

BOOTIME=`dmesg |tail -n 1`

#==============================================================================
LOGIT()
{
    echo "$1" >> /dev/tty0
    echo "$1"
    echo "$1" >> $LOGFILE
    sync
}
#==============================================================================



if [ $1 == "start" ]
then

    echo  "============================================================================" >> $LOGFILE
    LOGIT "DATE     : `date`"
    LOGIT "DEVICE   : $DEVICE"
    LOGIT "TYPE     : $DEVICE_TYPE"
    LOGIT "KERNEL   : $KERNEL"
    LOGIT "FILE     : $LOGFILE"
    LOGIT "IP       : $CURRENT_IP"
    LOGIT "STATE    : $1 "
    LOGIT "CPU TEMP : $CPU_TEMP_STR"
    LOGIT "BOOTIME  : $BOOTIME"

fi


if [ $1 == "stop" ]
then
    echo  "============================================================================" >> $LOGFILE
    LOGIT "DATE     : `date`"
    LOGIT "DEVICE   : $DEVICE"
    LOGIT "TYPE     : $DEVICE_TYPE"
    LOGIT "KERNEL   : $KERNEL"
    LOGIT "STATE    : $1 "
    LOGIT "CPU TEMP : $CPU_TEMP_STR"
    LOGIT "BOOTIME  : $BOOTIME"
    echo  "****************************************************************************"       >> $LOGFILE
    echo "VAL/LOG/DMESG"                >> $LOGFILE
    cat /var/log/dmesg |tail -n 50      >> $LOGFILE
    echo  "****************************************************************************"       >> $LOGFILE
    echo "VAL/LOG/MESSAGES"             >> $LOGFILE
    cat /var/log/messages |tail -n 50   >> $LOGFILE
fi



/home/root/alive.sh &

exit





































exit





CLEAR="\033c"
RED="\033[00;91m"
GREEN="\033[00;92m"
YELLOW="\033[00;93m"
BLUE="\033[00;94m"
WHITE="\033[00;40m"

echo 70000 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp
echo 90000 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp


CPU_ZONE_0=` cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp`
CPU_ZONE_1=` cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp`

#echo "CPU_ZONE_0_point_0 = $CPU_ZONE_0"
#echo "CPU_ZONE_0_point_1 = $CPU_ZONE_1"

#==============================================================================
#TEMP_WARNING
# Test Temperature sensor
#
#==============================================================================
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `
#==============================================================================

LOGFILE="/home/root/reboot_logger.txt"


#==============================================================================
clean_up()
{
    printf "${CLEAR}"  > /dev/tty0
    printf "${CLEAR}"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT
#==============================================================================





cur_time="$(date -u +%s)"
S=$((cur_time%60))
M=$((cur_time/60%60))
H=$((cur_time/60/60%24))
# LOGFILE="$(printf "/home/root/temperature-%02d:%02d:%02d.txt" $H $M $S)"
# MYDATE=`date '+%Y-%m-%d'`
# LOGFILE="/home/root/temperature-$MYDATE.txt"


#==============================================================================
LOGIT()
{
    TST=`echo $1 | grep -i 'Freq:'`

    CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

    if [ "$TST" = "" ]
    then
        if [ $COLOR_CHANGED -eq 1 ]
        then
            COLOR_CHANGED=0
            printf "${WHITE}"  > /dev/tty0
            printf "${WHITE}"
        fi
    else
        if [ $CPU_TEMP -ge $CPU_ZONE_1 ]
        then
                COLOR_CHANGED=1
                printf "${RED}"    > /dev/tty0
                printf "${RED}"
        elif [ $CPU_TEMP -ge $CPU_ZONE_0 ]
        then
                COLOR_CHANGED=1
                printf "${YELLOW}"  > /dev/tty0
                printf "${YELLOW}"
        fi
    fi

    echo "$1" >> /dev/tty0
    echo "$1"
    echo "$1" >> $LOGFILE
}
#==============================================================================

















#==============================================================================
PATH_1="/sys/class/i2c-dev/i2c-0/device/0-0048/hwmon/hwmon0/temp1_input"
PATH_2="/sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
PATH_3="/sys/class/i2c-dev/i2c-0/device/0-004a/hwmon/hwmon0/temp1_input"

if [ -f $PATH_1 ]
then
    EXE="cat $PATH_1"
fi

if [ -f $PATH_2 ]
then
    EXE="cat $PATH_2"
fi

if [ -f $PATH_3 ]
then
    EXE="cat $PATH_3"
fi


#======================================================================================================================
get_i2c_temperature()
{
    I2C_TEMP=`$EXE`

    if [ ${#I2C_TEMP} -gt 5 ] # temp > 99°
    then
        I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
        I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4-5`"
    else
        I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
        I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3-4`"
    fi
}
#======================================================================================================================
get_cpu_temperature()
{
    #------------------------------------------------
    CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
    #------------------------------------------------

    if [ ${#CPU_TEMP} -gt 5 ]  # temp > 99°
    then
        CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
        CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4-5`"
    else

        CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
        CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3-4`"
    fi
}
#======================================================================================================================



ELASPED=0
let ELAPSED+=1
TM=`date +%T`


LOGIT "==========================================================================================="
LOGIT "DATE     : `date`"
LOGIT "FILE     : $LOGFILE"
LOGIT "DEVICE   : $DEVICE_TYPE"
LOGIT "uname -a : `uname -a`"
LOGIT "uImage   : `ls -la /boot/Image`"
LOGIT "==========================================================================================="



#================================================================================================================================
start_time="$(date -u +%s)"

OLD_S=""

while [ 1 ]
do

    # end_time="$(date -u +%s)"
    # elapsed="$(($end_time-$start_time))"
    # S=$((elapsed%60))
    # M=$((elapsed/60%60))
    # H=$((elapsed/60/60%24))
    # hhmmss="$(printf "%02d:%02d:%02d" $H $M $S)"

    S=`date | cut -d ' ' -f5 | cut -c7-8`
    M=`date | cut -d ' ' -f5 | cut -c4-5`
    H=`date | cut -d ' ' -f5 | cut -c1-2`
    hhmmss="$H:$M:$S"

    if [ "$S" != "$OLD_S" ]
    then

        #------------------------------------------------
        get_cpu_temperature
        get_i2c_temperature
        #------------------------------------------------
        #------------------------------------------------
        LOG="==>:$hhmmss:cpu:$CPU_TEMP_STR:i2c:$I2C_TEMP_STR"
        OLD_S=$S
        LOGIT "$LOG "
    fi
    usleep 300000
done
#================================================================================================================================












T1=`cat /proc/uptime | cut -d ' ' -f 1`

while [ 1 ]
do

    T2=`cat /proc/uptime | cut -d ' ' -f 1`
    T3=`echo $T2 - $T1 | bc `


    #------------------------------------------------
    get_i2c_temperature
    get_cpu_temperature
    get_cpu_freq
    #------------------------------------------------


    elapsed=`echo $T3 | cut -d '.' -f1`

    S=$((elapsed%60))
    M=$((elapsed/60%60))
    H=$((elapsed/60/60%24))

    MS=`echo $T3 | cut -d '.' -f2`


    elapsed_hhmmss="$(printf "%02d:%02d:%02d:%s" $H $M $S $MS)"


    ET="$(printf "%07.2f" $T3)"
    #------------------------------------------------
    LOG="$ET: $elapsed_hhmmss  $ cpu:$CPU_TEMP_STR i2c:$I2C_TEMP_STR F:$FREQ"
    LOGIT "$LOG "

done



start_time="$(date -u +%s)"
T1=`cat /proc/uptime | cut -d ' ' -f 1`

while [ 1 ]
do

    TIME_STAMP=` date | cut -d ' ' -f4 `
    end_time="$(date -u +%s)"
    elapsed="$(($end_time-$start_time))"

    #------------------------------------------------
    get_i2c_temperature
    get_cpu_temperature
    get_cpu_freq
    #------------------------------------------------
    S=$((elapsed%60))
    M=$((elapsed/60%60))
    H=$((elapsed/60/60%24))
    MILLITIME=`cat /proc/uptime | cut -d '.' -f2 | cut -c 1-2`

    T2=`cat /proc/uptime | cut -d ' ' -f 1`
    T3=`echo $T2 - $T1 | bc `

    elapsed_hhmmss="$(printf "%02d:%02d:%02d:%s" $H $M $S $MILLITIME)"
    #------------------------------------------------
    LOG="$TIME_STAMP ($elapsed_hhmmss)  Temp:$CPU_TEMP_STR   Freq:$FREQ"
    LOG="($elapsed_hhmmss) cpu:$CPU_TEMP_STR Freq:$FREQ $I2C_TEMP_STR "
    LOG="$T3 $elapsed_hhmmss cpu:$CPU_TEMP_STR i2c:$I2C_TEMP_STR F:$FREQ"
    LOGIT "$LOG "

    usleep 100000
done

