#!/bin/sh

#
# Test Temperature sensor 
#
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `




#==============================================================================
#  me=`basename "$0"`
#  pid=`pidof -x $me`
#  echo "me=$me"
#  echo "pid=$pid"
#  if [ "$pid" != "" ]
#  then
#      echo "Already started"    
#  	exit 0
#  fi
#==============================================================================

chmod 777 /test/result-bat_level.txt
echo "===================================================================" >> /test/result-bat_level.txt



#==============================================================================
mkdir -p "/test"
while [ 1 ] 
do
	TM=`date +%T`
    SEC=`echo $TM |cut -d ':' -f3`
    MIN10=`echo $TM  |cut -d ':' -f2|cut -c 2`

    if [ "$SEC" == "00" ] && [ "$MIN10" == 0 ] 
    then
        BAT=`emvci -get_bat_level`
        echo "$TM:$BAT" >> /test/result-bat_level.txt
        sleep 2
    fi
	usleep 500000
done
#==============================================================================



# `clear > /dev/tty0`
# echo "$TM i2C:($I2C_MIN)($res)($I2C_MAX_STR)      CPU:$CPU" > /dev/tty0


