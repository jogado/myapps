#!/bin/sh

chmod 777 /etc/init.d/show_battery_level.sh
/etc/init.d/show_battery_level.sh &



# Check that device has a BCR
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------

BARCODE_DEV="/dev/barcode"

if [ ! -L $BARCODE_DEV ]
then
        echo "device not present : $BARCODE_DEV"
        exit
fi

if [ $DEVICE_TYPE = "mv3000_6_0" ]
then
	echo "MV3000_6_0:  config: No BCR"
	exit
fi

if [ $DEVICE_TYPE = "abt3000_4_0" ]
then
	echo "abt3000_4_0: config: No BCR"
	exit
fi

#
# BARCODE WAKE-UP
#
echo 1 >  /sys/class/leds/barcode_wake/brightness
sleep 1
echo 0 >  /sys/class/leds/barcode_wake/brightness


#
# BARCODE trigger level
#
echo 0 >  /sys/class/leds/barcode_trigger/brightness


if [ -f "/etc/barcode-info/device_initialized.log" ]
then
     echo "Barcode is initialized"
     return
fi


sleep 1
barcode-info &
sync
