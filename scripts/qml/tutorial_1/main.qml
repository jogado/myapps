import QtQuick 2.0

Rectangle {
    id: page
    width: 480; height: 640
    color: "lightgray"

    Text {
        id: helloText
        text: "Hello world!"
        y: 200
        anchors.horizontalCenter: page.horizontalCenter
        font.pointSize: 48; font.bold: true
    }
}
