import QtQuick 2.0

Rectangle {
    id: page
    width: 480; height: 640
    color: "lightgray"

    Text {
        id: helloText
        text: "Hello world!"
        y: 200
        anchors.horizontalCenter: page.horizontalCenter
        font.pointSize: 48; font.bold: true
    }

	Rectangle {
	    width: 100
	    height: 100
	    color: "red"
	    border.color: "black"
	    border.width: 5
	    radius: 10
	}

}
