#!/bin/sh
#------------------------------------------------------------------------------
clean_up()
{
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------


HR_FOLDER="/etc/HR"
HR_LOOP_COUNTER="hr_loop_counter.log"

#------------------------------------------------------------------------------
# prepare folder for first time
#------------------------------------------------------------------------------
if [ ! -d $HR_FOLDER ]
then
	echo "folder do not exist"
	echo "creating folder : $HR_FOLDER"
	mkdir -p $HR_FOLDER
fi

if [ ! -f $HR_FOLDER/$HR_LOOP_COUNTER ]
then
	echo "creating file   : $HR_FOLDER/$HR_LOOP_COUNTER"
	echo "0" > $HR_FOLDER/$HR_LOOP_COUNTER
fi
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
VAL=`cat $HR_FOLDER/$HR_LOOP_COUNTER`
VAL=$((VAL+1))
echo $VAL > $HR_FOLDER/$HR_LOOP_COUNTER
#------------------------------------------------------------------------------


