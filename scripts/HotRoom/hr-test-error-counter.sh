#!/bin/sh
#------------------------------------------------------------------------------
clean_up()
{
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT

#------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/demo_qt5.pid
#------------------------------------------------------------------------------


HR_FOLDER="/etc/HR"
HR_TEST_ERROR_COUNTER="hr_test_error_counter.log"
HR_ERROR_COUNTER="hr_error_counter.log"
HR_TEST_NAME="hr_test_description.log"

#------------------------------------------------------------------------------
# prepare folder for first time
#------------------------------------------------------------------------------
if [ ! -d $HR_FOLDER ]
then
	echo "folder do not exist"
	echo "creating folder : $HR_FOLDER"
	mkdir -p $HR_FOLDER
fi

if [ ! -f $HR_FOLDER/$HR_TEST_ERROR_COUNTER ]
then
	echo "creating file   : $HR_FOLDER/$HR_TEST_ERROR_COUNTER"
	echo "0" > $HR_FOLDER/$HR_TEST_ERROR_COUNTER
fi

if [ ! -f $HR_FOLDER/$HR_ERROR_COUNTER ]
then
	echo "creating file   : $HR_FOLDER/$HR_ERROR_COUNTER"
	echo "0" > $HR_FOLDER/$HR_ERROR_COUNTER
fi
#------------------------------------------------------------------------------




#------------------------------------------------------------------------------
# LOOP ERROR
#------------------------------------------------------------------------------
VAL=`cat $HR_FOLDER/$HR_TEST_ERROR_COUNTER`
VAL=$((VAL+1))
echo $VAL > $HR_FOLDER/$HR_TEST_ERROR_COUNTER
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# TOTAL ERROR
#------------------------------------------------------------------------------
VAL=`cat $HR_FOLDER/$HR_ERROR_COUNTER`
VAL=$((VAL+1))
echo $VAL > $HR_FOLDER/$HR_ERROR_COUNTER
#------------------------------------------------------------------------------

