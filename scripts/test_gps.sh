#!/bin/sh
#====================================================================================================================================
# Test GPS Fix delay
#====================================================================================================================================
MP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------
clean_up()
{
	echo "Cleaning"
	killall ./ubx
	killall ubx
}
trap clean_up SIGTERM SIGINT EXIT
#---------------------------------------------------------------------------------





#---------------------------------------------------------------------------
log()
{
	echo $1
}
#---------------------------------------------------------------------------
gps_on()
{
	#echo "GPS ON"
	echo 1 > /sys/class/leds/modem-pwr/brightness
}
#---------------------------------------------------------------------------
gps_off()
{
	#echo "GPS OFF"
	echo 0 > /sys/class/leds/modem-pwr/brightness
}
#---------------------------------------------------------------------------
gps_reset()
{
	echo "GPS RESET"
	gps_off
	sleep 2
	gps_on
}





log "Starting"

ubx > /dev/null &

gps_reset

ELAPSED=0
while [ 1 ]
do


    if [ -f "/tmp/nmea.GNGLL.Valid" ]
    then
    	res=`cat /tmp/nmea.GNGLL.Valid | tr -d '\n\r'`
	    echo "$ELAPSED :nmea.GNGLL.Valid =  $res"

	    if [ "$res" == "A" ]
	    then
	    	echo "Valid gps frame found (elapsed = $ELAPSED sec)"
	    	echo `date` "Valid gps frame found (elapsed = $ELAPSED sec)" >> /home/root/test_gps.log
	    	let ELAPSED=0
	    	rm /tmp/nmea.GNGLL.Valid
	    	gps_reset
	    fi
    fi

    sleep 1
    let ELAPSED=ELAPSED+1
    echo -n -e "\r"
    echo -n -e "$ELAPSED"
done






#   RX-GPS[1] $GNGLL,5052.48443,N,00429.55504,E,075046.00,A,A*74
#   [0:00:08] [GNGLL] - Latitude  : 5052.48443 N
#   [0:00:08] [GNGLL] - Longitude : 00429.55504 E
#   [0:00:08] [GNGLL] - Time      : 075046.00
#   [0:00:08] [GNGLL] - Valid     : A
#   [0:00:08] [GNGLL] - Mode      : A

#   RX-GPS[1] $GNRMC,075047.00,A,5052.48454,N,00429.55498,E,0.086,,250321,,,A*67
#   nmea_data : $GNRMC,075047.00,A,5052.48454,N,00429.55498,E,0.086,,250321,,,A*67                calc_crc (67)
#   [0:00:08] [GNRMC] - Time      : 075047.00
#   [0:00:08] [GNRMC] - Valid     : A
#   [0:00:08] [GNRMC] - Latitude  : 5052.48454 N
#   [0:00:08] [GNRMC] - Longitude : 00429.55498 E
#   [0:00:08] [GNRMC] - Speed      : 0.086
#   [0:00:08] [GNRMC] - Course      :
#   [0:00:08] [GNRMC] - Date      : 250321


#   nmea.GNGGA.Altitude   nmea.GNGLL.Time       nmea.GNRMC.Time
#   nmea.GNGGA.Latitude   nmea.GNGLL.Valid      nmea.GNRMC.Valid
#   nmea.GNGGA.Longitude  nmea.GNRMC.Course     nmea.GNVTG.Heading
#   nmea.GNGGA.Quality    nmea.GNRMC.Date       nmea.GNVTG.Speed
#   nmea.GNGLL.Latitude   nmea.GNRMC.Latitude   nmea.GPGSV.SatInView
#   nmea.GNGLL.Longitude  nmea.GNRMC.Longitude  rakinda.log
#   nmea.GNGLL.Mode       nmea.GNRMC.Speed      system.elapsed_time

