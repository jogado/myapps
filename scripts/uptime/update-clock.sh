#!/bin/sh

OUT="/dev/kmsg"

CONFIG_FILE="/etc/update-clock/update-clock.config"
TMP_FILE="/tmp/update-clock.log"
NTP_TIME_WINDOWS_COM="20.101.57.9"


fping -t100 $NTP_TIME_WINDOWS_COM > $TMP_FILE
RES=`cat $TMP_FILE  | grep -i "is alive" `

LEN=$(echo -n $RES | wc -m)

if [ $LEN -gt 5 ]
then
        echo "update-clock: time.windows.com ntp server online ( $NTP_TIME_WINDOWS_COM )" > $OUT
else
        beep
        echo "update-clock: time.windows.com ntp server not reachable ( $NTP_TIME_WINDOWS_COM) " > $OUT
        exit 0
fi

CURRENT_DATE=`date +%F-:%T`

echo "update-clock: current date : $CURRENT_DATE" > $OUT





pid=`pidof ntpd`
if [ "$pid" != "" ]; then
    /etc/init.d/ntpd stop
fi


pid=`pidof ntpd`
if [ "$pid" != "" ]; then
	kill $pid
fi


date 010113592001.00  > null
date +%F-:%T > null
sleep 1
ntpd -q -g > null

echo "ntpd result: $?"


NEW_DATE=`date +%F-:%T`
echo "update-clock: New date     : $NEW_DATE" > $OUT

hwclock --systohc
echo "update-clock: Date saved in RTC" > $OUT

