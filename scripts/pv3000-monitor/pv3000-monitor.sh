#!/bin/bash

T1=$(awk '{print int($1)}' /proc/uptime)

# Copy of predefined lookup table DTS pv3000_1_1
PREDEFINED_POINT=(
      0         #   < 18°C
      0         #   18 -  20
      0         #   20 -  22
     60         #   22 -  24  
     70         #   24 -  28        
     80         #   28 -  30    
     90         #   30 -  32   
    100         #   32 -  34   
    110         #   34 -  36   
    120         #   36 -  38   
    130         #   38 -  40   
    140         #   40 -  42   
    150 
    160 
    170 
    180 
    190 
    200 
    210 
    220 
    230 
    240 
    250 
    255 
    255 
    255 
    255 
    255 
    255 
    255 
    255 
    255
)

OVERWRITE_POINT=(
     0   3   5   8  10  13  15  18  20  23  #  1 - 10
    25  28  30  33  36  38  41  43  46  48  # 11 - 20
    51  53  56  58  61  64  66  69  71  74  # 21 - 30
    76  79  81  84  86  89  91  94  97  99  # 31 - 40
   102 104 107 109 112 114 117 119 122 125  # 41 - 50
   127 130 132 135 137 140 142 145 147 150  # 51 - 60
   153 155 158 160 163 165 168 170 173 175  # 61 - 70
   178 181 183 186 188 191 193 196 198 201  # 71 - 80
   203 206 209 211 214 216 219 221 224 226  # 81 - 90
   229 231 234 236 239 242 244 247 249 252  # 91 - 100
   255                                      # last value
)




MODE=$1

if [ "$MODE" = "OVERWRITE" ]
then
    echo "MODE=OVERWRITE   ( using local table )"
    echo 1  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1_enable

else
    echo "MODE=PREDEFINED  ( predefined table )"
fi



while true
do
    sleep 1
    T2=$(awk '{print int($1)}' /proc/uptime)
    UPTIME_SEC=$(($T2 - $T1))

    # Calculate days, hours, minutes, and seconds
    UPTIME_DAYS=$((UPTIME_SEC / 86400))
    UPTIME_HOURS=$(( (UPTIME_SEC % 86400) / 3600 ))
    UPTIME_MINUTES=$(( (UPTIME_SEC % 3600) / 60 ))
    UPTIME_SECONDS=$(( UPTIME_SEC % 60 ))

    # Format uptime as (days) hh:mm:ss
    UPTIME=$(printf "(%d) %02d:%02d:%02d" $UPTIME_DAYS $UPTIME_HOURS $UPTIME_MINUTES $UPTIME_SECONDS)

    TMP2=$(cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/temp2_input)
    TMP=$(echo $TMP2 | cut -c 1-2)

    PWM1=$(cat /sys/bus/i2c/devices/i2c-4/4-0050/hwmon/hwmon2/pwm1)


    if [ "$MODE" = "OVERWRITE" ]
    then
        INDEX=$((TMP-1))

        VAL=${OVERWRITE_POINT[$INDEX]}
        echo $VAL  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1

        echo "UPTIME: $UPTIME TEMP: $TMP PWM: $PWM1 estimated: ( ${OVERWRITE_POINT[$INDEX]} )"

    else
        INDEXL=$(( (TMP - 18) / 2 ))
        INDEXH=$(( INDEXL + 1 ))

        if [ $INDEXL -ge 0 ] && [ $INDEXH -lt ${#PREDEFINED_POINT[@]} ]; then
            echo "UPTIME: $UPTIME TEMP: $TMP PWM: $PWM1 estimated: ( ${PREDEFINED_POINT[$INDEXL]} - ${PREDEFINED_POINT[$INDEXH]} )"
        else
            echo "UPTIME: $UPTIME TEMP: $TMP PWM: $PWM1 PREDEFINED_POINT: Out of range"
        fi
    fi







    if (( UPTIME_SEC % 13 == 0 )); then
        echo "RED LED ON"
        echo 0      > /sys/class/leds/LED1/brightness
        echo 0      > /sys/class/leds/LED2/brightness
        echo 255    > /sys/class/leds/LED0/brightness
    elif (( UPTIME_SEC % 13 == 4 )); then
        echo "GREEN LED ON"
        echo 0      > /sys/class/leds/LED0/brightness
        echo 0      > /sys/class/leds/LED2/brightness
        echo 255    > /sys/class/leds/LED1/brightness
    elif (( UPTIME_SEC % 13 == 8 )); then
        echo 255 > /sys/class/leds/LED2/brightness
        echo "BLUE LED ON"
        echo 0      > /sys/class/leds/LED0/brightness
        echo 0      > /sys/class/leds/LED1/brightness
        echo 255    > /sys/class/leds/LED2/brightness
    fi


    if (( UPTIME_SEC % 60 == 10 )); then
        echo "BEEP TEST"
        beep
    fi

done

