#!/bin/sh


i2cset -f -y 4 0x50 0x00 0x58       # Set 2�C , 25KHZ
i2cset -f -y 4 0x50 0x01 0x03       # No Spin-up , direct access control
i2cset -f -y 4 0x50 0x02 0x30       # Ramp fast

sleep 1

echo 50  >   /sys/bus/i2c/devices/4-0050/hwmon/hwmon2/pwm1


R0=`i2cget -f -y 4 0x50 0x00`
R1=`i2cget -f -y 4 0x50 0x01`
R2=`i2cget -f -y 4 0x50 0x02`

echo "MAX31760: register <0>=$R0" > /dev/kmsg
echo "MAX31760: register <1>=$R1" > /dev/kmsg
echo "MAX31760: register <2>=$R2" > /dev/kmsg

