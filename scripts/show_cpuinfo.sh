#!/bin/sh

#
# Test Temperature sensor 
#
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`





# define OCOTP_TESTER3_SPEED_SHIFT	8
# OCOTP_TESTER3[9:8] (see Fusemap Description Table offset 0x440)
# defines a 2-bit SPEED_GRADING
#
#	OCOTP_TESTER3_SPEED_GRADE0,#	OCOTP_TESTER3_SPEED_GRADE1,
#	OCOTP_TESTER3_SPEED_GRADE2,
#	OCOTP_TESTER3_SPEED_GRADE3,
#	OCOTP_TESTER3_SPEED_GRADE4,
#u32 get_cpu_speed_grade_hz(void)


# define OCOTP_TESTER3_TEMP_SHIFT	6
# OCOTP_TESTER3[7:6] (see Fusemap Description Table offset 0x440)
#  defines a 2-bit SPEED_GRADING
#	CPU Temperature Grades
#	#define TEMP_COMMERCIAL         0
#	#define TEMP_EXTCOMMERCIAL      1
#	#define TEMP_INDUSTRIAL         2
#	#define TEMP_AUTOMOTIVE         3
#	




echo "OCOTP"

SOC_UID=`cat /sys/devices/soc0/soc_uid`


B1=`echo $SOC_UID | cut -c1-2`
B2=`echo $SOC_UID | cut -c3-4`
B3=`echo $SOC_UID | cut -c5-6`
B4=`echo $SOC_UID | cut -c7-8`
B5=`echo $SOC_UID | cut -c9-10`
B6=`echo $SOC_UID | cut -c11-12`
B7=`echo $SOC_UID | cut -c13-14`
B8=`echo $SOC_UID | cut -c15-16`


echo $SOC_UID
echo $B1-$B2-$B3-$B4-$B5-$B6-$B7-$B8


exit 0





