#!/bin/sh

FILENAME=$(basename $0)


DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`


timestamp()
{
	echo "==============================================" >> $FILENAME.log
	echo `date`  >> /home/root/$FILENAME.log
}



test_temp()
{

	EXE=`cat /sys/class/i2c-dev/i2c-0/device/0-0048/hwmon/hwmon0/temp1_input`

	DEG=`echo $EXE | cut -c1-2`
	MIN=`echo $EXE | cut -c3-5`

	RES_TEMP="Temp: $DEG.$MIN"
		
}


test_lightsensor()
{
	echo 0 > /sys/class/i2c-dev/i2c-0//device/0-0044/range
	echo 0 > /sys/class/i2c-dev/i2c-0/device/0-0044/power_state
	usleep 100000
	echo 1 > /sys/class/i2c-dev/i2c-0/device/0-0044/power_state
	usleep 100000
	
	res=`cat /sys/class/i2c-dev/i2c-0/device/0-0044/lux`
	RES_LUX="Test light sensor  :$res"
		
}



test_sam()
{

	SAM_TMP="/tmp/samci.tmp"
	samci 1 > $SAM_TMP 
	
	DATA=$(cat $SAM_TMP |grep samslot=0 | grep serialnr=UNKNOWN)

	if [ ! -n "${DATA}" ]
	then
		SN_0=$(cat $SAM_TMP |grep samslot=0 | cut -d '=' -f 4)
		RES_SAM="SAM OK ( SN1:0x$SN_0 )"
	else
		RES_SAM="SAM 0 not Present"
	fi

}

test_temp
test_lightsensor
test_sam

echo `date` "   [ $RES_LUX ]  [ $RES_SAM ]  [ $RES_TEMP ] " 
echo `date` "   [ $RES_LUX ]  [ $RES_SAM ]  [ $RES_TEMP ]" >> /home/root/$FILENAME.log


FILE="/home/root/REBOOT"
if [ -f "$FILE" ]; then

	for i in {1..30}
	do
	   echo "Shutdown  $i/30 "

		if [ -f "$FILE" ]
		then
		   	sleep 1
		else
			exit 0
		fi
	done

	beep
	usleep 100000
	beep
	usleep 100000
	reboot
fi

exit 0


