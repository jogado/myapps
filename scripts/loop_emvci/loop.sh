#!/bin/sh

for i in {1..1000} 
do
	BAT=`emvci -get_bat_level | tr -d '\n\r' `
	TYPE=`emvci -get 30 | tr -d '\n\r' `
	SERIAL=`emvci -get 31 | tr -d '\n\r' `


	echo "----------------------------------------"
	echo "loop         : $i"
	echo "battery test : $BAT"
	echo "emvci test   : $TYPE $SERIAL"
	echo "Prod Scripts :"
 	/usr/bin/production-test/testscripts/get_emv3000_sn.sh /dev/stdout 30 28
 	/usr/bin/production-test/testscripts/get_emv3000_sn.sh /dev/stdout 30 26 1 
	sleep 1
done
