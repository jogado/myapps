#!/bin/sh

#
# Test Temperature sensor
#
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `
#==============================================================================
clean_up()
{
	killall cat
}
trap clean_up SIGTERM SIGINT EXIT
#==============================================================================
















#==============================================================================

I2C_MIN=100000
I2C_MAX=0

get_i2c_temperature()
{
	I2C_ADDR="0-004a"
	I2Cbus=0
	EXE="cat /sys/class/i2c-dev/i2c-0/device/0-004a/hwmon/hwmon0/temp1_input"

	if [ $DEVICE_TYPE == "abt3000_5_0" ]
	then
		I2C_ADDR="0-0049"
		I2Cbus=0
		EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
	fi

	if [ $DEVICE_TYPE == "mv3000_7_0" ]
	then
		I2C_ADDR="0-0049"
		I2Cbus=0
		EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
	fi

	I2C_TEMP=`$EXE`


	if [ $I2C_TEMP -gt $I2C_MAX ]
	then
		let I2C_MAX=$I2C_TEMP

	fi

	if [ $I2C_TEMP -lt $I2C_MIN ]
	then
		let I2C_MIN=$I2C_TEMP

	fi



	if [ ${#I2C_MAX} -gt 5 ]
	then
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-3`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c4`"
	else
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-2`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c3`"
	fi

	if [ ${#I2C_MIN} -gt 5 ]
	then
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-3`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c4`"
	else
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-2`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c3`"
	fi


	if [ ${#I2C_TEMP} -gt 5 ]
	then
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
	else
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
	fi


}



#==============================================================================




CPU_MIN=100000
CPU_MAX=0
get_cpu_temperature()
{
	#------------------------------------------------
	CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
	#------------------------------------------------

	if [ $CPU_TEMP -gt $CPU_MAX ]
	then
		let CPU_MAX=$CPU_TEMP
	fi

	if [ $CPU_TEMP -lt $CPU_MIN ]
	then
		let CPU_MIN=$CPU_TEMP
	fi



	if [ ${#CPU_MAX} -gt 5 ]
	then
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-3`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c4`"
	else
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-2`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c3`"
	fi

	if [ ${#CPU_MIN} -gt 5 ]
	then
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-3`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c4`"
	else
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-2`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c3`"
	fi


	if [ ${#CPU_TEMP} -gt 5 ]
	then
		CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
		CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
	else
		CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
		CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"
	fi




}



get_cpu_freq()
{
	FREQ=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq`
	echo $FREQ
}



get_cpu_uid()
{
	SOC_UID=`cat /sys/devices/soc0/soc_uid`
	echo "CPU UID             : $SOC_UID"
}


get_cpu_core()
{
	CPU0_OLNINE=`cat /sys/devices/system/cpu/cpu0/online`
	CPU1_OLNINE=`cat /sys/devices/system/cpu/cpu1/online`
	CPU2_OLNINE=`cat /sys/devices/system/cpu/cpu2/online`
	CPU3_OLNINE=`cat /sys/devices/system/cpu/cpu3/online`

	echo "CPU 0 online        : $CPU0_OLNINE"
	echo "CPU 1 online        : $CPU1_OLNINE"
	echo "CPU 2 online        : $CPU2_OLNINE"
	echo "CPU 3 online        : $CPU3_OLNINE"

}

get_cpu_available_frequencies()
{
	CPU_AVAILABLE_FREQUENCIES=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies`
	echo "CPU frequencies     : $CPU_AVAILABLE_FREQUENCIES"
}

get_cpu_max_frequency()
{
	CPU_MAX_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq`
	echo "CPU MAX frequencies : $CPU_MAX_FREQUENCY"
}


get_cpu_min_frequency()
{
	CPU_MIN_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq`
	echo "CPU MIN frequencies : $CPU_MIN_FREQUENCY"
}

get_cpu_scaling_governor()
{
	CPU_SCALING_GOVERNOR=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor`
	echo "CPU scaling governor: $CPU_SCALING_GOVERNOR"
}



#================================================================================================================================================
#mpstat
#Linux 4.14.98 (abt3000-5-0)     05/03/21        _aarch64_       (4 CPU)
#
#07:11:11     CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
#07:11:11     all    0.62    0.00    0.30    0.00    0.15    0.15    0.00    0.00   98.78
#000000000111111111122222222223333333333444444444455555555556666666666777777777788888888889
#123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
#mpstat -I SCPU
#
#Linux 4.14.98-06374-g48c893a9abbd-dirty (abt3000-5-0)	05/03/21	_aarch64_	(4 CPU)
#
#06:48:04     CPU      HI/s   TIMER/s  NET_TX/s  NET_RX/s   BLOCK/s IRQ_POLL/s TASKLET/s  SCHED/s HRTIMER/s     RCU/s
#06:48:04       0      0.01    242.09      0.50      1.66      0.00      0.00     61.91      7.98      0.00     72.25
#06:48:04       1      0.00    194.34      0.00      0.00      0.00      0.00      0.00      6.46      0.00     79.36
#06:48:04       2      0.00    194.89      0.00      0.00      0.00      0.00      0.00      6.41      0.00     77.03
#06:48:04       3      0.00    189.85      0.00      0.00      0.00      0.00      0.02      6.22      0.00     71.22
#================================================================================================================================================

show_cpu_load()
{

	CPU_IDLE=`mpstat  |grep -i all | cut -c 84-89`
	CPU_USER=`mpstat  |grep -i all | cut -c 10-24`
	CPU_SYSTEM=`mpstat  |grep -i all | cut -c 34-39`

}




echo""
echo""
get_cpu_uid
get_cpu_core
get_cpu_available_frequencies
get_cpu_max_frequency
get_cpu_min_frequency
get_cpu_scaling_governor
echo""
echo""


TIME_ASOLUTE=0

while [ 1 ]
do

	FREQA=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies`
	FREQS="100000 200000 400000 600000 1200000 1600000 1800000 1600000 1200000 600000 400000 200000 100000"

	echo "userspace"	> /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor

	cat /dev/random > /dev/null &

	for i in $FREQS
	do


		echo "Set Frequency to $i"

		echo "userspace" > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
		echo $i > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
		echo $i > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
		echo $i > /sys/devices/system/cpu/cpu0/cpufreq/scaling_setspeed


		for s in {1..300}
		do
			C_FREQ=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq`

			#----------------------------------------------------------------------------------
			I2C_TEMP=`cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input`

			if [ ${#I2C_TEMP} -gt 5 ]
			then
				I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
				I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
			else
				I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
				I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
			fi


			#----------------------------------------------------------------------------------

			#----------------------------------------------------------------------------------
			CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

			if [ ${#CPU_TEMP} -gt 60 ]
			then
				CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
				CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
			else
				CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
				CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"
			fi
			#----------------------------------------------------------------------------------


#			echo "$TIME_ASOLUTE;$s;$C_FREQ;$CPU_TEMP_STR"
#			echo "$TIME_ASOLUTE;$s;$C_FREQ;$CPU_TEMP_STR" >> log.txt

			echo "$TIME_ASOLUTE;$s;$C_FREQ;$CPU_TEMP_STR;$I2C_TEMP_STR"
			echo "$TIME_ASOLUTE;$s;$C_FREQ;$CPU_TEMP_STR;$I2C_TEMP_STR" >> log.txt
			echo "$TIME_ASOLUTE;$s;$C_FREQ;$CPU_TEMP_STR;$I2C_TEMP_STR" > /dev/tty0



			let TIME_ASOLUTE+=1
			sleep 1
		done
	done

	clean_up
	exit 0
done







ELASPED=0
let ELAPSED+=1


while [ 1 ]
do

	#------------------------------------------------
	get_i2c_temperature
	get_cpu_temperature
	get_cpu_freq
	#------------------------------------------------

	#------------------------------------------------
	TM=`date +%T`


	#echo "$ELAPSED,$TM,i2C,$I2C_TEMP_STR,CPU,$CPU_TEMP_STR,Freq,$FREQ"
	#echo "$ELAPSED,$TM,i2C,$I2C_TEMP_STR,CPU,$CPU_TEMP_STR,Freq,$FREQ" > /dev/tty0
	#echo "$ELAPSED,$TM,i2C,$I2C_TEMP_STR,CPU,$CPU_TEMP_STR,Freq,$FREQ" >> log.txt

	LOG="$ELAPSED,$I2C_TEMP_STR,$CPU_TEMP_STR,$FREQ"
	echo $LOG
	echo $LOG > /dev/tty0
	echo $LOG >> log.txt

	#------------------------------------------------

	sleep 10
	let ELAPSED+=10
done

