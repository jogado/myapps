#!/bin/sh



EMV_DEVICE="/dev/emv3000"

if [ ! -L $EMV_DEVICE ]
then
	echo "Device missing : $EMV_DEVICE"
	exit
fi

CURRENT_VERSION=$(emvci -get 28 | tr -d '\n\r')
NEW_VERSION=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4`
CURRENT_BUILD=`emvci -get 28 |cut -d '-' -f 2| cut -d '.' -f4 | tr -d '\n\r' |tr -d ' '`
NEW_BUILD=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4|cut -d ' ' -f6`

echo "Current: ( $CURRENT_BUILD ) $CURRENT_VERSION"
echo "New    : ( $NEW_BUILD ) $NEW_VERSION"




if [ "$NEW_BUILD" -gt "$CURRENT_BUILD" ] ; then
	echo "auto emv3000 Upgrade is disabled"
	# echo "Upgrade in progress"
	#./update_firmware.sh
else
	echo "Current version is up-to-date"
fi





