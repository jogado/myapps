#!/bin/bash
BLHOST=${BLHOST:="blhost"}

OUT="/dev/tty0"
EMV_DEVICE="/dev/emv3000"



log() {
    echo "$*"
}





check_version()
{

	if [ ! -L $EMV_DEVICE ]
	then
		echo "Device missing : $EMV_DEVICE"
		exit
	fi

	CURRENT_VERSION=$(emvci -get 28 | tr -d '\n\r')
	NEW_VERSION=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4`
	CURRENT_BUILD=`emvci -get 28 |cut -d '-' -f 2| cut -d '.' -f4 | tr -d '\n\r' |tr -d ' '`
	NEW_BUILD=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4|cut -d ' ' -f6`

	echo "Current: ( $CURRENT_BUILD ) $CURRENT_VERSION"
	echo "New    : ( $NEW_BUILD ) $NEW_VERSION"

	if [ "$NEW_BUILD" -gt "$CURRENT_BUILD" ] ; then
		echo "auto emv3000 Upgrade is disabled"
	else
		echo "Current version is up-to-date"
		exit 0
	fi
}




reset_emv3000() {

    if [ -e /sys/class/leds/emv-reset/brightness ]; then
        log "******* Turn-off EMV3000"
        echo 1 > /sys/class/leds/emv-reset/brightness
        sleep 1

        log "******* Turn-on EMV3000"
        echo 0 > /sys/class/leds/emv-reset/brightness
        sleep 1
    fi;
}


wait_boot() {
    version=`$BLHOST -u -- get-property 1`
    if [ "$?" != "0" ]; then
        log "******* Waiting for EMV3000"
        wait_boot
	else
		log "******* version = $version"
    fi
}



flash_firmware() {

	echo "";echo ""
    log "******* Configuring QSPI"
    $BLHOST -u -- write-memory 0x20001000 ./qspi_config.bin  
    $BLHOST -u -- configure-quadspi 1 0x20001000						
    $BLHOST -u -- write-memory 0x400DA00C "{{00 00 01 00}}"				
    $BLHOST -u -- write-memory 0x400DA024 "{{01 00 00 00}}"				

	echo "";echo ""
	log "******* Erase QSPI Flash"
	$BLHOST -u -- flash-erase-region 0x68200000 0x100000

	echo "";echo ""
	log "******* Programming Update Image"
	log "update_sfw_1"
	$BLHOST -u -- flash-image hex/production/update_sfw_1.hex 

	echo "";echo ""
	log "update_sfw_2"
	$BLHOST -u -- flash-image hex/production/update_sfw_2.hex 

	echo "";echo ""
	log "******* RESET DEVICE"
	$BLHOST -u -- reset  
}



warning()
{
	BUILD=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4|cut -d ' ' -f6`

		beep ; usleep 100000
		beep ; usleep 100000
		beep ; usleep 100000

		printf "\033[2J\033[9;0]\033[0;0H" > $OUT	
 
        echo -n -e "\n\r\n\r\n\r\n\r\n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           **         FLASHING EMV3000         ** \n\r" >  $OUT
		echo -n -e "           **            ( $BUILD )              ** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           **             PLEASE               ** \n\r" >  $OUT
        echo -n -e "           **      DON'T POWER OFF DEVICE      ** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT  
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "\n\r\n\r" 												>  $OUT

}




if [ "$1" != '-f' ]
then
	check_version
fi


script=$(realpath $0)
script_dir=$(dirname $script)

warning
reset_emv3000
wait_boot

flash_firmware

log "******* All done"



sleep 15

beep;usleep 100000
beep;usleep 100000
beep;usleep 100000


printf "\033[2J\033[9;0]\033[0;0H :" > $OUT



