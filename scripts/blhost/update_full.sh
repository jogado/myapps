#!/bin/bash
BLHOST=${BLHOST:="blhost"}


OUT="/dev/tty0"
EMV_DEVICE="/dev/emv3000"

#==============================================================================
log() {
    echo "$*"
}
#==============================================================================
reset_emv3000() {

    if [ -e /sys/class/leds/emv-reset/brightness ]; then
        log "******* Turn-off EMV3000"
        echo 1 > /sys/class/leds/emv-reset/brightness
        sleep 1

        log "******* Turn-on EMV3000"
        echo 0 > /sys/class/leds/emv-reset/brightness
        sleep 1
    fi;
}
#==============================================================================
wait_boot() {
    version=`$BLHOST -u -- get-property 1`
    if [ "$?" != "0" ]; then
        wait_boot
    fi
}
#==============================================================================

#==============================================================================
flash_bootloader() {
    version=`$BLHOST -u -- get-property 1`
    if [ "$?" != "0" ]; then
        log "******* Waiting for EMV3000"
        flash_bootloader
    else
        log "******* Verify bootloader"
        log "$version"

        [[ "$version" =~ "K1.3.0" ]] && flash_bootloader_130
        [[ "$version" =~ "E3.0.1" ]] && flash_bootloader_301
        [[ "$version" =~ "E3.1.0" ]] && flash_bootloader_310
        [[ "$version" =~ "E3.1.1" ]] && flash_bootloader_310
    fi
}
#==============================================================================

#==============================================================================
flash_bootloader_310() {
    log "******* Bootloader E3.1.0 found"
    log "******* ERASING OLD bootloader"
    $BLHOST -u -- flash-image bin/Clearboot_E3.1.0.hex erase

    log "******* Reset device"
    $BLHOST -u -- reset

    sleep 10
    flash_bootloader
}
#==============================================================================

#==============================================================================
flash_bootloader_301() {
    log "******* Bootloader E3.0.1 found"
    log "******* ERASING OLD bootloader"
    $BLHOST -u -- flash-image bin/Clearboot_E3.0.1.hex erase

    log "******* Reset device"
    $BLHOST -u -- reset

    sleep 10
    flash_bootloader
}
#==============================================================================




#==============================================================================
warning()
{
	BUILD=`cat /opt/emv3000/hex/production/update_sfw_version.txt | tr -d '\n\r' |cut -d '.' -f4|cut -d ' ' -f6`

		beep ; usleep 100000
		beep ; usleep 100000
		beep ; usleep 100000

		printf "\033[2J\033[9;0]\033[0;0H" > $OUT	
 
        echo -n -e "\n\r\n\r\n\r\n\r\n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           **       FLASHING FULL EMV3000      ** \n\r" >  $OUT
		echo -n -e "           **            ( $BUILD )              ** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           **             PLEASE               ** \n\r" >  $OUT
        echo -n -e "           **      DON'T POWER OFF DEVICE      ** \n\r" >  $OUT
        echo -n -e "           **                                  ** \n\r" >  $OUT
        echo -n -e "           ************************************** \n\r" >  $OUT  
        echo -n -e "           ************************************** \n\r" >  $OUT
        echo -n -e "\n\r\n\r" 												>  $OUT

}
#==============================================================================


#==============================================================================
flash_bootloader_130() {
    log "******* Bootloader K1.3.0 found"
    log "******* We have CORRECT Bootloader"

	echo "";echo ""
    log "******* Unsecure"
    $BLHOST -u -- flash-erase-all-unsecure

	echo "";echo ""
    log "******* Configuring QSPI"
    $BLHOST -u -- write-memory 0x20001000 ./qspi_config.bin 
    $BLHOST -u -- configure-quadspi 1 0x20001000						
    $BLHOST -u -- write-memory 0x400DA00C "{{00 00 01 00}}"		
    $BLHOST -u -- write-memory 0x400DA024 "{{01 00 00 00}}"		

	echo "";echo ""
    log "******* Erase Main Flash"
    $BLHOST -u -- flash-erase-all 0

	echo "";echo ""
	log "******* Erase QSPI Flash"
	$BLHOST -u -- flash-erase-region 0x680FF000 0x001000
	$BLHOST -u -- flash-erase-region 0x68100000 0x100000



	echo "";echo ""
	log "******* Programing FW_EXT"
	$BLHOST -u -- flash-image hex/production/fw_ext_a.hex erase 
	$BLHOST -u -- flash-image hex/production/fw_ext_b.hex erase 

	echo "";echo ""
	log "******* Programing FW_INT"
	$BLHOST -u -- flash-image hex/production/fw_int.hex erase 

	echo "";echo ""
	log "******* Programing SBL"
	$BLHOST -u -- flash-image hex/production/sbl.hex erase


	echo "";echo ""
	log "******* Programing Production DATA"
	$BLHOST -u -- flash-image hex/production/prod_keys.hex erase 
	$BLHOST -u -- flash-image hex/production/keystore_a.hex erase 
	$BLHOST -u -- flash-image hex/production/keystore_b.hex erase 
	$BLHOST -u -- flash-image hex/production/lifecycle.hex erase 
	$BLHOST -u -- flash-image hex/production/kcv.hex erase 


	echo "";echo ""
	log "******* Programing Terminal Settings"
	$BLHOST -u -- flash-image hex/production/TerminalSettings.hex erase


	echo "";echo ""
    log "******* Reset device"
    $BLHOST -u -- reset

}




script=$(realpath $0)
script_dir=$(dirname $script)

warning
reset_emv3000
wait_boot
flash_bootloader


log "******* All done"

sleep 15

beep;usleep 100000
beep;usleep 100000
beep;usleep 100000


printf "\033[2J\033[9;0]\033[0;0H :" > $OUT

