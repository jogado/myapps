#!/bin/sh

#
# Test Temperature sensor 
#
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `


#==============================================================================

I2C_MIN=100000
I2C_MAX=0

get_i2c_temperature()
{
	I2C_ADDR="0-004a"
	I2Cbus=0
	EXE="cat /sys/class/i2c-dev/i2c-0/device/0-004a/hwmon/hwmon0/temp1_input"

	if [ $DEVICE_TYPE == "abt3000_5_0" ]
	then
		I2C_ADDR="0-0049"
		I2Cbus=0
		EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
	fi	
	
	I2C_TEMP=`$EXE`
	
		
	if [ $I2C_TEMP -gt $I2C_MAX ]  
	then
		let I2C_MAX=$I2C_TEMP

	fi

	if [ $I2C_TEMP -lt $I2C_MIN ]
	then
		let I2C_MIN=$I2C_TEMP

	fi	
	


	if [ ${#I2C_MAX} -gt 5 ] 
	then
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-3`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c4`"
	else
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-2`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c3`"
	fi	

	if [ ${#I2C_MIN} -gt 5 ] 
	then
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-3`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c4`"
	else
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-2`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c3`"
	fi	


	if [ ${#I2C_TEMP} -gt 5 ] 
	then
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
	else
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
	fi	

	
}
#==============================================================================




CPU_MIN=100000
CPU_MAX=0
get_cpu_temperature()
{
	#------------------------------------------------
	CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
	#------------------------------------------------

	if [ $CPU_TEMP -gt $CPU_MAX ]  
	then
		let CPU_MAX=$CPU_TEMP
	fi

	if [ $CPU_TEMP -lt $CPU_MIN ]
	then
		let CPU_MIN=$CPU_TEMP
	fi	
	


	if [ ${#CPU_MAX} -gt 5 ] 
	then
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-3`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c4`"
	else
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-2`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c3`"
	fi	

	if [ ${#CPU_MIN} -gt 5 ] 
	then
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-3`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c4`"
	else
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-2`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c3`"
	fi	


	if [ ${#CPU_TEMP} -gt 5 ] 
	then
		CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
		CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
	else
		CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
		CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"
	fi	



	
}









while [ 1 ] 
do
	#------------------------------------------------
	get_i2c_temperature
	get_cpu_temperature
	#------------------------------------------------

	#------------------------------------------------
	TM=`date +%T`
	echo "$TM   i2C:[ cur:$I2C_TEMP_STR  min:$I2C_MIN_STR  max:$I2C_MAX_STR]      CPU:[ cur:$CPU_TEMP_STR  min:$CPU_MIN_STR  max:$CPU_MAX_STR]"
	#------------------------------------------------

	sleep 5
done



# `clear > /dev/tty0`
# echo "$TM i2C:($I2C_MIN)($res)($I2C_MAX_STR)      CPU:$CPU" > /dev/tty0


