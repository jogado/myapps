#!/bin/sh

#==============================================================================
#
# Test Temperature sensor 
#
#==============================================================================
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `
#==============================================================================

LOGFILE="/home/root/log.txt"

DATE=`date`
TIME=`date |cut -d ' ' -f 5`

HOUR=`date |cut -d ' ' -f 5 | cut -d ':' -f1`
MIN=`date  |cut -d ' ' -f 5 | cut -d ':' -f2`
SEC=`date  |cut -d ' ' -f 5 | cut -d ':' -f3`


echo "date : $DATE"
echo "TIME : $TIME"
echo "Hour : $HOUR"
echo "Min  : $MIN"
echo "Sec  : $SEC"

LOGFILE="/home/root/temperature-$HOUR-$MIN-$SEC.txt"
echo  $LOGFILE


#==============================================================================
LOGIT()
{
	echo $1
	echo $1 >> /dev/tty0
	echo $1 >> $LOGFILE
}
#==============================================================================




















I2C_MIN=100000
I2C_MAX=0

get_i2c_temperature()
{
	I2C_ADDR="0-0048"
	I2Cbus=0
	EXE="cat /sys/class/i2c-dev/i2c-0/device/0-004a/hwmon/hwmon0/temp1_input"

	if [ $DEVICE_TYPE == "abt3000_5_0" ]
	then
		I2C_ADDR="0-0049"
		I2Cbus=0
		EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
	fi	

	if [ $DEVICE_TYPE == "mv3000_7_0" ]
	then
		I2C_ADDR="0-0049"
		I2Cbus=0
		EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
	fi	
	
	I2C_TEMP=`$EXE`
	
		
	if [ $I2C_TEMP -gt $I2C_MAX ]  
	then
		let I2C_MAX=$I2C_TEMP

	fi

	if [ $I2C_TEMP -lt $I2C_MIN ]
	then
		let I2C_MIN=$I2C_TEMP

	fi	
	


	if [ ${#I2C_MAX} -gt 5 ] 
	then
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-3`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c4`"
	else
		I2C_MAX_STR=`echo $I2C_MAX | cut -c1-2`
		I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c3`"
	fi	

	if [ ${#I2C_MIN} -gt 5 ] 
	then
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-3`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c4`"
	else
		I2C_MIN_STR=`echo $I2C_MIN | cut -c1-2`
		I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c3`"
	fi	


	if [ ${#I2C_TEMP} -gt 5 ] 
	then
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
	else
		I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
		I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
	fi	

	
}
#==============================================================================




CPU_MIN=100000
CPU_MAX=0
get_cpu_temperature()
{
	#------------------------------------------------
	CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
	#------------------------------------------------

	if [ $CPU_TEMP -gt $CPU_MAX ]  
	then
		let CPU_MAX=$CPU_TEMP
	fi

	if [ $CPU_TEMP -lt $CPU_MIN ]
	then
		let CPU_MIN=$CPU_TEMP
	fi	
	


	if [ ${#CPU_MAX} -gt 5 ] 
	then
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-3`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c4`"
	else
		CPU_MAX_STR=`echo $CPU_MAX | cut -c1-2`
		CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c3`"
	fi	

	if [ ${#CPU_MIN} -gt 5 ] 
	then
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-3`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c4`"
	else
		CPU_MIN_STR=`echo $CPU_MIN | cut -c1-2`
		CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c3`"
	fi	


	if [ ${#CPU_TEMP} -gt 5 ] 
	then
		CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
		CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
	else
	#	CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
	#	CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"

    CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
	
	fi	



	
}



get_cpu_freq()
{
	FREQ=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq`
}



get_cpu_uid()
{
	SOC_UID=`cat /sys/devices/soc0/soc_uid`
	LOGIT "CPU UID             : $SOC_UID"
}


get_cpu_core()
{
	echo 1 > /sys/devices/system/cpu/cpu0/online
	echo 0 > /sys/devices/system/cpu/cpu1/online
	echo 0 > /sys/devices/system/cpu/cpu2/online
	echo 0 > /sys/devices/system/cpu/cpu3/online

	CPU0_OLNINE=`cat /sys/devices/system/cpu/cpu0/online`
	CPU1_OLNINE=`cat /sys/devices/system/cpu/cpu1/online`
	CPU2_OLNINE=`cat /sys/devices/system/cpu/cpu2/online`
	CPU3_OLNINE=`cat /sys/devices/system/cpu/cpu3/online`
	
	LOGIT "CPU 0 online        : $CPU0_OLNINE"
	LOGIT "CPU 1 online        : $CPU1_OLNINE"
	LOGIT "CPU 2 online        : $CPU2_OLNINE"
	LOGIT "CPU 3 online        : $CPU3_OLNINE"
	
}

get_cpu_available_frequencies()
{
	CPU_AVAILABLE_FREQUENCIES=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies`
	LOGIT "CPU frequencies     : $CPU_AVAILABLE_FREQUENCIES"
}

get_cpu_available_governors()
{
	CPU_AVAILABLE_GOVERNORS=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors`
	LOGIT "CPU GOVERNORS       : $CPU_AVAILABLE_GOVERNORS"
}

get_cpu_max_frequency()
{
	CPU_MAX_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq`
	LOGIT "CPU MAX frequencies : $CPU_MAX_FREQUENCY"
}


get_cpu_min_frequency()
{
	CPU_MIN_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq`
	LOGIT "CPU MIN frequencies : $CPU_MIN_FREQUENCY"
}

get_cpu_scaling_governor()
{
	CPU_SCALING_GOVERNOR=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor`
	LOGIT "CPU scaling governor: $CPU_SCALING_GOVERNOR"
}



MODE="performance"
echo $MODE > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo $MODE > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
echo $MODE > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
echo $MODE > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor





#echo 1200000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
#echo 1200000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq

ELASPED=0
let ELAPSED+=1
TM=`date +%T`

echo "===========================================================================================" >> $LOGFILE
get_cpu_uid
get_cpu_core
get_cpu_available_frequencies
get_cpu_available_governors
get_cpu_max_frequency
get_cpu_min_frequency
get_cpu_scaling_governor



 
echo "DATE     : `date`" >> $LOGFILE
echo "DEVICE   : $DEVICE_TYPE">> $LOGFILE
echo "uname -a : `uname -a`" >> $LOGFILE
echo "uImage   : `ls -la /boot/Image`" >> $LOGFILE
echo "===========================================================================================" >> $LOGFILE





while [ 1 ] 
do
	
	#------------------------------------------------
#	get_i2c_temperature
	get_cpu_temperature
	get_cpu_freq
	#------------------------------------------------

	#------------------------------------------------
	LOG="$ELAPSED,Temp,$CPU_TEMP_STR,Freq,$FREQ" 


	LOGIT $LOG
#	echo $LOG
#	echo $LOG > /dev/tty0
#	echo $LOG >> /home/root/log.txt

#	S=$((ELAPSED%60))
#	M=$((ELAPSED/60%60))
#	H=$((ELAPSED/60/60%24))
#	printf "$H:$M:$S," 
#	printf "$H:$M:$S," >>/home/root/log.txt
	#------------------------------------------------

	sleep 1
	let ELAPSED+=1
done



