#!/bin/sh

CLEAR="\033c"
RED="\033[00;91m"
GREEN="\033[00;92m"
YELLOW="\033[00;93m"
BLUE="\033[00;94m"
WHITE="\033[00;40m"

echo 70000 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp
echo 90000 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp


CPU_ZONE_0=` cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp`
CPU_ZONE_1=` cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp`

#echo "CPU_ZONE_0_point_0 = $CPU_ZONE_0"
#echo "CPU_ZONE_0_point_1 = $CPU_ZONE_1"

#==============================================================================
#TEMP_WARNING
# Test Temperature sensor
#
#==============================================================================
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `
#==============================================================================

LOGFILE="/home/root/log.txt"


#==============================================================================
clean_up()
{
    printf "${CLEAR}"  > /dev/tty0
    printf "${CLEAR}"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT
#==============================================================================





cur_time="$(date -u +%s)"
S=$((cur_time%60))
M=$((cur_time/60%60))
H=$((cur_time/60/60%24))

LOGFILE="$(printf "/home/root/temperature-%02d:%02d:%02d.txt" $H $M $S)"
#echo  $LOGFILE






COLOR_CHANGED=0
#==============================================================================
LOGIT()
{
    TST=`echo $1 | grep -i 'Freq:'`

    CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

    if [ "$TST" = "" ]
    then
        if [ $COLOR_CHANGED -eq 1 ]
        then
            COLOR_CHANGED=0
            printf "${WHITE}"  > /dev/tty0
            printf "${WHITE}"
        fi
    else
        if [ $CPU_TEMP -ge $CPU_ZONE_1 ]
        then
                COLOR_CHANGED=1
                printf "${RED}"    > /dev/tty0
                printf "${RED}"
        elif [ $CPU_TEMP -ge $CPU_ZONE_0 ]
        then
                COLOR_CHANGED=1
                printf "${YELLOW}"  > /dev/tty0
                printf "${YELLOW}"
        fi
    fi

    echo "$1" >> /dev/tty0
    echo "$1"
    echo "$1" >> $LOGFILE
}
#==============================================================================




















I2C_MIN=100000
I2C_MAX=0

get_i2c_temperature()
{
    I2C_ADDR="0-0048"
    I2Cbus=0
    EXE="cat /sys/class/i2c-dev/i2c-0/device/0-004a/hwmon/hwmon0/temp1_input"

    if [ $DEVICE_TYPE == "abt3000_5_0" ] ||  [ $DEVICE_TYPE == "abt3000_5_3" ]
    then
        I2C_ADDR="0-0049"
        I2Cbus=0
        EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
    fi

    if [ $DEVICE_TYPE == "mv3000_7_0" ]
    then
        I2C_ADDR="0-0049"
        I2Cbus=0
        EXE="cat /sys/class/i2c-dev/i2c-0/device/0-0049/hwmon/hwmon0/temp1_input"
    fi

    I2C_TEMP=`$EXE`


    if [ $I2C_TEMP -gt $I2C_MAX ]
    then
        let I2C_MAX=$I2C_TEMP

    fi

    if [ $I2C_TEMP -lt $I2C_MIN ]
    then
        let I2C_MIN=$I2C_TEMP

    fi



    if [ ${#I2C_MAX} -gt 5 ]
    then
        I2C_MAX_STR=`echo $I2C_MAX | cut -c1-3`
        I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c4`"
    else
        I2C_MAX_STR=`echo $I2C_MAX | cut -c1-2`
        I2C_MAX_STR="$I2C_MAX_STR.`echo $I2C_MAX | cut -c3`"
    fi

    if [ ${#I2C_MIN} -gt 5 ]
    then
        I2C_MIN_STR=`echo $I2C_MIN | cut -c1-3`
        I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c4`"
    else
        I2C_MIN_STR=`echo $I2C_MIN | cut -c1-2`
        I2C_MIN_STR="$I2C_MIN_STR.`echo $I2C_MIN | cut -c3`"
    fi


    if [ ${#I2C_TEMP} -gt 5 ]
    then
        I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
        I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
    else
        I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
        I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
    fi


}
#==============================================================================




CPU_MIN=100000
CPU_MAX=0
get_cpu_temperature()
{
    #------------------------------------------------
    CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
    #------------------------------------------------

    if [ $CPU_TEMP -gt $CPU_MAX ]
    then
        let CPU_MAX=$CPU_TEMP
    fi

    if [ $CPU_TEMP -lt $CPU_MIN ]
    then
        let CPU_MIN=$CPU_TEMP
    fi



    if [ ${#CPU_MAX} -gt 5 ]
    then
        CPU_MAX_STR=`echo $CPU_MAX | cut -c1-3`
        CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c4`"
    else
        CPU_MAX_STR=`echo $CPU_MAX | cut -c1-2`
        CPU_MAX_STR="$CPU_MAX_STR.`echo $CPU_MAX | cut -c3`"
    fi

    if [ ${#CPU_MIN} -gt 5 ]
    then
        CPU_MIN_STR=`echo $CPU_MIN | cut -c1-3`
        CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c4`"
    else
        CPU_MIN_STR=`echo $CPU_MIN | cut -c1-2`
        CPU_MIN_STR="$CPU_MIN_STR.`echo $CPU_MIN | cut -c3`"
    fi


    if [ ${#CPU_TEMP} -gt 5 ]
    then
        CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
        CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
    else
    #    CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
    #    CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"

    CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`

    fi




}



get_cpu_freq()
{
    FREQ=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq`
}



get_cpu_uid()
{
    SOC_UID=`cat /sys/devices/soc0/soc_uid`
    LOGIT "CPU UID             : $SOC_UID"
}


get_cpu_core()
{
    # echo 1 > /sys/devices/system/cpu/cpu0/online
    # echo 1 > /sys/devices/system/cpu/cpu1/online
    # echo 1 > /sys/devices/system/cpu/cpu2/online
    # echo 1 > /sys/devices/system/cpu/cpu3/online

    CPU0_OLNINE=`cat /sys/devices/system/cpu/cpu0/online`
    CPU1_OLNINE=`cat /sys/devices/system/cpu/cpu1/online`
    CPU2_OLNINE=`cat /sys/devices/system/cpu/cpu2/online`
    CPU3_OLNINE=`cat /sys/devices/system/cpu/cpu3/online`

    LOGIT "CPU 0 online        : $CPU0_OLNINE"
    LOGIT "CPU 1 online        : $CPU1_OLNINE"
    LOGIT "CPU 2 online        : $CPU2_OLNINE"
    LOGIT "CPU 3 online        : $CPU3_OLNINE"

}

get_cpu_available_frequencies()
{
    CPU_AVAILABLE_FREQUENCIES=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_frequencies`
    LOGIT "CPU frequencies     : $CPU_AVAILABLE_FREQUENCIES"
}

get_cpu_available_governors()
{
    CPU_AVAILABLE_GOVERNORS=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors`
    LOGIT "CPU GOVERNORS       : $CPU_AVAILABLE_GOVERNORS"
}

get_cpu_max_frequency()
{
    CPU_MAX_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq`
    LOGIT "CPU MAX frequencies : $CPU_MAX_FREQUENCY"
}


get_cpu_min_frequency()
{
    CPU_MIN_FREQUENCY=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq`
    LOGIT "CPU MIN frequencies : $CPU_MIN_FREQUENCY"
}

get_cpu_scaling_governor()
{
    CPU_SCALING_GOVERNOR=`cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor`
    LOGIT "CPU scaling governor: $CPU_SCALING_GOVERNOR"
}


get_cpu_temperature_limits()
{
    CPU_ZONE_0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp`
    CPU_ZONE_1=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp`
    LOGIT "CPU temp warning    : $CPU_ZONE_0"
    LOGIT "CPU temp halt sys   : $CPU_ZONE_1"
}



ELASPED=0
let ELAPSED+=1
TM=`date +%T`



LOGIT "==========================================================================================="
LOGIT "DATE     : `date`"
LOGIT "FILE     : $LOGFILE"
LOGIT "DEVICE   : $DEVICE_TYPE"
LOGIT "uname -a : `uname -a`"
LOGIT "uImage   : `ls -la /boot/Image`"

# LOGIT "==========================================================================================="
# LOGIT "Settings:"

# MODE="powersave"
# MODE="ondemand"
# echo $MODE > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
# echo $MODE > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor
# echo $MODE > /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor
# echo $MODE > /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor

#echo 1200000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
#echo 1200000 > /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq

# echo 1 > /sys/devices/system/cpu/cpu0/online
# echo 1 > /sys/devices/system/cpu/cpu1/online
# echo 1 > /sys/devices/system/cpu/cpu2/online
# echo 1 > /sys/devices/system/cpu/cpu3/online

LOGIT "==========================================================================================="
get_cpu_uid
get_cpu_core
get_cpu_available_frequencies
get_cpu_available_governors
get_cpu_scaling_governor
get_cpu_max_frequency
get_cpu_min_frequency
get_cpu_temperature_limits




T1=`cat /proc/uptime | cut -d ' ' -f 1`

while [ 1 ]
do

    T2=`cat /proc/uptime | cut -d ' ' -f 1`
    T3=`echo $T2 - $T1 | bc `


    #------------------------------------------------
    get_i2c_temperature
    get_cpu_temperature
    get_cpu_freq
    #------------------------------------------------


    elapsed=`echo $T3 | cut -d '.' -f1`

    S=$((elapsed%60))
    M=$((elapsed/60%60))
    H=$((elapsed/60/60%24))

    MS=`echo $T3 | cut -d '.' -f2`


    elapsed_hhmmss="$(printf "%02d:%02d:%02d:%s" $H $M $S $MS)"


    ET="$(printf "%07.2f" $T3)"
    #------------------------------------------------
    LOG="$ET: $elapsed_hhmmss  $ cpu:$CPU_TEMP_STR i2c:$I2C_TEMP_STR F:$FREQ"
    LOGIT "$LOG "

done



start_time="$(date -u +%s)"
T1=`cat /proc/uptime | cut -d ' ' -f 1`

while [ 1 ]
do

    TIME_STAMP=` date | cut -d ' ' -f4 `
    end_time="$(date -u +%s)"
    elapsed="$(($end_time-$start_time))"

    #------------------------------------------------
    get_i2c_temperature
    get_cpu_temperature
    get_cpu_freq
    #------------------------------------------------
    S=$((elapsed%60))
    M=$((elapsed/60%60))
    H=$((elapsed/60/60%24))
    MILLITIME=`cat /proc/uptime | cut -d '.' -f2 | cut -c 1-2`

    T2=`cat /proc/uptime | cut -d ' ' -f 1`
    T3=`echo $T2 - $T1 | bc `

    elapsed_hhmmss="$(printf "%02d:%02d:%02d:%s" $H $M $S $MILLITIME)"
    #------------------------------------------------
    LOG="$TIME_STAMP ($elapsed_hhmmss)  Temp:$CPU_TEMP_STR   Freq:$FREQ"
    LOG="($elapsed_hhmmss) cpu:$CPU_TEMP_STR Freq:$FREQ $I2C_TEMP_STR "
    LOG="$T3 $elapsed_hhmmss cpu:$CPU_TEMP_STR i2c:$I2C_TEMP_STR F:$FREQ"
    LOGIT "$LOG "

    usleep 100000
done

