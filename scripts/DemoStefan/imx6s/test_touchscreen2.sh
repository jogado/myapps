#!/bin/sh
#
# Test the touchscreeen
#
source /usr/bin/production-test/testscripts/clear_pids.inc
source /usr/bin/production-test/testscripts/common.inc
clear_production_test_tasks

#c_kill xterm
#c_kill Xorg
#sleep 3

#--------------------------------------------------------------------
TEST_DESC="Touchscreen test"
RESULT="501 NOT IMPLEMENTED"
TIMEOUT=30
#--------------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#--------------------------------------------------------------------
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi
#--------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#--------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#------------------------------------------------------------------------------

echo "DEVICE_TYPE = $DEVICE_TYPE"


if [ $DEVICE_NAME = "abt3000" ] || [ $DEVICE_NAME = "mg3000" ] || [ $DEVICE_NAME = "dmt2000" ] || [ $DEVICE_TYPE = "mv3000_6_0" ] || [ $DEVICE_TYPE = "mv3000_7_0" ]
then

	c_startX

	xterm 3 "---" "Press all buttons" $TIMEOUT 1 1
	CONFIRM=$?

	if [ $CONFIRM -eq 0 ]
	then
		RESULT="200 $TEST_DESC: success (operator)"
	fi

	if [ $CONFIRM -eq 1 ]
	then
		RESULT="590 $TEST_DESC: failed (operator)"
	fi

	if [ $CONFIRM -eq 255 ]
	then
		RESULT="590 $TEST_DESC: Timeout"
	fi

#	c_stopX

	result
fi






kill_Xorg()
{
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		kill $pid
	fi

	pid=`pidof X`
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

startX()
{
	echo 0 >/sys/class/graphics/fb0/blank
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg -nocursor`
	fi
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}


detect_touchscreen()
{
	i2cbus=0
	if [ -d '/sys/class/i2c-dev/i2c-1/device/1-0038' ]; then
		echo 'edt-ft5x06'
		i2cbus=1
	elif [ -d '/sys/class/i2c-dev/i2c-1/device/1-0041' ]; then
		echo 'ili210x'
		i2cbus=1
	elif [ -d '/sys/class/i2c-dev/i2c-0/device/0-0038' ]; then
		echo 'edt-ft5x06'
	elif [ -d '/sys/class/i2c-dev/i2c-0/device/0-0041' ]; then
		echo 'ili210x'
	else
		echo 'unknown'
	fi
	return ${i2cbus}
}

run_touchscreen_test()
{
	DISPLAY=:0 /usr/bin/touchscreen-test -c $1 -r $2
	if [ $? -eq 0 ] ; then
		RESULT="200 Touchscreen test succeeded"
	else
		if [ $3 -eq 4 ] ; then
			RESULT="200 Touchscreen test finished"
		else
			RESULT="520 Touchscreen test failed"
		fi
	fi
}

run_test()
{
	modprobe -a ili210x edt-ft5x06

	model=$(detect_touchscreen)
	i2cbus=$?

	startX
	case "$model" in
		edt-ft5x06)
			run_touchscreen_test 3 4 $1
			;;
		ili210x)
			echo 1 >/sys/class/i2c-dev/i2c-${i2cbus}/device/${i2cbus}-0041/calibrate
			sleep 5
			run_touchscreen_test 5 5 $1
			;;
		*)
			RESULT="500 No touchscreen found"
			;;
	esac
#Voluntary taken out to have X running for succeeding tests
#	stopX
}




#-----------------------------------------------------------------------------------------
run_test2()
{
	kill_Xorg
	xstart
	beep
	X -retro &
	#echo `pidof X` > $TMP_FOLDER/X.pid

	CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
	RESULT=`${CONFIRM} "Touchscreen test" $2 1 $TIMEOUT 99`
}
#-----------------------------------------------------------------------------------------





warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
else 

	if [ $DEVICE_NAME = "abt3000" ] ; then
		run_test2 $2
	else
		run_test $2
	fi
fi
result
