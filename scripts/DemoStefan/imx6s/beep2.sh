#!/bin/sh
#
# Beeper test
#
source /usr/bin/production-test/testscripts/clear_pids.inc
source /usr/bin/production-test/testscripts/common.inc
clear_production_test_tasks

TEST_DESC="Buzzer test"
RESULT="501 NOT IMPLEMENTED"
TIMEOUT=15


#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then                                      
   out="/dev/stdout"                                            
else                                                            
   out="$1"                                                     
fi 
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
clean_up()
{
	c_kill beep
}
trap clean_up SIGTERM SIGINT EXIT
#----------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#----------------------------------------------------------------

echo "DEVICE_NAME= $DEVICE_NAME"

#----------------------------------------------------------------
# Using touchscreen
#----------------------------------------------------------------
if [ $DEVICE_NAME = "abt3000" ] || [ $DEVICE_NAME = "dmt2000" ] ; then

	c_startX

	xterm 2 "Buzzer test" "Do you hear the beep" $TIMEOUT 2 1
	CONFIRM=$?
	if [ $CONFIRM -eq 0 ]
	then
		RESULT="200 $TEST_DESC: success (operator)"
	fi

	if [ $CONFIRM -eq 1 ]
	then
		RESULT="590 $TEST_DESC: failed (operator)"
	fi

	if [ $CONFIRM -eq 255 ]
	then
		RESULT="590 $TEST_DESC: Timeout"
	fi

	#c_stopX 
	result
fi

/usr/bin/production-test/testscripts/beep_task.sh $TIMEOUT &
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
RESULT=`${CONFIRM} "Beeper Test" $2 1 $TIMEOUT 99`
result

