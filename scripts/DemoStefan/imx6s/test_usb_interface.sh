#!/bin/sh
source /usr/bin/production-test/testscripts/common.inc
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks

TIMEOUT=30
RESULT="501 NOT IMPLEMENTED"
#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#------------------------------------------------------------------------------
clean_up()
{
	killall beep > /dev/null 2>&1
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#----------------------------------------------------------------


more_detail()
{
	MANUFACTURER=`lsusb -v -d $1 | grep -i 'iManufacturer'  | tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	PRODUCT=`     lsusb -v -d $1 | grep -i 'iProduct'       | tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	SERIAL=`      lsusb -v -d $1 | grep -i 'iSerial'        | tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	CLASS=`       lsusb -v -d $1 | grep -i 'bInterfaceClass'| tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	IDVENDOR=`    lsusb -v -d $1 | grep -i 'idVendor'       | tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	IDPRODUCT=`   lsusb -v -d $1 | grep -i 'idProduct'      | tail -1 | tr -s ' ' | cut -d ' ' -f 4-20 `
	echo "DEVICE: $1"
	echo "   idVendor        : $IDVENDOR"
	echo "   idProduct       : $IDPRODUCT"
	echo "   iManufacturer   : $MANUFACTURER"
	echo "   iProduct        : $PRODUCT"
	echo "   bInterfaceClass : $CLASS"
	echo "   iSerial         : $SERIAL"
}

test_usb()
{
	EXIST=`lsusb | grep -i "$1" `

	if [ ! -n "${EXIST}" ]
	then
		RESULT="500 Error: USB device <$1> not present"
		result
	fi
}




if   [ $DEVICE_TYPE = "mv3000_5_0" ]  
then

# Bus 001 Device 004: ID 196d:0201
# Bus 001 Device 009: ID 1fc9:012a NXP Semiconductors
# Bus 001 Device 003: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
# Bus 001 Device 002: ID 0424:2514 Standard Microsystems Corp. USB 2.0 Hub
# Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub

	test_usb "196d:0201"
	test_usb "1fc9:012a"
	test_usb "0424:2514"
	test_usb "1d6b:0002"

	RESULT="200 All USB ID's are present"
	result
fi





result


