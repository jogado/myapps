#!/bin/sh
source /usr/bin/production-test/testscripts/common.inc
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks

TIMEOUT=30
RESULT="501 NOT IMPLEMENTED"
#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#------------------------------------------------------------------------------
clean_up()
{
	c_kill beep
	killall barcode
	killall xterm
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
restart_xorg()
{
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		# kil
		return
	fi
	Xorg -nocursor &
	sleep 1
}

startX()
{
        export DISPLAY=:0.0

	if [ $DEVICE_NAME = "abt3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "pr3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mg3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mv3000" ] ; then
		restart_xorg
	else
	        /usr/bin/production-test/testscripts/start_xserver.sh 0
	fi

        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
		echo "Xorg PID = $pid"
        fi

        echo 0 >/sys/class/graphics/fb0/blank
}

stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}
#------------------------------------------------------------------------------









#------------------------------------------------------------------------------
RESULT_FILE="/tmp/moti.tmp"


COUNT=1

execute()
{
	sh /usr/bin/production-test/testscripts/$2.sh /tmp/$2.result 30 $3 $4
	#cat /tmp/$1.result 
		
	DATA=$(cat /tmp/$2.result | grep '200' )

	if [ ! -n "${DATA}" ]
	then
		printf "%03d %-20.20s  ERRORS\n" $COUNT "$1" >> $RESULT_FILE

	else
		printf "%03d %-20.20s  OK\n" $COUNT "$1" >> $RESULT_FILE
	fi
	let COUNT=COUNT+1
}

execute2()
{
	sh /usr/bin/production-test/testscripts/$1.sh /tmp/$1.result 30 $2 $3
	#cat /tmp/$1.result 
		
	DATA=$(cat /tmp/$1.result | grep '200' )


	if [ ! -n "${DATA}" ]
	then
		printf "%03d %-25.25s...ERRORS\n" $COUNT "$1 $2" >> $RESULT_FILE

	else
		DATA2=`echo $DATA | cut -d ' ' -f 2-20`
		printf "%03d %-25.25s...OK\n" $COUNT "${DATA2}" >> $RESULT_FILE
	fi
	let COUNT=COUNT+1
}



#------------------------------------------------------------------------------
startX

rm  $RESULT_FILE

Diag_1()
{
	execute "Device Name" 	 get_device_name_ci
	execute "Device S/N"   	 get_device_sn_ci
	execute "Kernel version" get_version 1
	execute "Mac address"	 ethernet 2
	execute "Test count i2c" tst_i2c
	execute "Test count usb" usb 6
	execute "Test DOM"       get_usbdom_ci
	execute "Dallas count"   get_pcb_enum_ci 1
	execute "Barcode S/N"    get_barcode_ci
	execute "Test SAM's"     get_sam_ci 1
	execute "Test RTC"       rtc i2c
	execute "Temperature"    temperature i2c
	execute "Dallas IP"      get_dallas_ip
	execute "DPC TRIM"       test_emv3000_dpc_trim
	execute "USB Devices"    test_usb_interface
}

Diag_2()
{
	execute2 get_device_name_ci
	execute2 get_device_sn_ci
	execute2 get_version 1
	execute2 ethernet 2
	execute2 tst_i2c
	execute2 usb 6
	execute2 get_usbdom_ci
	execute2 get_pcb_enum_ci 1
	execute2 get_barcode_ci
	execute2 get_sam_ci 1
	execute2 rtc i2c
	execute2 temperature i2c
	execute2 get_dallas_ip
	execute2 test_emv3000_dpc_trim
	execute2 test_usb_interface
}


if [ "$1" == "1" ]
then
	Diag_1
fi

if [ "$1" == "2" ]
then
	Diag_2
fi



Body=`cat $RESULT_FILE`
cat /tmp/moti.tmp
xterm 6 "System Info" "$Body" 10 1 0 




