#!/bin/sh

#==============================================================================
# Test card reader
#==============================================================================
source /usr/bin/production-test/testscripts/common.inc
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks


RESULT="501 NOT IMPLEMENTED"
TIMEOUT=30
EXE=/usr/bin/moti 

#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then
	out="/dev/stdout"
else
	out="$1"
fi 
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
#----------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#------------------------------------------------------------------------------
clean_up()
{
	c_kill moti
	c_rm moti.tmp
	c_kill beep
	c_kill xterm
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#------------------------------------------------------------------------------








#------------------------------------------------------------------------------
restart_xorg()
{
	sleep 1
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		return
	fi
	Xorg -nocursor &
	sleep 1
}
#------------------------------------------------------------------------------
startX()
{
        export DISPLAY=:0.0

	if [ $DEVICE_NAME = "abt3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "pr3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mg3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mv3000" ] ; then
		restart_xorg
	else
	        /usr/bin/production-test/testscripts/start_xserver.sh 0
	fi

        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
		echo "Xorg PID = $pid"
        fi

        echo 0 >/sys/class/graphics/fb0/blank
}
#------------------------------------------------------------------------------
stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}
#------------------------------------------------------------------------------
kill_xterm()
{
	pid=`pidof xterm`
	if [ "$pid" != "" ]; then
		kill xterm
		echo "Killing xterm"
	fi
}
#------------------------------------------------------------------------------



run_test()
{
	#kill_xterm

	start_time="$(date -u +%s)"

	echo 0 > /sys/class/graphics/fbcon/cursor_blink

	c_kill moti
	echo "" > /tmp/moti.dat
	rm /tmp/moti.tmp
	moti -out /tmp/moti.dat &

	if [ $DEVICE_NAME = "abt3000" ] ; then
		xterm 5 "Card reader test" "Present card" $TIMEOUT 1 0 &
	elif [ $DEVICE_NAME = "pr3000" ] ; then
		xterm 5 "Card reader test" "Present card (5 times)" $TIMEOUT 1 0 &
	elif [ $DEVICE_NAME = "mg3000" ] ; then
		xterm 5 "Card reader test" "Present card (5 times)" $TIMEOUT 1 0 &
	elif [ $DEVICE_NAME = "mv3000" ] ; then
		xterm 5 "Card reader test" "Present card" $TIMEOUT 1 0 &
	else
		DISPLAY=:0.0;/usr/bin/screen-test -m "Present card " -b 0000ff &
	fi

	sleep 1

	while [ 1 ]
	do
		if [ -f /tmp/moti.dat ]
		then	
			DATA=`cat /tmp/moti.dat`
			FILESIZE="${#DATA}" 
			if [ $FILESIZE -gt 5 ];
			then
				cp /tmp/moti.dat /tmp/moti.tmp
				beep
				sleep 2
				RESULT="200 Test : $INFO "
				result
			fi
		fi

		end_time="$(date -u +%s)"
		elapsed="$(($end_time-$start_time))"

		if [ $elapsed -ge $TIMEOUT ]
		then
			RESULT="500 TIMEOUT"
			result
		fi
		usleep 300000
	done
}

c_startX

run_test

result


