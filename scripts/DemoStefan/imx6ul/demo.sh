#!/bin/sh

#Stop proxima application
systemctl stop proxima-framework.target
sleep 3

#Enable barcode
echo  0 > /sys/class/leds/barcode_wake/brightness


echo 0 > /sys/class/graphics/fbcon/cursor_blink



execute()
{
	./$1 /dev/stdout 30
}


while [ 1 ]
do
		
	execute status2.sh 
	execute beep2.sh 
	execute test_touchscreen2.sh 
	execute test_emv3000_leds2.sh
	execute test_emv3000_reader2.sh
	execute tst_barcode2.sh
done




