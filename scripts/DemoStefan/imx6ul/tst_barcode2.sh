#!/bin/sh
source /usr/bin/production-test/testscripts/common.inc
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks

TIMEOUT=30
RESULT="501 NOT IMPLEMENTED"
#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#------------------------------------------------------------------------------
clean_up()
{
	c_kill beep
	killall barcode
	killall xterm
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#----------------------------------------------------------------





#------------------------------------------------------------------------------
restart_xorg()
{
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		# kil
		return
	fi
	Xorg -nocursor &
	sleep 1
}

startX()
{
        export DISPLAY=:0.0

	if [ $DEVICE_NAME = "abt3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "pr3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mg3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mv3000" ] ; then
		restart_xorg
	else
	        /usr/bin/production-test/testscripts/start_xserver.sh 0
	fi

        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
		echo "Xorg PID = $pid"
        fi

        echo 0 >/sys/class/graphics/fb0/blank
}

stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}
#------------------------------------------------------------------------------


startX

BOARD_INFO_FILE=/tmp/rakinda.data

echo "" > $BOARD_INFO_FILE


if   [ $DEVICE_TYPE = "abt3000_5_0" ]  || [ $DEVICE_TYPE = "abt3000_5_1" ] || [ $DEVICE_TYPE = "abt3000_5_2" ]
then
	stty -F /dev/barcode 115200 -echo -raw
	cat /dev/barcode > $BOARD_INFO_FILE &

elif [ $DEVICE_TYPE = "abt3000_1_0" ] ; then
	#stty -F /dev/barcode 115200 -echo -raw
	#cat /dev/barcode > $BOARD_INFO_FILE &
	echo "." > /tmp/moti.tmp
	barcode -output /tmp/moti.tmp &

elif [ $DEVICE_NAME = "abt3000" ] ; then
	stty -F /dev/ttymxc4 9600 -echo -raw
	cat /dev/ttymxc4 > $BOARD_INFO_FILE &
elif [ $DEVICE_NAME = "pr3000" ] ; then
	stty -F /dev/ttymxc4 9600 -echo -raw
	cat /dev/ttymxc4 > $BOARD_INFO_FILE &
else
	barcode &
fi





if [ $DEVICE_NAME = "abt3000" ] ; then
	#xterm 1 "Barcode test" "Please present barcode" $TIMEOUT 1 1 &
	xterm 5 "Barcode test" "Present card (3 times)" $TIMEOUT 1 0 &

	pid_xterm=`pidof xterm`
	echo "================="
	echo "xterm PID = $pid_xterm"
	echo "================="

elif [ $DEVICE_NAME = "pr3000" ] ; then
	xterm 1 "Barcode test" "Please present barcode" $TIMEOUT 0 1 &
elif [ $DEVICE_NAME = "mv3000" ] ; then
	xterm 1 "Barcode test" "Please present barcode" $TIMEOUT 1 0 &
else
	DISPLAY=:0 /usr/bin/screen-test -m "Scan barcode" -b 0000ff -f 300 &
fi




start_time="$(date -u +%s)"

COUNT=0

while true
do

	if [ -f /tmp/moti.tmp ]
	then	
		DATA=`cat /tmp/moti.tmp`
		FILESIZE="${#DATA}" 
		if [ $FILESIZE -gt 5 ];
		then
			beep
			let COUNT=COUNT+1
			echo "." > /tmp/moti.tmp

			if [ $COUNT -ge 3 ]
			then
				echo "Barcode test OK" > /tmp/moti.tmp
				RESULT="200 Barcode OK" 
				sleep 1
				result
			fi
		fi
	fi

	end_time="$(date -u +%s)"
	elapsed="$(($end_time-$start_time))"

	if [ $elapsed -ge $TIMEOUT ]
	then
		RESULT="500 TIMEOUT" 
		result
	fi
	usleep 300000
done




