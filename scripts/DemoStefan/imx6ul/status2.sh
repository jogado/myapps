#!/bin/sh
source /usr/bin/production-test/testscripts/common.inc
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks

TIMEOUT=30
RESULT="501 NOT IMPLEMENTED"
#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
TMP_FOLDER="/var/run/production-test"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#------------------------------------------------------------------------------
clean_up()
{
	c_kill beep
	killall barcode
	killall xterm
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------
result()
{
	echo "${RESULT}" > $out
	exit 0
}
#----------------------------------------------------------------





#------------------------------------------------------------------------------
restart_xorg()
{
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		# kil
		return
	fi
	Xorg -nocursor &
	sleep 1
}

startX()
{
        export DISPLAY=:0.0

	if [ $DEVICE_NAME = "abt3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "pr3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mg3000" ] ; then
		restart_xorg
	elif [ $DEVICE_NAME = "mv3000" ] ; then
		restart_xorg
	else
	        /usr/bin/production-test/testscripts/start_xserver.sh 0
	fi

        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
		echo "Xorg PID = $pid"
        fi

        echo 0 >/sys/class/graphics/fb0/blank
}

stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}
#------------------------------------------------------------------------------





#------------------------------------------------------------------------------
RESULT_FILE="/tmp/moti.tmp"
echo "rtc .......... OK" 	>  $RESULT_FILE
echo "i2c .......... OK" 	>> $RESULT_FILE
echo "Sam ......... .OK"	>> $RESULT_FILE
echo "Eeprom ....... OK" 	>> $RESULT_FILE
echo "Dallas ....... OK" 	>> $RESULT_FILE
echo "Network ...... OK" 	>> $RESULT_FILE
echo "Modem ........ OK" 	>> $RESULT_FILE
#------------------------------------------------------------------------------

startX

Body=`cat $RESULT_FILE`

xterm 6 "System Info" $Body 5 0 0 


