#!/bin/sh
#
# Test emv3000 leds
#
source /usr/bin/production-test/testscripts/clear_pids.inc
source /usr/bin/production-test/testscripts/common.inc
clear_production_test_tasks


#----------------------------------------------------------------
TEST_DESC="Test EMV Leds"
RESULT="501 NOT IMPLEMENTED"
TIMEOUT=30
#----------------------------------------------------------------
if [ "$#" -lt "1" ] ; then                                      
   out="/dev/stdout"                                            
else                                                            
   out="$1"                                                     
fi 
#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
#----------------------------------------------------------------
result()
{
        echo "${RESULT}" > $out
        exit 0
}
#----------------------------------------------------------------


#----------------------------------------------------------------
# Check if EMV3000 device is present
#----------------------------------------------------------------
if [ ! -L /dev/emv3000 ]
then
	RESULT="500 No device found"
	result
fi
#----------------------------------------------------------------



echo 0 >/sys/class/graphics/fb0/blank
echo 0 > /sys/class/graphics/fbcon/cursor_blink


if [ $DEVICE_NAME = "abt3000" ] ; then

	/usr/bin/production-test/testscripts/test_emv3000_leds_task.sh $TIMEOUT &

	c_startX

	xterm 2 "Test EMV Leds" "Are EMV leds blinking ?" $TIMEOUT 1 0
	CONFIRM=$?

	if [ $CONFIRM -eq 0 ]
	then
		RESULT="200 $TEST_DESC: success (operator)"
	fi

	if [ $CONFIRM -eq 1 ]
	then
		RESULT="590 $TEST_DESC: failed (operator)"
	fi

	if [ $CONFIRM -eq 255 ]
	then
		RESULT="590 $TEST_DESC: Timeout"
	fi

	result
fi

/usr/bin/production-test/testscripts/test_emv3000_leds_task.sh $TIMEOUT &
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
RESULT=`${CONFIRM} "Test emv3000 leds" $2 1 $TIMEOUT 99`
result

