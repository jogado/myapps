#!/bin/sh

#--------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#--------------------------------------------------------------------


cmd_prsp=`cat /usr/bin/production-test-$DEVICE_TYPE/snmp/210_get_info.sh | grep -i "INFO=" | cut -d '"' -f 2`
cmd_kernel=`uname -a`
cmd_device_sn=`dallas-esc | grep "Product:" | cut -d ':' -f 2`
cmd_board_sn=`dallas-esc | grep "Board:" | cut -d ':' -f 2`
cmd_board_type=`dallas-esc | grep "Board:" | cut -c17-19,25-30`
cmd_production_test=`opkg list |grep -i 'production-tests -' | cut -d '-' -f 4`

cmd_liboti=`test-liboti -info | grep '( 0)' | cut -d ':' -f 2-5 | sed 's/ OTI /OTI/g'`

echo "Sysinfo           : v0001"
echo "Device Type       : $DEVICE_TYPE"
echo "Device Number     : $cmd_device_sn"
echo "Board type        : $cmd_board_type"
echo "Board S/N         : $cmd_board_sn"
echo "Software Package  : $cmd_prsp"
echo "kernel Version    : $cmd_kernel"
echo "Production test   : $cmd_production_test"
echo "Liboti            : $cmd_liboti"
