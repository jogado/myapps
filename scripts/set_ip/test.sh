#!/bin/sh


#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------

OUT="/dev/ttymxc0"


if [ $DEVICE_NAME = "abt3000" ] 
then
    OUT="/dev/ttymxc0"
fi

if [ $DEVICE_NAME = "mv3000" ] || [ $DEVICE_NAME = "dmt2000" ] 
then
    OUT="/dev/ttymxc1"
fi


ShowIP()
{

	IP=`ifconfig | grep -i bcast |cut -d ':' -f2 | cut -d ' ' -f0`
	MASK=`ifconfig | grep -i bcast |cut -d ':' -f4`
	GWS=$(ip route | grep default | cut -f3 -d ' ')

    echo ""                     >  $OUT
    echo "IpAddress: <$IP>"     >  $OUT
    echo "Mask     : <$MASK>"   >  $OUT
    echo "Gateway  : <$GWS>"    >  $OUT
    echo ""                     >  $OUT

    TM=`date +%T`
    echo "$TM IpAddress: <$IP>"  >>  /test/testip.txt
    sync
}


ShowIP 
ping -c 5 8.8.8.8 > $OUT


