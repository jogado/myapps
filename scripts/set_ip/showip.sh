#!/bin/sh


#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------

#OUT="/dev/stdout"
OUT="/dev/tty0"

ShowIP()
{

    IP=`ifconfig | grep -i bcast |cut -d ':' -f2 | cut -d ' ' -f0`
    MASK=`ifconfig | grep -i bcast |cut -d ':' -f4`
    GWS=$(ip route | grep default | cut -f3 -d ' ')

    echo -n -e "\n\n" 		         >  $OUT
    echo -n -e "IpAddress:<$IP>\n"   >  $OUT
    echo -n -e "Mask     :<$MASK>\n" >  $OUT
    echo -n -e "Gateway  :<$GWS>\n"  >  $OUT

    echo ""  
    echo "IpAddress:<$IP>"  
    echo "Mask     :<$MASK>"
    echo "Gateway  :<$GWS>" 

    if [ -f "/test/test.sh" ]
    then
       sh /test/test.sh &
    fi

}

ShowFirmwareVersion()
{
	#INFO="916-10948-03.xlsm (760-11031-03)"
	DATA=`cat  /usr/bin/production-test-$DEVICE_TYPE/snmp/210_get_info.sh | grep -i "INFO="`
	PRSP=`echo  $DATA |cut -d '(' -f2 | cut -c1-12`

    echo -n -e "\n" 		               >  $OUT
	echo "Firmware : $DEVICE_TYPE.$PRSP"   > $OUT
	echo "Firmware : $DEVICE_TYPE.$PRSP"
}
sleep 10
#beep

ShowIP 
ShowFirmwareVersion

sleep 10
printf "\033[2J\033[9;0]\033[0;0Hlogin :" > $OUT

