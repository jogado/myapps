#!/bin/sh


#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------

OUT="/dev/ttymxc0"

if [ $DEVICE_NAME = "abt3000" ] 
then
    OUT="/dev/ttymxc0"
fi

if [ $DEVICE_NAME = "mv3000" ] 
then
    OUT="/dev/ttymxc1"
fi


SetDefaultIP()
{ 
    IP="192.168.1.100" 
    NETMASK="255.255.252.0"
    GWS="192.168.1.1"


    echo ""                         > $OUT
    echo "SetDefaultIP()"           > $OUT
    echo "    IP=$IP"               > $OUT
    echo "    NETWMASK=$NETMASK"    > $OUT
    echo "    GWS=$GWS"             > $OUT

    udhcpc
    sleep 1
    ifconfig eth0 $IP netmask $NETMASK
    route add default gw $GWS eth0
    sleep 1
	/usr/bin/showip.sh &
    exit 0
}


SetDallasIP()
{
    #---------------------------------------------------------------
    IP=$(esc-read-dallas-ip |tr -d '\r' 2>/dev/null)
    if [ ! $? -eq 0 ]; then
        echo "Error reading dallas-ip"  > $OUT
        SetDefaultIP   
    fi

    len=${#IP}
    if [ $len -le 7 ] || [ $len -gt 15 ]
    then
        echo "Invalid size[$len]:<$IP>" > $OUT
        SetDefaultIP   
    fi
    #---------------------------------------------------------------

    #---------------------------------------------------------------
    NETMASK=$(esc-read-dallas-ip -mask |tr -d '\r'  2>/dev/null)
    if [ ! $? -eq 0 ]; then
        echo "Error reading dallas-netmask"  > $OUT
        NETMASK="255.255.252.0"
    fi
    #---------------------------------------------------------------

    #---------------------------------------------------------------
    GWS=$(ip route | grep default | cut -f3 -d ' ')
    if [ ! $? -eq 0 ]; then
        GWS="192.168.1.1"
    fi

    len=${#GWS}
    if [ $len -le 7 ]; then
        GWS="192.168.1.1"
    fi

    if [ $len -gt 15 ]; then
        GWS="192.168.1.1"
    fi

    #---------------------------------------------------------------

    echo "SetDallasIP()"    > $OUT
    echo "    IP=$IP"               > $OUT
    echo "    NETWMASK=$NETMASK"    > $OUT
    echo "    GWS=$GWS"             > $OUT

    udhcpc
    sleep 1
    ifconfig eth0 $IP netmask $NETMASK
    route add default gw $GWS eth0
    sleep 1
	/usr/bin/showip.sh &
	exit
}


SetDallasIP

/usr/bin/showip.sh &
echo "Setting IP failure"





