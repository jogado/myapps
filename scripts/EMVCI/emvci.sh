#!/bin/sh



declare -a StringArray=(
    "   -sn                 : Get serial number"
    "   -v                  : Get firmware version"
    "   -do_dpc_trim        : Do dpc trim"
    "   -get_dpc_trim       : Get DPC trim"
    "   -get_bat_level      : Get battery level"
    "   -l                  : Get Light Sensor Value"
    "    -get            00 : Protocol version"
    "    -get            20 : cpu id"
    "    -get            21 : Silicon SN"
    "    -get            22 : Main Board SN"
    "    -get            23 : Antenna SN"
    "    -get            24 : Product SN"
    "    -get            25 : Light Sensor type"
    "    -get            26 : SAM info"
    "    -get            28 : Firmware version"
    "    -get            30 : SAM type"
    "    -get            31 : SAM Serial"
    "    -get            32 : SAM Device Id"
    "    -get            33 : SAM Operator Id"
    "    -get            40 : Firmware version SBL & SFW"
    "    -gdate             : Get Date"
    "    -kcount            : Get list (kernel_number,kernel_id)"
)



# Iterate the string array using for loop
for val in "${StringArray[@]}"; do


    DESC=`echo $val | cut -d ':' -f2 `
    CMD=`echo $val | cut -d ':' -f1 `
    RES=`emvci $CMD | tr -d '\n\r'`

    echo "--------------------------------------------------------------------------"
    echo "DESC :$DESC"
    echo "CMD  : emvci $CMD "
    echo "RES  : $RES"


    if [ "$CMD" == "-kcount " ]
    then
        Count=`echo $RES| cut -d ':' -f2 `
        echo "       kernel count : $Count"

        for i in `seq 1 $Count`;
        do
            echo "          ..............."
            index=i*2+1
            index=`expr $i + 2`

            Name=`echo $RES| cut -d ':' -f $index`
            Id=`echo $Name| cut -d ',' -f 2 | tr -d '\n\r'`
            echo "          Kernel=$Name"
            echo "          Id    =$Id"

            kts=`emvci -kts $Id| tr -d '\n\r'`
            kinfo=`emvci -kinfo $Id| tr -d '\n\r'`
            echo "          kts   =$kts"
            echo "          kinfo =$kinfo"

            usleep 300000
        done
    fi
    usleep 300000
done



declare -a StringArray2=(
    "   -ctrl_on            : Get   emv3000 control"
    "   -o  -LED1           : LED 1 On"
    "   -o  -LED2           : LED 2 On"
    "   -o  -LED3           : LED 3 On"
    "   -o  -LED4           : LED 4 On"
    "   -o  -BUZ            : BUZZER on"
    "   -o                  : ALL Off"
    "   -ctrl_off           : Clear emv3000 control"
)


# OPERATOR CHECK
for val in "${StringArray2[@]}"; do


    DESC=`echo $val | cut -d ':' -f2 `
    CMD=`echo $val | cut -d ':' -f1 `
    RES=`emvci $CMD | tr -d '\n\r'`

    echo "--------------------------------------------------------------------------"
    echo "DESC :$DESC"
    echo "CMD  : emvci $CMD "
    echo "RES  : $RES"

    sleep 1
done

























ORIGINAL_MAIN_BOARD_SN=`emvci -get 22 | cut -d':' -f2 | tr -d '\n\r'`
ORIGINAL_ANTENNA_SN=`   emvci -get 23 | cut -d':' -f2 | tr -d '\n\r'`
ORIGINAL_PRODUCT_SN=`   emvci -get 24 | cut -d':' -f2 | tr -d '\n\r'`




declare -a StringArray3=(
    "    -b $ORIGINAL_MAIN_BOARD_SN     : Write Main Board SN"
    "    -a $ORIGINAL_ANTENNA_SN        : Write Antenna SN"
    "    -p $ORIGINAL_PRODUCT_SN        : Write Product SN"
    "    -get            22             : Read Main Board SN"
    "    -get            23             : Read Antenna SN"
    "    -get            24             : Read Product SN"
)


write_test()
{
    # Iterate the string array using for loop
    for val in "${StringArray3[@]}"; do
        DESC=`echo $val | cut -d ':' -f2 `
        CMD=`echo $val | cut -d ':' -f1 `
        RES=`emvci $CMD | tr -d '\n\r'`

        echo "--------------------------------------------------------------------------"
        echo "DESC :$DESC"
        echo "CMD  : emvci $CMD "
        echo "RES  : $RES"

        sleep 1
    done
}


if [ "$ORIGINAL_MAIN_BOARD_SN" == "" ] || [ "$ORIGINAL_MAIN_BOARD_SN" == "" ] || [ "$ORIGINAL_MAIN_BOARD_SN" == "" ]
then
    "ORIGINAL_MAIN_BOARD_SN = <$ORIGINAL_MAIN_BOARD_SN>"
    "ORIGINAL_ANTENNA_SN    = <$ORIGINAL_ANTENNA_SN>"
    "ORIGINAL_PRODUCT_SN    = <$ORIGINAL_PRODUCT_SN>"
    echo "Skipping write test "
else
    write_test
fi











echo "--------------------------------------------------------------------------"

