#!/bin/sh

FILES=*/

for f in $FILES
do
	echo ""
	echo /home/ubuntu/GITS/$f
	cd /home/ubuntu/GITS/$f
	git config --get remote.origin.url
	git branch
	cd /home/ubuntu/GITS
done
