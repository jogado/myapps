#!/bin/sh

USB_LARA_R6001="05c6:908b"
USB_LARA_R211="1546:110a"
USB_SARA_U201="1546:1102"


DEBUG="/dev/kmsg"
LOGTILE="/home/root/test.log"
CNT_FILE="/home/root/test.cnt"

COUNTER=100





#==========================================================================
mysleep()
{

    for i in `seq 1 $1`
    do
        echo "modem-test: Sleeping : $i sec"
        sleep 1
    done
}
mysleep 5
#==========================================================================








#==========================================================================
if [ ! -f $CNT_FILE ]
then
    echo "1" >  $CNT_FILE
    echo "================================================================================================" > $LOGTILE
    sync
fi

CURRENT_LOOP=$((`cat $CNT_FILE`))

if [ $CURRENT_LOOP -gt $COUNTER ]
then
    echo "current loop greater than $COUNTER"
    exit
else
echo "modem-test: loop: $CURRENT_LOOP / $COUNTER" > $DEBUG
    let CURRENT_LOOP=CURRENT_LOOP+1
    echo "$CURRENT_LOOP" > $CNT_FILE
fi

echo "modem-test: date : `date`" > $DEBUG







MODEM_R6001=`lsusb |grep $USB_LARA_R6001`
CMD_ATE1=`atcom -c ATE1 -d /dev/modem -s ';'| cut -d ';' -f 2`
CMD_ATI=`atcom -c ATI -d /dev/modem -s ';'| cut -d ';' -f 2`
CMD_CGN=`atcom -c AT+GSN -d /dev/modem -s ';'| cut -d ';' -f 2`





echo "modem-test: usb  : $MODEM_R6001" > $DEBUG
echo "modem-test: ATE1 : $CMD_ATE1" > $DEBUG
echo "modem-test: ATI  : $CMD_ATI" > $DEBUG
echo "modem-test: GSN  : $CMD_CGN" > $DEBUG



dmesg | grep -i "modem-" >> ./$myname.log >> $LOGTILE
    echo "================================================================================================" >> $LOGTILE
sync


if [ -f /home/root/modem_test_1.sh ]
then
    echo 1 > /proc/sys/kernel/sysrq
    echo b > /proc/sysrq-trigger
echo "REBBOTING"
fi

