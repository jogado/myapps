#!/bin/sh


OUT="/home/root/modem.log"

echo "" >> $OUT
echo "" >> $OUT
echo "" >> $OUT
echo "" >> $OUT
echo "" >> $OUT
echo "" >> $OUT

date >> $OUT

dmesg |grep -i "now attached to" |grep -i " GSM modem"     >> $OUT
echo ""       >> $OUT
ls -la /dev/modem    >> $OUT

RES=`atcom -d /dev/modem -c "ate1" -s ':' | tr -d '\r' `
echo "RES=<$RES>" >> $OUT

OK=`echo $RES |grep -i ":OK:" `
echo "OK=<$OK>" >> $OUT

if [ "$OK" == "" ]
then
        beep ; sleep 1 ;
        beep ; sleep 1 ;
        beep ; sleep 1 ;
fi


tail -n 10 $OUT > /dev/kmsg
tail -n 10 $OUT > /dev/stderr
tail -n 10 $OUT > /dev/tty0

sync


CMDLINE=`fw_printenv | grep -i autoreboot=true`

if [ "$CMDLINE" == "autoreboot=true" ]
then
        sleep 10
        sync
        reboot
fi

