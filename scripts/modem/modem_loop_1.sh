#!/bin/sh

#----------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
#----------------------------------------------------------------
c_kill()
{
	pid=`pidof $1`
	if [ "$pid" != "" ]; then
		kill $pid 2>&1
	fi
}
#----------------------------------------------------------------
clean_up()
{
	c_kill barcode
	ifconfig eth0 up

	#echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT SIGKILL EXIT 
#----------------------------------------------------------------








#----------------------------------------------------------------
PINNR="0000"
IP=""
CNT=0
init_mv3000_SIM_7600()
{

	#-------------------------------------------------------------
	if [ ! -e /dev/cdc-wdm0 ] ; then 
		echo "/dev/cdc-wdm0 do not exist => exit()"
		exit -1
	else
		echo "001 : /dev/cdc-wdm0 is present : OK"
	fi
	#-------------------------------------------------------------
	RES=`atcom  -d /dev/modem -c at+cpin? -s ':'`

	CHECK=`echo $RES |grep -i "SIM PIN"`
	if [ -n "${CHECK}" ]
	then
		echo "002 : pin needed :$RES"
		RES=`atcom  -s ':' -d /dev/modem -c at+cpin=\"$PINNR\"`
		echo "    => $RES"
	else
		echo "002 : pin not needed"
	fi
	#-------------------------------------------------------------
	RES=`atcom  -d /dev/modem -c at+cpin? -s ':'`
	CHECK=`echo $RES |grep -i "READY"`
	if [ -n "${CHECK}" ]
	then
		echo "003 : cpin is READY"
	fi
	#-------------------------------------------------------------
	RES=`qmi-network /dev/cdc-wdm0 status |grep -i Status:`

	CHECK=`echo $RES |grep -i "Status: disconnected"`
	if [ -n "${CHECK}" ]
	then
		echo "004 : cdc-wdm0 : $CHECK"
		echo "004a: cdc-wdm0 start"

		while [ 1 ]
		do 
			STATUS=`qmi-network /dev/cdc-wdm0 start |grep -i successfully`
			if [ -n "${CHECK}" ]
			then
				break
			fi

			qmi-network /dev/cdc-wdm0 stoplet CNT_A_ALL=CNT_A_ALL+1
			sleep 1

			let CNT=CNT+1
			if [ $CNT -ge 3 ]
			then
				echo "Start qmi-network failure"
				exit
			fi
		done
	fi

	udhcpc -q -f -i wwan0
	sleep 2

	IP_RAW=`qmicli --device=/dev/cdc-wdm0 --wds-get-current-settings |grep -i ' IPv4 address:' `
	echo "IP_RAW=$IP_RAW"

	IP=`echo $IP_RAW | cut -d ':' -f 2 |xargs`
	FILESIZE="${#DATA}" 

	if [ $FILESIZE -lt 5 ];
	then
		echo "Invalid IP detected"
		exit
	fi

	ifconfig eth0 down

}





#------------------------------------------------------------------------------



reset_modem()
{
	# Software reset
	# atcom -d /dev/modem -c "AT+CFUN=1,1"

	echo 1 > /sys/class/leds/gsm_pwr/brightness
	usleep 100000
	echo 0 > /sys/class/leds/gsm_pwr/brightness
}
























#================================
# START
#================================


#=====================================================
# Chech if network connection, otherwise start GPRS
#=====================================================

init_mv3000_SIM_7600

echo "IP=<$IP>"

barcode -beep &


SERVER_IP="194.78.128.98"
FILENAME=""
ELAPSED=0;
while [ 1 ]
do

	#  atcom -c ati -d /dev/modem -s ':' >> /tmp/modem.log
	# DATA=`atcom -c at+csq -d /dev/modem -s ':'  `
        # usleep 1000

		
	# RES=`ls /dev |grep -i modem_option`	
	# echo $RES

	# if [ ! -n "${RES}" ]
	# then
	# 	echo "$ELAPSED: No modem -no reset"
	# else
	# 	sleep 10
	# 	DATA=`atcom -c at+csq -d /dev/modem -s ':'  `
	# 	echo $DATA
	# 	reset_modem
	# 	echo "$ELAPSED: Resetting modem"
	# fi

	

	#--------------------------------------------------------------
	MYDATE=`date +%T`
	H=`echo $MYDATE | cut  -f 1 -d ':'`
	M=`echo $MYDATE | cut  -f 2 -d ':'`
	S=`echo $MYDATE | cut  -f 3 -d ':'`
	FILENAME="./log$H$M$S.txt"
	#--------------------------------------------------------------
	
	#--------------------------------------------------------------
	echo `date`  > $FILENAME
	#--------------------------------------------------------------
	DATA=`atcom -c at+csq -d /dev/modem -s ':'  `
	echo $DATA   >> $FILENAME
	#--------------------------------------------------------------

	SIGNAL=`echo $DATA | cut -d ':' -f 3`
	echo "FILENAME=$FILENAME    ($SIGNAL)"



	sync
	usleep 100000

	curl -s -T $FILENAME "ftp://FTPdilupe:ST7Sxi@$SERVER_IP:/test/"

	usleep 100000
	rm $FILENAME

  	let ELAPSED+=1


done
