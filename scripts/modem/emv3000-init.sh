#!/bin/sh

# Cleanup links:

echo "emv3000-init.sh"  > /dev/kmsg






#==============================================================================
emv3000_down()
{
    echo "    Reset emv3000" > /dev/kmsg
    echo 1 > /sys/class/leds/emv-reset/brightness
}
#==============================================================================
emv3000_up()
{
    echo "    Set emv3000" > /dev/kmsg
    echo 0 > /sys/class/leds/emv-reset/brightness
}
#==============================================================================
wait_emv3000_is_up()
{
    echo "Waiting emv3000 is up (25 sec)"
    for i in {1..25}
    do
        EMV3000=`lsusb |grep "1fc9:012a"`

        if [ "$EMV3000" == "" ]
        then
            echo "sleep $i"
            sleep 1
        else
            echo "sleep $i : $EMV3000" > /dev/kmsg
            break
        fi

        if [ "$i" == "1" ] || [ "$i" == "8" ] || [ "$i" == "16" ]
        then
                emv3000_down
                sleep1
                emv3000_up
        fi
    done
}
#==============================================================================


EMV3000=`lsusb |grep "1fc9:012a"`

if [ "$EMV3000" == "" ]
then
    echo "    EMV3000 not present !"  > /dev/kmsg
    wait_emv3000_is_up
else
    echo "    EMV3000 present"  > /dev/kmsg
    exit 0
fi



EMV3000=`lsusb |grep "1fc9:012a"`

if [ "$EMV3000" == "" ]
then
    echo "emv3000-init.sh: Failure"  > /dev/kmsg
else
    echo "emv3000-init.sh: OK" > /dev/kmsg
fi












