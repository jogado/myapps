#!/bin/sh

# Cleanup links:

echo "modem-init.sh"  > /dev/kmsg




#==============================================================================
modem_pwr_down()
{
    echo "Modem power down"> /dev/kmsg
    echo 0 > /sys/class/leds/modem-pwr/brightness
}
#==============================================================================
modem_pwr_up()
{
    echo "Modem power up"> /dev/kmsg
    echo 1 > /sys/class/leds/modem-pwr/brightness
}
#==============================================================================
modem_power()
{

    echo "Set modem-reset to 0"> /dev/kmsg
    echo 0 > /sys/class/leds/modem-reset/brightness

    modem_pwr_down
    sleep 3
    modem_pwr_up
    sleep 2
}
#==============================================================================
wait_modem_is_up()
{
    echo "Waiting modem is up (30 sec)"
    for i in {1..25}
    do
        MODEM=`lsusb |grep "1546:110a"`

        if [ "$MODEM" == "" ]
        then
            echo "sleep $i"
            sleep 1
        else
            echo "sleep $i : $MODEM" > /dev/kmsg
            break
        fi

        if [ "$i" == "1" ] || [ "$i" == "8" ] || [ "$i" == "16" ]
        then
            modem_power
        fi
    done
}
#==============================================================================





MODEM=`lsusb |grep "1546:110a"`

if [ "$MODEM" == "" ]
then
    echo "    modem not present !"  > /dev/kmsg
    wait_modem_is_up
else
    echo "    modem present"  > /dev/kmsg
    exit 0
fi



MODEM=`lsusb |grep "1546:110a"`
if [ "$MODEM" == "" ]
then
    echo "modem-init.sh: Failure"  > /dev/kmsg
else
    echo "modem-init.sh: OK" > /dev/kmsg
fi

exit 0


























ls /dev/ -la |grep -i "> ttyACM"

remove_file()
{
    TARGET=$1

#   echo "remove Target = /dev/$TARGET"
     if [ -L "/dev/$TARGET" ]
    then
        echo "    removing /dev/$TARGET"
        rm /dev/$TARGET
    else
        echo "    Not exist /dev/$TARGET"
    fi
}

echo "cleaning usb links"

remove_file "oti"
remove_file "emv3000"
remove_file "barcode"
remove_file "modem"




do_modem_power()
{
    echo "Modem Power Down (3sec)"
    echo 0 > /sys/class/leds/modem-pwr/brightness
    sleep 3
    echo "Modem Power Up (3 sec )"
    echo 1 > /sys/class/leds/modem-pwr/brightness
    sleep 3
}


do_modem_power

echo "Waiting modem is up (30 sec)"
for i in {1..30}
do
    MODEM=`lsusb |grep "1546:110a"`

    if [ "$MODEM" == "" ]
    then
        echo "sleep $i"
        sleep 1
    else
        echo "sleep $i : $MODEM"
        break
    fi


    if [ "$i" == "10" ]
    then
        do_modem_power
    fi
done

echo "Done"




echo "Reset emv3000"
echo 1 > /sys/class/leds/emv-reset/brightness
sleep 1
echo 0 > /sys/class/leds/emv-reset/brightness
echo "Done"

echo "Waiting EMV3000 is up"
for i in {1..30}
do
    EMV3000=`lsusb |grep "1fc9:012a"`

    if [ "$EMV3000" == "" ]
    then
        echo "sleep $i"
        sleep 1
    else
        echo "sleep $i : $EMV3000"
        break
    fi
done
echo "Done"
sleep 2
echo "links:"
ls /dev/ -la |grep -i "> ttyACM"










lsusb 



cd /dev
ln -sf ttyACM7 /dev/barcode
ln -sf ttyACM1 /dev/modem
ln -sf ttyACM6 /dev/oti
ln -sf ttyACM6 /dev/emv3000

ls -la /dev/ |grep -i "> ttyACM"
