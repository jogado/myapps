#!/bin/sh

COMS=`ls -la /dev/ttymxc* | cut -d ':' -f 2 | cut -c4-20`

while [ 1 ] 
do
	for i in $COMS
	do
		echo test $i >  /dev/kmsg
		MSG="    I'm uart $i"
		echo $MSG > $i
		sleep 1
	done
done