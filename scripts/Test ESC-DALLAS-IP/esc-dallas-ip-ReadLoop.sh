#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Read Loop - esc-dallas-ip"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------




LOOP_COUNT=1000
LOOP_DELAY=10000

READ_ERROR=0

read_test()
{
for i in $(seq 1 1 $LOOP_COUNT)
do
    ret=`esc-dallas-ip | grep 9999 | cut -d ':' -f 2`
    IP=`esc-dallas-ip | grep Ip | cut -d ':' -f 2`
#    echo "ret = $ret"
      
    if [ "$ret" = "9999" ]
	then
		  let READ_ERROR=READ_ERROR+0
    else
		  let READ_ERROR=READ_ERROR+1
    
	fi
    
   
    echo "$i - ($READ_ERROR) - $IP "
    echo "$i - ($READ_ERROR) - $IP " >> Log.txt

#    usleep $LOOP_DELAY

done
}





read_write_test()
{
	READ_ERROR=0


	esc-dallas-ip -w -ip 192.168.1.215 -mask 255.255.248.0 -gw 192.168.1.1 -tag 9999

	for i in $(seq 1 1 1000)
	do

		echo "==============================================="

        # Write new tag
		echo "Writing Tag ($i)"
        esc-dallas-ip -w -tag $i

		echo "Read back tag and check"
        Dallas_data=`esc-dallas-ip > /tmp/dallas.txt`

        IP=`cat /tmp/dallas.txt  | grep Ip  | cut -d ':' -f 2`
        TAG=`cat /tmp/dallas.txt | grep Tag | cut -d ':' -f 2`
      
        if [ "$TAG" = "$i" ]
    	then
    		  let READ_ERROR=READ_ERROR+0
        else
    		  let READ_ERROR=READ_ERROR+1
    	fi
    
        echo "Read (Tag=$TAG , Ip=$IP ) (err:$READ_ERROR) "
        echo "Read (Tag=$TAG , Ip=$IP ) (err:$READ_ERROR) ">> Log_read_write.txt

	done

	esc-dallas-ip -w -ip 192.168.1.215 -mask 255.255.248.0 -gw 192.168.1.1 -tag 9999

}


read_write_test

exit 0


