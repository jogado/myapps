#!/bin/sh

FILE_1="install.sh"
FILE_2="validator-mpasscloud-image-esc-imx6ul-20200721064354.rootfs.tar.bz2"



if echo $1 | grep -q "192.168"; then
	echo "*****************************"
	echo "***   IP: $1   ***"
	echo "*****************************"
	ssh-keygen -f "/home/ubuntu/.ssh/known_hosts" -R "$1"
	scp $FILE_1 root@$1:
	scp $FILE_2 root@$1:
	ssh root@$1 "sync;beep;sleep 1 ;beep"	
	ssh root@$1 "cd /home/root;chmod +x $FILE_1;ls -la;sync"	
	ssh root@$1 "cd /home/root;./$FILE_1"	
	exit
fi


transfer()
{
	echo "*****************************"
	echo "***   IP: $1   ***"
	echo "*****************************"
	ssh-keygen -f "/home/ubuntu/.ssh/known_hosts" -R "$1"
	scp $FILE_1 root@$1:
	scp $FILE_2 root@$1:
	ssh root@$1 "sync;beep;sleep 1 ;beep"	
	ssh root@$1 "cd /home/root;chmod +x $FILE_1;ls -la;sync"	
	ssh root@$1 "cd /home/root;./$FILE_1"	
	exit
}
#transfer 192.168.1.201
transfer 192.168.1.202
transfer 192.168.1.203
transfer 192.168.1.204
#transfer 192.168.1.205



