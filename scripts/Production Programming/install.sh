#!/bin/sh


FILE="validator-mpasscloud-image-esc-imx6ul-20200721064354.rootfs.tar.bz2"



echo "=================================="
echo " Install Image : $FILE"
sleep 2
rootfs-install -p 2 validator-mpasscloud-image-esc-imx6ul-20200721064354.rootfs.tar.bz2
echo 0 > /sys/block/mmcblk1boot0/force_ro
fw_setenv partition 2
echo 1 > /sys/block/mmcblk1boot0/force_ro
fw_printenv | grep -i 'partition='


#  echo "=================================="
#  echo " Re-Installing u-boot-install"
#  sleep 2
#  cd /boot
#  echo 0 > /sys/block/mmcblk1boot0/force_ro
#  u-boot-install -f u-boot.imx-512MB -d /dev/mmcblk1boot0
#  echo 1 > /sys/block/mmcblk1boot0/force_ro


echo "=================================="
echo " Change device.dtb to abt3000_1_0.dtb"
sleep 2
echo 0 > /sys/block/mmcblk1boot0/force_ro
fw_setenv fdt_file dtb/abt3000_1_0.dtb
echo 1 > /sys/block/mmcblk1boot0/force_ro
echo "=================================="



echo "=================================="
echo "Rebooting"
echo "=================================="
beep 
beep 
sleep 2
reboot

