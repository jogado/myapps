#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Time loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------




DATA=`wget -qSO- google.com 2>&1 | grep Date: | cut -d' ' -f5-8 `

if [ -n "${DATA}" ]
then
	echo "found : $DATA"
else
	echo "Update date faild"
	exit
fi



#
#30 Jun 2021 06:48:46
#
TM_DAY=`echo $DATA | cut -d ' ' -f 1` 
TM_MONTH=`echo $DATA | cut -d ' ' -f 2` 
TM_YEAR=`echo $DATA | cut -d ' ' -f 3` 
TIME=`echo $DATA | cut -d ' ' -f 4` 
TM_HOUR=`echo $TIME | cut -d ':' -f 1` 
TM_MIN=`echo $TIME |  cut -d ':' -f 2` 
TM_SEC=`echo $TIME |  cut -d ':' -f 3` 


case $TM_MONTH in
    Jan) MON="01" ;;
    Feb) MON="02" ;;
    Mar) MON="03" ;;
    Apr) MON="04" ;;
    May) MON="05" ;;
    Jun) MON="06" ;;
    Jul) MON="07" ;;
    Aug) MON="08" ;;
    Sep) MON="09" ;;
    Oct) MON="10" ;;
    Nov) MON="11" ;;
    Dec) MON="12" ;;
      *) MON="00" ;;	
esac

# echo "$TM_YEAR"
# echo "$MON"
# echo "$TM_DAY"
# echo "$TM_HOUR"
# echo "$TM_MIN"
# echo "$TM_SEC"


TZ=GMT+2 date $MON$TM_DAY$TM_HOUR$TM_MIN$TM_YEAR
hwclock --systohc


