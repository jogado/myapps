Section "Files"
	FontPath	"/usr/share/fonts/ttf"
EndSection

Section "Monitor"
    Identifier    "MV3000 Monitor"
    Option        "DPMS"
EndSection

Section "Device"
    Identifier    "MV3000 FrameBuffer"
    Driver        "fbdev"
    Option        "fbdev"                "/dev/fb0"
    Option        "ShadowFB"             "on"
    	Option		"Rotate"		"CW"
EndSection

Section "Screen"
    Identifier    "MV3000 Screen"
    Monitor       "MV3000 Monitor"
    Device        "MV3000 Frame Buffer"

    DefaultDepth  16

    SubSection "Display"
        Depth     16
        Modes     "480x800"
    EndSubSection
EndSection





Section "InputDevice"
    Identifier "MV3000 Touchscreen"
    Driver        "evdev"
    Option        "SendCoreEvents"       "true"
    Option        "Device"               "/dev/input/touchscreen0"
    Option        "InvertX"              "0"
    Option        "InvertY"              "1"
    Option		"SwapAxes"		"1"
EndSection

Section "ServerLayout"
    Identifier    "MV3000"
    Screen        "MV3000 Screen"
    InputDevice   "MV3000 Touchscreen"  "CorePointer"
    Option        "BlankTime"            "0"
    Option        "StandbyTime"          "0"
    Option        "SuspendTime"          "0"
    Option        "OffTime"              "0"
EndSection

Section "ServerFlags"
    Option        "AutoAddDevices"       "false"
EndSection

