#!/bin/sh

GPIO=148


#---------------------------------------------------------------------------------
TEST_DESC="Gpio loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------
if [ "$#" -lt "1" ]
then
    out="/tmp/gpio.log"
    
else
    out="$1"
fi
#---------------------------------------------------------------------------------
clean_up()
{
	echo "clean_up()"
	echo $GPIO > /sys/class/gpio/unexport
	exit
}
trap clean_up SIGTERM SIGINT  SIGKILL SIGHUP EXIT 
#---------------------------------------------------------------------------------




GPIO=148

echo $GPIO > /sys/class/gpio/export
echo "in" >  /sys/class/gpio/gpio$GPIO/direction
while [ 1 ]
do
	VAL=`cat /sys/class/gpio/gpio$GPIO/value`
	echo "GPIO($GPIO)=$VAL"
	sleep 1  
done

