#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Time loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------


if [ "$#" -lt "1" ]
then
    out="/tmp/time.txt"
    
else
    out="$1"
fi



while [ 1 ]
do

	mytime=`date |cut -c12-20`
 	
	RESULT=`echo $mytime`
	echo $RESULT
	echo $RESULT > $out

	sleep 1  
done

