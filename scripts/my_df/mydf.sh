#!/bin/sh



while [ 1 ]
do

LINE=`df |grep -i sda5`

TOT=`df |grep -i sda5 | tr -s ' ' | cut -d ' ' -f 2`
CUR=`df |grep -i sda5 | tr -s ' ' | cut -d ' ' -f 3`

PRECENT=`expr $TOT - $CUR`
PRECENT2=`expr $PRECENT \* 10000`
PRECENT3=`expr $PRECENT2 / $TOT`

P1=`echo $PRECENT3 | cut -c 1-2`
P2=`echo $PRECENT3 | cut -c 3-4`


USED2=`expr $CUR \* 10000`
USED3=`expr $USED2 / $TOT`


P3=`echo $USED3 | cut -c 1-2`
P4=`echo $USED3 | cut -c 3-4`


echo -n "$LINE   Used:$P3.$P4   Free: $P1.$P2 % \r"




done
