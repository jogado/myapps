#!/bin/sh


#  cur_time="$(date -u +%s)"
#  S=$((cur_time%60))
#  M=$((cur_time/60%60))
#  H=$((cur_time/60/60%24))
#  OUT="ping.result-$H:$M:$S.txt"

OUT=`date +'ping-result-%Y%m%d-%H:%M:%S.txt' `

MYIP=`ifconfig |grep -i -m 1 inet |cut -d ':' -f 2 |cut -d ' ' -f 1 `
while [ 1 ]
do

    ping google.com -c 1 -W 1 > ping.txt
    RES=`cat ping.txt  | grep -i "1 received, 0% packet loss" `
    RES=`cat ping.txt  | grep -i "1 packets received, 0% packet loss" `
    TIMESTAMP=`date +'%d/%m/%Y %H:%M:%S:%3N' `
    TIMESTAMP=`date +'%d/%m/%Y %H:%M:%S' `

    MYIP=`ifconfig |grep -i -m 1 inet |cut -d ':' -f 2 |cut -d ' ' -f 1 `


    LEN=$(echo -n $RES | wc -m)

    if [ $LEN -gt 10 ]
    then
        echo "$TIMESTAMP  : ( OK $MYIP ) $RES"
        echo "$TIMESTAMP  : ( OK $MYIP ) $RES" >> $OUT

    else
        echo "$TIMESTAMP  : ( BAD ) ***********************************"
        echo "$TIMESTAMP  : ( BAD ) ***********************************" >> $OUT
    fi
    sleep 5


done
