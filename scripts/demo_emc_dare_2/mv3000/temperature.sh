#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Test temperature loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------

I2C_ADDR="0-0048"
I2Cbus=0
EXE="cat /sys/class/i2c-dev/i2c-${I2Cbus}/device/${I2C_ADDR}/hwmon/hwmon0/temp1_input"
DIV="1000"


if [ "$#" -lt "1" ]
then
    out="/tmp/temperature.txt"
    
else
    out="$1"
fi



while [ 1 ]
do
	res=`$EXE`
	if [ $? -ne 0 ] ; then
		RESULT=`Temp Read Error`
	else
		let res=res/DIV

		if [ $res -gt 85 ] ; then 
			RESULT=`echo $res C HIGH`
		else
			RESULT=`echo $res C OK`
		fi

	fi
	echo $RESULT
	echo $RESULT > $out
	usleep 300000
done

