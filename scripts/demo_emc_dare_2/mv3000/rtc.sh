#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Time loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------


if [ "$#" -lt "1" ]
then
    out="/tmp/time.txt"
    
else
    out="$1"
fi



while [ 1 ]
do

	mytime=`date |cut -c12-20 | tr -d '\n\r'`

        C1=`echo $mytime | cut -c3`
        C2=`echo $mytime | cut -c6`

 
	if [ $C1 == ":" ] && [ $C2 == ":" ]
	then
		echo $mytime	
		echo -n -e $mytime > $out
		echo  $mytime > /dev/pts/0
	else
		echo "RTC read Error" 
		echo -n -e "RTC read Error" > $out
		echo "RTC read Error" > /dev/pts/0
	fi


	usleep 300000 
done

