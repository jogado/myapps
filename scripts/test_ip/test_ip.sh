#!/bin/sh

#==============================================================================
#
# Test Temperature sensor
#
#==============================================================================
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' ' `
#==============================================================================




IP_CHECK="8.8.8.8"




LOGFILE="/home/root/test_ip.log"
IP="`ifconfig | grep -i inet -m1 | cut -d ':' -f2 | cut -d ' ' -f1` "
MASK=`ifconfig | grep -i inet -m1 | cut -d ':' -f4`
WHEN=`date +%y.%m.%d-%H.%M.%S`

echo "===========================================================================================" >> $LOGFILE
echo "DATE        : $WHEN"           >> $LOGFILE
echo "DEVICE      : $DEVICE_TYPE"    >> $LOGFILE
echo "DEVICE IP   : $IP"		     >> $LOGFILE
echo "DEVICE MASK : $MASK"           >> $LOGFILE
echo "uname -a    : `uname -a`"      >> $LOGFILE
echo "IP_CHECK    : $IP_CHECK"       >> $LOGFILE
echo "===========================================================================================" >> $LOGFILE

echo "" > /dev/tty0
echo "================================================" > /dev/tty0

while [ 1 ]
do
    RESULT=`fping -t100 $IP_CHECK`
    FOUND=`echo $RESULT | grep -i "is alive" `

    if [ "$FOUND" == "" ]
    then

        beep -f 4000
        usleep  500000
        beep -f 4000

        WHEN=`date +%y.%m.%d-%H.%M.%S`
        echo "$WHEN: IP:$IP_CHECK is unreachable" >> $LOGFILE
        echo "$WHEN: IP:$IP_CHECK is unreachable *******" >> /dev/tty0
        echo "$WHEN: IP:$IP_CHECK is unreachable *******" 

        # ifconfig eth0 down
        # sleep 1
        # ifconfig eth0 up
        # sleep 9
        # route add default gw $GWS eth0

        /etc/init.d/networking stop  > /dev/null
        sleep 2
        /etc/init.d/networking start > /dev/null
        sleep 9

    else
        WHEN=`date +%y.%m.%d-%H.%M.%S`
        SEC=`date +%y.%m.%d-%H.%M.%S |cut -c 16-17`
        IP="`ifconfig | grep -i inet -m1 | cut -d ':' -f2 | cut -d ' ' -f1` "
        MASK=`ifconfig | grep -i inet -m1 | cut -d ':' -f4`

        # if [ "$SEC" == "00" ] || [ "$SEC" == "15" ]|| [ "$SEC" == "30" ] || [ "$SEC" == "45" ]
        # then
        #    WHEN=`date +%y.%m.%d-%H.%M.%S`
        #    echo "$WHEN: DeviceIP:$IP DeviceMASK=$MASK   ping $IP_CHECK : OK"  >> $LOGFILE
        #    echo "$WHEN: ping $IP_CHECK : OK"  >> /dev/tty0
        # fi

        echo "$WHEN: DeviceIP:$IP DeviceMASK=$MASK   ping $IP_CHECK : OK"
    fi

	usleep 800000

done

