#!/bin/sh


echo  "========================================================="
echo  "Barcode      : Set wake to 0"
echo 0 > /sys/class/leds/barcode_wake/brightness
usleep 300000

echo  "emv3000      : DOWN"
echo 1 > /sys/class/leds/emv-reset/brightness
usleep 300000
echo  "emv3000      : UP"
echo 0 > /sys/class/leds/emv-reset/brightness

echo  "emv3000      : WAIT 5 Sec (emv3000 boot)"
sleep 5


RES=`lsusb |grep NXP`

echo  "lsusb result : $RES"
echo  "========================================================="
echo  ""
echo  ""
echo  ""
