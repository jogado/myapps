#!/bin/sh

OUT="/home/root/log.check_loop.sh"


echo  "" >> $OUT
echo  "=======================================================" >> $OUT

ELAPSED=""
SEC=0
MIN=0
HOURS=0
DAY=0

partition=`fw_printenv |grep -i "partition="`
check_emv3000()
{
    EMV=`lsusb |grep -i 1fc9 |cut -d ' ' -f 6`
}

check_barcode()
{
    BARCO=`lsusb |grep -i "1eab:2C06" |cut -d ' ' -f 6`
}


time_convert()
{
    TSEC=$1
    SEC=`expr $TSEC % 60`
    MIN=`expr $TSEC / 60 % 60`
    HOUR=`expr $TSEC / 3600 % 24`
    DAY=`expr $TSEC / 3600 / 24`
#   ELAPSED=`printf '%d days %02d:%02d:%02d \n' "$DAY" "$HOUR" "$MIN" "$SEC"`
    ELAPSED=`printf '%02d:%02d:%02d \n' "$HOUR" "$MIN" "$SEC"`
}

clean_up()
{
    echo  "clean_up()" >> $OUT
    echo  "$partition: UpTime: $ELAPSED  emv3000( $EMV ) - barcode ( $BARCO )" >> $OUT
    echo  "=======================================================" >> $OUT
}
trap clean_up SIGTERM SIGINT EXIT



elapsed=0
start_time="$(date -u +%s)"
while [ 1 ]
do

    check_emv3000
    check_barcode

    end_time="$(date -u +%s)"
    elapsed="$(($end_time-$start_time))"
    time_convert $elapsed

    echo  "$partition: UpTime: $ELAPSED  emv3000( $EMV ) - barcode ( $BARCO )"

    if [ $SEC == "0" ]
    then
        echo  "$partition: UpTime: $ELAPSED  emv3000( $EMV ) - barcode ( $BARCO )" >> $OUT
        sleep 1
    fi


    if [ "$EMV" == "" ]
    then
        beep
        beep
        echo  "EMV3000 missing => exit" >> $OUT
        break
    fi

    if [ "$BARCO" == "" ]
    then
        beep
        beep
        echo  "BARCODE missing => exit" >> $OUT
        break
    fi
    usleep 500000
done