#!/bin/sh

#---------------------------------------------------------------------------------
TEST_DESC="Test temperature loop"
#---------------------------------------------------------------------------------
TMP_FOLDER="/var/run"
mkdir -p  $TMP_FOLDER
FILENAME=$(basename $0)
echo $$ > $TMP_FOLDER/$FILENAME.pid
#---------------------------------------------------------------------------------
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`
DEVICE_TYPE=`hwinfo -m | cut -f 2 -d ' '`
ARCH=$(uname -m)
#---------------------------------------------------------------------------------

I2C_ADDR="0-0048"
I2Cbus=0
EXE="cat /sys/class/i2c-dev/i2c-${I2Cbus}/device/${I2C_ADDR}/hwmon/hwmon0/temp1_input"
DIV="1000"


if [ "$#" -lt "1" ]
then
    out="/tmp/temperature.txt"
    
else
    out="$1"
fi


TIME_ASOLUTE=0

while [ 1 ]
do
			
			#----------------------------------------------------------------------------------
			C_FREQ=`cat /sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_cur_freq`
			#----------------------------------------------------------------------------------

			#----------------------------------------------------------------------------------
			I2C_TEMP=`cat /sys/class/i2c-dev/i2c-0/device/0-0048/hwmon/hwmon0/temp1_input`

			if [ ${#I2C_TEMP} -gt 5 ] 
			then
				I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-3`
				I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c4`"
			else
				I2C_TEMP_STR=`echo $I2C_TEMP | cut -c1-2`
				I2C_TEMP_STR="$I2C_TEMP_STR.`echo $I2C_TEMP | cut -c3`"
			fi	
			#----------------------------------------------------------------------------------

			#----------------------------------------------------------------------------------
			CPU_TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`

			if [ ${#CPU_TEMP} -gt 60 ] 
			then
				CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-3`
				CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c4`"
			else
				CPU_TEMP_STR=`echo $CPU_TEMP | cut -c1-2`
				CPU_TEMP_STR="$CPU_TEMP_STR.`echo $CPU_TEMP | cut -c3`"
			fi	
			#----------------------------------------------------------------------------------


			echo "$TIME_ASOLUTE ;$C_FREQ ;$CPU_TEMP_STR ;$I2C_TEMP_STR"
			echo "$TIME_ASOLUTE ;$C_FREQ ;$CPU_TEMP_STR ;$I2C_TEMP_STR" >> temperature.log.txt
			

			let TIME_ASOLUTE+=30
			sleep 30
done

