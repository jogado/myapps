#!/bin/bash


current_date_time="$(date -u +"%Y-%m-%d_%H_%M_%S")"
LOGFILE="test-$current_date_time.txt"

echo "Logfile=$LOGFILE"

echo "" > $LOGFILE



beep()
{
    amixer sset DAC 100% on
	amixer sset Speaker 50% on
    aplay /home/root/22294.wav
}




test_body()
{
    TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
    GPRS0=`ls -la /dev/gprs0`
    GPRS1=`ls -la /dev/gprs1`
    GPRS2=`ls -la /dev/gprs2`
    MODEM_CI=`cat /tmp/modem3gci`
    CSQ=`cat /tmp/modemcsq`

    echo "======================================================" >> $LOGFILE
    echo `date`                             >> $LOGFILE
    echo "======================================================" >> $LOGFILE
    lsusb |grep -i option                   >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    ls -la /dev/gprs*                       >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    dmesg |grep -i "new full speed USB"     >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    echo "MODEM_CI     : $MODEM_CI"         >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    echo "MODEM CSQ    : $CSQ"              >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    echo "Temperature  : $TEMP"             >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
    dmesg | grep USB                        >>  $LOGFILE
    echo "======================================================" >> $LOGFILE
}


for i in {1..30};do

    echo -n -e "\n\rCOUNT ****** $i *******  : "

    for t in {1..60};do
        echo -n -e "."
        sleep 1
    done

    echo "COUNT ****** $i ******* " >> $LOGFILE
    test_body
    sync
done

cat $LOGFILE

echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE
dmesg      >>  $LOGFILE
echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE
echo "======================================================" >> $LOGFILE


beep
sleep 10
reboot


exit 0

