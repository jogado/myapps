#!/bin/sh
#
# Test Audio 
#
REC=/tmp/raw
SQUARE=/usr/share/audio/square80.wav
SINE=/usr/share/audio/sine440.wav
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
        echo "${RESULT}" > $out
        exit 0
}

analyse()
{ # checks the number of samples in half period of a square wave
prev=''
thres=20000
correct=0
dd if=$1 count=4 skip=0 |hexdump -v -x > /tmp/samples
count=0
case $2 in
0)
samples=`cat /tmp/samples | grep 0 | tr -s ' ' | cut -d' ' -f2,4,6,8`
;;
1)
samples=`cat /tmp/samples | grep 0 | tr -s ' ' | cut -d' ' -f3,5,7,9`
;;
2)
samples=`cat /tmp/samples | grep 0 | tr -s ' ' | cut -d' ' -f2,3,4,5,6,7,8,9`
;;
*)
echo missing/wrong parameter
return 1
;;
esac
for sample in $samples ; do
count=$((count+1))
samplex=$(echo 0x$sample)
sample=$(printf %d $samplex)
if [ -n "$prev" ] ; then
if [ $prev -lt $sample ] ; then
        delta=$((sample-prev))
else
        delta=$((prev-sample))
fi
if [ $delta -gt $thres ] ; then
        if [ $count -eq $3 ] ; then
        correct=$((correct+1))
        fi
        count=0
fi
fi
prev=$sample
done
echo Correct=$correct
if [ $correct -gt 5 ] ; then 
	return 0;
else
	return 1;
fi
}

test_loop()
{
RETRIES=10
while [ $RETRIES -gt 0 ] ; do
case $1 in
	1) #MIC Record
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Mic
		amixer sset Capture 100 cap
		amixer sset Master 100%,100%
		amixer sset Mux 100%,100%
		LEFTRIGHT=0
	else
		# Set Record Mux to Mic
		amixer cset numid=35 0 
		amixer sset Record 100% on
		amixer sset "Mic Boost1" 10dB
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Speaker 100%,100% on
		LEFTRIGHT=2 #mono recording
	fi
	HALFPERIOD=50 #8000/80/2
	;;
	2) #Left Record
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Line
		amixer sset Capture 100%,0% cap
		amixer sset Master 100%,0%
		amixer sset Mux 100%,0%
		LEFTRIGHT=1
	else
		# Set Record Mux to Line
		amixer cset numid=35 4 
		amixer sset Record 100%,0% on 
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Speaker 100%,0% on
		LEFTRIGHT=2 #mono recording
	fi
	HALFPERIOD=50 #8000/80/2
	;;
	3) #Right Record
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Line
		amixer sset Capture 0%,100% cap
	  	amixer sset Master 0%,100%
		amixer sset Mux 0%,100%
		LEFTRIGHT=1
	else
		# Set Record Mux to Line
		amixer cset numid=35 4 
		amixer sset Record 0%,100% on 
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Speaker 0%,100% on
		LEFTRIGHT=2 #mono recording
	fi
	HALFPERIOD=50 #8000/80/2
	;;
	4) #Stereo Record
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Line
		amixer sset Capture 100%,100% cap
		amixer sset Master 100%,100%
		amixer sset Mux 100%,100%
		HALFPERIOD=100 #8000/80/2*2
	else
		# Set Record Mux to Line
		amixer cset numid=35 4 
		amixer sset Record 100%,100% on 
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Speaker 100%,100% on
		HALFPERIOD=50 #8000/80/2*2/2 Mono recording
	fi
	LEFTRIGHT=2
	;;
	5) #Left Record Mic
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Mic
		amixer sset Capture 100%,0% cap
		amixer sset Master 100%,0%
		amixer sset Mux 100%,0%
		LEFTRIGHT=1
	else
		# Set Record Mux to Line
		amixer cset numid=35 0 
		amixer sset Record 100%,100% on 
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Headphone 100%,0% on
		LEFTRIGHT=2 #mono recording
	fi
	HALFPERIOD=50 #8000/80/2
	;;
	6) #Right Record Mic
	if [ $AC97 -eq "0" ] ; then
		amixer sset 'Input Source' Mic
		amixer sset Capture 0%,100% cap
		amixer sset Master 0%,100%
		amixer sset Mux 0%,100%
		LEFTRIGHT=1
	else
		# Set Record Mux to Line
		amixer cset numid=35 0 
		amixer sset Record 100%,100% on 
		amixer sset DAC 100%,100% off
		amixer set "Pop Bypass Mux" "Bypass Mixer"
		amixer sset Headphone 0%,100% on
		LEFTRIGHT=2 #mono recording
	fi
	HALFPERIOD=50 #8000/80/2
	;;
	7) #Loopback Record
	# Set Record Mux to Stereo Mix 
	amixer cset numid=35 5 
	amixer sset Record 100%,100% on
	amixer sset DAC 100%,100% on
	amixer set "Record All Mux" "Analog plus DAC"
	LEFTRIGHT=2 #mono recording
	HALFPERIOD=50 #8000/80/2
	;;
        8) #MIC Record
	# Set Record Mux to Line Left
	amixer cset numid=35 0
	amixer sset Record 100% on
	amixer sset "Mic Boost1" 10dB
	amixer sset DAC 100%,100% off
	amixer set "Pop Bypass Mux" "Bypass Mixer"
	amixer sset Speaker 100%,0% on
	LEFTRIGHT=2 #mono recording
        HALFPERIOD=50 #8000/80/2
        ;;
	*)
	RESULT="501 Wrong Parameter"
	result
	;;
esac
aplay $SQUARE &
arecord -c 2 --duration=1 -f U16_LE -r 8000 -t raw $REC &
sleep 2 
killall -9 aplay
killall -9 arecord
analyse $REC $LEFTRIGHT $HALFPERIOD
RET=$?
if [ $RET -eq 0 ] ; then 
break
fi
RETRIES=$((RETRIES-1))
echo RETRIES LEFT: $RETRIES
done
return $RET

}
amixer > /dev/null
if [ "$?" -ne 0 ] ; then
RESULT="520 Sound driver not loaded"
result
fi
AC97=$(amixer | grep -c DAC)
if [ $AC97 -eq "0" ] ; then
	amixer sset Master on
	amixer sset IEC958 on cap
else
	amixer sset DAC 100% on
fi


warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi



case $3 in 
        10)# BC800
	if [ $2 -eq 4 ] ; then
		test_loop 7 
		if [ $? -ne 0 ] ; then
			RESULT="520 Audio Loopback failed"
			result
		fi
	else
		test_loop 4 
		if [ $? -ne 0 ] ; then
			RESULT="520 Stereo Line-out to Line-in failed"
			result
		fi
		test_loop 1
		if [ $? -ne 0 ] ; then
			RESULT="520 Line-out to Mic failed"
			result
		fi
	fi
	RESULT="200 Audio Test Success"
	result
	;;
        11)# DC800
	if [ $2 -eq 4 ] ; then
		test_loop 7 
		if [ $? -ne 0 ] ; then
			RESULT="520 Audio Loopback failed"
			result
		fi
	else
		test_loop 5
		if [ $? -ne 0 ] ; then
			RESULT="520 headphone-out left to Mic failed"
			result
		fi
		test_loop 6 
		if [ $? -ne 0 ] ; then
			RESULT="520 headphone-out right to Mic failed"
			result
		fi
	fi
	if [ $2 -eq 4 ] ; then
		amixer sset Speaker 10% on
	else
		amixer sset Speaker 50% on
	fi
	aplay -d 2 $SINE
	RESULT=`${CONFIRM} "Speaker tone" $2 1 60`
	result
	;;
        12)# V800
	if [ $2 -eq 4 ] ; then
		test_loop 7 
		if [ $? -ne 0 ] ; then
			RESULT="520 Audio Loopback failed"
			result
		fi
	fi	
	test-stm32 -m -1
	if [ $2 -eq 4 ] ; then
		amixer sset Speaker 10% on
	else
		amixer sset Speaker 50% on
	fi
	aplay -d 2 $SINE
	RESULT=`${CONFIRM} "Speaker tone" $2 1 60`
 	test-stm32 -m -0
	result
	;;
        100)# 
	aplay $SINE &
	RESULT=`${CONFIRM} "Sine" $2`
	result
	;;
        13)# DC802
	if [ $2 -eq 4 ] ; then
		test_loop 7 
		if [ $? -ne 0 ] ; then
			RESULT="520 Audio Loopback failed"
			result
		fi
	else
		test_loop 5
		if [ $? -ne 0 ] ; then
			RESULT="520 headphone-out left to Mic failed"
			result
		fi
		test_loop 6 
		if [ $? -ne 0 ] ; then
			RESULT="520 headphone-out right to Mic failed"
			result
		fi
		test_loop 8 
		if [ $? -ne 0 ] ; then
			RESULT="520 line-out left to Mic failed"
			result
		fi
	fi	
	if [ $2 -eq 4 ] ; then
		amixer sset Speaker 10% on
	else
		amixer sset Speaker 50% on
	fi
	aplay -d 2 $SINE
	RESULT=`${CONFIRM} "Speaker tone" $2 1 60`
	result
	;;
        100)# 
	aplay $SINE &
	RESULT=`${CONFIRM} "Sine" $2`
	result
	;;
	*)
	test_loop $3
	if [ $? -ne 0 ] ; then
		RESULT="520 audio test failed"
		result
	fi
	RESULT="200 Audio Test Success"
	result
	;;
esac
RESULT="501 Wrong Mode"
result
