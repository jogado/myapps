#!/bin/sh
#
# return serial number of device
#

out="$1"

# kill application
# Use /etc/init.d application startscripts for this purpose
# Wait after stop until no more users of oti/sam/ACM
#
killapp() {
	#Reminder: ideally all application developers use same service name
	appstarter=`ls /etc/init.d/mobileplatform*`
	if [ "$appstarter" == "" ] ; then
		appstarter=`ls /etc/init.d/launcherd*`
	fi
	if [ "$appstarter" == "" -o ! -x $appstarter ] ; then
		return
	fi
	# Stop the application
	$appstarter stop
	i=0
	while [ $i -lt 20 ] ; do
		usleep 500000
		pids=`ls -l /proc/[0-9]*/fd/[0-9]* 2&>/dev/null | grep -iE "/dev/ttyACM|/dev/oti|/dev/sam" | cut -d'/' -f 3 | sort -u`
		if [ "$pids" == "" ] ; then
			break
		fi
		let i=i+1
	done
}

# kill x here if not running with -noreset option
killxreset() {
	xpid=`pidof Xorg`
	if [ "$xpid" != "" ] ; then
		if ! cat /proc/${xpid}/cmdline | grep "noreset" ; then
			echo "Killing x $xpid"
			kill $xpid
		fi	
	fi	
}

result()
{
	echo "${RESULT}" > $out
	exit 0
}

monitor(){                                                                 
        sleep 600                                                          
        RESETS=`cat /apps/resets`                                          
        let RESETS=RESETS+1                                                
        echo $RESETS > /apps/resets                                                                                        
        reboot                                                             
}       

RESULT="501 NOT IMPLEMENTED"
BOARD_INFO_FILE=/tmp/dallasci-raw
RETRY=10
while [ ! -e "${BOARD_INFO_FILE}" -a ${RETRY} -gt 0 ] ; do
	# need to wait a bit if snmp commands start before 
	# init is done
	sleep 3
	let RETRY=$RETRY-1
done
echo 1 >/sys/class/graphics/fb0/blank
# for hotroom dim the backlight a bit
if [ $2 -eq 4 ] ; then
	BACKLIGHT_DEVICE="/sys/class/backlight/$(ls /sys/class/backlight)"
	echo 32 > $BACKLIGHT_DEVICE/brightness
	PID=`cat /tmp/monitor.pid` 
        kill -9 $PID                                                  
        PID=`ps | grep "sleep 300" | grep -v grep | awk '{print $1}'`
        kill -9 $PID              
        monitor & 
        echo $! > /tmp/monitor.pid
fi
killapp
killxreset
# removed because different for EBS/Arriva BC and avoid 2nd 5 seconds delay
#STOP_APP=/apps/mp/stop.sh
#S99APP=/etc/rc5.d/S99mobileplatform
#if [ -e "${STOP_APP}" -a -e "${S99APP}" ] ; then
#	echo "stopping the java app"
#	${STOP_APP}
#	sleep 5
#fi
if [ -e "${BOARD_INFO_FILE}" ]
then
   BOARD_INFO=$(grep product ${BOARD_INFO_FILE} 2>/dev/null )
   if [ -n "${BOARD_INFO}" ] && [ "${BOARD_INFO:0:4}" != "grep" ]
   then
         SERNR=$(echo ${BOARD_INFO} | cut -d' ' -f2 ) 
         RESULT="200 S/N=${SERNR}"
   else
      RESULT="500 NO S/N FOUND" 
   fi
else
      RESULT="500 NO S/N FOUND" 
fi

result


