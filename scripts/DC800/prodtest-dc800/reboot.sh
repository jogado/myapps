#!/bin/sh
#
# reboot
#
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

rebootme()
{
sleep 10
reboot
}

RESULT=`echo "200 REBOOT:$3 "`
echo "${RESULT}" > $out
rebootme &
sync
exit 0
