#!/bin/sh
#
# Analog in test 
#
EXE=/usr/bin/test-stm32
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
	case $2 in 
	*) 
        	res=`$EXE -a`
		err=`echo $res | grep Error`
		if [ -z "$err" ] ; then
			res=`echo $res |cut -d' ' -f4`
			if [ "$res" -gt "23000" ] ; then
				RESULT=`echo 200 Analog in OK: $res mV`
			else
				RESULT=`echo 530 Analog in NOK \(\< 23V\): $res mV`
			fi
		else
			RESULT=`echo "530 " $err`
		fi			
	;;
	esac
fi

result


