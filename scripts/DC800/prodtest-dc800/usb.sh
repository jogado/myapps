#!/bin/sh
#
# lsusb related tests 
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

ARCH=$(uname -m)
case $3 in
1)
	count=$4
        search=`lsusb | grep -c Bus`

        if [ $search -eq $count ] ; then
		RESULT="200 $4 USB devices detected"  
		result
        else
		RESULT="500 USB devices modules not detected properly $search instead of $count"  
		result
        fi
        ;;
*)
        search=`lsusb | grep -c Bus`
	RESULT="200 $search USB devices detected"  
	result
	;;
	
esac
RESULT="500 Wrong Test ID: $3"  
result


