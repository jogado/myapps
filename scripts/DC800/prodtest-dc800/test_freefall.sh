#!/bin/sh

MSG="Please move the device to generate a shock"
out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

run_shocksensor_test()
{
	DISPLAY=:0 screen-test -m "$MSG" -t 10 &
	pid_msg=$!

	read -n 1 -t 10 n </dev/freefall
	val=$(echo -n ${n:0:1} | od -i | head -n1 | awk '{print $2}')

	if [ "$val" ]; then
		RESULT="200 Shocksensor test succeeded"
	else
		RESULT="520 Shocksensor test failed"
	fi

	kill $pid_msg
}

run_test()
{
	startX

	run_shocksensor_test

	stopX
}

case $2 in
	0 ) # check if driver is loaded correctly
	if [ -a /sys/devices/platform/lis3lv02d/position ] ; then
		RESULT="200 Shock sensor detected"
	else
		RESULT="500 No Shock sensor detected"
	fi
	;;
	* ) 
	run_test
	;;
esac
result
