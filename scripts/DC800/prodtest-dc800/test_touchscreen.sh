#!/bin/sh
#
# Test the touchscreeen
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

startX()
{
	echo 0 >/sys/class/graphics/fb0/blank
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

detect_touchscreen()
{
	i2cbus=0
	if [ -d '/sys/class/i2c-dev/i2c-1/device/1-0038' ]; then
		echo 'edt-ft5x06'
		i2cbus=1
	elif [ -d '/sys/class/i2c-dev/i2c-1/device/1-0041' ]; then
		echo 'ili210x'
		i2cbus=1
	elif [ -d '/sys/class/i2c-dev/i2c-0/device/0-0038' ]; then
		echo 'edt-ft5x06'
	elif [ -d '/sys/class/i2c-dev/i2c-0/device/0-0041' ]; then
		echo 'ili210x'
	else
		echo 'unknown'
	fi
	return ${i2cbus}
}

run_touchscreen_test()
{
	DISPLAY=:0 /usr/bin/touchscreen-test -c $1 -r $2
	if [ $? -eq 0 ] ; then
		RESULT="200 Touchscreen test succeeded"
	else
		if [ $3 -eq 4 ] ; then
			RESULT="200 Touchscreen test finished"
		else
			RESULT="520 Touchscreen test failed"
		fi
	fi
}

run_test()
{
	modprobe -a ili210x edt-ft5x06

	model=$(detect_touchscreen)
	i2cbus=$?

	startX
	case "$model" in
		edt-ft5x06)
			run_touchscreen_test 3 4 $1
			;;
		ili210x)
			echo 1 >/sys/class/i2c-dev/i2c-${i2cbus}/device/${i2cbus}-0041/calibrate
			sleep 5
			run_touchscreen_test 5 5 $1
			;;
		*)
			RESULT="500 No touchscreen found"
			;;
	esac
#Voluntary taken out to have X running for succeeding tests
#	stopX
}

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
else 
	run_test $2
fi
result
