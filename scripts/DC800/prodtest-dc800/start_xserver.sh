#!/bin/sh
#
# Start x-server
# 1st parameter = screen number
# return: 0= OK, did not need to start it
#         1= OK, started it
#         2= not OK, X-server start failed
#


# return which screen is active
is_screen()
{
# Unfortunately, doesn't work in V
#	echo `xrandr 2>&1 | grep Screen | cut -d' ' -f2 | sed 's/://g'`
	echo `xdpyinfo 2>&1 | grep -e "name of display" | sed 's/.*\.//g'`
}

# startX start X-server, default on :0.0, optional :0.$1
	scr=0
	if [ $# -gt 0 ] ; then
		scr=$1
	fi
	export DISPLAY=:0.$scr
	screen=`is_screen`
	pid=`pidof Xorg`
	if [ "$pid" != "" ] ; then
		noreset=`cat /proc/$pid/cmdline | grep "noreset"`
	fi
# We want the X to be started with the -noreset option
	if [ "$screen" != "$scr" -o "$noreset" == ""  ] ; then
# Doesn't work when X is started via link, named X
		if [ "$pid" != "" ] ; then
			kill $pid
			let i=0
			while [ -e "/tmp/.X0-lock" -a $i -lt 10 ] ; do
				sleep 1
				let i=i+1
			done
		fi
		/usr/bin/Xorg :0 -br -pn -noreset > /dev/null 2>&1 &
		let i=0
		while [ $i -lt 10 ] ; do
			sleep 1
			screen=`is_screen`
			if [ "$screen" == "$scr" ] ; then
				break
			fi
			let i=i+1
		done
		if [ "$screen" == "$scr" ] ; then
			let r=1
		else
			let r=2
		fi
	else
		let r=0
	fi
	exit $r
