#!/bin/sh
#
# test if external dallas is detected correctly 
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

case $2 in
2 | 4)
	   DALLAS_SN=$(dallas-info -d /dev/stm0 -S | grep "Ext" | wc -l )
	   if [ "${DALLAS_SN}" -ne 0 ]
	   then
		RESULT="200 External Dallas detected"
	   else
		RESULT="520 No External Dallas"
	   fi
;;
1)
	case $3 in 
	0)
	   DALLAS_SN=$(dallas-info -d /dev/stm0 -S | grep "Ext" | cut -d' ' -f2)
	   if [ -n "${DALLAS_SN}" ]
	   then
		RESULT="200 External Dallas serials: "
      		for SERNR in ${DALLAS_SN}
      		do
         	RESULT=${RESULT}" ${SERNR}"
      		done
	   else
		RESULT="520 No Ext Dallas"
	   fi
	;;
	1)
	   DALLAS_SN=$(dallas-info -d /dev/stm0 -S | grep "Int" | cut -d' ' -f2)
	   if [ -n "${DALLAS_SN}" ]
	   then
		RESULT="200 Internal Dallas serials: "
      		for SERNR in ${DALLAS_SN}
      		do
         	RESULT=${RESULT}" ${SERNR}"
      		done
	   else
		RESULT="520 No Ext Dallas"
	   fi
	;;
	esac
;;
esac
result


