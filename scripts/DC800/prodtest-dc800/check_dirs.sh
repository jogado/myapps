#!/bin/sh
result()
{
        echo $RESULT > $out
        exit 0
}
RESULT="200 All files match"
comp_dirs()
{
	for F in `find $1 -type f` ; do
		F2=`echo $F | sed -e "s/a/b/"`
		MD1=`md5sum $F | cut -d' ' -f1`
		MD2=`md5sum $F2 | cut -d' ' -f1`
		if [ -z ${MD2} ] ; then 
			RESULT="500 file $F2 does not exist"
		fi
		if [ ${MD2} != ${MD1} ] ; then 
			RESULT="500 file $F2 is different" 
		fi
	done
}
# $1 contains the output file (/dev/stdio, /home/root/test, ...)
out=$1
if [ -z $1 ] ; then
        out="/dev/stdout"
fi
#setup_hotroom_network creates 50 files in /tmp/a and /tmp/b
while [ ! -e /tmp/b/50 -o ! -e /tmp/b/1 ] ; do sleep 5 ; done
comp_dirs /tmp/a
# compare data between 2 partitions
ln -sf /usr/lib/jvm /tmp/ulja
mkdir /tmp/sda2
mount -o ro /dev/sda2 /tmp/sda2
ln -sf /tmp/sda2/usr/lib/jvm /tmp/uljb
comp_dirs /tmp/ula/
umount /dev/sda2
result
