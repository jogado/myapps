#!/bin/sh
#
# Test the backlight
#
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then                                      
   out="/dev/stdout"                                            
else                                                            
   out="$1"                                                     
fi 

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	 echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

detect_backlight()
{
	if [ ! -d /sys/class/backlight ]; then
		return 1
	fi

	BACKLIGHT_DEVICE="/sys/class/backlight/$(ls /sys/class/backlight)"
	echo "$BACKLIGHT_DEVICE"

	return 0
}

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

run_screen_test()
{
	BACKLIGHT_DEVICE=$1
	MAX_BRIGHTNESS=$(cat "$BACKLIGHT_DEVICE/max_brightness")

	if [ $MAX_BRIGHTNESS -ge 100 ]; then
		SLEEP=10000
	else
		SLEEP=100000
	fi
	export DISPLAY=:0
	/usr/bin/screen-test -m "Decreasing backlight intensity" -b 0000ff -t 100 &
	echo $! >/tmp/screen-test.$$.pid
	for i in `seq 0 $MAX_BRIGHTNESS`; do
		echo $(($MAX_BRIGHTNESS-$i)) >$BACKLIGHT_DEVICE/brightness
		usleep $SLEEP
	done
	kill $(cat /tmp/screen-test.$$.pid)

	/usr/bin/screen-test -m "Increasing backlight intensity" -b 0000ff -t 100 &
	echo $! >/tmp/screen-test.$$.pid
	for i in `seq 0 $MAX_BRIGHTNESS`; do
		echo $i >$BACKLIGHT_DEVICE/brightness
		usleep $SLEEP
	done
	kill $(cat /tmp/screen-test.$$.pid)
	rm /tmp/screen-test.$$.pid
}

run_test()
{
	BACKLIGHT_DEVICE=$(detect_backlight)
	if [ "$BACKLIGHT_DEVICE" == "" ]; then
		RESULT="500 No backlight interface found"
		result
	fi

	CURRENT_BRIGHTNESS=$(cat "$BACKLIGHT_DEVICE/brightness")

	startX

	run_screen_test $BACKLIGHT_DEVICE

	RESULT=`${CONFIRM} "backlight decrease/increase" $1`
	stopX

	echo "$CURRENT_BRIGHTNESS" >$BACKLIGHT_DEVICE/brightness

}

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
else 
	run_test $2
fi
result
