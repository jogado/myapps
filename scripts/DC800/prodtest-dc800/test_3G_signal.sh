#!/bin/sh
#
# Test 3G signal quality (shoud be smaller than 16) 
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

RESULT="501 NOT IMPLEMENTED"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e /tmp/has_modem ] ; then
        RESULT="200 Device has no 3g modem"
        result
fi

RETRIES=3
while [ $RETRIES -gt 0 ] ; do
	if [ ! -e "/tmp/modemcsq" ]
	then
		RESULT="500 Signal quality not determined"
	else
		ERROR=$(grep ERROR /tmp/modemcsq)
		if [ -z "${ERROR}" ]
		then
		   DATA="$(cat /tmp/modemcsq)"
		else
		   DATA="-1"
		fi

		if [ $DATA -gt 7 -a $DATA -lt 99 ]
		then 
			RESULT="200 signal quality=${DATA}"
			break
		else
			RESULT="500 signal quality=${DATA} too low - check antenna connection"
		fi
	fi
	/etc/init.d/gprs-daemon stop
	sleep 4
	/etc/init.d/modem-3G_info start
	RETRIES=$((RETRIES-1))
	sleep 1
done
result


