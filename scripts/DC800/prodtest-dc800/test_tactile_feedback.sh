#!/bin/sh
#
# Test the tactile feedback
#
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
TTPATH="/sys/devices/platform/imx-i2c.0/i2c-0/0-0011"

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

run_test()
{
	echo 1 >$TTPATH/enable

	for i in `/usr/bin/seq 10`; do
		echo 1 >$TTPATH/vibrate_effect
	done

	echo 0 >$TTPATH/enable
	RESULT=`${CONFIRM} "Tactile feedback" $1`
}


run_test $2
result
