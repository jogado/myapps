#!/bin/sh
#
# return number of PCB found in device
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

BOARD_INFO=/etc/ci/dallas_boards

if [ -e "${BOARD_INFO}" ]
then
   BOARD_NUM=$(grep "BOARD=" ${BOARD_INFO} | wc -l)
   if [ -n "${BOARD_NUM}" ]
   then
      case $2 in
	1)
	   RESULT="200 #PCB BOARDS=${BOARD_NUM}"
	;;
	2 | 4 | 8)
	   if [ -n "$3" ] ; then
	   if [ $3 -eq ${BOARD_NUM} ] ; then
		RESULT="200 #PCB BOARDS=${BOARD_NUM}"
	   else
		RESULT="520 Incorrect #PCB BOARDS=${BOARD_NUM} i.s.o. $3"
	   fi
	   fi
	;;
	16 | 32 | 64 | 128)
	   if [ ${BOARD_NUM} -eq 1 ] ; then
		RESULT="200 #PCB BOARDS=${BOARD_NUM}"
	   else
		RESULT="520 Incorrect #PCB BOARDS=${BOARD_NUM} i.s.o. 1"
	   fi
	;;
	esac
   else
      RESULT="500 UNABLE TO ENUMERATE BOARDS" 
   fi
fi

result


