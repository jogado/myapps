#!/bin/sh
result()
{
        echo $RESULT > $out
        exit 0
}
# $1 contains the output file (/dev/stdio, /home/root/test, ...)
out=$1
if [ -z $1 ] ; then
        out="/dev/stdout"
fi
RESULT="200 fsck OK on all partitions"
fsck -nf /dev/sda2
if [ $? -ne 0 ] ; then 
RESULT="500 fsck error on sda2"
fi
fsck -nf /dev/sda3
if [ $? -ne 0 ] ; then 
RESULT="500 fsck error on sda3"
fi
if [ -f /home/root/fsck.part ] ; then
MSG=`cat /home/root/fsck.part`
RESULT="500 ${MSG}"
rm /home/root/fsck.part
fi
if [ -f /home/root/fsck.root ] ; then
MSG=`cat /home/root/fsck.root`
RESULT="500 ${MSG}"
rm /home/root/fsck.root
fi
result
