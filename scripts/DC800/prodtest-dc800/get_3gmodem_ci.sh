#!/bin/sh
#
# return 3G modem serial number and type
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e /tmp/has_modem ] ; then 
	RESULT="200 Device has no 3g modem"
	result
fi
RETRIES=3
while [ $RETRIES -gt 0 ] ; do
	if [ ! -e "/tmp/modem3gci" ]
	then
		/etc/init.d/gprs-daemon stop
		sleep 3
		rm /dev/gprs1
		ln -s ./ttyUSB3 /dev/gprs1
		/etc/init.d/modem-3G_info start
		rm /dev/gprs1
		RESULT="500 3G MODEM NOT FOUND"
	else
		DATA="$(cat /tmp/modem3gci)"
        RESULT="200 ${DATA}"
        break
	fi
	RETRIES=$((RETRIES-1))
done

result


