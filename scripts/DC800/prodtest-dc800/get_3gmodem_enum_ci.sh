#!/bin/sh
#
# return number of WLAN modules on device
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e /tmp/has_modem ] ; then
        RESULT="200 Device has no 3g modem"
        result
fi

RESULT="501 NOT IMPLEMENTED"
MODULE_COUNT="0"

USB_LIST=$(lsusb | grep "[Oo]ption" | tr -s ' ' | tr ' ' ':')

if [ -z "USB_LIST" ]
then
        RESULT="501 NO 3G MODEM DEVICE FOUND"
        result
fi

for DEVICE in $USB_LIST
do
	MODULE_COUNT="$(expr $MODULE_COUNT \+ 1)"
done

RESULT="200 MODULES FOUND=${MODULE_COUNT}"

result


