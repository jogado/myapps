#!/bin/sh
#
# Test K2000 (COM2) to COM1 link 
#
EXE1=/usr/bin/test-k2000
EXE2=/usr/bin/k2000-emu

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE1  ] ; then
        RESULT=`echo "530 test program \"$EXE1\" not found"`
else
if [ ! -f $EXE2  ] ; then
        RESULT=`echo "530 test program \"$EXE2\" not found"`
else
	case $2 in 
	*) 
        	err=`$EXE2 -s "COM1-COM2 communication OK" | grep -E "Error|timeout"`
		if [ ! -z "$err" ] ; then
			RESULT=`echo 500 K2000-emu  $err`
		else
        		res=`$EXE1`
			err=`echo $res | grep Error`
			if [ ! -z "$err" ] ; then
				RESULT=`echo 500 read-K2000-disp  $err`
			else
				RESULT=`echo "200 " $res`
			fi
		fi			
	;;
	esac
fi
fi
result


