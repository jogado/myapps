#!/bin/sh

# Production test script
# sam: test available sams

X="x"
NAME="Test sams"
EXE="/usr/bin/tsam"

# Last tested sam index
maxsam=0

result()
{
	echo $RESULT >$out
	exit 0
}

startX()
{
	echo 0 >/sys/class/graphics/fb0/blank
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
}

stopX()
{
	echo 0 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

testsam()
{
	res=`tsam $1 $1 1 | grep Result | sed 's/.*Result //g' | cut -c 1`
	if [ "$res" == "1" ] ; then
		echo $1
	else
		echo ""
	fi
}

initscreen()
{
	if [ "$X" == "/dev/tty0" ] ; then
		printf "\033[2J\033[9;0]\033[0;0H" >/dev/tty0
	else
		startX		
	fi
}

info()
{
	temp=`echo -n $@ | sed 's/%/%%/g'`
	printf "\r\n$temp" >/dev/tty0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits

# $3: executed test

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi

#Run the sam test
if [ ! -e $EXE ] ; then
	RESULT=`echo "530 executable $EXE could not be found"`
	result
fi
initscreen
let c=1
if [ $# -gt 3 ] ; then
	let c=$4
fi
let tot=0
let ok=0

res=`tsam 0 $maxsam $c 1 0 x`
resok=`echo $res | grep Result | sed 's/.*Result //g'`
if [ "$resok" == "" ] ; then
	RESULT=`echo "530 $res"`
else
	perc=`echo $resok | sed 's/.*Tot //g'`
	let ok=`echo $perc | cut -d'/' -f 1`
	let tot=`echo $perc | cut -d'/' -f 2`
	if [ $ok -eq $tot ] ; then
		RESULT=`echo "200 ok=100%, $resok"`
		touch /tmp/samok
	else
		let pc=100*ok/tot
		if [ ! -e /tmp/samok ] ; then
			RESULT=`echo "200 No SAM Present"`
		else
			RESULT=`echo "530 SAM Error: ok=$pc%, $resok"`
		fi
	fi
fi

stopX
result 
