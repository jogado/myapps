#!/bin/sh
#
# Test passenger display 
#
EXE=/usr/bin/test-passenger-display
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
	case $3 in 
	0)# Black screen 
        	res=`$EXE -0`
		PATTERN="BLACK"
	;;
	1)# White screen
        	res=`$EXE -1`
		PATTERN="White"
	;;
	2)#DeLijn
        	res=`$EXE -f /usr/share/icons/DeLijn_small.bmp`
		PATTERN="De Lijn"
	;;
	3)#Prodata
        	res=`$EXE`
		PATTERN="PRODATA"
	;;
	esac
fi
err=`echo $res | grep Error`
if [ ! -z "$err" ] ; then 
	RESULT=`echo "500 PASSENGER DISPLAY " $err`
else
	RESULT=`${CONFIRM} "Passenger display message:<$PATTERN>" $2`
fi
if [ $2 -eq 4 ] ; then
	sleep 5;
      	res=`$EXE -0`
fi
result


