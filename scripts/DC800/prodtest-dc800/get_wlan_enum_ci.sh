#!/bin/sh
#
# return number of WLAN modules on device
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"
MODULE_COUNT="0"

PCI_LIST=$(lspci | grep Atheros | tr -s ' ' | tr ' ' ':')

if [ -z "PCI_LIST" ]
then
        RESULT="501 NO WLAN DEVICE FOUND"
        result
fi

for DEVICE in $PCI_LIST
do
	MODULE_COUNT="$(expr $MODULE_COUNT \+ 1)"
done

RESULT="200 MODULES FOUND=${MODULE_COUNT}"

result


