#!/bin/sh
#
# Test STM32 switch SMI interface. 
#
EXE=/usr/bin/test-stm32-switch
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
    echo "${RESULT}" > $out
    exit 0
}

if [ ! -f $EXE ] ; then
    RESULT=`echo "530 test program \""$EXE"\" not found"`
else
    res=`$EXE`
    err=`echo $res | grep Error`
    if [ ! -z "$err" ] ; then
        RESULT=`echo "530 STM32 switch SMI "$err`
    else
        RESULT=`echo "200 " $res`
    fi    		
fi

result

