#!/bin/sh
#
# return SIM slot, PIN, and serial number
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

RESULT="501 NOT IMPLEMENTED"
SIM_SLOT="0"
SIM_PIN=""

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e /tmp/has_modem ] ; then
        RESULT="200 Device has no 3g modem"
        result
fi

if [ ! -e "/tmp/simci" ]
then
   RESULT="500 SIM NOT FOUND"
   result
fi

ERROR=$(grep ERROR /tmp/simci)
if [ -z "${ERROR}" ]
then
   DATA="$(cat /tmp/simci)"
else
   DATA="N/A"
fi


if [ ! -e "/etc/sysconfig/gprs-daemon" ]
then
   RESULT="501 SIM INFO NOT FOUND"
   result
fi

SIM_PIN=$(grep SIM_PIN= /etc/sysconfig/gprs-daemon | tr -s ' ' | cut -d'=' -f2 | tr -d '"')

if [ -z "${SIM_PIN}" ]
then
   SIM_PIN="N/A"
fi

RESULT="200 simslot=${SIM_SLOT}:pin=${SIM_PIN}:simserial=${DATA}"

result


