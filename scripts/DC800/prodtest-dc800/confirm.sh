#!/bin/sh
#
# Ask test result confirmation 
# 1st parameter = test name
# 2nd = mode, 4=hotroom: don't ask confirmation
# 3rd = 0: kill X again if started, 1=let X on after this script
#



test="void"
mode=0
xon=0
timeout=10
if [ $# -gt 0 ] ; then
	test=$1
fi
if [ $# -gt 1 ] ; then
	mode=$2
fi
if [ $# -gt 2 ] ; then
	xon=$3
fi
if [ $# -gt 3 ] ; then
	timeout=$4
fi

if [ ${mode} -eq 4 ] ; then 
#hotroom
	RESULT="200 ${test} test finished"
else
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	let RESX=$?
        echo 0 >/sys/class/graphics/fb0/blank
	if [ $RESX -gt 1 ] ; then
		RESULT="530 ${test} test failed, X-server start problem"
	else
		if [ "${test}" != "" ] ; then
			export DISPLAY=:0.0
			/usr/bin/screen-test -q Yes,No -b 404040 -t $timeout -m "Is the ${test} test OK?"
			let RES=$?
		else
			if [ $RESX -lt 2 ] ; then
				let RES=0
			else
				let RES=1
			fi
			test="X-start $xon"
		fi
		if [ "$xon" != "1" ] && [ $RESX -eq 1 ] ; then
			pid=`pidof Xorg`
			kill $pid	
		fi
		if [ $RES -eq 0 ] ; then
			RESULT="200 ${test} test OK"
		else
			RESULT="530 ${test} test failed" 
		fi
	fi
        echo 1 >/sys/class/graphics/fb0/blank
fi
echo $RESULT
