#!/bin/sh
#
# Test STM32 DATAFLASH 
#
EXE=/usr/bin/test-stm32-dataflash
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
        for STM in `ls /dev/stm*` ; do
            res0=`$EXE -p ${STM}`
            res=`echo ${res} ${STM} ${res0}`
        done
        err=`echo $res | grep Error`
        if [ ! -z "$err" ] ; then
	    RESULT=`echo "530 STM32 DATAFLASH "$err`
        else
	    RESULT=`echo "200 " $res`
        fi    			
fi

result

