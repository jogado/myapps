#!/bin/sh
#
# Test the touchscreeen
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

startX()
{
        export DISPLAY=:0.0
        /usr/bin/production-test/testscripts/start_xserver.sh 0
        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
        fi
        echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}
run_test()
{
        let RES=1
	if [ ! -d /sys/bus/i2c/drivers/isl29003 ]; then
		RESULT="500 No isl29003 found"
		return
	fi

	echo 1 >/sys/class/i2c-adapter/i2c-0/0-0044/power_state
	sleep 1
	lux=$(cat /sys/class/i2c-adapter/i2c-0/0-0044/lux)
        DISPLAY=:0 /usr/bin/screen-test \
         -m "Cover light sensor" -b 404040 &
        let a=0
        while [ $a -lt 5 ] ; do
                pid=`pidof screen-test`
                if [ "$pid" != "" ] ; then
                        break
                fi
                sleep 1
                let a=a+1
        done
        let b=0
        let RES=1
        while [ $b -lt 5 ] ; do
		lux2=$(cat /sys/class/i2c-adapter/i2c-0/0-0044/lux)
		if [ $lux -gt 14 -a $lux2 -lt 14 -o $1 -eq 4 ]; then
			RESULT="200 Lightsensor test succeeded! Lux = $lux"
			break
		else
			RESULT="520 Lightsensor lux value too low or did not covered ($lux)"
		fi
                sleep 1
                let b=b+1
        done
	echo 0 >/sys/class/i2c-adapter/i2c-0/0-0044/power_state
}

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
        RESULT="530 ${warn_pcie}"
else
        startX
        run_test $2
        stopX
fi
result
