#!/bin/sh
#
# Test RGB and EMV Leds
#
EXE=/usr/bin/test-leds
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
	case $3 in 
	0)# V800 
        	res=`$EXE -p /dev/stm0 -1 -c 1 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm0 -1 -c 2 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm0 -1 -c 4 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm0 -1 -c 7 -s 20`
		if [ $2 -eq 4 ] ; then 
			sleep 2
        		res=`$EXE -p /dev/stm0 -1 -c 4 -s 15`
		fi
	;;
	1)# DC800 
        	res=`$EXE -p /dev/stm1 -1 -c 1 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm1 -1 -c 2 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm1 -1 -c 4 -s 15`
		sleep 2
        	res=`$EXE -p /dev/stm1 -1 -c 7 -s 20`
		if [ $2 -eq 4 ] ; then 
			sleep 2
        		res=`$EXE -p /dev/stm1 -1 -c 4 -s 15`
		fi
	;;
	2)# BC800 dlp113 leds 
        	res=`$EXE -p /dev/stm0 -1 -s 21`
		if [ $2 -eq 4 ] ; then 
			sleep 2
        		res=`$EXE -p /dev/stm0 -0 -s 21`
		fi
	;;
	esac
fi
err=`echo $res | grep -c Error`
if [ $err -ne 0 ] ; then 
	RESULT=`echo "500 LEDS " $res`
else if [ $3 -eq 2 ] ; then                          
        RESULT="311 Confirm LEDS"
else 
	RESULT=`${CONFIRM} "RGB and EMV Leds" $2`
fi 
fi
result


