#!/bin/sh
#
# SD tests 
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi
if [ "$#" -lt "2" ]
then
   MODE=1
else
   MODE=$2
fi
if [ "$#" -lt "3" ]
then
   COUNT=1
else
   COUNT=$3
fi
PROOF="/usr/share/misc/sdtested"
mkdir -p /usr/share/misc
result()
{
	echo "${RESULT}" > $out
	exit 0
}
search=`ls /dev/mmcblk* | grep -v p | grep -v -c boot`
if [ $search -eq $COUNT ] ; then
	RESULT="200 SD card detected"  
	if [ $MODE -eq 2 ] ; then
		# For BC800/810 the SD is tested in assembly test only, but don't
		# need to retest after hotroom (avoid having to open unit again) 
		touch $PROOF
	fi
else
	if [ -e $PROOF -o $MODE -eq 1 ] ; then
		RESULT="200 SD card not present" 
	else
		RESULT="500 SD card not detected" 
	fi
fi
result


