#!/bin/sh
#
# reads all ci information about the sata ssd 
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

SFDISK=$(which sfdisk)
HDPARM=$(which hdparm)
SSD="/dev/sda"

if [ ! -e "${SSD}" ] || [ -z "${SFDISK}" ] || [  -z "${HDPARM}" ]
then
	RESULT="500 Test not started due to missing tools"
	result
fi

SATAID=$(${HDPARM} -i ${SSD} | grep "^ Model" )
SATASIZE=$(${SFDISK} -s -uM ${SSD})

SATASN=$(echo ${SATAID} | cut -d',' -f3 | cut -d'=' -f2)
SATAMODEL=$(echo ${SATAID} | cut -d',' -f1 | cut -d'=' -f2)
if [ $2 -eq 4 ] ; then
	#short ssd test
	dd if=/dev/urandom of=/tmp/random bs=1024 count=1024
	cp /tmp/random /home/root/random 
	sync
	sleep 5
	diff /tmp/random /home/root/random 
	if [ $? -ne 0 ] ; then
		RESULT="530 SSD file compare error"
		result
	fi
fi
RESULT="200 SSD MODEL=${SATAMODEL}:S/N=${SATASN}:SIZE=${SATASIZE}MB"

result


