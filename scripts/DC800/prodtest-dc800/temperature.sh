#!/bin/sh
#
# Test Temperature sensor 
#
EXE=/usr/bin/temperature-read
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
	case $2 in 
	*) 
        	res=`$EXE`
		err=`echo $res | grep Error`
		if [ ! -z "$err" ] ; then
			RESULT=`echo 530 Temperature $err`
		else
			RESULT=`echo "200 " $res`
			CPU=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
			echo 110 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp
			if [ $CPU -gt 108 ] ; then 
				RESULT=`echo 530 $res, CPU Temp is High: $CPU C`
			else
				RESULT=`echo $RESULT, CPU Temp = $CPU C`
			fi
		fi			
	;;
	esac
fi

result


