#!/bin/sh
#
# return OTI serial number and slot
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e "/etc/sysconfig/otici" ]
then
   RESULT="500 UNABLE TO RETRIEVE DATA"
   result
fi

OTIDATA="$(cat /etc/sysconfig/otici)"

if [ "${OTIDATA:0:9}" != "oti_ioctl" ]
then
   RESULT="200 ${OTIDATA}"
else
   RESULT="200 NO OTI READER FOUND"
fi

result


