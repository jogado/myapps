#!/bin/sh
#
# Test STM32 RTC 
#
EXE=/usr/bin/test-stm32-rtc
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -f $EXE ] ; then
        RESULT=`echo "530 test program \"$EXE\" not found"`
else
        TIME=`$EXE -t 0 | cut -d' ' -f2`
	res=`$EXE -t 1`
        err=`echo $res | grep Error`
        if [ ! -z "$err" ] ; then
	    RESULT=`echo "530 STM32 RTC "$err`
	else
	    RESULT=`echo "200 " $res Date/Time: $TIME`
	fi			
	$EXE -t 3 -T $TIME
fi

result

