#!/bin/sh
#
# return number of PCB found in device
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"
BOARD_INFO_FILE=/etc/ci/dallas_boards
BOARD_ID="DLP137"

if [ -e "${BOARD_INFO_FILE}" ]
then
   BOARD_INFO=$(grep ${BOARD_ID} ${BOARD_INFO_FILE} 2>/dev/null | tr ' ' '_' )
   if [ -n "${BOARD_INFO}" ] && [ "${BOARD_INFO:0:4}" != "grep" ]
   then
         DESCR=$(echo ${BOARD_INFO} | cut -d':' -f1 | tr '_' ' ') 
         SERNR=$(echo ${BOARD_INFO} | cut -d':' -f2 | tr '_' ' ') 
         REV=$(echo ${BOARD_INFO} | cut -d':' -f3 | tr '_' ' ') 
         RESULT="200 BOARD=${DESCR} S/N=${SERNR} REV=${REV}"
   else
      RESULT="200 NO BOARD=${BOARD_ID} FOUND" 
   fi
fi

result


