#!/bin/sh
#
# Test 3G signal quality (shoud be smaller than 16) 
#
LEVEL=0

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

RESULT="501 NOT IMPLEMENTED"

result()
{
	echo "${RESULT}" > $out
	exit 0
}
testwlan()
{
	for i in `seq 5`; do
		
		SIGNALS=$(iwlist $1 scan | grep Signal | cut -d'-' -f2 | cut -d' ' -f1)
		if [ ! -z "$SIGNALS" ]; then
			for SIGNAL in $SIGNALS
			do 
				if [ $SIGNAL -lt $LEVEL ]
				then
					return 0
				fi 
			done
		else
			ifconfig $1 up
			sleep 2 
		fi
	done

	RESULT="500 Signal for $1 too small $SIGNALS"
	result
}

if [ -z $4 ] 
then
	LEVEL=70
else
	LEVEL=$4	
fi
case $3 in 
0 | 1)
	testwlan wlan$3
;;
2)
	testwlan wlani
	testwlan wlane
;;
3)
	testwlan wli
	testwlan wle
;;
4)
	testwlan wle
;;
esac 
RESULT="200 Wifi Signals ok"
result


