#!/bin/sh
#
# return SAM serial number and slot
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

if [ ! -e "/etc/sysconfig/samci" ]
then
   RESULT="500 UNABLE TO RETRIEVE DATA"
   result
fi

SAMDATA="$(cat /etc/sysconfig/samci)"

RESULT="200 ${SAMDATA}"

result


