#!/bin/sh

# Production test script
# bsc: test bsc configuration
# first parameter $1 indicates if driver unload is allowed: default is yes
DEOFF=/usr/bin/deoff
EXAR_CONF_FILE=/usr/share/exar-conf/exar-configuration.conf
MPIODIR="/sys/class/gpio"
MPIO="P5"
P5NR="244"
# NBSC_UNLOAD=0: do not unload bsc driver for test - skip this test
NBSC_UNLOAD="0"
if [ $# -gt 0 ] ; then
	NBSC_UNLOAD="$1"
fi

machine=`cat /etc/hostname | cut -c 1`
if [ "$machine" != "d" ] ; then
	exit 0
fi

verify_level_mpio4() {
	is_loaded=`lsmod | grep n_bsc`
	if [ "$is_loaded" != "" ] ; then
		if [ "$1" != "1" ] ; then
# return here without test - can't test!
			return;
		fi
		rmmod n_bsc.ko	
	fi
	if [ ! -e "${MPIODIR}/${MPIO}" ] ; then
		echo ${P5NR} > ${MPIODIR}/export
	fi
	if [ ! -e "${MPIODIR}/${MPIO}" ] ; then
		echo "Error: Could not find ${MPIODIR}/${MPIO}"
		exit 1
	fi
	if [ ! -e "${MPIODIR}/${MPIO}/direction" ] ; then
		echo "Error: Could not find ${MPIODIR}/${MPIO}/direction"
		exit 1
	fi
	direction=`cat ${MPIODIR}/${MPIO}/direction`
	if [ "$direction" != "out" ] ; then
		echo "Error: MPIO4 on $2 is no output - ?${direction}?"
		exit 1
	fi
	level=`cat ${MPIODIR}/${MPIO}/value`	
	if [ "$2" == "d_imx" -a "$level" == "0" ] ; then
		echo "Error: MPIO4 is not active"
		exit 1
	fi
	if [ "$2" == "d_atom" -a "$level" == "1" ] ; then
		echo "Error: MPIO4 is not active"
		exit 1
	fi
	return
}
# start by checking which device
processor=`uname -m`
if [ "$processor" == "armv7l" ] ; then
	machine=`echo ${machine}_imx`
else
	machine=`echo ${machine}_atom`
	P5NR=232
fi
# verify MPIO in configuration file
conf_level=`grep P5 $EXAR_CONF_FILE`
if [ "$machine" == "d_imx" -a "$conf_level" != "P5:0:0:1:-" ] ; then
	echo "Error: MPIO4 is not configured active on $machine : [$conf_level]"
	exit 1
fi
if [ "$machine" == "d_atom" -a "$conf_level" != "P5:0:0:0:-" ] ; then
	echo "Error: MPIO4 is not configured active on $machine : [$conf_level]"
	exit 1
fi
# verify actual MPIO level
verify_level_mpio4 $NBSC_UNLOAD $machine

deoff_mode=`grep deoff /etc/init.d/exar-init | grep -v "#.*deoff"`
if [ "$deoff_mode" == "" -o ! -x $DEOFF ] ; then
	rsmode=`grep U3 $EXAR_CONF_FILE | sed 's/.*RS//g'`
	if [ "$rsmode" != "485" ] ; then
		echo "Error: echo U3 not configured in rs485 [$rsmode]"
		exit 1
	fi
## retrieve rs485 string from /sys/class/exar/xrgpio/rs485
	device=3
	CONFIG=$(cat /sys/class/exar/xrgpio/rs485)
	let i=8-$device
	rs485mode=`echo $CONFIG | cut -c $i`
	if [ "$rs485mode" != "1" ] ; then
		echo Error: UART$device rs485 mode = $rs485mode
		exit 1
	fi
	CONFIG=$(cat /sys/class/exar/xrgpio/rts)
	let i=8-$device
	rtsmode=`echo $CONFIG | cut -c $i`
	if [ "$rtsmode" != "1" ] ; then
		echo Error: UART$device rtsmode = $rtsmode
		exit 1
	fi
fi
# Exit here without echo means config OK
exit 0
