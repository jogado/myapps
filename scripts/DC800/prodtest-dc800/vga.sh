#!/bin/sh
#
# return WLAN serial number,type and MAC
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}
i=0
col=`stty -a -F /dev/tty0 | grep col | awk '{print $7}'| sed -e "s/;//"`
row=`stty -a -F /dev/tty0 | grep col | awk '{print $5}'| sed -e "s/;//"`
while [ "$i" -le "$row" ] ; do
	let "a=$i%8"
	let "bg=40+$a"
	let "fg=37-$a"
	echo -e "\033[m" >/dev/tty0
	echo -n -e "\033[$bg""m" >/dev/tty0
	echo -n -e "\033[$fg""m" >/dev/tty0
	x=0
	while [ "$x" -lt "$col" ] ; do
		echo -n @ > /dev/tty0
		let x+=1
		done
	let i+=1
done
RESULT="311 CONFIRM TESTPATTERN"

result


