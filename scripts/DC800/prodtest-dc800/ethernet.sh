#!/bin/sh

# Production test script
# Ethernet


RESULT="501 parameter missing"

result()
{
        echo $RESULT > $out
	exit 0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
out=$1
if [ -z $1 ] ; then
	out="/dev/stdout"
fi


# $2: mode bits

# $3: executed test
case $3 in
1)
	# ping test
	# $4: IP address
        if [ -z $4 ] ; then
                RESULT="501 parameter missing"
	else
        	ping -c 1 -w 5 $4
	        if [ $? -eq 0 ] ; then 
			RESULT="200 ping to $4 succeeded"
		else
	                RESULT="425 ping failed"
		fi
        fi
        ;;
2)
        #MAC Address
        temp=`ifconfig eth0 |grep eth0 | awk '{print $5}'| sed -e 's/addr://'`
        if [ -z $temp ] ; then
		RESULT="425 ping failed"
        else
		RESULT="200 MAC address: $temp"
        fi
        ;;
3)
	RESULT="311 Put Ethernet cable in each connector and verify if both LEDS burn, Are all OK?"
	;;
esac                                                                          
result
