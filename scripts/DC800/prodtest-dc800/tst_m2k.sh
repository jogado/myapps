#!/bin/sh

X="x"
EXE="/usr/bin/tbsc"
NAME="Test m2000 interface rs485"

# Production test script
# m2k: test if m2000 comes online within 10 seconds and stays online 20 sec
# For this test, the dc800 should start as master


result()
{
        echo $RESULT > $out
	exit 0
}

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

initscreen()
{
	if [ "$X" == "0" ] ; then
		printf "\033[2J\033[9;0]\033[0;0H" >/dev/tty0
	else
		startX		
	fi
}

info()
{
	temp=`echo -n $@ | sed 's/%/%%/g'`
	printf "\r\n$temp" >/dev/tty0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits

# $3: executed test


if [ ! -e $EXE ] ; then
	RESULT=`echo "530 executable $EXE could not be found"`
	result
fi

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi
warn_bsc=`/usr/bin/production-test/testscripts/verif_bsc_conf.sh`
if [ "$warn_bsc" != "" ] ; then
	RESULT="530 ${warn_bsc}"
	result
fi
#Run the m2k test
initscreen
res=`tbsc -o $X | grep Result | sed 's/Result //g'`
perc=`echo $res | cut -d' ' -f 1`
if [ $perc -eq 100 ] ; then
	/usr/bin/screen-test -q "Yes,No" -t 10 -b 404040 -m "Was the 2nd M2000 eject OK?"
	confirm=$?
	if [ $confirm -eq 0 ] ; then
		RESULT=`echo "200 $res"`
	else
		bad=`echo $res | sed 's/100 %/50 %/g'`
		RESULT="echo 530 2nd eject(dlp135) - failed, $bad"
	fi
else
	RESULT=`echo "530 $res"`
fi

stopX
result 
