#!/bin/sh

X="x"
EXE=/usr/bin/toti
NAME="Test OTI reader"

# Production test script
# oti: test for type A and type B cards


# Maximum number of otis per device

killoti()
{
        sleep $1 
        killall -9 toti &> /dev/null
}

result()
{
        echo $RESULT > $out
}

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

initscreen()
{
	if [ "$X" == "0" ] ; then
		printf "\033[2J\033[9;0]\033[0;0H" >/dev/tty0
	else
		startX		
	fi
}

info()
{
	temp=`echo -n $@ | sed 's/%/%%/g'`
	printf "\r\n$temp" >/dev/tty0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits

# $3: executed test

#Run the oti test
initscreen
if [ ! -f $EXE ] ; then
	RESULT=`echo "530 test program \"$EXE\" not found"`
else
	TO=30
	if [ $2 -eq 4 ] ; then
		TO=10
	fi
	let TOPLUS=TO+20
	killoti $TOPLUS &
	res=`toti -o $X -t $TO | grep Result | sed 's/.*Result //g'`
	perc=`echo $res | cut -d'%' -f 1`
	if [ $perc -eq 100 -o $2 -eq 4 ] ; then
		RESULT=`echo "200 $res"`
	else
		RESULT=`echo "530 $res"`
	fi
fi

result 
stopX
exit 0                        
