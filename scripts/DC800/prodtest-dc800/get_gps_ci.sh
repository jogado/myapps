#!/bin/sh
#
# return GPS type
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

GPSHIPPO=$(which test-gps-hippo)

if [ -z "${GPSHIPPO}" ] 
then
        RESULT="500 Test not started due to missing tools"
        result
fi
INFO=$(${GPSHIPPO} -t 2)
MODEL=$(echo ${INFO} | tr -s " " | cut -d' ' -f 12)
SN=$(echo ${INFO} | cut -d' ' -f4)
if [ -n "$MODEL" ] ; then
	RESULT="200 GPS MODEL=${MODEL}:S/N=${SN}"
else
	RESULT="520 GPS ERROR"
fi

result
