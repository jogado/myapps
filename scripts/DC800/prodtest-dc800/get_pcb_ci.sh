#!/bin/sh
#
# return number of PCB found in device
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"
BOARD_INFO=/tmp/dallasci-raw
PREV_BOARD_INFO=/etc/sysconfig/dallasci-raw

if [ -e "${BOARD_INFO}" ]
then
   BOARD_LIST=$(grep board ${BOARD_INFO} | tr ' ' ':')
   if [ -n "${BOARD_LIST}" ]
   then
      if [ $2 -eq 4 ] ; then 
	      #HOTROOM
	      if [ -e "${PREV_BOARD_INFO}" ] ; then
		 PREV_MD5=$(md5sum ${PREV_BOARD_INFO} | cut -d' ' -f1)
		 MD5=$(md5sum ${BOARD_INFO} | cut -d' ' -f1)
		 if [ ! "${MD5}" = "${PREV_MD5}" ] ; then
			rm ${PREV_BOARD_INFO}
			RESULT="500 ERROR PCB SERIAL NUMBER CHANGED"
			result
		 fi
	      else
		 cp ${BOARD_INFO} ${PREV_BOARD_INFO}
	      fi
      fi
      RESULT="200 PCB SerialNumbers: "
      for BRD in ${BOARD_LIST}
      do
         SERNR=$(echo ${BRD} | cut -d':' -f5) 
         RESULT=${RESULT}" ${SERNR}"
      done
   else
      RESULT="500 UNABLE TO FIND BOARD INFORMATION" 
   fi
fi

result


