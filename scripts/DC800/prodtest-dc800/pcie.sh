#!/bin/sh
#
# lspci related tests 
#

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}
warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi
case $3 in
0)
        search=`lspci | grep -c Exar`
        if [ $search -ne 0 ] ; then
		RESULT="200 Exar Detected"  
		result
        else
		RESULT="500 Exar not detected"  
		result
        fi
        ;;
1)
	count=$4
	if [ -z $4 ] ; then
		count="2" 
	fi
        search=`lspci -vv | grep -c ath9k`
        if [ $search -eq $count ] ; then
		RESULT="200 $4 Atheros wifi modules detected"  
		result
        else
		RESULT="500 wifi modules not detected correctly $search"  
		result
        fi
        ;;
2)
	count=$4
	if [ -z $4 ] ; then
		case $3 in
		16 | 32)
			count=36
		;;
		*)
			count=36
		;;
		esac
	fi
        search=`lspci | grep -c rev`
        if [ $search -eq $count ] ; then
		RESULT="200  correct amount of pcie devices detected"  
		result
        else
		RESULT="500 incorrect amount of pcie  $search"  
		result
        fi
        ;;
esac
RESULT="500 Wrong Test ID: $3"  
result


