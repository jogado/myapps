#!/bin/sh

# Production test script
# icc: test external icc reader

X="x"
NAME="Test icc reader"
EXE="/usr/bin/tsam"

testicc()
{
	res=`tsam 4 4 1 261 0 x | grep Result | sed 's/.*Result //g' | cut -c 1`
	if [ "$res" == "1" ] ; then
		echo $1
	else
		echo ""
	fi
}

result()
{
        echo $RESULT > $out
	exit 0
}

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof X`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}

initscreen()
{
	if [ "$X" == "0" ] ; then
		printf "\033[2J\033[9;0]\033[0;0H" >/dev/tty0
	else
		startX		
	fi
}

info()
{
	temp=`echo -n $@ | sed 's/%/%%/g'`
	printf "\r\n$temp" >/dev/tty0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits

# $3: executed test

#Run the icc test
warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi
if [ ! -e $EXE ] ; then
	RESULT=`echo "530 executable $EXE could not be found"`
	result
fi
initscreen
let c=1
if [ $# -gt 3 ] ; then
	let c=$4
fi
let n=0
let tot=0
let fault=0
let ok=0
let i=4
while [ $n -lt $c ] ; do
	res=`testicc $i`
	if [ "$res" == "$i" ] ; then
		let ok=ok+1
	else
		let fault=fault+1
	fi
	let tot=tot+1
	let n=n+1
done
let perc=100*ok/tot
if [ $perc -eq 100 ] ; then
	RESULT=`echo "200 ok=$perc% $ok/$tot"`
else
	let dec=1000*ok/tot-10*$perc
	RESULT=`echo "530 ok=$perc.$dec% $ok/$tot"`
fi

stopX
result 
