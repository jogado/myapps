#!/bin/sh
#
# Test the screen
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}
run_screen_test()
{
	if [ -z "$(echo $1 | grep '^[0-9]$')" ]; then
		RESULT="500 Invalid screen number"
		return
	fi

	export DISPLAY=:0.$1
	let RES=1
	if [ "$1" == "0" ] ; then
		/usr/bin/screen-test -t 10 -p2 -m "Are the colors OK?" -b 404040 -q Yes,No
		let RES=$?
	else
		DISPLAY=:0.$1 /usr/bin/screen-test \
	 -m "Are the colors OK? (confirm on touch screen)" -b 404040 -p 2 &
		let a=0
		while [ $a -lt 5 ] ; do
			pid=`pidof screen-test`
			if [ "$pid" != "" ] ; then
				break
			fi
			sleep 1
			let a=a+1
		done
		if [ "$pid" != "" ] ; then
			DISPLAY=:0.0 /usr/bin/screen-test -t 10 -m "Are the colors on the HDMI screen OK?" -q Yes,No -b 404040
			let RES=$?
			kill $pid
		fi
	fi		
	if [ "$2" == "4" ] ; then
		RESULT="200 Screen test finished"
	else
		if [ $RES -eq 0 ] ; then
			RESULT="200 Screen test OK"
		else
			RESULT="530 Screen test failed"
		fi
	fi
}

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
else 
	startX
	run_screen_test $3 $2
	stopX
fi
result
