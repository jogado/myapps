#!/bin/sh
#
# return WLAN serial number,type and MAC
#

IFID=""

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"
RESULTLIST="200 "

PCI_LIST=$(lspci | grep Atheros | tr -s ' ' | tr ' ' ':')

if [ -z "PCI_LIST" ]
then
	RESULT="501 NO WLAN DEVICE FOUND"
	result
fi

IFNAME=""
IFID_SYS=""
LOOP_COUNT="0"

for DEVICE in $PCI_LIST
do
        LOOP_COUNT=$(expr ${LOOP_COUNT} + 1)
	BUS_ID="$(echo ${DEVICE} | cut -d':' -f1-2)"
	IFNAME_LIST=$(ls /sys/bus/pci/devices/0000:${BUS_ID}/net/)

	for IF_DEVICE in ${IFNAME_LIST}
	do
		IF_IDSYS=$(echo ${IF_DEVICE} | tr -d [:alpha:])
		IF_NAME=$(echo ${IF_DEVICE} | tr -d [:digit:])

		if [ "${IF_NAME}" == "wli" ] || [ "${IF_NAME}" == "wle" ] || [ "${IF_NAME}" == "wlan" ]
		then
			IFNAME="${IF_DEVICE}"
			IFID_SYS="${IF_IDSYS}"
			break 
		fi
	done	

	MAC=$(cat /sys/bus/pci/devices/0000:${BUS_ID}/net/${IFNAME}/address | tr -d ':')
	DEVNAME=$(cat /sys/bus/pci/devices/0000:${BUS_ID}/net/${IFNAME}/device/device | cut -d ':' -f2)
        VENDOR_ID="$(dmesg | grep -m 1 Atheros | cut -d ' ' -f3,4)"
	BUS_ID_NAME="$(echo ${BUS_ID} | tr ':' '.')"
	
	if [ -n "$IFID" ] 
	then
		if [ "${IFID}" == "${IFID_SYS}" ]
		then
			RESULT="200 VENDOR=${VENDOR_ID}:S/N=${DEVNAME}-${MAC}:IFNAME=${IFNAME}"
			result
		fi
	else
                if [ "${LOOP_COUNT}" -gt "1" ]
                then
                   RESULTLIST="${RESULTLIST}|"
                fi
		RESULTLIST="${RESULTLIST} VENDOR=${VENDOR_ID}:S/N=${DEVNAME}-${MAC}:IFNAME=${IFNAME}:BUS=${BUS_ID_NAME} "
	fi
done

if [ -n "$IFID" ]
then
	RESULT="501 NO WLAN DEVICE FOUND"
else
	RESULT=${RESULTLIST}
fi
result

