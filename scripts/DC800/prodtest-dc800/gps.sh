#!/bin/sh
#
# test GPS fix 
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

GPSHIPPO=$(which test-gps-hippo)

if [ -z "${GPSHIPPO}" ] 
then
        RESULT="500 Test not started due to missing tools"
        result
fi
VALID=$(${GPSHIPPO} -t 0 | grep position_status | cut -d' ' -f 5)
FIX=$(${GPSHIPPO} -t 0 | grep latitude)
if [ "$VALID" = "valid" ] ; then
	RESULT="200 GPS FIX=${FIX}"
else
	if [ $(${GPSHIPPO} -t 3 | cut -d' ' -f 13) != "normal" ] ; then
		RESULT="520 GPS Antenna ERROR"
	else
		RESULT="520 GPS ERROR: No Fix"
	fi
fi

result
