#!/bin/sh
#
# return number of PCB found in device
#

out="$1"

USB_ID_0=""
USB_ID_1=""

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"
USB_LIST=$(lsusb | grep "ID 0483" | tr ' ' '_' )

if [ -n "${USB_LIST}" ]
then
   for USB_DEV in ${USB_LIST}
      do
         USB_ID=$(echo ${USB_DEV} | cut -d '_' -f6 | cut -d':' -f2 )
         USB_ID=${USB_ID:3:1}
        
         if [ -n "${USB_ID}" ] && [ "${USB_ID}" -eq "0" ]
         then
            USB_ID_0="${USB_ID}"
         fi
         if [ -n "${USB_ID}" ] && [ "${USB_ID}" -eq "1" ]
         then
            USB_ID_1="${USB_ID}"
         fi
      done
fi

if [ -n "${USB_ID_0}" ] && [ -n "${USB_ID_1}" ]
then
   RESULT="200 FOUND 2 DISTINCT STM32 IMAGES: STM${USB_ID_0} STM${USB_ID_1}"
else
   RESULT="500 ERROR: Only STM${USB_ID_0}${USB_ID_1} Found"
fi

result


