#!/bin/sh

if [ "$#" -lt "1" ]
then
	out="/dev/stdout"
else
	out="$1"
fi

RESULT="501 NOT IMPLEMENTED"

result()
{
        echo "${RESULT}" > $out
        exit 0
}

run_test()
{
	if [ -d /sys/class/net/can2 ] ; then
		KVASER=can2
	else
		KVASER=can1
	fi
	ip link set can0 down
	ip link set $KVASER down

	ip link set can0 up type can bitrate 125000
	ip link set $KVASER up type can bitrate 125000

	sleep 2

	canecho $KVASER 2>/dev/null &
	echo $! >/var/run/canecho.pid

	sleep 1

	for msg in "123#11" "123#1122" "123#112233" "123#11223344" \
			"123#1122334455" "123#112233445566" \
			"123#11223344556677" "123#1122334455667788"; do
		if ! test-can can0 $msg; then
			RESULT="520 CAN test failed (msg: $msg)"
			kill -9 $(cat /var/run/canecho.pid)
			ip link set can0 down
			ip link set $KVASER down
			return
		fi
	done

	kill -9 $(cat /var/run/canecho.pid)
	ip link set can0 down
	ip link set $KVASER down

	RESULT="200 CAN test succeeded"
}

run_test
result
