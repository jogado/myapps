#!/bin/sh
#
# Loopback tests of different GPIOs 
#
out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}
testgpio()
{
    if [ -z $1 -o -z $2 ] ; then
		RESULT="500 Missing Parameters"
		result
	fi
	outp=$1
	in1=$2
	echo 1 > /sys/devices/virtual/gpio/P$outp/value
	status1=`cat /sys/devices/virtual/gpio/P$in1/value`
	echo 0 > /sys/devices/virtual/gpio/P$outp/value
	status2=`cat /sys/devices/virtual/gpio/P$in1/value`
	if [ "$status1" -eq "$status2" ] ; then
		return 1
	fi
	return 0 
}
warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
	result
fi

case $3 in 
0)
	testgpio 1 9
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output1 toggle, toggles input 1"
	else
		RESULT="520 Error: GPIO output1 toggle does not toggles input 1"
	fi
	result
;;
1)
	testgpio 1 10
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output1 toggle, toggles input 2"
	else
		RESULT="520 Error: GPIO output1 toggle does not toggles input 2"
	fi
	result
;;
2)
	testgpio 2 11
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output2 toggle, toggles input 3"
	else
		RESULT="520 Error: GPIO output2 toggle does not toggles input 3"
	fi
	result
;;
3)
	testgpio 2 12
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output2 toggle, toggles input 4"
	else
		RESULT="520 Error: GPIO output2 toggle does not toggles input 4"
	fi
	result
;;
5)
	testgpio 3 14
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output3 toggle, toggles input 6"
	else
		RESULT="520 Error: GPIO output3 toggle does not toggles input 6"
	fi
	result
;;
6)
	testgpio 4 15
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output4 toggle, toggles input 7"
	else
		RESULT="520 Error: GPIO output4 toggle does not toggles input 7"
	fi
	result
;;
7)
	testgpio 5 16
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output5 toggle, toggles input 8"
	else
		RESULT="520 Error: GPIO output5 toggle does not toggles input 16"
	fi
	result
;;
10)	
	case $2 in 
	2)
		#bc800/bc810 integration test
		pairs="1,9 1,10 2,11 2,12 3,14 4,15 5,16"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
11)	
	case $2 in 
	2)
		#dc800 integration test
		pairs="2,9 2,10 3,11 3,12 4,13"
	;;
	32)
		#dlp131 test
		pairs="1,9 2,10 3,11 4,12 4,13"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
12)	
	case $2 in 
	2 | 128)
		#v800 integration/dlp150 test
		pairs="1,9 2,10 3,9 4,10"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
13)	
	case $2 in 
	2)
		#dc802 integration test (todo FW/TACHO via STM)
		pairs="2,9 2,10 3,11 3,12"
	;;
	32)
		#dlp131 test
		pairs="1,9 2,10 3,11 4,12 4,13"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
esac
RESULT="500 Wrong Test ID: $3"
result
