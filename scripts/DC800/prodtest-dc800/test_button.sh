#!/bin/sh
#
# Test the button on left side of dc (above hdmi/usb connectors)
#
BUTTON="/sys/class/gpio/P7"

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}
run_button_test()
{
	let RES=1
	transitions=`cat $BUTTON/transitions`
	DISPLAY=:0 /usr/bin/screen-test \
	 -m "Press button on left side" -b 404040 &
	let a=0
	while [ $a -lt 5 ] ; do
		pid=`pidof screen-test`
		if [ "$pid" != "" ] ; then
			break
		fi
		sleep 1
		let a=a+1
	done
	let b=0
	let RES=1
	while [ $b -lt 5 ] ; do
		transitions_now=`cat $BUTTON/transitions`
		if [ "$transitions" != "$transitions_now" ] ; then
			let RES=0
			break
		fi
		sleep 1
		let b=b+1
	done
	
	if [ "$1" == "4" ] ; then
		RESULT="200 Button test finished"
	else
		if [ $RES -eq 0 ] ; then
			RESULT="200 Button test OK"
		else
			RESULT="530 Button test failed"
		fi
	fi
}

warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
if [ "$warn_pcie" != "" ] ; then
	RESULT="530 ${warn_pcie}"
else 
	startX
	run_button_test $2
	stopX
fi
result
