#!/bin/sh
show_progress(){
        /usr/bin/production-test/testscripts/start_xserver.sh 0
        echo 0 >/sys/class/graphics/fb0/blank
        export DISPLAY=:0.0
        while [ 1 ] ; do
                TAIL=`tail  /tmp/$SN.log`
                /usr/bin/screen-test -b 404040 -t 1 -m $TAIL
        done
}

FTP_DETAILS=/tmp/ftp_details
MANIFEST=/tmp/manifest
check_upgrade()
{
        if [ -e $FTP_DETAILS -a ! -e $MANIFEST ] ; then
                source $FTP_DETAILS
		# monitor.pid is a watchdog created during hotroom
                PID=`cat /tmp/monitor.pid`
                kill -9 $PID
                SN=`cat /tmp/dallasci-raw | grep product | cut -d' ' -f2`
                echo Starting Upgrade $SN > /tmp/$SN.log
                show_progress &
                echo $! > /tmp/upgrade.$$.pid
                wget ftp://$USER:$PASSW@$FTPIP:21/$FTPDIR/manifest -O $MANIFEST
                ipkdm --debug=1 --dest=/tmp --install=1 --list=$MANIFEST --url=ftp://$USER:$PASSW@$FTPIP:21/$FTPDIR &> /tmp/$SN.log
                lftp -e "put /tmp/$SN.log" -u $USER,$PASSW ftp://$FTPIP:21/$LOGS/
                PID=`cat /tmp/upgrade.$$.pid`
                kill -9 $PID
                init 6
                RESULT="200 REBOOT:120 "
        else
                RESULT="200 SW update not needed"
	fi
        result
}

result()
{
        echo $RESULT > $out
        exit 0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
out=$1
if [ -z $1 ] ; then
        out="/dev/stdout"
fi
check_upgrade
