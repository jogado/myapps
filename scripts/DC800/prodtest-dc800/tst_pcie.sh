#!/bin/sh

# Production test script
# tst_pcie.sh: preventive pcie bus test to avoid crash in tests on
# pcie peripherals: try to return information without crashing

# Added i2c smbus for imx - 6e=pci clock buffer, 76=pci switch
i2cbus_sm=1
pcie_clock_buf="6e"
pcie_switch="76"
NAME="Test pcie bus"

testi2c()
{
	let x=0x$1
	dev=`echo $x`
	res=`i2cdetect -y $2 $dev $dev | grep '[0-7]0:' | sed 's/[0-7]0://g' | sed 's/ //g'`
	echo $res
}

result()
{
        echo $RESULT > $out
	exit 0
}
RETRIES=5
linkdown=`dmesg | grep "IMX PCIe port: link down!"`
if [ "$linkdown" != "" ] ; then
	RESULT=`echo "PCIe link down, abort to avoid timeout"`
else
	while [ $RETRIES -gt 0 ] ; do
		i=$pcie_clock_buf
		res=`testi2c $i $i2cbus_sm`
		if [ "$res" != "$i" -a "$res" != "UU" ] ; then
			RESULT=`echo "PCIe clock buffer 9DB801 missing, abort to avoid crash"`
		else
			i=$pcie_switch
			res=`testi2c $i $i2cbus_sm`
			if [ "$res" != "$i" -a "$res" != "UU" ] ; then
				RESULT=`echo "PCIe switch 89HPEST5T5 missing, abort to avoid crash"`
			else
				RESULT=`echo ""`
				break;
			fi
		fi
        RETRIES=$((RETRIES-1))
        done
fi		

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits

# $3: executed test

result 

