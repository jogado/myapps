#!/bin/sh



echo "==================================================================================================================" > temp.log


`/usr/bin/production-test/testscripts/get_device_sn_ci.sh /dev/stdout >> SN.TXT` &>/dev/null
SN=`cat SN.TXT |grep -i 'S/N' |cut -d '=' -f 2 `
echo "SN       : $SN" >> temp.log


`/usr/bin/production-test/testscripts/get_pcb_ci.sh  /dev/stdout >> PCBS.TXT` &>/dev/null

PCB1=`cat PCBS.TXT |cut -d ' ' -f 5` ;
echo "PCB1     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 6` ;
echo "PCB2     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 7` ;
echo "PCB3     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 8` ;
echo "PCB4     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 9` ;
echo "PCB5     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 10` ;
echo "PCB6     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 11` ;
echo "PCB7     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 12` ;
echo "PCB8     : $PCB1" >> temp.log

PCB1=`cat PCBS.TXT |cut -d ' ' -f 13` ;
echo "PCB9     : $PCB1" >> temp.log





   T0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_type`
   V0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp`
   echo "$T0 : $V0"	>> temp.log

   T0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_type`
   V0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_1_temp`
   echo "$T0      : $V0"	>> temp.log

   T0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_2_type`
   V0=`cat /sys/devices/virtual/thermal/thermal_zone0/trip_point_2_temp`
   echo "$T0   : $V0"	>> temp.log


   cat temp.log |tail -n 13



   start_time="$(date -u +%s)"


while true
do
	end_time="$(date -u +%s)"
	elapsed="$(($end_time-$start_time))"


    S=$((elapsed%60))
    M=$((elapsed/60%60))
    H=$((elapsed/60/60%24))
    D=$((elapsed/60/60/24))

    elapsed_hhmmss="$(printf "(%d) %02d:%02d:%02d:" $D $H $M $S )"


    DATE=`date`

    TEMP=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
    GPRS0=`ls -la /dev/gprs0`
    GPRS1=`ls -la /dev/gprs1`
    GPRS2=`ls -la /dev/gprs2`
    MODEM_CI=`cat /tmp/modem3gci`

    LSUSB=`lsusb |grep -i 9000`
    TAIL=`dmesg | tail -n 1`

    /etc/init.d/modem-3G_info start &>/dev/null


    CSQ=`cat /tmp/modemcsq`


    echo "==================================================================================================================" >> temp.log
    echo "$DATE"                    >> temp.log
    echo "$DATE"                    >> /dev/kmsg
    echo "UPTIME       : $elapsed  ==> $elapsed_hhmmss"  >> temp.log


    echo "MODEM_CI     : $MODEM_CI"    >> temp.log
    echo "GPRS0        : $GPRS0"    >> temp.log
    echo "GPRS1        : $GPRS1"    >> temp.log
    echo "GPRS2        : $GPRS2"    >> temp.log
    echo "LSUSB        : $LSUSB"    >> temp.log
    echo "MODEM CSQ    : $CSQ"      >> temp.log
    echo "Temperature  : $TEMP"     >> temp.log
    echo "TAIL         :     "  >> temp.log


    dmesg | tail -n 3 >> temp.log

    cat temp.log |tail -n 14


#   aplay MOS/WAV/cardread.wav  &>/dev/null
    sleep 10
done
