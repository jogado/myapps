#!/bin/sh

ARG1=$1
FILE="reseter"

export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-


if echo $1 | grep -q "imx6s"; then
	. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
elif  echo $1 | grep -q "imx6ul"; then
	. /opt/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi 
else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi

make clean
make
file $FILE

if echo $2 | grep -q "192.168"; then
	scp $FILE root@$2:/usr/bin/
fi

