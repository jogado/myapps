#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>
#include <linux/watchdog.h>
#include <unistd.h>



extern int	usb_reset	( int bus, int device );
extern int 	reset_emv3000	( void );
extern void 	system_call 	( const char *s);
extern void 	reset_hwdog 	( void );
extern void 	show_help	( void );

//============================================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}

//================================================================================================
char filename[100];
int usb_reset(int bus, int device)
{
	int fd;
	int rc;

	sprintf(filename,"/dev/bus/usb/%03d/%03d",bus,device);

	printf("Resetting USB device [%s]\r\n", filename);
	usleep(100000);

	fd = open(filename, O_WRONLY);
	if (fd < 0) {
		printf("Error opening usb file\n\r");
		return 1;
	}
	printf("open %s OK\r\n", filename);

	rc = ioctl(fd, USBDEVFS_RESET, 0);
	if (rc < 0) {
		printf("Error in ioctl:%d\n\r",rc);
		close(fd);
		return 1;
	}

	close(fd);
	printf("Reset successful\n");
	return 0;
}
//================================================================================================
int reset_emv3000(void)
{
	printf("Resetting emv3000 ....\n\r");
	system_call("echo  1 > /sys/class/leds/emv-reset/brightness");
	sleep(1);
	system_call("echo  0 > /sys/class/leds/emv-reset/brightness");
	return 0;
}
//================================================================================================


int 	refresh = 1;	// seconds
struct 	watchdog_info ident;
int 	temperature;
int 	options = 0;
int 	flags;
int	bootstatus;      /* Wathdog last boot status */
int	timeleft;

void reset_hwdog (void)
{
	int 	fd;		/* File handler for watchdog */
	char 	dev[100];
	int 	timeout = 3;	// seconds

	sprintf(dev,"/dev/watchdog");


	fd = open(dev, O_RDWR);
	if( fd == -1) 
	{
		fprintf(stderr, "Error opening '%s': %s\n", dev, strerror(errno));
		exit(EXIT_FAILURE);
	}

	/* Set watchdog timeout */
	fprintf(stdout, "Set watchdog timeout to %d\n", timeout);
	if (ioctl(fd, WDIOC_SETTIMEOUT, &timeout) != 0)
	{
		fprintf(stderr,"Error: Set watchdog timeout failed\n");
		exit(EXIT_FAILURE);
	}


	/* Display current watchdog timeout */
	if (ioctl(fd, WDIOC_GETTIMEOUT, &timeout) == 0)
	{
		fprintf(stdout, "Current watchdog timeout is %d\n", timeout);
	} 
	else 
	{
		fprintf(stderr, "Error: Cannot read watchdog timeout\n");
		exit(EXIT_FAILURE);
	}

	/* Check if last boot is caused by watchdog */
	if (ioctl(fd, WDIOC_GETBOOTSTATUS, &bootstatus) == 0)
	{
		fprintf(stdout, "Last boot is caused by : %s\n",
		(bootstatus != 0) ? "Watchdog" : "Power-On-Reset");
	}
	else
	{
		fprintf(stderr, "Error: Cannot read watchdog status\n");
		exit(EXIT_FAILURE);
	}


	ioctl(fd, WDIOC_GETTIMELEFT, &timeleft);
	fprintf(stdout, "The timeleft is %d seconds\n", timeleft);

	ioctl(fd, WDIOC_GETTEMP, &temperature);
	fprintf(stdout, "The temperature is %d \n", temperature);

	ioctl(fd, WDIOC_GETSUPPORT, &ident);
	fprintf(stdout, "info.identity : %s\n"  , ident.identity);
	fprintf(stdout, "info.options  : %04X\n", ident.options);
	fprintf(stdout, "info.firmware : %04X\n", ident.firmware_version);

	ioctl(fd, WDIOC_GETSTATUS, &flags);
	fprintf(stdout, "GETSTATUS flags    : %04X\n", flags);
	ioctl(fd, WDIOC_GETBOOTSTATUS, &flags);
	fprintf(stdout, "GETBOOTSTATUS flags : %04X\n", flags);


	/* refresh forever loop */
	fprintf(stdout, "Kick watchdog through IOCTL\n");


	/*
	while(1)
	{ 	
		ioctl(fd, WDIOC_KEEPALIVE, NULL);
		if(reset_now) exit(0);
		usleep (100000);

		if(argc == 1 )
		{
			write(fd, "V", 1);
			printf("hwdog has been disarmed !\n\r");
			close(fd);
			exit(0);
		}

	}
	*/	


	ioctl(fd, WDIOC_KEEPALIVE, NULL);
	exit(0);

}









int reset_emv 	= 0;
int reset_usb 	= 0;
int reset_all 	= 0;
int reset_hwd   = 0;
int bus;
int device;





void show_help(void)
{
	printf("\n\rreseter v2019.10.25\n\r");
	printf("usage reseter <options> <...>\n\r");
	printf("Options:\n\r");
	printf("    -h                  : This info		 \n\r");
	printf("    emv                 : reset emv3000 'gpio'   \n\r");
	printf("    usb <bus> <device>  : reset usb 		 \n\r");
	printf("    all <bus> <device>  : reset emv + usb 	 \n\r");
	printf("    hwdog               : Hardware watchdog 	 \n\r");

	printf("\n\r");
}


int main ( int argc, char **argv ) 
{
	int i;
	char * p;


	if(argc == 1)
	{
		show_help();
		exit(0);
	}

	for(i=0 ; i < argc ; i++)
	{
		p=argv[i];

		if(memcmp(p , "emv"  	, 3 ) == 0) { reset_emv   = 1; break;}
		if(memcmp(p , "usb"  	, 3 ) == 0) { reset_usb   = 1; break;}
		if(memcmp(p , "all"  	, 3 ) == 0) { reset_all   = 1; break;}
		if(memcmp(p , "hwdog" 	, 5 ) == 0) { reset_hwd   = 1; break;}
		if(memcmp(p,"-h"     	, 2 ) == 0)
		{ 
			show_help();
		}
	}

	
	if(reset_emv == 1)
	{
		reset_emv3000();
	}

	if(reset_usb == 1)
	{
		if(argc < 4 ) { show_help() ; exit(0); }
		
		bus 	=  atoi(argv[2]);
		device  =  atoi(argv[3]);
		usb_reset(bus,device);

	}
	if(reset_all == 1)
	{
		if(argc < 4 ) { show_help() ; exit(0); }

		reset_emv3000();
		sleep(5);

		bus 	=  atoi(argv[2]);
		device  =  atoi(argv[3]);
		usb_reset(bus,device);

	}

	if(reset_hwd == 1)
	{
	
		reset_hwdog();
	}

	return 0;
}




