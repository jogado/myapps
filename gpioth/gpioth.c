#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>


extern int  main 			( int argc, char **argv ) ;
extern void system_call 		( const char *s );
extern void ShowHelp			( void );
extern int  toggle_gpio			( char *gpio_path , int period );
extern int  gpio_exist			( char *gpio_path );
extern int  read_gpio			( char *gpio_path );
extern int  write_gpio			( char *gpio_path , int val ) ;
char aTmp[100];

int Debug = 1;

int gpio   = -1;
int io     = -1;
int action = -1;   // 1:get 2:set 3:toggle
int value  = -1;

int gp=0;





//============================================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}
//============================================================================================
void ShowHelp(void)
{
	printf("gpio utility  v2020.05.18\n\r\n\r");
	printf("    Options:\n\r");
	printf("        -gp      : gpio             ex: (0-6)         \n\r");
	printf("        -io      : io               ex: (1-32)        \n\r");
	printf("        -set     : set value        ex: (0-1)         \n\r");
	printf("        -get     : get input value                    \n\r");
	printf("        -toggle  : period in microseconds             \n\r");
	exit(1);
}
//============================================================================================


int gpio_exist(char *gpio_path)
{
	FILE *fp;

	if(Debug) printf(" gpio_exit(%s)\n\r",gpio_path);

	fp = fopen(gpio_path , "w+");

	if (fp <= 0) {
		return 0;
	}
	fclose(fp);
	return 1;
}


int read_gpio(char *gpio_path)
{
	FILE *fp;
	char aTmp[100];

	fp = fopen(gpio_path , "w+");
	if (fp < 0) {
		printf("Error opening %s\n\r",gpio_path);
		return 1;
	}

	fread (aTmp,1,1,fp ) ;
	printf("%s : %c\r\n", gpio_path,aTmp[0]);

	fclose(fp);
	return 0;
}

int write_gpio(char *gpio_path , int val)
{
	FILE *fp;
	char aTmp[20];

	fp = fopen(gpio_path , "w+");
	if (fp < 0) {
		printf("Error opening %s\n\r",gpio_path);
		return 1;
	}

	sprintf(aTmp,"%d",val);
	fwrite (aTmp,strlen(aTmp),1,fp ) ;

	fclose(fp);
	return 0;
}

int toggle_gpio(char *gpio_path , int period )
{
	FILE *fp;

	fp = fopen(gpio_path , "w+");
	if (fp < 0) {
		printf("Error opening %s\n\r",gpio_path);
		return 1;
	}

	while(1)
	{
		fwrite ("1",1,1,fp ) ;
		usleep(period);
		fwrite ("0",1,1,fp ) ;
		usleep(period);
	}

	fclose(fp);
	return 0;
}





int main ( int argc, char **argv ) 
{
	int i;
	char *p;

	
	for(i=0 ; i < argc ; i++)
	{
		p=argv[i];
		//=======================================================================
		if(memcmp(p,"-h" ,2)==0)
		{ 
			ShowHelp();
		};
		//=======================================================================
		if(memcmp(p,"-gp" ,3)==0)
		{ 
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				if(strlen(p))
				{
					gpio =atoi(p);
					if( Debug ) printf("    gpio   : <%d>\n\r",gpio);
				}
			}
			else
			{
					if( Debug ) printf("    UNDEFINED GPIO\n\r");
					exit (0);
			}
		};
		//=======================================================================
		if(memcmp(p,"-io" ,3)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];
				if(strlen(p))
				{
					io =atoi(p);
					if( Debug ) printf("    io     : <%d>\n\r",io);
				}
			}
			else
			{
					if( Debug ) printf("    UNDEFINED IO\n\r");
					exit (0);
			}
		};
		//=======================================================================
		if(memcmp(p,"-get" ,4)==0)
		{ 
			if(argc > i )
			{
				i++;
				action =1;
				if( Debug ) printf("    Action : read input\n\r");
			}
			break;
		};
		//=======================================================================
		if(memcmp(p,"-set" ,4)==0)
		{ 
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				if(strlen(p))
				{
					action = 2 ;
					value =atoi(p);
					if( Debug ) printf("    set    : <%d>\n\r",value);
				}
			}
			else
			{
					if( Debug ) printf("    value  : NOT DEFINED\n\r");
					exit (0);
			}
			break;
		};
		//=======================================================================
		if(memcmp(p,"-toggle" ,7)==0)
		{ 
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				if(strlen(p))
				{
					action = 3 ;
					value =atoi(p);
					if( Debug ) printf("    toggle : <%d>(usec)\n\r",value);
				}
			}
			else
			{
					if( Debug ) printf("    PERIOD  NOT DEFINED\n\r");
					exit (0);
			}
			break;
		};
		//=======================================================================
	}	


	if( gpio == -1 )
	{
		if( Debug ) printf("    GPIO NOT DEFINED ==> exit\n\r");
		exit (-1);
	}

	if( io == -1 )
	{
		if( Debug ) printf("    IO PORT NOT DEFINED ==> exit\n\r");
		exit (0);
	}

	
	gp = (gpio-1)*32+io ;

	sprintf(aTmp,"/sys/class/gpio/gpio%d/value",gp );
	if(gpio_exist(aTmp)==0)
	{
		sprintf(aTmp,"echo %d > /sys/class/gpio/export",gp );
		system_call(aTmp);
		if( Debug ) printf("%s\n\r",aTmp);
	}




	if(action == 1 )
	{
		sprintf(aTmp,"echo 'in' > /sys/class/gpio/gpio%d/direction",gp );
		system_call(aTmp);

		/*
		sprintf(aTmp,"cat /sys/class/gpio/gpio%d/value",gp );
		system_call(aTmp);
		*/
		sprintf(aTmp,"/sys/class/gpio/gpio%d/value",gp );
		read_gpio(aTmp);
		exit (0);
	}


	if(action == 2 )
	{
		sprintf(aTmp,"echo 'out' > /sys/class/gpio/gpio%d/direction",gp );
		system_call(aTmp);

		/*
		if( Debug ) printf("%s\n\r",aTmp);
		if(value)
			sprintf(aTmp,"echo 1 > /sys/class/gpio/gpio%d/value",gp );
		else
			sprintf(aTmp,"echo 0 > /sys/class/gpio/gpio%d/value",gp );
		system_call(aTmp);
		*/

		sprintf(aTmp,"/sys/class/gpio/gpio%d/value",gp );
		if(value)
			write_gpio(aTmp,1);
		else
			write_gpio(aTmp,0);

		read_gpio(aTmp);

		exit (0);
	}

	if(action == 3 )
	{
		sprintf(aTmp,"echo 'out' > /sys/class/gpio/gpio%d/direction",gp );
		system_call(aTmp);
		if( Debug ) printf("%s\n\r",aTmp);
		while(1)
		{
	
			sprintf(aTmp,"echo 1 > /sys/class/gpio/gpio%d/value",gp );
			system_call(aTmp);
			usleep(value);

			sprintf(aTmp,"echo 0 > /sys/class/gpio/gpio%d/value",gp );
			system_call(aTmp);
			usleep(value);
		}
	}
	return(0);
}


