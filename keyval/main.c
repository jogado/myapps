#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <unistd.h>

#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <termios.h>

#include <ctype.h>

#include "version.h"

#include "main.h"

//https://github.com/cmus/cmus/blob/master/keyval.c

int Debug=1;



#define MAX_SIZE 8*1024
unsigned char gBuf[MAX_SIZE+1];
int 	gBuf_size=0;


char 	eeprom_file[] = "/sys/bus/i2c/devices/0-0050/eeprom";
char 	data_file[]   = "/etc/eeprom/eeprom.dat";

ENTRY_TABLE table[MAX_INDEX_TABLE];



//=============================================================================================================================
int FileExist(char *s)
{
	FILE *fps;
	
	fps = fopen( s , "r" ); 
	if (fps == NULL) 
	{
		printf("File %s do not exist\n\r",s);
		return(0);
	}
	fclose(fps);
	return (1);
}
//=============================================================================================================================



//=============================================================================================================================
int FileSize(char *s)
{
	FILE *fps;
	int sz;
	
	fps = fopen( s , "r" ); 
	if (fps == NULL) 
	{
		printf("error opening file %snr",s);
		return(0);
	}

	fseek ( fps , 0L, SEEK_END);
	sz=ftell(fps);
	fseek ( fps , 0L, SEEK_SET);

	fclose(fps);

	return (sz);
}
//=============================================================================================================================


char aDump[100];
int  aDump_Offset=0;

void dump(unsigned char *gBuf , int size)
{
	int i;

	aDump_Offset=0;
	aDump[aDump_Offset]=0;
	
	for ( i = 0 ; i < size ; i++)
	{
		if( (i%16) == 0 )
		{
			printf("%s ",aDump);
			printf("\n%04X : ",i);
			aDump_Offset=0;
			aDump[aDump_Offset]=0;
		}
		
		printf("%02x ",gBuf[i] );

		if(isprint(gBuf[i]) )
		{
			aDump[aDump_Offset++] = gBuf[i] ;
			aDump[aDump_Offset]=0;
		}
		else
		{
			aDump[aDump_Offset++] = '.' ;
			aDump[aDump_Offset]=0;
		}
			
	} 
	printf("%s ",aDump);
	printf("\n");
}












int filecopy(char *d, char *s)
{
	FILE *fps;
	FILE *fpd;
	int in;
	int out;
	unsigned char buf[512+1];
	int bufsize;
	int Offset;
	
	Offset=0;
	
	bufsize=sizeof(buf);
	
	fps = fopen( s , "r" ); if (fps == NULL)
	{
		exit (-3);
	}

	fpd = fopen( d , "w" ); 
	if (fpd == NULL) 
	{
		fclose(fps);
		exit (-4);
	}

	while (1)
	{
		in = fread (buf,1, bufsize,fps);
		if(in == 0 ) break;
		memcpy(&gBuf[Offset] , buf, in );
		Offset+=in;

		out = fwrite ( buf,1,in,fpd);
		if(out == 0 ) break;
	}
	fclose(fps);
	fclose(fpd);
	return (0);
}

//=============================================================================================================================
int datafile_to_mem(char *s)
{
	FILE *fps;
	int in;
	unsigned char buf[512+1];
	int bufsize;
	int Offset;
	
	Offset=0;
	
	bufsize=sizeof(buf);
	
	fps = fopen( s , "r" ); if (fps == NULL)
	{
		printf("fopen error file: %s \n",s);
		exit (-6);
	}

	while (1)
	{
		in = fread (buf,1, bufsize,fps);
		if(in == 0 ) break;
		memcpy(&gBuf[Offset] , buf, in );
		Offset+=in;
	}
	fclose(fps);
	return (0);
}
//=========================================================================================
int mem_to_datafile(char *s)
{
	FILE *fp;
	int Count = gBuf_size ;

//	char *s ;
//	s = data_file;

	fp = fopen( s , "w" ); if (fp == NULL)
	{
		printf("fopen error file: %s \n",s);
		exit (-6);
	}

	int step=100;
	while (1)
	{
		if(Count >= 100 )
		{
			fwrite (&gBuf[gBuf_size - Count], 1, 100 ,fp);
			Count -= step ;
		}
		else
		{
			if(Count > 0 )
			{
				fwrite (&gBuf[gBuf_size - Count], 1, Count ,fp);
			}
			break;
		}

	}
	fclose(fp);
	return (0);
}
//=============================================================================================================================

void Check_datafile()
{
	int data_size = 0;

	if( FileExist(data_file) == 0)
	{
		printf("Copy eeprom file !\n\r");
		filecopy	( data_file , eeprom_file );
	}


	data_size = FileSize(data_file);

	if(data_size <=0 ) 
	{
		printf("Source file size = %d (%s)\n",data_size,data_file);
		exit(-1);
	}

	if (data_size > MAX_SIZE )
	{
		printf("Source file too big = %d (%s)\n",data_size,data_file);
		exit(-2);
	}

	gBuf_size= data_size;

	datafile_to_mem (data_file);
}


//=============================================================================================================================
int  add_key(const char *aKey , const char *aVal)
{
	int i;
	int valid_keys=0 ;
	char aTmp[200];
	char *p;
	int len;



	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if( table[i].valid ) 
				valid_keys++;
		else
				break;
	}


	if( valid_keys >= MAX_INDEX_TABLE )
	{
		printf("error: table index overflow  %d/%d \n\r",valid_keys,MAX_INDEX_TABLE );
		return(0);
	}


	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if(table[i].valid )
		{

			// akey --------------------------------
			p = (char *) &gBuf[ table[i].start +1 ];
			len = table[i].separator - table[i].start-1;
			memcpy( aTmp , p , len );
			aTmp[len]=0;

			if( Compare((char *)aKey,(char *)aTmp) )
			{
				printf("error: key '%s' already exists\n\r",aKey);
				return(0);
			}
		}
	}



    for ( i = 0; i <  gBuf_size ; i++)
	{
		if ( gBuf[i] == 0xff )
		{
			sprintf(aTmp,"{%s|%s}",aKey,aVal);
			memcpy( &gBuf[i] , aTmp,strlen(aTmp)); 

			printf("key:<%s><%s> added @ %04d \n\r",aKey,aVal,i);
			break;
		}
	}
	return(1);
}
//=============================================================================================================================
int format_all(void)
{

	if( gBuf_size == 0 || gBuf_size > MAX_SIZE ) 
	{
		printf("Formatting failure\n\r");
		return (-1);
	}

	memset( &gBuf[0] , 0xff, gBuf_size );

	return(0);
}
//=============================================================================================================================



void Update_index(void)
{
	int i;	
	int index=0;
	unsigned char c;
	int state = STATE_IDLE;

	ENTRY_TABLE	t;


	memset( table ,0x00,  sizeof(table) );


    for ( i = 0; i <  gBuf_size ; i++)
	{
		c = gBuf[i]; 

		if (c == 0xff )
		{
			//printf("Free space found (%d)\n\r",i);
			break;
		}
		

		switch(state)
		{

			case STATE_IDLE :
					memset (&t, 0x00, sizeof(ENTRY_TABLE) );

			case HUNTING_START :
				if( gBuf[i] == '{' )
				{
					t.start = i ;
					state = HUNTING_SEPARATOR ;
				}
				break;

			case HUNTING_SEPARATOR :
				if( gBuf[i] == '|' )
				{
					t.separator = i ;
					state = HUNTING_END ;
				}
				break;

			case HUNTING_END :
				if( gBuf[i] == '}' )
				{
					t.end = i ;
					state = STATE_IDLE ;
					t.valid =1 ;

					memcpy( &table[index] ,&t ,sizeof(ENTRY_TABLE));
					index ++;

				}
				break;

			default:
					state = STATE_IDLE ;
					break;
		}
	}
//	printf(" Table index = %d\n",index);

}

//========================================================================================
void Show_all_keyval(void)
{
	int i;
	char aKey[100];
	char aValue[100];
	char *p;
	int len;


	printf("Show_all_keyval()\n\r");
	


	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if(table[i].valid )
		{
			printf( "%03d  K:%04d   S:%04d  V:%04d"
				,i
				,table[i].start
				,table[i].separator
				,table[i].end
			);

			// akey --------------------------------
			p = (char *) &gBuf[ table[i].start +1 ];
			len = table[i].separator - table[i].start-1;
		
			memcpy( aKey , p , len );
			aKey[len]=0;
			printf ( "  <%s>",aKey);

			// aValuekey --------------------------------

			p = (char *) &gBuf[ table[i].separator +1 ];
			len = table[i].end - table[i].separator-1;
		
			memcpy( aValue , p , len );
			aValue[len]=0;
			printf ( " [%s]",aValue);

			printf("\n");
		}
	}
}
//========================================================================================


char aGet_keyval[120];

int get_keyval ( char *akey )
{
	int i;
	char aKey[50];
	char aValue[50];
	char *p;
	int len;

	//printf("get_keyval(%s)\n\r",akey);
	aGet_keyval[0]=0;

	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if(table[i].valid )
		{

			// akey --------------------------------
			p = (char *) &gBuf[ table[i].start +1 ];
			len = table[i].separator - table[i].start-1;
			memcpy( aKey , p , len );
			aKey[len]=0;
			// aValuekey --------------------------------
			p = (char *) &gBuf[ table[i].separator +1 ];
			len = table[i].end - table[i].separator-1;
			memcpy( aValue , p , len );
			aValue[len]=0;

			if( Compare(aKey,akey) )
			{
				sprintf(aGet_keyval,"%s=%s",aKey,aValue);
				printf("%s\n\r",aGet_keyval);
				return(1);
			}
		}
	}
	sprintf(aGet_keyval,"key (%s) not found",akey);
	printf("%s\n\r",aGet_keyval);

	return(0);
}

int delete_keyval ( char *akey )
{
	int i;
	char aKey[50];
	char aValue[50];
	char *p;
	int len;

	printf("delete_keyval(%s)\n\r",akey);
	aGet_keyval[0]=0;

	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if(table[i].valid )
		{
			// akey --------------------------------
			p = (char *) &gBuf[ table[i].start +1 ];
			len = table[i].separator - table[i].start-1;
			memcpy( aKey , p , len );
			aKey[len]=0;
			// aValuekey --------------------------------
			p = (char *) &gBuf[ table[i].separator +1 ];
			len = table[i].end - table[i].separator-1;
			memcpy( aValue , p , len );
			aValue[len]=0;
			//------------------------------------------

			if( Compare(aKey,akey) )
			{

				p = (char *) &gBuf[ table[i].start ];
				*p=0xdd;
				printf("%s key deleted\n\r",akey);
				return(1);
			}
		}
	}
	sprintf(aGet_keyval,"key (%s) not found",akey);
	printf("%s\n\r",aGet_keyval);

	return(0);
}













//=============================================================================================================================
void show_info(void)
{
	int i;
	int f;

	int valid_keys =0;

	for (i=0; i< MAX_INDEX_TABLE ; i++)
	{
		if( table[i].valid ) valid_keys++;
	}

  	for ( f = 0; f <  gBuf_size ; f++)
	{
		if (gBuf[f] == 0xff )
		{
			break;
		}
	}		
	printf("eeprom file: %s\n\r",eeprom_file);
	printf("Datafile   : %s\n\r",data_file);
	printf("Data_size  : %d bytes\n\r",gBuf_size);
	printf("Valid keys : %d \n\r",valid_keys);
	printf("Free       : %d%c \n\r", ((gBuf_size - f )*100)/gBuf_size,'%'  );

	return;
}
//=============================================================================================================================



//=============================================================================================================================
int eeprom_to_local(void)  // Eeprom > /etc/eeprom
{
	if( FileExist(eeprom_file) == 0)
	{
		printf("error: opening file : %s \n\r", eeprom_file);
		exit(-1);
	}
	filecopy	( data_file , eeprom_file );
	return(0);
}
//=============================================================================================================================


//=============================================================================================================================
int local_to_eeprom(void)  // Eeprom > /etc/eeprom
{
	if( FileExist(data_file) == 0)
	{
		printf("error: opening file : %s \n\r", data_file);
		exit(-1);
	}

	if( FileExist(eeprom_file) == 0)
	{
		printf("error: opening file : %s \n\r", eeprom_file);
		exit(-2);
	}

	filecopy	( eeprom_file , data_file );
	return(0);
}
//=============================================================================================================================



//=============================================================================================================================
void show_help(void)
{
	printf("\n\r");
	printf("keyval (%s)\n\r",Build_dateTime() );
	printf("-----------------------------\n\r");
	printf("usage: keyval <options> <...>\n\r");
	printf("Options:\n\r");
	printf("    -h                  : This info\n\r");
	printf("    -show               : show all keys/value \n\r");
	printf("    -add <key> <value>  : Add a pair\n\r");
	printf("    -get <key>          : Search the key value\n\r");
	printf("    -delete <key>       : Remove key\n\r");
	printf("    -info               : data info\n\r");
	printf("    -dump               : show first 256 bytes\n\r");
	printf("    -format_all         : Format local datafile\n\r");
	printf("    -local_to_eeprom    : Copy localfile to eeprom\n\r");
	printf("    -eeprom_to_local    : Copy eeprom to localfile\n\r");
	exit(0);
}
//=============================================================================================================================

char Compare(char *p1 , const char *p2 )
{
	int len1=strlen(p1);
	int len2=strlen(p2);

	if( len1 != len2 ) return 0;

	return(! memcmp(p1,p2,len1) );
}



typedef enum {
   CMD_SHOW = 10 	 ,
   CMD_ADD 			 ,
   CMD_GET 			 ,
   CMD_DEL 			 ,
   CMD_INFO 		 ,
   CMD_DUMP 		 ,
   CMD_FORMAT 		 ,
   CMD_EEPROM_LOCAL  ,
   CMD_LOCAL_EEPROM  ,

} HUNT_CMD;



int main ( int argc, char **argv ) 
{
	int i;
	int nocmd=0;
	char *p;
	int Cmd;
	char aKey[100];
	char aVal[100];
	

	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		//printf("%d : %s\n\r",i,p);

		if( p[0] != '-')break;

		if( Compare ( p , "-h"      ) ){ show_help();}
		if( Compare ( p , "-show"	) ){ Cmd=CMD_SHOW	; nocmd++; }


		if( Compare( p , "-add" ) && (argc > i+2))
		{ 
			nocmd++;
			Cmd = CMD_ADD; 
			memcpy ( aKey ,  argv[i+1] , strlen(argv[i+1]) ); aKey[strlen(argv[i+1])]=0;
			memcpy ( aVal ,  argv[i+2] , strlen(argv[i+2]) ); aVal[strlen(argv[i+2])]=0;
			i++;
			i++;
		}

		if( Compare( p , "-get" ) && (argc > i+1))
		{ 
			nocmd++;
			Cmd = CMD_GET; 
			memcpy ( aKey ,  argv[i+1] , strlen(argv[i+1]) ); aKey[strlen(argv[i+1])]=0;
			i++;
		}

		if( Compare( p , "-delete" ) && (argc > i+1))
		{ 
			nocmd++;
			Cmd = CMD_DEL; 
			memcpy ( aKey ,  argv[i+1] , strlen(argv[i+1]) ); aKey[strlen(argv[i+1])]=0;
			i++;
		}

		if( Compare( p , "-format_all" ) )
		{ 
			nocmd++;
			Cmd = CMD_FORMAT; 
			i++;
		}


		if( Compare( p , "-eeprom_to_local" ) )
		{ 
			nocmd++;
			Cmd = CMD_EEPROM_LOCAL; 
			i++;
		}

		if( Compare( p , "-local_to_eeprom" ) )
		{ 
			nocmd++;
			Cmd = CMD_LOCAL_EEPROM; 
			i++;
		}


		if( Compare( p , "-info" )  )
		{ 
			nocmd++;
			Cmd = CMD_INFO; 
			i++;
		}

		if( Compare( p , "-dump" )  )
		{ 
			nocmd++;
			Cmd = CMD_DUMP; 
			i++;
		}

	}

	if(nocmd==0)
	{
		show_help();
		return(-1);
	}


	Check_datafile();
	Update_index();

	switch(Cmd)
	{
		case CMD_DUMP :
						dump(gBuf,256);
						break;

		case CMD_SHOW :
						Show_all_keyval();
						break;
		case CMD_ADD :
						if( add_key( aKey, aVal ) )
						{
							mem_to_datafile(data_file);
						}
						break;
		case CMD_GET :
						get_keyval(aKey);
						break;
		case CMD_DEL :
						if( delete_keyval(aKey) )
						{
							mem_to_datafile(data_file);
						}
						break;

		case CMD_INFO :
						show_info();
						break;

		case CMD_FORMAT :
						if(! format_all() )
						{
							mem_to_datafile(data_file);
							dump(gBuf,256);
						} 
						break;

		case CMD_EEPROM_LOCAL :
						eeprom_to_local();
						dump(gBuf,256);
						break;

		case CMD_LOCAL_EEPROM :
						local_to_eeprom();
						break;


		default:
						show_help();
						break;		
	}

	exit(0);

}

