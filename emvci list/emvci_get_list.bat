#!/bin/sh

for i in {0..90}
do
	RES=`emvci -get $i | tr -d '\n\r' `

	len=`echo $RES |awk '{print length}'`

	if [ $len -gt 20 ]
	then
		printf "[%2s]" $i
		echo  $RES
	fi

    usleep 200000

done
