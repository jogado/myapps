#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>




extern int  main 			( int argc, char **argv ) ;
extern void to_barcode			( char *s,int len);
extern void logfile			( char *s,int len);
extern int  set_interface_attribs 	( int fd, int speed, int parity);
extern void set_blocking 		( int fd, int should_block) ;
extern void store 			( unsigned char c ) ;
extern void ShowHelp			( void );

//char portname[] = "/dev/ttyUSB2";


char portname[100];
char output_file[256];

int  fd ;
int  Baudrate = 115200 ;
int  Debug    = 0;
int  DoBeep   = 0;

char aSeparator[10];



int GOT_A_COMMAND	= 0 ;
int GOT_A_DEV  		= 0 ;
int GOT_A_BAUDRATE	= 0 ;
int GOT_A_SEPARATOR	= 0 ;
int GOT_A_OUTPUT_FILE   = 0 ;


int OK_DETECTED		= 0 ;
int ERROR_DETECTED	= 0 ;




int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf ("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
//        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
        tty.c_cc[VTIME] = 2;            // 0.2 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf ("error %d setting term attributes", errno);
}


void to_barcode(char *s,int len)
{
	write (fd, s , len);           // send 7 character greeting
	usleep (100000);
}



void logfile(char *s,int len)
{
	FILE *ftp;

	if ( GOT_A_OUTPUT_FILE == 0 ) return;

 	ftp = fopen( output_file , "w" );
	if (ftp == NULL)
	{	
        	printf ("error %d opening %s: %s", errno, output_file, strerror (errno));
	        return;
	}
	fwrite(s , 1 , len , ftp );
	fwrite("\n\r" , 1 , 2 , ftp );
	fclose(ftp);
}




#define MAX_LEN 1000
char buf [MAX_LEN+10];
int  Offset = 0 ;



void store (unsigned char c)
{

	if( (c == 0x0a || c == 0x0d)  )
	{
		if(Offset)
		{	

			if(GOT_A_SEPARATOR)
			{
	     			printf("%s%s",buf,aSeparator);
			}
			else
			{
	     			printf("%s\n\r",buf);
			}
		
	     		logfile(buf,Offset);

			if(memcmp(buf,"OK",2) == 0)
			{
				OK_DETECTED = 1 ;
			}

			if(memcmp(buf,"ERROR",5) == 0)
			{
				ERROR_DETECTED = 1 ;
			}
	    	 	buf[0]=0;
	     		Offset=0;
		}
     		return;
	}

	if( Offset < MAX_LEN )
	{
		buf[Offset]=c;
		Offset++;
		buf[Offset]=0;
	}
}







void ShowHelp(void)
{
	printf("atcom utility  v2020.01.16\n\r\n\r");
	printf("    Default Baudrate = 9600 bps\n\r");
	printf("    usage atcom <options> <...>\n\r");
	printf("    Options:\n\r");
	printf("        -c     : at command   ex: ati           \n\r");
	printf("        -d     : device       ex: /dev/ttyUSB2  \n\r");
	printf("        -b     : Baudrate     ex: 115200        \n\r");
	printf("        -s     : Separator    ex: ':'           \n\r");
	printf("        -o     : Output file  ex: ./atcom.log   \n\r");

	exit(1);
}




int main ( int argc, char **argv ) 
{
	int n;
	unsigned char c;
	int i;
	char *p;
	char aCmd[100];
	int to=1000000; // 2000000 uSec = 2sec
	int t=0; // 2000000 uSec = 2sec
	


	sprintf(portname,"%s","/dev/ttyUSB2");



	for(i=0 ; i < argc ; i++)
	{
		p=argv[i];
		//printf("argv[%d]= <%s>\n\r",i,p);

		if(memcmp(p,"-h" ,2)==0)
		{ 
			ShowHelp();
		};

		if(memcmp(p,"-c" ,2)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];

				if(strlen(p ) > 2)
				{
					sprintf( aCmd ,"%s",p );
					if( Debug ) printf("    Command   : <%s>\n\r",aCmd);
					GOT_A_COMMAND = 1 ;
				}
			}
		};

		if(memcmp(p,"-d" ,2)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];

				if(strlen(p ) > 2)
				{
					sprintf( portname ,"%s",p );
					if( Debug ) printf("    Device    : <%s>\n\r",portname);
					GOT_A_DEV = 1 ;
				}
			}
		};

		if(memcmp(p,"-b" ,2)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];

				if(strlen(p) > 3)
				{
					Baudrate=atoi(p);
					if(Debug ) printf("    Baudrate  : <%d>\n\r",Baudrate);
					GOT_A_BAUDRATE = 1 ;
				}
			}
		};

		if(memcmp(p,"-s" ,2)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];

				if(strlen(p) > 0)
				{
					sprintf( aSeparator ,"%s",p );
					if( Debug ) printf("    Separator : <%s>\n\r",p);
					GOT_A_SEPARATOR = 1 ;
				}
			}
		};

		if(memcmp(p,"-o" ,2)==0)
		{ 
			if(argc > i )
			{
				i++;
				p=argv[i];

				if(strlen(p) > 0)
				{
					sprintf( output_file ,"%s",p );
					if( Debug ) printf("    output_file: <%s>\n\r",p);
					GOT_A_OUTPUT_FILE = 1 ;
				}
			}
		};


	}


	if( GOT_A_COMMAND == 0 )
	{
		ShowHelp();
	}



	fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname, strerror (errno));
	        return(-1);
	}

	set_interface_attribs (fd, Baudrate, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (fd, 0);                    // set no blocking



	//================================================
	// Flush Buffer
	//================================================
	while(1)
	{
		n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
		if(n)
		{
			continue;
		}	
		break;
	}

	write (fd, aCmd , strlen(aCmd));  
	write (fd, "\r" , 1);  

	while(1)
	{
		n = read (fd, &c, 1); 
		if(n)
		{
			t=0;

			store(c);
			if( OK_DETECTED) 
			{
				printf("\r\n");
				break;
			}
			if( ERROR_DETECTED) 
			{
				printf("\r\n");
				break;
			}
			continue;
			
		}

		if( t > to ) 
		{
			printf("TIMEOUT\n\r");
			break;
		}

		usleep(100000);
		t+=100000;
	}
		
	close(fd);
	return(0);
}


















