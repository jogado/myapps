/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_rf.c
 *
 *  Created on: Oct 21, 2008
 *      Author: jvolckae
 */

#include <errno.h>

#include "oti_rf.h"

/**
 * This method will set/get the status of the rf antenna.
 *
 * @param reader	pointer to the reader control block.
 * @param code		operational code that needs to be performed (GET - SET - DO).
 * @param antenna	pointer to OTI reader rf antenna status.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_rf_antenna(POtiReaderCb reader, OpCode code, OtiRfAntenna* antenna)
{
	int result = 0;

	/*-- sanity check --*/
	if ( antenna == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char data[1] = { 0 };

		/*-- Get the current applications that are enabled --*/
		if ( code == GET )
		{
			result = oti_handle_tag( reader, code, OTI_RF_ANTENNA, NULL, 0, data, sizeof( data ), OtiTlvSizeAdd );

			/*-- on success copy result in appli buffer --*/
			if ( result > 0 )
			{
				*antenna = (OtiRfAntenna)data[0];
			}
		}
		/*-- DO or SET the applications --*/
		else
		{
			data[0] = (char)*antenna;
			result = oti_handle_tag( reader, code, OTI_RF_ANTENNA, data, sizeof( data ), NULL, 0, OtiTlvSizeAdd );
		}
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This method will set the modulation type.
 *
 * @param reader	pointer to the reader control block.
 * @param mode		RF operation mode (type A/B).
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_rf_antenna_mode(POtiReaderCb reader, OtiRfAntennaMode mode)
{
	int result = 0;
	char data[1];

	data[0] = (char)mode;

	result = oti_handle_tag( reader, DO, OTI_RF_MODULATION_TYPE, data, sizeof( data ), NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}


/**
 * This method will reset the rf field.
 *
 * @param reader	pointer to the reader control block.
 * @param timeout   reset timeout in msec
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_rf_reset(POtiReaderCb reader, int timeout)
{
	int result = 0;

	char data[1] = { 0 };

	data[0] = (char)(timeout&0xFF);
	result = oti_handle_tag( reader, DO, OTI_RF_RESET, data, sizeof( data ), NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}


/**
 * This method will read or set the current protocol interface.
 *
 * @param reader				pointer to reader control block.
 * @param code					SET, GET or DO
 * @param tx					mode (00 or 01)
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 *
 */

int oti_pcd_protocol_selector( POtiReaderCb reader, OpCode code, char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, OTI_HOST_PCD_PROTOCOL_SELECTOR, NULL, 0, rx, rxLen, OtiTlvSizeAdd );
	}	
	else if (code == SET)
	{
		result = oti_handle_tag( reader, SET, OTI_HOST_PCD_PROTOCOL_SELECTOR, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This method will read or set the RF Collision Message Enable.
 *
 * @param reader				pointer to reader control block.
 * @param code					SET, GET or DO
 * @param tx					mode (00 or 01), 1 means RF Collision Message is Enabled
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 *
 */

int oti_rf_collision_msg_enabled( POtiReaderCb reader, OpCode code, char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, OTI_RF_COLLISION_MSG_ENABLED, NULL, 0, rx, rxLen, OtiTlvSizeAdd );
	}	
	else if (code == SET)
	{
		result = oti_handle_tag( reader, SET, OTI_RF_COLLISION_MSG_ENABLED, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}
