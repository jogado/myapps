/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_technical.h
 *
 *  Created on: Oct 21, 2008
 *      Author: jvolckae
 */

#ifndef _PRODATA_SYSTEMS_OTI_TECHNICAL_H_
#define _PRODATA_SYSTEMS_OTI_TECHNICAL_H_ 1

#include <errno.h>
#include <oti.h>
#include "oti_reader.h"
#include "oti_command.h"



/*-- function declarations --*/
int oti_restore_factory_setttings( POtiReaderCb reader );
int oti_reset_pcd( POtiReaderCb reader );
int oti_load_default_parameters( POtiReaderCb reader );
int oti_get_version( POtiReaderCb reader, OtiVersion version, char* data, size_t length );
int oti_get_getdelay( POtiReaderCb reader, char* data, size_t length );
int oti_call_boot_loader( POtiReaderCb reader, char* data, size_t length );
int oti_flash_packet( POtiReaderCb reader, char* txData, size_t txLen, char* rxData, size_t rxLen );
int oti_run_application( POtiReaderCb reader, char* data, size_t length );
int oti_reader_command( POtiReaderCb reader, OpCode code, OtiTag otiTag, char* tx, size_t txLen, char* rx, size_t rxLen );
int oti_reader_raw_command( POtiReaderCb reader, char* tx, size_t txLen, char* rx, size_t rxLen );

#endif /* _PRODATA_SYSTEMS_OTI_TECHNICAL_H_ */
