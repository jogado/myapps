/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_trace.c
 *
 *  Created on: Oct 24, 2008
 *      Author: jvolckae
 */

#include "oti_trace.h"
OtiVerbosity verbosity = OtiVerbosityNone;
/**
 * This method will print-out the content of the data buffer to the console.
 *
 * @param str		pointer to string that specifies what is traced.
 * @param data		pointer to data that needs to be traced.
 * @param length	length of data in the buffer
 *
 */
void oti_trace_hex( POtiCb oti, const char* str, const char* data, size_t length)
{
	size_t idx = 0;
	struct tms tm;
	clock_t stamp = times( &tm );

	if ( !(verbosity&OtiVerbosityIo) ) return;

	fprintf(stdout,"%ld : %s", (stamp-(oti->tmOffset))*oti->timerRes, str );
	for ( idx=0 ; idx<length ; idx++ )
	{
		if ( (idx%16) == 0 ) fprintf(stdout,"\n    " );

		fprintf(stdout,"%02X ", data[idx]&0xFF );
	}

	fprintf(stdout,"\n");
}


