/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_poll.h
 *
 *  Created on: Oct 21, 2008
 *      Author: jvolckae
 */

#ifndef _PRODATA_SYSTEMS_OTI_POLL_H_
#define _PRODATA_SYSTEMS_OTI_POLL_H_ 1

#include <oti.h>

#include "oti_reader.h"
#include "oti_command.h"

/*-- Function declarations --*/
int oti_poll_start(POtiReaderCb reader, OpCode code, OtiPollMode* mode);
int oti_wait_for_card( POtiReaderCb reader, POtiCardInfo cardinfo, int timeout );
int oti_wait_for_card_extended( POtiReaderCb reader, POtiCardInfo cardinfo, int timeout );
int oti_poll_stop(POtiReaderCb reader);
int oti_remove_picc(POtiReaderCb reader);
int oti_remove_picc_extended(POtiReaderCb reader);
int oti_wait_for_card_removal( POtiReaderCb reader, int timeout );
int oti_wait_for_card_removal_extended( POtiReaderCb reader, int timeout );
int oti_poll_emv(POtiReaderCb reader, unsigned char *tx, unsigned int txLen, POtiCardInfo cardinfo );
int oti_enable_application(POtiReaderCb reader, OpCode code, OtiAppli_t* appli, OtiKernelID kernelId);
int oti_transparent_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen );
int oti_sr_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen );
int oti_transparent_layer_control( POtiReaderCb reader, OtiTransparentLayer device );
int oti_transparent_channel_select( POtiReaderCb reader, OtiTransparentChannel channel );
int oti_transparent_frame_size( POtiReaderCb reader, OtiTransparentFrameSize size );
int oti_mifare_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen );
int oti_mifare_key_region( POtiReaderCb reader, OtiKeyStoreRegion keyStoreRegion );
int oti_copy_data_2_tag_struct( char *rxBuf, size_t rxLen, POtiCardInfo cardinfo);

#endif /* _PRODATA_SYSTEMS_OTI_POLL_H_ */

