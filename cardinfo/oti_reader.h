/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_reader.h
 *
 *	Header file containing local structures, definitions and functions for the oti_reader.c
 *
 *  Created on: Oct 13, 2008
 *      Author: jvolckae
 */

#ifndef _PRODATA_SYSTEMS_OTI_READER_H_
#define _PRODATA_SYSTEMS_OTI_READER_H_ 1

#include <termios.h>
#include <pthread.h>
#include <sys/select.h>
#include <sys/times.h>

#include <oti.h>

#include "oti_tags.h"

/*-- Definitions --*/
#define OTI_READER_WRITE_TIMEOUT	  300
#define OTI_READER_ACK_TIMEOUT		  300
#define OTI_READER_RESPONSE_TIMEOUT	10000
#define OTI_READER_RETRIES				5
/* open retry 12 trials*500msec = 6 seconds window for opening */
#define OTI_OPEN_RETRIES		12
#define OTI_OPEN_RESET_RETRIES  3
#define OTI_OPEN_RESET_WAIT     5000
#define OTI_OPEN_INTERVAL		500	
#define OTI_OPEN_USB_RESET_INTERVAL 4000	


/*-- Structure declaration --*/
/**
 * @struct __OtiReaderOpen
 *
 * Structure that will contain the information used to initialise the reader.
 */
typedef struct __OtiReaderOpen
{
	char path[64];					        /*! path to the serial device where the reader is connected to */
	speed_t baudrate;			            /*! baudrate of to communicate with the reader */
    OtiReaderInterfaceType interfaceType;   /*! reader communication interface type */
	OtiReaderMode mode;
} OtiReaderOpen, * POtiReaderOpen;



/**
 * @struct __OtiTransaction
 *
 * Allocation of buffers used when communicating with the oti reader.
 */
typedef struct __OtiTransaction
{
	char tx[908];						/*! transmit buffer */
	char rx[908];						/*! receive buffer */

	char txTlv[900];
	char rxTlv[900];

	int txLength;						/*! number of bytes to transmit */
	int rxLength;						/*! number of bytes received */

	int txResult;						/*! result of the last write action */
	int rxResult;						/*! result of the last read action */

	char txLrc;							/*! LRC for transmit buffer */
	char rxLrc;							/*! LRC for receive buffer */

    unsigned char stmRx[908];           /*! STM32 OTI read buffer */
    int stmRxRd;
    int stmRxWr;

	char reserverd[2];					/*-- alignment --*/

} OtiTransaction, *POtiTransaction;

/**
 * @struct __OtiReaderCb
 *
 * Structure used to store the Oti Reader control block information.
 */
typedef struct __OtiReaderCb
{
	int fd;								/*! handle to the lowlevel communication device */
	fd_set rfds;						/*! fd_set used to wait for read input */
	fd_set wfds;						/*! fd_set used to wait for sending new data */
	int nfds;							/*! device handle + 1 to watch */

	struct termios options;				/*! lowlevel options of the communication device */
	OtiReaderOpen otiOpen;				/*! options used to open the reader */
	pthread_mutex_t mutex;				/*! handle to reader control block mutex */

	OtiTransaction trx;					/*! transaction buffer used when sending/receiving data to/from the reader */

	char unit;							/*! unit number of this reader control block */
	unsigned char pollMode;				/* type of poll going on - store required for new poll methods with SRI ticket */
	unsigned char piccType;				/* piccType detected */
	unsigned char chip_ID_SR;			/* Random chip_ID of SR ticket */
//	char reserved[1];				/*-- alignment --*/

	int pfd[2];				/* anonymous pipe to make some calls interruptible */
#define INTERRUPTIBLE_PP	1		/* it's a pending poll */
#define INTERRUPTIBLE_WAIT_CARD	2		/* waiting for a card */
	int interruptible;			/* interruptible state */
	OtiReaderTimeout timeout;
	ExtendedPollConfig pollConfig;		/* extended poll configuration */
	struct __OtiCb *oti;				/*! reference to the oti control block this reader is belonging to */
	struct __OtiReaderCb *previous;		/*! reference to the previous reader in the oti control block */
	struct __OtiReaderCb *next;			/*! reference to the next reader in the oti control block */

	pthread_t ptid;
	int wpfd[2];						/* write pipe */
	int rpfd[2];						/* read pipe */
	void (* edm_callback)(char *payload_tlv, int size);	/* external display_message handler callback - called with payload of message tag-length-value = eg DF460111 */

} OtiReaderCb, *POtiReaderCb;


/**
 * @struct __OtiCb
 *
 * Structure defining the readers conntected to the current context
 */
typedef struct __OtiCb
{
	int				timerRes;	/*! system resolution timer */
	clock_t			tmOffset;	/*! clock offset to init of program */
	pthread_mutex_t	mutex;		/*! handle to oti control block mutex */
	int 			nReaders;	/*! number of readers conntected to the context */
	POtiReaderCb    reader;		/*! list of readers connected */
} OtiCb, *POtiCb;



/*-- Function declaration --*/
POtiCb oti_reader_init( void );
int oti_reader_deinit( POtiCb oti );
POtiReaderCb oti_reader_open( POtiCb oti, const POtiReaderOpen settings );
int oti_reader_close( POtiReaderCb reader );

void * oti_thread (void * arg);
#endif /* _PRODATA_SYSTEMS_OTI_READER_H_ */
