#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <ctype.h>


#include <buildate.h>
#include <cardinfo.h>

#include <oti.h>

OtiOpen otiopen = {
    .path = "/dev/emv3000",
    .baudrate = 115200,
    .interfaceType = OtiReaderInterfaceTypeSerial,
};


//====================================================================
// Global variables
//====================================================================
int Debug = 0;
char aData[128+1];
unsigned char versbuf[ 2048 ];
int res;
int c;
void * hinit;
void * hrdr;
unsigned char rx_buf[ 2048 ];

int READ_MODE  	    = 0 ;
int WRITE_MODE  	= 0 ;
int FORMAT_MODE 	= 0 ;
int READ_ALL_MODE 	= 0 ;
int BEEP 	        = 0 ;
















//=====================================================================================================================
//--------------------------------------------------------
// Authorize sector
//--------------------------------------------------------
//  	ID  Item 		Length [bytes]
//--------------------------------------------------------
//  	1 	Opcode 			1
//  	2 	Mode 			1
//  	3 	Key number 		1
//  	4 	Sector number 	1
//--------------------------------------------------------
int old_sector=-1;

int Authorize_sector(int sector)
{
		unsigned char mfc_auth[] = { 0xB0, 0x00, 0x00, 0x01 };


		if(sector == old_sector ) return(0);

		mfc_auth[3] =sector;

		if( ( res = oti_ioctl( hrdr, OTI_IOCTL_MIFARE_COMMAND, ( const void *)mfc_auth, sizeof(mfc_auth), ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
		{
		    printf( "oti_ioctl error  err %X\n", errno);
			return -1;
		}
		if(Debug) printf( "oti_ioctl: OTI_IOCTL_MIFARE_COMMAND (%02X): OK\n", mfc_auth[0]);

		old_sector = sector;
		return(0);
}
//=====================================================================================================================





//=====================================================================================================================

//--------------------------------------------------------
// Load default KEY (A9) - pg 131
//--------------------------------------------------------
//  Data Order 	 Data Item 		Data Length [bytes]
//--------------------------------------------------------
//  	1 			Opcode 			1
//  	2 			Mode 			1
//  	3 			Key number 		1
// 	 	4 			Transport 		6
//  	5 			New key 		6
//--------------------------------------------------------


int Load_Default_Key( void )
{
//		unsigned char mfc_key[] = { 0xA9, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		unsigned char mfc_key[] = { 0xA9, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

		res = oti_ioctl( hrdr, OTI_IOCTL_MIFARE_COMMAND, ( const void *)mfc_key, sizeof(mfc_key), ( void * ) rx_buf, sizeof( rx_buf ) ) ;
		if( res  < 0 )
		{
		    printf( "oti_ioctl error  err %d  \n\r", res );
			return (-1);
		}

		printf( "Load Default Key\n\r");
	    printf( "    Opcode     : %02X \n\r", mfc_key[0]);
		printf( "    Mode       : %02X \n\r", mfc_key[1]);
		printf( "    Key number : %02X \n\r", mfc_key[2]);
		printf( "    Transport  : %02X %02X %02X %02X %02X %02X \n\r", mfc_key[3], mfc_key[4], mfc_key[5], mfc_key[6], mfc_key[7], mfc_key[8] );
		printf( "    New key    : %02X %02X %02X %02X %02X %02X \n\r", mfc_key[9], mfc_key[10], mfc_key[11], mfc_key[12], mfc_key[13], mfc_key[14] );
		printf( "\n\r");


		return(0);
}

//=====================================================================================================================
















//=====================================================================================================================
int read_sector_block(int sector , int block)
{
		int block_Index;


		Authorize_sector(sector);


	    if (sector < 32)
    	{
        	block_Index = sector * 4 + block;
	    }
    	else
    	{
    	    block_Index =  ( ((sector - 32) * 16) + 128) + block;
	    }


		//---------------------------------
		// Read and dump block 1
		//---------------------------------
		unsigned char tx_buf[] = { 0xA5, 0x00 };
		tx_buf[1] = block_Index;


		memset(rx_buf,0x00,sizeof(rx_buf));
		//------------------------------------------
		if( ( res = oti_ioctl( hrdr, OTI_IOCTL_MIFARE_COMMAND, ( const void *)tx_buf, sizeof(tx_buf), ( void * ) rx_buf, sizeof( rx_buf ) ) ) < 0 )
		{
		    printf( "oti_ioctl error  err %X\n", errno);
			return -1;
		}
		//------------------------------------------

		printf("Sector:%02d Block:%03d ", sector , block_Index);

		for (int n = 1; n < 17; n++)
		{
			printf("%02X ", (unsigned char) rx_buf[n]);
		}

		printf("        ");

		for (int n = 1; n < 17; n++)
		{
			c = rx_buf[n] & 0x0ff;

			if ( isprint(c)  )
				printf("%c", c);
			else
				printf(".");
		}
		printf("\n\r");
		//------------------------------------------
		return(0);
}
//=====================================================================================================================



int decode_sector_trailer( int sector )
{
	int block_Index;
	int Access_Bits;
	int c13,c12,c11,c10;
	int c33,c32,c31,c30;
	int c23,c22,c21,c20;


	//---------------------------------------------------------------
	if (sector < 32)
      	block_Index = sector * 4 + 3;
   	else
   	    block_Index =  ( ((sector - 32) * 16) + 128) + 15;
	//---------------------------------------------------------------
	Authorize_sector(sector);

	//---------------------------------
	// Read and dump block 1
	//---------------------------------
	unsigned char tx_buf[] = { 0xA5, 0x00 };
	tx_buf[1] = block_Index;


	memset(rx_buf,0x00,sizeof(rx_buf));
	//------------------------------------------
	if( ( res = oti_ioctl( hrdr, OTI_IOCTL_MIFARE_COMMAND, ( const void *)tx_buf, sizeof(tx_buf), ( void * ) rx_buf, sizeof( rx_buf ) ) ) < 0 )
	{
	    printf( "oti_ioctl error  err %X\n", errno);
		return -1;
	}
	//------------------------------------------

	//------------------------------------------
	printf("Sector:%02d Block:%03d ", sector , block_Index);
	for (int n = 1; n < 17; n++)
	{
		printf("%02X ", (unsigned char) rx_buf[n]);
	}

	printf("        ");

	for (int n = 1; n < 17; n++)
	{
		c = rx_buf[n] & 0x0ff;

		if ( isprint(c)  )
			printf("%c", c);
		else
			printf(".");
	}
	printf("\n\r");
	//------------------------------------------

	printf("Sector %02d , KEY_A       = %02X %02X %02X %02X %02X %02X\n\r",sector,
		rx_buf[1],
		rx_buf[2],
		rx_buf[3],
		rx_buf[4],
		rx_buf[5],
		rx_buf[6] );



	printf("Sector %02d , KEY_B       = %02X %02X %02X %02X %02X %02X\n\r",sector,
		rx_buf[11],
		rx_buf[12],
		rx_buf[13],
		rx_buf[14],
		rx_buf[15],
		rx_buf[16] );


	Access_Bits = 	(rx_buf[10] <<  0) +
		   			(rx_buf[ 9] <<  8) +
		   			(rx_buf[ 8] << 16) +
		   			(rx_buf[ 7] << 24) ;


	printf("Sector %02d , Access_bits = %08X \n\r",sector, Access_Bits);


	c13=(Access_Bits >> 23) & 0x01;
	c12=(Access_Bits >> 22) & 0x01;
	c11=(Access_Bits >> 21) & 0x01;
	c10=(Access_Bits >> 20) & 0x01;

	c33=(Access_Bits >> 15)&0x01;
	c32=(Access_Bits >> 14)&0x01;
	c31=(Access_Bits >> 13)&0x01;
	c30=(Access_Bits >> 12)&0x01;

	c23=(Access_Bits >> 11)&0x01;
	c22=(Access_Bits >> 10)&0x01;
	c21=(Access_Bits >>  9)&0x01;
	c20=(Access_Bits >>  8)&0x01;

	printf("c13,c12,c11,c10 : %01d %01d %01d %01d\n\r",c13,c12,c11,c10);
	printf("c23,c22,c21,c20 : %01d %01d %01d %01d\n\r",c23,c22,c21,c20);
	printf("c33,c32,c31,c30 : %01d %01d %01d %01d\n\r",c33,c32,c31,c30);



	printf(" Block 0 C10 C20 C30 : %01d %01d %01d \n\r",c10,c20,c30);
	printf(" Block 1 C11 C21 C31 : %01d %01d %01d \n\r",c11,c21,c31);
	printf(" Block 2 C12 C22 C32 : %01d %01d %01d \n\r",c12,c22,c32);
	printf(" Block 3 C13 C23 C33 : %01d %01d %01d \n\r",c13,c23,c33);


	return (0);
}






//=====================================================================================================================
int write_sector_block ( int sector, int block , char *buf ,int len )
{
	unsigned char w_buf[18];
	int res;
	int i;
	int block_Index;

    if (sector < 32)
       	block_Index = sector * 4 + block;
   	else
   	    block_Index =  ( ((sector - 32) * 16) + 128) + block;


	if ( len < 0 || len > 16 )
	{
	    printf( "block size error: incorrect len:( %d )\n\r", errno);
		return(-1);
	}

	if( ((sector < 32 ) && ( block >= 3)) ||  ((sector >= 32 ) && ( block >= 15 ))  )
	{
		printf ("*** Last block restricted *** \n\r");
		return (-1);
	}

	printf("WRITE BLOCK: %d\n\r",block_Index );
	printf("Block_ID   : %d \n\r",block_Index);
	printf("Buf length : %d \n\r",len);
	printf("Buf         : ");
	for ( i=0; i < len; i++ ) printf("%02X ",buf[i]);
	printf("\n\r\n\r");


	Authorize_sector(sector);

	memset(w_buf,0x00,sizeof(w_buf));
	w_buf[0] = 0xA6;
	w_buf[1] = block_Index; // Block

	memcpy(&w_buf[2],buf,len);
	memset(rx_buf , 0x00 ,sizeof(rx_buf));

	if( ( res = oti_ioctl( hrdr, OTI_IOCTL_MIFARE_COMMAND, ( const void *)w_buf, sizeof(w_buf), ( void * ) rx_buf, sizeof( rx_buf ) ) ) < 0 )
	{
	    printf( "oti_ioctl error  err %X\n", errno);
		return -1;
	}

	if(Debug) printf( "oti_ioctl: OTI_IOCTL_MIFARE_COMMAND (%02X): OK\n", w_buf[0]);

	return(0);
}
//=========================================================================================================================================





int vers[] = {
		OTI_VERSION_HOST_PROTOCOL,
		OTI_VERSION_FIRMWARE,
		OTI_VERSION_RF_UNIT,
		OTI_VERSION_COMM_UNIT,
    	OTI_VERSION_SMART_CARD_UNIT,
		OTI_VERSION_GPIO_UNIT,
		OTI_VERSION_EEPROM_UNIT,
		OTI_VERSION_UTILITIES
};

//=========================================================================================================================================
int Show_version( void )
{
	int ver = OTI_VERSION_HOST_PROTOCOL;

	memset(rx_buf,0x00, sizeof( rx_buf ));


	res = oti_ioctl( hrdr, OTI_IOCTL_GET_VERSION, ( const void *)&ver, sizeof( ver ), ( void * ) rx_buf, sizeof( rx_buf ) ) ;

	if( res  < 0 )
	{
		printf( "oti_ioctl error  err %d  \n\r", res );
	    return -1;
	}
	printf( "OTI_IOCTL_GET_VERSION           : %s\n" , rx_buf );
	return(0);
}
//=========================================================================================================================================





//=========================================================================================================================================
int Show_PCD_Serial_Number( void )
{
	memset(rx_buf,0x00, sizeof( rx_buf ));

    res = oti_ioctl( hrdr, OTI_IOCTL_GET_PCD_SERIAL_NUMBER, NULL, 0 , ( void * ) rx_buf, sizeof( rx_buf ) ) ;

    if( res  < 0 )
	{
		printf( "oti_ioctl error  err %d \n\n", res );
        return -1;
    }
    printf( "OTI_IOCTL_GET_PCD_SERIAL_NUMBER : %s\n", rx_buf );
	return(0);
}
//=========================================================================================================================================









//=============================================================================================================================
void show_help(void)
{
    printf("\n\r");
    printf("cardinfo (%s)\n\r",Build_dateTime() );
    printf("-----------------------------\n\r");
    printf("usage: cardinfo <options> <...>\n\r");
    printf("Options:\n\r");
    printf("    -h                  : This info                 \n\r");
    printf("    -debug              : Debug on                  \n\r");
    printf("    -format             : format sectors 1->39      \n\r");
    printf("    -readall            : read all blocks 0->255    \n\r");
    printf("    -w <sector> <data>  : sector ( 1 -31 ) Data:48  \n\r");
    printf("                        : sector (31 -39 ) Data:240 \n\r");
    printf("    -r <sector>         : read a sector             \n\r");
    printf("    -beep               : beep on read/write        \n\r");

    exit(0);
}
//=============================================================================================================================












//=========================================================================================================================================
int main ( int argc, char **argv )
{

    int res;
    OtiCardInfo	cardinfo;
	unsigned char buf[512];
	int i;
	int len;
    int cnt, err;
	char *p;
    int ret;

    int sector  =   0 ;
    int datalen =   0 ;


    //=============================================================================================
	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		if( p[0] != '-')break;

        if( ! strcmp ( p , "-h"         )){ show_help()         ; continue; }
 		if( ! strcmp ( p , "-debug"     )){ Debug = 1           ; continue; }
 		if( ! strcmp ( p , "-format"    )){ FORMAT_MODE =1      ; continue; }
 		if( ! strcmp ( p , "-readall"   )){ READ_ALL_MODE =1    ; continue; }
 		if( ! strcmp ( p , "-beep"      )){ BEEP = 1            ; continue; }


		if( ! strcmp(p,"-w") && (argc > i+2) )
		{

			sector=atoi( argv[i+1] );
            if( sector < 1  || sector > 39 )
			{
				printf("Wrong sector (1-39): secected :%d \n\r", sector );
				exit(0);;
			}

			datalen=strlen(argv[i+2]);
			if(datalen > 48 )
			{
				printf("Data overflow: size (%d) greater than 48 chars \n\r", datalen );
				exit(0);;
			}

			memset ( aData , 0x00, sizeof(aData));
			memcpy ( aData , argv[i+2] , datalen );
			WRITE_MODE =  1;

            printf("sector   : <%d>\n\r" , sector );
            printf("data[%02d] : <%s>\n\r" , datalen,aData );

			i+=2;
            
            continue;
		}



		if(!strcmp(p,"-r") && (argc > i+1))
		{
			sector=atoi( argv[i+1] );
            if( sector < 1  || sector > 39 )
			{
				printf("Wrong sector (1-39): secected :%d \n\r", sector );
				exit(0);;
			}
            printf("sector   : <%d>\n\r" , sector );
			READ_MODE =  1;
			i+=1;
            continue;
		}


	}
    //=============================================================================================








    if( ( hinit = oti_init() ) == NULL ){
        printf( "oti_init error\n");
        return -1;
    }
    if(Debug) printf( "oti_init OK\n");


    if( ( hrdr = oti_open( hinit, & otiopen )) == NULL ) {
        printf( "oti_open error\n");
        return -1;
    }
    if(Debug) printf( "oti_open OK\n");



    if( Debug )
    {
        printf( "\n\r\n\r");
        printf( "=============================================================\n");
        printf( "Reader open                     : OK\n");
    	Show_version();
    	Show_PCD_Serial_Number();
        printf( "=============================================================\n");
    }


	// Poll Pending
	buf[0]=3;
	buf[1]=0;
	err = 0;
    if( ( res = oti_ioctl( hrdr, OTI_IOCTL_POLL_START, ( const void *)buf, 1, ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
	{
        ++err;
        printf( "oti_ioctl error  err %d  cnt %d\n", err, cnt );
	    return -1;
    }
    if(Debug)  printf( "oti_ioctl: OTI_IOCTL_POLL_START : OK\n");






	// Wait for CARD
	printf("Present a card .....\n\r");

	memset(&cardinfo, 0x00, sizeof( cardinfo ));

	// Wait for 10 seconds
	buf[0] = 0x10;
	buf[1] = 0x27;
	buf[2] = 0;
	buf[3] = 0;
	for( cnt = err = 0; cnt < 1; ++cnt )
	{
	   if( ( res = oti_ioctl( hrdr, OTI_IOCTL_WAIT_FOR_CARD, buf,4,( void * ) &cardinfo, sizeof( cardinfo ) ) ) < 0 ){
            //printf( "oti_ioctl: OTI_IOCTL_WAIT_FOR_CARD : error  err %d  cnt %d\n", err, cnt );
			// usleep (100000);
			continue;
	    }
		break;
	}

	// Printf info
	if (res > 0)
	{
		len = cardinfo.length;

        if( Debug )
        {
    		printf("\n\rRX[%d] : ",len);
	    	for( i=0; i < len ; i++)
	    	{
	    			printf("%02X ",cardinfo.data[i]&0x0ff) ;
	    	}
	    	printf("\n\r");
        }

        if( Debug )
        {
    		for( i=0; i < len ; i++)
	    	{
	    	    if (cardinfo.data[i] == 0xDF)
	    		{
	    			if (cardinfo.data[i+1] == 0x16)
	    			{
	    				printf("Detected PICC type            : [ %c - %c ] \n\r",cardinfo.data[i+3], cardinfo.piccType & 0x7F );
	    			}
	    		}
	    	}
        }
	}


	//
	// Check the PICCTYPE for MIFARE
	// This is testcode so... horrible
	//
	if (cardinfo.piccType == 'M' || cardinfo.piccType == 'A')
	{
		// Print the UID
		if (cardinfo.uid )
		{
            if(Debug)
            {
    			printf("UID of card                   :");
	    		for (size_t n = 0; n < cardinfo.length_uid; n++)
	    		{
	    			printf(" %02X", (unsigned char) cardinfo.uid[n]);
	    		}
	    		printf("\r\n");
            }
		}
		printf("\r\n");
        printf( "=============================================================\n");

		Load_Default_Key();

        printf( "=============================================================\n");
		printf("\r\n");

		if( WRITE_MODE == 1 )
		{
			write_sector_block ( sector , 0 , &aData[  0] , 16 );
			write_sector_block ( sector , 1 , &aData[ 16] , 16 );
			write_sector_block ( sector , 2 , &aData[ 32] , 16 );

			read_sector_block(sector,0);
			read_sector_block(sector,1);
			read_sector_block(sector,2);

		}
		else if	( READ_MODE  == 1)
        {

            //=======================================================================================
			// READ ALL BLOCK
			//=======================================================================================

            if( sector > 0 && sector < 32 )
            {
				for (int blk = 0; blk < 4 ; blk++)
					read_sector_block(sector,blk);
            }

            if( sector >= 32 && sector < 40 )
			{
				for (int blk = 0; blk < 16 ; blk++)
					read_sector_block(sector,blk);
			}

        }
		else if	( FORMAT_MODE  == 1)
		{
			for (int sec=1; sec < 40 ; sec++ )
			{
				sprintf(aData,"SECTOR %02d Blk 1 ",sec);
				write_sector_block ( sec , 0 , &aData[  0] , 16 );
			}


			for (int sec=1; sec < 40 ; sec++ )
			{
				for (int blk = 0; blk < 4 ; blk++)
					read_sector_block(sec,blk);
			}
		}
		else if ( READ_ALL_MODE == 1 )
		{
			//=======================================================================================
			// READ ALL BLOCK
			//=======================================================================================
			for (int sec=0; sec< 32 ; sec++ )
			{
				for (int blk = 0; blk < 4 ; blk++)
					read_sector_block(sec,blk);
			}

			for (int sec=32; sec< 40 ; sec++ )
			{
				for (int blk = 0; blk < 16 ; blk++)
					read_sector_block(sec,blk);
			}
		}
		else
		{
				printf("Read sector 1\n\r");
				read_sector_block(1,0);
				read_sector_block(1,1);
				read_sector_block(1,2);
				read_sector_block(1,3);
				printf("\n\r");

				printf("Read sector trailer 1\n\r");
				decode_sector_trailer( 1 ) ;
				printf("\n\r");
		}


	}

    if( BEEP )
    {
        ret=system("beep");
	    if(ret) printf("system(beep) error\n\r");
    }

	// Stop Polling
    if( ( res = oti_ioctl( hrdr, OTI_IOCTL_POLL_STOP, ( const void *)buf, 1, ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
	{
        printf( "oti_ioctl error  err %d  cnt %d\n", err, cnt );
	    return -1;
    }
    if(Debug) printf( "oti_ioctl: OTI_IOCTL_POLL_STOP : OK\n");

	sleep(1);

	// Close reader
    if( ( res = oti_close( hrdr )) < 0 ) {
        printf( "oti_close error\n");
        return -1;
    }

    if(Debug) printf( "oti_close: OK\n");

	// deinit LIB
    if( ( res = oti_deinit( hinit )) < 0 ) {
        printf( "oti_deinit error\n");
        return -1;
    }
    if(Debug)  printf( "oti_deinit: OK\n");

    return 0;
}
