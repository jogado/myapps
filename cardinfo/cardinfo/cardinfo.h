#ifndef _CARDINFO_H
#define _CARDINFO_H

//====================================================================
// Prototypes
//====================================================================
extern int read_sector_block		( int sector , int block );
extern int write_sector_block 		( int sector , int block , char *buf ,int len );
extern int Authorize_sector			( int sector );
extern int Show_PCD_Serial_Number	( void );
extern int Show_version				( void );
extern int Load_Default_Key			( void );
extern int decode_sector_trailer	( int sector );

extern void show_help               ( void );

#endif
