/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_ndot.c
 *
 *  Created on: Jan 12, 2011
 *      Author: wmeganck
 */

#include <errno.h>
#include <string.h>

#include "oti_ndot.h"

/**
 * This function will initiate the polling for a NDoT card.
 *
 * @param reader	pointer to reader control block.
 * @param opId		pointer to NDoTOperatorId structure.
 * @param txLen		length of the transmit data buffer.
 * @param rxData	pointer to receive data buffer.
 * @param rxLen		length of the receive data buffer.
 */
int oti_ndot_poll( POtiReaderCb reader, NDoTOperatorId* opId, size_t txLen, POtiCardInfo cardinfo )
{
	int result = 0;
	char data[sizeof(NDoTOperatorId)];
	char *pData = data;
	char* rx = reader->trx.rx;
	size_t rxLen = sizeof( reader->trx.rx );

	
	memcpy(pData, opId->country, 2);
	pData += 2;
	memcpy(pData, opId->province, 2);
	pData += 2;
	*(pData++) = opId->municipalityType;
	*(pData++) = (opId->municipality >> 8) & 0xFF;
	*(pData++) = opId->municipality & 0xFF;
	memcpy(pData, opId->oper, 5);
	pData += 5;

	result = oti_handle_tag( reader, DO, OTI_NDOT_POLL, data, pData-data, rx, rxLen, OtiTlvSizeAdd );

	if (result > 0)
	{
		result = oti_copy_data_2_tag_struct( rx, result, cardinfo);
	}

	return check_oti_handle_tag_answer(result);

}

/**
 * This function will make a NDoT EMV transaction.
 *
 * @param reader	pointer to reader control block.
 * @param txData	pointer to transmit data buffer.
 * @param txLen		length of the transmit data buffer.
 * @param rxData	pointer to receive data buffer.
 * @param rxLen		length of the receive data buffer.
 */
int oti_ndot_emv( POtiReaderCb reader, char* txData, size_t txLen, POtiCardInfo cardinfo )
{
	int result = 0;
	char* rx = reader->trx.rx;
	size_t rxLen = sizeof( reader->trx.rx );

	result = oti_handle_tag( reader, DO, OTI_NDOT_EMV, txData, txLen, rx, rxLen, OtiTlvSizeAdd );

	if (result > 0)
	{
		result = oti_copy_data_2_tag_struct( rx, result, cardinfo);
	}

	return check_oti_handle_tag_answer(result);

}

