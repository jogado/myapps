#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <oti.h>
//#include <stm32.h>

OtiOpen otiopen = {
    .path = "/dev/oti",
    .baudrate = 115200,
    .interfaceType = OtiReaderInterfaceTypeSTM32,
};

int vers[] = { OTI_VERSION_HOST_PROTOCOL, OTI_VERSION_FIRMWARE, OTI_VERSION_RF_UNIT, OTI_VERSION_COMM_UNIT, 
    OTI_VERSION_SMART_CARD_UNIT, OTI_VERSION_GPIO_UNIT, OTI_VERSION_EEPROM_UNIT, OTI_VERSION_UTILITIES };


extern void show_buf	( char *Header, char *buf, int len);

void show_buf( char *Header, char *buf, int len)
{
	int i;

	printf("%s",Header);
	for(i=0;i<len; i++)
		printf("%02X ",buf[i]&0x0FF);
	printf("\n\r");

}



char versbuf[ 2096 ];
char buf 	[ 2096 ];

int main( void ){
    void * hinit;
    void * hrdr;
    int res;
    
	int i;
	int len;
	unsigned char c;
	int size;
    int cnt=0;


    if( ( hinit = oti_init() ) == NULL ){
        printf( "oti_init error\n");
        return -1;
    }
    printf( "oti_init OK\n");


    if( ( hrdr = oti_open( hinit, & otiopen )) == NULL ) {
        printf( "oti_open error\n");
        return -1;
    }
    printf( "oti_open OK\n");



	memset(versbuf,0x00, sizeof( versbuf ));
	if( ( res = oti_ioctl( hrdr, OTI_IOCTL_GET_VERSION, ( const void *)& vers[ cnt % 8 ], sizeof( vers[0] ), ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
	{
	    printf( "oti_ioctl error  cnt %d\n", cnt );
        return -1;
	}
    printf( "OTI_IOCTL_GET_VERSION : %s\n", versbuf );


	memset(versbuf,0x00, sizeof( versbuf ));
    if( ( res = oti_ioctl( hrdr, OTI_IOCTL_GET_PCD_SERIAL_NUMBER, ( const void *)& vers[ cnt % 8 ], sizeof( vers[0] ), ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 ){
            printf( "oti_ioctl error    cnt %d\n", cnt );
        return -1;
    } 
    printf( "OTI_IOCTL_GET_PCD_SERIAL_NUMBER : %s\n", versbuf );



	buf[0]=3;
	buf[1]=0;

    if( ( res = oti_ioctl( hrdr, OTI_IOCTL_POLL_START, ( const void *)buf, 1, ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
	{
        printf( "oti_ioctl error  cnt %d\n", cnt );
	    return -1;
    } 
    printf( "oti_ioctl: OTI_IOCTL_POLL_START : OK\n");



	memset(versbuf,0x00, sizeof( versbuf ));
	for( cnt = 0; cnt < 100; ++cnt ) 
	{
	   	if( ( res = oti_ioctl( hrdr, OTI_IOCTL_WAIT_FOR_CARD, buf,sizeof(buf),( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
		{
			usleep (100000);
			continue;
	    }
		break;
	}
	printf("res=  %d\n\r",res);

	len =50;

	show_buf((char *)"RX:",versbuf,len);

	i=0;

	while(1)
	{
		c = versbuf[i] & 0x0ff ;

		if (c != 0xDF)
		{
			break;
		}

		size = (versbuf[i+2] & 0x0ff)+3;
		show_buf((char*)"    ", &versbuf[i], size);

		i+=size;
	}
	printf("\n\r");



    if( ( res = oti_ioctl( hrdr, OTI_IOCTL_POLL_STOP, ( const void *)buf, 1, ( void * ) versbuf, sizeof( versbuf ) ) ) < 0 )
	{
        printf( "oti_ioctl error cnt %d\n", cnt );
	    return -1;
    } 
    printf( "oti_ioctl: OTI_IOCTL_POLL_STOP : OK\n");



    if( ( res = oti_close( hrdr )) < 0 ) {
        printf( "oti_close error\n");
        return -1;
    }
    if( ( res = oti_deinit( hinit )) < 0 ) {
        printf( "oti_deinit error\n");
        return -1;
    }
    return 0;
}
