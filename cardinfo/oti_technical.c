/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_technical.c
 *
 *  Created on: Oct 21, 2008
 *      Author: jvolckae
 */


#include <oti.h>

#include "oti_reader.h"
#include "oti_command.h"
#include "oti_technical.h"
#include "oti_usb.h"


/**
 * Function that will reset the reader to its factory defaults.
 *
 * @param reader	pointer to reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_restore_factory_setttings( POtiReaderCb reader )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_RESTORE_FACTORY_SETTINGS, NULL, 0, NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}



/**
 * Function that will reset the PCD.
 *
 * @param reader	pointer to reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_reset_pcd( POtiReaderCb reader )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_RESET_PCD, NULL, 0, NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}



/**
 * Function that will activate the Default CD configuration.
 *
 * @param reader	pointer to reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_load_default_parameters( POtiReaderCb reader )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_LOAD_DEFAULT_PARAMETERS, NULL, 0, NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}



/**
 * Function that will get version information from the OTI reader.
 * The version information is returned as an ASCII string.
 *
 * @param reader	pointer to reader control block.
 * @param version	version information that needs to be retrieved.
 * @param data		pointer to data buffer where the version information is stored.
 * @param length	length of the data buffer.
 */
int oti_get_version( POtiReaderCb reader, OtiVersion version, char* data, size_t length )
{
	int result = 0;
	char tx[1] = { (char)version };

	result = oti_handle_tag( reader, GET, OTI_VERSIONS, tx, sizeof( tx ), data, length, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}



/**
 * Function that will get the delay information between PollA and PollB.
 * The delay is returned.
 *
 * @param reader	pointer to reader control block.
 * @param data		pointer to data buffer where the version information is stored.
 * @param length	length of the data buffer.
 */
int oti_get_getdelay( POtiReaderCb reader, char* data, size_t length )
{
	int result = 0;
	char tx[2] = {0x08, 0x00};

	result = oti_handle_tag( reader, GET, OTI_GET_DELAY, tx, 2, data, length, OtiTlvSizeNo );

	return check_oti_handle_tag_answer(result);
}



/**
 * Function that will put the the reader in boot loader mode.
 *
 * @param reader	pointer to reader control block.
 * @param data		pointer to data buffer where the version information is stored.
 * @param length	length of the data buffer.
 */
int oti_call_boot_loader( POtiReaderCb reader, char* data, size_t length )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_BOOT_LOADER, NULL, 0, data, length, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}


/**
 * Function to send flash packages to the OTI boot loader.
 *
 * @param reader	pointer to reader control block.
 * @param txData	pointer to transmit data buffer.
 * @param txLen		length of the transmit data buffer.
 * @param rxData	pointer to receive data buffer.
 * @param rxLen		length of the receive data buffer.
 */
int oti_flash_packet( POtiReaderCb reader, char* txData, size_t txLen, char* rxData, size_t rxLen )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_FLASH_PACKET, txData, txLen, rxData, rxLen, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}

/**
 * Function that will put the the reader back in run mode.
 *
 * @param reader	pointer to reader control block.
 * @param data		pointer to data buffer where the version information is stored.
 * @param length	length of the data buffer.
 */
int oti_run_application( POtiReaderCb reader, char* data, size_t length )
{
	int result = 0;

	result = oti_handle_tag( reader, DO, OTI_RUN_APPLIC, NULL, 0, data, length, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}

/**
 * This function send a command to the OTI reader
 *
 * @param reader				pointer to reader control block.
 * @param code					SET, GET or DO
 * @param tx					pointer to transmit data buffer
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */
int oti_reader_command( POtiReaderCb reader, OpCode code, OtiTag otiTag, char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;

	if ( (code == GET) || (code == SET) || (code == DO) )
		result = oti_handle_tag( reader, code, otiTag, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}
/**
 * This function sends a raw command to the OTI reader.
 * It has been added to deal with an updated OTI API before support inclusion in this library
 * The user supplies the frame, starting with the opcode (SET, GET, DO), followed by the TAG, LENGTH and VALUE
 * This function takes the first byte as the opcode, the next byte(s) as tag, takes the length bytes and uses the 
 * usual functions for the remainder of handling.
 * Tag byte rules: First tag byte 5 ls bits == 0x1F ? At least two byte tag
 * Special case: DFA1 and DF81 are 3 byte tags
 *
 * @param reader				pointer to reader control block.
 * @param tx					pointer to transmit data buffer
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */
int oti_reader_raw_command( POtiReaderCb reader, char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;
	char code;
	unsigned otiTag;
	char *pTx=tx;
	size_t size=txLen,payload;
	if( (pTx == NULL) || (size < 2) ) {
		errno = EINVAL;
		result = -1;
		return result;
	}  
	code = *pTx; pTx++; size--;
	if ( (code == GET) || (code == SET) || (code == DO) ) {
		otiTag = *pTx; pTx++; size--;
		if( (otiTag&0x1f) == 0x1f) {	/* is it longer than 1 byte ? */
			if( size > 0) {
				otiTag = (otiTag << 8)|(*pTx); pTx++; size--;
			} else {
				errno = EINVAL;
				result = -1;
			}
			if( (otiTag == 0xdf81) || (otiTag == 0xdf82) || (otiTag == 0xdf83) || (otiTag == 0xdfa1) ) {
				if( size > 0 ) {
					otiTag = (otiTag << 8)|(*pTx); pTx++; size--;
				} else {
					errno = EINVAL;
					result = -1;
				}
			}
		}
		payload = *pTx; pTx++; size--;
		if( payload == 0x81) { payload = *pTx; pTx++; size--; }
		else if( payload == 0x82) { payload = ((*pTx)<<8)|(*(pTx+1)); pTx += 2; size -= 2; }
		if( payload != size ) {
			errno = EINVAL;
			result = -1;
		}
		dprintf(" >>%s result=%d tag:%x size:%d %02x...>>\n",__func__,result,otiTag,size,*pTx);
		if( result >= 0 ) result = oti_handle_tag( reader, code, otiTag, pTx, size, rx, rxLen, OtiTlvSizeAdd );
		dprintf(" <<%s raw result:%d %02x...<<\n",result,*rx);
	} else {
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}
