/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_reader.c
 *
 *  Created on: Oct 13, 2008
 *      Author: jvolckae
 *  Modification history.
 *        Date		Author	Description
 *        April 24, 2015 JB	Add infinite blocking poll and way to stop it
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/file.h>

#if defined INTERFACE_STM32
#include <stm32.h>
#include <stm32_api.h>
#endif

#include "oti_reader.h"
#include "oti_trace.h"

/**
 * This method will initialize the current device context.
 *
 * @remark This method must be called before the open or deinit method.
 *
 * @param void
 *
 * @return 	If NULL is returned an error occured, check errno for more details.<br>
 * 			If everything succeeded a handle is returned that must be used when
 * 			calling oti_reader_deinit or oti_reader_open.
 */
POtiCb oti_reader_init( void )
{
	POtiCb oti = NULL;
	struct tms tm;

	/*-- allocate memory for oti control block --*/
	oti = (POtiCb)calloc( 1, sizeof( OtiCb ) );

	/*-- set resolution timer --*/
	oti->timerRes = 1000 / sysconf(_SC_CLK_TCK);
	oti->tmOffset = times( &tm );

	/*-- initialise mutex for the control block --*/
	if ( pthread_mutex_init( &oti->mutex, NULL ) != 0 )
	{
		fprintf(stderr,"%s unable to create mutex - %s\n",__func__,strerror(errno));
		free( oti );
		oti = NULL;
	}

	//usleep( 1*1000*1000 );

	return ( oti );
}



/**
 * This method will deinitialise the given oti context.
 *
 * @brief 	The method will close all open readers within the context
 * 			and cleanup all allocated memory.
 *
 * @param otiHandle	pointer that was returned by oti_reader_init.
 *
 * @return 	If the function fails -1 is returned. Check errno for a more
 * 			detailed error code. O is returned when the fuction succeeds.
 */
int oti_reader_deinit( POtiCb oti )
{
	int result = 0;
	int nReaders = 0;
	int i;

	/*-- sanity check --*/
	if ( oti == NULL )
	{
		errno = EACCES;
		result = -1;
	}
	else
	{
		/*-- get mutex to gain access to oti control block --*/
		pthread_mutex_lock( &oti->mutex );

		/*-- close all open reader --*/
		if ( oti->nReaders > 0 )
		{
			/* store the currently available number of readers
			 * the nReaders in the oti control block will decrement
			 * closing the readers one by one.
			 */
			nReaders = oti->nReaders;

			/*-- select the first reader in the list --*/
			for ( i = 0 ; i < nReaders ; i++ )
			{
				/* we will keep on closing the first reader,
				 * since the close function will shift the reader's control blocks.
				 */
				if ( oti->reader != NULL )
				{
					/*-- close reader --*/
					oti_reader_close( oti->reader );
				}
				else
				{
					break;
				}
			}
		}

		pthread_mutex_unlock( &oti->mutex );
		pthread_mutex_destroy( &oti->mutex );
		free( oti );
	}

	return ( result );
}


/**
 * This method will open the serial device and
 * initialize it to work with the OTI reader.
 *
 * @param otiHandle		Handle to the oti context.
 * 						Handle that is returned by oti_reader_init.
 * @param settings		Oti reader settings.
 *
 * @return 	If NULL is returned an error occurred while opening the OTI reader.<br>
 * 			If no error occured a handle is returned that must be used to communicate to the OTI reader.
 */

static int oti_reader_open_serial( POtiReaderCb reader, const POtiReaderOpen settings )
{
    int result = -1;

    /*-- open the communication port to the reader --*/

    reader->fd = open(settings->path, O_RDWR | O_NONBLOCK | O_NOCTTY);
    if ( reader->fd == -1 )
    {
//      fprintf(stderr,"%s unable to open reader %s - %s",__func__,settings->path,strerror(errno) );
        goto oti_reader_open_serial_error;
    }
    if( (result = flock(reader->fd,LOCK_EX|LOCK_NB)) < 0 ) {
	fprintf(stderr, "%s flock - %s\n",__func__,strerror(errno) );
	goto oti_reader_open_serial_error;
    }
    /*-- set the communication port settings --*/
    memset( &reader->options, 0, sizeof( reader->options ) );
    reader->options.c_iflag = IGNPAR;
    reader->options.c_cflag = CREAD | CLOCAL | CS8;
    reader->options.c_cc[VMIN] = 1;

    cfsetispeed( &reader->options, settings->baudrate );
    cfsetospeed( &reader->options, settings->baudrate );

    /*-- Update the options and do it NOW --*/
    result = tcsetattr( reader->fd, TCSANOW, &reader->options );
    if ( result == -1 )
    {
        /* error occurred while retrieving device options */
        fprintf(stderr,"%s unable to set options - %s\n",__func__,strerror(errno) );
        goto oti_reader_open_serial_error;
    }
    /*-- Clear the line --*/
    result = tcflush( reader->fd, TCIOFLUSH );
    if ( result == -1 )
    {
        fprintf(stderr,"%s unable to flush - %s\n",__func__,strerror(errno) );
        goto oti_reader_open_serial_error;
    }
    return result;

oti_reader_open_serial_error:
    if( reader->fd > 0 ) {
        close( reader->fd );
    }
    return result;
}

#if defined INTERFACE_STM32
static int oti_reader_open_stm32( POtiReaderCb reader, const POtiReaderOpen settings ){
    int result = -1;

    reader->fd = stm32_open(settings->path);
    if( reader->fd < 0 )
        {
        fprintf(stderr, "stm32_open error %d\n", reader->fd );
        goto oti_reader_open_stm32_error;
        }
    result = stm32_sync(reader->fd);
    if( result < 0 )
        {
        fprintf(stderr, "stm32_sync error %d\n", result );
        goto oti_reader_open_stm32_error;
        }
#if 0
    result = stm32_oti_uart_config( reader->fd, settings->baudrate, STM32_UART_PARITY_NONE, STM32_UART_STOP_BITS_1 );
    if( result < 0 )
        {
        fprintf(stderr, "stm32_oti_uart_config error %d\n", result );
        goto oti_reader_open_stm32_error;
        }
#endif
	usleep(200000);
	stm32_oti_flush_data(reader->fd);	
    return result;

oti_reader_open_stm32_error:
    if( reader->fd > 0 ) {
        close( reader->fd );
    }
    return result;
}
#endif

POtiReaderCb oti_reader_open( POtiCb oti, const POtiReaderOpen settings )
{
	POtiReaderCb reader = NULL;
	int result = 0;

	/*-- sanity check --*/
	if ( oti == NULL )
	{
		errno = EINVAL;
		fprintf(stderr, "%s Invalid otiHandle\n",__func__ );
	}
	else
	{
		pthread_mutex_lock( &oti->mutex );

		/*-- allocate a new control block for the reader --*/
		reader = (POtiReaderCb)calloc(1, sizeof(OtiReaderCb));

		/*-- if allocation succeeded, start initialising the structure --*/
		if ( reader == NULL )
		{
			fprintf(stderr,"%s unable to allocate memory for reader - %s\n",__func__,strerror(errno) );
			goto OpenError;
		}

        #if defined INTERFACE_STM32
        if( settings->interfaceType == OtiReaderInterfaceTypeSerial ){
            if( ( result = oti_reader_open_serial( reader, settings ) ) < 0 )
                goto OpenError;
        } else if( settings->interfaceType == OtiReaderInterfaceTypeSTM32 ) {
            if( ( result = oti_reader_open_stm32( reader, settings ) ) < 0 )
                goto OpenError;
        } else {
            fprintf(stderr, "%s unknown interface type : %d\n",__func__, settings->interfaceType );
            goto OpenError;
        }
        #else
        if( ( result = oti_reader_open_serial( reader, settings ) ) < 0 )
            goto OpenError;
        #endif

		/*-- create mutex for the reader --*/
		result = pthread_mutex_init( &reader->mutex, NULL );
		if ( result != 0 )
		{
			fprintf(stderr, "%s unable to create mutex - %s\n",__func__, strerror(errno));
			goto OpenError;
		}

		/*-- Create pipe for interruptible calls --*/
		if (pipe2(reader->pfd,O_NONBLOCK) == -1) {
			fprintf(stderr,"%s couldn't create pipe for interruptible calls - %s\n",__func__,strerror(errno));
			goto OpenError;
		}

		reader->wpfd[0] = reader->wpfd[1] = reader->rpfd[0] = reader->rpfd[1] = -1;
#if 1
	if( settings->mode == OtiReaderModeCallback ) {
#else
        if( settings->interfaceType == OtiReaderInterfaceTypeSerial ){
#endif
            /*-- Create pipe for write channel --*/
            if (pipe2(reader->wpfd,O_NONBLOCK) == -1) {
                fprintf(stderr,"%s couldn't create pipe for writing channel to oti thread - %s\n",__func__,strerror(errno));
                goto OpenErrorPipe;
            }
            /*-- Create pipe for read channel --*/
            if (pipe2(reader->rpfd,O_NONBLOCK) == -1) {
                fprintf(stderr,"%s couldn't create pipe for read channel from oti thread - %s\n",__func__,strerror(errno));
                goto OpenErrorPipe;
            }

            if ((result=pthread_create(&reader->ptid, NULL, oti_thread, reader)) != 0) {
                fprintf(stderr,"%s couldn't create oti thread - result=%d %s\n",__func__,result,strerror(errno));
                goto OpenErrorPipe;
            }
            fprintf(stderr,"%s: Starting oti_thread succeeded\n",__func__);
        }

		/*-- store open settings --*/
		memcpy( &reader->otiOpen, settings, sizeof( OtiReaderOpen ) );

		/*-- preconfigure timeouts --*/
		reader->timeout.write = OTI_READER_WRITE_TIMEOUT;
		reader->timeout.ack = OTI_READER_ACK_TIMEOUT;
		reader->timeout.response = OTI_READER_RESPONSE_TIMEOUT;

		reader->pollConfig.fwti_poll = SR_DEFAULT_FWTI_POLL;		/* default waiting time for SR ticket in extended poll */
		reader->pollConfig.fwti_remove = SR_DEFAULT_FWTI_REMOVE;	/* default waiting time for SR ticket in extended remove */
		reader->pollConfig.activating_poll = OtiPollImmediate;		/* default last poll: fastest poll method but remove fails on -4 */
		reader->pollConfig.tlc = reader->pollConfig.tlc_appli = OtiTransparentLayerPcd;	/* default oti: pcd is responsible */

		/*-- initialise select fd_sets --*/
#if 1
	if( settings->mode == OtiReaderModeCallback ) 
#else
        if( settings->interfaceType == OtiReaderInterfaceTypeSerial )
#endif
            reader->nfds = 1 + ( (reader->pfd[0] > reader->rpfd[0]) ? reader->pfd[0] : reader->rpfd[0] );
        else
            reader->nfds = 1 + ( (reader->pfd[0] > reader->fd) ? reader->pfd[0] : reader->fd );

		/* add the reader control block to the oti control block
		 * the reader will be added in front off the list, this
		 * will increase the open when lots of readers are already opened
		 */
		reader->oti = oti;
		if ( oti->nReaders == 0 )
		{
			oti->reader = reader;
		}
		else
		{
			reader->next = oti->reader;
			oti->reader->previous = reader;
			oti->reader = reader;
		}

		oti->nReaders++;
	}
// Removed to speedup otici for retibo
//	usleep( 1*1000*1000 );

	pthread_mutex_unlock( &oti->mutex );
	return ( reader );
OpenErrorPipe:
	if ( reader->wpfd[0] >= 0 ) close( reader->wpfd[0] );
	if ( reader->wpfd[1] >= 0 ) close( reader->wpfd[1] );
	if ( reader->rpfd[0] >= 0 ) close( reader->rpfd[0] );
	if ( reader->rpfd[1] >= 0 ) close( reader->rpfd[1] );

OpenError:
	if ( reader != NULL )
	{
		free( reader );
		reader = NULL;
	}

	pthread_mutex_unlock( &oti->mutex );
	return ( reader );
}



/**
 * This method will close the currently selected reader
 * and removes its handle from the oti readers list.
 *
 * @param rdrHandle		handle of the reader that needsd to be closed.
 *
 * @return if an error occurs -1 is returned, check errno for a more detailed error code.
 * 		   on successful execution 0 is returned.
 */
int oti_reader_close( POtiReaderCb reader)
{
	int result = 0;
    void * presult = ( void * ) & result;

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		if( reader->ptid != NULL ) {
			write(reader->wpfd[1],"S",2);
			pthread_join(reader->ptid, & presult);
		}

		if ( reader->wpfd[0] >= 0 ) close( reader->wpfd[0] );
		if ( reader->wpfd[1] >= 0 ) close( reader->wpfd[1] );
		if ( reader->rpfd[0] >= 0 ) close( reader->rpfd[0] );
		if ( reader->rpfd[1] >= 0 ) close( reader->rpfd[1] );
		/*-- close serial port --*/
		close( reader->fd );
		if ( reader->pfd[0] >= 0 ) close( reader->pfd[0] );
		if ( reader->pfd[1] >= 0 ) close( reader->pfd[1] );

		/*-- update previous reader control block if existant --*/
		if ( reader->previous != NULL )
		{
			reader->previous->next = reader->next;
		}

		/*-- update next reader control block if existant --*/
		if ( reader->next != NULL )
		{
			reader->next->previous = reader->previous;
		}

		/*-- decrement number of readers --*/
		reader->oti->nReaders--;

		/*-- cleanup memory --*/
		free( reader );
	}

	return ( result );
}

