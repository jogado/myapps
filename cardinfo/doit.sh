#!/bin/sh

if echo $1 | grep -q "imx6s"; then
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/3.1.3/cortexa9t2hf/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi

elif  echo $1 | grep -q "imx6ul"; then
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/3.1.3/cortexa7t2hf/environment-setup-cortexa7t2hf-neon-poky-linux-gnueabi

elif  echo $1 | grep -q "imx8mm"; then
	export ARCH=arm64
	export CROSS_COMPILE=aarch64-linux-gnu-
	. /opt/poky-emsyscon/3.1.3/aarch64/environment-setup-aarch64-poky-linux

	unset LDFLAGS
	unset CFLAGS
else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi

make clean
make

if echo $2 | grep -q "192.168"; then
    scp liboti.so root@$2:/usr/lib
	scp liboti.a  root@$2:/usr/lib
fi

