/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti.h
 *
 *	Header file containing global API structures, defines and functions.
 *
 *  Created on: Oct 13, 2008
 *      Author: jvolckae
 *  Modification history.
 *        Date		Author	Description
 *        April 24, 2015 JB	replace enums by defines of size int32_t
 *				add ioctl's 120 thru 127
 */

#ifndef _PRODATA_SYSTEMS_OTI_H_
#define _PRODATA_SYSTEMS_OTI_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/*-- definitions --*/
#define	EOTI		0x1000
#define	EOTI_NAK	0x1100
#define	EOTI_PCD	0x1200


/*-- enumerations --*/
/**
 * @enum OtiNak
 *
 * Overview of errors reported in a NAK message.
 */
typedef int32_t OtiNak;
#define EOTI_NAK_STX 			(OtiNak)(EOTI_NAK|0x01) /*! STX is missing */
#define EOTI_NAK_ETX 			(OtiNak)(EOTI_NAK|0x02) /*! ETX is missing */
#define EOTI_NAK_LRC			(OtiNak)(EOTI_NAK|0x03) /*! Invalid LRC */
#define EOTI_NAK_LENGTH			(OtiNak)(EOTI_NAK|0x04) /*! Message length error */
#define EOTI_NAK_OPCODE			(OtiNak)(EOTI_NAK|0x05) /*! Invalid opcode */
#define EOTI_NAK_TLV			(OtiNak)(EOTI_NAK|0x06) /*! Wrong TLV Structure Note */
#define EOTI_NAK_TLV_OVERFLOW		(OtiNak)(EOTI_NAK|0x07) /*! TLV Tag Data Length overflow (Greater than 900 Bytes) */
#define EOTI_NAK_SERIAL_OVERFLOW	(OtiNak)(EOTI_NAK|0x08) /*! Serial Buffer overflow (Greater than 950 Bytes) */
#define EOTI_NAK_INSTRUCTIONS		(OtiNak)(EOTI_NAK|0x09) /*! More than one instruction (Tag) in DO command */

/**
 * @enum OtiPcd
 *
 * Overview of errors reported in PCD message response.
 */
typedef int32_t OtiPcd;
#define EOTI_PCD_PARAM			(OtiPcd)(EOTI_PCD|0x01)		/*! Parameter is not supported */
#define EOTI_PCD_OUT_OF_SCOPE		(OtiPcd)(EOTI_PCD|0x02)		/*! Requested parameter is out of scope */
#define EOTI_PCD_WR_NON_VOLATILE_MEMORY	(OtiPcd)(EOTI_PCD|0x03)		/*! Unable to write to the PCD Nonvolatile memory */
#define EOTI_PCD_RD_NON_VOLATILE_MEMORY	(OtiPcd)(EOTI_PCD|0x04)		/*! Unable to read from the PCD Nonvolatile memory */
#define EOTI_PCD_RF			(OtiPcd)(EOTI_PCD|0x05)		/*! RF failure (without PCD Error Recovery Sequence) */
#define EOTI_PCD_COLLISION		(OtiPcd)(EOTI_PCD|0x06)		/*! Collision detected - More than one PICC in the RF field */
#define EOTI_PCD_RECOVERY		(OtiPcd)(EOTI_PCD|0x07)		/*! RF failure after PCD Error Recovery Sequence */
#define EOTI_PCD_PICC_OVERFLOW		(OtiPcd)(EOTI_PCD|0x0A)		/*! PICC Data length overflow (Greater than 300 Bytes) */
#define EOTI_PCD_HERCULES		(OtiPcd)(EOTI_PCD|0x0B)		/*! Hercules Error Code */
#define EOTI_PCD_SAM			(OtiPcd)(EOTI_PCD|0x0C)		/*! SAM/Contact general failure */
#define EOTI_PCD_SAM_NOT_PRESENT	(OtiPcd)(EOTI_PCD|0x0D)		/*! SAM not present */
#define EOTI_PCD_SAM_NOT_ACTIVE		(OtiPcd)(EOTI_PCD|0x0E)		/*! SAM not active */
#define EOTI_PCD_SAM_PROTOCOL		(OtiPcd)(EOTI_PCD|0x0F)		/*! SAM Protocol error (Only in PCD control) */
#define EOTI_PCD_MISSING_PARAMETER	(OtiPcd)(EOTI_PCD|0x10)		/*! Missing Parameter */
#define EOTI_PCD_APPLICATION		(OtiPcd)(EOTI_PCD|0x11)		/*! Application Disabled */
#define EOTI_PCD_INDEX_NOT_FOUND 	(OtiPcd)(EOTI_PCD|0x13)		/*! Index not found (for Indexed Objects) */
#define EOTI_PCD_PKI			(OtiPcd)(EOTI_PCD|0x14)		/*! PKI Storage Error. if the PCD encounters a storage error during
									power up - it will refuse all Poll EMV (Do an EMV Transaction)
									Template commands with this error code. In addition - this error
									can occur on a temporary storage error. */
#define EOTI_PCD_HASH			(OtiPcd)(EOTI_PCD|0x15)		/*! Hash doesn't match for a public key */
#define EOTI_PCD_AID_LENGTH		(OtiPcd)(EOTI_PCD|0x16)		/*! AID length error (index field) */
#define EOTI_PCD_DATA_LENGTH		(OtiPcd)(EOTI_PCD|0x17)		/*! Data field length error (within indexed objects) */
#define EOTI_PCD_EMV_TERMINATED 	(OtiPcd)(EOTI_PCD|0x18)		/*! EMV Transaction Terminated */
#define EOTI_PCD_SETUP			(OtiPcd)(EOTI_PCD|0x19)		/*! Setup Error: The PCD failed during setup. When this failure happens -
									all commands will fail except Reset PCD. */
#define EOTI_PCD_SUB_SCHEME		(OtiPcd)(EOTI_PCD|0x1D)		/*! Sub Scheme not Applicable */
#define EOTI_PCD_BUSY			(OtiPcd)(EOTI_PCD|0x1E)		/*! PCD is BUSY */
#define EOTI_PCD_POLL_TIMEOUT		(OtiPcd)(EOTI_PCD|0x1F)		/*! Poll EMV Timeout ended */

/**
 * @enum OtiVersion
 *
 * List of all possible version information that can be requested from the OTI reader.
 */
typedef int32_t OtiVersion;
#define OTI_VERSION_HOST_PROTOCOL		(OtiVersion)(0x00)		/*! The OTI Protocol version which the Firmware is based on */
#define OTI_VERSION_FIRMWARE			(OtiVersion)(0x01)		/*! Overall Major Payment Firmware */
#define OTI_VERSION_RF_UNIT			(OtiVersion)(0x11)		/*! RF Unit Drivers */
#define OTI_VERSION_COMM_UNIT			(OtiVersion)(0x12)		/*! Communication Drivers (UART) USB) PS2) MSR Emulation) */
#define OTI_VERSION_SMART_CARD_UNIT		(OtiVersion)(0x13)		/*! Smart Cards Drivers (SAMs) Contacts) */
#define OTI_VERSION_GPIO_UNIT			(OtiVersion)(0x14)		/*! General Purpose Input/Output drivers (LEDs) Buzzer) */
#define OTI_VERSION_EEPROM_UNIT			(OtiVersion)(0x15)		/*! PCD internal EEPROM Driver (for Default parameters) */
#define OTI_VERSION_UTILITIES			(OtiVersion)(0x16)		/*! PCD Utilities */
#define OTI_VERSION_ISO_14443_3			(OtiVersion)(0x20)		/*! ISO 14443 Part 3 */
#define OTI_VERSION_HOST_COMMUN			(OtiVersion)(0x21)		/*! Host-PCD Communication Module */
#define OTI_VERSION_ISO_14443_4			(OtiVersion)(0x22)		/*! ISO 14443 Part 4 */
#define OTI_VERSION_PAYPASS			(OtiVersion)(0x23)		/*! PayPass ISO 14443 Implementation V1.1 */
#define OTI_VERSION_ISO_7816_4			(OtiVersion)(0x24)		/*! ISO 7819 Part 4 */
#define OTI_VERSION_ISO_7816_3			(OtiVersion)(0x25)		/*! ISO 7819 Part 3 */
#define OTI_VERSION_PAYMENT_APPLI_SELECTION	(OtiVersion)(0x31)		/*! Payment Application Selection (VISA) MC) AMEX) */
#define OTI_VERSION_APPLI_SELECTION		(OtiVersion)(0x32)		/*! Other than Payment Application Selection */
#define OTI_VERSION_PAYMENT_APPLI_COMMON	(OtiVersion)(0x33)		/*! Common Application Utilities */
#define OTI_VERSION_UI				(OtiVersion)(0x34)		/*! User Interface Module (Auto controls LCD) LEDs and Buzzer) */
#define OTI_VERSION_MC_PAYPASS_APPLI		(OtiVersion)(0x41)		/*! PayPass M-Stripe Application (Includes Magnetic Strip) V3.2 */
#define OTI_VERSION_VISA_APPLI			(OtiVersion)(0x42)		/*! VISA qVSDC + MSD V2.0.2 */
#define OTI_VERSION_AMEX_APPLI			(OtiVersion)(0x43)		/*! AMEX ExpressPay PIPS (Non-EMV) */
#define OTI_VERSION_DISCOVER_NON_EMV		(OtiVersion)(0x44)		/*! Discover ZIP (Non-EMV) */
#define OTI_VERSION_MIFARE			(OtiVersion)(0x51)		/*! Support Mifare macros */
#define OTI_VERSION_HERCULES			(OtiVersion)(0x52)		/*! Support OTI Hercules O.S. macros */
#define OTI_VERSION_ALL				(OtiVersion)(0xFF)		/*! All internal modules together at one call */

/**
 * @enum OtiIoctl
 *
 * List of functions that can be executed by the OTI reader
 */
typedef int32_t OtiIoctl;
#define	OTI_IOCTL_GET_VERSION			(OtiIoctl)(0)
#define	OTI_IOCTL_GET_APPLICATIONS		(OtiIoctl)(1)
#define	OTI_IOCTL_DO_APPLICATIONS		(OtiIoctl)(2)

#define	OTI_IOCTL_RESTORE_FACTORY_SETTINGS	(OtiIoctl)(3)
#define	OTI_IOCTL_LOAD_DEFAULT_PARAMETERS	(OtiIoctl)(4)
#define	OTI_IOCTL_RESET_PCD			(OtiIoctl)(5)

#define	OTI_IOCTL_POLL_START			(OtiIoctl)(6)
#define	OTI_IOCTL_POLL_STOP			(OtiIoctl)(7)
#define	OTI_IOCTL_WAIT_FOR_CARD			(OtiIoctl)(8)
#define	OTI_IOCTL_REMOVE_PICC			(OtiIoctl)(9)
#define	OTI_IOCTL_WAIT_FOR_CARD_REMOVAL		(OtiIoctl)(10)

#define	OTI_IOCTL_GET_RF_ANTENNA		(OtiIoctl)(11)
#define	OTI_IOCTL_SET_RF_ANTENNA		(OtiIoctl)(12)
#define	OTI_IOCTL_DO_RF_ANTENNA			(OtiIoctl)(13)
#define	OTI_IOCTL_RESET_RF			(OtiIoctl)(14)

#define	OTI_IOCTL_GET_PCD_SETTINGS		(OtiIoctl)(15)
#define	OTI_IOCTL_SET_PCD_SETTINGS		(OtiIoctl)(16)
#define	OTI_IOCTL_DO_PCD_SETTINGS		(OtiIoctl)(17)

#define	OTI_IOCTL_MIFARE_COMMAND		(OtiIoctl)(18)
#define	OTI_IOCTL_MIFARE_KEY_REGION		(OtiIoctl)(19)

#define	OTI_IOCTL_TRANSPARENT_DATA		(OtiIoctl)(20)
#define	OTI_IOCTL_RF_MODE			(OtiIoctl)(21)
#define	OTI_IOCTL_TRANSPARENT_LAYER_CONTROL	(OtiIoctl)(22)
#define	OTI_IOCTL_TRANSPARENT_CHANNEL		(OtiIoctl)(23)
#define	OTI_IOCTL_TRANSPARENT_FRAME_SIZE	(OtiIoctl)(24)
#define	OTI_IOCTL_GET_POLL_DELAY		(OtiIoctl)(25)

#define	OTI_IOCTL_BOOT_LOADER			(OtiIoctl)(26)
#define	OTI_IOCTL_FLASH_PACKET			(OtiIoctl)(27)
#define	OTI_IOCTL_RUN_APPLICATION		(OtiIoctl)(28)

#define	OTI_IOCTL_NDOT_POLL			(OtiIoctl)(29)
#define	OTI_IOCTL_NDOT_EMV			(OtiIoctl)(30)

#define	OTI_IOCTL_GET_EMV_COUNTRY_CODE		(OtiIoctl)(31)
#define	OTI_IOCTL_SET_EMV_COUNTRY_CODE		(OtiIoctl)(32)
#define	OTI_IOCTL_SET_PAYMENT_SUBSCHEME		(OtiIoctl)(33)
#define	OTI_IOCTL_GET_EMV_RESULT_TAG_LIST	(OtiIoctl)(34)
#define	OTI_IOCTL_SET_EMV_RESULT_TAG_LIST	(OtiIoctl)(35)
#define	OTI_IOCTL_GET_MAGSTRIPE_RESULT_TAG_LIST	(OtiIoctl)(36)
#define	OTI_IOCTL_SET_MAGSTRIPE_RESULT_TAG_LIST	(OtiIoctl)(37)
#define	OTI_IOCTL_POLL_EMV			(OtiIoctl)(38)
#define	OTI_IOCTL_EMV_TERM_ONLINE_STAT		(OtiIoctl)(39)
#define	OTI_IOCTL_EMV_GET_TRANS_CURR_CODE	(OtiIoctl)(40)
#define	OTI_IOCTL_EMV_SET_TRANS_CURR_CODE	(OtiIoctl)(41)
#define	OTI_IOCTL_EMV_GET_PUBLIC_KEYS		(OtiIoctl)(42)
#define	OTI_IOCTL_EMV_SET_PUBLIC_KEYS		(OtiIoctl)(43)
#define	OTI_IOCTL_EMV_GET_PUBLIC_KEYS_LIST	(OtiIoctl)(44)
#define	OTI_IOCTL_GET_TERMINAL_TYPE		(OtiIoctl)(45)
#define	OTI_IOCTL_SET_TERMINAL_TYPE		(OtiIoctl)(46)
#define	OTI_IOCTL_GET_VISA_CRYPTO_SUBSCHEME	(OtiIoctl)(47)
#define	OTI_IOCTL_SET_VISA_CRYPTO_SUBSCHEME	(OtiIoctl)(48)
#define	OTI_IOCTL_GET_APP_VERSION_NBR		(OtiIoctl)(49)
#define	OTI_IOCTL_GET_TERMINAL_FLOOR_LIMIT	(OtiIoctl)(50)
#define	OTI_IOCTL_SET_TERMINAL_FLOOR_LIMIT	(OtiIoctl)(51)
#define	OTI_IOCTL_GET_TERMINAL_CAPABILITIES	(OtiIoctl)(52)
#define	OTI_IOCTL_SET_TERMINAL_CAPABILITIES	(OtiIoctl)(53)
#define	OTI_IOCTL_GET_TCC			(OtiIoctl)(54)
#define	OTI_IOCTL_SET_TCC			(OtiIoctl)(55)
#define	OTI_IOCTL_GET_TARGET_PERC_BAISED_RAND	(OtiIoctl)(56)
#define	OTI_IOCTL_SET_TARGET_PERC_BAISED_RAND	(OtiIoctl)(57)
#define	OTI_IOCTL_GET_MAX_PERC_RAND		(OtiIoctl)(58)
#define	OTI_IOCTL_SET_MAX_PERC_RAND		(OtiIoctl)(59)
#define	OTI_IOCTL_GET_THRESHOLD_BAISED_RANDOM	(OtiIoctl)(60)
#define	OTI_IOCTL_SET_THRESHOLD_BAISED_RANDOM	(OtiIoctl)(61)
#define	OTI_IOCTL_GET_TAC_DEFAULT		(OtiIoctl)(62)
#define	OTI_IOCTL_SET_TAC_DEFAULT		(OtiIoctl)(63)
#define	OTI_IOCTL_GET_TAC_DENIAL		(OtiIoctl)(64)
#define	OTI_IOCTL_SET_TAC_DENIAL		(OtiIoctl)(65)
#define	OTI_IOCTL_GET_TAC_ONLINE		(OtiIoctl)(66)
#define	OTI_IOCTL_SET_TAC_ONLINE		(OtiIoctl)(67)
#define	OTI_IOCTL_GET_DEFAULT_DDOL		(OtiIoctl)(68)
#define	OTI_IOCTL_SET_DEFAULT_DDOL		(OtiIoctl)(69)
#define	OTI_IOCTL_GET_DEFAULT_TDOL		(OtiIoctl)(70)
#define	OTI_IOCTL_SET_DEFAULT_TDOL		(OtiIoctl)(71)
#define	OTI_IOCTL_GET_PCD_TRANSACTION_LIMIT	(OtiIoctl)(72)
#define	OTI_IOCTL_SET_PCD_TRANSACTION_LIMIT	(OtiIoctl)(73)
#define	OTI_IOCTL_GET_CVM_REQUIRED_LIMIT	(OtiIoctl)(74)
#define	OTI_IOCTL_SET_CVM_REQUIRED_LIMIT	(OtiIoctl)(75)
#define	OTI_IOCTL_GET_AMOUNT_ZERO_ONLINE_FLAG	(OtiIoctl)(76)
#define	OTI_IOCTL_SET_AMOUNT_ZERO_ONLINE_FLAG	(OtiIoctl)(77)
#define	OTI_IOCTL_GET_STATUS_CHECK_FLAG		(OtiIoctl)(78)
#define	OTI_IOCTL_SET_STATUS_CHECK_FLAG		(OtiIoctl)(79)
#define	OTI_IOCTL_GET_VISA_FDDA_VERS_0_ENABLE	(OtiIoctl)(80)
#define	OTI_IOCTL_SET_VISA_FDDA_VERS_0_ENABLE	(OtiIoctl)(81)
#define	OTI_IOCTL_GET_TERMINAL_COMM_CHECK	(OtiIoctl)(82)
#define	OTI_IOCTL_SET_TERMINAL_COMM_CHECK	(OtiIoctl)(83)
#define	OTI_IOCTL_GET_VISA_WAVE_ENABLE		(OtiIoctl)(84)
#define	OTI_IOCTL_SET_VISA_WAVE_ENABLE		(OtiIoctl)(85)
#define	OTI_IOCTL_GET_LOCK_PCD_PROTECTED	(OtiIoctl)(86)
#define	OTI_IOCTL_SET_LOCK_PCD_PROTECTED	(OtiIoctl)(87)

#define	OTI_IOCTL_GET_PCD_PROTOCOL_SELECTOR	(OtiIoctl)(88)
#define	OTI_IOCTL_SET_PCD_PROTOCOL_SELECTOR	(OtiIoctl)(89)
#define	OTI_IOCTL_GET_RF_COLLISION_MSG_ENABLED	(OtiIoctl)(90)
#define	OTI_IOCTL_SET_RF_COLLISION_MSG_ENABLED	(OtiIoctl)(91)
#define	OTI_IOCTL_GET_TRACKS_FOLLOWED_BY_AID	(OtiIoctl)(92)
#define	OTI_IOCTL_SET_TRACKS_FOLLOWED_BY_AID	(OtiIoctl)(93)
#define	OTI_IOCTL_GET_PUPI			(OtiIoctl)(94)
#define	OTI_IOCTL_SET_PUPI			(OtiIoctl)(95)
#define	OTI_IOCTL_GET_UID			(OtiIoctl)(96)
#define	OTI_IOCTL_SET_UID			(OtiIoctl)(97)
#define	OTI_IOCTL_GET_ATQB			(OtiIoctl)(98)
#define	OTI_IOCTL_SET_ATQB			(OtiIoctl)(99)
#define	OTI_IOCTL_GET_ATS			(OtiIoctl)(100)
#define	OTI_IOCTL_SET_ATS			(OtiIoctl)(101)
#define	OTI_IOCTL_GET_SAK			(OtiIoctl)(102)
#define	OTI_IOCTL_SET_SAK			(OtiIoctl)(103)
#define	OTI_IOCTL_DO_TRANSP_AUTO_DESELECT	(OtiIoctl)(104)
#define	OTI_IOCTL_DO_RF_PARAM_BIT_RATE		(OtiIoctl)(105)
#define	OTI_IOCTL_GET_RF_PARAM_MAX_BIT_RATE	(OtiIoctl)(106)
#define	OTI_IOCTL_SET_RF_PARAM_MAX_BIT_RATE	(OtiIoctl)(107)
#define	OTI_IOCTL_DO_RF_PARAM_FWTI		(OtiIoctl)(108)
#define	OTI_IOCTL_DO_DESELECT			(OtiIoctl)(109)
#define	OTI_IOCTL_GET_PCD_SERIAL_NUMBER		(OtiIoctl)(110)
#define	OTI_IOCTL_GET_TYPE_B_MODULATION_INDEX	(OtiIoctl)(111)
#define	OTI_IOCTL_GET_APP_SELECTION_INDICATOR	(OtiIoctl)(112)
#define	OTI_IOCTL_SET_APP_SELECTION_INDICATOR	(OtiIoctl)(113)
#define	OTI_IOCTL_UNLOCK_PROTECTED		(OtiIoctl)(114)
#define	OTI_IOCTL_DO_OUTPORT_CONTROL		(OtiIoctl)(115)
#define	OTI_IOCTL_SET_OUTPORT_CONTROL		(OtiIoctl)(116)
#define	OTI_IOCTL_DO_OUTPORT			(OtiIoctl)(117)
#define	OTI_IOCTL_SET_APPLICATIONS		(OtiIoctl)(118)
#define	OTI_IOCTL_SET_VERBOSITY			(OtiIoctl)(119)

#define	OTI_IOCTL_GET_TIMEOUTS			(OtiIoctl)(120)
#define	OTI_IOCTL_SET_TIMEOUTS			(OtiIoctl)(121)
#define	OTI_IOCTL_DO_RF_PARAM_MAX_BIT_RATE	(OtiIoctl)(122)
#define	OTI_IOCTL_DO_SAK			(OtiIoctl)(123)
#define	OTI_IOCTL_DO_PUPI			(OtiIoctl)(124)
#define	OTI_IOCTL_DO_ATS			(OtiIoctl)(125)
#define	OTI_IOCTL_DO_ATQB			(OtiIoctl)(126)
#define	OTI_IOCTL_DO_UID			(OtiIoctl)(127)

#define OTI_IOCTL_SET_FW_COMPATIBILITY          (OtiIoctl)(128)

#define	OTI_IOCTL_GET_VISA_WAVE_OFFLINE_ENABLE 	(OtiIoctl)(129)
#define	OTI_IOCTL_SET_VISA_WAVE_OFFLINE_ENABLE	(OtiIoctl)(130)

#define	OTI_IOCTL_GET_VISA_ENABLE_LIMIT 	(OtiIoctl)(131)
#define	OTI_IOCTL_SET_VISA_ENABLE_LIMIT 	(OtiIoctl)(132)
#define	OTI_IOCTL_GET_VISA_FORMAT_TRACK1_ENABLE	(OtiIoctl)(133)
#define	OTI_IOCTL_SET_VISA_FORMAT_TRACK1_ENABLE	(OtiIoctl)(134)
#define	OTI_IOCTL_GET_TRACK2_FORMAT             (OtiIoctl)(135)
#define	OTI_IOCTL_SET_TRACK2_FORMAT             (OtiIoctl)(136)
#define	OTI_IOCTL_GET_EXCAHNGE_DATA_FLAG 	(OtiIoctl)(137)
#define	OTI_IOCTL_SET_EXCAHNGE_DATA_FLAG 	(OtiIoctl)(138)
#define	OTI_IOCTL_DO_EXCAHNGE_DATA_FLAG 	(OtiIoctl)(139)
#define	OTI_IOCTL_GET_KERNEL_CONFIGURATION 	(OtiIoctl)(140)
#define	OTI_IOCTL_SET_KERNEL_CONFIGURATION 	(OtiIoctl)(141)
#define	OTI_IOCTL_GET_TERMINATE_RESULT_TAG_LIST	(OtiIoctl)(142)
#define	OTI_IOCTL_SET_TERMINATE_RESULT_TAG_LIST	(OtiIoctl)(143)
#define	OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM	(OtiIoctl)(144)
#define	OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM	(OtiIoctl)(145)
#define OTI_IOTCL_SAM_POWER_UP_RESET (OtiIoctl)(146)
#define	OTI_IOCTL_SR_COMMAND			(OtiIoctl)(147)		/* can be used with sr tickets - missing write reply is fixed by read block */
#define OTI_IOCTL_GET_EXT_POLL_CONFIG		(OtiIoctl)(148)		/* get current extended poll configuration settings */
#define OTI_IOCTL_SET_EXT_POLL_CONFIG		(OtiIoctl)(149)		/* set new extended poll configuration settings */
#define	OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM_MC	(OtiIoctl)(150)
#define	OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM_MC	(OtiIoctl)(151)

#define	OTI_IOCTL_RAW_COMMAND			(OtiIoctl)(152)

#define OTI_IOCTL_GET_EXTERNAL_DISPLAY_MESSAGES (OtiIoctl)(153)
#define OTI_IOCTL_SET_EXTERNAL_DISPLAY_MESSAGES (OtiIoctl)(154)
#define OTI_IOCTL_DO_EXTERNAL_DISPLAY_MESSAGES (OtiIoctl)(155)
/* static void edm_callback(char *payload_tlv,int size) */
#define OTI_IOCTL_EXT_DISP_MESS_CALLBACK	(OtiIoctl)(156)		/* tx holds a function pointer of a function with two parameters, char *payload_tlv, int length, and returning a void */

#define	OTI_IOCTL_COUNT                         (OtiIoctl)(157)

/**
 * @enum OtiPcdSettings
 *
 * Overview of flags in the OtiPcdSettingsMask
 */
typedef int32_t OtiPcdSettingsFlags;
#define	OtiPcdRfCollisionMessage 	(OtiPcdSettingsFlags)(0x0001)
#define	OtiPcdTracksFollowedByAID	(OtiPcdSettingsFlags)(0x0002)
#define	OtiPcdPupi			(OtiPcdSettingsFlags)(0x0004)
#define	OtiPcdUid			(OtiPcdSettingsFlags)(0x0008)
#define	OtiPcdAtqB			(OtiPcdSettingsFlags)(0x0010)
#define	OtiPcdAts			(OtiPcdSettingsFlags)(0x0020)

/**
 * @typedef OtiPcdSettingsMask
 *
 * Mask of the OTI PCD settings flags.
 */
typedef unsigned short OtiPcdSettingsMask;

/**
 * @struct __OtiPcdSettings
 *
 * Mask and values of the PCD Settings.
 */
typedef struct __OtiPcdSettings
{
	OtiPcdSettingsMask	mask;				/*! Overview mask of settings that need to be changed */
	OtiPcdSettingsMask	values;				/*! value mask of the settings that need to be set */
} OtiPcdSettings, *POtiPcdSettings;

/**
 * @enum OtiApplication
 *
 * List of application that can be enabled on the OTI reader.
 */
typedef int32_t OtiApplication;
#define	OTI_APPLI_VISA_VIRTUAL 		(OtiApplication)(0x80000000)
#define	OTI_APPLI_MC_VIRTUAL 		(OtiApplication)(0x40000000)
#define	OTI_APPLI_MIFARE 		(OtiApplication)(0x20000000)
#define	OTI_APPLI_ISO_14443_4_PICC	(OtiApplication)(0x10000000)
#define	OTI_APPLI_VISA			(OtiApplication)(0x08000000)
#define	OTI_APPLI_AMEX			(OtiApplication)(0x04000000)
#define	OTI_APPLI_MAESTRO	 	(OtiApplication)(0x02000000)
#define	OTI_APPLI_MASTER_CARD	 	(OtiApplication)(0x01000000)
#define	OTI_APPLI_VICINITY 		(OtiApplication)(0x00800000)
#define	OTI_APPLI_VISA_TEST		(OtiApplication)(0x00400000)
#define	OTI_APPLI_MAESTRO_TEST_LONG_AID (OtiApplication)(0x00200000)
#define	OTI_APPLI_MASTERCARD_DEBIT 	(OtiApplication)(0x00100000)
#define	OTI_APPLI_VISA_VPAY	 	(OtiApplication)(0x00080000)
#define	OTI_DISCOVER		 	(OtiApplication)(0x00020000)
#define	OTI_VISA_ELECTRON	 	(OtiApplication)(0x00010000)

#define	OTI_APPLI_UNKNOWN 		(OtiApplication)(0xFFFFFFFF)

/**
 * @typedef OtiAppli_t
 *
 * Type definition representing the OtiApplication bitmap for
 * the enable oti applications.
 */
typedef unsigned long OtiAppli_t;

/**
 * @enum OtiKernelId
 *
 * List of kernels that are known by the OTI reader.
 */
typedef int32_t OtiKernelID;
#define	OTI_KERNEL_ID_NOKERNEL 		(OtiKernelID)(0)
#define	OTI_KERNEL_ID_MASTERCARD	(OtiKernelID)(2)
#define	OTI_KERNEL_ID_VISA 		(OtiKernelID)(3)
#define	OTI_KERNEL_ID_AMEX 		(OtiKernelID)(4)


/**
 * @enum OtiPollMode
 */
typedef int32_t OtiPollMode;
#define OtiPollPendingAutoContinuously (OtiPollMode)(1)	/*! PCD polls for PICC, Runs full Transaction, returns PICC Application
												data to Host and Resumes Polling sequence. (Except for cases where
												the PICC does not support 'Full transaction', in this case the PCD
												will only activate the PICC and then send its data to the Host.
												In such a case the host is responsible to complete the operation and
												removal process of the PICC. The PCD will return to polling after the
												Host has performed 'Remove PICC' or Deselect' commands). */
#define	OtiPollPendingAutoOnce	(OtiPollMode)(2)	/*! PCD polls for PICC, Runs full Transaction, stops polling and returns
												PICC Application data to Host. (Except for cases where the PICC does
												not support 'Full transaction', in this case the PCD will only activate
												the PICC and then send its data to the Host. In such a case the host
												is responsible to complete the operation and removal process of the PICC.  */
#define	OtiPollPendingOnce	(OtiPollMode)(3)	/*! PCD poll for PICC, Activate it when it enters the RF field, stops polling
												and returns PICC data to Host. */
#define	OtiPollImmediate	(OtiPollMode)(4)	/*! PCD will run Anti collistion and Activation (if a PICC was found),
												returning PICC Type to Host (PICC Type / No PICC / Collistion). */
#define	OtiPollPendingAuto	(OtiPollMode)(5)	/*! PCD will run Anti collistion and Activation (if a PICC was found),
                                                returning PICC data to Host (PICC Type / No PICC / Collistion).
                                                This poll sequence is more tolerant than that of Poll Mode 03 (OtiPollPendingOnce),
                                                being used for cards requiring a larger response time.*/
#define	OtiPollPendingOnceExt	(OtiPollMode)(11)	/*! mode 3 extended with poll for tickets like SRI */
#define	OtiPollImmediateExt	(OtiPollMode)(12)	/*! mode 4 extended with poll for tickets like SRI */

#define	OtiPollUnknown		(OtiPollMode)(0xFF)

/**
 * @enum OtiRfAntenna
 *
 * Type enumeration of the OTI reader's antenna state.
 */
typedef int32_t OtiRfAntenna;
#define	OtiRfAntennaOff		(OtiRfAntenna)(0)
#define	OtiRfAntennaOn 		(OtiRfAntenna)(1)

#define OtiRfAntennaUnknown	(OtiRfAntenna)(0xFF)

/**
 * @enum OtiRfAntennaMode
 *
 * Type enumeration of the OTI reader's antenna mode.
 */
typedef int32_t OtiRfAntennaMode;
#define	OtiRfTypeA 		(OtiRfAntennaMode)(0x41)
#define	OtiRfTypeB 		(OtiRfAntennaMode)(0x42)

#define	OtiRfAntennaModeUnknown (OtiRfAntennaMode)(0xFF)


/**
 * @enum OtiTransparentLayer
 *
 * Type enumeration of the OTI reader's antenna mode.
 */
typedef int32_t OtiTransparentLayer;
#define OtiTransparentLayerPcd 		(OtiTransparentLayer)(0)
#define OtiTransparentLayerHost		(OtiTransparentLayer)(1)

#define OtiTransparentLayerUnknown 	(OtiTransparentLayer)(0xFF)


/**
 * @enum OtiTransparentChannel
 *
 * Type enumeration of the OTI reader's transparent channels.
 */
typedef int32_t OtiTransparentChannel;
#define OtiTransparentChannelRf 	(OtiTransparentChannel)(0x11)
#define OtiTransparentChannelSam	(OtiTransparentChannel)(0x21)

#define OtiTransparentChannelUnknown 	(OtiTransparentChannel)(0xFF)


/**
 * @enum OtiPiccType
 *
 * Type enumeration of the OTI reader PICC types.
 */
typedef int32_t OtiPiccType;
#define OtiPiccTypeA		(OtiPiccType)('A')
#define OtiPiccTypeB		(OtiPiccType)('B')
#define OtiPiccTypeM		(OtiPiccType)('M')
#define OtiPiccTypeT		(OtiPiccType)('T')	/* added for SR family of paper tickets */

#define OtiPiccTypeUnknown	(OtiPiccType)(-1)

/**
 * @enum OtiTransactionType
 *
 * Type enumeration of the OTI reader transaction types.
 */
typedef int32_t OtiTransactionType;
#define OtiTransactionTypePurchase		(OtiTransactionType)(0)
#define OtiTransactionTypeWithdrawal		(OtiTransactionType)(1)
#define OtiTransactionTypeGoodsPurchase		(OtiTransactionType)(9)
#define OtiTransactionTypeCredit		(OtiTransactionType)(17)
#define OtiTransactionTypeBalance		(OtiTransactionType)(30)

#define OtiTransactionTypeUnknown		(OtiTransactionType)(-1)

/**
 * @enum OtiCryptoInfoData
 *
 * Type enumeration of the OTI reader transaction types.
 */
typedef int32_t OtiCryptoInfoData;
#define OtiCryptoInfoDataAAC		(OtiCryptoInfoData)(0x00)
#define OtiCryptoInfoDataTC		(OtiCryptoInfoData)(0x40)
#define OtiCryptoInfoDataARQC		(OtiCryptoInfoData)(0x80)
#define OtiCryptoInfoDataRFU		(OtiCryptoInfoData)(0xC0)

#define OtiCryptoInfoDataUnknown	(OtiCryptoInfoData)(-1)

/**
 * @enum OtiTransactionRes
 *
 * Type enumeration of the OTI reader transaction types.
 */
typedef int32_t OtiTransactionRes;
#define OtiTransactionResDeclined	(OtiTransactionRes)(0x00)
#define OtiTransactionResOffline	(OtiTransactionRes)(0x40)
#define OtiTransactionResOnline		(OtiTransactionRes)(0x80)

#define OtiTransactionResUnknown	(OtiTransactionRes)(-1)

/**
 * @enum OtiTransparentFrameSize
 *
 * Type enumeration of the OTI reader ISO 14443 frame size.
 */
typedef int32_t OtiTransparentFrameSize;
#define OtiFrameSize16		(OtiTransparentFrameSize)(0)
#define OtiFrameSize24		(OtiTransparentFrameSize)(1)
#define OtiFrameSize32		(OtiTransparentFrameSize)(2)
#define OtiFrameSize40		(OtiTransparentFrameSize)(3)
#define OtiFrameSize48		(OtiTransparentFrameSize)(4)
#define OtiFrameSize64		(OtiTransparentFrameSize)(5)
#define OtiFrameSize96		(OtiTransparentFrameSize)(6)
#define OtiFrameSize128		(OtiTransparentFrameSize)(7)
#define OtiFrameSize256		(OtiTransparentFrameSize)(8)

#define OtiFramSizeUnknown	(Oti)(-1)

/**
 * @enum OtiTlvSize
 *
 * Type enumeration of the OTI reader ISO 14443 frame size.
 */
typedef int32_t OtiTlvSize;
#define OtiTlvSizeNo		(OtiTlvSize)(0)
#define OtiTlvSizeAdd		(OtiTlvSize)(1)

#define OtiTlvSizeUnknown	(Oti)(-1)

typedef int32_t OtiTerminalCountryCode;
#define OtiCountryAF		(OtiTerminalCountryCode)(0x0004)		/* Afghanistan */
#define OtiCountryAL		(OtiTerminalCountryCode)(0x0008)		/* Albania */
#define OtiCountryDZ		(OtiTerminalCountryCode)(0x0012)		/* Algeria */
#define OtiCountryAS		(OtiTerminalCountryCode)(0x0016)		/* American Samoa */
#define OtiCountryAD		(OtiTerminalCountryCode)(0x0020)		/* Andorra */
#define OtiCountryAO		(OtiTerminalCountryCode)(0x0024)		/* Angola */
#define OtiCountryAG		(OtiTerminalCountryCode)(0x0028)		/* Antigua & Barbuda */
#define OtiCountryAR		(OtiTerminalCountryCode)(0x0032)		/* Argentina */
#define OtiCountryAM		(OtiTerminalCountryCode)(0x0051)		/* Armenia */
#define OtiCountryAW		(OtiTerminalCountryCode)(0x0533)		/* Aruba */
#define OtiCountryAU		(OtiTerminalCountryCode)(0x0036)		/* Australia */
#define OtiCountryAT		(OtiTerminalCountryCode)(0x0040)		/* Austria */
#define OtiCountryAZ		(OtiTerminalCountryCode)(0x0031)		/* Azerbaijan */
#define OtiCountryBS		(OtiTerminalCountryCode)(0x0044)		/* Bahamas */
#define OtiCountryBH		(OtiTerminalCountryCode)(0x0048)		/* Bahrain */
#define OtiCountryBD		(OtiTerminalCountryCode)(0x0050)		/* Bangladesh */
#define OtiCountryBB		(OtiTerminalCountryCode)(0x0052)		/* Barbados */
#define OtiCountryBY		(OtiTerminalCountryCode)(0x0112)		/* Belarus */
#define OtiCountryBE		(OtiTerminalCountryCode)(0x0056)		/* Belgium */
#define OtiCountryBZ		(OtiTerminalCountryCode)(0x0084)		/* Belize */
#define OtiCountryBJ		(OtiTerminalCountryCode)(0x0204)		/* Benin */
#define OtiCountryBM		(OtiTerminalCountryCode)(0x0060)		/* Bermuda */
#define OtiCountryBT		(OtiTerminalCountryCode)(0x0064)		/* Bhutan */
#define OtiCountryBO		(OtiTerminalCountryCode)(0x0068)		/* Bolivia */
#define OtiCountryBA		(OtiTerminalCountryCode)(0x0070)		/* Bosnia and Herzegovina */
#define OtiCountryBW		(OtiTerminalCountryCode)(0x0072)		/* Botswana */
#define OtiCountryBR		(OtiTerminalCountryCode)(0x0076)		/* Brazil */
#define OtiCountryVG		(OtiTerminalCountryCode)(0x0092)		/* British Virgin Islands */
#define OtiCountryBN		(OtiTerminalCountryCode)(0x0096)		/* Brunei Darussalam */
#define OtiCountryBG		(OtiTerminalCountryCode)(0x0100)		/* Bulgaria */
#define OtiCountryBF		(OtiTerminalCountryCode)(0x0854)		/* Burkina Faso */
#define OtiCountryBI		(OtiTerminalCountryCode)(0x0108)		/* Burundi */
#define OtiCountryKH		(OtiTerminalCountryCode)(0x0116)		/* Cambodia */
#define OtiCountryCM		(OtiTerminalCountryCode)(0x0120)		/* Cameroon */
#define OtiCountryCA		(OtiTerminalCountryCode)(0x0124)		/* Canada */
#define OtiCountryCV		(OtiTerminalCountryCode)(0x0132)		/* Cape Verde */
#define OtiCountryKY		(OtiTerminalCountryCode)(0x0136)		/* Cayman Islands */
#define OtiCountryCF		(OtiTerminalCountryCode)(0x0140)		/* Central African Republic */
#define OtiCountryTD		(OtiTerminalCountryCode)(0x0148)		/* Chad */
#define OtiCountryCL		(OtiTerminalCountryCode)(0x0152)		/* Chile */
#define OtiCountryCN		(OtiTerminalCountryCode)(0x0156)		/* China */
#define OtiCountryCO		(OtiTerminalCountryCode)(0x0170)		/* Colombia */
#define OtiCountryKM		(OtiTerminalCountryCode)(0x0174)		/* Comoros */
#define OtiCountryCG		(OtiTerminalCountryCode)(0x0178)		/* Congo */
#define OtiCountryCK		(OtiTerminalCountryCode)(0x0184)		/* Cook Islands */
#define OtiCountryCR		(OtiTerminalCountryCode)(0x0188)		/* Costa Rica */
#define OtiCountryCI		(OtiTerminalCountryCode)(0x0384)		/* C�te d'Ivoire */
#define OtiCountryHR		(OtiTerminalCountryCode)(0x0191)		/* Croatia */
#define OtiCountryCU		(OtiTerminalCountryCode)(0x0192)		/* Cuba */
#define OtiCountryCY		(OtiTerminalCountryCode)(0x0196)		/* Cyprus */
#define OtiCountryCZ		(OtiTerminalCountryCode)(0x0203)		/* Czech Republic */
#define OtiCountryDK		(OtiTerminalCountryCode)(0x0208)		/* Denmark */
#define OtiCountryDJ		(OtiTerminalCountryCode)(0x0262)		/* Djibouti */
#define OtiCountryDM		(OtiTerminalCountryCode)(0x0212)		/* Dominica */
#define OtiCountryDO		(OtiTerminalCountryCode)(0x0214)		/* Dominican Republic */
#define OtiCountryEC		(OtiTerminalCountryCode)(0x0218)		/* Ecuador */
#define OtiCountryEG		(OtiTerminalCountryCode)(0x0818)		/* Egypt */
#define OtiCountrySV		(OtiTerminalCountryCode)(0x0222)		/* El Salvador */
#define OtiCountryGQ		(OtiTerminalCountryCode)(0x0226)		/* Equatorial Guinea */
#define OtiCountryER		(OtiTerminalCountryCode)(0x0232)		/* Eritrea */
#define OtiCountryEE		(OtiTerminalCountryCode)(0x0233)		/* Estonia */
#define OtiCountryET		(OtiTerminalCountryCode)(0x0231)		/* Ethiopia */
#define OtiCountryFK		(OtiTerminalCountryCode)(0x0238)		/* Falkland Islands (Malvinas) */
#define OtiCountryFJ		(OtiTerminalCountryCode)(0x0242)		/* Fiji */
#define OtiCountryFI		(OtiTerminalCountryCode)(0x0246)		/* Finland */
#define OtiCountryFR		(OtiTerminalCountryCode)(0x0250)		/* France */
#define OtiCountryGF		(OtiTerminalCountryCode)(0x0254)		/* French Guiana */
#define OtiCountryPF		(OtiTerminalCountryCode)(0x0258)		/* French Polynesia */
#define OtiCountryGA		(OtiTerminalCountryCode)(0x0266)		/* Gabon */
#define OtiCountryGM		(OtiTerminalCountryCode)(0x0270)		/* Gambia */
#define OtiCountryGE		(OtiTerminalCountryCode)(0x0268)		/* Georgia */
#define OtiCountryDE		(OtiTerminalCountryCode)(0x0276)		/* Germany */
#define OtiCountryGH		(OtiTerminalCountryCode)(0x0288)		/* Ghana */
#define OtiCountryGI		(OtiTerminalCountryCode)(0x0292)		/* Gibraltar */
#define OtiCountryGR		(OtiTerminalCountryCode)(0x0300)		/* Greece */
#define OtiCountryGL		(OtiTerminalCountryCode)(0x0304)		/* Greenland */
#define OtiCountryGD		(OtiTerminalCountryCode)(0x0308)		/* Grenada */
#define OtiCountryGP		(OtiTerminalCountryCode)(0x0312)		/* Guadeloupe */
#define OtiCountryGU		(OtiTerminalCountryCode)(0x0316)		/* Guam */
#define OtiCountryGT		(OtiTerminalCountryCode)(0x0320)		/* Guatemala */
#define OtiCountryGN		(OtiTerminalCountryCode)(0x0324)		/* Guinea */
#define OtiCountryGW		(OtiTerminalCountryCode)(0x0624)		/* Guinea-Bissau */
#define OtiCountryGY		(OtiTerminalCountryCode)(0x0328)		/* Guyana */
#define OtiCountryHT		(OtiTerminalCountryCode)(0x0332)		/* Haiti */
#define OtiCountryHN		(OtiTerminalCountryCode)(0x0340)		/* Honduras */
#define OtiCountryHU		(OtiTerminalCountryCode)(0x0348)		/* Hungary */
#define OtiCountryIS		(OtiTerminalCountryCode)(0x0352)		/* Iceland */
#define OtiCountryIN		(OtiTerminalCountryCode)(0x0356)		/* India */
#define OtiCountryID		(OtiTerminalCountryCode)(0x0360)		/* Indonesia */
#define OtiCountryIQ		(OtiTerminalCountryCode)(0x0368)		/* Iraq */
#define OtiCountryIE		(OtiTerminalCountryCode)(0x0372)		/* Ireland */
#define OtiCountryIR		(OtiTerminalCountryCode)(0x0833)		/* Isle of Man*/
#define OtiCountryIL		(OtiTerminalCountryCode)(0x0376)		/* Israel */
#define OtiCountryIT		(OtiTerminalCountryCode)(0x0380)		/* Italy */
#define OtiCountryJM		(OtiTerminalCountryCode)(0x0388)		/* Jamaica */
#define OtiCountryJP		(OtiTerminalCountryCode)(0x0392)		/* Japan */
#define OtiCountryJO		(OtiTerminalCountryCode)(0x0400)		/* Jordan*/
#define OtiCountryKZ		(OtiTerminalCountryCode)(0x0398)		/* Kazakhstan */
#define OtiCountryKE		(OtiTerminalCountryCode)(0x0404)		/* Kenya */
#define OtiCountryKI		(OtiTerminalCountryCode)(0x0296)		/* Kiribati */
#define OtiCountryKW		(OtiTerminalCountryCode)(0x0414)		/* Kuwait */
#define OtiCountryKG		(OtiTerminalCountryCode)(0x0417)		/* Kyrgyzstan */
#define OtiCountryLA		(OtiTerminalCountryCode)(0x0418)		/* Lao People's Democratic Republic */
#define OtiCountryLV		(OtiTerminalCountryCode)(0x0428)		/* Latvia */
#define OtiCountryLB		(OtiTerminalCountryCode)(0x0422)		/* Lebanon */
#define OtiCountryLS		(OtiTerminalCountryCode)(0x0426)		/* Lesotho */
#define OtiCountryLR		(OtiTerminalCountryCode)(0x0430)		/* Liberia */
#define OtiCountryLY		(OtiTerminalCountryCode)(0x0434)		/* Libyan Arab Jamahiriya */
#define OtiCountryLI		(OtiTerminalCountryCode)(0x0438)		/* Liechtenstein */
#define OtiCountryLT		(OtiTerminalCountryCode)(0x0440)		/* Lithuania */
#define OtiCountryLU		(OtiTerminalCountryCode)(0x0442)		/* Luxembourg */
#define OtiCountryMG		(OtiTerminalCountryCode)(0x0450)		/* Madagascar */
#define OtiCountryMW		(OtiTerminalCountryCode)(0x0454)		/* Malawi */
#define OtiCountryMY		(OtiTerminalCountryCode)(0x0458)		/* Malaysia */
#define OtiCountryMV		(OtiTerminalCountryCode)(0x0462)		/* Maldives */
#define OtiCountryML		(OtiTerminalCountryCode)(0x0466)		/* Mali */
#define OtiCountryMT		(OtiTerminalCountryCode)(0x0470)		/* Malta */
#define OtiCountryMH		(OtiTerminalCountryCode)(0x0584)		/* Marshall Islands */
#define OtiCountryMQ		(OtiTerminalCountryCode)(0x0474)		/* Martinique */
#define OtiCountryMR		(OtiTerminalCountryCode)(0x0478)		/* Mauritania */
#define OtiCountryMU		(OtiTerminalCountryCode)(0x0480)		/* Mauritius */
#define OtiCountryYT		(OtiTerminalCountryCode)(0x0175)		/* Mayotte*/
#define OtiCountryMX		(OtiTerminalCountryCode)(0x0484)		/* Mexico  */
#define OtiCountryFM		(OtiTerminalCountryCode)(0x0583)		/* Micronesia (Federated States of)*/
#define OtiCountryMC		(OtiTerminalCountryCode)(0x0492)		/* Monaco */
#define OtiCountryMN		(OtiTerminalCountryCode)(0x0496)		/* Mongolia */
#define OtiCountryMS		(OtiTerminalCountryCode)(0x0500)		/* Montserrat */
#define OtiCountryMA		(OtiTerminalCountryCode)(0x0504)		/* Morocco */
#define OtiCountryMZ		(OtiTerminalCountryCode)(0x0508)		/* Mozambique */
#define OtiCountryMM		(OtiTerminalCountryCode)(0x0104)		/* Myanmar */
#define OtiCountryNA		(OtiTerminalCountryCode)(0x0516)		/* Namibia */
#define OtiCountryNR		(OtiTerminalCountryCode)(0x0520)		/* Nauru */
#define OtiCountryNP		(OtiTerminalCountryCode)(0x0524)		/* Nepal */
#define OtiCountryNL		(OtiTerminalCountryCode)(0x0528)		/* Netherlands */
#define OtiCountryNC		(OtiTerminalCountryCode)(0x0540)		/* New Caledonia */
#define OtiCountryNZ		(OtiTerminalCountryCode)(0x0554)		/* New Zealand */
#define OtiCountryNI		(OtiTerminalCountryCode)(0x0558)		/* Nicaragua */
#define OtiCountryNE		(OtiTerminalCountryCode)(0x0562)		/* Niger */
#define OtiCountryNG		(OtiTerminalCountryCode)(0x0566)		/* Nigeria */
#define OtiCountryNU		(OtiTerminalCountryCode)(0x0570)		/* Niue */
#define OtiCountryNF		(OtiTerminalCountryCode)(0x0574)		/* Norfolk Island */
#define OtiCountryMP		(OtiTerminalCountryCode)(0x0580)		/* Northern Mariana Islands */
#define OtiCountryNO		(OtiTerminalCountryCode)(0x0578)		/* Norway */
#define OtiCountryOM		(OtiTerminalCountryCode)(0x0512)		/* Oman */
#define OtiCountryPK		(OtiTerminalCountryCode)(0x0586)		/* Pakistan */
#define OtiCountryPW		(OtiTerminalCountryCode)(0x0585)		/* Palau */
#define OtiCountryPA		(OtiTerminalCountryCode)(0x0591)		/* Panama */
#define OtiCountryPG		(OtiTerminalCountryCode)(0x0598)		/* Papua New Guinea */
#define OtiCountryPY		(OtiTerminalCountryCode)(0x0600)		/* Paraguay */
#define OtiCountryPE		(OtiTerminalCountryCode)(0x0604)		/* Peru */
#define OtiCountryPH		(OtiTerminalCountryCode)(0x0608)		/* Philippines */
#define OtiCountryPN		(OtiTerminalCountryCode)(0x0612)		/* Pitcairn */
#define OtiCountryPL		(OtiTerminalCountryCode)(0x0616)		/* Poland */
#define OtiCountryPT		(OtiTerminalCountryCode)(0x0620)		/* Portugal */
#define OtiCountryPR		(OtiTerminalCountryCode)(0x0630)		/* Puerto Rico */
#define OtiCountryQA		(OtiTerminalCountryCode)(0x0634)		/* Qatar */
#define OtiCountryRE		(OtiTerminalCountryCode)(0x0638)		/* R�union */
#define OtiCountryRO		(OtiTerminalCountryCode)(0x0642)		/* Romania */
#define OtiCountryRU		(OtiTerminalCountryCode)(0x0643)		/* Russian Federation */
#define OtiCountryRW		(OtiTerminalCountryCode)(0x0646)		/* Rwanda */
#define OtiCountryLC		(OtiTerminalCountryCode)(0x0662)		/* Saint Lucia */
#define OtiCountryWS		(OtiTerminalCountryCode)(0x0882)		/* Samoa */
#define OtiCountrySM		(OtiTerminalCountryCode)(0x0674)		/* San Marino */
#define OtiCountryST		(OtiTerminalCountryCode)(0x0678)		/* Sao Tome and Principe */
#define OtiCountrySA		(OtiTerminalCountryCode)(0x0682)		/* Saudi Arabia */
#define OtiCountrySN		(OtiTerminalCountryCode)(0x0686)		/* Senegal */
#define OtiCountrySC		(OtiTerminalCountryCode)(0x0690)		/* Seychelles */
#define OtiCountrySL		(OtiTerminalCountryCode)(0x0694)		/* Sierra Leone */
#define OtiCountrySG		(OtiTerminalCountryCode)(0x0702)		/* Singapore */
#define OtiCountrySK		(OtiTerminalCountryCode)(0x0703)		/* Slovakia */
#define OtiCountrySI		(OtiTerminalCountryCode)(0x0705)		/* Slovenia */
#define OtiCountrySB		(OtiTerminalCountryCode)(0x0090)		/* Solomon Islands */
#define OtiCountrySO		(OtiTerminalCountryCode)(0x0706)		/* Somalia */
#define OtiCountryZA		(OtiTerminalCountryCode)(0x0710)		/* South Africa */
#define OtiCountryES		(OtiTerminalCountryCode)(0x0724)		/* Spain */
#define OtiCountryLK		(OtiTerminalCountryCode)(0x0144)		/* Sri Lanka */
#define OtiCountrySD		(OtiTerminalCountryCode)(0x0736)		/* Sudan */
#define OtiCountrySR		(OtiTerminalCountryCode)(0x0740)		/* Suriname */
#define OtiCountrySJ		(OtiTerminalCountryCode)(0x0744)		/* Svalbard and Jan Mayen Islands */
#define OtiCountrySZ		(OtiTerminalCountryCode)(0x0748)		/* Swaziland */
#define OtiCountrySE		(OtiTerminalCountryCode)(0x0752)		/* Sweden */
#define OtiCountryCH		(OtiTerminalCountryCode)(0x0756)		/* Switzerland */
#define OtiCountrySY		(OtiTerminalCountryCode)(0x0760)		/* Syrian Arab Republic */
#define OtiCountryTJ		(OtiTerminalCountryCode)(0x0762)		/* Tajikistan */
#define OtiCountryTH		(OtiTerminalCountryCode)(0x0764)		/* Thailand */
#define OtiCountryTG		(OtiTerminalCountryCode)(0x0768)		/* Togo */
#define OtiCountryTK		(OtiTerminalCountryCode)(0x0772)		/* Tokelau */
#define OtiCountryTO		(OtiTerminalCountryCode)(0x0776)		/* Tonga */
#define OtiCountryTT		(OtiTerminalCountryCode)(0x0780)		/* Trinidad and Tobago */
#define OtiCountryTN		(OtiTerminalCountryCode)(0x0788)		/* Tunisia */
#define OtiCountryTR		(OtiTerminalCountryCode)(0x0792)		/* Turkey */
#define OtiCountryTM		(OtiTerminalCountryCode)(0x0795)		/* Turkmenistan */
#define OtiCountryTC		(OtiTerminalCountryCode)(0x0796)		/* Turks and Caicos Islands */
#define OtiCountryTV		(OtiTerminalCountryCode)(0x0798)		/* Tuvalu */
#define OtiCountryUG		(OtiTerminalCountryCode)(0x0800)		/* Uganda */
#define OtiCountryUA		(OtiTerminalCountryCode)(0x0804)		/* Ukraine */
#define OtiCountryAE		(OtiTerminalCountryCode)(0x0784)		/* United Arab Emirates */
#define OtiCountryGB		(OtiTerminalCountryCode)(0x0826)		/* United Kingdom of Great Britain and Northern Ireland*/
#define OtiCountryUS		(OtiTerminalCountryCode)(0x0840)		/* United States of America*/
#define OtiCountryVI		(OtiTerminalCountryCode)(0x0850)		/* United States Virgin Islands */
#define OtiCountryUY		(OtiTerminalCountryCode)(0x0858)		/* Uruguay */
#define OtiCountryUZ		(OtiTerminalCountryCode)(0x0860)		/* Uzbekistan */
#define OtiCountryVU		(OtiTerminalCountryCode)(0x0548)		/* Vanuatu */
#define OtiCountryVE		(OtiTerminalCountryCode)(0x0862)		/* Venezuela (Bolivarian Republic of)*/
#define OtiCountryVN		(OtiTerminalCountryCode)(0x0704)		/* Viet Nam */
#define OtiCountryWF		(OtiTerminalCountryCode)(0x0876)		/* Wallis and Futuna Islands */
#define OtiCountryEH		(OtiTerminalCountryCode)(0x0732)		/* Western Sahara */
#define OtiCountryYE		(OtiTerminalCountryCode)(0x0887)		/* Yemen */
#define OtiCountryZM		(OtiTerminalCountryCode)(0x0894)		/* Zambia */
#define OtiCountryZW		(OtiTerminalCountryCode)(0x0716)		/* Zimbabwe */

#define OtiCountryUnknown	(OtiTerminalCountryCode)(-1)

typedef int32_t OtiPaymentScheme;
#define OtiPaymentSchemeMagStripe		(OtiPaymentScheme)(1)
#define OtiPaymentSchemeEMV			(OtiPaymentScheme)(2)
#define OtiPaymentSchemeMagStripeEMV		(OtiPaymentScheme)(3)

#define OtiPaymentSchemeUnknown			(OtiPaymentScheme)(-1)

/**
 * @enum OtiKeyStoreRegion
 *
 * Defines the PCD Region for keeping the Mifare Keys.
 */
typedef int32_t OtiKeyStoreRegion;
#define OtiKeyStoreRfDeviceE2			(OtiKeyStoreRegion)(0)
#define OtiKeyStoreRfDeviceRAM			(OtiKeyStoreRegion)(1)

#define OtiKeyStoreUnknown			(OtiKeyStoreRegion)(-1)

/**
 * @enum OtiTermOnlineStat
 *
 * Defines the terminal online status.
 */
typedef int32_t OtiTermOnlineStat;
#define OtiTermOnlineStatFailed			(OtiTermOnlineStat)(0)
#define OtiTermOnlineStatSucceeded		(OtiTermOnlineStat)(1)
#define OtiTermOnlineStatRestartTimer		(OtiTermOnlineStat)(2)

#define OtiTermOnlineStatUnknown		(OtiTermOnlineStat)(-1)

/**
 * @enum OtiTermType
 *
 * Defines the terminal type.
 */
typedef int32_t OtiTermType;
#define OtiTermTypeAttFinOnline			(OtiTermType)(0x11)
#define OtiTermTypeAttFinOfflineOnline		(OtiTermType)(0x12)
#define OtiTermTypeAttFinOffline		(OtiTermType)(0x13)
#define OtiTermTypeUnattFinOnline		(OtiTermType)(0x14)
#define OtiTermTypeUnattFinOfflineOnline	(OtiTermType)(0x15)
#define OtiTermTypeUnattFinOffline		(OtiTermType)(0x16)
#define OtiTermTypeAttMerchOnline		(OtiTermType)(0x21)
#define OtiTermTypeAttMerchOfflineOnline	(OtiTermType)(0x22)
#define OtiTermTypeAttMerchOffline		(OtiTermType)(0x23)
#define OtiTermTypeUnattMerchOnline		(OtiTermType)(0x24)
#define OtiTermTypeUnattMerchOfflineOnline	(OtiTermType)(0x25)
#define OtiTermTypeUnattMerchOffline		(OtiTermType)(0x26)
#define OtiTermTypeUnattCardhOnline		(OtiTermType)(0x34)
#define OtiTermTypeUnattCardhOfflineOnline	(OtiTermType)(0x35)
#define OtiTermTypeUnattCardhOffline		(OtiTermType)(0x36)

#define OtiTermTypeUnknown			(OtiTermType)(-1)

/**
 * @enum OtiVisaCryptoSubscheme
 *
 * Enables Host to control and select the crypto mechanism of a VISA Card transaction.
 */
typedef int32_t OtiVisaCryptoSubscheme;
#define OtiVisaCryptoSubschemedCVV		(OtiVisaCryptoSubscheme)(1)	/* dCVV Crypto Only */
#define OtiVisaCryptoSubschemeCVN17		(OtiVisaCryptoSubscheme)(2)	/* CVN 17 (Crypto 17) Only */
#define OtiVisaCryptoSubschemedCVVCVN17		(OtiVisaCryptoSubscheme)(3)	/* dCVV Crypto + CVN 17 */

#define OtiVisaCryptoSubschemeUnknown		(OtiVisaCryptoSubscheme)(-1)

/**
 * @enum OtiTermCap
 *
 * This section provides the coding for Terminal Capabilities.
 */
typedef int32_t OtiTermCap;
	/* byte 1) Card Data Input Capability */
#define OtiTermCapManKeyEntry		(OtiTermCap)(0x800000)	/* Manual key entry */
#define OtiTermCapMagnStripe		(OtiTermCap)(0x400000)	/* Magnetic stripe */
#define OtiTermCapContactIc		(OtiTermCap)(0x200000)	/* IC with contacts */
	/* byte 2) Card Data Input Capability */
#define OtiTermCapPlaintxtPin		(OtiTermCap)(0x008000)	/* Plaintext PIN for ICC verification */
#define OtiTermCapEncPin		(OtiTermCap)(0x004000) /* Enciphered PIN for online verification */
#define OtiTermCapSignature		(OtiTermCap)(0x002000)	/* Signature (paper) */
#define OtiTermCapEncPinOffline		(OtiTermCap)(0x001000)	/* Enciphered PIN for offline verification */
#define OtiTermCapNoCvm			(OtiTermCap)(0x000800)	/* No CVM Required */
	/* byte 3) CVM Capability */
#define OtiTermCapSDA			(OtiTermCap)(0x000080)	/* SDA */
#define OtiTermCapDDA			(OtiTermCap)(0x000040)	/* DDA */
#define OtiTermCapCardCapture		(OtiTermCap)(0x000020)	/* Card Capture */
#define OtiTermCapCDA			(OtiTermCap)(0x000008)	/* CDA */

#define OtiTermCapUnknown		(OtiTermCap)(-1)

/**
 * @enum OtiTCC
 *
 * Transaction Category Code
 */
typedef int32_t OtiTCC;
#define OtiTCCCash		(OtiTCC)('C')	/* Cash Disbursement */
#define OtiTCCAtm		(OtiTCC)('Z')	/* ATM Cash Disbursement */
#define OtiTCCSchool		(OtiTCC)('O')	/* College/School Expense Hospitalization */
#define OtiTCCHotel		(OtiTCC)('H')	/* Hotel/Motel and Cruise Ship Services */
#define OtiTCCTransport		(OtiTCC)('X')	/* Transportation */
#define OtiTCCAutomobile	(OtiTCC)('A')	/* Automobile/Vehicle Rental */
#define OtiTCCRestaurant	(OtiTCC)('F')	/* Restaurant */
#define OtiTCCMail		(OtiTCC)('T')	/* Mail/Telephone Order Preauthorized Order */
#define OtiTCCUnique		(OtiTCC)('U')	/* Unique Transaction */
#define OtiTCCRental		(OtiTCC)('R')	/* Retail/All Other Transactions */

#define OtiTCCUnknown		(OtiTCC)(-1)

/**
 * @enum OtiAmountZeroOnline
 *
 * If this flag is ON and the requested amount is zero) the PCD must perform an online transaction.
 */
typedef int32_t OtiAmountZeroOnline;
#define OtiAmountZeroOnlineOff	(OtiAmountZeroOnline)(0)
#define OtiAmountZeroOnlineOn	(OtiAmountZeroOnline)(1)

#define OtiAmountZeroOnlineUnknown	(OtiAmountZeroOnline)(-1)

/**
 * @enum OtiStatusCheck
 *
 * If this flag is ON and the requested amount is "00 00 00 00 01 00" (single unit currency)) and if the PICC presented is VISA) the PCD must perform an online transaction..
 */
typedef int32_t OtiStatusCheck;
#define OtiStatusCheckOff	(OtiStatusCheck)(0)
#define OtiStatusCheckOn	(OtiStatusCheck)(1)

#define OtiStatusCheckUnknown	(OtiStatusCheck)(-1)

/**
 * @enum OtiVisaFddaVers0
 *
 * When flag is ON (01) the old fDDA version 0 is enabled
 */
typedef int32_t OtiVisaFddaVers0;
#define OtiVisaFddaVers0Disable	(OtiVisaFddaVers0)(0)
#define OtiVisaFddaVers0Enable	(OtiVisaFddaVers0)(1)

#define OtiVisaFddaVers0Unknown	(OtiVisaFddaVers0)(-1)

/**
 * @enum OtiVisaWave
 *
 * When flag is ON (01) VISA WAVE2 application enabled
 */
typedef int32_t OtiVisaWave;
#define OtiVisaWaveDisable	(OtiVisaWave)(0)
#define OtiVisaWaveEnable	(OtiVisaWave)(1)

#define OtiVisaWaveUnknown	(OtiVisaWave)(-1)

/**
 * @enum OtiPcdProtectedCmd
 *
 * Instructs the PCD to Lock the Protected commands
 */
typedef int32_t OtiPcdProtectedCmd;
#define OtiPcdProtectedCmdDisable	(OtiPcdProtectedCmd)(0)	/* lock disabled */
#define OtiPcdProtectedCmdEnable	(OtiPcdProtectedCmd)(1)	/* lock enabled */

#define OtiPcdProtectedCmdUnknown	(OtiPcdProtectedCmd)(-1)

/**
 * @enum OtiRfCollisionMsg
 *
 * Enables Host to get an �RF collision state� warning when PCD is in Pending Poll for a PICC.
 */
typedef int32_t OtiRfCollisionMsg;
#define OtiRfCollisionMsgDisable	(OtiRfCollisionMsg)(0)
#define OtiRfCollisionMsgEnable		(OtiRfCollisionMsg)(1)

#define OtiRfCollisionMsgUnknown	(OtiRfCollisionMsg)(-1)

/**
 * @enum OtiTrackAid
 *
 * Determines if the application ID is returned with the track data.
 */
typedef int32_t OtiTrackAid;
#define OtiTrackAidNotFollowed	(OtiTrackAid)(0)
#define OtiTrackAidFollowed	(OtiTrackAid)(1)

#define OtiTrackAidUnknown	(OtiTrackAid)(-1)

/**
 * @enum OtiPupi
 *
 * Return PUPI of a detected Type B PICC during polling operation. 
 * Please note that this response is available only if �NON Full transaction� 
 * PICCs or Poll Modes (03/04) are used. 
 * Else the PCD will complete transaction and sent �PICC transaction Data� to Host.
 */
typedef int32_t OtiPupi;
#define OtiPupiDisable	(OtiPupi)(0)
#define OtiPupiEnable	(OtiPupi)(1)

#define OtiPupiUnknown	(OtiPupi)(-1)

/**
 * @enum OtiUid
 *
 * Return UID of a detected Type A/Mifare/Vicinity during polling operation.
 */
typedef int32_t OtiUid;
#define OtiUidDisable	(OtiUid)(0)
#define OtiUidEnable	(OtiUid)(1)

#define OtiUidUnknown	(OtiUid)(-1)

/**
 * @enum OtiAtqb
 *
 * Return ATQB of a detected Type B PICC during polling operation.
 */
typedef int32_t OtiAtqb;
#define OtiAtqbDisable	(OtiAtqb)(0)
#define OtiAtqbEnable	(OtiAtqb)(1)

#define OtiAtqbUnknown	(OtiAtqb)(-1)

/**
 * @enum OtiAts
 *
 * Return ATS of a detected Type A PICC during polling operation.
 */
typedef int32_t OtiAts;
#define OtiAtsDisable	(OtiAts)(0)
#define OtiAtsEnable	(OtiAts)(1)

#define OtiAtsUnknown	(OtiAts(-1)

/**
 * @enum OtiSak
 *
 * Return SAK of a detected Type A PICC during polling operation.
 */
typedef int32_t OtiSak;
#define OtiSakDisable	(OtiSak)(0)
#define OtiSakEnable	(OtiSak)(1)

#define OtiSakUnknown	(OtiSak)(-1)

/**
 * @enum OtiRfBitRate
 *
 * Return PICC - PCD RF bitrates.
 */
typedef int32_t OtiRfBitRate;
#define OtiRfBitRatePCD106	(OtiRfBitRate)(0x00)
#define OtiRfBitRatePCD212	(OtiRfBitRate)(0x01)
#define OtiRfBitRatePCD424	(OtiRfBitRate)(0x02)
#define OtiRfBitRatePCD848	(OtiRfBitRate)(0x03)

#define OtiRfBitRatePICC106	(OtiRfBitRate)(0x00)
#define OtiRfBitRatePICC212	(OtiRfBitRate)(0x10)
#define OtiRfBitRatePICC424	(OtiRfBitRate)(0x20)
#define OtiRfBitRatePICC848	(OtiRfBitRate)(0x30)

#define OtiRfBitRateUnknown	(OtiRfBitRate)(-1)

/**
 * @enum OtiAppSelectInd
 *
 * Application Selection Indicator.
 */
typedef int32_t OtiAppSelectInd;
#define OtiAppSelectIndSupported	(OtiAppSelectInd)(0)
#define OtiAppSelectIndNotSupported	(OtiAppSelectInd)(1)

#define OtiAppSelectIndUnknown		(OtiAppSelectInd)(-1)

typedef int32_t OtiReaderInterfaceType;
#define OtiReaderInterfaceTypeSerial 	(OtiReaderInterfaceType)(0)
#define OtiReaderInterfaceTypeSTM32	(OtiReaderInterfaceType)(1)

typedef int32_t OtiReaderMode;
#define OtiReaderModeDefault		(OtiReaderMode)(0)
#define OtiReaderModeCallback		(OtiReaderMode)(1)

/**
 * @enum OtiVerbosity
 *
 * Verbosity Level.
 */
typedef int32_t OtiVerbosity;
#define OtiVerbosityNone		(OtiVerbosity)(0)
#define OtiVerbosityIo			(OtiVerbosity)(1)
#define OtiVerbosityError		(OtiVerbosity)(2)
#define OtiVerbosityCommands		(OtiVerbosity)(4)
#define OtiVerbosityAll			(OtiVerbosityIo|OtiVerbosityError|OtiVerbosityCommands)

#define OtiVerbosityUnknown		(OtiVerbosity)(-1)

/**
 * @enum OtiFWCompatibility
 *
 * Firmware compatibility: in new firmware releases, some tags may require a different
 * size of data field.
 */
typedef int32_t OtiFWCompatibility;
#define OtiFWCompatibilityNone		(OtiFWCompatibility)(0)
#define OtiFWCompatibility040506        (OtiFWCompatibility)(1)

#define OtiFWCompatibilityLast          (OtiFWCompatibility040506)

#define OtiFWCompatibilityUnknown	(OtiFWCompatibility)(-1)

/**
 * SR Commands: to be used in OTI_IOCTL_SR_COMMANDS: see SRI(X)512 but also SRT512, SRI(X)4K, SR176, SRI2K
 *
 */
#define SR_READ_BLOCK		0x8
#define SR_WRITE_BLOCK		0x9
#define SR_GET_UID		0xb
#define SR_RESET_TO_INVENTORY	0xc
#define SR_SELECT		0xe
#define SR_COMPLETION		0xf
/*
 * @struct __ExtendedPollConfig
 *
 * This structure is used in OTI_IOCTL_GET_EXT_POLL_CONFIG, OTI_IOCTL_SET_EXT_POLL_CONFIG and allows some control over the behavior
 * of the extended poll(s) for SR and other tickets (OtiPollPendingOnceExt and OtiPollImmediateExt)
 *
 */
#define SR_DEFAULT_FWTI_POLL		4	/* +/- 5 msec */
#define SR_DEFAULT_FWTI_REMOVE		3	/* +/- 2.5 msec */
#define SR_DEFAULT_PRE_RF_ON		0	/* most firmware revisions don't need this */
#define SR_DEFAULT_POST_RF_OFF		0	/* and if it is not switched on why switch it off */
#define SR_DEFAULT_NO_TICKET_SLEEP	0	/* sleep time if no ticket was detected during previous extended poll cycle */

typedef struct __ExtendedPollConfig
{
	unsigned char fwti_poll;	/* frame waiting time initialized before the SR ticket poll - and in use during subsequent sr application handling */
	unsigned char fwti_remove;	/* frame waiting time initialized before the SR ticket poll - and in use during subsequent sr application handling */
	unsigned char pre_rf_on;	/* 0,1 activate rf field before extended poll - required with some fw versions like ARM_OPMT_040506_3h */
	unsigned char post_rf_off;	/* 0,1 de-activate rf field after extended poll if no ticket is detected */
	unsigned char no_ticket_sleep;	/* time in msec to sleep if no ticket is detected */
	unsigned char activating_poll;	/* type A/B card detected, this is the poll that activates the card - default OtiPollImmediate vs. OtiPollPendingOnce */
	unsigned char tlc_appli;	/* Internal: current TLC copy as configured by application: default = 0 = pcd is responsible, 1 = host is resp */
	unsigned char tlc;		/* Internal: current TLC copy as configured by ExtendedPoll: used internally to decide if do is required */

} ExtendedPollConfig;

/**
 * @enum OpCode
 *
 * Enumeration of OpCodes known by the OTI reader.
 *
 * Added to API to allow use at application level
 */
typedef enum
{
	SET	= 0x3C,
	GET = 0x3D,
	DO = 0x3E,
	NAK = 0x15
} OpCode;

/**
 * @struct __OtiCardInfo
 *
 * This structure will be used as a poll response by the OTI reader.
 */
typedef struct __OtiCardInfo
{
	char 				data[900];					/*!	data buffer containing raw response data */
	size_t				length;						/*! length of data stored in data buffer */

	char*				track1;						/*! pointer in data buffer to start of track1 */
	size_t				length_track1;				/*! length of track1 data stored in data buffer */

	char*				track2;						/*! pointer in data buffer to start of track1 */
	size_t				length_track2;				/*! length of track1 data stored in data buffer */

	char*				aid;						/*! pointer in data buffer to start of aid */
	size_t				length_aid;					/*! length of aid data stored in data buffer */

	char*				pupi;						/*! pointer in data buffer to start of pupi */
	size_t				length_pupi;				/*! length of pupi data stored in data buffer */

	char*				uid;						/*! pointer in data buffer to start of uid */
	size_t				length_uid;					/*! length of uid data stored in data buffer */

	char*				atqb;						/*! pointer in data buffer to start of atq */
	size_t				length_atqb;				/*! length of atq data stored in data buffer */

	char*				ats;						/*! pointer in data buffer to start of ats */
	size_t				length_ats;					/*! length of ats data stored in data buffer */

	char*				sak;						/*! pointer in data buffer to start of sak */
	size_t				length_sak;					/*! length of sak data stored in data buffer */

	OtiPiccType			piccType;					/*! picc type of the card that has been detected */

	char				error;						/*! error that occured on card during poll */
	char*				amountAuthorised;			/*! pointer in data buffer to Amount Authorised */
	size_t				length_amountAuthorised;	/*! length of amountAuthorised data stored in data buffer */

	char*				transactionDate;			/*! pointer in data buffer to transaction date */
	size_t				length_transactionDate;		/*! length of transaction date stored in data buffer */

	OtiTransactionType	transactionType;			/*! transaction type that has been detected */

	char*				transactionTime;			/*! pointer in data buffer to transaction time */
	size_t				length_transactionTime;		/*! length of transaction time stored in data buffer */

	char*				cvmResult;					/*! pointer in data buffer to Card holder Verification results */
	size_t				length_cvmResult;			/*! length of Card holder Verification results stored in data buffer */

	char*				termVerification;			/*! pointer in data buffer to Terminal Verification results */
	size_t				length_termVerification;	/*! length of Terminal Verification results stored in data buffer */

	char*				transactionStatus;			/*! pointer in data buffer to Transaction Status information */
	size_t				length_transactionStatus;	/*! length of Transaction Status information stored in data buffer */

	char				capki;						/*! Certification Authority Public Key Index */

	char*				issAppData;					/*! pointer in data buffer to Issuer Application Data */
	size_t				length_issAppData;			/*! length of Issuer Application Data stored in data buffer */

	char*				unpredictableNbr;			/*! pointer in data buffer to Unpredictable Number */
	size_t				length_unpredictableNbr;	/*! length of Unpredictable Number stored in data buffer */

	char*				appInterchangeProf;			/*! pointer in data buffer to Application Interchange Profile */
	size_t				length_appInterchangeProf;	/*! length of Application Interchange Profile stored in data buffer */

	char*				DFName;						/*! pointer in data buffer to Dedicated File (DF) Name */
	size_t				length_DFName;				/*! length of Dedicated File (DF) Name stored in data buffer */

	OtiCryptoInfoData	cryptoInfoData;				/*! Cryptogram Information Data */

	char*				appCrypto;					/*! pointer in data buffer to Application Cryptogram */
	size_t				length_appCrypto;			/*! length of Application Cryptogram stored in data buffer */

	char*				appTransactionCnt;			/*! pointer in data buffer to Cardholder Verification Method (CVM) List */
	size_t				length_appTransactionCnt;	/*! length of Cardholder Verification Method (CVM) List stored in data buffer */

	char*				cvmList;					/*! pointer in data buffer to Application Transaction Counter */
	size_t				length_cvmList;				/*! length of Application Transaction Counter stored in data buffer */

	OtiTransactionRes	transactionResult;			/*! PCD Transaction Result */

	char*				dataAuthCode;				/*! pointer in data buffer to Data Authentication Code */
	size_t				length_dataAuthCode;		/*! length of Data Authentication Code stored in data buffer */

	char*				termTransactQual;			/*! pointer in data buffer to Terminal Transaction Qualifiers */
	size_t				length_termTransactQual;	/*! length of Terminal Transaction Qualifiers stored in data buffer */

	char				dataChannel;				/*! Data Channel */

	char*				customerExclData;			/*! pointer in data buffer to start of Customer Exclusive Data */
	size_t				length_customerExclData;	/*! length of Customer Exclusive Data data stored in data buffer */

	char*				bankIdCode;					/*! pointer in data buffer to start of Bank Identifier Code */
	size_t				length_bankIdCode;			/*! length of Bank Identifier Code data stored in data buffer */

	char*				cardholderNameExt;			/*! pointer in data buffer to start of Cardholder Name Extended */
	size_t				length_cardholderNameExt;	/*! length of Cardholder Name Extended data stored in data buffer */

	char*				appDiscrData;				/*! pointer in data buffer to start of Application Discretionary Data */
	size_t				length_appDiscrData;		/*! length of Application Discretionary data stored in data buffer */

	char*				formFactorInd;				/*! pointer in data buffer to start of Form Factor Indicator (9F 6E VISA) */
	size_t				length_formFactorInd;		/*! length of Form Factor Indicator (9F 6E VISA) data stored in data buffer */

	char*				payPassThPartyData;			/*! pointer in data buffer to start of PayPass Third Party Data (9F 6E MC) */
	size_t				length_payPassThPartyData;	/*! length of PayPass Third Party Data (9F 6E MC) data stored in data buffer */

	char*				tranactionSeqCnt;			/*! pointer in data buffer to start of Transaction Sequence Counter */
	size_t				length_tranactionSeqCnt;	/*! length of Transaction Sequence Counter data stored in data buffer */

	char*				discrDataTrack1;			/*! pointer in data buffer to start of Discretionary Data Track1 */
	size_t				length_discrDataTrack1;		/*! length of Discretionary Data Track1 data stored in data buffer */

	char*				discrDataTrack2;			/*! pointer in data buffer to start of Discretionary Data Track2 */
	size_t				length_discrDataTrack2;		/*! length of Discretionary Data Track2 data stored in data buffer */

	char*				ndotSecFrame;				/*! pointer in data buffer to start of NDoT secured card frame */
	size_t				length_ndotSecFrame;		/*! length of NDot secured card frame data stored in data buffer */

	char*				ndotOfflineBalance;			/*! pointer in data buffer to start of NDoT offline balance */
	size_t				length_ndotOfflineBalance;	/*! length of NDot offline balance data stored in data buffer */

	char*				ndotSecFixFrame1;			/*! pointer in data buffer to start of NDoT secured fixed frame 1 */
	size_t				length_ndotSecFixFrame1;	/*! length of NDot secured fixed frame 1 data stored in data buffer */

	char*				ndotUnsecFixFrame1;			/*! pointer in data buffer to start of NDoT unsecured fixed frame 1 */
	size_t				length_ndotUnsecFixFrame1;	/*! length of NDot unsecured fixed frame 1 data stored in data buffer */

	char*				ndotSecFixFrame2;			/*! pointer in data buffer to start of NDoT secured fixed frame 2 */
	size_t				length_ndotSecFixFrame2;	/*! length of NDot secured fixed frame 2 data stored in data buffer */

	char*				ndotUnsecFixFrame2;			/*! pointer in data buffer to start of NDoT unsecured fixed frame 2 */
	size_t				length_ndotUnsecFixFrame2;	/*! length of NDot unsecured fixed frame 2 data stored in data buffer */

	char*				ndotSecFixFrame3;			/*! pointer in data buffer to start of NDoT secured fixed frame 3 */
	size_t				length_ndotSecFixFrame3;	/*! length of NDot secured fixed frame 3 data stored in data buffer */

	char*				ndotUnsecFixFrame3;			/*! pointer in data buffer to start of NDoT unsecured fixed frame 3 */
	size_t				length_ndotUnsecFixFrame3;	/*! length of NDot unsecured fixed frame 3 data stored in data buffer */

	char*				ndotUnsecGenVarFrame;		/*! pointer in data buffer to start of NDoT unsecured general variable frame */
	size_t				length_ndotUnsecGenVarFrame;/*! length of NDot unsecured general variable frame data stored in data buffer */

	char				reserved[3];

} OtiCardInfo, *POtiCardInfo;


/*-- Structure declaration --*/
/**
 * @struct __OtiReaderOpen
 *
 * Structure that will contain the information used to initialise the reader.
 */
typedef struct __OtiOpen
{
	char path[64];								/*! path to the serial device where the reader is connected to */
	speed_t baudrate;							/*! baudrate of to communicate with the reader */
    OtiReaderInterfaceType interfaceType;                          /*! reader communication interface type */
	OtiReaderMode mode;						/* reader mode: default = classic + support for external display messages = OtiReaderModeCallback */
} OtiOpen, *POtiOpen;

/**
 * @struct __OtiPaymentScheme
 *
 * Structure that will contain the information used for the PollNDoT function.
 */
typedef struct __NDoTOperatorId
{
		char	country[2];
		char	province[2];
		char	municipalityType;
		int		municipality;
		char	oper[5];
} NDoTOperatorId, *PNDoTOperatorId;

/**
 * @struct __OtiEmvConfig
 *
 * Structure that will contain the information used for the PollNDoT function.
 */
typedef struct __OtiEmvConfig
{
		unsigned char	AIDLen;
		unsigned char	*AID;
		unsigned char	dataLen;
//		unsigned char	*data;
		union 
		{
			unsigned char			*data;
			OtiPaymentScheme		paymentScheme;
			OtiTerminalCountryCode	currencyCode;
			OtiTermCap				termCap;
                        OtiTermType                     termType;
		};
} OtiEmvConfig, *POtiEmvConfig;

/**
 * @struct __OtiReaderTimeout
 *
 * Structure used to initialize/get/set the timeouts of main 3 oti exchanges
 */
typedef struct __OtiReaderTimeout
{
	int write;
	int ack;
	int response;
} OtiReaderTimeout, *POtiReaderTimeout;

/*-- Function declarations --*/
void* oti_init( void );
int oti_deinit( void* handle );
void* oti_open( void* handle, POtiOpen open );
int oti_close( void* handle );
int oti_ioctl( void* handle, OtiIoctl command, const void* tx, size_t txLen, void* rx, size_t rxLen );

#ifdef __cplusplus
}
#endif

#endif /* _PRODATA_SYSTEMS_OTI_H_ */



