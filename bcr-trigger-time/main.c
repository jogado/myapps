#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>


#include "main.h"

#include "comm.h"


#define MAX_TASKS 3
pthread_t tid[MAX_TASKS];


int count =0;

extern int 	main			( void       );
extern void 	*Task0			( void *arg  );
extern void 	*Task1			( void *arg  );
extern void 	*Task2			( void *arg  );
extern char 	*MyDateTime		( int mode   );
extern int 	GetTaskID		( void       );
extern void 	system_call 		( const char *s ) ;
extern void 	BCR_trigger_off		( void );
extern void 	BCR_trigger_on		( void );
extern char 	*Build_dateTime		( void );



tQUEUE		Q_USART0    , *pQ_USART0;
u16 		Buffer_USART0 [Q_SIZE];


typedef struct DateAndTime
{
    int year;
    int month;
    int day;
    int hour;
    int minutes;
    int seconds;
    int msec;
}DateAndTime;



char aMyDateTime[50];      // (DDD)HH:MM:SS.SSS
char aMyTime    [50];      // (DDD)HH:MM:SS.SSS


unsigned long Elapsed_ms = 0;

struct timeval tStartUp;

char *MyDateTime(int mode)
{
    DateAndTime date_and_time;
    struct timeval tCurrent;
    struct tm *tm;

    struct timeval tDiff;

    gettimeofday(&tCurrent, NULL);

    timersub(  &tCurrent, &tStartUp , &tDiff );

    tm = localtime(&tDiff.tv_sec);
//    tm = localtime(&tv.tv_sec);


    // Add 1900 to get the right year value
    // read the manual page for localtime()
    date_and_time.year 		= tm->tm_year + 1900;
    // Months are 0 based in struct tm
    date_and_time.month 	= tm->tm_mon + 1;
    date_and_time.day 		= tm->tm_mday;
    date_and_time.hour 		= tm->tm_hour;
    date_and_time.minutes 	= tm->tm_min;
    date_and_time.seconds 	= tm->tm_sec;
    date_and_time.msec 		= (int) (tDiff.tv_usec / 1000);


	if(mode==1)
	    sprintf(aMyDateTime, "%02d:%02d:%02d.%03d %02d-%02d-%04d",
    	    date_and_time.hour,
    	    date_and_time.minutes,
    	    date_and_time.seconds,
    	    date_and_time.msec,
    	    date_and_time.day,
    	    date_and_time.month,
    	    date_and_time.year
    		);
	else
	    sprintf(aMyTime, "%02d:%02d:%02d.%03d",
    	    date_and_time.hour,
    	    date_and_time.minutes,
    	    date_and_time.seconds,
    	    date_and_time.msec
    		);

    return aMyTime;
}


//=============================================================================
int GetTaskID(void)
{
	int Task_ID = -1;
	int i;

   	pthread_t id = pthread_self();

    	for(i=0; i < MAX_TASKS ; i++)
    	{
		if(pthread_equal(id,tid[i]))
		{
			Task_ID = i;
	        	printf("\n I'm task :%d \n\n",Task_ID);
			break;
		}
	}
	return(Task_ID);	
}
//=============================================================================







//=============================================================================

void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}

void BCR_trigger_off(void)
{
	printf("BCR_trigger_off()\n\r");
	system_call("echo  0 > /sys/class/leds/barcode_trigger/brightness");
}

void BCR_trigger_on(void)
{
	printf("BCR_trigger_on()\n\r");
	system_call("echo  1 > /sys/class/leds/barcode_trigger/brightness");
}
//=============================================================================


struct timeval timer_1;
struct timeval timer_2;
struct timeval timer_delta;
void *Task0(void *arg)
{
	int Task_ID;
	int i = 0;
	int timeout;
	int n;
	unsigned char c;
	int got_char=0;
	int count;
	//struct tm *tm;

	Task_ID = GetTaskID();
	if(Task_ID <0 )
    	return NULL;



	fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname, strerror (errno));
	        return(NULL);
	}

	Baudrate = B9600 ;
	Baudrate = B115200 ;
	set_interface_attribs (fd, Baudrate, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (fd, 0);    



		
	while(1)
   	{	

		//====================================
		printf("===================================\n\r");
		BCR_trigger_off();
		
		for (i=0; i < 5 ; i++)
	   	{
			printf(" Sleeping .... %d \n\r",(int)i );
			sleep(1); 	
		}



		//====================================
		printf("\n\rTrigger ON \n\r");
		BCR_trigger_on();
		gettimeofday(&timer_1, NULL);
	


		//==============================================================
		// usleep(20000);
		//==============================================================
		timeout=0;
		got_char=0;
		while(1)
		{
			n = read (fd, &c, 1);  // read up to 100 characters if ready to read
			if(n)
			{
				Q_Put ( pQ_USART0, (u16) c ) ;
				got_char++;
				break;
			}	
	
			if (timeout++ > 7 )
			{
				printf("(TIMEOUT)\n\r");
				break;
			}
			usleep(1000);
		}

		//==============================================================
		if(got_char)
		{
			//==============================================================
		 	gettimeofday(&timer_2, NULL);
	    		timersub(  &timer_2, &timer_1 , &timer_delta );
			printf("Got a BCR RX after : ( %03d ) ms\n\r", (int) (timer_delta.tv_usec / 1000));
			//==============================================================

			while(1)
			{
				n = read (fd, &c, 1);  // read up to 100 characters if ready to read
				if(n)
				{
					Q_Put ( pQ_USART0, (u16) c ) ;
 					continue;
					
				}	

				count=Q_Ninq(pQ_USART0);

				printf("Rx (%d) : ",count);

				for(i=0; i< count; i++)
				{
					c = (char )Q_Get( pQ_USART0 ) ;
					if(c == 0x0a )					
						printf("<0x0A>");
					else if (c == 0x0d )					
						printf("<0x0D>");
					else
						printf("%c",c);
				}	
				printf("\n\r");
				break;
			}

		}
		//==============================================================
		
   		sleep(1);
 	}
   	return NULL;
}







char completeVersion[50];

char *Build_dateTime(void)
{
	char aTmp[100];
	int Month = 0;
 	int Year  = 0;
 	int Day   = 0;

	sprintf(aTmp,"%s",__DATE__);

	if(memcmp(aTmp,"Jan",3)==0) Month=1;
	if(memcmp(aTmp,"F"  ,1)==0) Month=2;
	if(memcmp(aTmp,"Mar",3)==0) Month=3;
	if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
	if(memcmp(aTmp,"May",3)==0) Month=5;
	if(memcmp(aTmp,"Jun",3)==0) Month=6;
	if(memcmp(aTmp,"Jul",3)==0) Month=7;
	if(memcmp(aTmp,"Au" ,2)==0) Month=8;
	if(memcmp(aTmp,"S"  ,1)==0) Month=9;
	if(memcmp(aTmp,"O"  ,1)==0) Month=10;
	if(memcmp(aTmp,"N"  ,1)==0) Month=11;
	if(memcmp(aTmp,"D"  ,1)==0) Month=13;
	

	aTmp[6]=0;
	Day  = atoi(&aTmp[4]);
	Year = atoi(&aTmp[7]);

	sprintf(completeVersion,"%02d/%02d/%04d %s"
		,Day
		,Month
		,Year
		,__TIME__);

	return(completeVersion);
}
//=============================================================================================================================






int main(void)
{
	int i = 0;
	int err;


	printf("\n\r");
	printf("bcr-trigger-time (%s)\n\r",Build_dateTime() );
	printf("------------------------------------------\n\r");

	

	gettimeofday(&tStartUp, NULL);

	//==================================================
	// Initialise buffers
	//==================================================
	pQ_USART0 			      = &Q_USART0 ;
	pQ_USART0->QSize 		      = Q_SIZE ;
	pQ_USART0->pQ 			      = &Buffer_USART0[0];
	Q_Init (pQ_USART0);
   	//==================================================


    	i=0;
    	err = pthread_create(&(tid[i]), NULL, &Task0, NULL);
    	if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));


	while(1)
	{

	    sleep(1);
	}
	return 0;
}
