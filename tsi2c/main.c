#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/input.h>
#include <string.h>


#define I2C_ADDR 0x38
#define TOUCH_REG 0x00
#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

extern int main(void) ;
extern void i2c_read ( int reg , int response_len , unsigned char *buf );
extern void reset_ts(void);








int fd;
int ret;


/*
	reg = <0x38>;
		interrupt-parent = <&gpio4>;
		interrupts = <16 0>;
		int-gpio = <&gpio4 16 0>;
*/

/*
rese 1_3int main() {
*/

//================================================================================================
void reset_ts(void)
{

	printf("Reset touchscreen ....\n\r");

	system("echo 3 > /sys/class/gpio/export");

	system("echo out > /sys/class/gpio/gpio3/direction");
	usleep(100000);

	system("echo  1 > /sys/class/gpio/gpio3/value");
	usleep(100000);
	system("echo  0 > /sys/class/gpio/gpio3/value");
	usleep(100000);
	system("echo  1 > /sys/class/gpio/gpio3/value");
	usleep(100000);

	system("echo  3 > /sys/class/gpio/unexport");

	return ;
}
//================================================================================================





//===============================================================================================================
void i2c_read ( int reg , int ResponseLen , unsigned char *aBuf )
{

    unsigned char r;

    r = (unsigned char) reg;


    ret = write(fd, &r, 1);
    if (ret < 0) {
        perror("Failed to write to i2c device");
        close(fd);
        exit (-1);
    }

    usleep(10000);

    ret = read(fd, aBuf, ResponseLen);
    if (ret < 0) {
        perror("Failed to read from i2c device");
        close(fd);
        exit (-1);
    }
    return;
}
//===============================================================================================================






int FIRST=1;
char aEvent[20];
int Mode_Switch=0;
unsigned char buf[20];

int event=0;
int x=0;
int y=0;
int TouchId=0;
int Count=0;
int GestId=0;
char aGest[20];





int main()
{
    reset_ts();

    // Open i2c device
    fd = open("/dev/i2c-0", O_RDWR);
    if (fd < 0) {
        perror("Failed to open i2c device");
        return 1;
    }

    // Set slave address
    ret = ioctl(fd, I2C_SLAVE, I2C_ADDR);
    if (ret < 0) {
        perror("Failed to set i2c slave address");
        return 1;
    }




    //==================================================================
    // Show Mode_Switch
    //==================================================================
    i2c_read ( TOUCH_REG, 1 , &buf[0] );
    Mode_Switch = ((buf[0] >> 4 ) & 0x07);
    printf("Mode_Switch= %d\n\r", Mode_Switch);
    //==================================================================



    while (1)
    {
        if(Count++ > 500)
            break;

        //==================================================================
        // Read Nb of points
        //==================================================================
        i2c_read ( TOUCH_REG+2, 1 , &buf[0] );

        int nb = buf[0];

        if(nb == 0 || nb > 10 )
        {
            if( FIRST == 0 )
            {
                FIRST=1;
            }
            else
            {

                 usleep(10000);
                continue;
            }
        }
        else
        {
            FIRST=0;
        }

        printf("NbPoints=%d  ", nb);
        //==================================================================

        Count=0;



        //==================================================================
        // Read X coordinate
        //==================================================================
        i2c_read ( TOUCH_REG+3, 2 , &buf[0] );

        x = ( (buf[0]&0x0f) << 8) | buf[1];
        event = (buf[0] >> 6) & 0x03 ;

        switch(event)
        {
                case 0 : sprintf(aEvent ,"DOWN   "); break;
                case 1 : sprintf(aEvent ,"UP     "); break;
                case 2 : sprintf(aEvent ,"CONTACT"); break;
                case 3 : sprintf(aEvent ,"NO EVENT"); break;
                default: sprintf(aEvent ,"--------"); break;
        }

        printf("X=%03d  [%02d:%s]  ", x, event,aEvent );


        //==================================================================
        // Read Y coordinate
        //==================================================================
        i2c_read ( TOUCH_REG+5, 2 , &buf[0] );

        y = ( (buf[0]&0x0f) << 8) | buf[1];

        TouchId = (buf[0] >> 4) & 0x0F ;

        if( TouchId != 0x0f )
            printf("Y=%03d  [Id:%02d]   ", y,TouchId );
        else
            printf("Y=%03d  [Id:--]   ", y );




        //==================================================================
        // Check for touch gesture
        //==================================================================
        i2c_read ( TOUCH_REG+1, 1 , &buf[0] );
        GestId= buf[0];


        switch( GestId )
        {
                case 0x10 : sprintf(aGest ,"Move Up   "); break;
                case 0x14 : sprintf(aGest ,"Move Right"); break;
                case 0x18 : sprintf(aGest ,"Move Down "); break;
                case 0x1C : sprintf(aGest ,"Move Left "); break;
                case 0x48 : sprintf(aGest ,"Zoom in   "); break;
                case 0x49 : sprintf(aGest ,"Zoom out  "); break;
                case 0x00 : sprintf(aGest ,"No gesture"); break;
                default   : sprintf(aGest ,"----------"); break;
        }

        printf("Gest=%02X[%s]   ", GestId,aGest );




        //==================================================================
        // TOUCH1_WEIGHT
        //==================================================================
        i2c_read ( TOUCH_REG+7, 1 , &buf[0] );
        int weight_1 = buf[0];
        printf("Weight=%02X    ",weight_1);
        //==================================================================



        //==================================================================
        // TOUCH1_MISC
        //==================================================================
        i2c_read ( TOUCH_REG+8, 1 , &buf[0] );
        int misc_1 = ((buf[0]>>4) & 0x0f);
        printf("misc=%02X \n\r",misc_1);
        //==================================================================


        usleep(10000); // 10ms delay
    }

    // Close i2c device
    close(fd);
    return 0;
}

