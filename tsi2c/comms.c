#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>
#include "comms.h"


char devname[128] ;
int  devfd;

int TheSelectedBaudrate = B115200 ;

typedef struct {
    unsigned int baud;
    unsigned int cbaud;
} BaudDefines;

static BaudDefines baudDefines[] = {
    {   9600 , B9600   },
    {  19200 , B19200  },
    {  38400 , B38400  },
    {  57600 , B57600  },
    { 115200 , B115200 },
    { 230400 , B230400 },
    { 460800 , B460800 }
};

int RTS_flag ;
int DTR_flag ;

void closePort(int dev)
{
    //printf("Clear DTR/RTS\n\r");
    ioctl(dev,TIOCMBIC,&DTR_flag);//Set DTR pin
    ioctl(dev,TIOCMBIC,&RTS_flag);//Set DTR pin
    close(dev);
}


int openPort(char *name,unsigned int baud)
{
    struct termios ntio, otio;
    unsigned int i;
    unsigned int cbaud=B115200;
    int r;
    int Flags=0;


    if(baud ==9600   ) TheSelectedBaudrate = B9600;
    if(baud ==19200  ) TheSelectedBaudrate = B19200;
    if(baud ==38400  ) TheSelectedBaudrate = B38400;
    if(baud ==57600  ) TheSelectedBaudrate = B57600;
    if(baud ==115200 ) TheSelectedBaudrate = B115200;
    if(baud ==230400 ) TheSelectedBaudrate = B230400;
    if(baud ==460800 ) TheSelectedBaudrate = B460800;

    for( i = 0 ; i < sizeof(baudDefines)/sizeof(BaudDefines) ; i++ )
    {
        if(baudDefines[i].baud == baud)
        {
            cbaud =  baudDefines[i].cbaud;
            break;
        }
    }

    Flags |= O_RDWR ;
//	Flags |= O_NONBLOCK;
//	Flags |= O_NOCTTY;

    devfd = open(name, Flags);

    if( devfd < 0 )
    {
        printf("open [%s] failed %d %s\n", name, devfd, strerror(errno));
        exit(-1);
    }


    DTR_flag = TIOCM_DTR;
    RTS_flag = TIOCM_RTS;


    //printf("DTR/RTS Set \n\r");
    ioctl(devfd,TIOCMBIS,&DTR_flag);//Set DTR pin
    ioctl(devfd,TIOCMBIS,&RTS_flag);//Set DTR pin
    //sleep(1);


    if( ( r = tcgetattr( devfd, & otio ) ) < 0 ){
        printf("openPort [%s] error tcgetattr %d %s\n",
            name,r, strerror( errno ) );
        return r;
    }
    //=================================================
    bzero( & ntio, sizeof( struct termios ) );

    cfmakeraw( &ntio );

    ntio.c_lflag 		= 0;			/* c_lflag : local mode flags   */
    ntio.c_oflag 		= 0;			/* c_oflag : output mode flags  */
    ntio.c_cc[VMIN] 	= 0;
    ntio.c_cc[VTIME] 	= 0;

    ntio.c_cflag = CS8 | CLOCAL | CREAD;	/* c_oflag; control mode flags */
    ntio.c_cflag &= ~HUPCL;
    ntio.c_cflag &= ~PARENB;		/* CLEAR Parity Bit PARENB*/
    ntio.c_cflag &= ~CSTOPB;		/* Stop bits = 1 */
    //ntio.c_cflag &= ~CRTSCTS;		/* Turn off hardware based flow control (RTS/CTS). */

    cfsetispeed(&ntio, cbaud);
    cfsetospeed(&ntio, cbaud);

    if( ( r = tcsetattr( devfd, TCSANOW, & ntio ) ) < 0 ){
        printf("open_port [%s] error tcsetattr %d %s\n",
            name,r, strerror( errno ) );
        return r;
    }

    if( ( r = tcflush( devfd, TCIOFLUSH ) ) < 0 ){
        printf("tcflush [%s] error  %d %s\n",
            name,r, strerror( errno ) );
        return r;
    }
    //=================================================
    return devfd;
}



