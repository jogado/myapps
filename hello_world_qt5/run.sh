#!/bin/sh


clean_up()
{
	killall X
	killall xterm
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 

killall X

#----------------------------------------------------------------
if [ -d "/usr/lib/fonts" ]
then
	echo "Fonts folder present"
else
	echo "Installing Fonts folder"
	ln -s /usr/share/fonts/ttf/ /usr/lib/fonts
fi
#----------------------------------------------------------------
echo 0 > /sys/class/graphics/fbcon/cursor_blink
#----------------------------------------------------------------




export DISPLAY=:0.0
X -nocursor &


if [ -d "/etc/vnc" ]
then
	echo "VNC Server present"
	x11vnc -rfbauth /etc/vnc/passfile &
else
	echo "VNC Server not present"
	# mkdir /etc/vnc
	# x11vnc -storepasswd emsyscon /etc/vnc/passfile
	# sleep 1
	# x11vnc -rfbauth /etc/vnc/passfile &
fi

xterm 3 "Touchscreen Test" "Please, press all butttons" 30 1 1

echo "Result:$?"





