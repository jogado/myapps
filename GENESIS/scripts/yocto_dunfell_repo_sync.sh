#!/bin/sh
#==============================================================================
# DUNFELL
#==============================================================================


#--------------------------------------------------------------------------------------------------
CONFIG_FILE="/home/ubuntu/GITS/myapps/GENESIS/scripts/genesis.inc"

if [ ! -f "$CONFIG_FILE" ]; then
    echo "Missing config file: <$CONFIG_FILE>"
    exit 1
fi

. $CONFIG_FILE     # Use '.' instead of 'source' for compatibility with sh/dash

#--------------------------------------------------------------------------------------------------



#--------------------------------------------------------------------------------------------------
if [ -z $PATH_YOCTO_DUNFELL ]
then
	echo "PATH_YOCTO_DUNFELL  : not defined [ "$PATH_YOCTO_DUNFELL" ]"
	exit 1
fi
echo "PATH_YOCTO_DUNFELL  : [$PATH_YOCTO_DUNFELL]"
#--------------------------------------------------------------------------------------------------




if [ -d "$PATH_YOCTO_DUNFELL" ]
then
	echo "Directory [ "$PATH_YOCTO_DUNFELL" ] exist"
	exit 1
fi

mkdir -p $PATH_YOCTO_DUNFELL

cd $PATH_YOCTO_DUNFELL

repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m dunfell.xml
repo sync


cp  $SCRIPT_FILES/yocto_dunfell_doit.sh  $PATH_YOCTO_DUNFELL/doit.sh






cd $PATH_YOCTO_DUNFELL

if [ ! -d $PATH_YOCTO_DUNFELL/build-imx6s ];then
    bash -c "source ./setup-env	build-imx6s  esc-imx6s-sodimm"
else
    echo "error forder exist : [ $PATH_YOCTO_DUNFELL/build-imx6s ]"
fi

if [ ! -d $PATH_YOCTO_DUNFELL/build-imx6ul ];then
    bash -c "source ./setup-env build-imx6ul esc-imx6ul"
else
    echo "error forder exist : [ $PATH_YOCTO_DUNFELL/build-imx6ul ]"
fi

if [ ! -d $PATH_YOCTO_DUNFELL/build-imx8mm ];then
    bash -c "source ./setup-env	build-imx8mm esc-imx8mm"
else
    echo "error forder exist : [ $PATH_YOCTO_DUNFELL/build-imx8mm ]"
fi








