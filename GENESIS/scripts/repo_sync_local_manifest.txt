#==============================================================================
# DUNFELL
#==============================================================================
THE_PATH="/home/ubuntu/yocto/manifest"

if [ -d $THE_PATH ]
then
	echo "Directory [ $THE_PATH ] exist"
	return
fi

mkdir -p $THE_PATH
cd $THE_PATH

repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m /home/ubuntu/tmp/yocto_local_dunfell/manifest.xml
repo sync

