#!/bin/bash

#==============================================================================
# SCARTHGAP
#==============================================================================


#--------------------------------------------------------------------------------------------------
CONFIG_FILE="/home/ubuntu/GITS/myapps/GENESIS/scripts/genesis.inc"

if [ ! -f "$CONFIG_FILE" ]; then
    echo "Missing config file: <$CONFIG_FILE>"
    exit 1
fi



. $CONFIG_FILE     # Use '.' instead of 'source' for compatibility with sh/dash
#--------------------------------------------------------------------------------------------------



if [ -z $PATH_YOCTO_SCARTHGAP ]
then
	echo "PATH_YOCTO_SCARTHGAP  : not defined [ "$PATH_YOCTO_SCARTHGAP" ]"
	exit 1
fi

echo "PATH_YOCTO_SCARTHGAP  : [$PATH_YOCTO_SCARTHGAP]"


if [ -d "$PATH_YOCTO_SCARTHGAP" ]
then
	echo "Directory [ "$PATH_YOCTO_SCARTHGAP" ] exist"
	exit 1
fi

mkdir -p $PATH_YOCTO_SCARTHGAP

cd $PATH_YOCTO_SCARTHGAP


repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m scarthgap.xml
repo sync


cp  $SCRIPT_FILES/yocto_scarthgap_doit.sh  $PATH_YOCTO_SCARTHGAP/doit.sh


cd $PATH_YOCTO_SCARTHGAP

if [ ! -d $PATH_YOCTO_SCARTHGAP/build-imx6s ];then
    bash -c "source ./setup-env	build-imx6s  esc-imx6s-sodimm"
else
    echo "error forder exist : [ $PATH_YOCTO_SCARTHGAP/build-imx6s ]"
fi

if [ ! -d $PATH_YOCTO_SCARTHGAP/build-imx6ul ];then
    bash -c "source ./setup-env build-imx6ul esc-imx6ul"
else
    echo "error forder exist : [ $PATH_YOCTO_SCARTHGAP/build-imx6ul ]"
fi

if [ ! -d $PATH_YOCTO_SCARTHGAP/build-imx8mm ];then
    bash -c "source ./setup-env	build-imx8mm esc-imx8mm"
else
    echo "error forder exist : [ $PATH_YOCTO_SCARTHGAP/build-imx8mm ]"
fi

if [ ! -d $PATH_YOCTO_SCARTHGAP/build-imx93 ];then
    bash -c "source ./setup-env	build-imx93 esc-imx93"
else
    echo "error forder exist : [ $PATH_YOCTO_SCARTHGAP/build-imx93 ]"
fi

source ./setup-env	build-imx93 esc-imx93
bitbake emsyscon-base-image

exit 0

