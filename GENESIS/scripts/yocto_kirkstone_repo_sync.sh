#!/bin/sh

#==============================================================================
# DUNFELL
#==============================================================================


#--------------------------------------------------------------------------------------------------
CONFIG_FILE="/home/ubuntu/GITS/myapps/GENESIS/scripts/genesis.inc"

if [ ! -f "$CONFIG_FILE" ]; then
    echo "Missing config file: <$CONFIG_FILE>"
    exit 1
fi

. $CONFIG_FILE     # Use '.' instead of 'source' for compatibility with sh/dash
#--------------------------------------------------------------------------------------------------


#--------------------------------------------------------------------------------------------------
if [ -z $PATH_YOCTO_KIRKSTONE ]
then
	echo "PATH_YOCTO_KIRKSTONE  : not defined [ "$PATH_YOCTO_KIRKSTONE" ]"
	exit
fi

echo "PATH_YOCTO_KIRKSTONE  : [$PATH_YOCTO_KIRKSTONE]"
#--------------------------------------------------------------------------------------------------



if [ -d "$PATH_YOCTO_KIRKSTONE" ]
then
	echo "Directory [ "$PATH_YOCTO_KIRKSTONE" ] exist"
	return
fi

mkdir -p $PATH_YOCTO_KIRKSTONE

cd $PATH_YOCTO_KIRKSTONE

repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m kirkstone.xml
repo sync


cp  $SCRIPT_FILES/yocto_kirkstone_doit.sh  $PATH_YOCTO_KIRKSTONE/doit.sh


cd $PATH_YOCTO_KIRKSTONE

if [ ! -d $PATH_YOCTO_KIRKSTONE/build-imx6s ];then
    bash -c "source ./setup-env	build-imx6s  esc-imx6s-sodimm"
else
    echo "error forder exist : [ $PATH_YOCTO_KIRKSTONE/build-imx6s ]"
fi

if [ ! -d $PATH_YOCTO_KIRKSTONE/build-imx6ul ];then
    bash -c "source ./setup-env build-imx6ul esc-imx6ul"
else
    echo "error forder exist : [ $PATH_YOCTO_KIRKSTONE/build-imx6ul ]"
fi

if [ ! -d $PATH_YOCTO_KIRKSTONE/build-imx8mm ];then
    bash -c "source ./setup-env	build-imx8mm esc-imx8mm"
else
    echo "error forder exist : [ $PATH_YOCTO_KIRKSTONE/build-imx8mm ]"
fi

if [ ! -d $PATH_YOCTO_KIRKSTONE/build-imx93 ];then
    bash -c "source ./setup-env	build-imx93 esc-imx93"
else
    echo "error forder exist : [ $PATH_YOCTO_KIRKSTONE/build-imx93 ]"
fi


exit 0


