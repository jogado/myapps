YOCTO_DIR="/home/ubuntu/yocto-scarthgap"


# Check if the Yocto directory already exists
if [ -d "$YOCTO_DIR" ]; then
    echo "Directory '$YOCTO_DIR' already exists. Exiting to avoid overwriting."
    exit 1
fi

# Create the Yocto directory
mkdir -p $YOCTO_DIR
cd $YOCTO_DIR


# Set variables
MANIFEST_REPO="git@git.emsyscon.com:os/yocto-manifests.git"
MANIFEST_FILE="scarthgap.xml"

# Initialize the repo
repo init -u $MANIFEST_REPO -m $MANIFEST_FILE

# Sync the repository
repo sync

