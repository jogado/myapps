#!/bin/bash



reserve()
{
	sudo apt-get install libsdl1.2-dev
	sudo apt-get install gnome-terminal

	sudo apt-get install libqtwebkit4
	sudo apt-get install qt4-dev-tools
	sudo apt-get install subversion

	sudo apt-get install texi2html



	cd ~/yocto/thud
	#-------------------------------------------------------------
	source ./setup-env build-imx6s  esc-imx6s-sodimm
	gedit  ./conf/local.conf
	Add	LICENSE_FLAGS_WHITELIST = "commercial"

	bitbake emsyscon-mg3000-demo
	#-------------------------------------------------------------
	source ./setup-env build-imx6ul esc-imx6ul

	#deja fait ??????
	#gedit  ./conf/local.conf
	#Add : LICENSE_FLAGS_WHITELIST = "commercial"

	bitbake emsyscon-abt3000-demo


}


install_001()
{
	echo "====================="
	echo "install_001"
	echo "====================="
	sudo apt install net-tools
	sudo apt install openssh-server
	sudo service ssh status
	sudo service ssh start
	sudo apt install chrome-gnome-shell gnome-shell-extension-prefs
}


install_Essentials()
{
	echo "====================="	
	echo "install_Essentials"
	echo "====================="	

	sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib
	sudo apt-get install build-essential chrpath socat cpio python3 python3-pip python3-pexpect
	sudo apt-get install xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa
	sudo apt-get install libsdl1.2-dev pylint3 xterm	

	sudo apt-get install g++-multilib
}

install_myutils()
{
	echo "====================="	
	echo "install_myutils"
	echo "====================="	
	sudo apt-get install lzop
	sudo apt-get install vim
	sudo apt-get install curl
	sudo apt-get install u-boot-tools
	sudo apt-get install apache2
	sudo apt-get install gitk
	sudo apt-get install libncurses5-dev
	sudo apt-get install tig
	sudo apt-get install python
}

install_qt5()
{
	echo "====================="	
	echo "install_qt5"
	echo "====================="	
	sudo apt install build-essential
	sudo apt install qtcreator
	sudo apt install qt5-default
	sudo apt install qt5-doc
	sudo apt install qt5-doc-html qtbase5-doc-html
	sudo apt install qtbase5-examples	
}






GIT_USERNAME="jose.garcia"
GIT_EMAIL="jose@emsyscon.com"
install_git()
{
	echo "====================="	
	echo "install_git"
	echo "====================="	

	sudo apt-get update
	sudo apt-get install git 
	git config --global user.name "jose.garcia"
 	git config --global user.email "jose@emsyscon.com" 
}




#================================================================================================
REPO_PATH="/home/ubuntu/bin/repo"
install_repo()
{
	sudo apt install repo

		OR

	echo "====================="	
	echo "install_repo"
	echo "====================="	

	mkdir -p ~/bin
	
	if [ -e $REPO_PATH ]
	then
		echo "File $REPO_PATH exist  !!"
	else
		echo "File $REPO_PATH do not exist"
		curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > $REPO_PATH
		chmod a+x $REPO_PATH
	fi
}
#================================================================================================
SSH_PATH="/home/ubuntu/.ssh/id_rsa.pub"
install_ssh()
{

	echo "====================="	
	echo "install_repo"
	echo "====================="	

	#mkdir -p ~/bin
	
	if [ -e $SSH_PATH ]
	then
		echo "File $SSH_PATH exist  !!"
	else
		echo "File $SSH_PATH do not exist"
		ssh-keygen
	fi
	cat $SSH_PATH
}
#================================================================================================
PATH_YOCTO_DUNFELL="/home/ubuntu/yocto-dunfell"
install_yocto_dunfell()
{
	if [ -d $PATH_YOCTO_DUNFELL ]
	then
		printf "Directory %-44.44s: Already exist\n\r" $PATH_YOCTO_DUNFELL
		return
	fi	

	mkdir -p $PATH_YOCTO_DUNFELL
	cd $PATH_YOCTO_DUNFELL
	repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m dunfell.xml
	repo sync
}
#================================================================================================
PATH_YOCTO_THUD="/home/ubuntu/yocto-thud"
install_yocto_thud()
{
	if [ -d $PATH_YOCTO_THUD ]
	then
		echo "Directory $PATH_YOCTO_THUD exist"
		return
	fi	

	mkdir -p $PATH_YOCTO_THUD
	cd $PATH_YOCTO_THUD
	repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m thud-20.0.3-x.xml
	repo sync
}
#================================================================================================



#================================================================================================
check_and_fix()
{
	THE_FILE=$1
	echo $THE_FILE

	if grep "LICENSE_FLAG" $THE_FILE
	then
		echo "Fix Already done ($THE_FILE)"
	else
		echo "Fix not done ($THE_FILE)"
		echo `grep -Fxq "LICENSE_FLAG" $THE_FILE`
		echo "LICENSE_FLAGS_WHITELIST=\"commercial\"" >> $THE_FILE
	fi
}
fix_commercial()
{
	check_and_fix "/home/ubuntu/yocto-dunfell/build-imx8mm/conf/local.conf"
	check_and_fix "/home/ubuntu/yocto-dunfell/build-imx6ul/conf/local.conf"
	check_and_fix "/home/ubuntu/yocto-dunfell/build-imx6s/conf/local.conf"


	check_and_fix "/home/ubuntu/yocto-kirkstone/build-imx8mm/conf/local.conf"
	check_and_fix "/home/ubuntu/yocto-kirkstone/build-imx6ul/conf/local.conf"
	check_and_fix "/home/ubuntu/yocto-kirkstone/build-imx6s/conf/local.conf"
}

}
#================================================================================================
PATH_EMSYSCON_KIRKSTONE="/home/ubuntu/GITS/meta-emsyscon-kirkstone"
clone_meta_emsyscon_kirkstone()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_EMSYSCON_KIRKSTONE ]
	then
		printf "Directory %-44.44s: Already exist\n\r" $PATH_EMSYSCON_KIRKSTONE
		return
	fi

	git clone git@git.emsyscon.com:os/meta-emsyscon.git -b kirkstone $PATH_EMSYSCON_KIRKSTONE

	cd $PATH_EMSYSCON_KIRKSTONE
	git branch -a
	git fetch --all
	git fetch --tags
	git log -1 |grep commit
}
#================================================================================================
PATH_EMSYSCON_DUNFELL="/home/ubuntu/GITS/meta-emsyscon-dunfell"
clone_meta_emsyscon_dunfell()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_EMSYSCON_DUNFELL ]
	then
		printf "Directory %-44.44s: Already exist\n\r" $PATH_EMSYSCON_DUNFELL
		return
	fi


	git clone git@git.emsyscon.com:os/meta-emsyscon.git -b dunfell $PATH_EMSYSCON_DUNFELL

	cd $PATH_EMSYSCON_DUNFELL
	git branch -a
	git fetch --all
	git fetch --tags
}

#================================================================================================
PATH_EMSYSCON_THUD="/home/ubuntu/GITS/meta-emsyscon-thud"
clone_meta_emsyscon_thud()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_EMSYSCON_THUD ]
	then
		printf "Directory %-44.44s: Already exist\n\r" $PATH_EMSYSCON_THUD
		return
	fi	

	git clone git@git.emsyscon.com:os/meta-emsyscon.git -b thud-20.0.3-x $PATH_EMSYSCON_THUD

	cd $PATH_EMSYSCON_THUD
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================

#================================================================================================
PATH_EMSYSCON_TOOLS="/home/ubuntu/GITS/emsyscon-tools"
clone_emsyscon_tools()
{

	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_EMSYSCON_TOOLS ]
	then
		echo "Directory $PATH_EMSYSCON_TOOLS            : Already exist"
		cd $PATH_EMSYSCON_TOOLS
		return
	fi	
		
	echo "=========================="	
	echo "clone_emsyscon_tools"
	echo "=========================="	
	
	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/emsyscon-tools.git $PATH_EMSYSCON_TOOLS
	
	cd $PATH_EMSYSCON_TOOLS
	git branch -a
	git fetch --all
	git fetch --tags
	git log -1 |grep commit
}
#================================================================================================






#================================================================================================
PATH_LINUX_IMX6S="/home/ubuntu/GITS/linux-imx6s-emsyscon-4.9"
clone_linux-imx6s-emsyscon-4.9()
{
	mkdir -p /home/ubuntu/GITS


	if [ -d $PATH_LINUX_IMX6S ]
	then
		echo "Directory $PATH_LINUX_IMX6S  : Already exist"
		cd $PATH_LINUX_IMX6S
		return
	fi
	

	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x $PATH_LINUX_IMX6S
	
	cd $PATH_LINUX_IMX6S

	git branch -a
	git fetch --all
	git fetch --tags
	git log -1 |grep commit
}
#================================================================================================





#================================================================================================
PATH_LINUX_IMX6UL="/home/ubuntu/GITS/linux-imx6ul-emsyscon-4.9"
clone_linux-imx6ul-emsyscon-4.9()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_LINUX_IMX6UL ]
	then
		echo "Directory $PATH_LINUX_IMX6UL : Already exist"
		cd $PATH_LINUX_IMX6UL
		return
	fi
	
	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x $PATH_LINUX_IMX6UL
	
	cd $PATH_LINUX_IMX6UL
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================



#================================================================================================
PATH_LINUX_IMX8MM="/home/ubuntu/GITS/linux-imx8mm-4.14"
clone_linux-imx8mm-4.14()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_LINUX_IMX8MM ]
	then
		echo "Directory $PATH_LINUX_IMX8MM         : Already exist"
		return
	fi
	
	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/linux.git -b imx8mm-esc-tn $PATH_LINUX_IMX8MM
	
	cd $PATH_LINUX_IMX8MM
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================

#================================================================================================
PATH_LINUX_IMX8MM_5_13="/home/ubuntu/GITS/linux-imx8mm-5.13"
clone_linux-imx8mm-5.13()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_LINUX_IMX8MM ]
	then
		echo "Directory $PATH_LINUX_IMX8MM    : Already exist"
		cd $PATH_LINUX_IMX8MM
		echo "    ==> `git log -1 |grep commit`"
		return
	fi
	
	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/linux.git -b imx8mm-emsyscon-master $PATH_LINUX_IMX8MM_5_13
	
	cd $PATH_LINUX_IMX8MM
	git branch -a
	git fetch --all
	git fetch --tags
	git log -1 |grep commit
}
#================================================================================================










#================================================================================================
PATH_UBOOT_IMX6S="/home/ubuntu/GITS/uboot-imx6s"
clone_uboot-imx6s()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_UBOOT_IMX6S ]
	then
		echo "Directory $PATH_UBOOT_IMX6S               : Already exist"
		cd $PATH_UBOOT_IMX6S
		return
	fi

	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/u-boot.git -b imx-esc $PATH_UBOOT_IMX6S

	cd $PATH_UBOOT_IMX6S
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================
PATH_UBOOT_IMX6UL="/home/ubuntu/GITS/uboot-imx6ul"
clone_uboot-imx6ul()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_UBOOT_IMX6UL ]
	then
		echo "Directory $PATH_UBOOT_IMX6UL              : Already exist"
		cd $PATH_UBOOT_IMX6S
		return
	fi

	cd /home/ubuntu/GITS
	git clone git@git.emsyscon.com:os/u-boot.git -b imx-esc $PATH_UBOOT_IMX6UL

	cd $PATH_UBOOT_IMX6UL
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================
PATH_UBOOT_IMX8MM="/home/ubuntu/GITS/uboot-imx8mm"
clone_uboot-imx8mm()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_UBOOT_IMX8MM ]
	then
		echo "Directory $PATH_UBOOT_IMX8MM              : Already exist"
		cd $PATH_UBOOT_IMX6S
		return
	fi

	cd /home/ubuntu/GITS
#	git clone git@git.emsyscon.com:os/u-boot.git -b imx8mm-esc-tn $PATH_UBOOT_IMX8MM
	git clone git@git.emsyscon.com:os/u-boot.git -b esc-imx8mm $PATH_UBOOT_IMX8MM

	cd $PATH_UBOOT_IMX8MM
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================
PATH_MYAPPS="/home/ubuntu/GITS/myapps"
clone_myapps()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_MYAPPS ]
	then
		echo "Directory $PATH_MYAPPS                    : Already exist"
		cd $PATH_MYAPPS
		return
	fi

	cd /home/ubuntu/GITS
	git clone git@gitlab.com:jogado/myapps.git $PATH_MYAPPS

	cd $PATH_MYAPPS
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================
PATH_QT5_APPS="/home/ubuntu/GITS/qt5_apps"
clone_qt5_apps()
{
	mkdir -p /home/ubuntu/GITS

	if [ -d $PATH_QT5_APPS ]
	then
		echo "Directory $PATH_QT5_APPS                  : Already exist"
		cd $PATH_QT5_APPS
		return
	fi

	cd /home/ubuntu/GITS
	git clone git@gitlab.com:jogado/qt5_apps.git $PATH_QT5_APPS

	cd $PATH_QT5_APPS
	git branch -a
	git fetch --all
	git fetch --tags
}
#================================================================================================

fix_bb()
{
	
	THE_ALIAS="alias bb="
	THE_FILE="/home/ubuntu/.bashrc"
	echo $THE_FILE

	
	if grep $THE_ALIAS $THE_FILE
	then
		echo "Alias Already done (`grep $THE_ALIAS $THE_FILE)`"
	else
		echo "Alias not found ($THE_FILE)"
		echo "alias bb='bitbake'" >> $THE_FILE
	fi

}


install_sdk()
{
	echo "Installing SDK's"
	sudo ~/Downloads/poky-emsyscon-glibc-x86_64-emsyscon-qt5-sdk-aarch64-esc-imx8mm-toolchain-3.1.3.sh
	sudo ~/Downloads/poky-emsyscon-glibc-x86_64-emsyscon-qt5-sdk-cortexa7t2hf-neon-esc-imx6ul-toolchain-3.1.3.sh
	sudo ~/Downloads/poky-emsyscon-glibc-x86_64-emsyscon-qt5-sdk-cortexa9t2hf-neon-esc-imx6s-sodimm-toolchain-3.1.3.sh
}












PATH_YOCTO_KIRKSTONE="/home/ubuntu/yocto-kirkstone"

install_yocto_kirkstone()
{
	if [ -d $PATH_YOCTO_KIRKSTONE ]
	then
		printf "Directory %-44.44s: Already exist\n\r" $PATH_YOCTO_KIRKSTONE
		return
	fi	

	echo "=========================="	
	echo "install_yocto_kirkstone"
	echo "=========================="	

	mkdir -p $PATH_YOCTO_KIRKSTONE
	cd $PATH_YOCTO_KIRKSTONE
	
	
	repo init -u git@git.emsyscon.com:os/yocto-manifests.git -m kirkstone.xml
	repo sync
	sudo apt-get install zstd

	#yocto-kirkstone/scripts/conf/local.conf.esc-imx6s-sodimm:
	#MACHINE ??= "esc-imx6-sodimm"  => MACHINE ??= "esc-imx6s-sodimm"	
}





do_full_populate()
{
	# install_001
	# install_Essentials
	# install_myutils
	# install_qt5
	# install_git
	# install_repo
	# install_ssh

	install_yocto_kirkstone
	#install_yocto_dunfell
	#install_yocto_thud

	# fix_commercial
	# fix_bb

	clone_meta_emsyscon_kirkstone
	clone_meta_emsyscon_dunfell
	clone_meta_emsyscon_thud
    
	clone_emsyscon_tools
	clone_linux-imx6s-emsyscon-4.9
	clone_linux-imx6ul-emsyscon-4.9
	clone_linux-imx8mm-4.14
	clone_uboot-imx6s
	clone_uboot-imx6ul
	clone_uboot-imx8mm
	clone_myapps
	clone_qt5_apps
	install_sdk
}

do_ubuntu_22_4_2()
{
    	# install_001
	# install_Essentials
	# install_myutils
	# install_qt5
	
	# Better manual
	#install_git
	#install_repo !!!! 
	#install_ssh
	
	install_yocto_dunfell
	install_yocto_kirkstone


	clone_meta_emsyscon_kirkstone
	clone_meta_emsyscon_dunfell
	clone_meta_emsyscon_thud

	
	clone_emsyscon_tools
	clone_linux-imx6s-emsyscon-4.9
	clone_linux-imx6ul-emsyscon-4.9
	clone_linux-imx8mm-4.14

	clone_uboot-imx6s
	clone_uboot-imx6ul
	clone_uboot-imx8mm
	clone_myapps
	clone_qt5_apps
	
	# Better manual
	# install_sdk
	# fix_commercial
	# fix_bb

}

do_ubuntu_22_4_2





