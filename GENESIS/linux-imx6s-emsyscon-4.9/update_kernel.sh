#!/bin/sh




if [ -f ./modules.tar.gz ]
then
    echo "modules present"
    tar -xzf ./modules.tar.gz -C /tar -xzf ./modules.tar.gz -C /lib/modules/
else
    echo "modules not present" 
fi

echo "Creating image backup : /boot/uImage.backup"

cp /boot/uImage /boot/uImage.backup
cp ./uImage  /boot
cp ./*.dtb   /boot/dtb

sync

echo "========================"
echo "Rebooting .......       "
echo "========================"
beep
sleep 1
reboot



