#!/bin/sh
PATH_LINUX_IMX6S_1="/home/ubuntu/GITS/linux-imx6s-emsyscon-4.9"
PATH_LINUX_IMX6S_2="/home/ubuntu/GITS/linux-imx6s-emsyscon-4.9-w1-i2c"

clone()
{
	git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x $PATH_LINUX_IMX6S_1
	git clone git@git.emsyscon.com:os/linux.git -b w1-i2c "/home/ubuntu/GITS/linux-imx6s-emsyscon-4.9-w1-i2c"
}



make_module()
{
    #
	# Make a temp directory for the modules
	#
	rm -rf   modules_temp
	mkdir -p modules_temp
	make modules_install INSTALL_MOD_PATH=./modules_temp

	#
	#Pack up the modules into a nice .tar.gz file:
	#
	cd modules_temp/lib/modules
	rm modules.tar.gz
	tar czvf modules.tar.gz *
	cd ../../..
	ls -la modules_temp/lib/modules/modules.tar.gz
}




if  echo $3 | grep -q "nobuild"; then

	echo "********************"
	echo "***   No build   ***"
	echo "********************"

else
	ARG1=$1
	if echo $1 | grep -q "imx6s"; then
		export ARCH=arm
		export CROSS_COMPILE=arm-poky-linux-gnueabi-
		. /opt/poky-emsyscon/3.1.3/cortexa9t2hf/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi

		make imx_v6_v7_emsyscon_defconfig
		make -j4
        if [ $? -eq 0 ]
        then
            echo "Make success"
        else
            echo "failure :("
            exit
        fi
		make uImage LOADADDR=0x10800000

        make_module

		ls -la arch/arm/boot/uImage
		ls -la arch/arm/boot/dts/imx6dl-esc-*.dtb

	elif  echo $1 | grep -q "imx6ul"; then
		export ARCH=arm
		export CROSS_COMPILE=arm-poky-linux-gnueabi-
		. /opt/poky-emsyscon/3.1.3/cortexa7t2hf/environment-setup-cortexa7t2hf-neon-poky-linux-gnueabi

		make imx_v6_v7_emsyscon_defconfig
		make

		make uImage LOADADDR=0x80800000

		make_module

		ls -la arch/arm/boot/uImage
		ls -la arch/arm/boot/dts/imx6ul-esc-*.dtb

	elif  echo $1 | grep -q "imx8mm"; then
		export ARCH=arm64
		export CROSS_COMPILE=aarch64-linux-gnu-
		. /opt/poky-emsyscon/3.1.3/aarch64/environment-setup-aarch64-poky-linux
		unset LDFLAGS
		unset CFLAGS

		make imx8mm_emsyscon_defconfig
		make

        if [ $? -eq 0 ]
        then
            echo "Make success"
        else
            echo "failure :("
            exit
        fi

		make_module

		ls -la arch/arm64/boot/Image
		ls -la arch/arm64/boot/dts/freescale/imx8mm-esc-*.dtb

	else
		echo "syntax doit [ imx6s | imx6ul |imx8mm ] < 192.168.0.xxx > <nobuild> <boot>"
		exit
	fi

fi
#----------------------------------------------
# Transfer new kernel if target is defined
#----------------------------------------------
ARG2=$2
if echo $2 | grep -q "192.168"; then

	ssh root@$2 "rm /apps/update_kernel/*"
	ssh root@$2 "mkdir -p /apps/update_kernel"


	if echo $1 | grep -q "imx6s"; then
		ssh root@$2 "rm -rf /apps/update_kernel"
		ssh root@$2 "mkdir -p /apps/update_kernel"
		scp arch/arm/boot/uImage  		 	            root@$2:/apps/update_kernel/
	    scp arch/arm/boot/dts/imx6dl-esc-*.dtb   	    root@$2:/apps/update_kernel/
		scp update_kernel.sh	                        root@$2:/apps/update_kernel/
		scp modules_temp/lib/modules/modules.tar.gz		root@$2:/apps/update_kernel/

	elif  echo $1 | grep -q "imx6ul";then
		ssh root@$2 "rm -rf /apps/update_kernel"
		ssh root@$2 "mkdir -p /apps/update_kernel"
		scp arch/arm/boot/uImage  			            root@$2:/apps/update_kernel/
		scp arch/arm/boot/dts/imx6ul-esc-yes*.dtb	    root@$2:/apps/update_kernel/
		scp update_ kernel.sh	                        root@$2:/apps/update_kernel/
		scp modules_temp/lib/modules/modules.tar.gz		root@$2:/apps/update_kernel/

	elif  echo $1 | grep -q "imx8mm";then
		scp arch/arm64/boot/Image  				            root@$2:/apps/update_kernel/
		scp arch/arm64/boot/dts/freescale/imx8mm-esc-*.dtb	root@$2:/apps/update_kernel/
		scp update_kernel.sh					            root@$2:/apps/update_kernel/
		#scp inittab						                root@$2:/apps/update_kernel/
		scp modules_temp/lib/modules/modules.tar.gz		    root@$2:/apps/update_kernel/
		#tar -xzf modules.tar.gz -C /
	fi


	if  echo $3 | grep -q "boot"; then
		ssh root@$2 "cd /apps/update_kernel;sync;./update_kernel.sh"
	fi

	if  echo $4 | grep -q "boot"; then
		ssh root@$2 "cd /apps/update_kernel;sync;./update_kernel.sh"
	fi

fi

