#!/bin/bash

sync
echo "=== Transfer file          ==="
echo "=== Done                   ==="



if [  ! -f /mnt/p1 ]
then
	mkdir -p /mnt/p1
fi




echo "mount mmcblk2p1"
mount /dev/mmcblk2p1 /mnt/p1/
sleep 1

echo "copy Image"
cp Image /mnt/p1/
sleep 1

echo "copy dtb"
cp /apps/update_kernel/*.dtb /mnt/p1/

echo "Files in /mnt/p1/"
ls -la /mnt/p1
sleep 5

echo "sync"
sync
echo "umount p1"
umount /mnt/p1/


echo "=== rebooting              ==="
sleep 3
reboot 

