#!/bin/sh


#======================================================================================================================
build_uboot_imx8mm_esc_tn()
{
	export ARCH=arm64
	export CROSS_COMPILE=aarch64-linux-gnu-

		. /opt/poky-emsyscon/3.1.3/aarch64/environment-setup-aarch64-poky-linux

	#
	# 'unrecognized option '-Wl,-O1'
	#
	unset LDFLAGS
	unset CFLAGS

	make pico-imx8mm_defconfig

   	make -j4
   	rm -f flash.bin && ./install_uboot_imx8.sh -b imx8mm-pico-pi -d flash.bin
   	dd if=flash.bin of=flash2.bin bs=1 skip=$((0x8400))

	#
    	# At the end you obtain the flash2.bin which is the first file to provide to "uuu".
	#

}
#======================================================================================================================
build_uboot_esc_imx8mm()
{
	export ARCH=arm64
	export CROSS_COMPILE=aarch64-linux-gnu-

	. /opt/poky-emsyscon/3.1.3/aarch64/environment-setup-aarch64-poky-linux

	#
	# 'unrecognized option '-Wl,-O1'
	#
	#unset LDFLAGS
	#unset CFLAGS

	make imx8mm_esc_abt3000_defconfig

	rm bl31.bin
	rm lpddr4_pmu_train_1d_dmem.bin
	rm lpddr4_pmu_train_1d_imem.bin
	rm lpddr4_pmu_train_2d_dmem.bin
	rm lpddr4_pmu_train_2d_imem.bin
	sync

	cp /home/ubuntu/yocto-dunfell/build-imx8mm/tmp/deploy/images/esc-imx8mm/imx-atf/bl31-imx8mm.bin 	bl31.bin

	cp /home/ubuntu/yocto-dunfell/build-imx8mm/tmp/deploy/images/esc-imx8mm/imx-firmware//lpddr4_pmu_train_1d_dmem.bin .
	cp /home/ubuntu/yocto-dunfell/build-imx8mm/tmp/deploy/images/esc-imx8mm/imx-firmware//lpddr4_pmu_train_1d_imem.bin .
	cp /home/ubuntu/yocto-dunfell/build-imx8mm/tmp/deploy/images/esc-imx8mm/imx-firmware//lpddr4_pmu_train_2d_dmem.bin .
	cp /home/ubuntu/yocto-dunfell/build-imx8mm/tmp/deploy/images/esc-imx8mm/imx-firmware//lpddr4_pmu_train_2d_imem.bin .


	export ATF_LOAD_ADDR=0x920000

	make flash.bin

	ls -la flash.bin

}
#======================================================================================================================ni


#build_uboot_imx8mm_esc_tn
build_uboot_esc_imx8mm

#----------------------------------------------
# Transfer new kernel if target is defined
#----------------------------------------------
ARG2=$2
if echo $2 | grep -q "192.168"; then

	ssh root@$2   "rm -rf /apps/update_uboot/*"
	ssh root@$2   "mkdir -p /apps/update_uboot"
	scp flash.bin 		root@$2:/apps/update_uboot/flash.bin.new

	#scp flash2.bin 	root@$2:/apps/update_uboot/
	#scp flash2.bin 	root@$2:/apps/update_uboot/flash.bin
fi


if  echo $3 | grep -q "boot"; then
	ssh root@$2 "cp /apps/update_uboot/flash.bin.new /boot/"
	ssh root@$2 "ln -sf /boot/flash.bin.new /boot/flash.bin"

	ssh root@$2 "sync"
	ssh root@$2 "u-boot-install"
	ssh root@$2 "beep;beep;beep"
	ssh root@$2 "reboot"
fi

