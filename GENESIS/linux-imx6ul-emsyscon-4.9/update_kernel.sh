#!/bin/sh


echo "copy Image"
cp ./uImage  /boot

echo "copy dtbs"
cp ./*.dtb   /boot/dtb


echo "copy modules"
tar -xzf ./modules.tar.gz -C /


sync

beep
echo "========================"
echo "Rebooting .......       "
echo "========================"
sleep 3
sync
reboot



