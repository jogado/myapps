#!/bin/sh








export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-



# Export the Cross Compiler and tool chain path
#----------------------------------------------
ARG1=$1
if echo $1 | grep -q "imx6s"; then
echo "testA"
	/opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
echo "testB"
	make emsyscon_imx6s_defconfig
	make 

elif  echo $1 | grep -q "imx6ul"; then

	. /opt/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi 
	make emsyscon_imx6ul_1G_defconfig
	make 

else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi





DD=$(date +'%Y%m%d%H%M%S')

rm u-boot-*
cp u-boot.imx u-boot-$DD.imx
ls -la u-boot-*
ls -la u-boot.imx

#----------------------------------------------
# Transfer new kernel if target is defined
#----------------------------------------------

if echo $2 | grep -q "192.168"; then
	ssh root@$2 "rm       /apps/update_u_boot/*"	
	ssh root@$2 "mkdir -p /apps/update_u_boot"
	scp u-boot.imx		root@$2:/apps/update_u_boot/
	scp update_uboot.sh	root@$2:/apps/update_u_boot/
fi







#================================================================================================
# echo 0 > /sys/block/mmcblk1boot0/force_ro
# dd if=/boot/u-boot.imx of=/dev/mmcblk1boot0 bs=512 seek=2
# echo 1 > /sys/block/mmcblk1boot0/force_ro
#================================================================================================
# mmc bootpart enable 1 1 /dev/mmcblk1
#================================================================================================




