#include <stdio.h>
#include <stdlib.h>




#include <linux/signal.h>
#include <linux/interrupt.h>
#include <linux/sched.h>

// Define the IRQ number to use
#define IRQ_NUM 112

// The IRQ handler function
irqreturn_t irq_handler(int irq, void *dev_id)
{
    printk(KERN_INFO "HELLO\n");
    return IRQ_HANDLED;
}

// The module initialization function
int main()
{
    int result;

    // Register the IRQ handler function with the kernel
    result = request_irq( IRQ_NUM, irq_handler, IRQF_SHARED, "my_irq_handler", &irq_handler);
    if (result)
    {
        printf("Error registering IRQ handler: %d\n", result);
        return result;
    }
    print("IRQ handler registered.\n");



    while(1)
    {
        usleep (10000);

    }

    free_irq(IRQ_NUM, &irq_handler);
    print("IRQ handler freed.\n");

}

