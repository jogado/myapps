#include <stdio.h>
#include <stdlib.h>


extern const char *ErrorCodeStr(int code);


const char *ErrorCodeStr(int code)
{
	const char *p;

	switch(code)
	{
		case 0x01 : p = "Parameter is not supported" 						; break ;
		case 0x02 : p = "Requested parameter is out of scope" 					; break ; 
		case 0x03 : p = "Unable to write to the PCD Nonvolatile memory" 			; break ; 
		case 0x04 : p = "Unable to read from the PCD Nonvolatile memory" 			; break ; 
		case 0x05 : p = "RF failure (without PCD Error Recovery Sequence)" 			; break ; 
		case 0x06 : p = "Collision detected - More than one PICC in the RF Field" 		; break ; 
		case 0x07 : p = "RF failure after PCD Error Recovery Sequence" 				; break ;  
		case 0x08 : p = "Transparent Buffer Length overflow" 					; break ; 
		case 0x09 : p = "EMV Transaction Terminated" 						; break ; 
		case 0x0A : p = "PICC Data length overflow (Greater than 512 Bytes) " 			; break ;
		case 0x0B : p = "Hercules Error Code" 							; break ; 
		case 0x0C : p = "SAM/Contact general failure" 						; break ; 
		case 0x0D : p = "SAM not present" 							; break ; 
		case 0x0E : p = "SAM not active" 							; break ; 
		case 0x0F : p = "SAM Protocol error (Only in PCD control) " 				; break ;
		case 0x10 : p = "Missing Parameter" 							; break ; 
		case 0x11 : p = "Application Disabled" 							; break ; 
		case 0x12 : p = "PCD Serial No. not suitable (For Password mechanism) " 		; break ;
		case 0x13 : p = "Index not found (for Indexed Objects)." 				; break ; 
		case 0x14 : p = "PKI Storage Error. " 							; break ;
		case 0x15 : p = "Hash doesn't match for a public key " 					; break ;
		case 0x16 : p = "AID length error (index field)" 					; break ; 
		case 0x17 : p = "Data field length error (within indexed objects) " 			; break ;
		case 0x18 : p = "Contactless Transaction Terminated" 					; break ; 
		case 0x19 : p = "Setup Error: The PCD failed during setup. When this failure happens" 	; break ;
		case 0x1A : p = "Password not set correctly" 						; break ; 
		case 0x1B : p = "CID Error (For Password mechanism)" 					; break ; 
		case 0x1C : p = "FW Version not suitable (For Password mechanism) " 			; break ;
		case 0x1D : p = "Sub Scheme not Applicable" 						; break ; 
		case 0x1E : p = "PCD is BUSY" 								; break ; 
		case 0x1F : p = "Poll EMV Timeout ended " 						; break ;
		case 0x20 : p = "Terminal Not Authorized" 						; break ; 
		default:    p = "Unknown code" 								; break ;
	}
	return(p);
}
