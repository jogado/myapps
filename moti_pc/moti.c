#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>


#include <messages.h>

#define MOTI_DATA_PRODTEST		"/tmp/moti.tmp"
#define MOTI_ERROR_LOG_PATH		"/tmp/moti.error"
#define MOTI_DEFAULT_DATA_PATH		"/tmp/moti.data"
char MOTI_DATA_PATH	[256];
char aError		[256];


extern void treat_df(unsigned char *p);
extern void treat_ff(unsigned char *p);
extern void treat_fc(unsigned char *p);
extern void treat_84(unsigned char *p);

extern void DF_68 (unsigned char *p);
extern void DF_30 (unsigned char *p);
extern void DF_04 (unsigned char *p);
extern void DF_19 (unsigned char *p);
extern void DF_06 (unsigned char *p);
extern void DF_4D (unsigned char *p);
extern void DF_79 (unsigned char *p);
extern void DF_7A (unsigned char *p);
extern void DF_13 (unsigned char *p);
extern void DF_16 (unsigned char *p);
extern void DF_14 (unsigned char *p);
extern void DF_0D (unsigned char *p);
extern void DF_6B (unsigned char *p);
extern void DF_4E (unsigned char *p);
extern void DF_DE (unsigned char *p);
extern void DF_0E (unsigned char *p);
extern void DF_7E (unsigned char *p);
extern void DF_7D (unsigned char *p);
extern void DF_XX (unsigned char *p);


typedef enum {
   HUNT_STX = 1   ,
   HUNT_LEN       ,
   HUNT_UNIT      ,
   HUNT_OPCODE    ,
   HUNT_DATA      ,
   HUNT_LRC       ,
   HUNT_ETX       ,
} HUNT_STATES;


typedef struct
{
   unsigned char START_CHR;
   unsigned char REC_LEN;
   unsigned char UNIT;
   unsigned char OPCODE;
   unsigned char DATA[128];
   unsigned char LRC;
   unsigned char END_CHR;
}
OTI_REC;

OTI_REC ri;
OTI_REC r_old;

int PICC_type = 0;
//==============================================================================
typedef struct
{
	int Type;
	int Id;
	int Id2;
	int Len;
	int Val;
	unsigned char *data;
}
HEAD_REC;

HEAD_REC h;



#define STX 0x02
#define ETX 0x03

#define EMV300_SET	0x3c
#define EMV300_GET	0x3d
#define EMV300_DO	0x3e


#define ms_1 	1000
#define ms_100 	100000
#define ms_200 	200000
#define ms_500 	500000
#define ms_50  	50000




extern void store_record	(OTI_REC *r,OTI_REC *r_old);
extern int same_record		(OTI_REC *r,OTI_REC *r_old);
extern void clear_record	(OTI_REC *rec);


extern int  main 		( int argc, char **argv ) ;
extern void Decode_Head		(unsigned char *p);
extern void Decode_Head_3	(unsigned char *p);
extern void ShowData		(HEAD_REC *h);
extern void ProdTestData_file	(char * s);
extern int  usb_reset		(int bus, int device);
extern int  reset_emv3000	(void);
extern void RxLoop		(void);
extern void treat_data		(OTI_REC *r);
extern void SendCommand		(unsigned char *s);

extern void errorfile		(char * s);
extern void system_call 	(const char *s);
extern int  file_exists		(const char *fname);
extern void set_device_name	(void);

extern const char *ErrorCodeStr	(int code);
extern char *mydatetime		( void );


char devname[128] ;
int  devfd;



int PollingRequested=1;
int PollPending = 0;
int Ack_Received=0;
int Poll_stop_requested=0;
int link_requested = 0;




int CARD_M=0;
int CARD_B=0;
int CARD_F=0;
int CARD_X=0;
int DoBeep=0;
int Debug=0;
int DebugTXRX=0;


int OneRead = 0;
int AllowDuplicates = 0;
int ExitOneRead=0;

int ERROR_COUNTS=0;
int nothing=0;
int Done = 0;
int Toggle=0;

extern char 	*Build_dateTime				( void );
extern void 	show_help				( void );


//============================================================================================
void store_record(OTI_REC *r,OTI_REC *r_old)
{
	memcpy(r_old,r,sizeof(OTI_REC));
}
//============================================================================================
int same_record(OTI_REC *r,OTI_REC *r_old)
{
	if(OneRead) return 1;
	return( memcmp(r,r_old,sizeof(OTI_REC)) ) ;
}
//============================================================================================
void clear_record(OTI_REC *rec)
{
	 memset(rec,0,sizeof(OTI_REC));
}
//============================================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}
//============================================================================================





//============================================================================================
void treat_data(OTI_REC *r)
{
	int len = r->REC_LEN -6;
	unsigned char *p = &r->DATA[0] ;
	int r_type;
	int r_len;
	



	if(Debug) printf("      Treat_data(%d)\n\r", len);


	if( (r->REC_LEN==8) && (r->DATA[0]==0x00) && (r->DATA[1]==0x00) )
	{
		if(Debug) printf("	PDC ACK\n\r");
		Ack_Received=1;
		return;
	}


	if( (r->REC_LEN==7) && (r->OPCODE==0x15) )
	{
		printf("	PDC NACK (error: 0x%02X)\n\r",r->DATA[0]);
		PollingRequested =1;
		PollPending     = 0 ;
		return;
	}
	

	if(r->DATA[7] == 0xDF && r->DATA[8] != 0x4E )
	{
		//link check
	}
	else
	{
		//nothing = 0;
		// Due to new card info : DF_DE_47  record is not always identical, so the test is less precise
		Done    = 0;
		if( (same_record(r,&r_old)==0)  && (AllowDuplicates == 0 ) )
		{
			if(Debug) printf("	Same record, data discarted\n\r");
			PollingRequested = 1;
			PollPending      = 0;
			return;
		}
	}


	while (len > 0)
	{
		r_type= *(p+0);
//		r_id  = *(p+1);
		r_len = *(p+2);

	
		switch(r_type)
		{
			case 0xDF :
					treat_df ( p );
					p   += r_len +3 ;
					len -= r_len +3 ;
					continue;
			case 0xFF :
					treat_ff ( p );
					p+=3 ;
					len -= 3;	// ADDED by LUC !!!!! but it still blocks :-(
					continue;
			case 0xFC :
					treat_fc ( p );
					p+=2 ;
					len -= 2;	// ADDED by LUC !!!!! but it still blocks :-(
					continue;

			case 0x84 :
					treat_84 ( p );
					p   += r_len +2 ;
					len -= r_len +2 ;
					continue;

			default:
					printf("Unknown type: %X\r\n", r_type);
		}
		p++;
		len--;
	}


}
//============================================================================================





unsigned char Buffer[256];
int Buffer_len=0;
int Buffer_offset=0;

void RxLoop(void)
{
	static unsigned char    State = HUNT_STX;
	static int		Offset;
	static unsigned char	LRC;
	OTI_REC			*r=&ri;
	int 			len;
	int 			TheChar;


	while (1)
	{
		if(Buffer_len)
		{
			TheChar = (int)Buffer[Buffer_offset++];
			if(Buffer_offset >= Buffer_len )
			{
				Buffer_len    = 0;
				Buffer_offset = 0;
				//usleep(100000); // System give hand, could help ?
			}
		}
		else
		{
			len = read(devfd,&Buffer,200);
			if(len < 0)
			{
				printf("read error : (%d) %s\n\r", len , strerror(errno));
				return;
			}
			if(len==0)
			{
				return;
			}
			Buffer_len    = len;
			Buffer_offset = 0;
			continue;
		}

		//nothing=0;
		TheChar = TheChar & 0x00ff;

		switch (State)
		{
			case HUNT_STX:
				LRC = 0;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LEN:
			case HUNT_UNIT:
			case HUNT_OPCODE:
			case HUNT_DATA:
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LRC:
			case HUNT_ETX:
				break;
			default:
				break;
		}

		if( State == HUNT_LEN )
		{
			if(Debug||DebugTXRX) printf("    RX [%03d]: 02",TheChar);
		}

		if( State != HUNT_STX )
		{
			if(Debug||DebugTXRX) printf(" %02X",TheChar);
		}

		if( State == HUNT_ETX && TheChar == ETX )
		{
			if(Debug||DebugTXRX) printf("\n\r");
		}

		switch (State)
		{
			case HUNT_STX:
				if( TheChar == STX )
				{
					memset(r,0x00,sizeof(OTI_REC));
					Offset = 0;
					State  = HUNT_LEN;
					r->START_CHR = TheChar;
				}
				break;
			case HUNT_LEN:
				r->REC_LEN =  TheChar ;
				State = HUNT_UNIT;
				break;
			case HUNT_UNIT:
				r->UNIT = TheChar ;
				State = HUNT_OPCODE;
				break;
			case HUNT_OPCODE:
				r->OPCODE =  TheChar ;
				State = HUNT_DATA;
				break;
			case HUNT_DATA:
	
				if(Offset >100)
				{
					State=HUNT_STX;
					printf("************************\n\r");
					printf("************************\n\r");
					printf("***** Offset error *****\n\r");
					printf("************************\n\r");
					printf("************************\n\r");
					system_call("beep &");
					exit (0);
					break;
				}

				r->DATA[Offset++] = TheChar;
				if (Offset >= r->REC_LEN -6)
				{
					State = HUNT_LRC;
				}
				break;
			case HUNT_LRC:
				r->LRC =  TheChar ;
				if(r->LRC != LRC )
				{
					printf("LRC = 0x%02X  / Calc = %02X \r\n",r->LRC,LRC);
					State = HUNT_STX;
					PollingRequested=1;
					//printf("resend requested - Error LRC\n\r");

				}
				else
				{
					State = HUNT_ETX;
				}
				break;
			case HUNT_ETX:
				if( TheChar == ETX )
				{
					r->END_CHR =  TheChar ;
			
					treat_data(r);
				}
				State = HUNT_STX;
				break;
			default:
				printf("Default case in Rxloop\r\n");
				State = HUNT_STX;
				//PollingRequested=1;
				printf("resend requested - Default State\n\r");
				break;
		}
	}
}
//===============================================================================================
char aProd[256];
void ProdTestData_file(char * s)
{
	FILE *f;
	int len = strlen(s);

	f= fopen (MOTI_DATA_PRODTEST,"w");
	fwrite(s , 1 , len , f );
	fclose(f);

	if(DoBeep) system_call("beep &");

}
//================================================================================================
void errorfile(char * s)
{
	FILE *f;
	int len = strlen(s);

	f= fopen (MOTI_ERROR_LOG_PATH,"a");
	fwrite(s , 1 , len , f );
	fclose(f);
}
//================================================================================================




//=====================================================================================================================
void treat_df(unsigned char *p)
{
	Decode_Head(p);

	switch(h.Id)
	{
		case 0x7A : DF_7A( p ) ; break; // Transparent - Transport Layer Control
		case 0x79 : DF_79( p ) ; break; // RF Parameters – Modulation Type
		case 0x4D : DF_4D( p ) ; break; // PCD Serial Number
		case 0x06 : DF_06( p ) ; break; // RF - ON/OFF
		case 0x16 : DF_16( p ) ; break; // Detected PICC type
		case 0x6B : DF_6B( p ) ; break; // SAK
		case 0x4E : DF_4E( p ) ; break; // Version
		case 0x0D : DF_0D( p ) ; break; // UID
		case 0x0E : DF_0E( p ) ; break; // ATS  of a detected Type A PICC
		case 0x13 : DF_13( p ) ; break; // ATQB of a detected Type B PICC
		case 0x14 : DF_14( p ) ; break; // PUPI of a detected Type B PICC
		case 0x19 : DF_19( p ) ; break; // Reset PCD
		case 0x04 : DF_04( p ) ; break; // Load Defaults Parameters
		case 0x30 : DF_30( p ) ; break; // RF Collision Message Enable
		case 0x68 : DF_68( p ) ; break; // Poll EMV error
		case 0x7E : DF_7E( p ) ; break; 
		case 0x7D : DF_7D( p ) ; break; // Stop Macro
		case 0xDE : DF_DE( p ) ; break; // Gear - AGC info
		default   : DF_XX( p ) ; break;
	}
}
//=====================================================================================================================






//=====================================================================================================================
void treat_84(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Dedicated File (DF) Name" ;
	int i;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("				[ " );
			for(i=0; i <h.Len ;i++)
			{
				if(h.data[i] >= 0x20 && h.data[i] < 0x7f)
					printf("%c",h.data[i]);
				else
					printf("<%02X>",h.data[i]);
			}
			printf("]\n\r");
		}
	}
}
//=====================================================================================================================


void treat_ff(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[100] ;

	Decode_Head(p);

	if(h.Id == 3 ) 
	{

		clear_record(&r_old);
		ERROR_COUNTS++;
		Poll_stop_requested =1 ;      
	}

	if(h.Id == 1 ) 
	{

		if(h.Len > 3 ) // true data records , Not a ACK/NACK/Stop polling 
		{

			if( h.data[0]== 0xDF && h.data[1])
			{
				// ACK link request
				if(Debug) printf("	PDC LINK DATA\n\r");
				link_requested 	= 0 ;
			}
			else
			{
				ERROR_COUNTS = 0;
				store_record(&ri,&r_old);
				PollPending=0;
				PollingRequested=1;
			}

		}
//		printf("resend requested - Success full template\n\r");

	}

	if(Debug) 
	{
		if(h.Len)
		{
			switch(h.Id)
			{
					//                    123456789012345678901234567890" ;
				case 1 : sprintf(Description,"Success Template        ");break ;
				case 2 : sprintf(Description,"Unsupported Template    ");break ;
				case 3 : sprintf(Description,"Failed Template         ");break ;
				default: sprintf(Description,"????                    ");break ;
			}
		}
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================
void treat_fc(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Result Data Template    " ;

	Decode_Head(p);
	h.Type  = *(p+0);
	h.Len   = *(p+1);
	h.data  = p+2;


	if(Debug) 
	{
		printf("	%02X       (len=%02d): %s",h.Type,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================



















































//==============================================================================
void ShowData(HEAD_REC *h)
{
	int i;

	if(h->Len )
	{
		printf(" [ ");
		for(i=0; i <h->Len ;i++)
		{
			printf("%02X ",h->data[i]);
		}
		printf("]");
	}
	printf("\n\r");

}
//=====================================================================================================================
// TAG 2 BYTES
//=====================================================================================================================
void Decode_Head(unsigned char *p)
{
   h.Type  = *(p+0);
   h.Id    = *(p+1);
   h.Id2   = 0;
   h.Len   = *(p+2);
   h.Val   = *(p+3);

   h.data  = p+3;
}
//=====================================================================================================================


//=====================================================================================================================
// TAG 3 BYTES
//=====================================================================================================================
void Decode_Head_3(unsigned char *p)
{
   h.Type  = *(p+0);
   h.Id    = *(p+1);
   h.Id2   = *(p+2);
   h.Len   = *(p+3);
   h.Val   = *(p+4);

   h.data  = p+4;
}
//=====================================================================================================================



//=====================================================================================================================
void DF_04 (unsigned char *p)
{

	//                    123456789012345678901234567890" ;
	char Description[] = "Load Default Parameters " ;
	
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}




//=====================================================================================================================
void DF_06 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "RF - ON/OFF             " ;
	
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ ");
			switch(h.Val)
			{
				case 0 : printf("RF 13.56 Mhz is OFF");break;
				case 1 : printf("RF 13.56 Mhz is ON");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}


}
//=====================================================================================================================

void DF_0D (unsigned char *p)
{
	FILE *f;
	int i;

	//                    123456789012345678901234567890" ;
	char Description[] = "UID                     " ;
	
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}

	if(h.Len)
	{
		f= fopen ( MOTI_DATA_PATH ,"w");
		if(PICC_type)
		{
				fprintf(f,"   [Card %c]-",PICC_type);
				PICC_type =0;

				if(OneRead)	ExitOneRead=1;
		}
		fprintf(f,"[");
		for(i=0; i <h.Len ;i++)
					{
			fprintf(f,"%02X ",h.data[i] );
		}
		fprintf(f,"]");
		fclose(f); 

	}
}
//==============================================================================


//==============================================================================
void DF_0E (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "ATS of Type A PICC      " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_13 (unsigned char *p )
{
	//                    123456789012345678901234567890" ;
	char Description[] = "PUPI of Type B PICC     " ;
	int i;
	FILE *f;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}


	if(h.Len)
	{
		f= fopen (MOTI_DATA_PATH,"w");
		if(PICC_type)
		{
				fprintf(f,"   [Card %c]-",PICC_type);
				PICC_type =0;

				if(OneRead)	ExitOneRead=1;
		}
		fprintf(f,"[");
		for(i=0; i <h.Len ;i++)
		{
			fprintf(f,"%02X ",h.data[i]);
		}
		fprintf(f,"]");
		fclose(f); 

	}

}
//==============================================================================

void DF_14 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "ATQB of Type B PICC     " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_16 (unsigned char *p)
{
	char Description[100] ;

	Decode_Head(p);

	if(Debug) 
	{
		if(h.Val >= 0x20 && h.Val < 0x7F )
		{
			sprintf(Description,"Detected PICC type       [ %c ]\n\r",h.Val);
		}
		else
		{
			sprintf(Description,"Detected PICC type       [ 0x%02X ]\n\r",h.Val);
		}
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
	}

	if(h.Val >= 0x20 && h.Val < 0x7F )
	{
		PICC_type = h.Val ;
	}
	//============= Prod test data file========================
	if(  (CARD_M + CARD_B + CARD_F + CARD_X) < 5)
	{
		if	(h.Val == 'M') 	CARD_M ++;
		else if	(h.Val == 'B') 	CARD_B ++;
		else if	(h.Val == 'F') 	CARD_F ++;
		else		 	CARD_X ++;
	}
	if(  (CARD_M + CARD_B + CARD_F + CARD_X) >= 5)
	{
		sprintf(aProd,"CARD_M:%02d,CARD_B:%02d,CARD_F:%02d,CARD_X:%02d READ_CARD_OK\n\r",CARD_M ,CARD_B ,CARD_F ,CARD_X);
	}
	else
	{
		sprintf(aProd,"CARD_M:%02d,CARD_B:%02d,CARD_F:%02d,CARD_X:%02d\n\r",CARD_M ,CARD_B ,CARD_F ,CARD_X);
	}
	//ProdTestData_file(aProd);
	if(DoBeep) system_call("beep &");

	//==========================================================
}
//=====================================================================================================================
void DF_19 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Reset PCD               " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================
void DF_30 (unsigned char *p)
{

	//                    123456789012345678901234567890" ;
	char Description[] = "RF Collision Message    " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================



void DF_4D (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "PCD Serial Number       " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ ");
			printf("%02X%02X%02X%02X-%02X%02X%02X%02X",
				*(p+3),*(p+4),*(p+5),*(p+6),
				*(p+7),*(p+8),*(p+9),*(p+10));
			printf(" ]\n\r");
		}
	}
}
//==============================================================================
void DF_4E (unsigned char *p)
{
	char aData[100];
	Decode_Head(p);

	if(Debug) 
	{
		if (h.Len > (int) (sizeof(aData)-2) ) 
			return;
		memcpy(aData,h.data,h.Len);
		aData[h.Len]=0;
		printf("	%02X_%02X    (len:%d) : Version [%s]",h.Type,h.Id,h.Len,aData);
		printf("\n\r");
	}
}
//=====================================================================================================================
void DF_DE (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Gear - AGC info         " ;

	Decode_Head_3(p);
	if(Debug)
	{
		printf("	%02X_%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Id2,h.Len,Description);
		ShowData(&h);
	}

}

//=====================================================================================================================
void DF_68 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Poll EMV error          " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================

void DF_6B (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "SAK                     " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_79 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "RF Parameters           " ;

 
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		");
			switch(h.Val)
			{
				case 0x42 : printf("Modulation Type: [ Type B (FS)");break;
				case 0x41 : printf("Modulation Type: [ Type A ]"   );break;
				default   : printf("Modulation Type: [ RFU ]"      );break;
			}
			printf("\n\r");
		}
	}
}
//==============================================================================
void DF_7A (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Transport layer Control " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ " );
			switch(h.Val)
			{
				case 0 : printf("PCD is responsible (FS)");break;
				case 1 : printf("Host is responsible");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}
}
//==============================================================================
void DF_7D (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Stop Macro              " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ " );
			switch(h.Val)
			{
				case 0 : printf("PCD is responsible (FS)");break;
				case 1 : printf("Host is responsible");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}
}
//==============================================================================
void DF_7E (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Poll for non emv picc   " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);

		if(h.Len == 1 )
		{
			printf(" [ %02X ] %s \n\r",h.Val,ErrorCodeStr(h.Val));
		}
		else
		{
			ShowData(&h);
		}
	}
	

	if( h.Val == 0x05 )
	{
		//Poll_stop_requested = 1;
	} 
	if( h.Val == 0x1E )
	{
		Poll_stop_requested = 1;
	} 
}



//=====================================================================================================================
void DF_XX(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Unknow DF               " ;
  
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X    (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================










#define aSendBuffer_LEN 100
char aSendBuffer [100];

void SendCommand(unsigned char *s)
{
	int i=0;
	int ret;
	int len = s[1] ;
	
	if(len >= aSendBuffer_LEN )
	{
		printf("SendCommand() : Incorrect Command length \n\r");
		return;
	}
	
	memcpy(aSendBuffer,s,len);


	ret = write(devfd, aSendBuffer, len);
	if(ret < 0 )
	{
		printf("write() error (%d) \r\n", ret);
	}

	if(Debug||DebugTXRX) 
	{
		//printf("(%d)\n\r",Toggle++);
		printf("    TX [%03d]:",len);
		for(i=0;i< len ; i++)
		{
			printf(" %02X",aSendBuffer[i] ) ;
		}
		printf("\n\r");
	}

}


//================================================================================================
char filename[100];
int usb_reset(int bus, int device)
{
	int fd;
	int rc;

	sprintf(filename,"/dev/bus/usb/%03d/%03d",bus,device);

	printf("Resetting USB device [%s]\r\n", filename);
	usleep(100000);

	fd = open(filename, O_WRONLY);
	if (fd < 0) {
		printf("Error opening usb file\n\r");
		return 1;
	}
	printf("open %s OK\r\n", filename);

	rc = ioctl(fd, USBDEVFS_RESET, 0);
	if (rc < 0) {
		printf("Error in ioctl:%d\n\r",rc);
		close(fd);
		return 1;
	}

	close(fd);
	printf("Reset successful\n");
	return 0;
}
//================================================================================================
int reset_emv3000(void)
{
	printf("Resetting emv3000 ....\n\r");
	system_call("echo  1 > /sys/class/leds/emv-reset/brightness");
	sleep(1);
	system_call("echo  0 > /sys/class/leds/emv-reset/brightness");
	sleep(5);
	return 0;
}
//================================================================================================








typedef struct {
	unsigned int baud;
	unsigned int cbaud;
} BaudDefines;

static BaudDefines baudDefines[] = { 
	{9600, B9600},
	{19200, B19200},
	{38400, B38400},
	{57600, B57600},
	{115200, B115200},
	{230400, B230400}, 
	{460800, B460800}
};

int RTS_flag ;
int DTR_flag ;


static void closePort(void)
{
	ioctl(devfd,TIOCMBIC,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIC,&RTS_flag);//Set DTR pin
	sleep(1);

	if( Debug ) printf("Closing device ....\n\r");
	close(devfd);
}



int TheSelectedBaudrate = B115200 ;


static int openPort(char *name,unsigned int baud)
{
	unsigned int i,cbaud=TheSelectedBaudrate;
	int r;
	int Flags=0;

	

	struct termios ntio, otio;
	for( i = 0 ; i < sizeof(baudDefines)/sizeof(BaudDefines) ; i++ )
	{
		if(baudDefines[i].baud == baud) 
		{
			cbaud =  baudDefines[i].cbaud; 
			break;
		}
	}

	Flags |= O_RDWR ;
//	Flags |= O_NONBLOCK;
//	Flags |= O_NOCTTY;

	devfd = open(name, Flags);

	if( devfd < 0 ) 
	{
		printf("open [%s] failed %d %s\n", name, devfd, strerror(errno));
		exit(-1);
	}


	DTR_flag = TIOCM_DTR;
	RTS_flag = TIOCM_RTS;


//	printf("DTR/RTS Set \n\r");
	ioctl(devfd,TIOCMBIS,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIS,&RTS_flag);//Set DTR pin
	sleep(1);


	if( ( r = tcgetattr( devfd, & otio ) ) < 0 ){
		printf("openPort [%s] error tcgetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}


	//=================================================
	bzero( & ntio, sizeof( struct termios ) );

	cfmakeraw( &ntio );

	ntio.c_lflag 		= 0;			/* c_lflag : local mode flags   */
	ntio.c_oflag 		= 0;			/* c_oflag : output mode flags  */
	ntio.c_cc[VMIN] 	= 0;
	ntio.c_cc[VTIME] 	= 0;

	ntio.c_cflag = CS8 | CLOCAL | CREAD;	/* c_oflag; control mode flags */
	ntio.c_cflag &= ~HUPCL;
	ntio.c_cflag &= ~PARENB;		/* CLEAR Parity Bit PARENB*/
	ntio.c_cflag &= ~CSTOPB;		/* Stop bits = 1 */
	ntio.c_cflag &= ~CRTSCTS;		/* Turn off hardware based flow control (RTS/CTS). */

	cfsetispeed(&ntio, cbaud);
	cfsetospeed(&ntio, cbaud);

	if( ( r = tcsetattr( devfd, TCSANOW, & ntio ) ) < 0 ){
		printf("open_port [%s] error tcsetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}

	if( ( r = tcflush( devfd, TCIOFLUSH ) ) < 0 ){
		printf("tcflush [%s] error  %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}
	//=================================================

	return devfd;  
}


#define _ms    1000

#define t_50ms  (  50 * _ms     )
#define t_100ms ( 100 * _ms     )
#define t_300ms (   3 * t_100ms )
#define t_1s 	(  10 * t_100ms )
#define t_3s 	(  30 * t_100ms )
#define t_5s 	(  50 * t_100ms )
#define t_10s 	( 100 * t_100ms )
#define t_50s 	( 500 * t_100ms )
#define t_60s 	( 600 * t_100ms )










int file_exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}


#define DEV_OTI     "/dev/oti"
#define DEV_EMV3000 "/dev/emv3000"
#define DEV_TTYACM0 "/dev/ttyACM0"

void set_device_name(void)
{

sprintf( devname, "%s" , "/dev/ttyACM0");
return;
	
	if( file_exists ( DEV_EMV3000  ) == 1 )
	{
		sprintf( devname, "%s" , DEV_EMV3000);
		return;
	}

	if( file_exists ( DEV_OTI  ) == 1 )
	{
		sprintf( devname, "%s" , DEV_OTI );
		return;
	}
	printf("\nmoti  v2020.02.13\n\r");
	printf("device %s and %s doesn't exist ! \n\r\n\r",DEV_OTI,DEV_EMV3000 );
	exit(-1);
}



//=============================================================================
char a_mydatetime[100];  // returns "HH:MM:SS"
char *mydatetime(void)
{
	time_t seconds;   // Seconds since January 1, 1970
	struct tm *ptm;

	seconds = time(NULL);

	ptm = localtime(&seconds);

	sprintf( a_mydatetime, "%02d:%02d:%02d",
		ptm->tm_hour,
		ptm->tm_min,
		ptm->tm_sec
	);
	return(a_mydatetime);
}
//=============================================================================



char completeVersion[50];

char *Build_dateTime(void)
{
	char aTmp[100];
	int Month = 0;
	int Year  = 0;
	int Day   = 0;

	sprintf(aTmp,"%s",__DATE__);

	if(memcmp(aTmp,"Jan",3)==0) Month=1;
	if(memcmp(aTmp,"F"  ,1)==0) Month=2;
	if(memcmp(aTmp,"Mar",3)==0) Month=3;
	if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
	if(memcmp(aTmp,"May",3)==0) Month=5;
	if(memcmp(aTmp,"Jun",3)==0) Month=6;
	if(memcmp(aTmp,"Jul",3)==0) Month=7;
	if(memcmp(aTmp,"Au" ,2)==0) Month=8;
	if(memcmp(aTmp,"S"  ,1)==0) Month=9;
	if(memcmp(aTmp,"O"  ,1)==0) Month=10;
	if(memcmp(aTmp,"N"  ,1)==0) Month=11;
	if(memcmp(aTmp,"D"  ,1)==0) Month=13;

	aTmp[6]=0;
	Day  = atoi(&aTmp[4]);
	Year = atoi(&aTmp[7]);

	sprintf(completeVersion,"%02d/%02d/%04d %s"
		,Day
		,Month
		,Year
		,__TIME__);

	return(completeVersion);
}


//=============================================================================================================================
void show_help(void)
{
	printf("\n\r");
	printf("moti (%s)\n\r",Build_dateTime() );
	printf("-----------------------------\n\r");
	printf("usage: barcode <options> <...>\n\r");
	printf("Options:\n\r");
	printf("    -h       : This info\n\r");
	printf("    -dev     : change default device\n\r");
	printf("    -debug   : Debug on\n\r");
	printf("    -beep    : Beep on valid read\n\r");
	printf("    -out     : Output file\n\r");
	printf("    -oneread : Oneread and exit\n\r");

	exit(0);
}
//=============================================================================================================================


int main ( int argc, char **argv ) 
{
	int i;
	char * p;
	time_t t = time(NULL);

	// printf("\n\rmoti ( %s %s )\n\r",__DATE__,__TIME__);

	sprintf(MOTI_DATA_PATH, MOTI_DEFAULT_DATA_PATH);

	set_device_name();

	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		if(memcmp(p,"-debug"  ,6)==0){ Debug     = 1; }
		if(memcmp(p,"-txrx"   ,5)==0){ DebugTXRX = 1; }
		if(memcmp(p,"-beep"   ,5)==0){ DoBeep    = 1; }
		if(memcmp(p,"-h"      ,2)==0){ show_help();   }


		if(memcmp(p,"-out"    ,4)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				sprintf(MOTI_DATA_PATH,"%s",p);
			}
		};


		if(memcmp(p,"-oneread"    ,8)==0)
		{
			OneRead=1;
		};

		if(memcmp(p,"-AllowDuplicates"    ,8)==0)
		{
			AllowDuplicates=1;
		};

	}


	printf("Output data    : %s\n\r",MOTI_DATA_PATH);
	//==================================================================================	
	sprintf(aError,"\n\r\n\rMoti started : %s",ctime(&t) ) ;
	errorfile(aError);
	//==================================================================================	
	
	printf("STEP 1\n\r");


	devfd=openPort(devname,TheSelectedBaudrate);
	if(devfd == -1)
	{
		printf("\n open() failed with error [%s]\n",strerror(errno));
		return -1;
	}

	//printf("%s : READY\n\r",mydatetime());


	while(1)
	{
		RxLoop();

		if( (nothing%300000) == 100000  )
		{		
			clear_record(&r_old);
		}


		if( ERROR_COUNTS > 50 )
		{
			printf("%s : ERROR_COUNTS > 50 \n\r",mydatetime());
			reset_emv3000();
			break;
		}


		if( PollPending &&  ( Ack_Received == 0 )  && (nothing > t_3s)  )
		{
			if(Debug) printf("%s : No ACK received wihtin 3 seconds \n\r",mydatetime());
			reset_emv3000();
			break;
		}


		if( PollPending  && Ack_Received  && (nothing > t_50s)  )
		{
			if(Debug) printf("%s : Sending Poll stop (1) : %d \n\r",mydatetime(),ERROR_COUNTS);

			SendCommand(Do_Poll_stop );

			PollingRequested 	= 1 ;
			Poll_stop_requested 	= 0;
			PollPending 	 	= 0;
			nothing          	= 0;
			usleep(1000000);
			break;
		}



		if(Poll_stop_requested )
		{
			if(Debug) printf("%s : Sending Poll stop (2) : %d \n\r",mydatetime(),ERROR_COUNTS);
			SendCommand(Do_Poll_stop );

			PollingRequested 	= 1 ;
			Poll_stop_requested 	= 0;
			PollPending 		= 0;
			nothing          	= 0;
			usleep(1000000);
		}

		if(PollingRequested==1 && (nothing > t_50ms) )
		{
			if(Debug) printf("%s : Sending Poll Request\n\r",mydatetime());
			SendCommand(Do_Poll3);

			nothing          = 0 ;
			PollingRequested = 0 ;
			PollPending      = 1 ;
			Ack_Received     = 0 ;
		}

		
		if(nothing > t_60s )
		{
			printf("%s : No activity since 60 seconds\n\r",mydatetime());
			break;
		}
		
		if( ExitOneRead )
		{
			printf("%s : One read exit\n\r",mydatetime());
			break;
		}
		
		nothing += 1000;
		usleep(1000);
	}
	printf("%s : exited from the loop ...\n",mydatetime());
	closePort();
	usleep(100000);
	return 0;
}



