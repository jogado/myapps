#include <stdio.h>
#include <stdlib.h>

#include <messages.h>

/*
[PREDEFINED] [GET - Get FW Version       ]       [02 0A 00 3D DF 4E 01 01 A4 03]    
[PREDEFINED] [GET - Get PDC Serial Number]       [02 09 00 3D DF 4D 00 A4 03]    
[PREDEFINED] [DO  - POLL]                        [02 0A 00 3E DF 7E 01 03 95 03]    
[PREDEFINED] [DO  - POLL EMV 1]                  [02 1E 00 3E FD 16 9F 02 06 00 00 00 00 10 00 9C 01 00 9A 03 17 05 29 5F 2A 02 09 71 72 03]    
[PREDEFINED] [DO  - POLL EMV 2]                  [02 19 00 3E FD 11 9F 02 06 00 00 00 00 12 34 9C 01 00 9A 03 06 12 31 55 03 ]    
[PREDEFINED] [DO  - Set Transparent]             [02 0A 00 3E DF 7A 01 00 92 03]   
[PREDEFINED] [DO  - RF Parameters]               [02 0A 00 3E DF 79 01 41 D0 03]    
[PREDEFINED] [DO  - RF OFF/OFF]                  [02 0A 00 3E DF 06 01 00 EE 03]    
[PREDEFINED] [SET - RF OFF]                      [02 0A 00 3C DF 06 01 00 EC 03]    
[PREDEFINED] [SET - RF ON]                       [02 0A 00 3C DF 06 01 01 ED 03]    
[PREDEFINED] [GET - RF ON/OFF]                   [02 09 00 3D DF 06 00 EF 03]    
[PREDEFINED] [DO  - Load Default Parameters]     [02 09 00 3E DF 04 00 EE 03]    
[PREDEFINED] [DO  - Reset PCD]                   [02 09 00 3E DF 19 00 F3 03]    
[PREDEFINED] [DO  - Enable Application]          [02 0D 00 3E DF 4F 04 30 00 00 00 95 03]    
[PREDEFINED] [SET - Enable Application]          [02 0D 00 3C DF 4F 04 30 00 00 00 97 03]    
[PREDEFINED] [GET - Enable Application]          [02 09 00 3D DF 4F 00 A6 03]    
[PREDEFINED] [SET - PCD Settings]                [02 1A 00 3C DF 13 01 01 DF 0D 01 01 DF 0E 01 01 DF 14 01 01 DF 30 01 01 CF 03]
[PREDEFINED] [GET - PCD Settings]                [02 15 00 3D DF 13 00 DF 0D 00 DF 0E 00 DF 14 00 DF 30 00 C1 03]
[PREDEFINED] [SET - SAK]                         [02 0A 00 3C DF 6B 01 01 80 03]
[PREDEFINED] [GET - SAK]                         [02 09 00 3D DF 6B 00 82 03]
[PREDEFINED] [GET - SERIAL & FIRMWARE]           [02 0D 00 3D DF 4E 01 01 DF 4D 00 31 03 ]
[PREDEFINED] [SET - ALL CARDS]                   [02 0D 00 3C DF 4F 04 FF FF 00 00 A7 03 ]
[PREDEFINED] [SET - Collision Enabled]           [02 0A 00 3C DF 30 01 01 DB 03 ]
*/




unsigned char Do_Poll1        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x01,0x97,0x03}; /* Pending Auto Polling Continuously for supported Application Transaction */
unsigned char Do_Poll2        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x02,0x94,0x03}; /* Pending Auto Polling once for supported Application Transaction  */
unsigned char Do_Poll3          	[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x03,0x95,0x03}; /* Pending Polling once and ONLY Activate PICC */
unsigned char Do_Poll4        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x04,0x92,0x03}; /* Immediate Polling once and ONLY Activate PICC */


unsigned char Do_Poll_stop 		[] = {0x02,0x09,0x00,0x3E,0xDF,0x7D,0x00,0x97,0x03}; 		// Pending Polling once and ONLY Activate PICC 
unsigned char Get_FW_Version		[] = {0x02,0x0A,0x00,0x3D,0xDF,0x4E,0x01,0x01,0xA4,0x03};
unsigned char Get_PDC_Serial_Number	[] = {0x02,0x09,0x00,0x3D,0xDF,0x4D,0x00,0xA4,0x03};
unsigned char Get_RF_param_MaxBitRate	[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};
unsigned char Do_RF_param_FWTI		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};
unsigned char Do_PCD_serial_number	[] = {0x02,0x09,0x00,0x3D,0xDF,0x4D,0x00,0xA4,0x03};  		// PCD Serial number
unsigned char do_RF_max_bitrate		[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};		// RF Parameter MAX bit rate
unsigned char Do_RF_Parameter		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};	// RF Parameter

unsigned char Do_RF_OFF			[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x00 ,0xEE ,0x03};
unsigned char Do_RF_ON	          	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x01 ,0xEF ,0x03};




