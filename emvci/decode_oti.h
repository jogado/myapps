#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>

extern void treat_df(unsigned char *p);
extern void treat_ff(unsigned char *p);
extern void treat_fc(unsigned char *p);
extern void treat_84(unsigned char *p);

extern void DF_68 (unsigned char *p);
extern void DF_30 (unsigned char *p);
extern void DF_04 (unsigned char *p);
extern void DF_19 (unsigned char *p);
extern void DF_06 (unsigned char *p);
extern void DF_4D (unsigned char *p);
extern void DF_79 (unsigned char *p);
extern void DF_7A (unsigned char *p);
extern void DF_13 (unsigned char *p);
extern void DF_16 (unsigned char *p);
extern void DF_14 (unsigned char *p);
extern void DF_0D (unsigned char *p);
extern void DF_68 (unsigned char *p);
extern void DF_6B (unsigned char *p);
extern void DF_4E (unsigned char *p);
extern void DF_0E (unsigned char *p);
extern void DF_7D (unsigned char *p);
extern void DF_7E (unsigned char *p);
extern void DF_7F (unsigned char *p);
extern void DF_XX (unsigned char *p);


extern void DF_DE (unsigned char *p);

