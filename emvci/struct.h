

typedef struct
{
   unsigned char START_CHR;
   unsigned char REC_LEN;
   unsigned char REC_LEN2;
   unsigned char UNIT;
   unsigned char OPCODE;
   unsigned char DATA[512];
   unsigned char LRC;
   unsigned char END_CHR;
}
OTI_REC;

typedef struct
{
	int Type;
	int Id;
	int Len;
	int Val;
	unsigned char *data;
}
HEAD_REC;

typedef enum {
   HUNT_STX = 1   ,
   HUNT_LEN       ,
   HUNT_LEN2      ,
   HUNT_UNIT      ,
   HUNT_OPCODE    ,
   HUNT_DATA      ,
   HUNT_LRC       ,
   HUNT_ETX       ,
} HUNT_STATES;


