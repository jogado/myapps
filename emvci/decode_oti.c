#include "struct.h"
#include "decode_oti.h"


extern HEAD_REC h;
extern void 	Decode_Head		(unsigned char *p);
extern void 	ShowData		(HEAD_REC *h);
extern void 	ShowDataStr		(HEAD_REC *h);

extern int 	Debug;
extern int 	CARD_M;
extern int 	CARD_B;
extern int 	CARD_F;
extern int 	CARD_X;
extern char 	aProd[256];
extern void 	ProdTestData_file	(char * s);
extern void 	clear_record	(OTI_REC *rec);
extern OTI_REC 	r_old;
extern OTI_REC 	ri;
extern int 	PollingRequested;
extern int 	PollPending;
extern void 	store_record	(OTI_REC *r,OTI_REC *r_old);

extern char EMV_GET_STRING[];
extern char RESPONSE_TYPE;
extern char RESPONSE_LEN;

extern char emv_sn [100];
extern char emv_fw [100];
extern int  emv_rf;
extern int  emv_outport;
extern int emv_version;
//=====================================================================================================================
void treat_df(unsigned char *p)
{
	Decode_Head(p);

	switch(h.Id)
	{
		case 0x7A : DF_7A( p ) ; break; // Transparent - Transport Layer Control
		case 0x79 : DF_79( p ) ; break; // RF Parameters – Modulation Type
		case 0x4D : DF_4D( p ) ; break; // PCD Serial Number
		case 0x06 : DF_06( p ) ; break; // RF - ON/OFF
		case 0x16 : DF_16( p ) ; break; // Detected PICC type
		case 0x6B : DF_6B( p ) ; break; // SAK
		case 0x4E : DF_4E( p ) ; break; // Version
		case 0x0D : DF_0D( p ) ; break; // UID
		case 0x0E : DF_0E( p ) ; break; // ATS  of a detected Type A PICC
		case 0x13 : DF_13( p ) ; break; // ATQB of a detected Type B PICC
		case 0x14 : DF_14( p ) ; break; // PUPI of a detected Type B PICC
		case 0x19 : DF_19( p ) ; break; // Reset PCD
		case 0x04 : DF_04( p ) ; break; // Load Defaults Parameters
		case 0x30 : DF_30( p ) ; break; // RF Collision Message Enable
		case 0x68 : DF_68( p ) ; break; // Poll EMV error
		case 0x7E : DF_7E( p ) ; break; 
		case 0x7F : DF_7F( p ) ; break; 
		case 0x7D : DF_7D( p ) ; break; // Stop Macro
		case 0xDE : DF_DE( p ) ; break; // Get Version
		default   : DF_XX( p ) ; break;
	}
}
//=====================================================================================================================
void treat_84(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Dedicated File (DF) Name" ;
	int i;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("				[ " );
			for(i=0; i <h.Len ;i++)
			{
				if(h.data[i] >= 0x20 && h.data[i] < 0x7f)
					printf("%c",h.data[i]);
				else
					printf("<%02X>",h.data[i]);
			}
			printf("]\n\r");
		}
	}
}
//=====================================================================================================================
void treat_ff(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[100] ;


	Decode_Head(p);


	if(h.Id == 3 ) 
	{
		clear_record(&r_old);
		PollPending=0;
		PollingRequested=1;
	}

	if(h.Id == 1 ) 
	{

		if(h.Len > 3 ) // true data records , Not a ACK/NACK/Stop polling 
		{
			store_record(&ri,&r_old);
			PollPending=0;
			PollingRequested=1;
		}
//		printf("resend requested - Success full template\n\r");

	}

	if(Debug ) 
	{
		if(h.Len)
		{
			switch(h.Id)
			{
					//                    123456789012345678901234567890" ;
				case 1 : sprintf(Description,"Success Template        ");break ;
				case 2 : sprintf(Description,"Unsupported Template    ");break ;
				case 3 : sprintf(Description,"Failed Template         ");break ;
				default: sprintf(Description,"????                    ");break ;
			}
		}
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================
void treat_fc(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Result Data Template    " ;

	h.Type  = *(p+0);
	h.Len   = *(p+1);
	h.data  = p+2;


	if(Debug) 
	{
		printf("	%02X    (len=%02d): %s",h.Type,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================





























//=====================================================================================================================
void DF_04 (unsigned char *p)
{

	//                    123456789012345678901234567890" ;
	char Description[] = "Load Default Parameters " ;
	
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}




//=====================================================================================================================
void DF_06 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "RF - ON/OFF             " ;
	
	Decode_Head(p);

	emv_rf = 1;

	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ ");
			switch(h.Val)
			{
				case 0 : printf("RF 13.56 Mhz is OFF");break;
				case 1 : printf("RF 13.56 Mhz is ON");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}

}
//=====================================================================================================================


void DF_0D (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "UID                     " ;
	
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_0E (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "ATS of Type A PICC      " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_13 (unsigned char *p )
{
	//                    123456789012345678901234567890" ;
	char Description[] = "PUPI of Type B PICC     " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}

}
//==============================================================================

void DF_14 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "ATQB of Type B PICC     " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_16 (unsigned char *p)
{
	char Description[100] ;

	Decode_Head(p);

	if(Debug) 
	{
		if(h.Val >= 0x20 && h.Val < 0x7F )
		{
			sprintf(Description,"Detected PICC type       [ %c ]\n\r",h.Val);
		}
		else
		{
			sprintf(Description,"Detected PICC type       [ 0x%02X ]\n\r",h.Val);
		}
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
	}

	//============= Prod test data file========================
	/*
	if(  (CARD_M + CARD_B + CARD_F + CARD_X) < 5)
	{
		if	(h.Val == 'M') 	CARD_M ++;
		else if	(h.Val == 'B') 	CARD_B ++;
		else if	(h.Val == 'F') 	CARD_F ++;
		else		 	CARD_X ++;
	}
	if(  (CARD_M + CARD_B + CARD_F + CARD_X) >= 5)
	{
		sprintf(aProd,"CARD_M:%02d,CARD_B:%02d,CARD_F:%02d,CARD_X:%02d READ_CARD_OK\n\r",CARD_M ,CARD_B ,CARD_F ,CARD_X);
	}
	else
	{
		sprintf(aProd,"CARD_M:%02d,CARD_B:%02d,CARD_F:%02d,CARD_X:%02d\n\r",CARD_M ,CARD_B ,CARD_F ,CARD_X);
	}
	ProdTestData_file(aProd);
	*/
	//==========================================================
}
//=====================================================================================================================
void DF_19 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Reset PCD               " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================
void DF_30 (unsigned char *p)
{

	//                    123456789012345678901234567890" ;
	char Description[] = "RF Collision Message    " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================
void DF_4D (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "PCD Serial Number       " ;
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ ");
			printf("%02X%02X%02X%02X-%02X%02X%02X%02X",
				*(p+3),*(p+4),*(p+5),*(p+6),
				*(p+7),*(p+8),*(p+9),*(p+10));
			printf(" ]\n\r");

		}
	}

	if(h.Len)
	{
		sprintf(emv_sn,"%02X%02X%02X%02X%02X",
			*(p+4),*(p+5),*(p+6),
			*(p+7),*(p+8));
	}

}
//==============================================================================
void DF_4E (unsigned char *p)
{
	char aData[100];
	Decode_Head(p);

	if (h.Len > (int) (sizeof(aData)-2) ) 
			return;
	memcpy(aData,h.data,h.Len);
	aData[h.Len]=0;

	if(Debug) 
	{
		printf("	%02X_%02X(len:%d) : Version [%s]",h.Type,h.Id,h.Len,aData);
		printf("\n\r");
	}

	sprintf(emv_fw,"%s",aData);
}
//=====================================================================================================================
void DF_68 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Poll EMV error          " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//=====================================================================================================================

void DF_6B (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "SAK                     " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_79 (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "RF Parameters           " ;

 
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		");
			switch(h.Val)
			{
				case 0x42 : printf("Modulation Type: [ Type B (FS)");break;
				case 0x41 : printf("Modulation Type: [ Type A ]"   );break;
				default   : printf("Modulation Type: [ RFU ]"      );break;
			}
			printf("\n\r");
		}
	}
}
//==============================================================================
void DF_7A (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Transport layer Control " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ " );
			switch(h.Val)
			{
				case 0 : printf("PCD is responsible (FS)");break;
				case 1 : printf("Host is responsible");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}
}
//==============================================================================
void DF_7D (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Stop Macro              " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ " );
			switch(h.Val)
			{
				case 0 : printf("PCD is responsible (FS)");break;
				case 1 : printf("Host is responsible");break;
				default: printf("RFU");break;
			}
			printf("]\n\r");
		}
	}
}
//==============================================================================
void DF_7E (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Unknow code             " ;

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================
void DF_7F (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Outport                 " ;

	emv_outport=1;	

	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);

		if(h.Len)
		{
			printf("		[ %04X ]\n\r", h.Val);
		}
	}
}
//==============================================================================
void DF_DE (unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Version                 " ;

	Decode_Head(p+1);

	
	RESPONSE_TYPE = h.Id ;
	RESPONSE_LEN  = h.Len ;

	emv_version=1;	
	memcpy(EMV_GET_STRING, h.data , h.Len);
	EMV_GET_STRING [h.Len]=0;
	

	if(Debug) 
	{
		if(h.Id == 0x14)
		{
			printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,"DPC_TRIM                ");
		}
		else
		{
			printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		}
		ShowDataStr(&h);


	}


}
//==============================================================================















//=====================================================================================================================
void DF_XX(unsigned char *p)
{
	//                    123456789012345678901234567890" ;
	char Description[] = "Unknow DF               " ;
  
	Decode_Head(p);
	if(Debug) 
	{
		printf("	%02X_%02X (len=%02d): %s",h.Type,h.Id,h.Len,Description);
		ShowData(&h);
	}
}
//==============================================================================






