#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>
#include "struct.h"
#include "decode_oti.h"



char aError[256];

int Debug     = 0 ;
int DebugTXRX = 0 ;




OTI_REC 	ri;
OTI_REC 	r_old;
HEAD_REC 	h;



#define STX 0x02
#define ETX 0x03

#define EMV300_SET	0x3c
#define EMV300_GET	0x3d
#define EMV300_DO	0x3e


#define ms_1 	1000
#define ms_100 	100000
#define ms_200 	200000
#define ms_500 	500000
#define ms_50  	50000




extern void store_record	(OTI_REC *r,OTI_REC *r_old);
extern int same_record		(OTI_REC *r,OTI_REC *r_old);
extern void clear_record	(OTI_REC *rec);
extern void FixCrc			(unsigned char *s);

extern int  main 		( int argc, char **argv ) ;
extern void Decode_Head		(unsigned char *p);
extern void ShowData		(HEAD_REC *h);
extern void ShowDataStr		(HEAD_REC *h);

extern int  usb_reset		(int bus, int device);
extern int  reset_emv3000	(void);
extern void RxLoop		(void);
extern void treat_data		(OTI_REC *r);
extern void SendCommand		(unsigned char *s);
extern void Send_do_Command 	( char c, char *s);
extern void Send_set_Command	( char reg, char *s);


extern void errorfile		(char * s);
extern void system_call 	(const char *s);
extern int  file_exists		(const char *fname);
extern void set_device_name	(void);
extern char *Build_dateTime	(void);
extern int  ani 		(char *s ,int len);

extern char BcdToDecimal	(char);
extern char DecimalToBcd	(char);






char devname[128] ;
int  devfd;


int PollingRequested=1;
int PollPending = 0;
int Ack_Received=0;
int nothing=0;
int Done = 0;
int Toggle=0;
int Command_rejected = 0;


//============================================================================================
void store_record(OTI_REC *r,OTI_REC *r_old)
{
	memcpy(r_old,r,sizeof(OTI_REC));
}
//============================================================================================
int same_record(OTI_REC *r,OTI_REC *r_old)
{
	return( memcmp(r,r_old,sizeof(OTI_REC)) ) ;
}
//============================================================================================
void clear_record(OTI_REC *rec)
{
	 memset(rec,0,sizeof(OTI_REC));
}
//============================================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}
//============================================================================================





//============================================================================================
void treat_data(OTI_REC *r)
{
	int len = r->REC_LEN -6;
	unsigned char *p = &r->DATA[0] ;
	int r_type;
	int r_len;

	if( r->REC_LEN2)
	{
		len = r->REC_LEN2 -7;
	}

	
	if(Debug) printf("Treat_data(%d)\n\r", len);

	//nothing=0;
	Done=0;

	if( (r->REC_LEN==8) && (r->DATA[0]==0x00) && (r->DATA[1]==0x00) )
	{
		if(Debug) printf("	PDC ACK\n\r");
		Ack_Received=1;
		return;
	}

	if( (r->REC_LEN==7) && (r->OPCODE==0x15) )
	{
		printf("	PDC NACK (error: 0x%02X)\n\r",r->DATA[0]);
		Command_rejected = 1;
		PollingRequested = 1;
		PollPending      = 0 ;
		return;
	}
	


	while (len > 0)
	{
		r_type= *(p+0);
//		r_id  = *(p+1);
		r_len = *(p+2);

	
		if( r_type == 0xDF )
		{
			r_len = *(p+3);

			treat_df ( p );
			p   += r_len +4 ;
			len -= r_len +4 ;
			continue;
		}

		else if( r_type == 0xFF ) // PDC Response templates
		{


			treat_ff ( p );

			if(*(p+2)==0x81)
			{
				p+=4 ;
				len -= 4;
				continue;
			}
			p+=3 ;
			len -= 3;	// ADDED by LUC !!!!! but it still blocks :-(


			continue;
		}
		else if( r_type == 0xFC ) // Result Data Template
		{
			treat_fc ( p );
			p+=2 ;
			len -= 2;	// ADDED by LUC !!!!! but it still blocks :-(
			continue;
		}

		else if( r_type == 0x84 ) // Dedicated File (DF) Name (EMV Cards)
		{
			//r_type= *(p+0);
			//r_id  = *(p+1);
			//r_len = *(p+2);

			treat_84 ( p );
			p   += r_len +2 ;
			len -= r_len +2 ;
			continue;
		}
		else	// ADDED by LUC
		{
			printf("Unknown type: %X\r\n", r_type);
		}

		p++;
		len--;
	}
}
//============================================================================================




char FixedRecord[512];

void FixCrc(unsigned char *s)
{
	int i;
	int State = HUNT_STX;
	int LRC =0;
	int REC_LEN=0;
	int	TheChar;
	int Len = s[1] ;
	


	memset( FixedRecord ,0x00,sizeof( FixedRecord ) );

	for(i=0;i<Len;i++)
	{
		TheChar = (int)s[i] & 0x00ff;

		FixedRecord[i] = TheChar ;


		//printf("(%d/%d) %02X\n\r",i,Len,TheChar);

		switch (State)
		{
			case HUNT_STX:
				LRC = 0;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				State  = HUNT_LEN;
				break;
			case HUNT_LEN:
				State 	= HUNT_UNIT;
				REC_LEN =  TheChar ;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LEN2:
				State 	= HUNT_UNIT;
				REC_LEN =  TheChar ;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_UNIT:
				State = HUNT_OPCODE;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;

			case HUNT_OPCODE:
				State = HUNT_DATA;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			
			case HUNT_DATA:
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				if (i >= REC_LEN -3)
				{
					State = HUNT_LRC;
				}			
				break;
			case HUNT_LRC:
					State = HUNT_ETX;
					if(Debug && TheChar != LRC)
					{
						//printf("LRC = 0x%02X  / Calc = %02X \r\n",TheChar,LRC);
					}
					FixedRecord[i] = LRC ;
					break;
			case HUNT_ETX:
					break;
			default:
					break;
		}
	}

	if(Debug)
	{	/*
		printf("Old REC : ");
		for(i=0;i<Len;i++)
		{
			printf("%02X",s[i]);
		}
		printf("\n\r");


		printf("New REC : ");
		for(i=0;i<Len;i++)
		{
			printf("%02X",FixedRecord[i]);
		}
		printf("\n\r");
		*/
	}	
}








unsigned char Buffer[512];
int Buffer_len=0;
int Buffer_offset=0;

void RxLoop(void)
{
	static unsigned char    State = HUNT_STX;
	static int		Offset;
	static unsigned char	LRC;
	OTI_REC			*r=&ri;
	int 			len;
	int 			TheChar;


	while (1)
	{
		if(Buffer_len)
		{
			TheChar = (int)Buffer[Buffer_offset++];
			if(Buffer_offset >= Buffer_len )
			{
				Buffer_len    = 0;
				Buffer_offset = 0;
				//usleep(100000); // System give hand, could help ?
			}
		}
		else
		{
			len = read(devfd,&Buffer,200);
			if(len < 0)
			{
				printf("read error : (%d) %s\n\r", len , strerror(errno));
				return;
			}
			if(len==0)
			{
				return;
			}
			Buffer_len    = len;
			Buffer_offset = 0;
			continue;
		}



		//nothing=0;
		TheChar = TheChar & 0x00ff;

		//printf("%02X\n\r",TheChar);

		switch (State)
		{
			case HUNT_STX:
				LRC = 0;
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LEN:
			case HUNT_LEN2:
			case HUNT_UNIT:
			case HUNT_OPCODE:
			case HUNT_DATA:
				LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
				break;
			case HUNT_LRC:
			case HUNT_ETX:
				break;
			default:
				break;
		}

		if( State == HUNT_LEN )
		{
			if(DebugTXRX  && (TheChar != 0x81) ) printf("RX [%03d]: 02",TheChar);
		}

		if( State == HUNT_LEN2 )
		{
			if(DebugTXRX ) printf("\n\rRX [%03d]: %02x %02x",TheChar,0x02,r->REC_LEN);
		}


		if( State != HUNT_STX   && DebugTXRX )
		{
			if( State == HUNT_LEN )		
			{
				if(TheChar != 0x81 )
				{
					printf(" %02X",TheChar);
				}
			}
			else
			{
				 printf(" %02X",TheChar);
			}
				
		}

		if( State == HUNT_ETX && TheChar == ETX )
		{
			if(DebugTXRX ) printf("\n\r");
		}

		switch (State)
		{
			case HUNT_STX:
				if( TheChar == STX )
				{
					memset(r,0x00,sizeof(OTI_REC));
					Offset = 0;
					State  = HUNT_LEN;
					r->START_CHR = TheChar;
				}
				break;
			case HUNT_LEN:
				r->REC_LEN =  TheChar ;

				if(TheChar == 0x81 )
				{
					State  = HUNT_LEN2;
					break;
				}
				State = HUNT_UNIT;
				break;
			case HUNT_LEN2:
				r->REC_LEN2 =  TheChar ;
				State = HUNT_UNIT;
				break;
			case HUNT_UNIT:
				r->UNIT = TheChar ;
				State = HUNT_OPCODE;
				break;
			case HUNT_OPCODE:
				r->OPCODE =  TheChar ;
				State = HUNT_DATA;
				break;
			case HUNT_DATA:
	
				if(Offset >200)
				{
					State=HUNT_STX;
					printf("************************\n\r");
					printf("************************\n\r");
					printf("***** Offset error *****\n\r");
					printf("************************\n\r");
					printf("************************\n\r");
					system_call("beep &");
					exit (0);
					break;
				}

				r->DATA[Offset++] = TheChar;

				if( r->REC_LEN2 )
				{
					if (Offset >= r->REC_LEN2 -7)
					{
						State = HUNT_LRC;
					}
				}
				else
				{
					if (Offset >= r->REC_LEN -6)
					{
						State = HUNT_LRC;
					}
				}
				break;
			case HUNT_LRC:
				r->LRC =  TheChar ;
				if(r->LRC != LRC )
				{
					printf("\n\rLRC = 0x%02X  / Calc = %02X \r\n",r->LRC,LRC);
					State = HUNT_STX;
					PollingRequested=1;
					//printf("resend requested - Error LRC\n\r");

				}
				else
				{
					State = HUNT_ETX;
				}
				break;
			case HUNT_ETX:
				if( TheChar == ETX )
				{
					r->END_CHR =  TheChar ;
					treat_data(r);
				}
				State = HUNT_STX;
				break;
			default:
				printf("Default case in Rxloop\r\n");
				State = HUNT_STX;
				PollingRequested=1;
				printf("resend requested - Default State\n\r");
				break;
		}
	}
}




//================================================================================================
void ShowData(HEAD_REC *h)
{
	int i;
	int max = 15 ;	

	if(h->Len > max)
	{
		printf(" [ ");
		for(i=0; i < max ;i++)
		{
			printf("%02X ",h->data[i]);
		}
		printf("  .....]\n\r");
	}	
	else
	{
		if(h->Len )
		{
			printf(" [ ");
			for(i=0; i <h->Len ;i++)
			{
				printf("%02X ",h->data[i]);
			}
			printf("]");
		}
		printf("\n\r");
	}
}

void ShowDataStr(HEAD_REC *h)
{
	int i;

	if(h->Len )
	{
		printf(" [ ");
		for(i=0; i <h->Len ;i++)
		{	
			//printf("%c",h->data[i]);
			printf("%02X ",h->data[i]);

			/*
			if( isalpha(h->data[i]) )
				printf("%c",h->data[i]);
			else
				printf("0x%02X",h->data[i]);
			*/
		}
		printf(" ]");
	}
	printf("\n\r");
}

//==============================================================================
void Decode_Head(unsigned char *p)
{
   h.Type  = *(p+0);
   h.Id    = *(p+1);
   h.Len   = *(p+2);
   h.Val   = *(p+3);
   h.data  = p+3;


   if( h.Len == 0x81)
   {
	h.Len   = *(p+3);
	h.Val   = *(p+4);
	h.data  = p+4;
   }




}
//=====================================================================================================================









#define aSendDoBuffer_LEN 100
unsigned char aSendDoBuffer [aSendDoBuffer_LEN +10];


void Send_do_Command( char c, char *s)
{
	int i=0;
	int len = strlen(s);

	aSendDoBuffer[0] = 0x02 ;
	aSendDoBuffer[1] = len + 11 ;
	aSendDoBuffer[2] = 0 ;
	aSendDoBuffer[3] = 0x44 ;
	aSendDoBuffer[4] = 0xDF ;
	aSendDoBuffer[5] = 0xDE ;
	aSendDoBuffer[6] = 0x6F ;
	aSendDoBuffer[7] = len + 1 ;
	aSendDoBuffer[8] = c ;
	
	for(i = 0; i < len ; i++)
	{
		aSendDoBuffer[9+i] = s[i];
	}

	aSendDoBuffer[9+len]   = 0xff ;
	aSendDoBuffer[9+len+1] = 0x03 ;

	SendCommand(aSendDoBuffer);
}

void Send_set_Command( char reg, char *s)
{
	int i=0;
	
	aSendDoBuffer[0] = 0x02 ;
	aSendDoBuffer[1] = 0x11 ;
	aSendDoBuffer[2] = 0x00 ;
	aSendDoBuffer[3] = 0x44 ;
	aSendDoBuffer[4] = 0xDF ;
	aSendDoBuffer[5] = 0xDE ;
	aSendDoBuffer[6] = 0x10 ;
	aSendDoBuffer[7] = 0x07 ;
	
	for(i = 0; i < 7 ; i++)
	{
		aSendDoBuffer[8+i] = s[i];
	}
	aSendDoBuffer[15] = 0xff ;
	aSendDoBuffer[16] = 0x03 ;

	SendCommand(aSendDoBuffer);
}
















#define aSendBuffer_LEN 100
char aSendBuffer [aSendBuffer_LEN+10];

void SendCommand(unsigned char *s)
{
	int i=0;
	int ret;
	int len = s[1] ;
	

	if(len >= aSendBuffer_LEN )
	{
		printf("SendCommand() : Incorrect Command length \n\r");
		return;
	}

	FixCrc(s);


	memcpy(aSendBuffer,FixedRecord,len);

	ret = write(devfd, aSendBuffer, len);
	if(ret < 0 )
	{
		printf("write() error (%d) \r\n", ret);
	}

	if( DebugTXRX ) 
	{
		//printf("(%d)\n\r",Toggle++);

		printf("TX [%03d]:",len);
		for(i=0;i< len ; i++)
		{
			printf(" %02X",aSendBuffer[i] ) ;
		}
		printf("\n\r");
	}
}


//================================================================================================
char filename[100];
int usb_reset(int bus, int device)
{
	int fd;
	int rc;

	sprintf(filename,"/dev/bus/usb/%03d/%03d",bus,device);

	printf("Resetting USB device [%s]\r\n", filename);
	usleep(100000);

	fd = open(filename, O_WRONLY);
	if (fd < 0) {
		printf("Error opening usb file\n\r");
		return 1;
	}
	printf("open %s OK\r\n", filename);

	rc = ioctl(fd, USBDEVFS_RESET, 0);
	if (rc < 0) {
		printf("Error in ioctl:%d\n\r",rc);
		close(fd);
		return 1;
	}

	close(fd);
	printf("Reset successful\n");
	return 0;
}
//================================================================================================
int reset_emv3000(void)
{
	printf("Resetting emv3000 ....\n\r");
	system_call("echo  1 > /sys/class/leds/emv-reset/brightness");
	sleep(1);
	system_call("echo  0 > /sys/class/leds/emv-reset/brightness");
	return 0;
}
//================================================================================================








typedef struct {
	unsigned int baud;
	unsigned int cbaud;
} BaudDefines;

static BaudDefines baudDefines[] = { 
	{9600, B9600},
	{19200, B19200},
	{38400, B38400},
	{57600, B57600},
	{115200, B115200},
	{230400, B230400}, 
	{460800, B460800}
};

int RTS_flag ;
int DTR_flag ;


static void closePort(void)
{
	//printf("Clear DTR/RTS\n\r");
	ioctl(devfd,TIOCMBIC,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIC,&RTS_flag);//Set DTR pin
	close(devfd);
}



int TheSelectedBaudrate = B115200 ;


static int openPort(char *name,unsigned int baud)
{
	unsigned int i,cbaud=TheSelectedBaudrate;
	int r;
	int Flags=0;

	

	struct termios ntio, otio;
	for( i = 0 ; i < sizeof(baudDefines)/sizeof(BaudDefines) ; i++ )
	{
		if(baudDefines[i].baud == baud) 
		{
			cbaud =  baudDefines[i].cbaud; 
			break;
		}
	}

	Flags |= O_RDWR ;
//	Flags |= O_NONBLOCK;
//	Flags |= O_NOCTTY;

	devfd = open(name, Flags);

	if( devfd < 0 ) 
	{
		printf("open [%s] failed %d %s\n", name, devfd, strerror(errno));
		exit(-1);
	}


	DTR_flag = TIOCM_DTR;
	RTS_flag = TIOCM_RTS;


	//printf("DTR/RTS Set \n\r");
	ioctl(devfd,TIOCMBIS,&DTR_flag);//Set DTR pin
	ioctl(devfd,TIOCMBIS,&RTS_flag);//Set DTR pin
	//sleep(1);


	if( ( r = tcgetattr( devfd, & otio ) ) < 0 ){
		printf("openPort [%s] error tcgetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}


	//=================================================
	bzero( & ntio, sizeof( struct termios ) );

	cfmakeraw( &ntio );

	ntio.c_lflag 		= 0;			/* c_lflag : local mode flags   */
	ntio.c_oflag 		= 0;			/* c_oflag : output mode flags  */
	ntio.c_cc[VMIN] 	= 0;
	ntio.c_cc[VTIME] 	= 0;

	ntio.c_cflag = CS8 | CLOCAL | CREAD;	/* c_oflag; control mode flags */
	ntio.c_cflag &= ~HUPCL;
	ntio.c_cflag &= ~PARENB;		/* CLEAR Parity Bit PARENB*/
	ntio.c_cflag &= ~CSTOPB;		/* Stop bits = 1 */
	//ntio.c_cflag &= ~CRTSCTS;		/* Turn off hardware based flow control (RTS/CTS). */

	cfsetispeed(&ntio, cbaud);
	cfsetospeed(&ntio, cbaud);

	if( ( r = tcsetattr( devfd, TCSANOW, & ntio ) ) < 0 ){
		printf("open_port [%s] error tcsetattr %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}

	if( ( r = tcflush( devfd, TCIOFLUSH ) ) < 0 ){
		printf("tcflush [%s] error  %d %s\n", 
			name,r, strerror( errno ) );
		return r;
	}
	//=================================================

	return devfd;  
}




/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/



unsigned char Do_Poll_1        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x01,0x97,0x03}; /* Pending Auto Polling Continuously for supported Application Transaction */
unsigned char Do_Poll_2        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x02,0x94,0x03}; /* Pending Auto Polling once for supported Application Transaction  */
unsigned char Do_Poll         		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x03,0x95,0x03}; /* Pending Polling once and ONLY Activate PICC */
unsigned char Do_Poll_4        		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x7E,0x01,0x04,0x92,0x03}; /* Immediate Polling once and ONLY Activate PICC */
unsigned char Do_Poll_stop 		[] = {0x02,0x09,0x00,0x3E,0xDF,0x7D,0x00,0x97,0x03}; 		// Pending Polling once and ONLY Activate PICC 
unsigned char Get_RF_param_MaxBitRate	[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};
unsigned char Do_RF_param_FWTI		[] = {02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};
unsigned char do_RF_max_bitrate		[] = {0x02,0x09,0x00,0x3D,0xDF,0x17,0x00,0xFE,0x03};		// RF Parameter MAX bit rate
unsigned char Do_RF_Parameter		[] = {0x02,0x0A,0x00,0x3E,0xDF,0x03,0x01,0x08,0xE3,0x03};	// RF Parameter


unsigned char Do_RF_OFF			[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x00 ,0xEE ,0x03};
unsigned char Do_RF_ON	          	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x06 ,0x01 ,0x01 ,0xEF ,0x03};

unsigned char Do_OUTPORT_BUZZER_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x01 ,0x96 ,0x03};
unsigned char Do_OUTPORT_LED1_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x02 ,0x95 ,0x03};
unsigned char Do_OUTPORT_LED2_ON	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x04 ,0x93 ,0x03};
unsigned char Do_OUTPORT_LED3_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x08 ,0x9F ,0x03};
unsigned char Do_OUTPORT_LED4_ON 	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x10 ,0x87 ,0x03};
unsigned char Do_OUTPORT_ALL_OFF  	[] = {0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7F ,0x01 ,0x00 ,0x97 ,0x03};
unsigned char Get_FW_Version		[] = {0x02 ,0x0A ,0x00 ,0x3D ,0xDF ,0x4E ,0x01 ,0x01 ,0xA4 ,0x03};


unsigned char Do_CONTROL_OFF	  	[] = {0x02 ,0x0B ,0x00 ,0x44 ,0xDF ,0xDE ,0x28 ,0x01 ,0x03 ,0x66 ,0x03};
unsigned char Do_CONTROL_ON	  	[] = {0x02 ,0x0B ,0x00 ,0x44 ,0xDF ,0xDE ,0x28 ,0x01 ,0x00 ,0x65 ,0x03};

unsigned char Do_GET_LIGHT_VAL  	[] = {0x02 ,0x0A ,0x00 ,0x44 ,0xDF ,0xDE ,0x12 ,0x00 ,0x65 ,0x03};
unsigned char Do_GET_DATE  	        [] = {0x02 ,0x0A ,0x00 ,0x47 ,0xDF ,0xDE ,0x10 ,0x00 ,0x65 ,0x03};




unsigned char Do_DPC_TRIM	  	[] = {0x02 ,0x0A ,0x00 ,0x44 ,0xDF ,0xDE ,0x14 ,0x00 ,0x66 ,0x03};
unsigned char Get_DPC_TRIM	  	[] = {0x02 ,0x0A ,0x00 ,0x47 ,0xDF ,0xDE ,0x14 ,0x00 ,0x66 ,0x03};






//unsigned char Get_PCD_serial_number	[] = {0x02 ,0x09 ,0x00 ,0x3D ,0xDF ,0x4D ,0x00 ,0xA4 ,0x03};  
unsigned char Get_PCD_serial_number	[] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01 ,0x24, 0x3A , 0x03};  

		
unsigned char Get_SN_FW 		[] = {0x02 ,0x0D ,0x00 ,0x3D ,0xDF ,0x4E ,0x01 ,0x01 ,0xDF ,0x4D ,0x00 ,0x31 ,0x03 };
unsigned char Do_RESET_PCD	  	[] = {0x02 ,0x09 ,0x00 ,0x3E ,0xDF ,0x19 ,0x00 ,0x00 ,0x03};




unsigned char GET_0x00   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x00 ,0x3E ,0x03 };
unsigned char GET_0x20   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x20 ,0x3E ,0x03 };
unsigned char GET_0x21   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x21 ,0x3E ,0x03 };
unsigned char GET_0x22   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x22 ,0x3E ,0x03 };
unsigned char GET_0x23   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x23 ,0x3E ,0x03 };
unsigned char GET_0x24   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x24 ,0x3E ,0x03 };
unsigned char GET_0x25   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x25 ,0x3E ,0x03 };
unsigned char GET_0x28   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x28 ,0x3E ,0x03 };
unsigned char GET_0x40   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x40 ,0x3E ,0x03 };
unsigned char GET_0xXX   [] = {0x02 ,0x0B ,0x00 ,0x47 ,0xDF ,0xDE ,0x50 ,0x01,0x40 ,0x3E ,0x03 };





int EMV_SN		=	0;
int EMV_FW_VERSION	=	0;
int EMV_RFON		=	0;
int EMV_RFOFF		=	0;
int EMV_OUTPORT		= 	0;
int EMV_PORTVAL		= 	0;
int EMV_RESET_PCD	= 	0;


int EMV_CTRL_ON		= 	0;
int EMV_CTRL_OFF	= 	0;
int EMV_LIGHT_VAL	= 	0;

int EMV_DO_DPC_TRIM	= 	0;
int EMV_GET_DPC_TRIM	= 	0;


int  EMV_SET_DATE     	= 0 ;
//int  EMV_SET_REG 	= 0 ;
char EMV_SET_VALUE      [256];


int  EMV_GET_DATE     	= 0 ;


int  EMV_GET     	= 0 ;
int  EMV_GET_VAL 	= 0 ;


int  EMV_DO_BOARD_ID      = 0 ;
int  EMV_DO_ANTENNA_ID    = 0 ;
int  EMV_DO_PRODUCT_ID    = 0 ;



char EMV_GET_STRING    [256];
char RESPONSE_TYPE;
int  RESPONSE_LEN;

extern void show_syntax(void);
//=============================================================================================================================
char completeVersion[50];

char *Build_dateTime(void)
{
	char aTmp[100];
	int Month = 0;
 	int Year  = 0;
 	int Day   = 0;

	sprintf(aTmp,"%s",__DATE__);

	if(memcmp(aTmp,"Jan",3)==0) Month=1;
	if(memcmp(aTmp,"F"  ,1)==0) Month=2;
	if(memcmp(aTmp,"Mar",3)==0) Month=3;
	if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
	if(memcmp(aTmp,"May",3)==0) Month=5;
	if(memcmp(aTmp,"Jun",3)==0) Month=6;
	if(memcmp(aTmp,"Jul",3)==0) Month=7;
	if(memcmp(aTmp,"Au" ,2)==0) Month=8;
	if(memcmp(aTmp,"S"  ,1)==0) Month=9;
	if(memcmp(aTmp,"O"  ,1)==0) Month=10;
	if(memcmp(aTmp,"N"  ,1)==0) Month=11;
	if(memcmp(aTmp,"D"  ,1)==0) Month=13;
	

	aTmp[6]=0;
	Day  = atoi(&aTmp[4]);
	Year = atoi(&aTmp[7]);

	sprintf(completeVersion,"%02d/%02d/%04d %s"
		,Day
		,Month
		,Year
		,__TIME__);

	return(completeVersion);
}

//===========================================================================================================
void show_syntax(void)
{
	printf("\nemvci (%s)\n\r",Build_dateTime() );
	printf("usage: emvci <option> \n\r");
	printf("Option:\n\r");
	printf("    -debug     : Enable Debug mode \n\r");
	printf("    -sn        : Get serial number		\n\r");
	printf("    -v         : Get firmware version	\n\r");
	printf("    -ctrl_on   : Get   emv3000 control\n\r");
	printf("    -ctrl_off     : Clear emv3000 control\n\r");
	printf("    -do_dpc_trim  : antenna setup \n\r");
	printf("    -get_dpc_trim : antenna setup \n\r");
	printf("    -o            : Set Outport\n\r");
	printf("    		      -LED1 \n\r");
	printf("                      -LED2 \n\r");
	printf("                      -LED3 \n\r");
	printf("                      -LED4 \n\r");
	printf("                      -BUZ  \n\r");
	printf("    -l         : Get Light Sensor Value \n\r");
	printf("    -b <sn>    : Write Board Id \n\r");
	printf("    -a <sn>    : Write Antenna Id \n\r");
	printf("    -p <sn>    : Write Product Id \n\r");
	printf("    -reset     : Reset PCD	\n\r");
	printf("    -rfon      : Set RF on	\n\r");
	printf("    -rfoff     : Set RF off \n\r");
	printf("    -get <xx>  : Get versions \n\r");
	printf("                  00 : Protocol version \n\r" 	);
	printf("                  20 : cpu id \n\r"		);
	printf("                  21 : Silicon SN \n\r"		);
	printf("                  22 : Main Board SN \n\r"	);
	printf("                  23 : Antenna SN \n\r"		);
	printf("                  24 : Product SN \n\r"		);
	printf("                  25 : Light Sensor type \n\r"	);
	printf("                  26 : SAM info \n\r"	        );
	printf("                  28 : Firmware version \n\r"	);
	printf("    -sdate <xx>:  set date (YYYYMMDDHHMMSS)\n\r");
	printf("    -gdate 	: Get Date \n\r"		);

}
//===========================================================================================================
char aValue   		[100];
char aBoardId   	[100];
char aAntennaId 	[100];
char aProductId 	[100];
char emv_sn     	[100];
char emv_fw     	[100];
char aLightSensorValue  [100];

int  emv_rf 	 = 0;
int  emv_outport = 0;
int  nocmd	 = 0;
int  emv_version = 0;

unsigned char tmp[256];


int file_exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}


#define DEV_OTI     "/dev/oti"
#define DEV_EMV3000 "/dev/emv3000"

void set_device_name(void)
{
	if( file_exists ( DEV_EMV3000  ) == 1 )
	{
		sprintf( devname, "%s" , DEV_EMV3000);
		return;
	}

	if( file_exists ( DEV_OTI  ) == 1 )
	{
		sprintf( devname, "%s" , DEV_OTI );
		return;
	}
	printf("\nemvci (%s)\n\r",Build_dateTime() );
	printf("device %s and %s doesn't exist ! \n\r\n\r",DEV_OTI,DEV_EMV3000 );
	exit(-1);
}

char aDate[50];




char ani_aTmp[32];
int ani ( char *s ,int len)
{
	if ( len > 9 ) return (0);

	memcpy(ani_aTmp,s,len);
	ani_aTmp[len]=0;

	return(atoi(ani_aTmp));
}




#define HI_NIBBLE(b) (((b) >> 4) & 0x0F)
#define LO_NIBBLE(b) ((b) & 0x0F)

char BcdToDecimal(char bcd){
    return (char)((HI_NIBBLE(bcd)*10)+(LO_NIBBLE(bcd)));
}

char DecimalToBcd(char decimal){
    return (char) ((decimal / 10)*16)+(decimal % 10);
}



short int DPC_XI    = 0;
unsigned short int REF_AGC   = 0;
unsigned short int CALC_AGC  = 0;
char DPC_str[20];


int main ( int argc, char **argv ) 
{
	int i;
	int c;
	char * p;
	int retry 	= 3;
	
	set_device_name();

	emv_sn [0] = 0;
	emv_fw [0] = 0;



	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		//printf("%d : %s\n\r",i,p);

		if( p[0] != '-')break;

 
		if(memcmp(p,"-debug"	   , 5)==0){ Debug 				= 1; DebugTXRX =1;}
		if(memcmp(p,"-v"   	   , 2)==0){ EMV_FW_VERSION  		= 1; nocmd++;}
		if(memcmp(p,"-sn"     	   , 3)==0){ EMV_SN   			= 1; nocmd++;}
		if(memcmp(p,"-rfon"   	   , 4)==0){ EMV_RFON  			= 1; nocmd++;}
		if(memcmp(p,"-rfoff"   	   , 5)==0){ EMV_RFOFF 			= 1; nocmd++;}
		if(memcmp(p,"-reset"   	   , 5)==0){ EMV_RESET_PCD 		= 1; nocmd++;}
		if(memcmp(p,"-ctrl_on"     , 8)==0){ EMV_CTRL_ON 		= 1; nocmd++;}
		if(memcmp(p,"-ctrl_off"    , 9)==0){ EMV_CTRL_OFF 		= 1; nocmd++;}
		if(memcmp(p,"-do_dpc_trim" ,12)==0){ EMV_DO_DPC_TRIM 		= 1; nocmd++;}
		if(memcmp(p,"-get_dpc_trim",13)==0){ EMV_GET_DPC_TRIM 		= 1; nocmd++;}





		if(memcmp(p,"-get" ,4)==0 && (argc > i+1))
		{ 
			nocmd++;
			EMV_GET     = 1; 
			EMV_GET_VAL = atoi(argv[i+1]);
			EMV_GET_VAL = (int)strtol(argv[i+1], NULL, 16);
		}

		if(memcmp(p,"-sdate" ,6)==0 && (argc > i+1))  // REG + VAL
		{ 

			if(strlen(argv[i+1]) != 14 )
			{
				printf(" Bad date format (YYYYMMDDHHMMSS)\n\r");
				continue;
			}

			nocmd++;
			EMV_SET_DATE     = 1; 

			memcpy (  aDate , argv[i+1] , strlen(argv[i+1]) );
			for ( c = 0 ; c  < 7  ; c++)
			{
				EMV_SET_VALUE[c] = DecimalToBcd( ani(&aDate[c*2],2));
			}
			EMV_SET_VALUE[8] = 0 ; 
		}
 
		
		
		if(memcmp(p,"-b" ,2)==0 && (argc > i+1))
		{
 			nocmd++;
			memcpy (  aBoardId, argv[i+1] , strlen(argv[i+1]) );
			i++;
			EMV_DO_BOARD_ID = 1;
		}

		if(memcmp(p,"-gdate" ,6)==0 ) 
		{
			EMV_GET_DATE = 1;
			nocmd++;
		}


		if(memcmp(p,"-l" ,2)==0 )
		{
 			nocmd++;
			i++;
			EMV_LIGHT_VAL = 1;
		}

		if(memcmp(p,"-a" ,2)==0 && (argc > i+1))
		{
  			nocmd++;
			memcpy ( aAntennaId ,  argv[i+1] , strlen(argv[i+1]) );
			i++;
			EMV_DO_ANTENNA_ID = 1;
		}

		if(memcmp(p,"-p" ,2)==0 && (argc > i+1))
		{
  			nocmd++;
			memcpy ( aProductId , argv[i+1] , strlen(argv[i+1]) );
			i++;
			EMV_DO_PRODUCT_ID = 1;
		}


		if(memcmp(p,"-o" ,2)==0 )
//		if(memcmp(p,"-o" ,2)==0 && (argc > i+1))
		{ 
			EMV_PORTVAL = 0x03 ;
			/*
			EMV_PORTVAL = atoi( argv[i+1] );
			if( EMV_PORTVAL < 0 || EMV_PORTVAL >31)
			{
				printf("Invalid port value:%d  range:(0-31)\n\r",EMV_PORTVAL);
				exit(0);
			}
			*/
			EMV_OUTPORT  = 1; 
			nocmd++;
		}

		if( EMV_OUTPORT )
		{
			// if(memcmp(p,"-BUZ_CTRL" ,9)==0){ EMV_PORTVAL |= 0x01; nocmd++;}
			// if(memcmp(p,"-LED_CTRL" ,9)==0){ EMV_PORTVAL |= 0x02; nocmd++;}
			if(memcmp(p,"-BUZ" 	,4)==0){ EMV_PORTVAL |= 0x08; nocmd++;}
			if(memcmp(p,"-LED1" 	,5)==0){ EMV_PORTVAL |= 0x10; nocmd++;}
			if(memcmp(p,"-LED2" 	,5)==0){ EMV_PORTVAL |= 0x20; nocmd++;}
			if(memcmp(p,"-LED3" 	,5)==0){ EMV_PORTVAL |= 0x40; nocmd++;}
			if(memcmp(p,"-LED4" 	,5)==0){ EMV_PORTVAL |= 0x80; nocmd++;}
		}

		//if(nocmd) break;

		
	}

	if(nocmd==0)
	{
		show_syntax();
		return(-1);
	}


	if(EMV_OUTPORT)
	{
		printf( "Set  L1=%d   L2=%d   L3=%d L4=%d   BUZ=%d  Dummy:%d  LED_CTRL:%d  BUZ_CTRL:%d\n\r",
				(EMV_PORTVAL & 0x10)?1:0 ,
				(EMV_PORTVAL & 0x20)?1:0 ,
				(EMV_PORTVAL & 0x40)?1:0 ,
				(EMV_PORTVAL & 0x80)?1:0 ,
				
				(EMV_PORTVAL & 0x08)?1:0 ,
				(EMV_PORTVAL & 0x04)?1:0 ,
				(EMV_PORTVAL & 0x02)?1:0 ,
				(EMV_PORTVAL & 0x01)?1:0	
		);
	}


	devfd=openPort(devname,TheSelectedBaudrate);
	if(devfd == -1)
	{
		printf("\n open() failed with error [%s]\n",strerror(errno));
		return -1;
	}
	
	nothing = 0;
	
	while(1)
	{
		RxLoop();

		if( Command_rejected ) break;

		if( EMV_DO_BOARD_ID && nothing == 0)	
		{ 
			Send_do_Command ( 'M' , aBoardId ); 
		}

		if( EMV_DO_ANTENNA_ID && nothing == 0)	
		{ 
			Send_do_Command ( 'A' , aAntennaId ); 
		}

		if( EMV_DO_PRODUCT_ID && nothing == 0)	
		{ 
			Send_do_Command ( 'P' , aProductId ); 
		}


		if( EMV_RFOFF && nothing == 0)	
		{ 
			SendCommand ( Do_RF_OFF ); 
		}


		if( EMV_CTRL_ON && nothing == 0)	
		{ 
			SendCommand ( Do_CONTROL_ON ); 
		}


		if( EMV_CTRL_OFF && nothing == 0)	
		{ 
			SendCommand ( Do_CONTROL_OFF ); 
		}


		if( EMV_DO_DPC_TRIM  && nothing == 0)	
		{ 
			SendCommand ( Do_DPC_TRIM ); 
		}

		if( EMV_GET_DPC_TRIM  && nothing == 0)	
		{ 
			SendCommand ( Get_DPC_TRIM ); 
		}


		if( EMV_RESET_PCD && nothing == 0)
		{
			SendCommand ( Do_RESET_PCD );
		}

		if( EMV_RFON && nothing == 0 ) 
		{ 
			SendCommand ( Do_RF_ON  ); 
		}

		if(EMV_FW_VERSION && nothing == 0)
		{
			SendCommand( Get_FW_Version );
		}

		if(EMV_SN && nothing == 0)
		{
			SendCommand( Get_PCD_serial_number );
		}



		if( EMV_GET_DATE && nothing == 0)	
		{ 
			SendCommand ( Do_GET_DATE ); 
		}



		if(EMV_OUTPORT && nothing == 0  )
		{
			memcpy(tmp,Do_OUTPORT_ALL_OFF,Do_OUTPORT_ALL_OFF[1]);
			tmp[7]=EMV_PORTVAL;

			SendCommand( tmp ); 
			usleep(10000);
		}



		if( EMV_SET_DATE && nothing == 0)	
		{ 
			Send_set_Command ( 0x10 , EMV_SET_VALUE ); 
		}

		if(EMV_LIGHT_VAL && nothing == 0)
		{
			SendCommand( Do_GET_LIGHT_VAL );
		}



		if(EMV_GET && (emv_version == 0) && (nothing == 0))
		{

			EMV_GET_STRING[0]=0;
			GET_0xXX [8] = EMV_GET_VAL & 0x0ff;

			switch (EMV_GET_VAL)
			{

				case 0x00: SendCommand( GET_0x00   ); 	break ; // Protocol version
				case 0x20: SendCommand( GET_0x20   ); 	break ; // cpu id
				case 0x21: SendCommand( GET_0x21   ); 	break ; // siliconS N
				case 0x22: SendCommand( GET_0x22   );	break ; // Main Board SN
				case 0x23: SendCommand( GET_0x23   );	break ; // Antenna SN
				case 0x24: SendCommand( GET_0x24   );	break ; // Product SN
				case 0x25: SendCommand( GET_0x25   );	break ; // Light Sensor type
				case 0x28: SendCommand( GET_0x28   );	break ; // Firmware version
				default  : SendCommand( GET_0xXX   );	break ; // General purpose
			}
		}
	
		if( emv_version )
		{
			//printf(" RESPONSE_TYPE =  0x%02X\n\r",RESPONSE_TYPE);
			
			if( RESPONSE_TYPE == 0x12 )
			{
				printf("Light sensor value :%d\n\r",EMV_GET_STRING[0]); 
				return(0) ;
			}

			if( RESPONSE_TYPE == 0x14 )
			{

				DPC_XI   = (EMV_GET_STRING[0] << 8) + EMV_GET_STRING[1];
				REF_AGC  = (EMV_GET_STRING[2] << 8) + EMV_GET_STRING[3];
				CALC_AGC = (EMV_GET_STRING[4] << 8) + EMV_GET_STRING[5];

				if(DPC_XI ==0 ) 
				{
					sprintf( DPC_str, "RED");
				}
				else if(DPC_XI <= -127 || DPC_XI >= +127 ) 
				{
					sprintf( DPC_str, "RED");
				}
				else if( (DPC_XI >= -80) && (DPC_XI <= 80) ) 
				{
					sprintf( DPC_str, "GREEN");
				}
				else
				{
					sprintf( DPC_str, "ORANGE");
				}
				

				/*
				printf("DPC_TRIM : %02X %02X %02X %02X %02X %02X\n\r"
						,EMV_GET_STRING[0]
						,EMV_GET_STRING[1]
						,EMV_GET_STRING[2]
						,EMV_GET_STRING[3]
						,EMV_GET_STRING[4]
						,EMV_GET_STRING[5]
				); 
				*/

				
				printf("DPC_TRIM=DPC_XI:%d:REF_AGC:%d:CALC_AGC:%d:%s:\n\r"
						,DPC_XI
						,REF_AGC
						,CALC_AGC
						,DPC_str
				); 

				return(0) ;
			}

			if( RESPONSE_TYPE == 0x10  )
			{
				if( RESPONSE_LEN )
				{
					printf("Date&Time:%02X%02X:%02X:%02X:%02X:%02X:%02X\n\r"
						,EMV_GET_STRING[0]
						,EMV_GET_STRING[1]
						,EMV_GET_STRING[2]
						,EMV_GET_STRING[3]
						,EMV_GET_STRING[4]
						,EMV_GET_STRING[5]
						,EMV_GET_STRING[6]
						); 
				}
				return(0) ;
			}

			switch (EMV_GET_VAL)
			{

				case 0x00: printf("Protocol version:%s \n\r"	, EMV_GET_STRING); return(0) ;
				case 0x20: printf("cpu id:%s \n\r"		, EMV_GET_STRING); return(0) ;
				case 0x21: printf("Silicon SN:%s \n\r"		, EMV_GET_STRING); return(0) ;
				case 0x22: printf("Mainboard SN:%s \n\r"	, EMV_GET_STRING); return(0) ;
				case 0x23: printf("Antenna SN:%s \n\r"		, EMV_GET_STRING); return(0) ;
				case 0x24: printf("Product SN:%s \n\r"		, EMV_GET_STRING); return(0) ;
				case 0x25: printf("Light Sensor type:%s \n\r"	, EMV_GET_STRING); return(0) ;
				case 0x26: printf("SAM info:%s \n\r"		, EMV_GET_STRING); return(0) ;
				case 0x28: printf("Firmware version:%s \n\r"	, EMV_GET_STRING); return(0) ;
				default  : printf("Command result:%s \n\r"	, EMV_GET_STRING); return(0) ;
			}
			return(-1);
		}



		if( strlen(emv_sn) > 5 )
		{
			printf("serialnr='00%s'\n\r",emv_sn);
			break;
		}

		if( strlen(emv_fw) > 5 )
		{
			printf("version='%s'\n\r",emv_fw);
			break;
		}

		if(emv_rf)
		{
			break;
		}

		if(emv_outport)
		{
			break;
		}


		if(nothing > 3000000 )
		{
			retry--;

			if( retry <= 0 ) 
			{
				printf("command failed !\n\r");
				break;
			}

			nothing = 0;
			continue;
		}
		nothing+=1000;
		usleep(1000);
	}
	closePort();
	return -1;
}






