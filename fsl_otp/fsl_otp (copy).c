#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>    
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/usbdevice_fs.h>
#include <termios.h>
#include <time.h>
#include <linux/watchdog.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>


extern void treat_HW_OCOTP_TESTER3	(int value);
extern int hextoi	( char *hex  );
extern int read_otp	( char *aReg );
extern void show_help	( void );
extern int is_mx7    	( void );
extern int is_imx8mm 	( void );
extern int is_imx8mq 	( void );
extern int is_imx8mp 	( void );
extern int is_imx8mn 	( void );


//=============================================================================================
void show_help(void)
{
	printf("\n\rfls_otp jgd@v2021.05.04\n\r");
	printf("Options:\n\r");
	printf("    -h                  : This info		 \n\r");
	printf("\n\r");
}
//=============================================================================================
int hextoi( char *hex )
{
	int number = 0, pos = strlen(hex), mult = 1;

	for( ; pos+1; pos-- )
	{
	 if( (hex[pos] >= 'a') && (hex[pos] <= 'f') ) hex[pos] = toupper( hex[pos] );
	 if( ((hex[pos] >= '0') && (hex[pos] <= '9')) )
	  { number += (hex[pos]-'0') * mult; mult *= 16; }
	 if( ((hex[pos] >= 'A') && (hex[pos] <= 'F')) )
	  { number += (hex[pos]-'7') * mult; mult *= 16; }
	}

	return number;
}
//=============================================================================================
char	arr[100];
char 	afilename[100];
char 	afsl_otp[]="/sys/fsl_otp"; 

int read_otp(char *aReg)
{
	int fd;
	int size;
	int val;


	DIR* dir = opendir(afsl_otp);
	if(dir)
	{
		closedir(dir);
	}
	else
	{
		printf(" folder : %s do not exist \n\r",afsl_otp);
		exit(0);
	}
	
	sprintf(afilename,"/sys/fsl_otp/%s",aReg);
	usleep(100000);

	fd = open(afilename, O_RDWR| O_NOCTTY | O_SYNC);
	if (fd < 0) {
		printf("Error opening OTP register :%s\n\r",aReg);
		return -1;
	}
	
	size=read(fd, arr, 10);
	arr[size]=0;

	val = hextoi(&arr[2]);
	
	printf("%s ( 0x%08X ) \n",aReg,val);




	close(fd);
	return val;
}
//=============================================================================================










//=============================================================================================
char HW_OCOTP_TESTER3[]="HW_OCOTP_TESTER3";

#define OCOTP_TESTER3_TEMP_SHIFT	6
#define OCOTP_TESTER3_SPEED_SHIFT	8




/* CPU Temperature Grades */
#define TEMP_COMMERCIAL         0
#define TEMP_EXTCOMMERCIAL      1
#define TEMP_INDUSTRIAL         2
#define TEMP_AUTOMOTIVE         3



enum cpu_speed {
	OCOTP_TESTER3_SPEED_GRADE0,
	OCOTP_TESTER3_SPEED_GRADE1,
	OCOTP_TESTER3_SPEED_GRADE2,
	OCOTP_TESTER3_SPEED_GRADE3,
	OCOTP_TESTER3_SPEED_GRADE4,
};


int is_mx7    (void)  { return 0; }
int is_imx8mm (void)  { return 1; }
int is_imx8mq (void)  { return 0; }
int is_imx8mp (void)  { return 0; }
int is_imx8mn (void)  { return 0; }

void treat_HW_OCOTP_TESTER3(int value)
{
	int minc;
	int maxc;
	int val;
	int speed=0;
	
	/*
	* OCOTP_TESTER3[7:6] (see Fusemap Description Table offset 0x440)
	* defines a 2-bit SPEED_GRADING
	*/
	val = value;
	val >>= OCOTP_TESTER3_TEMP_SHIFT;
	val &= 0x3;

	if (val == TEMP_AUTOMOTIVE) {
		minc = -40;
		maxc = 125;
		printf("Temperature grade : Automotive ( %d°C  %d°C ) \n", minc , maxc );
	} else if (val == TEMP_INDUSTRIAL) {
		minc = -40;
		maxc = 105;
		printf("Temperature grade : Industrial ( %d°C  %d°C ) \n", minc , maxc );
	} else if (val == TEMP_EXTCOMMERCIAL) {
		minc = -20;
		maxc = 105;
		printf("Temperature grade : Extended Commercial ( %d°C  %d°C ) \n", minc , maxc );
	} else {
		minc = 0;
		maxc = 95;
		printf("Temperature grade : Commercial ( %d°C  %d°C ) \n", minc , maxc );
	}



	/*
	 * OCOTP_TESTER3[9:8] (see Fusemap Description Table offset 0x440)
	 * defines a 2-bit SPEED_GRADING
	 */


	val = value;
	val >>= OCOTP_TESTER3_SPEED_SHIFT;

	if (is_imx8mn() || is_imx8mp()) {
		val &= 0xf;
		speed=(2300000000 - val * 100000000);
	}

	if (is_imx8mm())
		val &= 0x7;
	else
		val &= 0x3;

	switch(val) {
	case OCOTP_TESTER3_SPEED_GRADE0:
		speed= 800000000;
		break;
	case OCOTP_TESTER3_SPEED_GRADE1:
		speed=(is_mx7() ? 500000000 : (is_imx8mq() ? 1000000000 : 1200000000));
		break;
	case OCOTP_TESTER3_SPEED_GRADE2:
		speed=  (is_mx7() ? 1000000000 : (is_imx8mq() ? 1300000000 : 1600000000));
		break;
	case OCOTP_TESTER3_SPEED_GRADE3:
		speed=  (is_mx7() ? 1200000000 : (is_imx8mq() ? 1500000000 : 1800000000));
		break;
	case OCOTP_TESTER3_SPEED_GRADE4:
		speed=  2000000000;
		break;
	}

	printf("CPU  speed  grade : %d MHz \n",  speed /1000000 );




	return ;
}





//=============================================================================================



int main ( int argc, char **argv ) 
{
	int i;
	char * p;
	int val;

	for(i=0 ; i < argc ; i++)
	{
		p=argv[i];

		if(memcmp(p,"-h"     	, 2 ) == 0)
		{ 
			show_help();
		}
	}


	printf("\n");

	val = read_otp(HW_OCOTP_TESTER3);
	treat_HW_OCOTP_TESTER3(val);
	
	return(0);
}




