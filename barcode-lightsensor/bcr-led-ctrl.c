/*ts:4*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>

int Debug=0;

//extern int  Send_to_N3680			(unsigned char *aCmd, char *aResponse,int RespLen);
extern int  Send_N3680   			( unsigned char *prefix , unsigned char *aCmd, char *Description,char *aResponse,int MaxRespLen);
extern void N3680_settings			( void );
extern int  ReadDataOneLine			( char *ifile,char *data,int MaxLen);
extern void Flush_Buffer			( void );

extern int  Read_ConfigFile			( void );
extern void treat_config_line			( char *s);
extern int  IsBarcodeInitialized		( void );
extern int  Send_Command			( char *cmd,char *description,int retries);
extern void store 				( unsigned char c );
extern void logfile				( char *s,int len );
extern void ClearLogfile			( void );
extern int  set_interface_attribs 		( int fd, int speed, int parity );
extern void set_blocking 			( int fd, int should_block ) ;
extern void BARCO_Settings			( void );
extern int  BARCO_ProductInfo			( void );
extern int  StoreInitializationDate		( void );
extern void ShowRecord				( void );

extern int IsRackindaPresent(void);
extern int Get_Rakinda_SN (void);
extern int Get_Rakinda_ESN (void);
extern int Get_Rakinda_version (void);


extern int WriteValue				( char *ifile , char *data );
extern int ReadValue				( char *ifile , char *data , int MaxLen);
extern int Send_Command_bin			( char *cmd   , int len,char *description,int retries);




char 	portname[] 	= "/dev/barcode";
char 	init_filename[] = "/etc/rakinda-info/device_initialized.log";
char 	config_file[] 	= "/etc/rakinda-info/rakinda-info.cfg";
char 	c_sn[100] 	= "";
int 	fd ;


unsigned char Prefix_triggger  [3] = { 0x16 , 't' , 0x0d };
unsigned char Prefix_untrigger [3] = { 0x16 , 'u' , 0x0d };
unsigned char Prefix_request   [3] = { 0x16 , 'y' , 0x0d };
unsigned char Prefix_cmd       [3] = { 0x16 , 'm' , 0x0d };


int ENABLE_READING_ALL_CODE		= 0;
int ENABLE_READING_ALL_1D		= 0;
int ENABLE_READING_ALL_2D		= 0;
int ENABLE_CODE_128			= 0;
int ENABLE_UCC_EAN_8			= 0;
int ENABLE_EAN_13			= 0;
int ENABLE_UPC_E			= 0;
int ENABLE_UPC_A			= 0;
int ENABLE_INTERLEAVED_2_OF_5		= 0;
int ENABLE_MATRIX_2_OF_5		= 0;
int ENABLE_CODE_39			= 0;
int ENABLE_CODABAR			= 0;
int ENABLE_CODE_93			= 0;
int ENABLE_UCC_EAN_128			= 0;
int ENABLE_GS1_DATABAR			= 0;
int ENABLE_EAN_UCC_COMPOSITE		= 0;
int ENABLE_CODE_11			= 0;
int ENABLE_ISBN			        = 0;
int ENABLE_INDUSTRIAL_25	        = 0;
int ENABLE_STANDARD_25			= 0;
int ENABLE_PLESSEY			= 0;
int ENABLE_MSI_PLESSEY			= 0;
int ENABLE_PDF417			= 0;
int ENABLE_QR_CODE			= 0;
int ENABLE_AZTEC			= 0;
int ENABLE_DATA_MATRIX			= 0;
int ENABLE_MAXICODE			= 0;
int ENABLE_CHINESE_SENSIBLE_CODE	= 0;




unsigned char power_up_beep_off      []  = "BEPPWR0."; // Request Current value
unsigned char power_up_beep_on       []  = "BEPPWR1."; // Request available value

unsigned char good_read_off          []  = "BEPBEP0."; // Request available value
unsigned char good_read_on 	     []  = "BEPBEP1."; // Request available value

unsigned char beeper_vol_off 	     []  = "BEPLVL0."; 
unsigned char beeper_vol_low 	     []  = "BEPLVL1."; 
unsigned char beeper_vol_medium      []  = "BEPLVL2."; 
unsigned char beeper_vol_high 	     []  = "BEPLVL3."; 

unsigned char good_read_delay_0	     []  = "DLYGRD0."; 
unsigned char good_read_delay_500    []  = "DLYGRD500."; 
unsigned char good_read_delay_1000   []  = "DLYGRD1000."; 
unsigned char good_read_delay_1500   []  = "DLYGRD1500."; 

unsigned char Illumination_low       []  = "PWRNOL50."; 
unsigned char Illumination_high      []  = "PWRNOL150."; 


//unsigned char PresentationMode     []  = "PAPPST."; 
unsigned char PresentationMode      []  = "TRGMOD3."; 

unsigned char Illumination_off      []  = "PWRIDL0."; 
unsigned char Illumination_on       []  = "PWRIDL50."; 

unsigned char aSernum[]      = "SERNUM?.";



//=====================================================================================================================
int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
//      tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
        tty.c_cc[VTIME] = 1;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf ("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}
//=====================================================================================================================
void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
  //    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
        tty.c_cc[VTIME] = 1;            // 0.2 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf ("error %d setting term attributes", errno);
}
#define MAX_LEN 2000
char buf [MAX_LEN+10];
int  Offset = 0 ;
//=====================================================================================================================
void store (unsigned char c)
{

	if( (c == 0x0a || c == 0x0d)  )
	{
		if(Offset)
		{	
			logfile(buf,Offset);
			buf[0]=0;
			Offset=0;
		}
		return;
	}

	if( Offset < MAX_LEN )
	{
		buf[Offset]=c;
		Offset++;
		buf[Offset]=0;
	}
}
//=====================================================================================================================
int Send_Command(char *cmd,char *description,int retries)
{
	int len;
	int timeout = 3000 ;
	int n;
	unsigned char c;
	int r = retries;
	
	len = strlen(cmd);
	int previous=0;


	while(1)
	{
		//----------------------------------------------------
		// Send Command
		//----------------------------------------------------
		write (fd, cmd , len);    
		usleep (1000000);

		//----------------------------------------------------
		// Waiting for replay ( Ack / Nack )
		//----------------------------------------------------
		while(1)
		{
			n = read (fd, &c, 1);  // read up to 100 characters if ready to read

			if(n)
			{
				if ( c == 0x06 )
				{
					printf("%s (ACK)\n\r",description);
					return 0;
				}
				if ( c == 0x15 )
				{
					printf("%s (NACK)\n\r",description);
					break;
				}
				store(c);
				if( c == 0x0a) 
				{
					if(previous != 0x0a )
						printf("%c",c);
					previous = c;
				}
				else
				{
					printf("%c",c);
					previous=0;
				}
				timeout = 3000;
			}	
			

			if (timeout <=0 )
			{
				printf("%s (TIMEOUT)\n\r",description);
				break;
			}
			timeout-=100;
		}
		r--;
		if( r <= 0) break;
	}
	return 1;
}
//=====================================================================================================================
void ClearLogfile( void )
{
	FILE *fpt;
	fpt = fopen( "/tmp/rakinda.log" , "w" );
	fwrite("\n\r" , 1 , 2 , fpt );
	fclose(fpt);
}
//=====================================================================================================================
void logfile(char *s,int len)
{
	FILE *ftp2;

 	ftp2 = fopen( "/tmp/rakinda.log" , "a" );
	fwrite(s , 1 , len , ftp2 );
	fwrite("\n\r" , 1 , 2 , ftp2 );
	fclose(ftp2);

	if(memcmp(s,"S/N: ",5)==0 )
	{
		sprintf(c_sn,"%s",s);
	}
}
//=====================================================================================================================
void BARCO_Settings(void)
{
	int errors = 0;

	printf("BARCO_Setting()\n\r");

	if(IsBarcodeInitialized()==0)
	{
		errors+=Send_Command((char *)"nls0006010",(char *)"Code Programming ON          " ,3 );
		errors+=Send_Command((char *)"nls0302010",(char *)"Auto Mode                    " ,3 );
		errors+=Send_Command((char *)"nls0310020",(char *)"x0D x0a as Terminal          " ,3 );
		errors+=Send_Command((char *)"nls0309010",(char *)"Enable Terminal              " ,3 );


		errors+=Send_Command((char *)"nls0200000",(char *)"Illumination Normal          " ,3 );
//		errors+=Send_Command((char *)"nls0200020",(char *)"Illumination OFF             " ,3 );
//		errors+=Send_Command((char *)"nls0200010",(char *)"Illumination Always ON       " ,3 );

//		errors+=Send_Command((char *)"nls0312020",(char *)"Low sensitivity              " ,3 );
//		errors+=Send_Command((char *)"nls0312020",(char *)"Medium sensitivity           " ,3 );
//		errors+=Send_Command((char *)"nls0312020",(char *)"High sensitivity             " ,3 );
//		errors+=Send_Command((char *)"nls0312020",(char *)"Enhanced sensitivity         " ,3 );

//		errors+=Send_Command((char *)"nls0313150",(char *)"Normal Illuminace            " ,3 );
//		errors+=Send_Command((char *)"nls0313151",(char *)"High Illuminance             " ,3 );



		
		if ( ENABLE_READING_ALL_CODE	  ) errors+=Send_Command((char *)"nls0001020",(char *)"Enable reading all code      " ,3 );
		if ( ENABLE_READING_ALL_1D	  ) errors+=Send_Command((char *)"nls0001040",(char *)"ENABLE_READING_ALL_1D        " ,3 );
		if ( ENABLE_READING_ALL_2D	  ) errors+=Send_Command((char *)"nls0001060",(char *)"ENABLE_READING_ALL_2D        " ,3 );
		//
		if ( ENABLE_CODE_128		  ) errors+=Send_Command((char *)"nls0400000",(char *)"LOAD_CODE_128 DEFAULT        " ,3 );
		if ( ENABLE_CODE_128		  ) errors+=Send_Command((char *)"nls0400020",(char *)"ENABLE_CODE_128              " ,3 );
		//
		if ( ENABLE_UCC_EAN_8		  ) errors+=Send_Command((char *)"nls0401000",(char *)"LOAD_UCC_EAN_8 DEFAULT       " ,3 );
		if ( ENABLE_UCC_EAN_8		  ) errors+=Send_Command((char *)"nls0401020",(char *)"ENABLE_UCC_EAN_8             " ,3 );
		//
		if ( ENABLE_EAN_13		  ) errors+=Send_Command((char *)"nls0402000",(char *)"LOAD_EAN_13 DEFAULT          " ,3 );
		if ( ENABLE_EAN_13		  ) errors+=Send_Command((char *)"nls0402020",(char *)"ENABLE_EAN_13                " ,3 );
		//
		if ( ENABLE_UPC_E		  ) errors+=Send_Command((char *)"nls0403000",(char *)"LOAD_UPC_E                   " ,3 );
		if ( ENABLE_UPC_E		  ) errors+=Send_Command((char *)"nls0403020",(char *)"ENABLE_UPC_E                 " ,3 );
		//
		if ( ENABLE_UPC_A		  ) errors+=Send_Command((char *)"nls0404000",(char *)"LOAD_UPC_A                   " ,3 );
		if ( ENABLE_UPC_A		  ) errors+=Send_Command((char *)"nls0404020",(char *)"ENABLE_UPC_A                 " ,3 );
		//
		if ( ENABLE_INTERLEAVED_2_OF_5	  ) errors+=Send_Command((char *)"nls0405000",(char *)"LOAD_INTERLEAVED_2_OF_5      " ,3 );
		if ( ENABLE_INTERLEAVED_2_OF_5	  ) errors+=Send_Command((char *)"nls0405020",(char *)"ENABLE_INTERLEAVED_2_OF_5    " ,3 );
		//
		if ( ENABLE_MATRIX_2_OF_5	  ) errors+=Send_Command((char *)"nls0406000",(char *)"LOAD_MATRIX_2_OF_5           " ,3 );
		if ( ENABLE_MATRIX_2_OF_5	  ) errors+=Send_Command((char *)"nls0406020",(char *)"ENABLE_MATRIX_2_OF_5         " ,3 );
		//
		if ( ENABLE_CODE_39		  ) errors+=Send_Command((char *)"nls0408000",(char *)"LOAD_CODE_39                 " ,3 );
		if ( ENABLE_CODE_39		  ) errors+=Send_Command((char *)"nls0408020",(char *)"ENABLE_CODE_39               " ,3 );
		//
		if ( ENABLE_CODABAR		  ) errors+=Send_Command((char *)"nls0409000",(char *)"LOAD_CODABAR                 " ,3 );
		if ( ENABLE_CODABAR		  ) errors+=Send_Command((char *)"nls0409020",(char *)"ENABLE_CODABAR               " ,3 );
		//
		if ( ENABLE_CODE_93		  ) errors+=Send_Command((char *)"nls0410000",(char *)"LOAD_CODE_93                 " ,3 );
		if ( ENABLE_CODE_93		  ) errors+=Send_Command((char *)"nls0410010",(char *)"ENABLE_CODE_93               " ,3 );
		//
		if ( ENABLE_UCC_EAN_128		  ) errors+=Send_Command((char *)"nls0412000",(char *)"LOAD_UCC_EAN_128             " ,3 );
		if ( ENABLE_UCC_EAN_128		  ) errors+=Send_Command((char *)"nls0412020",(char *)"ENABLE_UCC_EAN_128           " ,3 );
		//
		if ( ENABLE_GS1_DATABAR		  ) errors+=Send_Command((char *)"nls0413000",(char *)"LOAD_GS1_DATABAR             " ,3 );
		if ( ENABLE_GS1_DATABAR		  ) errors+=Send_Command((char *)"nls0413020",(char *)"ENABLE_GS1_DATABAR           " ,3 );
		//
		if ( ENABLE_EAN_UCC_COMPOSITE	  ) errors+=Send_Command((char *)"nls0414000",(char *)"LOAD_EAN_UCC_COMPOSITE       " ,3 );
		if ( ENABLE_EAN_UCC_COMPOSITE	  ) errors+=Send_Command((char *)"nls0414020",(char *)"ENABLE_EAN_UCC_COMPOSITE     " ,3 );
		//
		if ( ENABLE_CODE_11		  ) errors+=Send_Command((char *)"nls0415000",(char *)"LOAD_CODE_11                 " ,3 );
		if ( ENABLE_CODE_11		  ) errors+=Send_Command((char *)"nls0415020",(char *)"ENABLE_CODE_11               " ,3 );
		//
		if ( ENABLE_ISBN 		  ) errors+=Send_Command((char *)"nls0416000",(char *)"LOAD_ISBN                    " ,3 );
		if ( ENABLE_ISBN		  ) errors+=Send_Command((char *)"nls0416020",(char *)"ENABLE_ISBN                  " ,3 );
		//
		if ( ENABLE_INDUSTRIAL_25	  ) errors+=Send_Command((char *)"nls0417000",(char *)"LOAD_INDUSTRIAL_25           " ,3 );
		if ( ENABLE_INDUSTRIAL_25	  ) errors+=Send_Command((char *)"nls0417020",(char *)"ENABLE_INDUSTRIAL_25         " ,3 );
		//
		if ( ENABLE_STANDARD_25		  ) errors+=Send_Command((char *)"nls0418000",(char *)"LOAD_STANDARD_25             " ,3 );
		if ( ENABLE_STANDARD_25		  ) errors+=Send_Command((char *)"nls0418020",(char *)"ENABLE_STANDARD_25           " ,3 );
		//
		if ( ENABLE_PLESSEY		  ) errors+=Send_Command((char *)"nls0419000",(char *)"LOAD_PLESSEY                 " ,3 );
		if ( ENABLE_PLESSEY		  ) errors+=Send_Command((char *)"nls0419020",(char *)"ENABLE_PLESSEY               " ,3 );
		//
		if ( ENABLE_MSI_PLESSEY		  ) errors+=Send_Command((char *)"nls0420000",(char *)"LOAD_MSI_PLESSEY             " ,3 );
		if ( ENABLE_MSI_PLESSEY		  ) errors+=Send_Command((char *)"nls0420020",(char *)"ENABLE_MSI_PLESSEY           " ,3 );
		//
		if ( ENABLE_PDF417		  ) errors+=Send_Command((char *)"nls0501000",(char *)"LOAD_PDF417                  " ,3 );
		if ( ENABLE_PDF417		  ) errors+=Send_Command((char *)"nls0501020",(char *)"ENABLE_PDF417                " ,3 );
		//
		if ( ENABLE_QR_CODE		  ) errors+=Send_Command((char *)"nls0502000",(char *)"LOAD_QR_CODE                 " ,3 );
		if ( ENABLE_QR_CODE		  ) errors+=Send_Command((char *)"nls0502020",(char *)"ENABLE_QR_CODE               " ,3 );
		//
		if ( ENABLE_AZTEC		  ) errors+=Send_Command((char *)"nls0503000",(char *)"LOAD_AZTEC                   " ,3 );
		if ( ENABLE_AZTEC		  ) errors+=Send_Command((char *)"nls0503020",(char *)"ENABLE_AZTEC                 " ,3 );
		//
		if ( ENABLE_DATA_MATRIX		  ) errors+=Send_Command((char *)"nls0504000",(char *)"LOAD_DATA_MATRIX             " ,3 );
		if ( ENABLE_DATA_MATRIX		  ) errors+=Send_Command((char *)"nls0504020",(char *)"ENABLE_DATA_MATRIX           " ,3 );
		//
		if ( ENABLE_MAXICODE		  ) errors+=Send_Command((char *)"nls0505000",(char *)"LOAD_MAXICODE                " ,3 );
		if ( ENABLE_MAXICODE		  ) errors+=Send_Command((char *)"nls0505020",(char *)"ENABLE_MAXICODE              " ,3 );
		//
		if ( ENABLE_CHINESE_SENSIBLE_CODE ) errors+=Send_Command((char *)"nls0508000",(char *)"LOAD_CHINESE_SENSIBLE_CODE   " ,3 );
		if ( ENABLE_CHINESE_SENSIBLE_CODE ) errors+=Send_Command((char *)"nls0508020",(char *)"ENABLE_CHINESE_SENSIBLE_CODE " ,3 );
		//

	        errors+=Send_Command((char *)"nls0006000",(char *)"Code Programming OFF         " ,3 );

		printf("Detected errors : %d \n\r",errors);
		if(errors==0)
		{
			StoreInitializationDate();
		}
	}
}
//=====================================================================================================================
int BARCO_ProductInfo(void)
{
	int ret;
	ret=Send_Command((char *)"nls0003000",(char *)"Query Product Info           " ,1 );
	return(ret);
}
//=====================================================================================================================
void treat_config_line(char *s)
{
	int len;
//	char *ret;

	if ( s[0] == '#' ) 
		return;

	len = strlen(s);
	if(len==0) return ;	
	
	while( s[len-1] == 0x0a || s[len-1] == 0x0d || s[len-1] == ' ' )
	{
		if(len<=1) return ;		
		len --;
	}
	s[len]=0;	

	printf("	%s\n", s);


	/*
	ret= strstr( s , "=");ret++;

	if(strlen(ret) == 0)
		return;

	if(strstr( s , "ENABLE_READING_ALL"	)) 	ENABLE_READING_ALL	= atoi(ret);	}
	if(strstr( s , "ENABLE_AZTEC"		)) 	ENABLE_AZTEC		= atoi(ret);	}
	*/


	if(strstr( s , "ENABLE_READING_ALL_CODE"      )) 	ENABLE_READING_ALL_CODE=1;
	if(strstr( s , "ENABLE_READING_ALL_1D"	      )) 	ENABLE_READING_ALL_1D=1;
	if(strstr( s , "ENABLE_READING_ALL_2D"        )) 	ENABLE_READING_ALL_2D=1;
	if(strstr( s , "ENABLE_CODE_128"              )) 	ENABLE_CODE_128=1;
	if(strstr( s , "ENABLE_UCC_EAN_8"             )) 	ENABLE_UCC_EAN_8=1;
	if(strstr( s , "ENABLE_EAN_13"                )) 	ENABLE_EAN_13=1;
	if(strstr( s , "ENABLE_UPC_E"                 )) 	ENABLE_UPC_E=1;
	if(strstr( s , "ENABLE_UPC_A"                 )) 	ENABLE_UPC_A=1;
	if(strstr( s , "ENABLE_INTERLEAVED_2_OF_5"    )) 	ENABLE_INTERLEAVED_2_OF_5=1;
	if(strstr( s , "ENABLE_MATRIX_2_OF_5"         )) 	ENABLE_MATRIX_2_OF_5=1;
	if(strstr( s , "ENABLE_CODE_39"               )) 	ENABLE_CODE_39=1;
	if(strstr( s , "ENABLE_CODABAR"               )) 	ENABLE_CODABAR=1;
	if(strstr( s , "ENABLE_CODE_93"               )) 	ENABLE_CODE_93=1;
	if(strstr( s , "ENABLE_UCC_EAN_128"           )) 	ENABLE_UCC_EAN_128=1;
	if(strstr( s , "ENABLE_GS1_DATABAR"           )) 	ENABLE_GS1_DATABAR=1;
	if(strstr( s , "ENABLE_EAN_UCC_COMPOSITE"     )) 	ENABLE_EAN_UCC_COMPOSITE=1;
	if(strstr( s , "ENABLE_CODE_11"               )) 	ENABLE_CODE_11=1;
	if(strstr( s , "ENABLE_ISBN"                  )) 	ENABLE_ISBN=1;
	if(strstr( s , "ENABLE_INDUSTRIAL_25"         )) 	ENABLE_INDUSTRIAL_25=1;
	if(strstr( s , "ENABLE_STANDARD_25"           )) 	ENABLE_STANDARD_25=1;
	if(strstr( s , "ENABLE_PLESSEY"               )) 	ENABLE_PLESSEY=1;
	if(strstr( s , "ENABLE_MSI_PLESSEY"           )) 	ENABLE_MSI_PLESSEY=1;
	if(strstr( s , "ENABLE_PDF417"                )) 	ENABLE_PDF417=1;
	if(strstr( s , "ENABLE_QR_CODE"               )) 	ENABLE_QR_CODE=1;
	if(strstr( s , "ENABLE_AZTEC"                 )) 	ENABLE_AZTEC=1;
	if(strstr( s , "ENABLE_DATA_MATRIX"           )) 	ENABLE_DATA_MATRIX=1;
	if(strstr( s , "ENABLE_MAXICODE"              )) 	ENABLE_MAXICODE=1;
	if(strstr( s , "ENABLE_CHINESE_SENSIBLE_CODE" )) 	ENABLE_CHINESE_SENSIBLE_CODE=1;
}

int Read_ConfigFile()
{
	FILE *fp;
	char str[100+2];

 	fp = fopen(config_file,"r");

  	if(fp == NULL) {
    	perror("Error opening file");
      	return(-1);
   	}

	printf("\n\rReading config file : \n\r");
	while(1) 
	{
		if( fgets (str, 100, fp)!=NULL )
		{
			treat_config_line(str);
			continue;
		}
		break;
	}
	fclose(fp);
	return(0);
}



int ReadDataOneLine( char *ifile , char *data , int MaxLen)
{
	FILE *fp;

 	fp = fopen(ifile,"r");

  	if(fp == NULL) 
	{
	    	perror("Error opening file");
      		return(-1);
   	}

	if( fgets (data, MaxLen, fp)!=NULL )
	{
		fclose(fp);
		return(strlen(data));
	}

	fclose(fp);
	return(0);
}


//=====================================================================================================================
int IsBarcodeInitialized()
{
	FILE *fpt;
	char aTmp [100+1];
	int ret = 0;


	printf("IsBarcodeInitialized()\n\r");

	fpt = fopen( init_filename , "r" );
	if(fpt == NULL)
	{
		printf("    Not Inisialized file found\n\r");
		return ret;
	}


	while ((fgets(aTmp, 100, fpt)) > 0) 
	{
		printf("    %s", aTmp);
		if(memcmp(aTmp,c_sn,strlen(c_sn))==0)
		{
			ret = 1;
			printf("    ==> Same barcode : current %s   old %s \n\r",c_sn,aTmp);
			break;
		}		 
	}
	printf("\n\r");

	fclose(fpt);
	return (ret);
}
//=====================================================================================================================
int StoreInitializationDate()
{
	FILE *fpt;
	char aTmp [100+1];
	time_t t = time(NULL);

	fpt = fopen( init_filename , "w" );
	if(fpt == NULL)
	{
		printf("Error opening file '%s'\n\r",init_filename);
		return (-1);
	}

	sprintf(aTmp, "Initialization date : %s",ctime(&t));
	fwrite(aTmp , 1 , strlen(aTmp), fpt );

	sprintf(aTmp,"%s\n\r",c_sn);
	fwrite(aTmp , 1 , strlen(aTmp), fpt );

	sprintf(aTmp,"%s\n\r",c_sn);
	fwrite(aTmp , 1 , strlen(aTmp), fpt );


	fclose(fpt);

	system("sync");

	return (0);
}
//=====================================================================================================================
char aDeviceTree[128+2];

int ABT3000_1_0 = 0;
int ABT3000_2_0 = 0;
int ABT3000_3_0 = 0;
int ABT3000_4_0 = 0;

int MV3000_1_0 = 0;
int MV3000_2_0 = 0;
int MV3000_3_0 = 0;
int MV3000_4_0 = 0;

int dev_rakinda;
int dev_n3680;




//==============================================================================
void Flush_Buffer(void)
{
	char buf[100];
	int n;
	while(1)
	{
		usleep(100000);
		n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
		if(n)
		{
			continue;
		}	
		break;
	}
	usleep(200000);
}
//==============================================================================

char aSeriaNumber_Data [100];
char data_firmware   [100];


int main ( int argc, char **argv ) 
{
	//int n;
	int i;
	char aTmp[255];
	int Illumination_state = 9; 
	int last_lux = 0;
	int cur_lux = 0;


	printf("\n\r");
	printf("BCR led control v2020.02.19\n\r");
	printf("---------------------------\n\r");

	// inline parameters
	for(i=0 ; i < argc ; i++)
	{
		if(memcmp(argv[i],"debug"  ,5)==0){ Debug    = 1; }
	}

	//
	// Search Devicetree	
	//
	if( ReadDataOneLine((char *)"/proc/device-tree/model",aDeviceTree,128) <=0 )
	{
		printf("devicetree not found!!!\n\r");
		return(-1);
	
	}

	if(strstr(aDeviceTree,"abt3000_1_0")!= NULL) ABT3000_1_0 = 1;
	if(strstr(aDeviceTree,"abt3000_2_0")!= NULL) ABT3000_2_0 = 1;
	if(strstr(aDeviceTree,"abt3000_3_0")!= NULL) ABT3000_3_0 = 1;
	if(strstr(aDeviceTree,"abt3000_4_0")!= NULL) ABT3000_4_0 = 1;

	if(strstr(aDeviceTree,"mv3000_1_0")!= NULL) MV3000_1_0 = 1;
	if(strstr(aDeviceTree,"mv3000_2_0")!= NULL) MV3000_2_0 = 1;
	if(strstr(aDeviceTree,"mv3000_3_0")!= NULL) MV3000_3_0 = 1;
	if(strstr(aDeviceTree,"mv3000_4_0")!= NULL) MV3000_4_0 = 1;


	ClearLogfile();

	fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname, strerror (errno));
	        return(-1);
	}

	set_interface_attribs (fd, B9600, 0);    // set to default settings: 9600 Bps
	set_blocking (fd, 0);                    // set no blocking

	dev_rakinda =  IsRackindaPresent() ;

	if( ! dev_rakinda )
	{	
		set_interface_attribs (fd, B115200, 0);    // set to default settings: 9600 Bps
		dev_n3680 = ! Send_N3680( Prefix_request, aSernum  , (char *)"", aSeriaNumber_Data , 100 );
	}
	
	if( ! dev_n3680  && ! dev_rakinda )
	{	
		printf("N3680       : not present\n\r");
		close(fd);
		return(-1);
	}


	if( dev_n3680 )
	{
		printf("N3680       : present\n\r");
		sprintf(c_sn,"%s",&aSeriaNumber_Data[6]);
		i = strlen(c_sn);
		if( c_sn[i-1] == '.') c_sn[i-1] = 0;
	}


	//================================================
	// Flush Buffer
	//================================================
	Flush_Buffer();

	printf("BCR S/N     : <%s>\n\r",c_sn);


	WriteValue( (char * )"/sys/bus/i2c/devices/0-0044/power_state" , (char *)"1" );

	if( dev_n3680 )
	{
	 	while(1)
		{
			if( ReadDataOneLine((char *)"//sys/bus/i2c/devices/0-0044/lux",aDeviceTree,128) <=0 )
			{
				printf("devicetree not found!!!\n\r");
				break;
			}
			sleep(1);
			cur_lux = atoi(aDeviceTree) ;

			if(last_lux != cur_lux)
			{
				printf("Lux = %d\n\r",cur_lux);
				last_lux = cur_lux ;
			}

			if(atoi(aDeviceTree) > 50 )
			{
				if( Illumination_state != 0 )
				{
					Send_N3680( Prefix_cmd , Illumination_off     	, (char *) "Illumination_off" 		, aTmp , 255 );
					Illumination_state = 0 ;
				}
			}
			else
			{
				if( Illumination_state != 1)
				{
					Send_N3680( Prefix_cmd , Illumination_on     	, (char *) "Illumination_on" 		, aTmp , 255 );
					Illumination_state = 1 ;
				}
			}
		}
	}
	close(fd);
	return(0);
}
//=====================================================================================================================



typedef enum {
   HUNT_START_1=1 ,
   HUNT_START_2   ,
   HUNT_LEN_1     ,
   HUNT_LEN_2     ,
   HUNT_TYPE      ,
   HUNT_CHAR_1      ,
   HUNT_CHAR_2      ,
   HUNT_DATA_LEN_1     ,
   HUNT_DATA_LEN_2     ,
   HUNT_DATA      ,
   HUNT_LRC       ,
} HUNT_STATES;

typedef struct
{
   unsigned char START_1;
   unsigned char START_2;
   unsigned char LEN_1;
   unsigned char LEN_2;
   unsigned char TYPE;
   unsigned char CHAR_1;
   unsigned char CHAR_2;
   unsigned char DATA_LEN_1;
   unsigned char DATA_LEN_2;
   unsigned char DATA[256];
   unsigned char LRC;
}
RACKINDA_REC;

RACKINDA_REC ri;


void ShowRecord(void)
{
	RACKINDA_REC 		*r=&ri;

	printf("START_1    = %02X\n\r"		,r->START_1);
	printf("START_2    = %02X\n\r"		,r->START_2);
	printf("LEN_1      = %02x\n\r"		,r->LEN_1);
	printf("LEN_2      = %02x (%d)\n\r"	,r->LEN_2,r->LEN_2);
	printf("TYPE       = %02X\n\r"		,r->TYPE);
	printf("CHAR_1     = %02X\n\r"		,r->CHAR_1);
	printf("CHAR_2     = %02X\n\r"		,r->CHAR_2);
	printf("DATA_LEN_1 = %02X (%c)\n\r"	,r->DATA_LEN_1,r->DATA_LEN_1);
	printf("DATA_LEN_2 = %02X (%c)\n\r"	,r->DATA_LEN_2,r->DATA_LEN_2);
	printf("Data       = <%s>\n\r"		,r->DATA);
	printf("LRC        = %02X \r\n"		,r->LRC);


}



int Send_Command_bin(char *cmd,int len,char *description,int retries)
{
	static int 		Offset;
	unsigned char	LRC = 0;
	int 			TheChar;
	int 			State = HUNT_START_1 ;
	RACKINDA_REC 	*r=&ri;
	int 			to_msec=0;
	int 			n;
	unsigned char 	c;
	int 			l = 0;

	

	if(len > 3 )
	{
		LRC=0;
		for(n=2; n<len-1 ; n++)
		{
		
			TheChar = cmd[n];
			LRC = LRC ^ (TheChar&0x0ff);

		}
	

		if(Debug)
		{
			printf("\n\r\nTX : ");
			for(n=0; n<len ; n++)
			{
				printf("%02X ",cmd[n]);
			}
			printf("\n\r");
			printf("Calculated LRC: %02X\n\r\n\r", ((LRC ^ 0xFF) + 0) & 0xFF);
		}
	}


	while(1)
	{
		//----------------------------------------------------
		// Send Command
		//----------------------------------------------------
		write (fd, cmd , len);    
		usleep (1000000);

		//----------------------------------------------------
		// Waiting for replay ( Ack / Nack )
		//----------------------------------------------------
		while(1)
		{

			usleep(1000);
			to_msec+=1;

			if(to_msec > 20)
			{
				printf("Send_Command_bin(): Timeout \n\r");
				return(-1); 
			}
//			printf("to_msec = %d\n\r",to_msec);



			n = read (fd, &c, 1);  // read up to 100 characters if ready to read
			if(n==0)continue;

			to_msec =0;							
			
			TheChar = c & 0x00ff;
		
			if (Debug)
			{
	 			if(State == HUNT_START_1 )
				{
					printf("RX : ");
				}
				printf("%02X",TheChar);

				if(State == HUNT_LRC )
					printf("\n\r");
			}

			switch (State)
			{
				case HUNT_START_1:
				case HUNT_START_2:
					LRC = 0;
					break;
				case HUNT_LEN_1:
				case HUNT_LEN_2:
				case HUNT_TYPE:
				case HUNT_CHAR_1:
				case HUNT_CHAR_2:
				case HUNT_DATA_LEN_1:
				case HUNT_DATA_LEN_2:
				case HUNT_DATA:
					LRC = (LRC ^ (TheChar&0x0ff)) & 0x0ff;
					break;
				case HUNT_LRC:
					break;
				default:
					break;
			}

			switch (State)
			{
				case HUNT_START_1:
					if( TheChar == 0x02 )
					{
						memset(r,0x00,sizeof(RACKINDA_REC));
						Offset = 0;
						State  = HUNT_START_2;
						r->START_1 = TheChar;
					}
					break;

				case HUNT_START_2:
					r->START_2 = TheChar;
					State  = HUNT_LEN_1;
					break;

				case HUNT_LEN_1:
					r->LEN_1 =  TheChar ;
					State = HUNT_LEN_2;
					break;
				case HUNT_LEN_2:
					r->LEN_2 = TheChar ;
					State = HUNT_TYPE;
					break;
	
				case HUNT_TYPE:
					r->TYPE =  TheChar ;
					State = HUNT_CHAR_1;
					break;

				case HUNT_CHAR_1:
					r->CHAR_1 =  TheChar ;
					State = HUNT_CHAR_2;
					break;

				case HUNT_CHAR_2:
					r->CHAR_2 =  TheChar ;
					State = HUNT_DATA_LEN_1;
					break;

				case HUNT_DATA_LEN_1:
					r->DATA_LEN_1 =  TheChar  ;
					State = HUNT_DATA_LEN_2;
					break;
				case HUNT_DATA_LEN_2:
					r->DATA_LEN_2 = TheChar ;
					State = HUNT_DATA;

					l= (r->DATA_LEN_1-'0')*10 + (r->DATA_LEN_2-'0') ;

					if(l)
						State = HUNT_DATA ;
					else
						State = HUNT_LRC;
					break;

				case HUNT_DATA:
					if(Offset >100)
					{
						State=HUNT_START_1;
						printf("************************\n\r");
						printf("***** Offset error *****\n\r");
						printf("************************\n\r");
						break;
					}

					r->DATA[Offset++] = TheChar;
					r->DATA[Offset] = 0;
					if (Offset >= l)
					{
						State = HUNT_LRC;
					}
					break;
				case HUNT_LRC:
					LRC =  (LRC ^ 0xFF) ;
					r->LRC =  TheChar ;

					if(Debug) printf("RX LRC = 0x%02X  / Calc = %02X \r\n",r->LRC,LRC);

					State = HUNT_START_1;
					sleep(1);
					while( read (fd, &c, 1) )
					{
						printf("[%02X]\n\r",c);
						sleep(1);
					}
					if (Debug) ShowRecord();
					return(0);
					break;
				default:
					State = HUNT_START_1;
					break;
			}

		}
	}
	return 1;
}


//=============================================================================================================================
int IsRackindaPresent(void)
{
	unsigned char c;
	int ret;

	write (fd, "?" ,1);
	usleep (100000);

	while(1)
	{
		ret = read (fd, &c, 1) ;
		if(ret)
		{
			printf("rx: <%c>\n\r",c);
			if(c == '!' )
			{
				printf("Rakinda     : present\n\r");
				return(1);
			}
		}
		printf("Rakinda     : not present\n\r");
		return(0);
	}
}
//=============================================================================================================================




//=============================================================================================================================
int Get_Rakinda_SN (void)
{
	char aTmp[512];

	Send_Command_bin((char *) "\x7E\x00\x00\x05\x33\x48\x30\x33\x30\xB2",10,(char *)"\n\rQuery SN",1);
	
	if( strlen((char *)ri.DATA) > 3 )
	{
		sprintf(aTmp,"S/N: %s",ri.DATA);
		logfile(aTmp,strlen(aTmp));
		return(1);
	}
	return(0);
}
//=============================================================================================================================
int Get_Rakinda_ESN (void)
{
	char aTmp[512];

	Send_Command_bin((char *)"\x7E\x00\x00\x05\x33\x48\x30\x32\x30\xB3",10,(char *)"\n\rQuery SN",1);
	
	if( strlen((char *)ri.DATA) > 3 )
	{
		sprintf(aTmp,"ESN: %s",ri.DATA);
		logfile(aTmp,strlen(aTmp));
		return(1);
	}
	return(0);
}
//=============================================================================================================================
int Get_Rakinda_version (void)
{
	char aTmp[512];

	Send_Command_bin((char *)"\x7E\x00\x00\x02\x33\x47\x89",7,(char *)"\n\rQuery SN",1);
	
	if( strlen((const char *)ri.DATA) > 3 )
	{
		sprintf(aTmp,"Version: %s",ri.DATA);
		logfile(aTmp,strlen(aTmp));
		return(1);
	}
	return(0);
}
//=============================================================================================================================













int  Send_N3680 ( unsigned char *prefix , unsigned char *aCmd, char *Description,char *aResponse,int MaxRespLen)
{
	int offset=0;
	int ret;
	int i;
	int iCmdLen;

	unsigned char c;
	unsigned char aData    [100];	


	iCmdLen = strlen((const char *)aCmd);

	

	for(i = 0; i < 3 ; i++)
	{
		aData [i] = prefix[i] ;
	}

	for(i=0; i< iCmdLen ; i++)
	{
		aData[i+3] = aCmd[i] ;
	}

	//----------------------------------------
	if(Debug)
	{	
		printf("\n\rTX[%02d]: ",iCmdLen);
		for(i=0; i< (iCmdLen+3) ; i++)
		{
			if( isprint(aData[i]) )
				printf("%c",aData[i]);
			else
				printf("<%02X>",aData[i]);
		}
		printf("\n\r");
	}
	//----------------------------------------

	write (fd, aData , iCmdLen + 3);
	usleep (500000);

	while(1)
	{
		if(offset >= MaxRespLen -1) break;

		ret = read (fd, &c, 1) ;
		if(ret)
		{
			aResponse[offset++]   = ( char ) c;
			aResponse[offset  ]   = '\0';
			continue;
		}
		break;		
	}


	if(Debug)
	{
		printf("RX[%02d]: ",offset);
		for( i=0 ; i<offset ; i++ )
		{
			if( isprint(aResponse[i]) )
				printf("%c",aResponse[i]);
			else
				printf("<%02X>",aResponse[i]);
		}
		printf("\n\r");
	}

	if( aResponse[offset-2]==0x06)  // 0x06 == ACK
	{
		if(strlen(Description)) printf("%-25s : OK\n\r",Description);
		return(0);
	}

	if(strlen(Description)) printf("%-25s : ERROR\n\r",Description);
	return(1);
}







void N3680_settings(void)
{
	int errors = 0;
	char aTmp[256];

	printf("N3680_settings()\n\r");

 
//	errors+= Send_N3680( Prefix_cmd , power_up_beep_off 	, (char *) "power_up_beep_off" 	, aTmp , 255 );
	errors+= Send_N3680( Prefix_cmd , power_up_beep_on  	, (char *) "power_up_beep_on"  	, aTmp , 255 );

//	errors+= Send_N3680( Prefix_cmd , good_read_off  	, (char *) "good_read_off"  	, aTmp , 255 );
	errors+= Send_N3680( Prefix_cmd , good_read_on     	, (char *) "good_read_on" 	, aTmp , 255 );

//	errors+= Send_N3680( Prefix_cmd , beeper_vol_off     	, (char *) "beeper_vol_off" 	, aTmp , 255 );
//	errors+= Send_N3680( Prefix_cmd , beeper_vol_low     	, (char *) "beeper_vol_low" 	, aTmp , 255 );
	errors+= Send_N3680( Prefix_cmd , beeper_vol_medium     , (char *) "beeper_vol_medium" 	, aTmp , 255 );
//	errors+= Send_N3680( Prefix_cmd , beeper_vol_high     	, (char *) "beeper_vol_high" 	, aTmp , 255 );

//	errors+= Send_N3680( Prefix_cmd , good_read_delay_500   , (char *) "good_read_delay_500" 	, aTmp , 255 );
//	errors+= Send_N3680( Prefix_cmd , good_read_delay_1000  , (char *) "good_read_delay_1000" 	, aTmp , 255 );
//	errors+= Send_N3680( Prefix_cmd , good_read_delay_1500  , (char *) "good_read_delay_1500" 	, aTmp , 255 );
	errors+= Send_N3680( Prefix_cmd , good_read_delay_0     , (char *) "good_read_delay_0" 		, aTmp , 255 );


//	errors+= Send_N3680( Prefix_cmd , Illumination_high     , (char *) "Illumination_high" 		, aTmp , 255 );
	errors+= Send_N3680( Prefix_cmd , Illumination_low     	, (char *) "Illumination_low" 		, aTmp , 255 );

	errors+= Send_N3680( Prefix_cmd , PresentationMode     	, (char *) "PresentationMode" 		, aTmp , 255 );

	errors+= Send_N3680( Prefix_cmd , Illumination_on     	, (char *) "Illumination_on" 		, aTmp , 255 );
//	errors+= Send_N3680( Prefix_cmd , Illumination_off     	, (char *) "Illumination_off" 		, aTmp , 255 );

	printf("Detected errors : %d \n\r",errors);
	if(errors==0)
	{
		StoreInitializationDate();
	}
}








int ReadValue( char *ifile , char *data , int MaxLen)
{
	FILE *fp;

 	fp = fopen(ifile,"r");

  	if(fp == NULL) 
	{
	    	perror("Error opening file");
      		return(-1);
   	}

	if( fgets (data, MaxLen, fp)!=NULL )
	{
		fclose(fp);
		return(strlen(data));
	}

	fclose(fp);
	return(0);
}



int WriteValue( char *ifile , char *data )
{
	FILE *fp;
	int len;

	len = strlen(data);

 	fp = fopen(ifile,"w");

  	if(fp == NULL) 
	{
	    	perror("Error opening file");
      		return(-1);
   	}

	if( fwrite( data , 1,len,  fp ) != (unsigned int) len )
	{
		printf("Error writting  <%s> to [%s]",data,ifile);
		perror("Write error");
		fclose(fp);
		return(-1);
	}

	fclose(fp);
	return(0);
}

