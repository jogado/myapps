#!/bin/sh


#
# Info
#
qmicli --device=/dev/cdc-wdm0 --dms-get-model
qmicli --device=/dev/cdc-wdm0 --dms-get-revision
qmicli --device=/dev/cdc-wdm0 --dms-get-ids
qmicli --device=/dev/cdc-wdm0 --uim-get-card-status


#
# Setup connection
#
qmi-network /dev/cdc-wdm0 start
sleep 1

qmicli --device=/dev/cdc-wdm0 --get-wwan-iface
sleep 1

udhcpc -q -f -i wwan0
sleep 1

ping -I wwan0 8.8.8.8

