
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>    

#include <oti.h>
#include <errno.h>
#include <libesc.h>



extern void treat_df(unsigned char *p);
extern void treat_ff(unsigned char *p);
extern void treat_84(unsigned char *p);

extern void DF_68 (unsigned char *p);
extern void DF_30 (unsigned char *p);
extern void DF_04 (unsigned char *p);
extern void DF_19 (unsigned char *p);
extern void DF_06 (unsigned char *p);
extern void DF_4D (unsigned char *p);
extern void DF_79 (unsigned char *p);
extern void DF_7A (unsigned char *p);
extern void DF_13 (unsigned char *p);
extern void DF_16 (unsigned char *p);
extern void DF_14 (unsigned char *p);
extern void DF_0D (unsigned char *p);
extern void DF_6B (unsigned char *p);
extern void DF_4E (unsigned char *p);
extern void DF_0E (unsigned char *p);
extern void DF_XX (unsigned char *p);


typedef enum {
   HUNT_STX = 1   ,
   HUNT_LEN       ,
   HUNT_UNIT      ,
   HUNT_OPCODE    ,
   HUNT_DATA      ,
   HUNT_LRC       ,
   HUNT_ETX       ,
} HUNT_STATES;


typedef struct
{
   unsigned char START_CHR;
   unsigned char REC_LEN;
   unsigned char UNIT;
   unsigned char OPCODE;
   unsigned char DATA[512];
   unsigned char LRC;
   unsigned char END_CHR;
}
OTI_REC;




#define STX 0x02
#define ETX 0x03

































int resend=1;

//============================================================================================
void Show(char *aTmp, int aLen)
{
    char aBuffer[512];
    int i;
    
    sprintf(aBuffer,"RX :");
    for (i = 0 ; i < aLen ; i++)
    {
	sprintf(&aBuffer[strlen(aBuffer)],"[%02X]",aTmp[i]);
    }
    sprintf(&aBuffer[strlen(aBuffer)],"\n\r");	
    printf(aBuffer);
   
    if(aTmp[1]!= 0x08)
    {
	sleep(1);
	resend = 1;
    }
}
//============================================================================================















int fd;
//char *fp = "/dev/ttyACM0";
char *fp = "/dev/oti";
int flag = O_RDWR | O_NONBLOCK;

OTI_REC ri;

int show_rxmsg = 0;

//============================================================================================
void show_record(OTI_REC *r)
{
   int i;

   if (!show_rxmsg) return;

   printf("RX:");
   printf( "[%02X]" , r->START_CHR);
   printf( "[%02X]" , r->REC_LEN);
   printf( "[%02X]" , r->UNIT);
   printf( "[%02X]" , r->OPCODE);

   for(i=0; i < r->REC_LEN-6;i++)
   {
      printf( "[%02X]" , r->DATA[i] );
   }
   printf( "[%02X]" ,r->LRC);
   printf( "[%02X]" ,r->END_CHR);
   printf( "\n\r");
}

//============================================================================================
void treat_data(OTI_REC *r)
{
   int i;
   int len = r->REC_LEN -6;
   unsigned char *p = &r->DATA[0] ;
   char aTmp[100];

   int r_type;
   int r_id;
   int r_len;

   if( (r->REC_LEN==8) && (r->DATA[0]==0x00) && (r->DATA[1]==0x00) )
   {
         printf("    PDC ACK\n\r");
         return;
   }

   if( (r->REC_LEN==7) && (r->OPCODE==0x15) )
   {
         printf("    PDC NACK (error: 0x%02X)\n\r",r->DATA[0]);
         return;
   }

   while (len > 0)
   {
      r_type= *(p+0);
      r_id  = *(p+1);
      r_len = *(p+2);

      if( r_type == 0xDF )
      {
         treat_df ( p );
         p   += r_len +3 ;
         len -= r_len +3 ;
         continue;
      }

      if( r_type == 0xFF ) // PDC Response templates
      {
         treat_ff ( p );
         p   +=  3 ;
         len -=  3 ;
         continue;
      }

      if( r_type == 0x84 ) // Dedicated File (DF) Name (EMV Cards)
      {
         r_type= *(p+0);
         r_id  = *(p+0);
         r_len = *(p+1);

         treat_84 ( p );
         p   += r_len +2 ;
         len -= r_len +2 ;
         continue;
      }
      p++;
      len--;
   }
}
//============================================================================================









void RxLoop(void)
{
	static unsigned char    State = HUNT_STX;
//	static unsigned char    TheChar;
	static unsigned int     Offset;
	static unsigned char    LRC;
   	char                    aTmp[512];
   	OTI_REC                 *r=&ri;
	int len;
	int TheChar;


    while (1)
    {

        len = read(fd,&TheChar,1);
        
        TheChar = TheChar & 0x0ff;

	    if(len != 1) return;


        // LRC
        switch (State)
        {
	        case HUNT_STX:
                    LRC = 0;
                    LRC = (LRC ^ TheChar&0x0ff) & 0x0ff;
                    break;
    	    case HUNT_LEN:
	        case HUNT_UNIT:
	        case HUNT_OPCODE:
  	        case HUNT_DATA:
                    LRC = (LRC ^ TheChar&0x0ff) & 0x0ff;
                    break;
	        case HUNT_LRC:
	        case HUNT_ETX:
                    break;
            default:
                    break;
        }


        switch (State)
	    {
	    	case HUNT_STX:
	        		if( TheChar == STX )
	    			{
                        memset(r,0x00,sizeof(OTI_REC));
	    				Offset      = 0;
		    			State       = HUNT_LEN;
                        r->START_CHR = TheChar;
		    		}
	    			break;

            case HUNT_LEN:
                    r->REC_LEN =  TheChar ;
        			State = HUNT_UNIT;
                    break;

		    case HUNT_UNIT:
                    r->UNIT = TheChar ;
        			State = HUNT_OPCODE;
                    break;

		    case HUNT_OPCODE:
                    r->OPCODE =  TheChar ;
        			State = HUNT_DATA;
                    break;

	    	case HUNT_DATA:
                    r->DATA[Offset++] = TheChar;
		    		if (Offset >= r->REC_LEN -6)
                    {
		    			State = HUNT_LRC;
                    }
		    		break;

		    case HUNT_LRC:
                    r->LRC =  TheChar ;
                    if(r->LRC != LRC )
                    {
                        printf("        LRC = 0x%02X  / Calc = %02X ",r->LRC,LRC)              ;	
              			State = HUNT_STX;
                    }
                    else
                    {
             			State = HUNT_ETX;
                    }
                    break;

		    case HUNT_ETX:
		    		if( TheChar == ETX )
	    			{
                        r->END_CHR =  TheChar ;
                        show_record(r);
                        treat_data(r);
		    		}
                    State       = HUNT_STX;
                    break;

            default:
                    State = HUNT_STX;
                    break;
		}
	}

}
//==============================================================================






















int main0(void)
{
	OtiOpen open;
	OtiAppli_t appli;
	OtiPcdSettings settings = { 0 };
	OtiCardInfo cardinfo;
	void *initHandle;
	void *oti;
	const OtiTransparentChannel TransparentChannel = OtiTransparentChannelRf;
	const OtiTransparentLayer TransparentLayer = OtiTransparentLayerHost;
	char rxbuffer[4096];
	char sak = OtiSakEnable;
	const OtiPollMode mode = OtiPollPendingOnce;
	unsigned int timeout_ms = 10000;
	int rc;
	char tx_rats[] = { 0xE0, 0x80 };
	char rx_ats[256];

	int command;
	char aTmp[256];



	printf("Starting ...\n");


	/* open oti */
	initHandle = oti_init();

	memset(&open, 0x00, sizeof(open));
	strcpy(open.path, "/dev/ttyACM0");
	open.baudrate = B9600;
	open.interfaceType = OtiReaderInterfaceTypeSerial;

	oti = oti_open(initHandle, &open);
	if (!oti) {
		perror("oti_open");
		return 1;
	}


	/*===================================================================================================================*/
	/* ADDED by JGD: Read current OTI firmware version 								     */
	/*===================================================================================================================*/
	command = OTI_VERSION_FIRMWARE;
	if ( (rc = oti_ioctl(oti, OTI_IOCTL_GET_VERSION, &command, sizeof(command), aTmp, sizeof(aTmp))) <0)
		return rc;
	printf(" Current OTI software version %s\n", aTmp);
	/*===================================================================================================================*/

//int oti_ioctl( void* handle, OtiIoctl command, const void* tx, size_t txLen, void* rx, size_t rxLen );//




	/* card app select */
	appli = (OTI_APPLI_MIFARE | OTI_APPLI_ISO_14443_4_PICC);

	rc = oti_ioctl(oti, OTI_IOCTL_DO_APPLICATIONS,
		       &appli, sizeof(appli), NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_DO_APPLICATIONS");
		return 1;
	}


	rc = oti_ioctl(oti, OTI_IOCTL_SET_APPLICATIONS,  &appli,sizeof(appli),
					 NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_SET_APPLICATIONS");
//		return 1;
	}



	rc = oti_ioctl(oti, OTI_IOCTL_LOAD_DEFAULT_PARAMETERS, NULL, 0,
		       rxbuffer, sizeof(rxbuffer));
	if (rc) {
		perror("OTI_IOCTL_LOAD_DEFAULT_PARAMETERS");
//		return 1;
	}


	rc = oti_ioctl(oti, OTI_IOCTL_GET_APPLICATIONS, NULL, 0,
			 &appli,  sizeof(appli));
	if (rc) {
		perror("OTI_IOCTL_GET_APPLICATIONS");
//		return 1;
	}





	// Parameters to return on card activation 
	/*
	settings.mask   = OtiPcdRfCollisionMessage | OtiPcdTracksFollowedByAID \
			  | OtiPcdPupi | OtiPcdUid | OtiPcdAtqB | OtiPcdAts;
	settings.values = OtiPcdRfCollisionMessage | OtiPcdTracksFollowedByAID \
			  | OtiPcdPupi | OtiPcdUid | OtiPcdAtqB | OtiPcdAts;
	*/
	settings.mask   = OtiPcdRfCollisionMessage  \
			  | OtiPcdPupi | OtiPcdUid | OtiPcdAtqB | OtiPcdAts;
	settings.values = OtiPcdRfCollisionMessage \
			  | OtiPcdPupi | OtiPcdUid | OtiPcdAtqB | OtiPcdAts;

	rc = oti_ioctl(oti, OTI_IOCTL_SET_PCD_SETTINGS, (void*)&settings,
			sizeof(settings), NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_SET_PCD_SETTINGS");
//		return 1;
	}

	rc = oti_ioctl(oti, OTI_IOCTL_LOAD_DEFAULT_PARAMETERS, NULL, 0,
			rxbuffer, sizeof(rxbuffer) );
	if (rc) {
		perror("OTI_IOCTL_LOAD_DEFAULT_PARAMETERS");
//		return 1;
	}

	rc = oti_ioctl(oti, OTI_IOCTL_SET_SAK, &sak, 1, rxbuffer,
		       sizeof(rxbuffer));
	if (rc) {
		perror("OTI_IOCTL_SET_SAK");
//		return 1;
	}

	rc = oti_ioctl(oti, OTI_IOCTL_LOAD_DEFAULT_PARAMETERS, NULL, 0,
		       rxbuffer, sizeof(rxbuffer) );
	if (rc) {
		perror("OTI_IOCTL_LOAD_DEFAULT_PARAMETERS");
//		return 1;
	}






	/* Card await and select */
	rc = oti_ioctl(oti, OTI_IOCTL_POLL_START, (void*)&mode, sizeof(mode),
		       NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_POLL_START");
//		return 1;
	}

	rc = oti_ioctl(oti, OTI_IOCTL_WAIT_FOR_CARD, (void*)&timeout_ms,
		       sizeof(timeout_ms), (void*)&cardinfo, sizeof(cardinfo));
	if (rc) {
		perror("OTI_IOCTL_WAIT_FOR_CARD");
		return 1;
	}


	/* Setting reader in transparent mode (to send APDUs) */
	rc = oti_ioctl(oti, OTI_IOCTL_TRANSPARENT_CHANNEL, &TransparentChannel,
		       sizeof(TransparentChannel), NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_TRANSPARENT_CHANNEL");
		return 1;
	}

	rc = oti_ioctl(oti, OTI_IOCTL_TRANSPARENT_LAYER_CONTROL, &TransparentLayer,
		       sizeof(TransparentLayer),   NULL, 0);
	if (rc) {
		perror("OTI_IOCTL_TRANSPARENT_LAYER_CONTROL");
		return 1;
	}

	/* Setting card to ISO14443-4 */
	memset(rx_ats, 0x00, sizeof(rx_ats));
	rc = oti_ioctl(oti, OTI_IOCTL_TRANSPARENT_DATA, tx_rats, 2,
		       rx_ats, sizeof(rx_ats));
	fprintf(stderr, "ATS rc (%d)\n", rc);
	hexdump(rx_ats, sizeof(rx_ats));

	/* Send PPS */
	char tx_pps[] = { 0xD0, 0x11, 0x00 };
	char rx_pps[16];

	memset(rx_pps, 0x00, sizeof(rx_pps));
	rc = oti_ioctl(oti, OTI_IOCTL_TRANSPARENT_DATA, tx_pps, sizeof(tx_pps),
		       rx_pps, sizeof(rx_pps));
	fprintf(stderr, "PPS rc (%d)\n", rc);
	hexdump(rx_pps, sizeof(rx_pps));

	/* EMV-protocol realization */
	char tx[] = { 0x00, 0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00  };
	char rx[256];

	rc = oti_ioctl(oti, OTI_IOCTL_TRANSPARENT_DATA, tx, sizeof(tx), rx, sizeof(rx));
	fprintf(stderr, "EMV errno %d %s rc (%d)\n", errno, strerror(errno), rc);
	hexdump(rx, sizeof(rx));

	return 0;
}




























//=====================================================================================================================
void treat_df(unsigned char *p)
{
   int Type,Id,Len;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);

   switch(Id)
   {
      case 0x7A : DF_7A( p ) ; break; // Transparent - Transport Layer Control
      case 0x79 : DF_79( p ) ; break; // RF Parameters – Modulation Type
      case 0x4D : DF_4D( p ) ; break; // PCD Serial Number
      case 0x06 : DF_06( p ) ; break; // RF - ON/OFF
      case 0x16 : DF_16( p ) ; break; // Detected PICC type
      case 0x6B : DF_6B( p ) ; break; // SAK
      case 0x4E : DF_4E( p ) ; break; // Version
      case 0x0D : DF_0D( p ) ; break; // UID
      case 0x0E : DF_0E( p ) ; break; // ATS  of a detected Type A PICC
      case 0x13 : DF_13( p ) ; break; // ATQB of a detected Type B PICC
      case 0x14 : DF_14( p ) ; break; // PUPI of a detected Type B PICC
      case 0x19 : DF_19( p ) ; break; // Reset PCD
      case 0x04 : DF_04( p ) ; break; // Load Defaults Parameters
      case 0x30 : DF_30( p ) ; break; // RF Collision Message Enable
      case 0x68 : DF_68( p ) ; break; // Poll EMV error
      default   : DF_XX( p ) ; break;
   }
resend=1;
}
//=====================================================================================================================
void treat_ff(unsigned char *p)
{
   int Type,Id,Len;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);

   if(Type != 0xff ) return;

   printf("    FF_%02X : \n\r",Id);

   switch(Id)
   {
      case 1 : printf("Success Template\n\r")    ; /*system("beep");*/ break ;
      case 2 : printf("Unsupported Template\n\r"); break ;
      case 3 : printf("Failed Template\n\r")     ; break ;
      default: printf("????\n\r")		 ; break ;
   }
   printf(" [Template len : 0x%02X ]\n\r",Len);

    /*
   if( SEND_REPEAT )
   {
      Timer_SendRepeat->Enabled = true;
   }
   else
   {
      Timer_SendRepeat->Enabled = false;
   }



   if( SEND_REPEAT == 1 )
   {
   }
    */
}
//=====================================================================================================================
void treat_84(unsigned char *p)
{
   int Type,Id,Len,i;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+0);
   Len   = *(p+1);


   printf("    %02X : Dedicated File (DF) Name",Id);

   if(Len)
   {
      printf(" [ ");

      for(i=0; i <Len ;i++)
      {
         if(p[i+2] >= 0x20 && p[i+2] < 0x7f)
            printf("%c",p[i+2]);
         else
            printf("<%02X>",p[i+2]);

      }
      printf("]");
   }
   printf("\n\r");
}
//=====================================================================================================================












































void DF_68 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];
   int i;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : Poll EMV error",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
  printf("\n\r");
}


void DF_30 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];
   int i;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : RF Collision Message",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
  printf("\n\r");
}




void DF_04 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];
   int i;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : Load Default Parameters",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
  printf("\n\r");
}

void DF_19 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];
   int i;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : Reset PCD",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");
}





void DF_06 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : RF - ON/OFF",Id);


   if(Len)
   {
      printf(" [");
      switch(Val)
      {
         case 0 : printf("RF 13.56 Mhz is OFF");break;
         case 1 : printf("RF 13.56 Mhz is ON");break;
         default: printf("RFU");break;
      }
      printf("]");
   }
   printf("\n\r");
}
//==============================================================================
void DF_4D (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : PCD Serial Number",Id);

   if(Len)
   {
      printf(" [%02X%02X%02X%02X-%02X%02X%02X%02X]",
            *(p+3),
            *(p+4),
            *(p+5),
            *(p+6),
            *(p+7),
            *(p+8),
            *(p+9),
            *(p+10)
      );
   }
  printf("\n\r");
}
//==============================================================================
void DF_79 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : RF Parameters – Modulation Type",Id);

   if(Len)
   {
      printf(" [");
      switch(Val)
      {
         case 0x42 : printf("Type B (FS)" );break;
         case 0x41 : printf("Type A"      );break;
         default   : printf("RFU"         );break;
      }
      printf("]");
   }
  printf("\n\r");
}


//==============================================================================
void DF_7A (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : Transport layer Control",Id);


   if(Len)
   {
      printf(" [");
      switch(Val)
      {
         case 0 : printf("PCD is responsible (FS)");break;
         case 1 : printf("Host is responsible");break;
         default: printf("RFU");break;
      }
      printf("]");
   }
  printf("\n\r");
}
//==============================================================================
void DF_13 (unsigned char *p )
{
   int Type,Id,Len,Val;
   char aTmp [256];
   int i;
FILE *f;

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);


   printf("    DF_%02X : PUPI of Type B PICC",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");


  if(Len)
   {
   	f= fopen ("moti.tmp","w");
   	fprintf(f,"[");
   	for(i=0; i <Len ;i++)
   	{
   		fprintf(f,"%02X ",p[i+3]);
   	}
   	fprintf(f,"]\n\r");
   	fclose(f); 
   }

}
//==============================================================================
void DF_16 (unsigned char *p)
{
   int Type,Id,Len,Val;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);


   if(Val >= 0x20 && Val < 0x7F )
   {
      printf("    DF_%02X : Detected PICC type [%c]",Id,Val);
   }
   else
   {
      printf("    DF_%02X : Detected PICC type [%02X]",Id,Val);
   }
   printf("\n\r");
}
//==============================================================================
void DF_14 (unsigned char *p)
{
   int Type,Id,Len,Val;
   int i;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);


   printf("    DF_%02X : ATQB of Type B PICC",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");

}
//==============================================================================
void DF_0D (unsigned char *p)
{
   int Type,Id,Len,Val;
   int i;
   char aTmp [256];

   FILE *f;


   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : UID",Id);

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");

   if(Len)
   {
   	f= fopen ("moti.tmp","w");
   	fprintf(f,"[");
   	for(i=0; i <Len ;i++)
   	{
   		fprintf(f,"%02X ",p[i+3]);
   	}
   	fprintf(f,"]\n\r");
   	fclose(f); 
   }

}
//==============================================================================
void DF_6B (unsigned char *p)
{
   int Type,Id,Len,Val,i;
   char aTmp [256];

   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
   Val   = *(p+3);

   printf("    DF_%02X : SAK",Id,Val);     

   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");
}
//==============================================================================
void DF_4E (unsigned char *p)
{
//   int Type,Id,Len,Val;
//   char aTmp [256];
   int Id,Len;

   char aData[100];

//   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
//   Val   = *(p+3);


   if (Len > sizeof(aData)-2) return;
   memcpy(aData,p+3,Len);
   aData[Len]=0;

   printf("    DF_%02X : Version [%s]",Id,aData);
   printf("\n\r");
}
//==============================================================================
void DF_0E (unsigned char *p)
{
//   int Type,Id,Len,Val;
//   char aTmp [256];
   int Id,Len;
   int i;

//   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
//   Val   = *(p+3);

   printf("    DF_%02X : ATS of Type A PICC",Id);


   if(Len)
   {
      printf(" [ ");
      for(i=0; i <Len ;i++)
      {
         printf("%02X ",p[i+3]);
      }
      printf("]");
   }
   printf("\n\r");


}
//==============================================================================
void DF_XX(unsigned char *p)
{
//   int Type,Id,Len,Val;
//   char aTmp [256];
   int Id,Len;

//   Type  = *(p+0);
   Id    = *(p+1);
   Len   = *(p+2);
//   Val   = *(p+3);

   printf("    DF_%02X(len:0x%02X)",Id,Len);

   printf("\n\r");
}
//==============================================================================










































/*
[PREDEFINED] [GET - Get FW Version       ]       [02 0A 00 3D DF 4E 01 01 A4 03]    
[PREDEFINED] [GET - Get PDC Serial Number]       [02 09 00 3D DF 4D 00 A4 03]    
[PREDEFINED] [DO  - POLL]                        [02 0A 00 3E DF 7E 01 03 95 03]    
[PREDEFINED] [DO  - POLL EMV 1]                  [02 1E 00 3E FD 16 9F 02 06 00 00 00 00 10 00 9C 01 00 9A 03 17 05 29 5F 2A 02 09 71 72 03]    
[PREDEFINED] [DO  - POLL EMV 2]                  [02 19 00 3E FD 11 9F 02 06 00 00 00 00 12 34 9C 01 00 9A 03 06 12 31 55 03 ]    
[PREDEFINED] [DO  - Set Transparent]             [02 0A 00 3E DF 7A 01 00 92 03]   
[PREDEFINED] [DO  - RF Parameters]               [02 0A 00 3E DF 79 01 41 D0 03]    
[PREDEFINED] [DO  - RF OFF/OFF]                  [02 0A 00 3E DF 06 01 00 EE 03]    
[PREDEFINED] [SET - RF OFF]                      [02 0A 00 3C DF 06 01 00 EC 03]    
[PREDEFINED] [SET - RF ON]                       [02 0A 00 3C DF 06 01 01 ED 03]    
[PREDEFINED] [GET - RF ON/OFF]                   [02 09 00 3D DF 06 00 EF 03]    
[PREDEFINED] [DO  - Load Default Parameters]     [02 09 00 3E DF 04 00 EE 03]    
[PREDEFINED] [DO  - Reset PCD]                   [02 09 00 3E DF 19 00 F3 03]    
[PREDEFINED] [DO  - Enable Application]          [02 0D 00 3E DF 4F 04 30 00 00 00 95 03]    
[PREDEFINED] [SET - Enable Application]          [02 0D 00 3C DF 4F 04 30 00 00 00 97 03]    
[PREDEFINED] [GET - Enable Application]          [02 09 00 3D DF 4F 00 A6 03]    
[PREDEFINED] [SET - PCD Settings]                [02 1A 00 3C DF 13 01 01 DF 0D 01 01 DF 0E 01 01 DF 14 01 01 DF 30 01 01 CF 03]
[PREDEFINED] [GET - PCD Settings]                [02 15 00 3D DF 13 00 DF 0D 00 DF 0E 00 DF 14 00 DF 30 00 C1 03]
[PREDEFINED] [SET - SAK]                         [02 0A 00 3C DF 6B 01 01 80 03]
[PREDEFINED] [GET - SAK]                         [02 09 00 3D DF 6B 00 82 03]
[PREDEFINED] [GET - SERIAL & FIRMWARE]           [02 0D 00 3D DF 4E 01 01 DF 4D 00 31 03 ]
[PREDEFINED] [SET - ALL CARDS]                   [02 0D 00 3C DF 4F 04 FF FF 00 00 A7 03 ]
[PREDEFINED] [SET - Collision Enabled]           [02 0A 00 3C DF 30 01 01 DB 03 ]
*/

unsigned char Get_FW_Version 	[] = 	{0x02 ,0x0A ,0x00 ,0x3D ,0xDF ,0x4E ,0x01 ,0x01 ,0xA4 ,0x03};
unsigned char Do_Poll       	[] = 	{0x02 ,0x0A ,0x00 ,0x3E ,0xDF ,0x7E ,0x01 ,0x03 ,0x95 ,0x03};
unsigned char Set_RF_OFF	[] = 	{0x02 ,0x0A ,0x00 ,0x3C ,0xDF ,0x06 ,0x01 ,0x00 ,0xEC ,0x03};
unsigned char Set_RF_ON		[] = 	{0x02 ,0x0A ,0x00 ,0x3C ,0xDF ,0x06 ,0x01 ,0x01 ,0xED ,0x03};





unsigned char Do_RF_OFF			[] = "020A003EDF060100EE03"
unsigned char Do_RF_ON	          	[] = "020A003EDF060101EF03"

unsigned char Do_OUTPORT_CTRL_PCD  	[] = "020A003EDF0A0100E203"
unsigned char Do_OUTPORT_CTRL_HOST  	[] = "020A003EDF0A0101E303"

unsigned char Do_OUTPORT_ALL_OFF  	[] = "020A003EDF7F01009703"
unsigned char Do_OUTPORT_BUZZER_ON 	[] = "020A003EDF7F01019603"
unsigned char Do_OUTPORT_LED1_ON 	[] = "020A003EDF7F01029503"
unsigned char Do_OUTPORT_LED2_ON	[] = "020A003EDF7F01049303"
unsigned char Do_OUTPORT_LED3_ON 	[] = "020A003EDF7F01089F03"
unsigned char Do_OUTPORT_LED4_ON 	[] = "020A003EDF7F01108703"



//=====================================================================================================================

int main() {
	int c;
	int len;
	int nothing=0;
	char aTmp[100];
	char aLen=0;
	FILE *f;

	printf("m-oti v2018.10.25...\n\r");
	system("toti -t 1 > /dev/null 2>&1\n\r"); 
	usleep(1000000);

	fd = open(fp, flag);

	/*
	printf("\n\rSet_RF_ON() \n\r");	
	write(fd, Set_RF_ON, Set_RF_ON[1]);
   	usleep(300000);
	*/

	printf("Get_FW_Version() \n\r");	
	write(fd,  Get_FW_Version,  Get_FW_Version[1]);


	while(1)
	{
	    RxLoop();

	    if(resend==1)
	    {
			break;
			/*
        	sleep(1);
	        resend =0;
	        printf("\n\rTX: Poll Request \n\r");	
			write(fd, Do_Poll, Do_Poll[1]);
			*/
	}

	    
		usleep(1000);

	    if(nothing > 30000 ) 
	    {
		    resend =1;
		    printf("timeout !! \n\r");
	    }
    }

	printf("exited from the loop ...\n");

	close(fd);
	return 0;
}

