/*****************************************************************************
*
*      Copyright (C) 2021 ... Emsyscon
*
* All rights reserved.
* 
* Permission to use, copy, modify, and distribute this software for any purpose
* with or without fee is hereby granted, provided that the above copyright
* notice and this permission notice appear in all copies.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
* NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
* OR OTHER DEALINGS IN THE SOFTWARE.
* 
* Except as contained in this notice, the name of a copyright holder shall not
* be used in advertising or otherwise to promote the sale, use or other dealings
* in this Software without prior written authorization of the copyright holder.
*
*******************************************************************************/

/******************************************************************************
* program to request S/N from SAM device 
* maintainer: Emsyscon Solutions
*******************************************************************************/





#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <libsam.h>


#include <winscard.h>

#include "buildate.h"
#include "main.h"








int             DEBUG=0;
unsigned char   sam_type=sam_type_unknown;
char            sam_atype[20];


//=====================================================================================================================
static const unsigned char SAMS1_ATR[]  = { 0x3B, 0x3F, 0x96, 0x00, 0x80, 0x5A, 0x2A, 0x80, 0xE1, 0x08, 0x40, 0x25, 0xAE, 0x10, 0xFD, 0x53, 0x82, 0x90, 0x00 };
static const unsigned char NSAM2_ATR[]  = { 0x3B, 0xD7, 0x18, 0x00, 0x00, 0x80, 0x31, 0x80, 0x73, 0x88, 0x21, 0x0B };
static const unsigned char S9_ATR[] 	= { 0x3B, 0x5E, 0x18, 0x00, 0x00, 0x31, 0xC0, 0x65, 0x7C, 0xB5, 0x01, 0x00, 0x87, 0x71, 0xD6, 0x8C, 0x61, 0x23};
static const unsigned char IDBT_ATR[]   = { 0x3B, 0xD5, 0x18, 0xFF, 0x81, 0xB1, 0xFE, 0x43, 0x1F, 0xC3, 0x80, 0x73, 0xC8, 0x21, 0x10, 0x69};
static const unsigned char AV3_ATR[] 	= { 0x3B, 0xDF, 0x18, 0xFF, 0x81, 0xF1, 0xFE, 0x43, 0x00, 0x3F, 0x07, 0x83, 0x4D, 0x49, 0x46, 0x41,
								            0x52, 0x45, 0x20, 0x53, 0x41, 0x4D, 0x20, 0x41, 0x56, 0x33, 0x30, 0x11 };

//=====================================================================================================================








//=====================================================================================================================
// Try to define the SAM that is in the system
// Verify all known SAM ATR's
// Warm ATR is one byte shorter than Cold ATR
// If found, do some initialization of the know SAM
//=====================================================================================================================
int FindSamType(unsigned char *ATR, int len)
{
	sam_type = sam_type_unknown; 
    sprintf(sam_atype,"%s","UNKNOWN");

    //-------------------------------------------------------------------------
	if ( len == sizeof(IDBT_ATR) ) 
    {
       if( memcmp(ATR, IDBT_ATR, len ) ==0) 
       {
            sam_type = sam_type_idbt;
            sprintf(sam_atype,"%s","IDBT");
       }
	}
    //-------------------------------------------------------------------------
	if ( len == sizeof(S9_ATR) ) 
    {
       if( memcmp(ATR, S9_ATR, len ) ==0) 
       {
            sam_type = sam_type_s9;
            sprintf(sam_atype,"%s","S9");
       }
	}
    //-------------------------------------------------------------------------
	if ( len == sizeof(AV3_ATR) ) 
    {
       if( memcmp(ATR, AV3_ATR, len ) ==0) 
       {
            sam_type = sam_type_av3;
            sprintf(sam_atype,"%s","AV3");
       }
	}
    //-------------------------------------------------------------------------
	if ( len == sizeof(SAMS1_ATR) ) 
    {
       if( memcmp(ATR, SAMS1_ATR, len ) ==0) 
       {
            sam_type = sam_type_sams1;
            sprintf(sam_atype,"%s","S1");
       }
	}
    //-------------------------------------------------------------------------
	if ( len == sizeof(NSAM2_ATR) ) 
    {
       if( memcmp(ATR, NSAM2_ATR, len ) ==0) 
       {
            sam_type = sam_type_nsam2;
            sprintf(sam_atype,"%s","NSAM");
       }
	}
    //-------------------------------------------------------------------------

    if(DEBUG) printf("sam_type = [%s] \n\r",sam_atype);

    return(sam_type);
}
//=====================================================================================================================


//=====================================================================================================================
static void samci_hex_dump( unsigned char * buf, unsigned short len ){
    unsigned short idx;

    for( idx = 0; idx < len; ++idx ){
        if( idx && idx % 16 == 0) 
            fprintf(stderr, "\r\n");
        fprintf( stderr,"%02X ", buf[ idx ] );   
    }
    fprintf( stderr, "\r\n\r\n" );
}
//=====================================================================================================================



//=====================================================================================================================
static int sam_idbt_get_serial( unsigned char nsam )
{
    int rx_len, tx_len;
    unsigned char rx_buf[ 256 ];
    unsigned char *ptx_buf;
    unsigned short mode=1;
    unsigned char idbt_asn[32];
	unsigned char idbt_tsv[16];
	unsigned char idbt_tiv[16];
	int val;

    mode=1;
    libpdhw_sam_set_T1_TLC (nsam , &mode );



    ptx_buf = ( unsigned char * )cmd_get_FCI ;
    tx_len  = sizeof( cmd_get_FCI ) ;

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf, tx_len , rx_buf, sizeof( rx_buf ) ) ) < 0 )
    {
       goto sam_idbt_get_serial_err_resp;
    }



    if(rx_len < 32)
    {
        printf( "samslot=%d:type=%s:serialnr=UNKNOWN\n\r", nsam, sam_atype);
    }
    else
    {

        /*
        printf( "samslot=%d:type=%s:serialnr=%02X%02X%02X\n", nsam, sam_atype,
                    rx_buf[ 2 ], rx_buf[ 1 ], rx_buf[ 0 ] );
        */



		val = (rx_buf[0x1D] << 24) + (rx_buf[0x1E] << 16) + (rx_buf[0x1F] <<  8) + rx_buf[0x20];
		sprintf ((char *) idbt_asn, "%X", val);

		val = (rx_buf[0x14] <<  8) + rx_buf[0x15];
		sprintf ((char *) idbt_tsv, "TSV: %X", val);

		val = (rx_buf[0x19] <<  8) + rx_buf[0x1A];
		sprintf ((char *) idbt_tiv, "TIV: %X", val);


		if(DEBUG) printf("SamType        = IDBT\n\r");
		if(DEBUG) printf("SamSerial      = %s\n\r" , (char *) idbt_asn);
		if(DEBUG) printf("SamExtraInfo1  = %s\n\r" , (char *) idbt_tsv);
		if(DEBUG) printf("SamExtraInfo2  = %s\n\r" , (char *) idbt_tiv);

        printf( "samslot=%d:type=%s:serialnr=%s\n",
                    nsam, sam_atype,idbt_asn);
   
    }

    return 0;



    
sam_idbt_get_serial_err_resp:
    fprintf( stderr, "error libpdhw_sam_tranceive invalid response :\n" );
    fprintf( stderr, "command :\n");
    samci_hex_dump( ptx_buf, tx_len );
    fprintf( stderr, "response :\n" );
    samci_hex_dump( rx_buf, rx_len );
    return -1;

}
//=====================================================================================================================





//=====================================================================================================================
static int sam_av3_get_serial( unsigned char nsam )
{
    int rx_len, tx_len;
    unsigned char rx_buf[ 256 ];
    unsigned char * ptx_buf;
	unsigned char av3_serial[32];
	unsigned char av3_state[32];

   
    unsigned short mode=1;
    libpdhw_sam_set_T1_TLC (nsam , &mode );


    ptx_buf = ( unsigned char * )cmd_GetVersion ;
    tx_len  = sizeof( cmd_GetVersion ) ;

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf, tx_len , rx_buf, sizeof( rx_buf ) ) ) < 0 )
    {
       goto sam_av3_get_serial_err_resp;
    }


    if(rx_len < 32)
    {
        printf( "samslot=%d:type=%s:serialnr=UNKNOWN\n\r", nsam, sam_atype);
    }
    else
    {
        // Get serial
        for (int n = 0; n < 7; n++)
		{
			sprintf((char *) av3_serial + n*2, "%02X", rx_buf[14+n]);
		}

        // Get state
		sprintf((char *) av3_state, "[%s]", rx_buf[30] == 0x03 ? "Not Activated" : "Activated");


		if(DEBUG) printf("SamSerial: %s\n\r", av3_serial );
		if(DEBUG) printf("SamState : %s\n\r", av3_state  );

        printf( "samslot=%d:type=%s:serialnr=%s\n", nsam, sam_atype,av3_serial
        );
    }
    return 0;

   
sam_av3_get_serial_err_resp:
    fprintf( stderr, "error libpdhw_sam_tranceive invalid response :\n" );
    fprintf( stderr, "command :\n");
    samci_hex_dump( ptx_buf, tx_len );
    fprintf( stderr, "response :\n" );
    samci_hex_dump( rx_buf, rx_len );
    return -1;


}
//=====================================================================================================================






//=====================================================================================================================

// The C-APDU's
static const unsigned char VerifyCHV[] 		= { 0x00, 0x20, 0x00, 0x80, 0x08, 0x2C, 0x21, 0x96, 0x28, 0x90, 0x60, 0x76, 0xFF };
static const unsigned char SelectSnFile[] 	= { 0x90, 0xA4, 0x02, 0x00, 0x02, 0x00, 0x02 };
static const unsigned char ReadSn[] 		= { 0x00, 0xB0, 0x00, 0x00, 0x08 };
static const unsigned char SelectInfoFile[]	= { 0x90, 0xA4, 0x02, 0x00, 0x02, 0x09, 0x02 };
static const unsigned char ReadInfo[] 		= { 0x00, 0xB0, 0x00, 0x00, 0x17 };




int sam_s9_get_serial( unsigned char nsam ){
    int rx_len, tx_len;
    unsigned char rx_buf[ 128 ];
    unsigned char *ptx_buf;
    unsigned short mode=0;
    int ret;
    int DeviceId;
    int OperatorId;


    if(DEBUG) printf("sam_s9_get_serial()\n\r");

    libpdhw_sam_set_T1_TLC (nsam , &mode );

    //=================================================================================================================
    if(DEBUG) printf("\nVerifyCHV ...\n\r");

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )VerifyCHV, tx_len = sizeof( VerifyCHV ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto err_resp;

    ret =  rx_buf[ rx_len -2 ] *256 + rx_buf[ rx_len -1 ];

    switch(ret)
    {
        case 0x9000:
                        break;
        case 0x6983:
                        printf("    Key or PIN blocked  Error: ( %04X ) \n\r",ret);
                        goto err_resp;
                        break;
        default:
                        printf("    Error code : %04X\n\r",ret);
                        goto err_resp;
                        break;
    }
    if(DEBUG) printf("VerifyCHV OK\n\r");
    //=================================================================================================================



    //=================================================================================================================
    if(DEBUG) printf("\nSelectInfoFile ... \n\r");
    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )SelectInfoFile, tx_len = sizeof( SelectInfoFile ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto err_resp;
    if(DEBUG) printf("SelectInfoFile OK\n\r");
    //=================================================================================================================


    //=================================================================================================================
    if(DEBUG) printf("ReadInfo ... \n\r");

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )ReadInfo, tx_len = sizeof( ReadInfo ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto err_resp;

    if(DEBUG) printf("ReadInfo OK\n\r");

    // Save Device ID, Operator ID
	DeviceId = (rx_buf[3] << 24) + (rx_buf[2] << 16) + (rx_buf[1] <<  8) + rx_buf[0];
	if(DEBUG)printf("Device Id    : %X\n\r", DeviceId);

	OperatorId = (rx_buf[7] << 24) + (rx_buf[6] << 16) + (rx_buf[5] <<  8) + rx_buf[4];
	if(DEBUG)printf("Operator Id  : %X\n\r", OperatorId);
    //=================================================================================================================


    //=================================================================================================================
    if(DEBUG) printf("\nSelectSnFile ... \n\r");

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )SelectSnFile, tx_len = sizeof( SelectSnFile ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto err_resp;
    if( rx_buf[ rx_len -2 ] != 0x61 )
        goto err_resp;
    if(DEBUG) printf("SelectSnFile OK \n\r");
    //=================================================================================================================

    printf( "samslot=%d:type=%s:serialnr=%06X\n", nsam, sam_atype,  DeviceId   );

    return 0;







    //=================================================================================================================
    if(DEBUG) printf("ReadSn ... \n\r");

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )ReadSn, tx_len = sizeof( ReadSn ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto err_resp;

    printf( "samslot=%d:type=%s:serialnr=%02X%02X-%02X%02X-%02X%02X-%02X%02X:DeviceID:%06X:OperatorId:%X\n", 
                nsam, sam_atype,
                rx_buf[ 0 ],rx_buf[ 1 ], 
                rx_buf[ 2 ],rx_buf[ 3 ], 
                rx_buf[ 4 ],rx_buf[ 5 ], 
                rx_buf[ 6 ],rx_buf[ 7 ] ,
                DeviceId,
                OperatorId


     );
    //=================================================================================================================


    return 0;

err_resp:
    fprintf( stderr, "error libpdhw_sam_tranceive invalid response :\n" );
    fprintf( stderr, "command :\n");
    samci_hex_dump( ptx_buf, tx_len );
    fprintf( stderr, "response :\n" );
    samci_hex_dump( rx_buf, rx_len );
    return -1;


}
//=====================================================================================================================








//=====================================================================================================================
static int samci_arriva_sam_get_serial( unsigned char nsam ){
    int rx_len, tx_len;
    unsigned char rx_buf[ 128 ];
    unsigned char * ptx_buf;
    unsigned short mode=0;

    mode=0;
    libpdhw_sam_set_T1_TLC (nsam , &mode );
    
    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )cmd_chv, tx_len = sizeof( cmd_chv ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto arriva_sam_get_serial_err_resp;
    if( rx_buf[ rx_len -2 ] != 0x90 && rx_buf[ rx_len -1 ] != 0x00 )
        goto arriva_sam_get_serial_err_resp;

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )cmd_select_file, tx_len = sizeof( cmd_select_file ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto arriva_sam_get_serial_err_resp;
    if( rx_buf[ rx_len -2 ] != 0x61 && rx_buf[ rx_len -1 ] != 0x16 )
        goto arriva_sam_get_serial_err_resp;

    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )cmd_get_response, tx_len = sizeof( cmd_get_response ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto arriva_sam_get_serial_err_resp;
    if( rx_buf[ rx_len -2 ] != 0x90 && rx_buf[ rx_len -1 ] != 0x00 )
        goto arriva_sam_get_serial_err_resp;
    
    if( ( rx_len = libpdhw_sam_tranceive( nsam, ptx_buf = ( unsigned char * )cmd_read_binary, tx_len = sizeof( cmd_read_binary ), rx_buf, sizeof( rx_buf ) ) ) < 0 )
        goto arriva_sam_get_serial_err_tran;
    if( rx_buf[ rx_len -2 ] != 0x90 && rx_buf[ rx_len -1 ] != 0x00 )
        goto arriva_sam_get_serial_err_resp;

    printf( "samslot=%d:type=%s:serialnr=%02X%02X%02X\n", nsam,sam_atype, 
                    rx_buf[ 2 ], rx_buf[ 1 ], rx_buf[ 0 ] );
    return 0;

arriva_sam_get_serial_err_resp:
    fprintf( stderr, "error libpdhw_sam_tranceive invalid response :\n" );
    fprintf( stderr, "command :\n");
    samci_hex_dump( ptx_buf, tx_len );
    fprintf( stderr, "response :\n" );
    samci_hex_dump( rx_buf, rx_len );
    return -1;

arriva_sam_get_serial_err_tran:
    fprintf( stderr, "error libpdhw_sam_tranceive : %d\n", rx_len );
    return rx_len;
}
//=====================================================================================================================



//=============================================================================
void show_ATR(unsigned char *ATR, int len)
{
    int i;

    if(!DEBUG) return;

    printf("ATR[%d]: ",len);
    for (i=0; i < len ; i++)
    {
        printf("%02X ",ATR[i]);
    }
    printf("\n");
}
//=============================================================================


void show_calipso_history(struct calypso_hist *pch)
{

    if(!DEBUG) return;
    printf("ATR history:");

    printf("%02X ", pch->t1 );
    printf("%02X ", pch->t2 );
    printf("%02X ", pch->t3 );
    printf("%02X ", pch->t4 );
    printf("%02X ", pch->t4 );
    printf("%02X ", pch->t6 );
    printf("%02X ", pch->t7 );
    printf("%02X ", pch->t8 );

    printf("[ %02X ", pch->sn[0] );
    printf("%02X ", pch->sn[1] );
    printf("%02X ", pch->sn[2] );
    printf("%02X ] ", pch->sn[3] );

    printf("%02X ", pch->t13 );
    printf("%02X ", pch->t14 );
    printf("%02X ", pch->t15 );

    printf("\n\r\n\r");

}




//=====================================================================================================================
static void samci_parse_atr( int nsam ){
    int res, len, ofs, ofs_tdx, ofs_his, his_len, ofs_iss_dat = -1, idx_his; 
    // int len_pre_iss_dat = 0; 
    // int len_iss_dat = 0;
    // int ofs_pre_iss_dat = -1,
    unsigned char atr[MAX_ATR];
    struct calypso_hist *pch;



    memset(atr,0x00, MAX_ATR);

   //printf("samci_parse_atr(%d) \n\r",nsam);

    //--------------------------------------------------------------------------
    if( ( res = libpdhw_sam_get_atr(nsam, atr, sizeof(atr) ) ) < 0 ){
        printf( "error %d reading ATR \n\r", res );
        printf( "samslot=%d:type=UNKNOWN:serialnr=UNKNOWN\n\r", nsam );
        return;
    }
    len = res;
    //--------------------------------------------------------------------------
  

    show_ATR(atr,len);
    FindSamType(atr,len);




    his_len = atr[ 1 ] & 0x0F;

    if(DEBUG) printf( "ATR hist = %d\n\r", his_len );    

    /* search the historical data */
    for( ofs = 2, ofs_tdx = 1; ofs < len; ) {
        if( atr[ ofs_tdx ] & 0x10 ) ++ofs;
        if( atr[ ofs_tdx ] & 0x20 ) ++ofs;
        if( atr[ ofs_tdx ] & 0x40 ) ++ofs;
        if( ! ( atr[ ofs_tdx ] & 0x80 ) )
            break;
        ofs_tdx = ofs;
        ++ofs;
    }
    if( ofs >= len ) {
        fprintf( stderr, "invalid ATR\n" );
        return;
    }
    ofs_his = ofs;


    /* search for issuer and pre issuer data */
    for( idx_his = ofs_his + 1; idx_his < ofs_his + his_len; ) {
        if( ( atr[ idx_his ] & 0xF0 ) == TAG_ISS_DAT ) {
            ofs_iss_dat = idx_his;
            //len_iss_dat = atr[ idx_his ] & 0x0F; 
        }
        if( ( atr[ idx_his ] & 0xF0 ) == TAG_PRE_ISS_DAT ) {
            //ofs_pre_iss_dat = idx_his;
            //len_pre_iss_dat = atr[ idx_his ] & 0x0F; 
        }
        idx_his += atr[ idx_his ] & 0x0F;
        idx_his++;
        if( idx_his >= his_len - 3 )
            break;
    }





    pch = ( struct calypso_hist * )& atr[ ofs_his ];
    show_calipso_history(pch);




    switch ( sam_type )
    {
        case sam_type_nsam2:
                        samci_arriva_sam_get_serial(nsam);
                        return;
        case sam_type_s9:
                        sam_s9_get_serial(nsam) ;
                        //samci_arriva_sam_get_serial(nsam);
                        return;
        case sam_type_av3 :
                        sam_av3_get_serial(nsam) ;
                        return;

        case sam_type_idbt:
                        sam_idbt_get_serial(nsam) ;
                        return;
        default:    
                        break;
    }





    /* 
     * Fuzzy code to read the SAM serial number : 
     *  
     * - If there is issuer data assume it is a Calypso SAM and use the serial number in the 
     *   issuer data. 
     * - Otherwise assume it is a Arriva S9 sam and try to read the serial number 
     *   from a file. 
     */



    if( ofs_iss_dat > 0 && his_len >= (int)sizeof( struct calypso_hist ) )
    {
        /* found issuer data, asume it is a Calypso sam */
        pch = ( struct calypso_hist * )& atr[ ofs_his ];
       
       // printf( "samslot=%d:type=%02X%02X%02X%02X%02X%02X:serialnr=%02X%02X%02X%02X\n", nsam, 
        //                pch->t3, pch->t4, pch->t5, pch->t6, pch->t7,pch->t8,
        //                pch->sn[0], pch->sn[1], pch->sn[2], pch->sn[3] );

        printf( "samslot=%d:type=%s:serialnr=%02X%02X%02X%02X\n", nsam, 
                    sam_atype,
                    pch->sn[0], pch->sn[1], pch->sn[2], pch->sn[3] );


    } 
    else if( samci_arriva_sam_get_serial( nsam ) != 0 ) 
    {
        /* no issuer data found and unable to read serial number file */
        printf("Sam Type = %d \n\r",sam_type);
        printf( "samslot=%d:type=%s:serialnr=UNKNOWN\n"  , nsam,sam_atype );

    } 
}






//=====================================================================================================================
int CheckSam ( int SamSlot )
{
    int res;

    res = libpdhw_sam_open(SamSlot);
    if(res <= 0) 
    {
        fprintf(stderr, "error %d unable to open device %d\n", res, SamSlot) ;
        return -1;
    }

    samci_parse_atr(SamSlot);

    libpdhw_sam_close(SamSlot);

    return (0);
}
//=====================================================================================================================





//=====================================================================================================================
int main(int argc, char *argv[])
{
    int max_sam = 4;
    int nsam;
    int i;
	char * p;


	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		if( p[0] != '-') continue;

 		if(strcmp(p,"-debug")==0)
        { 
            DEBUG = 1;
            break;
        }
	}


    libpdhw_SetDebug(DEBUG);
    if(DEBUG) printf("\n\r\n\rSamSN (Build:%s)\n\r",Build_dateTime());
    if(DEBUG) libpdhw_version();




	for(i=1 ; i < argc ; i++)
	{


		p=argv[i];

		if( p[0] == '-') 
            continue;

         nsam = atoi(argv[i]);
         return(  CheckSam (nsam)  );
	
	}


    /*
    if( argc > 1 )
    {
        nsam = atoi(argv[1]);
        if( nsam < 0 || nsam > 4 ) {
            fprintf(stderr, "error SamSlot out of range\n" );
            return 1;
        }
        return(  CheckSam (nsam)  );
    }
    */

    for( nsam = 0; nsam < max_sam; ++nsam )
    {
        if(  CheckSam (nsam) < 0  )
            continue;
    }
 
    return (0);
}
//=====================================================================================================================







