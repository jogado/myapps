/*****************************************************************************
*
*      Copyright (C) 2021 ... emsyscon
*
* All rights reserved.
* 
* Permission to use, copy, modify, and distribute this software for any purpose
* with or without fee is hereby granted, provided that the above copyright
* notice and this permission notice appear in all copies.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
* NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
* DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
* OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
* OR OTHER DEALINGS IN THE SOFTWARE.
* 
* Except as contained in this notice, the name of a copyright holder shall not
* be used in advertising or otherwise to promote the sale, use or other dealings
* in this Software without prior written authorization of the copyright holder.
*
*******************************************************************************/

/*********************************************
* program to request S/N from SAM device 
* maintainer: Emsyscon Solutions
*********************************************/




struct calypso_hist {
   unsigned char t1;		/* category indicator, history bytes complying to ISO/IEC 7816-4	0x80 */
   unsigned char t2;		/* Tag COMPACT-TLV                                                  0x5A */
   unsigned char t3;		/* ChipType								                            0x06 */
   unsigned char t4;		/* application type: SAM						                    0x80 */
   unsigned char t5;		/* appliction subtype for the SAM-S1					            D[2/6/7] */
   unsigned char t6;		/* application issuer							                    0x08 */
   unsigned char t7;		/* Rom version: v3.0							                    0x30 */
   unsigned char t8;		/* EEPROM version							                        0xUV */
   unsigned char sn[4];	    /* SN3 */
   unsigned char t13;		/* COMPACT-TLV tag for ATR status complying to ISO/IEC 7816-4		0x82 */
   unsigned char t14;		/* SW 									                            0x90 0x91 */
   unsigned char t15;		/* SW									                            0x00 */ 
} __attribute__((packed));

#define MAX_ATR 32
#define TAG_ISS_DAT 0x50
#define TAG_PRE_ISS_DAT 0x60


// SAMS9
static const unsigned char cmd_chv          [] = { 0x00, 0x20, 0x00, 0x80, 0x08, 0x2c, 0x21, 0x96, 0x28, 0x90, 0x60, 0x76, 0xff };
static const unsigned char cmd_select_file  [] = { 0x90, 0xa4, 0x02, 0x00, 0x02, 0x09, 0x02 };
static const unsigned char cmd_get_response [] = { 0x00, 0xc0, 0x00, 0x00, 0x16 };
static const unsigned char cmd_read_binary  [] = { 0x00, 0xb0, 0x00, 0x00, 0x17 };


// SAM AV3
static const unsigned char cmd_GetVersion   [] = { 0x80, 0x60, 0x00, 0x00, 0x00 };

// IDBT
static const unsigned char cmd_get_FCI      [] = { 0x00, 0xA4, 0x04, 0x00, 0x0B, 0xD2, 0x76, 0x00, 0x00, 0x21, 0xFC, 0x53, 0x41, 0x4D, 0x56, 0x32, 0x00 };





static void samci_hex_dump( unsigned char * buf, unsigned short len );
static int  samci_arriva_sam_get_serial( unsigned char nsam );
static void samci_parse_atr( int nsam );




//=============================================================================
//Type Definitions
//=============================================================================
typedef enum
{
	sam_type_sam_not_present = 0,
	sam_type_unknown,

	sam_type_idbt,
	sam_type_idbt_test,

	sam_type_s9,
	sam_type_s9_test,

	sam_type_av3,
	sam_type_nsam2,
	sam_type_sams1,

} sam_type_e;



extern void show_ATR    (unsigned char *ATR, int len);
extern int  FindSamType (unsigned char *ATR, int len);


extern int CheckSam     ( int SamSlot );
extern void show_calipso_history(struct calypso_hist *pch);


extern int  sam_s9_get_serial( unsigned char nsam );

