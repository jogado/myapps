/*============================================================================
 *
 * Copyright (c) 2018-2020 EMSYSCON Solutions
 * All Rights Reserved.
 *
 * This software is  the  confidential and proprietary information of EMSYSCON
 * ('Confidential Information').  You  shall  not disclose  such  Confidential
 * Information  and  shall  use  it only in  accordance with  the terms of the
 * license agreement you entered into with EMYSCON.
 *
 * EMSYSCON makes no representations  or warranties about  the  suitability of
 * the software, either  express  or  implied, including  but  not  limited to
 * the implied warranties of merchantability, fitness for a particular purpose
 * or non-infringement. EMSYSCON shall  not be liable for any damages suffered
 * by licensee as the result of using, modifying or distributing this software
 * or its derivatives.
 *
 *==========================================================================*/
/*============================================================================
 *
 * Author(s):    Emsyscon Solutions
 * Contributors:
 * Date:
 * Purpose:
 * Description:  xxxx
 *
 *==========================================================================*/
/*============================================================================
 * TRACING and DEBUGGING Configuration
 *==========================================================================*/
//#define CONFIG_CH_SAM_HAVE_DIAGNOSTIC
#ifdef CONFIG_CH_SAM_HAVE_DIAGNOSTIC
# define TRACE(_fmt_, ...)          DEBUG_TRACE(_fmt_, ##__VA_ARGS__)
# define DUMP(_msg_,_a1_,_a2_)      DEBUG_DUMP(_msg_,_a1_,_a2_)
# define ASSERT(_c_,_msg_)          DEBUG_ASSERT(_c_,_msg_)
#else
# define TRACE(_msg_, ...)          EMPTY_STATEMENT
# define DUMP(_msg_,_a1_,_a2_)      EMPTY_STATEMENT
# define ASSERT(_c_,_msg_)          EMPTY_STATEMENT
#endif

/* *****************************************************************************************************************
 * Includes
 * ***************************************************************************************************************** */
#include "command_handler_sam.h"

#include "parameters/parameters.h"

#include "tag_handler/T_versions/T_versions.h"

#include "protocol/protocol.h"
#include "protocol/protocol_esc.h"

#include "tag_utilities/tag_info.h"
#include "tag_utilities/tag_lookup.h"
#include "tag_utilities/tag_emv_emsyscon.h"
#include "tag_utilities/tlv.h"

#include "util/tlv.h"

#include "board_iso7816/iso7816_core.h"

/* *****************************************************************************************************************
 * Internal Definitions
 * ***************************************************************************************************************** */
#define CH_SAM_MSG_QUEUE_SIZE		10
#define CH_SAM_DESTINATION_ADDRESS	2

/* *****************************************************************************************************************
 * Type Definitions
 * ***************************************************************************************************************** */
typedef enum
{
	sam_type_sam_not_present = 0,
	sam_type_unknown,

	sam_type_idbt,
	sam_type_idbt_test,

	sam_type_s9,
	sam_type_s9_test,

	sam_type_av3,

} sam_type_e;

/* *****************************************************************************************************************
 * Global and Static Variables
 * ***************************************************************************************************************** */
StaticTask_t	xChSamTaskBuffer;
StackType_t		xChSamTaskStack[CH_SAM_TASK_STACK_SIZE];

// The KNOWN Cold Reset IDBT_ATR (Warm ATR is one byte shorter)
//static const uint8_t xxxx_ATR[] = {0x3B, 0xF9, 0x11, 0x00, 0x00, 0x10, 0x80, 0x01, 0x01, 0x01, 0x11, 0x48, 0x38, 0x6E, 0x19, 0x7A };
static const uint8_t IDBT_ATR[] = {0x3B, 0xD5, 0x18, 0xFF, 0x81, 0xB1, 0xFE, 0x43, 0x1F, 0xC3, 0x80, 0x73, 0xC8, 0x21, 0x10, 0x69};
static const uint8_t S9_ATR[] 	= {0x3B, 0x5E, 0x18, 0x00, 0x00, 0x31, 0xC0, 0x65, 0x7C, 0xB5, 0x01, 0x00, 0x87, 0x71, 0xD6, 0x8C, 0x61, 0x23};
static const uint8_t AV3_ATR[] 	= {0x3B, 0xDF, 0x18, 0xFF, 0x81, 0xF1, 0xFE, 0x43, 0x00, 0x3F, 0x07, 0x83, 0x4D, 0x49, 0x46, 0x41,
								   0x52, 0x45, 0x20, 0x53, 0x41, 0x4D, 0x20, 0x41, 0x56, 0x33, 0x30, 0x11 };

static QueueHandle_t s_ch_sam_msgQ;

static uint8_t WarmAtr[EMVL1_MAX_TRANSFER_SIZE];
static uint32_t WarmAtrLength;

static uint8_t *pColdAtr;
static uint32_t ColdAtrLength;
static uint8_t atrout[100];

static uint8_t rxBuff[2048];

// THe IDBT SAM variables
static uint8_t idbt_ifs[70];
static uint32_t idbt_ifs_len;

static uint8_t sam_type;

/* *****************************************************************************************************************
 * External Functions Prototypes
 * ***************************************************************************************************************** */
extern int sprintf (char *buf, const char *fmt, ...);

/* *****************************************************************************************************************
 * Static Functions Prototypes
 * ***************************************************************************************************************** */
static board_api_status_t ch_sam_enq		(void *pItcb);
static board_api_status_t initialize_sam	(void);

static void initialize_idbt_sam				(void);
static void initialize_s9_sam				(void);
static void initialize_av3_sam				(void);
static void CHSAM_SendResponse				(ItcbComms_t *pComms, uint32_t Tag, uint8_t *pResp, uint32_t length);
static void CHSAM_SendError					(ItcbComms_t *pComms, uint32_t Tag, uint32_t errorcode);
static void CHSAM_SendNotImplemented		(ItcbComms_t *pComms, uint32_t Tag);

/* *****************************************************************************************************************
 * Public Functions
 * ***************************************************************************************************************** */
void command_handler_sam_task (void *params)
{
	Itcb_t *pItcb = NULL;
	board_api_status_t status;

	// No warnings required :-)
	UNUSED(params);

	// Sam type is unknown
	sam_type = sam_type_sam_not_present;

    /* Create a Message QUEUE. */
	s_ch_sam_msgQ = xQueueCreate(CH_SAM_MSG_QUEUE_SIZE, sizeof(void *));
    if (!s_ch_sam_msgQ)
    {
    	TRACE("Cannot create Message Queue");
    }

    // Register the Enqueue function
	register_enq_callback(CH_SAM_DESTINATION_ADDRESS, ch_sam_enq);

    // Initialize the SMARTCARD
	initialize_sam();

	// Show we are ready and wait for all tasks to be ready

    SetTaskReady(kTaskSync_ChSamTask);

    WaitForTask(kTaskSync_MainTask);

    // If first time failed...
    // We try 3 times to get the COLD reset ATR
    if (!ColdAtrLength)
    {
    	int n;
    	for (n = 0; n < 3; n++)
    	{
    		// Delay a little bit
    		vTaskDelay(200);
    		status = initialize_sam();
    		if (status == kBoard_Api_Success)
    			break;
    	}
    }

    // If Sam Exists Open the SAM
    // Also here, try maximum 3 times
    if (ColdAtrLength)
    {
    	int n;
    	for (n = 0; n < 3; n++)
    	{
    		// Delay a little bit
    		vTaskDelay(200);

    		WarmAtrLength = sizeof(WarmAtr);
			status = BOARD_smartcard_open(0, WarmAtr, &WarmAtrLength);
			if (status == kBoard_Api_Success)
				break;

			WarmAtrLength = 0;
    	}
    }

    // Try to define the SAM that is in the system
	// Verify all known SAM ATR's
    // Warm ATR is one byte shorter than Cold ATR
    // If found, do some initialization of the know SAM
    if (WarmAtrLength)
    {
		if ((WarmAtrLength == sizeof(IDBT_ATR) - 1) && !memcmp(WarmAtr, IDBT_ATR, sizeof(IDBT_ATR) - 1))
		{
			sam_type = sam_type_idbt;
			initialize_idbt_sam();
		}
		else
		if ((WarmAtrLength == sizeof(S9_ATR) - 1) && !memcmp(WarmAtr, S9_ATR, sizeof(S9_ATR) - 1))
		{
			sam_type = sam_type_s9;
			initialize_s9_sam();
		}
		else
			if ((WarmAtrLength == sizeof(AV3_ATR) - 1) && !memcmp(WarmAtr, AV3_ATR, sizeof(AV3_ATR) - 1))
			{
				sam_type = sam_type_av3;
				initialize_av3_sam();
			}
			else
		{
			sam_type = sam_type_unknown;
		}
    }


	// Print out SAM information if we have a known SAM
    // Information will be filled in by SAM initialization routines
	if (EscVersion[EscVersion_SamType])
	{
		PRINTF("SAM Type        : %s\r\n", EscVersion[EscVersion_SamType]);
	}

	if (EscVersion[EscVersion_SamSerial])
	{
		PRINTF("SAM Serial      : %s\r\n", EscVersion[EscVersion_SamSerial]);
	}

	if (EscVersion[EscVersion_SamExtraInfo1])
	{
		PRINTF("SAM Extra Info 1: %s\r\n", EscVersion[EscVersion_SamExtraInfo1]);
	}

	if (EscVersion[EscVersion_SamExtraInfo2])
	{
		PRINTF("SAM Extra Info 2: %s\r\n", EscVersion[EscVersion_SamExtraInfo2]);
	}

    // Loop treating incoming packets
	while (1)
	{
		// Release buffer if we have one
		if (pItcb)
		{
			if (pItcb->info.itcb_free)
			{
				pItcb->info.itcb_free(pItcb);
			}
			else
			{
				TRACE("NO Free Callback Function");
			}

			pItcb = NULL;
		}

		if (s_ch_sam_msgQ)
		{
			// This only returns if we have a message
			xQueueReceive(s_ch_sam_msgQ, &pItcb, portMAX_DELAY);

			// Verify if not a wrong pointer
			if (pItcb->info.itcb_magic != ITCB_MAGIC)
			{
				TRACE("Wrong MAGIC");
				pItcb = 0;
				continue;
			}

			// Verify if we can do something with the packet
			if ((pItcb->info.itcb_type != ItcbType_Sam) && (pItcb->info.itcb_type != ItcbType_Comms))
			{
				TRACE("Why does this packet come here? type:%u", pItcb->info.itcb_type);
				continue;
			}

			if (pItcb->info.itcb_type == ItcbType_Sam)
			{
				TRACE("SAM command received");
				continue;
			}


			if (pItcb->info.itcb_type == ItcbType_Comms)
			{
				// Map Comms Pointer
				ItcbComms_t *pComms = (ItcbComms_t *) pItcb;
				uint8_t	*pData;
				uint32_t Tag, DataLen;

				pData = pComms->pTlvStart;

				Tag 	= TLV_GetTLV_Tag   (&pData);
				DataLen = TLV_GetTLV_Length(&pData);

				// Check if Command OK
				if (!WarmAtrLength)
				{
					CHSAM_SendError(pComms, Tag, kError_SAMNotPresent);
				}
				else if (pComms->pTlvEnd != (pData + DataLen))
				{
					CHSAM_SendError(pComms, Tag, kError_DataFieldLengthError);
				}
				else
				{
					uint32_t RespLen;

					// Handle idbt type commands
					if (sam_type == sam_type_idbt)
					{
						// Handle the IFS Request
						switch (Tag)
						{
						case TAG_IFS:
							if (idbt_ifs_len)
							{
								CHSAM_SendResponse(pComms, Tag, idbt_ifs, idbt_ifs_len);
							}
							else
							{
								CHSAM_SendError(pComms, Tag, kError_SAMProtocolError);
							}

							// Handled command, so
							pItcb = NULL;
							continue;

						default:
							break;
						}
					}

					// Handle the rest of the commands
					switch (Tag)
					{
					case TAG_PICC_TYPE:
						CHSAM_SendResponse(pComms, Tag, &sam_type, 1);
						break;

					case TAG_HW_SAM_RESET:
						// CHeck Data length
						if (DataLen != 3)
						{
							CHSAM_SendError(pComms, Tag, kError_DataFieldLengthError);
						}
						RespLen = sizeof(rxBuff);
						status = BOARD_smartcard_reset(pData[0], rxBuff, &RespLen, pData[1], pData[2]);
						if (status != kBoard_Api_Success)
						{
							TRACE("Smartcard Reset Error (%X)", status);
							CHSAM_SendError(pComms, Tag, kError_SmartCardResetError);
						}
						else
						{
							CHSAM_SendResponse(pComms, Tag, rxBuff, RespLen);
						}
						break;

					case TAG_TRANSPARENT_DATA_SCR:
						status = BOARD_smartcard_transceive(0, pData, DataLen, rxBuff, &RespLen, NULL, NULL);
						if (status != kBoard_Api_Success)
						{
							if (status == kBoard_Api_SmartCard_Deactivated)
								CHSAM_SendError(pComms, Tag, kError_SmartcardDeactivated);
							else
								CHSAM_SendError(pComms, Tag, kError_ApduExchangeError);
						}
						else
						{
							CHSAM_SendResponse(pComms, Tag, rxBuff, RespLen);
						}
						break;

					case TAG_ATR:
						CHSAM_SendResponse(pComms, Tag, pColdAtr, ColdAtrLength);
						break;

					default:
						CHSAM_SendNotImplemented(pComms, Tag);
						break;
					}
				}

				// make sure we do NOT free the buffer
				pItcb = NULL;
				continue;
			}

		}
		else
		{
			vTaskDelay(1000);
		}
	}
}

void Ch_Sam_Task_Start(bool wait_for_start)
{
	task_handle[CH_SAM_TASK_ID] = xTaskCreateStatic(
			CH_SAM_TASK_FUNCTION_NAME,
			CH_SAM_TASK_TASK_NAME,
			CH_SAM_TASK_STACK_SIZE,
			NULL,
			CH_SAM_TASK_PRIORITY,
			xChSamTaskStack,
			&xChSamTaskBuffer
		);

	if (wait_for_start)
	{
		WaitForTask(kTaskSync_ChSamTask);
	}
	else
	{
		vTaskDelay(10);
	}
}

static board_api_status_t ch_sam_enq(void *pItcb)
{
	if (xQueueSendToBack(s_ch_sam_msgQ, &pItcb, 5000) != pdPASS)
	{
		TRACE("Waited too long for room in QUEUE");

		// Free the buffer
		((Itcb_t *)pItcb)->info.itcb_free(((Itcb_t *)pItcb));

		// Exit with error
		return kBoard_Api_Error;
	}
	return kBoard_Api_Success;
}


static board_api_status_t initialize_sam(void)
{
	board_api_status_t status;

    status = BOARD_smartcard_init();
    if (status == kBoard_Api_Success)
    {
    	uint32_t len = 0;
    	uint32_t atrlen;

    	status = BOARD_smartcard_get_atr(&pColdAtr, &ColdAtrLength);
        if (status == kBoard_Api_Success)
        {
        	atrlen = ColdAtrLength;
        	if (atrlen > (sizeof(atrout) / 3))
        	{
        		TRACE("Truncating ATR");
        		atrlen = sizeof(atrout)/3 - 1;
        	}

        	for (uint32_t idx = 0; idx < atrlen; idx++)
        	{
        		len += sprintf((char *) &atrout[len], "%02X ", pColdAtr[idx]);
        	}
        }
        else
        {
        	strcpy((char *) atrout, "Error reading SAM ATR");
        }
    }
    else
    {
    	strcpy((char *) atrout, "SAM Not Present");
    }

    // Save as version String TODO: Change this
    EscVersion[EscVersion_Sam1_ATR] = (char *) atrout;

    // return with status
    return status;
}


static void initialize_idbt_sam(void)
{
	board_api_status_t status;
	uint32_t val;
	uint32_t RespLen;
	uint8_t sw1, sw2;
	static uint8_t idbt_asn[32];
	static uint8_t idbt_tsv[16];
	static uint8_t idbt_tiv[16];

	// The C-APDU's
	static const uint8_t get_FCI[] = { 0x00, 0xA4, 0x04, 0x00, 0x0B, 0xD2, 0x76, 0x00, 0x00, 0x21, 0xFC, 0x53, 0x41, 0x4D, 0x56, 0x32, 0x00 };

	// Assume an error
	sam_type = sam_type_unknown;

	// Get SAM info
	do
	{
		// Get FCI
		status = BOARD_smartcard_transceive(0, get_FCI, sizeof(get_FCI), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || !RespLen)
		{
			break;
		}

		// Copy FCI data for later use
		idbt_ifs_len = 0;
		if (RespLen <= sizeof(idbt_ifs))
		{
			idbt_ifs_len = RespLen;
			memcpy(idbt_ifs, rxBuff, idbt_ifs_len);
		}

		// Extract data out of IFS
		// Hard CODED for the moment
		val = (rxBuff[0x1D] << 24) + (rxBuff[0x1E] << 16) + (rxBuff[0x1F] <<  8) + rxBuff[0x20];
		sprintf ((char *) idbt_asn, "%X", val);

		val = (rxBuff[0x14] <<  8) + rxBuff[0x15];
		sprintf ((char *) idbt_tsv, "TSV: %X", val);

		val = (rxBuff[0x19] <<  8) + rxBuff[0x1A];
		sprintf ((char *) idbt_tiv, "TIV: %X", val);

		// We know the SAM type
		sam_type = sam_type_idbt;

		// Save Serial Info
		EscVersion[EscVersion_SamType] 			= "IDBT";
		EscVersion[EscVersion_SamSerial]		= (char *) idbt_asn;
		EscVersion[EscVersion_SamExtraInfo1]	= (char *) idbt_tsv;
		EscVersion[EscVersion_SamExtraInfo2]	= (char *) idbt_tiv;
	}
	while (0);
}

static void initialize_s9_sam(void)
{
	board_api_status_t status;
	uint32_t val;
	uint32_t RespLen;
	uint8_t sw1, sw2;
	static uint8_t s9_serial[32];
	static uint8_t s9_device_id[32];
	static uint8_t s9_operator_id[32];

	// The C-APDU's
	static const uint8_t VerifyCHV[] 		= { 0x00, 0x20, 0x00, 0x80, 0x08, 0x2C, 0x21, 0x96, 0x28, 0x90, 0x60, 0x76, 0xFF };
	static const uint8_t SelectSnFile[] 	= { 0x90, 0xA4, 0x02, 0x00, 0x02, 0x00, 0x02 };
	static const uint8_t ReadSn[] 			= { 0x00, 0xB0, 0x00, 0x00, 0x00 };
	static const uint8_t SelectInfoFile[]	= { 0x90, 0xA4, 0x02, 0x00, 0x02, 0x09, 0x02 };
	static const uint8_t ReadInfo[] 		= { 0x00, 0xB0, 0x00, 0x00, 0x00 };

	// Assume an error
	sam_type = sam_type_unknown;

	// Try to access the SAM and read the serials
	do
	{
		// Verify the pin
		status = BOARD_smartcard_transceive(0, VerifyCHV, sizeof(VerifyCHV), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || RespLen || (sw1 != 0x90) || (sw2 != 0x00))
		{
			TRACE("VerifyPin Error (%02X%02X)", sw1, sw2);
			break;
		}

		// Select the serial number File
		status = BOARD_smartcard_transceive(0, SelectSnFile, sizeof(SelectSnFile), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || RespLen || (sw1 != 0x61))
		{
			TRACE("Select File Error (%02X%02X)", sw1, sw2);
			break;
		}

		// Read Serial Number
		status = BOARD_smartcard_transceive(0, ReadSn, sizeof(ReadSn), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || !RespLen)
		{
			TRACE("Read Serial Error (%02X%02X)", sw1, sw2);
			break;
		}

		// Save s9_serial in printable form......X
		sprintf((char *) s9_serial,"%02X%02X-%02X%02X-%02X%02X-%02X%02X",
				rxBuff[0], rxBuff[1],
				rxBuff[2], rxBuff[3],
				rxBuff[4], rxBuff[5],
				rxBuff[6], rxBuff[7]
			);


		// Select the Info File
		status = BOARD_smartcard_transceive(0, SelectInfoFile, sizeof(SelectInfoFile), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || RespLen || (sw1 != 0x61))
		{
			TRACE("Select Info File (%02X%02X)", sw1, sw2);
			break;
		}

		// Read Serial Number
		status = BOARD_smartcard_transceive(0, ReadInfo, sizeof(ReadInfo), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || !RespLen)
		{
			TRACE("Read Info Error (%02X%02X)", sw1, sw2);
			break;
		}

		// If we get here we definitely have an S9 SAM
		sam_type = sam_type_s9;

		// Save Device ID, Operator ID
		val = (rxBuff[3] << 24) + (rxBuff[2] << 16) + (rxBuff[1] <<  8) + rxBuff[0];
		sprintf((char *) s9_device_id, 	 "Device Id  : %X", val);

		val = (rxBuff[7] << 24) + (rxBuff[6] << 16) + (rxBuff[5] <<  8) + rxBuff[4];
		sprintf((char *) s9_operator_id, "Operator Id: %X", val);

		// Save Serial Info
		EscVersion[EscVersion_SamType] 			= "SAM S9";
		EscVersion[EscVersion_SamSerial]		= (char *) s9_serial;
		EscVersion[EscVersion_SamExtraInfo1]	= (char *) s9_device_id;
		EscVersion[EscVersion_SamExtraInfo2]	= (char *) s9_operator_id;
	}
	while (0);
}

static void initialize_av3_sam(void)
{
	board_api_status_t status;
	uint32_t RespLen;
	uint8_t sw1, sw2;
	static uint8_t av3_serial[32];
	static uint8_t av3_state[32];

	// The C-APDU's
	static const uint8_t GetVersion[] 		= { 0x80, 0x60, 0x00, 0x00, 0x00 };

	// Assume an error
	sam_type = sam_type_unknown;

	// Get SAM info
	do
	{
		// Get Sam Version
		status = BOARD_smartcard_transceive(0, GetVersion, sizeof(GetVersion), rxBuff, &RespLen, &sw1, &sw2);
		if ((status != kBoard_Api_Success) || !RespLen)
		{
			break;
		}

		// If we get here we definitely have an S9 SAM
		sam_type = sam_type_av3;

		// Get serial
		for (int n = 0; n < 7; n++)
		{
			sprintf((char *) av3_serial + n*2, "%02X", rxBuff[14+n]);
		}

		// Get state
		sprintf((char *) av3_state, "SAM State [%s]", rxBuff[30] == 0x03 ? "Not Activated" : "Activated");
	}
	while (0);

	// Save Serial Info
	EscVersion[EscVersion_SamType] 			= "SAM AV3";
	EscVersion[EscVersion_SamSerial]		= (char *) av3_serial;
	EscVersion[EscVersion_SamExtraInfo1]	= (char *) av3_state;
	EscVersion[EscVersion_SamExtraInfo2]	= (char *) 0;
}

static void CHSAM_SendResponse(ItcbComms_t *pComms, uint32_t Tag, uint8_t *pResp, uint32_t length)
{
	uint8_t *pTmp = pComms->pBuffer;

	// Output TAG and Length
	TLV_EmitTLV_Tag    (&pTmp, Tag);
	TLV_EmitTLV_Length (&pTmp, length);

	// Check for buffer size
	if ((length + (pTmp - pComms->pBuffer)) > pComms->buffer_size)
	{
		return CHSAM_SendError(pComms, Tag, kError_TransparentBufferLengthOverflow);
	}

	// Copy the data
	memmove(pTmp, pResp, length);
	pTmp += length;

	// Save length
	pComms->buffer_idx = pTmp - pComms->pBuffer;

	// Set Extra tag to send
	pComms->extra_tag = TAG_SUCCESS_TEMPLATE;

	// Reply the buffer
	pComms->info.itcb_reply(pComms);
}

static void CHSAM_SendNotImplemented(ItcbComms_t *pComms, uint32_t Tag)
{
	uint8_t *pTmp = pComms->pBuffer;

	// Output TAG and Length
	TLV_EmitTLV_Tag    (&pTmp, Tag);
	TLV_EmitTLV_Length (&pTmp, 0);

	// Save length
	pComms->buffer_idx = pTmp - pComms->pBuffer;

	// Set Extra tag to send
	pComms->extra_tag = TAG_UNSUPPORTED_TAG_TEMPLATE;

	// Reply the buffer
	pComms->info.itcb_reply(pComms);
}

static void CHSAM_SendError(ItcbComms_t *pComms, uint32_t Tag, uint32_t errorcode)
{
	uint8_t *pTmp = pComms->pBuffer;

	// Output TAG and Length and error code
	TLV_EmitTLV_Tag    (&pTmp, Tag);
	if (errorcode <= 0xFF)
	{
		*pTmp++ = 1;
		*pTmp++ = (uint8_t) errorcode;
	}
	else if (errorcode <= 0xFFFF)
	{
		*pTmp++ = 2;
		*pTmp++ = (uint8_t) ((errorcode >>  8) & 0xFF);
		*pTmp++ = (uint8_t) ((errorcode      ) & 0xFF);
	}
	else
	{
		*pTmp++ = 4;
		*pTmp++ = (uint8_t) ((errorcode >> 24) & 0xFF);
		*pTmp++ = (uint8_t) ((errorcode >> 16) & 0xFF);
		*pTmp++ = (uint8_t) ((errorcode >>  8) & 0xFF);
		*pTmp++ = (uint8_t) ((errorcode      ) & 0xFF);
	}

	// Save length
	pComms->buffer_idx = pTmp - pComms->pBuffer;

	// Set Extra tag to send
	pComms->extra_tag = TAG_FAILURE_TEMPLATE;

	// Reply the buffer
	pComms->info.itcb_reply(pComms);
}
