/*============================================================================
 *
 * Copyright (c) 2018-2019 EMSYSCON Solutions
 * All Rights Reserved.
 *
 * This software is  the  confidential and proprietary information of EMSYSCON
 * ('Confidential Information').  You  shall  not disclose  such  Confidential
 * Information  and  shall  use  it only in  accordance with  the terms of the
 * license agreement you entered into with EMYSCON.
 *
 * EMSYSCON makes no representations  or warranties about  the  suitability of
 * the software, either  express  or  implied, including  but  not  limited to
 * the implied warranties of merchantability, fitness for a particular purpose
 * or non-infringement. EMSYSCON shall  not be liable for any damages suffered
 * by licensee as the result of using, modifying or distributing this software
 * or its derivatives.
 *
 *==========================================================================*/

/*============================================================================
 *
 * Author(s):    Emsyscon Solutions - Luc Vansteenkiste
 * Contributors:
 * Date:		 01.02.2020
 * Purpose:		 Board smartcard functions
 *
 * File Name:	 board_smartcard.c
 *
 *==========================================================================*/
/*============================================================================
 * TRACING and DEBUGGING Configuration
 *==========================================================================*/
//#define CONFIG_BOARD_SMARTCARD_HAVE_DIAGNOSTIC
#ifdef CONFIG_BOARD_SMARTCARD_HAVE_DIAGNOSTIC
# define TRACE(_fmt_, ...)          DEBUG_TRACE(_fmt_, ##__VA_ARGS__)
# define DUMP(_msg_,_a1_,_a2_)      DEBUG_SLOWDUMP(_msg_,_a1_,_a2_)
# define ASSERT(_c_,_msg_)          DEBUG_ASSERT(_c_,_msg_)
#else
# define TRACE(_msg_, ...)          EMPTY_STATEMENT
# define DUMP(_msg_,_a1_,_a2_)      EMPTY_STATEMENT
# define ASSERT(_c_,_msg_)          EMPTY_STATEMENT
#endif

/* ****************************************************************************************************************
 * Required include files
 * ***************************************************************************************************************** */
#include "fsl_common.h"

#include "board.h"
#include "board_smartcard/board_smartcard.h"

#include "fsl_port.h"
#include "fsl_gpio.h"
#include "fsl_dspi.h"

#include "fsl_smartcard_freertos/fsl_smartcard_freertos.h"
#include "fsl_smartcard_phy.h"

#include "board_iso7816/iso7816_core.h"
#include "board_iso7816/iso7816_adapter_sdk.h"

#include "freertos.h"
#include "task.h"

/* *****************************************************************************************************************
 * Internal Definitions
 * ***************************************************************************************************************** */
#define ATRBUF_SIZE	100

/* *****************************************************************************************************************
 * Type Definitions
 * ***************************************************************************************************************** */

/* *****************************************************************************************************************
 * Global Variables
 * ***************************************************************************************************************** */
pInterruptFunction_t pSmartCard_InterruptFn;

/* *****************************************************************************************************************
 * Local Variables
 * ***************************************************************************************************************** */
static const uint8_t DiTable[16]  = {   0,   1,   2,   4,    8,   16,   32, 64, 12,  20,   0,    0,    0,    0, 0, 0};
static const uint16_t FiTable[16] = { 372, 372, 558, 744, 1116, 1488, 1860,  0,  0, 512, 768, 1024, 1536, 2048, 0, 0};

static smartcard_core_params_t		_smartcard_core_params;
static rtos_smartcard_context_t		_rtos_smartcard_context;

static uint8_t		_init_done;

static uint8_t	ColdAtr[EMVL1_MAX_TRANSFER_SIZE];
static uint32_t ColdAtrLength = 0;

static uint8_t		_sam_present;

/* *****************************************************************************************************************
 * Static function declarations
 * ***************************************************************************************************************** */

/* *****************************************************************************************************************
 * Function API
 * ***************************************************************************************************************** */
board_api_status_t BOARD_smartcard_init(void)
{
	rtos_smartcard_context_t *rtos_ctx = &_rtos_smartcard_context;
	smartcard_core_params_t *core = &_smartcard_core_params;
	smartcard_context_t *ctx = &rtos_ctx->x_context;
	int error;

	// Return if init already done
	if (!_init_done)
	{
		// Clear context variables
		memset((uint8_t *) rtos_ctx, 0, sizeof(rtos_smartcard_context_t));
		memset((uint8_t *) ctx,      0, sizeof(smartcard_context_t));
		memset((uint8_t *) core,     0, sizeof(smartcard_core_params_t));

		// Save RTOS context
		core->x_context = rtos_ctx;

    	// Select ISO7816 cards
    	core->EMVL1 = 0;

    	// Set up direction and level of the interface pins
    	{
    		gpio_pin_config_t in_config =
    		{
    			.pinDirection = kGPIO_DigitalInput,
    			.outputLogic = 0u
    		};
    		gpio_pin_config_t out0_config =
    		{
    			.pinDirection = kGPIO_DigitalOutput,
    			.outputLogic = 0u
    		};
    		gpio_pin_config_t out1_config =
    		{
    			.pinDirection = kGPIO_DigitalOutput,
    			.outputLogic = 1u
    		};


    		// Set up the Pin values
    		GPIO_PinInit(BOARD_SMARTCARD_RST_GPIO,			BOARD_SMARTCARD_RST_PIN,		&out0_config);
    		GPIO_PinInit(BOARD_SMARTCARD_VSEL0_GPIO,		BOARD_SMARTCARD_VSEL0_PIN,		&out0_config);
    		GPIO_PinInit(BOARD_SMARTCARD_VSEL1_GPIO,		BOARD_SMARTCARD_VSEL1_PIN,		&out1_config);
    		GPIO_PinInit(BOARD_SMARTCARD_IRQ_GPIO,  		BOARD_SMARTCARD_IRQ_PIN,		&in_config);
    	}

        // Configure I/O pin as Open Drain with internal pull-up and High Drive Strength
    	{
    		BOARD_SMARTCARD_IO_PORT->PCR[BOARD_SMARTCARD_IO_PIN] =
    			((BOARD_SMARTCARD_IO_PORT->PCR[BOARD_SMARTCARD_IO_PIN] &
    				  /* Mask bits to zero which are setting */
    				  (~(PORT_PCR_PS_MASK | PORT_PCR_PE_MASK | PORT_PCR_ODE_MASK | PORT_PCR_DSE_MASK | PORT_PCR_ISF_MASK)))

    				 /* Pull Select: Internal pullup resistor is enabled on the corresponding pin, if the
    				  * corresponding PE field is set. */
    				 | (uint32_t)(kPORT_PullUp)

    				 /* Open Drain Enable: Open drain output is enabled on the corresponding pin, if the pin is
    				  * configured as a digital output. */
    				 | PORT_PCR_ODE(kPORT_OpenDrainEnable))

    				 /* Drive strength on the corresponding pin, if the pin is
    				  * configured as a digital output. */
    				 | PORT_PCR_DSE(kPORT_HighDriveStrength);

    	}


    	// Setup the IOMUX
    	{
    		PORT_SetPinMux(BOARD_SMARTCARD_VSEL0_PORT,		BOARD_SMARTCARD_VSEL0_PIN,		kPORT_MuxAsGpio);
    		PORT_SetPinMux(BOARD_SMARTCARD_VSEL1_PORT,		BOARD_SMARTCARD_VSEL1_PIN,		kPORT_MuxAsGpio);
    		PORT_SetPinMux(BOARD_SMARTCARD_IRQ_PORT,		BOARD_SMARTCARD_IRQ_PIN,		kPORT_MuxAsGpio);

    		PORT_SetPinMux(BOARD_SMARTCARD_IO_PORT, 		BOARD_SMARTCARD_IO_PIN,			kPORT_MuxAlt2);
    		PORT_SetPinMux(BOARD_SMARTCARD_CLK_PORT,		BOARD_SMARTCARD_CLK_PIN,		kPORT_MuxAlt2);
    		PORT_SetPinMux(BOARD_SMARTCARD_RST_PORT, 		BOARD_SMARTCARD_RST_PIN,    	kPORT_MuxAlt2);
    		PORT_SetPinMux(BOARD_SMARTCARD_CONTROL_PORT,	BOARD_SMARTCARD_CONTROL_PIN,	kPORT_MuxAlt2);
    	}

        // Ungate RTC clock for EMVSIM functionality ????
        CLOCK_EnableClock(kCLOCK_Rtc0);

        // Configure the IRQ pin
        //PORT_SetPinInterruptConfig(BOARD_SMARTCARD_IRQ_PORT, BOARD_SMARTCARD_IRQ_PIN, kPORT_InterruptLogicZero);

        // Set Interrupt Priority
        // Set smartcard EMVSIM1 communication peripheral interrupt priority
        NVIC_SetPriority(BOARD_SMARTCARD_MODULE_IRQ, BOARD_SMARTCARD_MODULE_IRQ_PRIORITY);

        // Set smartcard presence detect gpio PORT pin interrupt priority
        NVIC_SetPriority(BOARD_SMARTCARD_IRQ_PIN_IRQ, BOARD_SMARTCARD_IRQ_PIN_IRQ_PRIORITY);

        // Enable Interrupt
        // NVIC_EnableIRQ(BOARD_SMARTCARD_IRQ_PIN_IRQ);
        NVIC_EnableIRQ(BOARD_SMARTCARD_MODULE_IRQ);

        // Initialize smartcard context
        SMARTCARD_PHY_GetDefaultConfig(&ctx->interfaceConfig);

        ctx->interfaceConfig.smartCardClock			= BOARD_SMARTCARD_CLOCK_VALUE;
        ctx->interfaceConfig.vcc					= kSMARTCARD_VoltageClassB3_3V; // kSMARTCARD_VoltageClassA5_0V;
        ctx->interfaceConfig.clockModule			= BOARD_SMARTCARD_CLOCK_MODULE;
        ctx->interfaceConfig.clockModuleChannel		= BOARD_SMARTCARD_CLOCK_MODULE_CHANNEL;
        ctx->interfaceConfig.clockModuleSourceClock	= BOARD_SMARTCARD_CLOCK_MODULE_SOURCE_CLK;

        ctx->interfaceConfig.controlPort			= BOARD_SMARTCARD_CONTROL_PORT_NUM;
        ctx->interfaceConfig.controlPin				= BOARD_SMARTCARD_CONTROL_PIN;

        ctx->interfaceConfig.irqPort				= BOARD_SMARTCARD_IRQ_PORT_NUM;
        ctx->interfaceConfig.irqPin					= BOARD_SMARTCARD_IRQ_PIN;

        ctx->interfaceConfig.resetPort				= BOARD_SMARTCARD_RST_PORT_NUM;
        ctx->interfaceConfig.resetPin				= BOARD_SMARTCARD_RST_PIN;

        ctx->interfaceConfig.vsel0Port				= BOARD_SMARTCARD_VSEL0_PORT_NUM;
        ctx->interfaceConfig.vsel0Pin				= BOARD_SMARTCARD_VSEL0_PIN;

        ctx->interfaceConfig.vsel1Port				= BOARD_SMARTCARD_VSEL1_PORT_NUM;
        ctx->interfaceConfig.vsel1Pin				= BOARD_SMARTCARD_VSEL1_PIN;

        // Install Time Delay
        ctx->timeDelay	= BOARD_timer_delay_us;

        // Initialize smartcard
    	error = SMARTCARD_RTOS_Init(BOARD_SMARTCARD_BASE, rtos_ctx, BOARD_SMARTCARD_CLOCK_MODULE_CLK_FREQ);
        if (error)
        {
        	return kBoard_Api_Error;
        }

		// We only do this once
    	_init_done = 1;
	}

	// Try to read COLD Reset ATR
	if (!ColdAtrLength)
	{
		// Read ATR after Cold Reset
		ColdAtrLength = 0;
		error = smartcard_reset_handle_atr(core, ColdAtr, kSMARTCARD_ColdReset, kSMARTCARD_NoWarmReset, &ColdAtrLength);
		if (error)
		{
			ColdAtrLength = 0;
			_sam_present = false;
			return kBoard_Api_Error;
		}
	}

    // Else all is good
	_sam_present = true;
    return kBoard_Api_Success;
}

board_api_status_t BOARD_smartcard_open(uint16_t sam_slot, uint8_t *pAtr, uint32_t *pAtrLen)
{
	UNUSED(sam_slot);

	// Init needs to be called first
	if (!_init_done)
	{
		return kBoard_Api_SmartCard_NotInitialized;
	}

	// Sanity check
	if (!pAtr || !pAtrLen)
	{
		return kBoard_Api_Parameter_Error;
	}


	// Reset Smartcard
	return BOARD_smartcard_reset(sam_slot, pAtr, pAtrLen, kSMARTCARD_WarmReset, 0);
}

board_api_status_t BOARD_smartcard_reset(uint16_t sam_slot, uint8_t *pAtr, uint32_t *pAtrLen, uint8_t resetMode, uint8_t speedFiDi)
{
	board_api_status_t status = kBoard_Api_Smartcard_WarmAtr_Error;
	rtos_smartcard_context_t *rtos_ctx = &_rtos_smartcard_context;
	smartcard_core_params_t *core = &_smartcard_core_params;
	uint32_t AtrLen, AtrMaxLen;
	uint16_t Fi, Di;
	uint8_t	LocalAtr[256];
	uint8_t selectedFiDi;
	int error;

	uint8_t ppsout[8];
	uint8_t ppsin [8];
	uint16_t i = 0;


	UNUSED(sam_slot);

	// Sanity check
	if (!pAtr || !pAtrLen)
	{
		return kBoard_Api_Parameter_Error;
	}

	// Verify speedFiDi
	selectedFiDi = speedFiDi ? speedFiDi : 0x11;
	if (speedFiDi)
	{
		Di = DiTable[selectedFiDi & 0xF];
		Fi = FiTable[selectedFiDi >> 4];
		if (!Di || !Fi)
		{
			return kBoard_Api_Parameter_Error;
		}
	}

	do
    {
		// Copy max length of buffer
		AtrMaxLen = *pAtrLen;

		// No Atr yet
		*pAtrLen = 0;

		// Clear destination buffer
        memset(pAtr, 0, AtrMaxLen);

#if 0
        // Check if we need to do a cold reset too
        if (!resetMode)
        {
        	TRACE("Cold Reset");
            AtrLen = 0;
			error = smartcard_reset_handle_atr(core, LocalAtr, kSMARTCARD_ColdReset, kSMARTCARD_NoWarmReset, &AtrLen);
			if (error)
			{
	        	TRACE("ColdReset Failed %u", error);
				return kBoard_Api_Smartcard_ColdAtr_Error;
			}
        }
#endif
        uint8_t coldreset = resetMode & 0xF0 ? kSMARTCARD_ColdReset : kSMARTCARD_NoColdReset;
        uint8_t warmreset = resetMode & 0x0F ? kSMARTCARD_WarmReset : kSMARTCARD_NoWarmReset;

        // Do a Warm Reset
//    	TRACE("Warm Reset with %u - %u", coldreset, warmreset);
        AtrLen = 0;
        error = smartcard_reset_handle_atr(core, LocalAtr, coldreset, warmreset, &AtrLen);
        if (error)
        {
        	TRACE("ATR Reset Failed %u", error);
        	break;
        }

		// Verify speedFiDi
		selectedFiDi = speedFiDi ? speedFiDi : rtos_ctx->x_context.cardParams.TA1;
		Di = DiTable[selectedFiDi & 0xF];
		Fi = FiTable[selectedFiDi >> 4];
		if (!Di || !Fi)
		{
			return kBoard_Api_Parameter_Error;
		}

//		TRACE(" TA1: %02X", rtos_ctx->x_context.cardParams.TA1);
//		TRACE("  Fi: %u", rtos_ctx->x_context.cardParams.Fi);
//		TRACE("  Di: %u", rtos_ctx->x_context.cardParams.Di);
//		TRACE(" CWI: %u", rtos_ctx->x_context.cardParams.CWI);
//		TRACE(" BGI: %u", rtos_ctx->x_context.cardParams.BGI);
//		TRACE(" BWI: %u", rtos_ctx->x_context.cardParams.BWI);
//		TRACE(" GTN: %u", rtos_ctx->x_context.cardParams.GTN);
//		TRACE("IFSC: %u", rtos_ctx->x_context.cardParams.IFSC);
//		TRACE("FiDi: %02X, Fi: %u, Di: %u", selectedFiDi, Fi, Di);

		// Select the mode
    	status = kBoard_Api_Smartcard_Protocol_Error;
        if (rtos_ctx->x_context.cardParams.t1Indicated)
        {
        	// Check if negotiable
        	if (rtos_ctx->x_context.cardParams.modeNegotiable)
        	{
        		// prepare PPS
				ppsout[i++] = 0xFF;
				ppsout[i++] = 0x11;
				ppsout[i++] = selectedFiDi;
				ppsout[i  ] = 0x00;

				for (uint16_t j = 0; j < i; j++)
					ppsout[i] ^= ppsout[j];

				// send out PPS
				for (int n = 0; n < 3; n++)
				{
					memset(ppsin, 0, 4);
					error = smartcard_send_pps(core, ppsout, ppsin, 4);
					if (error)
					{
						TRACE("Error Sending PPS: 0x%X", error);
					}
					else
					{
						break;
					}
				}

				// Exit if error
				if (error)
				{
					status = kBoard_Api_Smartcard_Pps_Error;
					break;
				}

				// Compare PPS
				if (memcmp(ppsout, ppsin, 4))
				{
					DUMP("PPS rx:", ppsin, 4);
					status = kBoard_Api_Smartcard_Pps_Error;
					break;
				}
//				TRACE("PPS with FiDi: 0x%02X Done OK", selectedFiDi);

				// Save new Fi
				rtos_ctx->x_context.cardParams.Fi =  Fi;

				// Save new Di
				rtos_ctx->x_context.cardParams.currentD =  Di;
//				TRACE("Changing baud rate to %u baud", (rtos_ctx->x_context.cardParams.currentD * 4800000) / rtos_ctx->x_context.cardParams.Fi);
        	}

        	// Setup T1 mode
//			error = smartcard_setup_t1(core);
//			if (error)
//			{
//				TRACE("Error setup t1: %u", error);
//				break;
//			}

			// Send IFS
        	// This also sets T=1 mode
	        uint32_t respLen = 0;
			error = smartcard_send_ifs(core, &respLen);
			if (error)
			{
				TRACE("Error sending IFS: %u", error);
				break;
			}

        }
        else if (rtos_ctx->x_context.cardParams.t0Indicated)
        {
        	if (rtos_ctx->x_context.cardParams.modeNegotiable)
        	{
//        		TRACE("T=0 Negotiable");

        		// prepare PPS
				ppsout[i++] = 0xFF;
				ppsout[i++] = 0x10;
				ppsout[i++] = selectedFiDi;
				ppsout[i  ] = 0x00;

				for (uint16_t j = 0; j < i; j++)
					ppsout[i] ^= ppsout[j];

				// send out PPS
				for (int n = 0; n < 3; n++)
				{
					memset(ppsin, 0, 4);
					error = smartcard_send_pps(core, ppsout, ppsin, 4);
					if (error)
					{
						TRACE("Error Sending PPS: 0x%X", error);
					}
					else
					{
						break;
					}
				}

				// Exit if error
				if (error)
				{
					status = kBoard_Api_Smartcard_Pps_Error;
					break;
				}

				// Compare PPS
				if (memcmp(ppsout, ppsin, 4))
				{
					DUMP("PPS rx:", ppsin, 4);
					status = kBoard_Api_Smartcard_Pps_Error;
					break;
				}
//				TRACE("PPS with FiDi: 0x%02X Done OK", selectedFiDi);

				// Save new Di
	        	rtos_ctx->x_context.cardParams.currentD = rtos_ctx->x_context.cardParams.Di;
        	}

        	// Setup T0 mode
			error = smartcard_setup_t0(core);
			if (error)
			{
				TRACE("Error setup t0: %u", error);
				break;
			}
        }
        else
        {
        	TRACE("T0 nor T1 indicated");
        	break;
        }


        // Verify if ATR buffer large enough
        if (AtrLen > AtrMaxLen)
        {
        	TRACE("ATR Overrun");
        	status = kBoard_Api_Buffer_Size_Error;
        	break;
        }

        // Copy the ATR
        *pAtrLen = AtrLen;
        memmove(pAtr, LocalAtr, AtrLen);

        // Return with success
        (void)status;
        status = kBoard_Api_Success;
    }
    while (0);

	return status;
}

board_api_status_t BOARD_smartcard_transceive(uint16_t sam_slot,
											  const uint8_t *pC_Apdu, uint32_t lC_Apdu,
											  uint8_t *pR_Apdu, uint32_t *plR_Apdu,
											  uint8_t *pSw1, uint8_t *pSw2)
{
	board_api_status_t status = kBoard_Api_Success;
	smartcard_core_params_t *core = &_smartcard_core_params;
	rtos_smartcard_context_t *rtos_ctx = &_rtos_smartcard_context;

	smartcard_tal_cmd_t talCmd = { 0 };
	smartcard_tal_resp_t talResp = { 0 };
	smartcard_core_error_t error;

	UNUSED(sam_slot);

	// Sanity checks
	if (!plR_Apdu)
		return kBoard_Api_Parameter_Error;

	// Check if Card still active
	if (!rtos_ctx->x_context.cardParams.active)
	{
		return kBoard_Api_SmartCard_Deactivated;
	}

	talCmd.apdu		= (uint8_t *) pC_Apdu;
	talCmd.length	= lC_Apdu;

	talResp.resp	= pR_Apdu;
	talResp.length	= 0;

	// CLear Result codes
	if (pSw1)
		*pSw1 = 0;
	if (pSw2)
		*pSw2 = 0;

	error = smartcard_tal_send_cmd(core, &talCmd, &talResp);
	if (error != kSCCoreSuccess)
	{
		TRACE("Error (%X)", error);
		return kBoard_Api_Error;
	}

	if (talResp.length >= 2)
	{
		if (pSw2)
			*pSw2 = talResp.resp[talResp.length - 1];
		if (pSw1)
			*pSw1 = talResp.resp[talResp.length - 2];

		if (pSw2 && pSw1)
			talResp.length -= 2;
	}

	// copy resulting length
	*plR_Apdu = talResp.length;

#if 0
	// Debugging
	int n = talResp.length;
	int m = 0;

	while (n)
	{
		int x = (n <= 32) ? n : 32;
		PRINTF("%03X: %s\r\n", m, hexdump(talResp.resp + m, x));
		m += x;
		n -= x;
	}
	PRINTF("\r\n");
#endif

	return status;
}

board_api_status_t BOARD_smartcard_get_atr(uint8_t **pBuf, uint32_t *pLen)
{
	board_api_status_t status = kBoard_Api_Success;

	do
	{
		// Check if parameter OK
		if ((pBuf == NULL) || (pLen == NULL))
		{
			status = kBoard_Api_Parameter_Error;
			break;
		}

		// Preset values
		*pBuf = NULL;
		*pLen = 0;

		// Clear resulting length
		// Check if we have an ATR
		// Exit if not
		if (!ColdAtrLength)
		{
			status = kBoard_Api_Smartcard_ColdAtr_Error;
			break;
		}

		// Save real values
		*pBuf = ColdAtr;
		*pLen = ColdAtrLength;
	}
	while (0);

	return status;
}

void BOARD_smartcard_register_interrupt(pInterruptFunction_t pFn)
{
	uint32_t regPrimask = DisableGlobalIRQ();

	pSmartCard_InterruptFn = pFn;

	// restore global IRQ's
	EnableGlobalIRQ(regPrimask);
}


/* IRQ handler for emvsim */
void BOARD_SMARTCARD_MODULE_IRQ_HANDLER(void)
{
    SMARTCARD_EMVSIM_IRQHandler(BOARD_SMARTCARD_BASE, &_rtos_smartcard_context.x_context);
    /* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
    exception return operation might vector to incorrect interrupt */
    __DSB();
}
