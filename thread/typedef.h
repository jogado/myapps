#ifndef	__TYPEDEFS_H__
#define	__TYPEDEFS_H__

/*
#define TRUE  (1==1)
#define FALSE (0==1)
*/

#define TRUE  1
#define FALSE 0


typedef	unsigned char	    u8,  *pu8;
typedef	unsigned char	    U8,  *pU8;
typedef	char	            s8,  *ps8;
typedef	char	            S8,  *pS8;


typedef	unsigned short	    u16,  *pu16;
typedef	unsigned short	    U16,  *pU16;
typedef	short	            s16,  *ps16;
typedef	short	            S16,  *pS16;

typedef	unsigned int	    u32,  *pu32;
typedef	unsigned int	    U32,  *pU32;
typedef	int	                s32,  *ps32;
typedef	int	                S32,  *pS32;

typedef	unsigned int	    U_INT;
typedef	unsigned int	    COLOR;


#endif



