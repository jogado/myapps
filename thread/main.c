#include "main.h"

#define MAX_TASKS 3
pthread_t tid[MAX_TASKS];


int count =0;

extern int 	main			( void       );
extern void 	*Task0			( void *arg  );
extern void 	*Task1			( void *arg  );
extern void 	*Task2			( void *arg  );
extern char 	*MyDateTime		( int mode   );
extern int 	GetTaskID		( void       );





tQUEUE		Q_USART0    , *pQ_USART0;
u16 		Buffer_USART0 [Q_SIZE];


typedef struct DateAndTime
{
    int year;
    int month;
    int day;
    int hour;
    int minutes;
    int seconds;
    int msec;
}DateAndTime;



char aMyDateTime[50];      // (DDD)HH:MM:SS.SSS
char aMyTime    [50];      // (DDD)HH:MM:SS.SSS


unsigned long Elapsed_ms = 0;

struct timeval tStartUp;

char *MyDateTime(int mode)
{
    DateAndTime date_and_time;
    struct timeval tCurrent;
    struct tm *tm;

    struct timeval tDiff;

    gettimeofday(&tCurrent, NULL);

    timersub(  &tCurrent, &tStartUp , &tDiff );

    tm = localtime(&tDiff.tv_sec);
//    tm = localtime(&tv.tv_sec);


    // Add 1900 to get the right year value
    // read the manual page for localtime()
    date_and_time.year 		= tm->tm_year + 1900;
    // Months are 0 based in struct tm
    date_and_time.month 	= tm->tm_mon + 1;
    date_and_time.day 		= tm->tm_mday;
    date_and_time.hour 		= tm->tm_hour;
    date_and_time.minutes 	= tm->tm_min;
    date_and_time.seconds 	= tm->tm_sec;
    date_and_time.msec 		= (int) (tDiff.tv_usec / 1000);


	if(mode==1)
	    sprintf(aMyDateTime, "%02d:%02d:%02d.%03d %02d-%02d-%04d",
    	    date_and_time.hour,
    	    date_and_time.minutes,
    	    date_and_time.seconds,
    	    date_and_time.msec,
    	    date_and_time.day,
    	    date_and_time.month,
    	    date_and_time.year
    		);
	else
	    sprintf(aMyTime, "%02d:%02d:%02d.%03d",
    	    date_and_time.hour,
    	    date_and_time.minutes,
    	    date_and_time.seconds,
    	    date_and_time.msec
    		);

    return aMyTime;
}


//=============================================================================
int GetTaskID(void)
{
	int Task_ID = -1;
	int i;

   	pthread_t id = pthread_self();

    	for(i=0; i < MAX_TASKS ; i++)
    	{
		if(pthread_equal(id,tid[i]))
		{
			Task_ID = i;
	        	printf("\n I'm task :%d \n\n",Task_ID);
			break;
		}
	}
	return(Task_ID);	
}
//=============================================================================







//=============================================================================
// This task just count elapsed milliseconds since startup
//=============================================================================
void *Task0(void *arg)
{
	int Task_ID;
	static int InitDone=0;

	if(InitDone++==0)
   	{	
		printf("Elased time Initialized\n\r");
		Elapsed_ms = 0;
 	}
	
	Task_ID = GetTaskID();
	if(Task_ID <0 )
    		return NULL;

	while(1)
   	{	
		Elapsed_ms ++ ;
		usleep(1000);
 	}
   	return NULL;
}
//=============================================================================




//=============================================================================
void *Task1(void *arg)
{
	int Task_ID;
	unsigned long i = 0;

	Task_ID = GetTaskID();
	if(Task_ID <0 )
    	return NULL;

	
	while(1)
   	{	
		printf("%s : Task(%d) :  Elapsed: %05d.%03d \n\r",
				MyDateTime(0),
				Task_ID,
				Elapsed_ms/1000,
				Elapsed_ms%1000);
		sleep(1);
 	}
   	return NULL;
}
//=============================================================================


//=============================================================================
void system_call (const char *s)
{
	int ret;
	ret=system( s );
	if(ret<0)
	{
		printf("system_call error %d\n\r",ret);
	}
}

void BCR_trigger_off(void)
{
	printf("BCR_trigger_off()\n\r");
	system_call("echo  0 > /sys/class/leds/barcode_trigger/brightness");
}

void BCR_trigger_on(void)
{
	printf("BCR_trigger_on()\n\r");
	system_call("echo  1 > /sys/class/leds/barcode_trigger/brightness");
}
//=============================================================================


struct timeval timer_1;
struct timeval timer_2;
struct timeval timer_delta;
void *Task2(void *arg)
{
	int Task_ID;
	unsigned long i = 0;
	struct tm *tm;

	Task_ID = GetTaskID();
	if(Task_ID <0 )
    	return NULL;



	gettimeofday(&timer_1, NULL);
		
	while(1)
   	{	

		//====================================
		printf("===================================\n\r");
		BCR_trigger_off();
		for (i=0; i < 5 ; i++)
	   	{
			printf(" Sleeping .... %d \n\r",i );
			sleep(1); 	
		}



		//====================================
		printf("\n\rTrigger ON \n\r");
		BCR_trigger_on();
		gettimeofday(&timer_1, NULL);
	



		usleep(20000);




	 	gettimeofday(&timer_2, NULL);
    		timersub(  &timer_2, &timer_1 , &timer_delta );
		tm = localtime(&timer_1.tv_sec);
		printf(" Got a BCR Charcater after : ( %03d ) ms\n\r", (int) (timer_delta.tv_usec / 1000));
		
		
   		sleep(1);
 	}
   	return NULL;
}





int main(void)
{
	int i = 0;
	int err;

	gettimeofday(&tStartUp, NULL);

	//==================================================
	// Initialise buffers
	//==================================================
	pQ_USART0 			      = &Q_USART0 ;
	pQ_USART0->QSize 		      = Q_SIZE ;
	pQ_USART0->pQ 			      = &Buffer_USART0[0];
	Q_Init (pQ_USART0);
   	//==================================================


	/*
    	i=0;
    	err = pthread_create(&(tid[i]), NULL, &Task0, NULL);
    	if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));

    	i=1;
    	err = pthread_create(&(tid[i]), NULL, &Task1, NULL);
    	if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));
	*/
    	i=2;
    	err = pthread_create(&(tid[i]), NULL, &Task2, NULL);
    	if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));


	while(1)
	{

	    sleep(1);
	}
	return 0;
}
