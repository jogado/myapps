#include<stdio.h>
#include<string.h>
#include<stdlib.h>


#include<typedef.h>
#include<queue.h>

void DISABLE_INTERRUPTS(void)
{
}
void ENABLE_INTERRUPTS(void)
{
}



//
////////////////////////////////////////////////////////////////////////////////////
//
// Queue Handling
//
////////////////////////////////////////////////////////////////////////////////////
//
u16 Q_Ninq ( tpQUEUE qp )
{
	if (qp)
		return (u16) qp->NInQ;
	else
		return 0;
}

void Q_Init ( tpQUEUE qp )
{
	DISABLE_INTERRUPTS();
	qp->NInQ  = 0;			// Nothing in the Queue
	qp->QInp  = 0;			// Set input pointer
	qp->QOutp = 0;			// Set Output pointer
	ENABLE_INTERRUPTS();
}

void Q_PeekInit ( tpQUEUE qp )
{
	DISABLE_INTERRUPTS();
	qp->QPeekp = qp->QOutp;	// Init the Peek Pointer
	ENABLE_INTERRUPTS();
}

u16 Q_Peek ( tpQUEUE qp )
{
	u16 rv = 0;

	if (qp && (qp->NInQ)) // && (qp->QPeekp != qp->QInp))
	{
		DISABLE_INTERRUPTS();
		rv = qp->pQ[qp->QPeekp++];
       	if (qp->QPeekp >= qp->QSize)
			qp->QPeekp = 0;
		ENABLE_INTERRUPTS();
	}
	return rv;
}

u16 Q_Get ( tpQUEUE qp)
{
	u16 rv = 0;

	if (qp && (qp->NInQ))
	{
		DISABLE_INTERRUPTS();
		rv = qp->pQ[qp->QOutp++];
       	if (qp->QOutp >= qp->QSize)
			qp->QOutp = 0;
		qp->NInQ--;
		ENABLE_INTERRUPTS();
	}
	return rv;
}

u16 Q_Put ( tpQUEUE qp, u16 ch )
{
	if (qp && (qp->NInQ < qp->QSize))
	{
		DISABLE_INTERRUPTS();

		qp->pQ[qp->QInp] =  ch;
		qp->QInp++;

      if (qp->QInp >= qp->QSize)
			qp->QInp = 0;

		qp->NInQ++;
		ENABLE_INTERRUPTS();

		return (0);
	}
	else
	{
		return (1);
	}
}
//======================================================================================================================






//======================================================================================================================
u16 Q32_Ninq ( tpQUEUE32 qp )
{
	if (qp)
		return (u16) qp->NInQ;
	else
		return 0;
}
//======================================================================================================================
void Q32_Init ( tpQUEUE32 qp )
{
	DISABLE_INTERRUPTS();
	qp->NInQ  = 0;			// Nothing in the Queue
	qp->QInp  = 0;			// Set input pointer
	qp->QOutp = 0;			// Set Output pointer
	ENABLE_INTERRUPTS();
}
//======================================================================================================================
void Q32_PeekInit ( tpQUEUE32 qp )
{
	DISABLE_INTERRUPTS();
	qp->QPeekp = qp->QOutp;	// Init the Peek Pointer
	ENABLE_INTERRUPTS();
}
//======================================================================================================================
u16 Q32_Peek ( tpQUEUE32 qp )
{
	u16 rv = 0;

	if (qp && (qp->NInQ)) // && (qp->QPeekp != qp->QInp))
	{
		DISABLE_INTERRUPTS();
		rv = qp->pQ[qp->QPeekp++];
       	if (qp->QPeekp >= qp->QSize)
			qp->QPeekp = 0;
		ENABLE_INTERRUPTS();
	}
	return rv;
}
//======================================================================================================================
u32 Q32_Get ( tpQUEUE32 qp)
{
	u32 rv = 0;

	if (qp && (qp->NInQ))
	{
		DISABLE_INTERRUPTS();
		rv = qp->pQ[qp->QOutp++];
       	if (qp->QOutp >= qp->QSize)
			qp->QOutp = 0;
		qp->NInQ--;
		ENABLE_INTERRUPTS();
	}
	return rv;
}
//======================================================================================================================
u16 Q32_Put ( tpQUEUE32 qp, u32 ch )
{
	if (qp && (qp->NInQ < qp->QSize))
	{
		DISABLE_INTERRUPTS();

		qp->pQ[qp->QInp] =  ch;
		qp->QInp++;

      if (qp->QInp >= qp->QSize)
			qp->QInp = 0;

		qp->NInQ++;
		ENABLE_INTERRUPTS();

		return (0);
	}
	else
	{
		return (1);
	}
}
//======================================================================================================================









