#ifndef	__QUEUE_H__
#define	__QUEUE_H__


#define 		Q_SIZE   2048

typedef volatile unsigned short int		vu16, *pvu16;
typedef volatile unsigned int	      	vu32, *pvu32;

typedef struct _queue
{
	vu16	QInp;		// Queue input pointer
	vu16	QOutp;		// Queue output pointer
	vu16	QPeekp;		// Queue Peek Pointer
	vu16	NInQ;		// Number in the queue
	u16		QSize;   	// The Queue size
	pu16	pQ;			// Pointer to QUEUE
}
tQUEUE, *tpQUEUE;


//==============================================================================
// The Queue Functions
//==============================================================================
extern void		Q_Init		( tpQUEUE );
extern u16		Q_Get		( tpQUEUE );
extern u16		Q_Put   	( tpQUEUE, u16 );
extern u16		Q_Ninq  	( tpQUEUE );
extern void		Q_PeekInit	( tpQUEUE );
extern u16		Q_Peek		( tpQUEUE );

//==============================================================================
// The Queue32 Functions
//==============================================================================

#define INIT32_PAD 0x4A4F5345
typedef struct _queue32
{
	vu16	QInp;			// Queue input pointer
	vu16	QOutp;		   	// Queue output pointer
	vu16	QPeekp;		   	// Queue Peek Pointer
	vu16	NInQ;			// Number in the queue
   	u16		QSize;   		// The Queue size
	pu32	pQ;				// Pointer to QUEUE
   u32	Initialised;  		// Initialisation flag
}
tQUEUE32, *tpQUEUE32;

extern void		Q32_Init		( tpQUEUE32 );
extern u32		Q32_Get			( tpQUEUE32 );
extern u16		Q32_Ninq  		( tpQUEUE32 );
extern void		Q32_PeekInit	( tpQUEUE32 );
extern u16		Q32_Peek		( tpQUEUE32 );
extern u16		Q32_Put   		( tpQUEUE32, u32 val);


extern void DISABLE_INTERRUPTS(void);
extern void ENABLE_INTERRUPTS(void);


#endif
