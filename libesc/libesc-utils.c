#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libesc.h"

void hexdump(const void* _buf, size_t n)
{
	const unsigned char* buf = _buf;
	size_t i;

	for (i=0; i< n; i++)
	{
		if(i)
		{
			if ((i % 4) == 0)
				fprintf(stderr, " ");
			if ((i % 16) == 0)
				fprintf(stderr, "\n");
		}
		fprintf(stderr, " %02x", buf[i]);
	}
	fprintf(stderr, "\n");
}

void *xmalloc(size_t size)
{
	void *ptr;

	ptr = malloc(size);
	if (!ptr) {
		ERROR("No memory left!");
		abort();
	}

	return ptr;
}

void *xrealloc(void *ptr, size_t size)
{
	ptr = realloc(ptr, size);
	if (!ptr) {
		ERROR("No memory left!");
		abort();
	}

	return ptr;
}

char *xstrdup(const char* str)
{
	size_t len;
	char *ptr;

	if (!str)
		return NULL;

        len = strlen(str) + 1;
        ptr = xmalloc(len);

        memcpy(ptr, str, len);

        return ptr;
}

char *xstrcat(const char *str1, const char *str2)
{
	size_t len1;
	size_t len2;
	char *ptr;

	assert(str1 != NULL);
	assert(str2 != NULL);

	len1 = strlen(str1);
	len2 = strlen(str2);
	ptr = xmalloc(len1 + len2 + 1);

	memcpy(ptr, str1, len1);
	memcpy(ptr + len1, str2, len2 + 1);

	return ptr;
}

int xstrtoul(const char *str, unsigned int base, unsigned long *val)
{
	char *endptr;

	errno = 0;
	*val = strtoul(str, &endptr, base);
	if (errno == ERANGE && (*val == ULONG_MAX))
		return -ERANGE;

	if (errno != 0 && (*val == 0))
		return -errno;

	if (endptr == str)
		return -EINVAL;

	return 0;
}

int xstrtol(const char *str, unsigned int base, long *val)
{
	char *endptr;

	errno = 0;
	*val = strtol(str, &endptr, base);
	if (errno == ERANGE && ((*val == LONG_MAX) || (*val == LONG_MIN)))
		return -ERANGE;

	if (errno != 0 && (*val == 0))
		return -errno;

	if (endptr == str)
		return -EINVAL;

	return 0;
}
