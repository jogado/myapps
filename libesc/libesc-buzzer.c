#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>
#include <linux/types.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "libesc.h"

#define BEEPER_PATH	"/dev/input/beeper"

int libesc_buzzer_set(unsigned int hertz, unsigned int vol)
{
	int fd;
	struct input_event event;

	if (hertz > 20000) {
		ERROR("Invalid frequency");
		return -1;
	}

	if (vol == 0)
		hertz = 0;

	fd = open(BEEPER_PATH, O_WRONLY);
	if (fd < 0) {
		PERROR("open");
		return -1;
	}

	memset(&event, 0x00, sizeof(event));
	event.type = EV_SND;
	event.code = SND_TONE;
	event.value = hertz;

	if (write(fd, &event, sizeof(event)) != sizeof(event)) {
		PERROR("write");
		close(fd);
		return -1;
	}

	close(fd);

	return 0;
}
