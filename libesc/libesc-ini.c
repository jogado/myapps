#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libesc.h"
#include "list.h"

struct option {
	char *key;
	char *val;
	struct list_head list;
};

struct section {
	char *name;
	struct list_head options;
	struct list_head list;
};

struct ini {
	char *file;
	unsigned int modified:1;
	struct list_head sections;
};

static void str_trim_whitespaces(char *str)
{
	char *p = str;
	int l = strlen(p);

	while (l && isspace(p[l - 1]))
		p[--l] = 0;

	while (l && *p && isspace(*p)) {
		++p,
		--l;
	}

	memmove(str, p, l + 1);
}

static void delete_section(struct section *section)
{
	list_del(&section->list);
	free(section->name);
	free(section);
}

static void delete_option(struct option *option)
{
	list_del(&option->list);
	free(option->key);
	free(option->val);
	free(option);
}

static struct option *create_option(struct section *section,
				    const char *key, const char *val)
{
	struct option *option;

	assert(key != NULL);

	option = xmalloc(sizeof(*option));
	option->key = xstrdup(key);
	option->val = xstrdup(val);
	str_trim_whitespaces(option->key);
	str_trim_whitespaces(option->val);
	list_add_tail(&option->list, &section->options);

	return option;
}

static struct section *create_section(struct ini *ini,
				      const char *section_name)
{
	struct section *section;

	assert(section_name != NULL);

	section = xmalloc(sizeof(*section));
	section->name = xstrdup(section_name);
	str_trim_whitespaces(section->name);
	INIT_LIST_HEAD(&section->options);
	list_add_tail(&section->list, &ini->sections);

	return section;
}

static struct option *lookup_key(const struct section *section,
				 const char *key)
{
	struct option *option;
	struct list_head *entry;

	list_for_each(entry, &section->options) {
		option = list_entry(entry, struct option, list);
		if (!strcmp(key, option->key))
			return option;
	}

	return NULL;
}

static struct section *lookup_section(const struct ini *ini,
				      const char *section_name)
{
	struct section *section;
	struct list_head *entry;

	list_for_each(entry, &ini->sections) {
		section = list_entry(entry, struct section, list);
		if (!strcmp(section_name, section->name))
			return section;
	}

	return NULL;
}

struct ini *ini_parse(const char *file)
{
	struct ini *ini;
	FILE *fp;
	char *filepath;
	char *line = NULL;
	size_t llen = 0;
	size_t len;
	ssize_t nread;
	struct section *section = NULL;
	char *key, *val;

	fp = fopen(file, "r");
	if (!fp) {
		PERROR("fopen");
		return NULL;
	}

	filepath = realpath(file, NULL);
	if (!filepath) {
		PERROR("realpath");
		return NULL;
	}

	ini = xmalloc(sizeof(*ini));
	memset(ini, 0x00, sizeof(*ini));
	ini->file = filepath;
	INIT_LIST_HEAD(&ini->sections);

	while ((nread = getline(&line, &llen, fp)) != -1) {
		/* remove comments */
		val = strchr(line, '#');
		if (val)
			*val = 0x00;

		str_trim_whitespaces(line);

		/* check for empty line */
		len = strlen(line);
		if (!len)
			continue;

		/* new section */
		if ((line[0] == '[') && (line[len - 1] == ']')) {
			line[len - 1] = 0x00;
			if (!strlen(&line[1]))
				DEBUG("No section name");

			DEBUG("New section: %s", &line[1]);
			section = create_section(ini, &line[1]);
			continue;
		}

		/* new option */
		val = strchr(line, '=');
		if (!val)
			continue;

		if (!section) {
			WARNING("Ignoring option outside a section");
			continue;
		}

		*val = 0x00;
		key = line;

		DEBUG("New option: %s = %s", key, ++val);
		create_option(section, key, val);
	}

	free(line);
	fclose(fp);

	return ini;
}

void ini_free(struct ini *ini)
{
	struct section *section;
	struct option *option;
	struct list_head *entry, *entry2;
	struct list_head *next, *next2;

	if (!ini)
		return;

	list_for_each_safe(entry, next, &ini->sections) {
		section = list_entry(entry, struct section, list);

		list_for_each_safe(entry2, next2, &section->options) {
			option = list_entry(entry2, struct option, list);
			delete_option(option);
		}

		delete_section(section);
	}

	free(ini->file);
	free(ini);
}

const char *ini_get(const struct ini *ini, const char *section_name,
		    const char *key)
{
	struct section *section;
	struct option *option;

	assert(ini != NULL);
	assert(section_name != NULL);
	assert(key != NULL);

	section = lookup_section(ini, section_name);
	if (!section) {
		DEBUG("No section '%s' found!", section_name);
		return NULL;
	}

	option = lookup_key(section, key);
	if (!option) {
		DEBUG("No key '%s' found in section '%s'", key, section_name);
		return NULL;
	}

	return option->val;
}

int ini_set(struct ini *ini, const char *section_name,
	    const char *key, const char *val)
{
	struct section *section;
	struct option *option;

	assert(ini != NULL);

	section = lookup_section(ini, section_name);
	if (!section && section_name) {
		DEBUG("Creating new section '%s'", section_name);
		section = create_section(ini, section_name);
	} else if (section && !key && !val) {
		delete_section(section);
		ini->modified = 1;
		return 0;
	}

	option = lookup_key(section, key);
	if (!option && val) {
		DEBUG("Creating new key '%s' in section '%s'", key,
		      section->name);
		option = create_option(section, key, val);
	} else if (option && !val) {
		delete_option(option);
		ini->modified = 1;
		return 0;
	}

	if (!strcmp(val, option->val)) {
		DEBUG("Valud unchanged for key '%s'", key);
		return 1;
	}

	free(option->val);
	option->val = xstrdup(val);

	ini->modified = 1;

	return 0;
}

void ini_dump(struct ini *ini, FILE *f)
{
	struct section *section;
	struct option *option;
	struct list_head *entry, *entry2;

	list_for_each(entry, &ini->sections) {
		section = list_entry(entry, struct section, list);
		fprintf(f, "[%s]\n", section->name);

		list_for_each(entry2, &section->options) {
			option = list_entry(entry2, struct option, list);
			fprintf(f, "%s=%s\n", option->key, option->val);
		}

		if (!list_is_last(entry, &ini->sections))
			fprintf(f, "\n");
	}
}

int ini_save(struct ini *ini)
{
	FILE *f;
	char *tmpfile;

	if (!ini->modified)
		return 0;

	tmpfile = xstrcat(ini->file, ".new");
	f = fopen(tmpfile, "w");
	if (!f) {
		PERROR("fopen('%s')", tmpfile);
		free(tmpfile);
		return -1;
	}

	ini_dump(ini, f);
	fclose(f);

	rename(tmpfile, ini->file);
	free(tmpfile);

	return 0;
}
