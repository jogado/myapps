#include <limits.h>
#include <stdio.h>

#include "libesc.h"

#define PATH_LEDS "/sys/class/leds"

static FILE *libesc_led_open(const char *name, const char *entry,
			     const char *mode)
{
	FILE *fp;
	char path[PATH_MAX];

	if (snprintf(path, sizeof(path), PATH_LEDS "/%s/%s", name, entry) < 0) {
		PERROR("snprintf");
		return NULL;
	}

	fp = fopen(path, mode);
	if (!fp) {
		PERROR("fopen");
		return NULL;
	}

	return fp;
}

static int libesc_led_write_ulong(const char *name, const char *prop,
				  unsigned long value)
{
	FILE *fp;
	char buf[128];
	unsigned int n;

	fp = libesc_led_open(name, prop, "w");
	if (!fp)
		return -1;

	n = snprintf(buf, sizeof(buf), "%ld", value);

	if (fwrite(buf, 1, n, fp) < 0) {
		PERROR("fwrite");
		fclose(fp);
		return -1;
	}

	fclose(fp);

	return 0;
}

static int libesc_led_read_ulong(const char *name, const char *prop,
				 unsigned long *val)
{
	FILE *fp;
	int n;
	char buf[128];

	fp = libesc_led_open(name, prop, "r");
	if (!fp)
		return -1;

	n = fread(buf, 1, sizeof(buf), fp);
	if (n < 0) {
		PERROR("fread");
		fclose(fp);
		return -1;
	}

	fclose(fp);

	if (xstrtoul(buf, 10, val)) {
		ERROR("Invalid value");
		return -1;
	}

	return 0;
}

int libesc_led_set_brightness(const char *name, unsigned long value)
{
	unsigned long maxvalue;

	if (libesc_led_read_ulong(name, "max_brightness", &maxvalue))
		return -1;

	if (value > maxvalue) {
		ERROR("Invalid value (max = %ld)", maxvalue);
		return -1;
	}

	return libesc_led_write_ulong(name, "brightness", value);
}

int libesc_led_get_brightness(const char *name, unsigned long *value)
{
	return libesc_led_read_ulong(name, "brightness", value);
}

int libesc_led_get_max_brightness(const char *name, unsigned long *value)
{
	return libesc_led_read_ulong(name, "max_brightness", value);
}
