#ifndef __LIBESC_H__
#define __LIBESC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <syslog.h>

#ifdef NDEBUG
#define HEXDUMP_DEBUG(buf, len) do { } while (0)
#else
#define HEXDUMP_DEBUG(buf, len) hexdump(buf, len)
#endif

#define DEBUG(fmt, ...) \
	log_msg(LOG_DEBUG, "%s:%d:" fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define reached \
	DEBUG("reached!");

#define INFO(fmt, ...) \
	log_msg(LOG_INFO, "%s:%d:" fmt "\n",  __FILE__, __LINE__, ##__VA_ARGS__)

#define WARNING(fmt, ...) \
	log_msg(LOG_WARNING, "%s:%d:" fmt "\n",  __FILE__, __LINE__, ##__VA_ARGS__)

#define ERROR(fmt, ...) \
	log_msg(LOG_ERR, "%s:%d:" fmt "\n",  __FILE__, __LINE__, ##__VA_ARGS__)

#define PERROR(fmt, ...) \
	log_msg(LOG_ERR, "%s:%d:" fmt ": %s\n", __FILE__, __LINE__, \
		##__VA_ARGS__, strerror(errno))

#define ESC_SUPPLIER_LEN	4
#define ESC_MONTH_LEN		2
#define ESC_YEAR_LEN		2
#define ESC_CODE_LEN		5
#define ESC_SERIAL_LEN		5
#define ESC_REVISION_LEN	2
#define ESC_STUFFING_LEN	3

typedef unsigned char u8;
typedef unsigned short __le16;
typedef unsigned short __be16;
typedef unsigned int __le32;
typedef unsigned int __be32;

struct libesc_hw_dallas_info {
	struct {
		__be16	supplier;
		u8	month;
		u8	year;
		__be32	code;
		__be32	serial;
	} __attribute__((packed)) product;

	struct {
		__be16	supplier;
		u8	month;
		u8	year;
		__be32	code;
		__be32	serial;
		u8	rev[ESC_REVISION_LEN];
		u8	stuffing[ESC_STUFFING_LEN];
		u8	reserved;
	} __attribute__((packed)) board;

	__be16	crc;
} __attribute__((packed));

struct libesc_product_sn_str {
	char supplier[ESC_SUPPLIER_LEN];
	char month[ESC_MONTH_LEN];
	char year[ESC_YEAR_LEN];
	char code[ESC_CODE_LEN];
	char serial[ESC_SERIAL_LEN];
	char null;
} __attribute__((packed));

struct libesc_board_sn_str {
	char supplier[ESC_SUPPLIER_LEN];
	char month[ESC_MONTH_LEN];
	char year[ESC_YEAR_LEN];
	char code[ESC_CODE_LEN];
	char serial[ESC_SERIAL_LEN];
	char rev[ESC_REVISION_LEN];
	char stuffing[ESC_STUFFING_LEN];
} __attribute__((packed));

/* utils functions */

void hexdump(const void* _buf, size_t n);
void *xmalloc(size_t size);
void *xrealloc(void *ptr, size_t size);
char *xstrdup(const char* str);
char *xstrcat(const char *str1, const char *str2);
int xstrtol(const char *str, unsigned int base, long *val);
int xstrtoul(const char *str, unsigned int base, unsigned long *val);

static inline int32_t swap32(int32_t x)
{
	return ((((uint32_t)x & 0x000000ffU) << 24) |
		(((uint32_t)x & 0x0000ff00U) <<  8) |
		(((uint32_t)x & 0x00ff0000U) >>  8) |
		(((uint32_t)x & 0xff000000U) >> 24));
}

static inline int16_t swap16(int16_t x)
{
	return ((((uint16_t)x & 0x00ffU) <<  8) |
		(((uint16_t)x & 0xff00U) >>  8));
}

#if BYTE_ORDER == LITTLE_ENDIAN
static inline int32_t be32_to_cpu(int32_t x)
{
	return swap32(x);
}

static inline int16_t be16_to_cpu(int16_t x)
{
	return swap16(x);
}

static inline int32_t le32_to_cpu(int32_t x)
{
	return x;
}

static inline int16_t le16_to_cpu(int16_t x)
{
	return x;
}
#else
static inline int32_t be32_to_cpu(int32_t x)
{
	return x;
}

static inline int16_t be16_to_cpu(int16_t x)
{
	return x;
}

static inline int32_t le32_to_cpu(int32_t x)
{
	return swap32(x);
}

static inline int16_t le16_to_cpu(int16_t x)
{
	return swap16(x);
}
#endif

static inline int32_t cpu_to_be32(int32_t x)
{
	return be32_to_cpu(x);
}

static inline int32_t cpu_to_le32(int32_t x)
{
	return le32_to_cpu(x);
}

static inline int16_t cpu_to_be16(int16_t x)
{
	return be16_to_cpu(x);
}

static inline int16_t cpu_to_le16(int16_t x)
{
	return le16_to_cpu(x);
}

/* logging functions */

void log_init(const char *pname, unsigned int log_level,
	      unsigned int to_syslog, unsigned int to_stderr);
void log_close(void);
void log_msg(unsigned int level, const char *msg, ...)
	__attribute__ ((format (printf, 2, 3)));

/* ini parser */

struct ini *ini_parse(const char *file);
void ini_free(struct ini *ini);
const char *ini_get(const struct ini *ini, const char *section_name,
		    const char *key);
int ini_set(struct ini *ini, const char *section_name,
	    const char *key, const char *value);
void ini_dump(struct ini *ini, FILE *f);
int ini_save(struct ini *ini);

/* w1 functions */

struct w1_slave;

struct w1_bus *libesc_w1_bus_init(const char *path);
void libesc_w1_bus_free(struct w1_bus *bus);
int libesc_w1_bus_slave_iterate(const struct w1_bus *bus,
				 void (*cb)(const struct w1_slave*, void *),
				 void *args);
const struct w1_slave *libesc_w1_bus_slave_get(const struct w1_bus *bus,
					       unsigned int index);
int libesc_w1_slave_open_eeprom(const struct w1_slave *slave, int flags);
const char *libesc_w1_slave_name(const struct w1_slave *slave);

/* hw functions */

int libesc_hw_get_model(char *buf, unsigned int len);
const struct libesc_product_sn_str *libesc_hw_get_sn(void);
int libesc_hw_write_dallas(unsigned int busnr, unsigned int dallasnr,
			   struct libesc_hw_dallas_info *info);
int libesc_hw_read_dallas(unsigned int busnr, unsigned int dallasnr,
			  struct libesc_hw_dallas_info *info);

/* led functions */

int libesc_led_set_brightness(const char *name, unsigned long brightness);
int libesc_led_get_brightness(const char *name, unsigned long *value);
int libesc_led_get_max_brightness(const char *name, unsigned long *value);

/* buzzer functions */

int libesc_buzzer_set(unsigned int hertz, unsigned int vol);

#ifdef __cplusplus
}
#endif

#endif
