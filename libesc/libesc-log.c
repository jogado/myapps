#include <stdarg.h>
#include <stdio.h>
#include <syslog.h>

#include "libesc.h"

#ifndef NDEBUG
static unsigned int log_level = LOG_DEBUG;
#else
static unsigned int log_level = LOG_INFO;
#endif
static unsigned int log_stderr = 1;
static unsigned int log_syslog = 1;

void log_msg(unsigned int level, const char *msg, ...)
{
	va_list args;

	if (level > log_level)
		return;

	if (log_stderr) {
		va_start(args, msg);
		vprintf(msg, args);
		va_end(args);
	}

	if (log_syslog) {
		va_start(args, msg);
		vsyslog(level, msg, args);
		va_end(args);
	}
}

void log_init(const char *pname, unsigned int level,
	      unsigned int to_syslog, unsigned int to_stderr)
{
	log_level = level;

	if (to_syslog) {
		openlog(pname, 0, LOG_USER);
		log_syslog = 1;
	} else {
		log_syslog = 0;
	}

	if (to_stderr)
		log_stderr = 1;
	else
		log_stderr = 0;
}

void log_close(void)
{
	if (log_syslog)
		closelog();
}
