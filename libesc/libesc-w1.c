#include <errno.h>
#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

#include "libesc.h"

#define W1_BUS_PATH		"/sys/bus/w1/devices"
#define W1_SLAVE_NAME_MAX	256

static const char *dallas_buses[] = {
	"/sys/bus/w1/devices/w1_bus_master2",
	"/sys/bus/w1/devices/w1_bus_master1",
	NULL,
};

struct w1_slave {
	char name[W1_SLAVE_NAME_MAX];
	struct w1_bus *bus;
	struct w1_slave *next;
};

struct w1_bus {
	struct w1_slave *slaves;
	unsigned int slave_count;
	struct w1_slave *slave_ptr;
};

static int w1_bus_get_slaves(struct w1_bus *bus, const char *path)
{
	FILE *fp;
	struct w1_slave **slave;
	char filepath[PATH_MAX];
	char buf[W1_SLAVE_NAME_MAX];

	snprintf(filepath, PATH_MAX, "%s/w1_master_slaves", path);

	fp = fopen(filepath, "r");
	if (!fp) {
		PERROR("fopen(%s/%s) failed", path, "w1_master_slaves");
		return 1;
	}

	slave = &bus->slaves;
	while (*slave)
		slave = &(*slave)->next;

	while (fgets(buf, sizeof(buf), fp)) {
		if (!strncmp(buf, "not found.", sizeof("not found") - 1))
			continue;

		*slave = xmalloc(sizeof(**slave));
		memset(*slave, 0x00, sizeof(**slave));

		strncpy((*slave)->name, buf, sizeof((*slave)->name));
		(*slave)->name[strlen((*slave)->name) - 1] = '\0';
		(*slave)->bus = bus;

		slave = &(*slave)->next;
		bus->slave_count++;
	}

	fclose(fp);

	return 0;
}

struct w1_bus *libesc_w1_bus_init(const char *path)
{
	struct w1_bus *bus;
	const char **dallas_bus;

	bus = xmalloc(sizeof(*bus));
	memset(bus, 0x00, sizeof(*bus));

	if (path) {
		if (w1_bus_get_slaves(bus, path))
			goto error;
	} else {
		for (dallas_bus = &dallas_buses[0]; *dallas_bus; dallas_bus++)
			w1_bus_get_slaves(bus, *dallas_bus);
	}

	return bus;

error:
	free(bus);
	return NULL;
}

void libesc_w1_bus_free(struct w1_bus *bus)
{
	struct w1_slave *slave, *next;

	if (!bus)
		return;

	next = bus->slaves;
	while ((slave = next)) {
		next = slave->next;
		free(slave);
	}

	free(bus);
}

const struct w1_slave *libesc_w1_bus_slave_get(const struct w1_bus *bus,
					       unsigned int index)
{
	struct w1_slave *slave;
	unsigned int i = 0;

	slave = bus->slaves;
	while (slave && (i != index)) {
		slave = slave->next;
		++i;
	}

	return slave;
}

int libesc_w1_bus_slave_iterate(const struct w1_bus *bus,
				 void (*cb)(const struct w1_slave*, void *),
				 void *args)
{
	struct w1_slave *slave;
	int i = 0;

	slave = bus->slaves;
	while (slave) {
		cb(slave, args);
		slave = slave->next;
		i++;
	}

	return i;
}

int libesc_w1_slave_open_eeprom(const struct w1_slave *slave, int flags)
{
	char path[PATH_MAX];

	assert(slave);

	snprintf(path, sizeof(path), "%s/%s/eeprom", W1_BUS_PATH, slave->name);

	return open(path, flags, 0);
}

const char *libesc_w1_slave_name(const struct w1_slave *slave)
{
	return slave->name;
}
