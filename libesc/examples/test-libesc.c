#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "libesc.h"

#define TMP_TEMPLATE	"test-libesc.XXXXXX"

static FILE *create_tmp_ini_file_01(void)
{
	int fd;
	FILE *f;
	char filename[256];

	snprintf(filename, sizeof(filename), TMP_TEMPLATE);
	fd = mkstemp(filename);
	if (!fd)
		return NULL;

	f = fdopen(fd, "r+");
	fprintf(f, "[section1]\n");
	fprintf(f, "key1=val1\n");
	fprintf(f, "key2=val2 # comment\n");
	fprintf(f, "# other comment\n");
	fprintf(f, "[section2]\n");
	fprintf(f, "key1sec2=sec2val1\n");
	fprintf(f, "key2sec2=sec2val2 \n");
	fflush(f);

	return f;
}

static int test_ini_0002(void)
{
	FILE *f;
	struct ini *ini = NULL;
	char tmp[PATH_MAX];
	char path[PATH_MAX];
	int rc = -1;
	const char *val;

	f = create_tmp_ini_file_01();
	if (!f) {
		ERROR("Cannot create temporary ini file");
		goto end;
	}

	snprintf(tmp, sizeof(tmp), "/proc/self/fd/%d", fileno(f));
	memset(path, 0x00, sizeof(path));
	if (readlink(tmp, path, sizeof(path)) < 0) {
		PERROR("readlink(%s)", path);
		goto end;
	}

	ini = ini_parse(path);
	if (!ini) {
		ERROR("Parsing error");
		goto end;
	}

	ini_set(ini, "section2", "key1sec2", "newval");
	ini_save(ini);
	ini_free(ini);

	ini = ini_parse(path);
	if (!ini) {
		ERROR("Parsing error");
		goto end;
	}

	val = ini_get(ini, "section2", "key1sec2");
	if (!val) {
		ERROR("Cannot get value of 'key1sec2'");
		goto end;
	}

	if (strcmp(val, "newval")) {
		ERROR("Invalid value for 'key1sec2': %s", val);
		goto end;
	}

	rc = 0;
end:
	if (ini)
		ini_free(ini);

	unlink(path);
	fclose(f);
	return rc;
}

static int test_ini_0001(void)
{
	FILE *f;
	struct ini *ini = NULL;
	char tmp[PATH_MAX];
	char path[PATH_MAX];
	int rc = -1;
	const char *val;

	f = create_tmp_ini_file_01();
	if (!f) {
		ERROR("Cannot create temporary ini file");
		goto end;
	}

	snprintf(tmp, sizeof(tmp), "/proc/self/fd/%d", fileno(f));
	memset(path, 0x00, sizeof(path));
	if (readlink(tmp, path, sizeof(path)) < 0) {
		PERROR("readlink(%s)", path);
		goto end;
	}

	ini = ini_parse(path);
	if (!ini) {
		ERROR("Parsing error");
		goto end;
	}

	val = ini_get(ini, "section1", "key2");
	if (!val) {
		ERROR("Cannot read value of key1");
		goto end;
	}

	if (strcmp(val, "val2")) {
		ERROR("Value does not match (%s != val2)", val);
		goto end;
	}

	val = ini_get(ini, "section1", "key1error");
	if (val) {
		ERROR("Get value for key1error");
		goto end;
	}

	rc = 0;
end:
	if (ini)
		ini_free(ini);

	unlink(path);
	fclose(f);
	return rc;
}

static int (*test_functions[])(void) =
{
	test_ini_0001,
	test_ini_0002,
	NULL,
};

int main(int argc, const char *argv[])
{
	unsigned int i = 0;
	int (*test_fun)(void);

	while ((test_fun = test_functions[i++])) {
		if (test_fun())
			ERROR("Test %p failed!", test_fun);
	}

	return 0;
}
