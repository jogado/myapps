#include <stdio.h>
#include "libesc.h"

static void usage(const char *pname)
{
	fprintf(stderr, "usage: %s", pname);
}

int main(int argc, const char *argv[])
{
	const struct libesc_product_sn_str *sn;

	if (argc > 1) {
		usage(argv[0]);
		return 1;
	}

	sn = libesc_hw_get_sn();
	if (!sn)
		return 1;

	printf("Serial number: %s\n", (const char*) sn);
}
