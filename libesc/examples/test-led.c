#include <stdio.h>
#include "libesc.h"

static void usage(const char *pname)
{
	fprintf(stderr, "usage: %s ledname <value>", pname);
}

int main(int argc, const char *argv[])
{
	const char *name;
	unsigned long val;

	if ((argc < 2) || (argc > 3)) {
		usage(argv[0]);
		return 1;
	}

	name = argv[1];

	if (argc == 2) {
		if (libesc_led_get_brightness(name, &val))
			return 1;

		printf("Current brightness: %ld\n", val);
	} else if (argc == 3) {
		const char *value = argv[2];
		if (xstrtoul(value, 10, &val)) {
			ERROR("Invalid value provided");
			return 1;
		}

		if (libesc_led_set_brightness(name, val))
			return 1;
	}
}
