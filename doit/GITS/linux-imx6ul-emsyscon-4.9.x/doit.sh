#!/bin/sh



export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-
#------------------------------------------------------------
# Set destination code
#------------------------------------------------------------
ARG1=$1
if echo $1 | grep -q "imx6s"; then

	. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
	make imx_v6_v7_emsyscon_defconfig
	make -j4
	make uImage LOADADDR=0x10800000

	ls -la arch/arm/boot/uImage 
	ls -la arch/arm/boot/dts/imx6dl-esc-*.dtb  


elif  echo $1 | grep -q "imx6ul"; then

	. /opt/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi 
	make imx_v6_v7_emsyscon_defconfig
	make 
	make uImage LOADADDR=0x80800000
	ls -la arch/arm/boot/uImage 
	ls -la arch/arm/boot/dts/imx6ul-esc-*.dtb

else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi


#----------------------------------------------
# Transfer new kernel if target is defined
#----------------------------------------------
ARG2=$2
if echo $2 | grep -q "192.168"; then

	if echo $1 | grep -q "imx6s"; then
		scp arch/arm/boot/uImage  		 	root@$2:/boot/
	        scp arch/arm/boot/dts/imx6dl-esc-*.dtb   	root@$2:/boot/dtb
	elif  echo $1 | grep -q "imx6ul";then
		scp arch/arm/boot/uImage  			root@$2:/boot/
		scp arch/arm/boot/dts/imx6ul-esc-*.dtb		root@$2:/boot/dtb/
	fi
fi

