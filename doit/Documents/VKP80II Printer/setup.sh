#!/bin/sh

# sh /etc/printer/VKP80III_CUPSDrv-202.sh

cupsd
cupsctl --remote-admin
lpadmin -p VKP80III -v socket://192.168.0.1 -P /etc/printer/VKP80III.ppd
lpadmin -p VKP80III -u allow:root
lpadmin -d VKP80III
cupsenable VKP80III

lpstat -p VKP80III -l
lpstat -p -d

cupsd -t


echo -n -e "\n\rView All Available Ports for Printer Setup :\n\r"
lpinfo -v
echo " "

echo -n -e "\n\rView All jobs :\n\r"
lpstat -o
echo " "

echo -n -e "\n\rRemoving all jobs :\n\r"
lprm -
echo " "


echo -n -e "\n\rPrinter options :\n\r"
lpoptions -p VKP80III -l
echo " "

cupsaccept VKP80III
 
lp -d VKP80III -o raw ./Edit1.TXT
#lp -d VKP80III -o PageSize=X50MMY70MM -o raw ./Edit1.TXT 
#lp -d VKP80III -o EjectRetractTimeout=0 -o raw ./Edit1.TXT 
# echo "test" |lp


