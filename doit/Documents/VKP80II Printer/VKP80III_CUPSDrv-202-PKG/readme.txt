
------------------------------------------------------------------------------------

                  Custom Enginnering S.p.A VKP80III CUPS driver 64bit

------------------------------------------------------------------------------------

Package description:

This is the CUPS printer driver package, containing:

1. Compiled printer drivers for VKP80III printer:
   - rastertovkp80iii_64bit

2. Printer driver option setting help files, PPD file
   - VKP80III_64bit.ppd.gz

3. setup script dor install the driver and the PPD file

Requirements:

This software requires that the following is present on your computer
the CUPS server & architecure (see www.cups.org)

Install Instructions

To begin using this software, please do the following:

1. Open a shell (bash, etc.)

2. Type ./VKP80III_CUPSDrv-202.sh script

3. Goto http://localhost:631 or use your favorite CUPS admin tool.

4. Add a new printer queue for your model.

5. Print test page and see the result

Driver Option Settings

1. Page Sizes:
   - 50mm/60mm/70mm/80mm/82.5mm * 80mm
   - 50mm/60mm/70mm/80mm/82.5mm * 120mm
   - 50mm/60mm/70mm/80mm/82.5mm * 160mm
   - 50mm/60mm/70mm/80mm/82.5mm * 200mm
   - 50mm/60mm/70mm/80mm/82.5mm Roll
   - ZOOM 50mm/60mm/70mm/80mm/82.5mm * 80mm
   - ZOOM 50mm/60mm/70mm/80mm/82.5mm * 120mm
   - ZOOM 50mm/60mm/70mm/80mm/82.5mm * 160mm
   - ZOOM 50mm/60mm/70mm/80mm/82.5mm * 200mm
   - ZOOM 50mm/60mm/70mm/80mm/82.5mm Roll
   the default page is 80mm * 160mm

 2. Halftoning Algorithm:
  - Accurate
  - Radial
  - WTS
  - Standard

3. Printer Quality:
   - High Quality
   - Normal
   - HighSpeed
   the default is normal

4. Print Density:
   - -50%
   - -37%
   - -25%
   - -12%
   - 0%
   - +12%
   - +25%
   - +37%
   - +50%
   the default is 0%

5. Paper presentation:
   - 40 mm
   - 80 mm
   - 120 mm
   - 160 mm
   - 200 mm
   - 240 mm
   the default value is 80 mm

6. Eject-Retract Timeout 
   - None(Hold in Presentation)
   - Eject after 5 sec
   - Eject after 10 sec
   - Eject after 20 sec
   - Eject after 30 sec
   - Eject after 60 sec
   - Eject after 120 sec
   - Retract after 5 sec
   - Retract after 10 sec
   - Retract after 20 sec
   - Retract after 30 sec
   - Retract after 60 sec
   - Retract after 120 sec
   the default value is None(Hold in Presentation)

7. Paper Mouth Led 
   - Blinking
   - Off
   the default value is Blinking

8. Black Mark Alignement 
   - Disable 
   - At Begin Page
   - At End Page
   the default value is Disable



