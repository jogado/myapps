#!/bin/sh
#
# Loopback tests of different UARTS 
#

out="$1"

speed=$4
if [ -z $4 ] ; then
        speed="115200"
fi

testuart()
{
        stty -F /dev/$1 1:0:1cb2:0:0:0:0:0:0:5:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0 &> /dev/null
        stty -F /dev/$1 $2
        if [ $? -ne 0 ] ; then
                RESULT="520: stty $1 failure"
                result
        fi
        testresult=`/usr/bin/testuart -a/dev/$1 -s"2a 55 2a 55 2a 55 2a 55"`
        if [ "$testresult" != "*U*U*U*U" ] ; then
                return 1 
        fi
	return 0
}
result()
{
	echo "${RESULT}" > $out
	exit 0
}

#warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
#if [ "$warn_pcie" != "" ] ; then
#	RESULT="530 ${warn_pcie}"
#	result
#fi



case $3 in 
0)
	testuart ttyXR0 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM1 matches"
	else
		RESULT="520 Error: Loopback for COM1 failed"
	fi
	result
;;
1)
	testuart ttyXR1 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM2 matches"
	else
		RESULT="520 Error: Loopback for COM2 failed"
	fi
	result
;;
2)
	testuart ttyXR2 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM3 matches"
	else
		RESULT="520 Error: Loopback for COM3 failed"
	fi
	result
;;
3)
	testuart ttyXR3 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM4 matches"
	else
		RESULT="520 Error: Loopback for COM4 failed"
	fi
	result
;;
4)
	testuart ttyXR4 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM5 matches"
	else
		RESULT="520 Error: Loopback for COM5 failed"
	fi
	result
;;
5)
	testuart ttyXR5 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM6 matches"
	else
		RESULT="520 Error: Loopback for COM6 failed"
	fi
	result
;;
6)
	testuart ttyXR6 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM7 matches"
	else
		RESULT="520 Error: Loopback for COM7 failed"
	fi
	result
;;
7)
	testuart ttyXR7 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for COM8 matches"
	else
		RESULT="520 Error: Loopback for COM8 failed"
	fi
	result
;;
8)
	testuart usbcon0 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for USB0 matches"
	else
		RESULT="520 Error: Loopback for USB0 failed"
	fi
	result
;;
9)
	testuart usbcon1 $speed
	if [ $? -eq 0 ] ; then 
		RESULT="200 Loopback for USB1 matches"
	else
		RESULT="520 Error: Loopback for USB1 failed"
	fi
	result
;;
10)	#bc800/bc810
	ports="ttyXR2 ttyXR3 ttyXR4 ttyXR5 ttyXR6 ttyXR7 usbcon0 usbcon1"
	for port in $ports
	do
		testuart $port $speed
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports $port"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 Loopback for matches for all COMs"
	else
		RESULT="520 Error: Loopback for $failed_ports failed"
	fi
	result
;;
11)	#dc800
	case $2 in 
	32)
		ports="ttyXR1 ttyXR2 ttyXR3"
	;;
	*)	#IBIS
		ports="ttyXR1"
		speed=1200
	;;
	esac
	for port in $ports
	do
		testuart $port $speed
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports $port"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 Loopback for matches for all COMs"
	else
		RESULT="520 Error: Loopback for $failed_ports failed"
	fi
	result
;;
12)     #V800
	case $2 in 
	128)
		ports="ttyXR4 ttyXR2 ttyXR3"
	;;
	*)	
		ports="ttyXR3 ttyXR4"
	;;
	esac
	#enable RS232 on com2 by setting MPIO4=P5
	echo 1 > /sys/class/gpio/P5/value
	for port in $ports
	do
		testuart $port $speed
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports $port"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 Loopback for matches for all COMs"
	else
		RESULT="520 Error: Loopback for $failed_ports failed"
	fi
	result
;;
13)	#dc802
	case $2 in 
	32)
		ports="ttyXR1 ttyXR2 ttyXR3"
	;;
	*)	#IBIS + COM1
		ports="ttyXR1 ttyXR2"
		speed=1200
	;;
	esac
	for port in $ports
	do
		testuart $port $speed
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports $port"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 Loopback for matches for all COMs"
	else
		RESULT="520 Error: Loopback for $failed_ports failed"
	fi
	result
;;
15)	#485 COM1-COM2
	# One port to echo back what the other one sends
	/usr/bin/testuart -a/dev/ttyXR0 -t2 -Q & 
	testresult=`/usr/bin/testuart -a/dev/ttyXR1 -S"2a 55 2a 55 2a 55 2a 55"`
        if [ "$testresult" == "*U*U*U*U" ] ; then
		RESULT="200 COM1-2 echo matches"
	else
		RESULT="520 Error: Echo COM1-2 (RS485) failed"
	fi
	result
;;
16)	#485 COM1-IPS485
        echo 00000011 > /sys/devices/virtual/exar/xrgpio/rs485
	# One port to reply on IPS test command
	/usr/bin/testuart -a/dev/ttyXR0 -t2 -I & 
	/usr/bin/test-ips
        if [ "$?" -eq 0 ] ; then
		RESULT="200 COM1-IPS485 echo matches"
	else
		RESULT="520 Error: Echo COM1-IPS485 failed"
	fi
	result
;;
17)	#485 COM2-IPS485
        echo 00000011 > /sys/devices/virtual/exar/xrgpio/rs485
	# One port to reply on IPS test command
	/usr/bin/testuart -a/dev/ttyXR1 -t2 -I & 
	/usr/bin/test-ips
        if [ "$?" -eq 0 ] ; then
		RESULT="200 COM2-IPS485 echo matches"
	else
		RESULT="520 Error: Echo COM2-IPS485 failed"
	fi
	result
;;
20)	#All BC800 485 COM1-COM2-IPS485
        # Need to preinitialise both ports somehow...
        echo 00000011 > /sys/devices/virtual/exar/xrgpio/rs485
	/usr/bin/testuart -a/dev/ttyXR0 -t1 -Q & 
	sleep 1 
	/usr/bin/testuart -a/dev/ttyXR1 -t1 -Q & 
	sleep 1 
	killall -9 testuart
	# One port to reply on IPS test command
	/usr/bin/testuart -a/dev/ttyXR0 -t2 -I & 
	/usr/bin/test-ips
        if [ "$?" -eq 0 ] ; then
		/usr/bin/testuart -a/dev/ttyXR1 -t2 -I & 
		/usr/bin/test-ips
        	if [ "$?" -eq 0 ] ; then
			RESULT="200 COM1-COM2-IPS485 test messages ok"
		else
			RESULT="520 Error: Echo COM2-IPS485 failed"
		fi
	else
		RESULT="520 Error: Echo COM1-IPS485 failed"
	fi
	result
;;
21)	#DC800: 485 COM4/BSC-COM3/IPS485
	#enable the RS485 porti
	echo 1 > /sys/class/gpio/P5/value
	# One port to reply on IPS test command
	/usr/bin/testuart -a/dev/ttyXR3 -t2 -I & 
	/usr/bin/test-ips
        if [ "$?" -eq 0 ] ; then
		RESULT="200 COM3-4 echo matches"
	else
		RESULT="520 Error: Echo COM3-4 failed"
	fi
	result
;;
30)     #V790 rs232 loopback test :                                                
        /usr/bin/testuart_new -t 1 -p /dev/rs232                         
        if [ $? -eq 0 ] ; then                        
                RESULT="200 Loopback for RS232 matches"
        else                                           
                RESULT="520 Error: Loopback for RS232 failed"            
        fi                                                   
        result                                                    
;;       
31)     #V790 rs232 <> rs485 loopback test.                                                 
        /usr/bin/testuart_new -t 0 -p /dev/rs232 -P /dev/rs485 -r 2                    
        if [ $? -eq 0 ] ; then                        
                RESULT="200 Loopback for RS232 <> RS485 matches"
        else                                           
                RESULT="520 Error: Loopback for RS232 <> RS485 failed"            
        fi                                                   
        result                                                    
;;  
32)     #ov760 rs232 <> rs232 loopback test.   
        /bin/sed -i s/mxc1/#mxc1/ /etc/inittab
        /sbin/init q                                               
        /usr/bin/testuart_new -t 0 -p /dev/ttymxc1 -P /dev/ttymxc0 -r 0                    
        if [ $? -eq 0 ] ; then                        
                RESULT="200 Loopback for RS232 <> RS232 matches"
        else                                           
                RESULT="520 Error: Loopback for RS232 <> RS232 failed"            
        fi           
        /bin/sed -i s/#mxc1/mxc1/ /etc/inittab
        /sbin/init q                                          
        result                                                    
;;  
 

esac
RESULT="500 Wrong Test ID: $3"
result


