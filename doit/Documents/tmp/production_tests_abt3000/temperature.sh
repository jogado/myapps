#!/bin/sh
#
# Test Temperature sensor 
#
DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`



if [ $DEVICE_NAME = "ov760"  ] ;then
	I2C_ADDR="0-0027"
	I2Cbus=0
elif [ $DEVICE_NAME = "abt3000"  ] ; then
	I2C_ADDR="1-0048"
	I2Cbus=1
elif  [ $DEVICE_NAME = "ov760"  ] ; then
        I2C_ADDR="0-0048"
	I2Cbus=0
fi



echo "I2C_ADDR  : $I2C_ADDR"
echo "I2Cbus    : $I2Cbus"

EXE_stm="/usr/bin/temperature-read |  cut -d' ' -f 3"
EXE_i2c="cat /sys/class/i2c-dev/i2c-${I2Cbus}/device/${I2C_ADDR}/hwmon/hwmon0/temp1_input"

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi
if [ "$#" -lt "2" ]
then
   mode="1"
else
   mode="$2"
fi
if [ "$3" = "i2c" ]
then
	EXE=$EXE_i2c
else
	EXE=$EXE_stm
fi

if [ `uname -r` > "3.10" ] ; then 
# In 3.10.17 kernel temperature values are multiplied with 1000
	DIV="1000"
fi

result()
{
	echo "${RESULT}" > $out
	exit 0
}

case $mode in 
*) 
	res=`$EXE`
	if [ $? -ne 0 ] ; then
		RESULT=`echo 530 Temperature Error $res`
	else
		if [ -n $DIV ] ; then
			let res=res/DIV
		fi
		RESULT=`echo "200 Internal Temp" $res "C"`
		CPU=`cat /sys/devices/virtual/thermal/thermal_zone0/temp`
		if [ -n $DIV ] ; then
			let CPU=CPU/DIV
		fi
		echo 110 > /sys/devices/virtual/thermal/thermal_zone0/trip_point_0_temp
		if [ $CPU -gt 108 ] ; then 
			RESULT=`echo 530 $res, CPU Temp is High: $CPU C`
		else
			RESULT=`echo $RESULT, CPU Temp = $CPU C`
		fi
	fi			
;;
esac

result


