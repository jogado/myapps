#!/bin/sh

TMP_FOLDER="/var/run/production-test"


#--------------------------------------
# Perform program exit housekeeping
#--------------------------------------
clean_up()
{
	killall aplay
	echo "Cleaning_done"
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 

mkdir -p $TMP_FOLDER

echo $$ > $TMP_FOLDER/sound_task.pid
echo $$ > /tmp/sound_task.pid


beep -f 2400 -l 50 
beep -f 2400 -l 50 
amixer sset 'PCM' 100%

if [ -f "/usr/share/audio/22294.wav" ]
then
	aplay /usr/share/audio/22294.wav
else
	aplay /usr/share/audio/sine440_stereo.wav
fi

sleep 1
exit

for i in `seq 0 1`
do 
	aplay /usr/share/audio/sine880_stereo.wav
	sleep 1
	play /usr/share/audio/sine440_stereo.wav
	sleep 1
	aplay /usr/share/audio/square80_stereo.wav
	sleep 1
done


