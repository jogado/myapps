#!/bin/sh
#
# Loopback tests of different GPIOs 
#

source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks

out="$1"
CONFIRM=/usr/bin/production-test/testscripts/confirm.sh

result()
{
	echo "${RESULT}" > $out
	exit 0
}
startX()
{
        export DISPLAY=:0.0
        /usr/bin/production-test/testscripts/start_xserver.sh 0
        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
        fi
        echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pidof" != "" ]; then
                kill $pid
        fi
}
testgpio()
{
    if [ -z $1 -o -z $2 ] ; then
		RESULT="500 Missing Parameters"
		result
	fi
	outp=$1
	in1=$2
	echo 1 > /sys/class/gpio/P$outp/value
	status1=`cat /sys/class/gpio/P$in1/value`
	echo 0 > /sys/class/gpio/P$outp/value
	status2=`cat /sys/class/gpio/P$in1/value`
	if [ "$status1" -eq "$status2" ] ; then
		return 1
	fi
	return 0 
}

#warn_pcie=`/usr/bin/production-test/testscripts/tst_pcie.sh`
#if [ "$warn_pcie" != "" ] ; then
#	RESULT="530 ${warn_pcie}"
#	result
#fi

case $3 in 
0)
	testgpio 1 9
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output1 toggle, toggles input 1"
	else
		RESULT="520 Error: GPIO output1 toggle does not toggles input 1"
	fi
	result
;;
1)
	testgpio 1 10
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output1 toggle, toggles input 2"
	else
		RESULT="520 Error: GPIO output1 toggle does not toggles input 2"
	fi
	result
;;
2)
	testgpio 2 11
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output2 toggle, toggles input 3"
	else
		RESULT="520 Error: GPIO output2 toggle does not toggles input 3"
	fi
	result
;;
3)
	testgpio 2 12
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output2 toggle, toggles input 4"
	else
		RESULT="520 Error: GPIO output2 toggle does not toggles input 4"
	fi
	result
;;
5)
	testgpio 3 14
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output3 toggle, toggles input 6"
	else
		RESULT="520 Error: GPIO output3 toggle does not toggles input 6"
	fi
	result
;;
6)
	testgpio 4 15
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output4 toggle, toggles input 7"
	else
		RESULT="520 Error: GPIO output4 toggle does not toggles input 7"
	fi
	result
;;
7)
	testgpio 5 16
	if [ $? -eq 0 ] ; then 
		RESULT="200 GPIO output5 toggle, toggles input 8"
	else
		RESULT="520 Error: GPIO output5 toggle does not toggles input 16"
	fi
	result
;;
10)	
	case $2 in 
	2)
		#bc800/bc810 integration test
		pairs="1,9 1,10 2,11 2,12 3,14 4,15 5,16"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
11)	
	case $2 in 
	2)
		#dc800 integration test
		pairs="2,9 2,10 3,11 3,12 4,13"
	;;
	32)
		#dlp131 test
		pairs="1,9 2,10 3,11 4,12 4,13"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
12)	
	case $2 in 
	2 | 128)
		#v800 integration/dlp150 test
		pairs="1,9 2,10 3,9 4,10"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
13)	
	case $2 in 
	2)
		#dc802 integration test (todo FW/TACHO via STM)
		pairs="2,9 2,10 3,11 3,12"
	;;
	32)
		#dlp131 test
		pairs="1,9 2,10 3,11 4,12 4,13"
	;;
	*)
		RESULT="501 Wrong mode"
		result
	;;
	esac
	
	for pair in $pairs
	do
		testgpio `echo $pair |tr , \ `
		if [ $? -ne 0 ] ; then 
			 failed_ports=`echo "$failed_ports ($pair)"`
		fi
	done
	if [ -z "$failed_ports" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test for output/input pairs $failed_ports failed"
	fi
	result
;;
14) #v790
	#Loopback Digital In/Out
	INPUT="130"
	I_PATH="/sys/class/gpio/gpio${INPUT}/value"
	O_PATH="/sys/class/leds/output0/brightness"

	if [ ! -e ${I_PATH} ] ; then
	        echo ${INPUT} > /sys/class/gpio/export
		sleep 1
	fi

        echo 1 > ${O_PATH}
        status1=`cat ${I_PATH}`

        echo 0 > ${O_PATH}
        status2=`cat ${I_PATH}`

        if [ "$status1" -eq "$status2" ] ; then
		RESULT="520 Error: DIGITAL IN/OUT test failed $status1 $status2"
	else
		RESULT="200 DIGITAL IN/OUT test success $status1 $status2"
	fi
	result
;;
15) #v790
	# RS485 Address selection bits test

	IOS="16 167 36 20"
	LINE=1
	for IO in ${IOS} ; do
		I_PATH="/sys/class/gpio/gpio${IO}/value"
		if [ ! -e ${I_PATH} ] ; then
		        echo ${IO} > /sys/class/gpio/export
		fi
		if [ `cat ${I_PATH}` -eq "1" ] ; then
			RESULT="520 Error: Addres Line $LINE already HIGH"
			result
		fi
		let LINE=LINE+1
	done
        /usr/bin/production-test/testscripts/start_xserver.sh 0
        let RESX=$?
        echo 0 >/sys/class/graphics/fb0/blank
        if [ $RESX -gt 1 ] ; then
                RESULT="530 ${test} test failed, X-server start problem"
        else
                export DISPLAY=:0.0
				/usr/bin/screen-test -b 404040 -t 10 -m "Press Button within 10 seconds!" &
	fi


	TIMEOUT=100
	COUNT=0
	BAD="1234"
	OK=0

        beep -f 2400 -l 20 

	while [ ${TIMEOUT} -gt 0 ] ; do
		LINE=1
		OK=0
		for IO in ${IOS} ; do
			I_PATH="/sys/class/gpio/gpio${IO}/value"
			if [ `cat ${I_PATH}` -eq "1" ] ; then
				BAD=`echo $BAD | sed "s/${LINE}//"`	
				let OK=OK+1
			fi
			let LINE=LINE+1
		done
		let TIMEOUT=TIMEOUT-1

		if [ $OK -eq 4 ]
		then
			echo 1 >/sys/class/graphics/fb0/blank
			RESULT="200 Poll Address Input test success"
			result
		fi		

		usleep 100000

		let COUNT=COUNT+1
	done 

	if [ -n "${BAD}" ] ; then
		RESULT="520 Error: Poll Address Input test failed: ${BAD}"
	else
		RESULT="200 Poll Address Input test success"
	fi
	echo 1 >/sys/class/graphics/fb0/blank
	result
;;
16) #ov760
        startX
        failed=""
        echo 240 > /sys/class/gpio/export
        echo 241 > /sys/class/gpio/export
        echo 242 > /sys/class/gpio/export
        echo 243 > /sys/class/gpio/export
        echo 250 > /sys/class/gpio/export
        echo 251 > /sys/class/gpio/export
        echo out > /sys/class/gpio/gpio250/direction
        echo out > /sys/class/gpio/gpio251/direction
        #fan / in1
        echo 20000 > /sys/class/thermal/thermal_zone0/trip_point_0_temp
        sleep 3
        status1=`cat /sys/class/gpio/gpio240/value`
        echo 100000 > /sys/class/thermal/thermal_zone0/trip_point_0_temp
        sleep 3
        status2=`cat /sys/class/gpio/gpio240/value`
        if [ "$status1" -eq "$status2" ] ; then
            failed="$failed fan/in1"
        fi
        if [ $2 -eq 1 ]; then
            #interactive application diagnastics mode test with loopback cable on GPIOs :
            #heater / in2
            echo 1 > /sys/class/hwmon/hwmon1/device/heater_enable
            status1=`cat /sys/class/gpio/gpio241/value`
            echo 0 > /sys/class/hwmon/hwmon1/device/heater_enable
            status2=`cat /sys/class/gpio/gpio241/value`
            if [ "$status1" -eq "$status2" ] ; then
                failed="$failed heater/in2"
            fi
            #out3 / in3
            echo 1 > /sys/class/gpio/gpio250/value
            status1=`cat /sys/class/gpio/gpio242/value`
            echo 0 > /sys/class/gpio/gpio250/value
            status2=`cat /sys/class/gpio/gpio242/value`
            if [ "$status1" -eq "$status2" ] ; then
                failed="$failed out3/in3"
            fi
            #out4 / in4
            echo 1 > /sys/class/gpio/gpio251/value
            status1=`cat /sys/class/gpio/gpio243/value`
            echo 0 > /sys/class/gpio/gpio251/value
            status2=`cat /sys/class/gpio/gpio243/value`
            if [ "$status1" -eq "$status2" ] ; then
                failed="$failed out4/in4"
            fi
        else
            #heater interactive :
            echo 1 > /sys/class/hwmon/hwmon1/device/heater_enable
            sleep 5
            echo 0 > /sys/class/hwmon/hwmon1/device/heater_enable
            CNF=`${CONFIRM} "Heater" $2 1 5`                                                         
            if echo $CNF | grep 530 ; then                                      
                failed="$failed heater"       
            fi    
        fi
        echo 240 > /sys/class/gpio/unexport
        echo 241 > /sys/class/gpio/unexport
        echo 242 > /sys/class/gpio/unexport
        echo 243 > /sys/class/gpio/unexport
        echo 250 > /sys/class/gpio/unexport
        echo 251 > /sys/class/gpio/unexport
	if [ -z "$failed" ] ;  then
		RESULT="200 GPIO test success"
	else
		RESULT="520 Error: GPIO test(s) for $failed failed"
	fi
        stopX
	result
;;
100) #ABT3000
	#Loopback Digital In/Out
	INPUT="27"
	I_PATH="/sys/class/gpio/gpio${INPUT}/value"
	O_PATH="/sys/class/leds/output1/brightness"

	if [ ! -e ${I_PATH} ] ; then
	        echo ${INPUT} > /sys/class/gpio/export
		sleep 1
	fi

        echo 1 > ${O_PATH}
        status1=`cat ${I_PATH}`

        echo 0 > ${O_PATH}
        status2=`cat ${I_PATH}`

        if [ "$status1" -eq "$status2" ] ; then
		RESULT="520 ABT3000 : Error !! DIGITAL IN/OUT test failed $status1 $status2"
	else
		RESULT="200 ABT3000 : DIGITAL IN/OUT test success $status1 $status2"
	fi
	result
;;
esac
RESULT="500 Wrong Test ID: $3"
result
