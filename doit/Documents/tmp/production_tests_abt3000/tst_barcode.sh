#!/bin/sh
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks


DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`


# Production test script
# return barcode serial number
#


# $1 contains the output file
if [ "$#" -lt "1" ]
then
    out="/dev/stdout"
else
    out="$1"
fi

RESULT="501 NOT IMPLEMENTED"

result()
{
    echo "${RESULT}" > $out
    exit 0
}

startX()
{
    export DISPLAY=:0.0
    /usr/bin/production-test/testscripts/start_xserver.sh 0
    if [ $? -eq 1 ] ; then
        pid=`pidof Xorg`
    fi
    echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
    echo 1 >/sys/class/graphics/fb0/blank
    if [ "$pidof" != "" ]; then
        kill $pid
    fi
}


startX

BOARD_INFO_FILE=/tmp/barcode_sn.txt

echo "" > $BOARD_INFO_FILE



if [ $DEVICE_NAME = "abt3000" ] ; then
	stty -F /dev/ttymxc4 115200 -echo -raw
	cat /dev/ttymxc4 > $BOARD_INFO_FILE &
else
	cat /dev/barcode > $BOARD_INFO_FILE &
fi




myloop=1
timeout=0

beep
DISPLAY=:0 /usr/bin/screen-test -m "Scan barcode" -b 0000ff -f 300 &

while [ $myloop -eq 1 ]
do

    BOARD_INFO=$(grep -m 1 "Serial Number:" ${BOARD_INFO_FILE}  2>/dev/null )
    if [ -n "${BOARD_INFO}" ]
    then
        SERNR=$(echo ${BOARD_INFO} | cut -d':' -f2 ) 
        RESULT="200 S/N=${SERNR}"
        let myloop=0
        echo "" > $BOARD_INFO_FILE
        beep
    fi

    actualsize=$(wc -c <"$BOARD_INFO_FILE")
    if [ $actualsize -ge 10 ]
    then
        RESULT="200 S/N= `tail ${BOARD_INFO_FILE}`"
        let myloop=0
	    beep
    fi

    usleep 100000
    let timeout=timeout+1
    if [ $timeout -gt 100 ]; then
   	    RESULT="500 TIMEOUT" 
        let myloop=0
    fi   

done

stopX
killall cat
killall screen-test
killall Xorg
result

