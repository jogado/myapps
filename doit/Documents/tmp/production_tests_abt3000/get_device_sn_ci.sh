#!/bin/sh
#
# return board serial number
#

out="$1"

RESULT="501 NOT IMPLEMENTED"


result()
{
	echo "${RESULT}" > $out
	exit 0
}


# for hotroom dim the backlight a bit and start / restart the hotroom test monitoring function.
echo 1 >/sys/class/graphics/fb0/blank
if [ $2 -eq 4 ] ; then
	BACKLIGHT_DEVICE="/sys/class/backlight/$(ls /sys/class/backlight)"
	echo 48 > $BACKLIGHT_DEVICE/brightness
	PID=`cat /tmp/monitor.pid` 
        kill -9 $PID                                                  
        PID=`ps | grep "sleep 600" | grep -v grep | awk '{print $1}'`
        kill -9 $PID              
        monitor & 
        echo $! > /tmp/monitor.pid
fi

BOARD_INFO_FILE=/tmp/serialnumber.txt
/usr/bin/dallas-esc > $BOARD_INFO_FILE


#=====================fake emulation to be removed !!!!! ======================
echo "" >>$BOARD_INFO_FILE
echo "Serialnumber: 1234567890123" >>$BOARD_INFO_FILE
echo "Product: abcde"              >>$BOARD_INFO_FILE
#===============================================================================


if [ -e "${BOARD_INFO_FILE}" ]
then
   BOARD_INFO=$(grep "Serialnumber:" ${BOARD_INFO_FILE} 2>/dev/null )
   BOARD_INFO=$(grep "Product:" ${BOARD_INFO_FILE} 2>/dev/null )
   if [ -n "${BOARD_INFO}" ]
   then
         SERNR=$(echo ${BOARD_INFO} | cut -d':' -f2 ) 
         RESULT="200 S/N=${SERNR}"
   else
      RESULT="500 NO VALID S/N FOUND" 
   fi
else
      RESULT="500 serialnumber.txt NOT FOUND" 
fi

result
