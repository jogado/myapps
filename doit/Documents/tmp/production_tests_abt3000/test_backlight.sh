#!/bin/sh
#
# Test the backlight
#
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks


CONFIRM=/usr/bin/production-test/testscripts/confirm.sh

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then                                      
   out="/dev/stdout"                                            
else                                                            
   out="$1"                                                     
fi 


TIMEOUT=10

result()
{
        echo "${RESULT}" > $out
        exit 0
}

/usr/bin/production-test/testscripts/test_backlight_task.sh $TIMEOUT &
RESULT=`${CONFIRM} "Backlight test" $2 1 $TIMEOUT 99`
result
