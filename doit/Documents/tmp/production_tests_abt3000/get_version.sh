#!/bin/sh

# Production test script
# Getversion

result()
{
        echo $RESULT > $out
	exit 0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
out=$1
if [ -z $1 ] ; then
	out="/dev/stdout"
fi


# $3: executed test
case $3 in
1)
	# uname
	OUTPUT=`uname -a`
	if [ $? -eq 0 ] ; then 
		RESULT="200 $OUTPUT"
	else
                RESULT="501 uname failed"
	fi
        ;;
2)
	# production-test package
	OUTPUT=`opkg list-installed | grep production-test | tr -d ' '`
	if [ $? -eq 0 ] ; then 
		RESULT="200 $OUTPUT"
	else
                RESULT="501  production-test version not found"
	fi
	if [ $2 -eq 4 ] ; then
		#in hotroom, verify if we need to update the SW
		check_upgrade
	fi
        ;;
*)
	# package
	OUTPUT=`opkg list-installed | grep $4 | tr -d ' '`
	if [ $? -eq 0 ] ; then 
		RESULT="200 $OUTPUT"
	else
                RESULT="501 package $3 not found"
	fi
        ;;
esac                                                                          
result
