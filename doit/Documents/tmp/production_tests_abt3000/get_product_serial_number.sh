#!/bin/sh
#
# return PRODUCT serial number
#

out="$1"

result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"


/usr/bin/dallas-esc > /tmp/dallas.txt
BOARD_INFO_FILE=/tmp/dallas.txt

#=====================fake emulation to be removed !!!!! ======================
echo "" >>$BOARD_INFO_FILE
echo "Serialnumber: 1234567890123" >>$BOARD_INFO_FILE
echo "Product: abcde"              >>$BOARD_INFO_FILE
#===============================================================================


if [ -e "${BOARD_INFO_FILE}" ]
then
   BOARD_INFO=$(grep "Product:" ${BOARD_INFO_FILE} 2>/dev/null )
   if [ -n "${BOARD_INFO}" ]
   then
         SERNR=$(echo ${BOARD_INFO} | cut -d':' -f2 ) 
         RESULT="200 S/N=${SERNR}"
   else
      RESULT="500 NO PRODUCT S/N FOUND" 
   fi
else
      RESULT="500 NO FILE FOUND" 
fi

result
