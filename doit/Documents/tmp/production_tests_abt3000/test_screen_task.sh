#!/bin/sh


TMP_FOLDER="/var/run/production-test"

TIMEOUT=$1


#--------------------------------------
# Perform program exit housekeeping
#--------------------------------------
clean_up()
{
	killall beep
	echo 0 >/sys/class/graphics/fb0/blank
	pid=`pidof Xorg`
	if [ "$pid" != "" ]; then
		kill $pid
	fi
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 


mkdir -p $TMP_FOLDER

echo $$ > $TMP_FOLDER/test_screen_task.pid


/usr/bin/screen-test -t $TIMEOUT -p2 -m "Color test" &

beep -f 2400 -l 20 

sleep 7
clean_up


for i in `seq 0 $TIMEOUT`
do 
 	if [ `expr $i % 3` -eq 0 ]
	then
	        usleep 20
	fi
	sleep 1

done



