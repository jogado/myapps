#!/bin/sh


FILENAME=$0
TIMEOUT=$1
TMP_FOLDER="/var/run/production-test"
BACKLIGHT_DEVICE="/sys/class/backlight/$(ls /sys/class/backlight)"
MAX_BRIGHTNESS=255
mkdir -p  $TMP_FOLDER
echo $$ > $TMP_FOLDER/test_backlight.pid


#------------------------------------------------------------------------------
# Perform program exit housekeeping
#------------------------------------------------------------------------------
clean_up()
{
	echo $MAX_BRIGHTNESS > $BACKLIGHT_DEVICE/brightness
	stopX
	exit 0
}
trap clean_up SIGTERM SIGINT EXIT 
#------------------------------------------------------------------------------



detect_backlight()
{
	if [ ! -d /sys/class/backlight ]; then
		return 1
	fi

	BACKLIGHT_DEVICE="/sys/class/backlight/$(ls /sys/class/backlight)"
	echo "$BACKLIGHT_DEVICE"

	return 0
}

result()
{
	echo "${RESULT}" > $out
	exit 0
}

startX()
{
	export DISPLAY=:0.0
	/usr/bin/production-test/testscripts/start_xserver.sh 0
	if [ $? -eq 1 ] ; then
		pid=`pidof Xorg`
	fi
	echo 0 >/sys/class/graphics/fb0/blank

	if [ "$pid" != "" ]; then
		echo $pid > $TMP_FOLDER/Xorg.pid
	fi


}

stopX()
{
	echo 1 >/sys/class/graphics/fb0/blank
	if [ "$pid" != "" ]; then
		kill $pid
	fi
}



BACKLIGHT_DEVICE=$(detect_backlight)
if [ "$BACKLIGHT_DEVICE" == "" ]; then
	RESULT="500 No backlight interface found"
	result
fi


MAX_BRIGHTNESS=$(cat "$BACKLIGHT_DEVICE/max_brightness")


if [ $MAX_BRIGHTNESS -lt 10 ]; then 
	STEP=1
	SLEEP=100000
	LOOPS=5
else
	STEP=16
	SLEEP=100000
	LOOPS=3
fi

echo "MAX_BRIGHTNESS  $MAX_BRIGHTNESS"
echo "STEP            $STEP"
echo "LOOPS           $LOOPS"


startX


/usr/bin/screen-test -m "Backlight test" -b 0000ff -t $TIMEOUT &

pid2=`pidof /usr/bin/screen-test`
if [ "$pid2" != "" ]; then
	echo $pid2 > $TMP_FOLDER/screen-test.pid
fi

beep

for j in `seq 0 $LOOPS`
do 


	for i in `seq $MAX_BRIGHTNESS -$STEP 1`; do
		echo $i >$BACKLIGHT_DEVICE/brightness
		usleep $SLEEP
	done


	for i in `seq 1 $STEP $MAX_BRIGHTNESS`; do
		echo $i >$BACKLIGHT_DEVICE/brightness
		usleep $SLEEP
	done

done


clean_up



