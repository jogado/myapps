#!/bin/sh
#
# Test the touchscreeen
#
source /usr/bin/production-test/testscripts/clear_pids.inc
clear_production_test_tasks
TIMEOUT=15

out="$1"

DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`

if [ $DEVICE_NAME = "abt3000" ] ; then

	LS_PATH="/sys/class/i2c-dev/i2c-1/device/1-0044"
	CHIP="isl29023"
else

	LS_PATH="/sys/class/i2c-dev/i2c-0/device/0-0044"
	CHIP="isl29003"
fi



result()
{
	echo "${RESULT}" > $out
	exit 0
}

RESULT="501 NOT IMPLEMENTED"

startX()
{
        export DISPLAY=:0.0
        /usr/bin/production-test/testscripts/start_xserver.sh 0
        if [ $? -eq 1 ] ; then
                pid=`pidof Xorg`
        fi
        echo 0 >/sys/class/graphics/fb0/blank
}
stopX()
{
        echo 1 >/sys/class/graphics/fb0/blank
        if [ "$pid" != "" ]; then
                kill $pid
        fi
}

run_test()
{
	beep
	echo 0 >/sys/class/graphics/fb0/blank
	echo 0 > /sys/class/graphics/fbcon/cursor_blink
    
	let RES=1
	if [ ! -d /sys/bus/i2c/drivers/$CHIP ]; then
		RESULT="500 No $CHIP found"
		return
	fi

	echo 1 > ${LS_PATH}/power_state
	sleep 1
	lux=$(cat ${LS_PATH}/lux)
    
	DISPLAY=:0 /usr/bin/screen-test \
         -m "Cover light sensor" -b 404040 &
    
    let a=0
    while [ $a -lt $TIMEOUT ] ; do
                pid=`pidof screen-test`
                if [ "$pid" != "" ] ; then
                        break
                fi
                sleep 1
                let a=a+1
    done

   let b=0
   let RES=1
   while [ $b -lt $TIMEOUT ] ; do
		lux2=$(cat ${LS_PATH}/lux)
		if [ $lux -gt 30 -a $lux2 -lt 30 -o $1 -eq 4 ] ; then
			RESULT="200 Lightsensor test succeeded! Lux = $lux"
			break
		else
			RESULT="520 Lightsensor lux value too low or did not covered ($lux)"
		fi
        sleep 1
        let b=b+1
    done
	echo 0 >${LS_PATH}/power_state
}

run_test2()
{
	beep
	echo 0 > /sys/class/graphics/fb0/blank
	echo 0 > /sys/class/graphics/fbcon/cursor_blink
    
	let RES=1
	if [ ! -d /sys/bus/i2c/drivers/$CHIP ]; then
		RESULT="500 No $CHIP found"
		return
	fi

	echo 0 > ${LS_PATH}/power_state
	echo 1 > ${LS_PATH}/power_state
	sleep 1
	lux=$(cat ${LS_PATH}/lux)

    
	DISPLAY=:0 /usr/bin/screen-test -m "Cover light sensor" -b 0000ff -f 30 &
    
	let a=0
	while [ $a -lt $TIMEOUT ] ; do
                pid=`pidof screen-test`
                if [ "$pid" != "" ] ; then
                        break
                fi
                sleep 1
                let a=a+1
    done

   let b=0
   let RES=1
   while [ $b -lt $TIMEOUT ] ; do

  	echo 0 >${LS_PATH}/power_state
  	echo 1 >${LS_PATH}/power_state
		lux2=$(cat ${LS_PATH}/lux)

		let delta=$lux-$lux2 

		if [ $delta -gt 10 ] ; then
			RESULT="200 Lightsensor test succeeded! Lux ($lux,$lux2)"
			break
		else
			RESULT="520 Lightsensor lux value too low or did not covered ($lux,$lux2)"
		fi
        sleep 1
        let b=b+1
    done
    echo 0 >${LS_PATH}/power_state
}



startX


if [ $DEVICE_NAME = "abt3000" ] ; then
	run_test2 $2
else
	run_test $2
fi

stopX

result
