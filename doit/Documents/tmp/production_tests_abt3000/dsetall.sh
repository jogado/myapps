#!/bin/sh

ip=$1
file=$2

if [ -z "$ip" ]; then
    echo "Missing device ip address"
    exit
fi


#---------------------------------------------------------------------------------------------
scp hwinfo 				root@$1:/usr/bin/hwinfo
scp xorg.conf				root@$1:/etc/X11/xorg.conf
scp xorg.conf				root@$1:/etc/X11/xorg-abt3000_1_0.conf
scp snmpd 				root@$1:/etc/init.d/snmpd 
scp production_tests.conf		root@$1:/etc/snmp/conf.d/production_tests.conf

# scp test_lightsensor.sh  		root@$1:/usr/bin/production-test/testscripts
# scp tst_barcode.sh	 		root@$1:/usr/bin/production-test/testscripts
# scp tst_i2c.sh	  			root@$1:/usr/bin/production-test/testscripts
# scp sound_task.sh	  		root@$1:/usr/bin/production-test/testscripts
# scp test_backlight_task.sh  		root@$1:/usr/bin/production-test/testscripts
# scp temperature.sh  			root@$1:/usr/bin/production-test/testscripts
# scp get_version.sh  			root@$1:/usr/bin/production-test/testscripts
# scp get_q7_ci.sh  			root@$1:/usr/bin/production-test/testscripts
# scp get_device_sn_ci.sh  		root@$1:/usr/bin/production-test/testscripts

# scp get_board_serial_number.sh	root@$1:/usr/bin/production-test/testscripts   
# scp get_product_serial_number.sh 	root@$1:/usr/bin/production-test/testscripts
# scp gpio.sh 				root@$1:/usr/bin/production-test/testscripts

#---------------------------------------------------------------------------------------------
scp snmptrapd.conf 		root@$1:/etc/snmp/snmptrapd.conf
scp snmpd.conf			root@$1:/etc/snmp/snmpd.conf
scp K60snmpd                    root@$1:/etc/rc6.d/K60snmpd
scp K60snmpd                    root@$1:/etc/rc1.d/K60snmpd
scp K60snmpd             	root@$1:/etc/rc0.d/K60snmpd
scp S90snmpd                    root@$1:/etc/rc2.d/S90snmpd
scp S90snmpd                    root@$1:/etc/rc5.d/S90snmpd
scp S90snmpd                    root@$1:/etc/rc3.d/S90snmpd
scp S90snmpd                    root@$1:/etc/rc4.d/S90snmpd
#---------------------------------------------------------------------------------------------
scp 50-serial.rules		root@$1:/etc/udev/rules.d/50-serial.rules
#---------------------------------------------------------------------------------------------
scp 22294.wav 			root@$1:/usr/share/audio
#---------------------------------------------------------------------------------------------

