#!/bin/sh

ip=$1
file=$2

if [ -z "$ip" ]; then
    echo "Missing device ip address"
    exit
fi

if [ -z "$file" ]; then
    echo "missing filename"
    exit
fi



scp $2 root@$1:/usr/bin/production-test/testscripts

