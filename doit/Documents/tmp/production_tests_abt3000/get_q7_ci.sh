#!/bin/sh
#
# return Q7 serial number,type, BIOS version and physical RAM
#

DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`

if [ "$#" -lt "1" ]
then
   out="/dev/stdout"
else
   out="$1"
fi


result()
{
	echo "${RESULT}" > $out
	exit 0
}


if [ $DEVICE_NAME = "abt3000" ] ; then
   MODELINFO=$(cat /proc/cpuinfo | grep "Hardware" | cut -d':' -f2 )
   CPUINFO=$(cat /proc/cpuinfo | grep "Revision" | cut -d':' -f2 | tr '\t' ' ' | tr -s ' ' | tr -d ' ' )
   SERIAL=$(cat /proc/cpuinfo | grep "Serial" | cut -d':' -f2 | tr '\t' ' ' | tr -s ' ' | tr -d ' ' )
   LOW_preUDI=`cat /sys/fsl_otp/HW_OCOTP_CFG0 |cut -c 3-`
   HIGH_preUDI=`cat /sys/fsl_otp/HW_OCOTP_CFG1 |cut -c 3-`

   RESULT="200 HARDWARE=${MODELINFO},${CPUINFO}:S/N=${HIGH_preUDI}${LOW_preUDI}"
   
  result
fi




ARCH=$(uname -m)

if [ "${ARCH:0:3}" == "arm" ] ; then

   MODELINFO=$(cat /proc/cpuinfo | grep "Hardware" | cut -d':' -f2 )
   CPUINFO=$(cat /proc/cpuinfo | grep "Revision" | cut -d':' -f2 | tr '\t' ' ' | tr -s ' ' | tr -d ' ' )
   SERIAL=$(cat /proc/cpuinfo | grep "Serial" | cut -d':' -f2 | tr '\t' ' ' | tr -s ' ' | tr -d ' ' )

   RESULT="200 HARDWARE=${MODELINFO},${CPUINFO}:S/N=${SERIAL}"

else
   MODELINFO=$(cat /proc/cpuinfo | grep model | sort | uniq | tr '\t' ' ' | tr -s ' ' | tr ' ' '_')
   CPUINFO=$(cat /proc/cpuinfo | grep cpu | sort | uniq | tr '\t' ' ' | tr -s ' ' | tr ' ' '_')
   MAC=$(ifconfig eth0 | grep HWaddr | tr '\t' ' ' | tr -s ' ' | cut -d' ' -f5 | tr -d ':')

   for MODEL in ${MODELINFO}
   do
      if [ ${MODEL:0:7} == "model_:" ]
      then
         MODEL_TYPE=$(echo $MODEL | cut -d':' -f2 | tr -d '_')
      fi
      if [ ${MODEL:0:12} == "model_name_:" ]
      then
         MODEL_NAME=$(echo $MODEL | cut -d':' -f2 | tr -d '_')
      fi
   done

   for CPU in ${CPUINFO}
   do
      if [ ${CPU:0:12} == "cpu_family_:" ]
      then
         CPU_FAMILY=$(echo $CPU | cut -d':' -f2 | tr -d '_')
      fi
   done
   RESULT="200 CPU TYPE=${MODEL_NAME} S/N=1${CPU_FAMILY}${MODEL_TYPE}${MAC}"
fi


result







