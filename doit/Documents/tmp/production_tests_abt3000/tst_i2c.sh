#!/bin/sh

DEVICE_NAME=`hwinfo -m | cut -f 2 -d ' ' | cut -f 1 -d '_'`


# Production test script
# i2c: detect devices on i2cbus by means of i2cdetect utility

# The list of available i2c client addresses in hex
i2csb_atom="38 4c 6e 76"
# Added for imx platforms
i2csb_imx="38"
i2csd_imx="20 21 22 23 24 25 27 3a 41 44"
i2csd2_imx="20 21 24 25 3a 3b 41 44" # DC800/2
i2csv_imx="20 21 24 25 38 3a 44"
i2csv760_imx="20 24 44 48 50 68 69 6a"
i2csv790_imx="20 24 44 48 50 68 69 6a"
i2csov760_imx="20 21 24 25 50 69 6a"
i2csabt3000="0a 20 24 38 42 44 48 50 68 69 6a"

if [ $DEVICE_NAME = "abt3000" ] ; then
	i2cbus=1
else
	i2cbus=0
fi

# Added i2c smbus for imx - 6e=pci clock buffer, 76=pci switch
i2cs_sm="50 6e 76"
i2cbus_sm=1
NAME="Test i2c devices"

testi2c()
{
	let x=0x$1
	dev=`echo $x`
	res=`i2cdetect -y $2 $dev $dev | grep '[0-7]0:' | sed 's/[0-7]0://g' | sed 's/ //g'`
	echo $res
}

result()
{
        echo $RESULT > $out
	exit 0
}

initscreen()
{
	printf "\033[2J\033[9;0]\033[0;0H" >/dev/tty0
}
info()
{
	temp=`echo -n $@ | sed 's/%/%%/g'`
	printf "\r\n$temp" >/dev/tty0
}

# $1 contains the output file (/dev/stdio, /home/root/test, ...)
if [ "$#" -lt "1" ] ; then
   out="/dev/stdout"
else
   out="$1"
fi

# $2: mode bits
if [ "$#" -lt "2" ] ; then
   mode="1"
else
   mode="$2"
fi

# $3: executed test

case $3 in
	bc810) i2cs=$i2csb_atom;;
	bc800) i2cs=$i2csb_imx;;
	dc800) i2cs=$i2csd_imx;;
	dc802) i2cs=$i2csd2_imx;;
	v800) i2cs=$i2csv_imx;;
        v760) i2cs=$i2csv760_imx
              i2cs_sm="";;
        v790) i2cs=$i2csv790_imx
              i2cs_sm="";;
        ov760) i2cs=$i2csov760_imx
              i2cs_sm="";;

	*) i2cs=$i2csv_imx;;
esac


if [ $DEVICE_NAME = "abt3000" ] ; then
	i2cs=$i2csabt3000
fi


#Run the i2cdetect test
if [ $mode -eq 2 -o $mode -eq 4 ] ; then
initscreen
info "$NAME: $i2cs"
fi
let c=1
if [ $# -gt 3 ] ; then
	let c=$4
fi
RETRIES=3
while [ $RETRIES -gt 0 ] ; do
let n=0
let tot=0
let fault=0
let ok=0
baddvcs=""
while [ $n -lt $c ] ; do
	for i in $i2cs ; do
		res=`testi2c $i $i2cbus`
		if [ "$res" == "$i" ] || [ "$res" == "UU" ] ; then
			let ok=ok+1
		else
			let fault=fault+1
			temp=`echo $baddvcs | grep $i`
			if [ "$temp" == "" ] ; then
				baddvcs="$baddvcs $i"
			fi
		fi
		let tot=tot+1
	done
	if [ $3 == "dc800" ] ; then
	# DC800 has either device on 0x22 or 0x3A
		let tot=tot-1
	fi	
	for i in $i2cs_sm ; do
		res=`testi2c $i $i2cbus_sm`
		if [ "$res" == "$i" ] || [ "$res" == "UU" ] ; then
			let ok=ok+1
		else
			let fault=fault+1
			temp=`echo $baddvcs | grep $i`
			if [ "$temp" == "" ] ; then
				baddvcs="$baddvcs $i"
			fi
		fi
		let tot=tot+1
	done
	let n=n+1
done
if [ $3 != "v790" ] ; then
	warn_pci=`/usr/bin/production-test/testscripts/tst_pcie.sh`
fi
let perc=100*ok/tot
if [ $perc -eq 100 -a "$warn_pci" == "" ] ; then
	RESULT=`echo "200 ok=$perc%, $ok/$tot $machine"`
	break;
else
	let dec=1000*ok/tot-10*$perc
	RESULT=`echo "530 ok=$perc.$dec%, $ok/$tot $machine $fault failed:$baddvcs $warn_pci"`
fi
RETRIES=$((RETRIES-1))
done
if [ $mode -eq 2 -o $mode -eq 4 ] ; then
info "$NAME: Finished"
info "$RESULT $RETRIES"
fi
result 
