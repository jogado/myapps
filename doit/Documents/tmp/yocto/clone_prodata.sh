#!/bin/sh



cd ~/yocto

mkdir -p kapsch
cd kapsch

export GIT_SSL_NO_VERIFY=1

repo init -u https://jgd@opensource.kapschcarrier.com/os/prodata-manifests.git

#==============================================================================
# jgd
# jogado01
#==============================================================================
# Change in .repo/manifest.xml:
#==============================================================================
#  <remote fetch="http://opensource.kapschcarrier.com/os" name="prodata"/>
#  <remote fetch="http://jgd:jogado01@opensource.kapschcarrier.com/os" name="prodata"/>
#==============================================================================
repo sync

source ./setup-env build


ls ../sources/meta-prodata/recipes-core/images/ -la
	# base-v760-400-image.bb
	# base-v760-400-x-image.bb
	# bc800-base-image.bb
	# bc800-base-image-directdisk.bb
	# bc800-java-image.bb
	# bc800-qt-demo-image.bb
	# bc800-smarttram-image.bb
	# c7602-base-image.bb
	# dc800-base-image.bb
	# dc800-base-image-directdisk.bb
	# dc800-java-image.bb
	# dc800-qt-demo-image.bb
	# dc800-video-kcc-ittrans2016-image.bb
	# dc800-video-kcc-vi-ci-en-image.bb
	# dc802-base-image.bb
	# dc802-base-image-directdisk.bb
	# dmt2000-arriva-image.bb
	# dmt2000-base-image.bb
	# imx-base-image.bb
	# imx-base-image-directdisk.bb
	# imx-fwoti-image.bb
	# imx-qt-sdk-image.bb
	# imx-ramdisk.bb
	# imxsolo-base-image.bb
	# imxsolo-extended-image.bb
	# imxsolo-fwoti-image.bb
	# imxsolo-java-image.bb
	# imxsolo-openjfx.inc
	# imxsolo-qt-sdk-image.bb
	# imxsolo-ramdisk.bb
	# mg7604-base-image.bb
	# mg7605-base-image.bb
	# mg7605-marta-image.bb
	# mg760-base-image-fcc-test.bb
	# ov760-base-image.bb
	# package-exclude-qt.inc
	# v76010-base-image.bb
	# v7604-base-image.bb
	# v7604-marta-image.bb
	# v7605-base-image.bb
	# v7606-base-image.bb
	# v7606-qt-sdk-image.bb
	# v7607-base-image.bb
	# v7608-base-image.bb
	# v7608-qt-demo-image.bb
	# v7609-base-image.bb
	# v760-base-image.bb
	# v760-base-image-fcc-test.bb
	# v790-base-image.bb
	# v790-qt-demo-image.bb
	# v800-base-image.bb
	# v800-base-image-directdisk.bb
	# v800-java-image.bb
	# v800-qt-demo-image.bb
	# v800-qt-sdk-image.bb



bitbake dmt2000-arriva-image

















