==============================================================================================================================================
# iWave G18D
==============================================================================================================================================
mkdir yocto
cd yocto
repo init -u git://git.freescale.com/imx/fsl-arm-yocto-bsp.git -b imx-4.1-krogoth -m imx-4.1.15-2.0.3.xml 
repo sync
	# Fetching project meta-fsl-demos
	# Fetching project meta-openembedded
	# Fetching project meta-fsl-arm
	# Fetching project meta-fsl-arm-extra
	# Fetching projects:  33% (3/9)  Fetching project fsl-community-bsp-base
	# Fetching projects:  44% (4/9)  Fetching project meta-browser
	# Fetching projects:  55% (5/9)  Fetching project poky
	# Fetching projects:  66% (6/9)  Fetching project meta-qt5
	# Fetching projects:  77% (7/9)  Fetching project meta-fsl-bsp-release

patch -Np1 < ~/PATCH/iw/REL2.0/PATCH000-iW-PREVZ-SC-01-R2.0-REL1.0-YoctoKrogoth_basic_customization.patch
	# patching file sources/meta-fsl-arm-extra/conf/machine/imx6-iwg18.conf
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest/GPL-2
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest/slide1
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest/slide2
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest/slide3
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest/tamper
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/iWavebin/iwtest_1.0.bb
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/u-boot/u-boot-iwg18/PATCH001-iW-PREVZ-SC-01-R2.0-REL1.0-Linux4.1.15_UBoot_basic_customization.patch
	# patching file sources/meta-fsl-arm-extra/recipes-bsp/u-boot/u-boot-iwg18_2016.03.bb
	# patching file sources/meta-fsl-arm-extra/recipes-kernel/linux/linux-iwg18/0001-ARM-imx-imx6ul-add-PHY-KSZ8081-new-silicon-revision-.patch
	# patching file sources/meta-fsl-arm-extra/recipes-kernel/linux/linux-iwg18/0001-MLK-13418-ASoC-wm8960-workaround-no-sound-issue-in-m.patch
	# patching file sources/meta-fsl-arm-extra/recipes-kernel/linux/linux-iwg18/0002-MLK-13422-ASoC-wm8960-fix-the-pitch-shift-issue-afte.patch
	# patching file sources/meta-fsl-arm-extra/recipes-kernel/linux/linux-iwg18/PATCH002-iW-PREVZ-SC-01-R2.0-REL1.0-Linux4.1.15_Kernel_basic_customization.patch
	# patching file sources/meta-fsl-arm-extra/recipes-kernel/linux/linux-iwg18_4.1.15.bb
	# patching file sources/meta-fsl-bsp-release/imx/meta-bsp/recipes-bsp/imx-test/imx-test/PATCH000-iW-PREVZ-SC-01-R2.0-REL1.0-Linux4.1.15_imxtest.patch
	# patching file sources/meta-fsl-bsp-release/imx/meta-bsp/recipes-bsp/imx-test/imx-test_5.7.bb
	# patching file sources/meta-fsl-bsp-release/imx/meta-sdk/conf/distro/include/fsl-imx-preferred-env.inc
	# patching file sources/poky/meta/recipes-core/base-files/base-files_3.0.14.bb
	# patching file sources/poky/meta/recipes-core/init-ifupdown/init-ifupdown-1.0/interfaces
	# patching file sources/poky/meta/recipes-extended/logrotate/logrotate_3.9.1.bb

=================================================cd yo	======================================================================
# Build 
=======================================================================================================================
cd ~/yocto
DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source fsl-setup-release.sh -b build
DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source setup-environment build

DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source setup-environment build_x11  (iwEmsyscon)

bitbake meta-toolchain
ls -la tmp/deploy/sdk/*.sh


bitbake fsl-image-qt5
bitbake fsl-image-qt5 -c populate_sdk
=======================================================================================================================
































#=======================================================================================================================
# EMSYSCON FILESYSTEM
#=======================================================================================================================
mkdir yocto_emsyscon
cd yocto_emsyscon

repo init -u git@git.emsyscon.com:os/yocto-manifests.git
repo sync
	# Fetching project meta-qt4
	# Fetching project meta-emsyscon
	# Fetching project meta-openembedded
	# Fetching project documentation
	# Fetching projects:  12% (1/8)  Fetching project meta-qt5
	# Fetching projects:  25% (2/8)  Fetching project poky
	# Fetching projects:  37% (3/8)  Fetching project yocto-scripts
	# Fetching projects:  50% (4/8)  Fetching project meta-java
#=======================================================================================================================
ls -la ~/yocto/emsyscon/sources/meta-emsyscon/recipes-kernel/linux/
		linux-at91-esc_3.11.bb
		linux-imx-esc_4.9.bb
		linux-imxsolo_3.10.17.bb
#=======================================================================================================================
gedit ~/yocto/emsyscon/sources/meta-emsyscon/conf/machine/esc-imx6s-sodimm.conf
	check: PREFERRED_PROVIDER_virtual/kernel = "linux-imx-esc"
#=======================================================================================================================

#=======================================================================================================================
# Build 
#=======================================================================================================================

source ./setup-env build

#=======================================================================================================================
ls -la ~/yocto/sources/meta-emsyscon/recipes-kernel/linux/
	bitbake emsyscon-base-image
	bitbake emsyscon-full-image
	bitbake emsyscon-qt5-sdk
#=======================================================================================================================


remark:
Copy meta-emsyscon to "yocto iWave G18D" source folder to generate imx6UL 
compatible image with emsyscon tools.























#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
#                                        iWave + Emsyscon meta layer
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
#=======================================================================================================================
cd ~/yocto/iwEmsyscon

cd sources
	mv meta-emsyscon meta-emsyscon-000
	git clone git@git.emsyscon.com:os/meta-emsyscon.git -b rocko-18.0.1-x

cd meta-emsyscon
	git branch -a
		#	* rocko-18.0.1-x
		#	  remotes/origin/HEAD -> origin/master
		#	  remotes/origin/master
		#	  remotes/origin/morty-16.0.0-x
		#	  remotes/origin/rocko-18.0.1-x
	git fetch --all
	git fetch --tags

	mv recipes-core/images/emsyscon-ramdisk-image.bb  recipes-core/images/emsyscon-ramdisk-image.bbb
	mv recipes-bsp/u-boot/u-boot-fw-utils_2016.11.bb recipes-bsp/u-boot/u-boot-fw-utils_2016.11.bbb

cd ~/yocto/iwEmsyscon

rm -rf build*

DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source setup-environment build
rm -rf config
cp -r ../../abt3000_imported/build/conf/ conf



bitbake emsyscon-qt5-sdk
-------------------------------------------------------------------------------------------------



DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source setup-environment build
bitbake emsyscon-qt5-sdk

		ERROR: No IMAGE_CMD defined for IMAGE_FSTYPES entry 'cpio.gz.u-boot' 
		ERROR: Failed to parse recipe: .../meta-emsyscon/recipes-core/images/emsyscon-ramdisk-image.bb

mv  /home/ubuntu/yocto/abt3000_imported/sources/meta-emsyscon/recipes-core/images/emsyscon-ramdisk-image.bb  
    /home/ubuntu/yocto/abt3000_imported/sources/meta-emsyscon/recipes-core/images/emsyscon-ramdisk-image.bbb



	ERROR: u-boot-fw-utils-2016.11-r0 do_configure: QA Issue: u-boot-fw-utils: LIC_FILES_CHKSUM points 
	to an invalid file: /home/ubuntu/yocto/abt3000_imported/build/tmp/work/imx6ulevk-poky-linux-gnueabi/u-boot-fw-utils/2016.11-r0/git/Licenses/README [license-checksum]
	ERROR: u-boot-fw-utils-2016.11-r0 do_configure: Fatal QA errors found, failing task.
	ERROR: u-boot-fw-utils-2016.11-r0 do_configure: Function failed: do_qa_configure
	ERROR: Logfile of failure stored in: /home/ubuntu/yocto/abt3000_imported/build/tmp/work/imx6ulevk-poky-linux-gnueabi/u-boot-fw-utils/2016.11-r0/temp/log.do_configure.103586
	ERROR: Task 3315 (/home/ubuntu/yocto/abt3000_imported/sources/meta-emsyscon/recipes-bsp/u-boot/u-boot-fw-utils_2016.11.bb, do_configure) failed with exit code '1'


mv  /home/ubuntu/yocto/abt3000_imported/sources/meta-emsyscon/recipes-bsp/u-boot/u-boot-fw-utils_2016.11.bb /home/ubuntu/yocto/abt3000_imported/sources/meta-emsyscon/recipes-bsp/u-boot/u-boot-fw-utils_2016.11.bbb






