#!/bin/sh

mkdir -p ~/GITS;

#----------------------------------------------------------------------------------------------------------
# U-boot sources from iWave deliverables 
#----------------------------------------------------------------------------------------------------------
cd ~/GITS
tar -xvzf ~/PATCH/iw/REL3.0/u-boot/u-boot.tar.gz

mv u-boot uboot-iw-tar
# cp  ~/Downloads/iW-EMEQC-DF-01-R1.0_REL1.0_Deliverables/Source_Code/U-boot/000PATCH001-iW-EMEQC-SC-01-R1.0-REL1.0_UBoot_Intermediate_Release_1.patch  ~/GITS
# cp  ~/Downloads/Ethernet/Patches/U-boot/001PATCH003-iW-EMEQC-SC-01-R1.0-REL1.0_UBoot_Intermediate_Release_2_Ethernet.patch ~/GITS
#--------------------------------------------------



#==========================================================================================================
# U-boot sources from freescale
#==========================================================================================================
cd ~/GITS
git clone git://git.freescale.com/imx/uboot-imx.git -b imx_v2016.03_4.1.15_2.0.0_ga uboot-iw-git

#----------------------------------------------------------------------------------------------------------
# U-boot Patches
#----------------------------------------------------------------------------------------------------------
cd ~/GITS/uboot-iw-git
patch -Np1 < ~/PATCH/iw/REL3.0/u-boot/Patches/000PATCH001-iW-EMEQC-SC-01-R1.0-REL1.0_UBoot_Intermediate_Release_1.patch 
	# patching file arch/arm/cpu/armv7/mx6/Kconfig
	# patching file board/freescale/mx6ul_dilupe/imximage.cfg
	# patching file board/freescale/mx6ul_dilupe/imximage_lpddr2.cfg
	# patching file board/freescale/mx6ul_dilupe/Kconfig
	# patching file board/freescale/mx6ul_dilupe/MAINTAINERS
	# patching file board/freescale/mx6ul_dilupe/Makefile
	# patching file board/freescale/mx6ul_dilupe/mx6ul_dilupe.c
	# patching file board/freescale/mx6ul_dilupe/plugin.S
	# patching file board/freescale/mx6ul_dilupe/README
	# patching file configs/mx6ul_dilupe_emmc_defconfig
	# patching file include/configs/mx6ul_dilupe.h

patch -Np1 < ~/PATCH/iw/REL3.0/u-boot/Patches/001PATCH002-iW-EMEQC-SC-01-R1.0-REL2.0_UBoot_Intermediate_Release_2_ETHERNET.patch 
	# patching file board/freescale/mx6ul_dilupe/mx6ul_dilupe.c

patch -Np1 < ~/PATCH/iw/REL3.0/u-boot/Patches/002PATCH002-iW-EMEQC-SC-01-R1.0-REL3.0_UBoot_Intermediate_Release_3_AMP_EN.patch
	# patching file board/freescale/mx6ul_dilupe/mx6ul_dilupe.c






#----------------------------------------------------------------------------------------------------------
# U-boot Build
#----------------------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------
# Export the Cross Compiler and tool chain path
#----------------------------------------------------------------------------------------------------------
export ARCH=arm
export PATH=$PATH:/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-

make mx6ul_dilupe_emmc_defconfig

make CC="arm-poky-linux-gnueabi-gcc --sysroot=/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/cortexa7hf-neon-poky-linux-gnueabi/"
ls -la u-boot.imx

#----------------------------------------------------------------------------------------------------------
. /opt/iw/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi
make mx6ul_dilupe_emmc_defconfig
make
ls -la u-boot.imx
#----------------------------------------------------------------------------------------------------------


