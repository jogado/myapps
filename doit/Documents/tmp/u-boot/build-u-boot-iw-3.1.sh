#!/bin/sh
# Export the Cross Compiler and tool chain path
export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-
export PATH=$PATH:/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi


#Configure for abt3000 platform
make mx6ul_dilupe_emmc_defconfig

#Compile the u-boot source code
make CC="arm-poky-linux-gnueabi-gcc --sysroot=/opt/fsl-imx-x11/4.1.15-2.0.3/sysroots/cortexa7hf-neon-poky-linux-gnueabi/"


DD=$(date +'%Y%m%d%H%M%S')

rm u-boot-*
cp u-boot.imx u-boot-$DD.imx
ls -la u-boot*


