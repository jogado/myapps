#!/bin/sh

mkdir -p ~/GITS;
git clone git@git.emsyscon.com:os/u-boot.git -b imx-esc u-boot_esc-imx6ul

cd u-boot_esc-imx6ul


# Export the Cross Compiler and tool chain path
#----------------------------------------------
export ARCH=arm
export PATH=$PATH:/opt/poky-emsyscon/2.4.1/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-


make emsyscon_imx6ul_512M_defconfig

make
#make CC="arm-poky-linux-gnueabi-gcc --sysroot=/opt/fsl-imx-x11/4.1.15-2.0.3/sysroots/cortexa7hf-neon-poky-linux-gnueabi/"

ls -la u-boot.imx


# echo 0 > /sys/block/mmcblk1boot0/force_ro
# mount /dev/mmcblk1boot0 /mnt/boot0/
# dd if=/boot/u-boot.imx of=/dev/mmcblk1boot0 bs=512 seek=2
# echo 1 > /sys/block/mmcblk1boot0/force_ro
#
# mmc bootpart enable 1 1 /dev/mmcblk1

