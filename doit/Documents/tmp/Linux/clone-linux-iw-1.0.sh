#!/bin/sh

mkdir -p ~/GITS

#=======================================================================================================================================
# Linux sources from iWave deliverables
#=======================================================================================================================================
cd ~/GITS
# tar -xvsf ~/Downloads/iW-EMEQC-DF-01-R1.0_REL1.0_Deliverables/Source_Code/Linux/linux.tar.gz
git clone git://git.freescale.com/imx/linux-imx.git -b imx_4.1.15_2.0.0_ga


#=======================================================================================================================================
# iwave Linux Kernel Patches R01
#=======================================================================================================================================
cd ~/GITS/linux
patch -Np1 --binary < ~/PATCH/000PATCH002-iW-EMEQC-SC-01-R1.0-REL1.0_Linux_Intermediate_Release_1.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/boot/dts/Makefile
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file arch/arm/mach-imx/mach-dilupe.c
	# patching file arch/arm/mach-imx/Makefile
	# patching file drivers/input/touchscreen/ft5x06_ts.c
	# patching file drivers/input/touchscreen/ft5x06_ts.h
	# patching file drivers/input/touchscreen/Kconfig
	# patching file drivers/input/touchscreen/Makefile
patch -Np1 < ~/PATCH/001PATCH004-iW-EMEQC-SC-01-R1.0-REL1.0_Linux_Intermediate_Release_2_Ethernet.patch
	# (Stripping trailing CRs from patch; use --binary to disable.)
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patch -p1 -R < ../PATCH/001PATCH004-iW-EMEQC-SC-01-R1.0-REL1.0_Linux_Intermediate_Release_2_Ethernet.patch
patch -Np1 < ~/PATCH/002PATCH005-iW-EMEQC-SC-01-R1.0-REL1.0_Linux_Intermediate_Release_2_Ethernet_2.patch 
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/mach-imx/mach-dilupe.c



