#!/bin/sh

mkdir -p ~/GITS

#=======================================================================================================================================
# Linux sources NXP
#=======================================================================================================================================
cd ~/GITS
git clone git://git.freescale.com/imx/linux-imx.git -b imx_4.1.15_2.0.0_ga linux-iw-3.1

#=======================================================================================================================================
# iwave patches revision 3.1
#=======================================================================================================================================
cd ~/GITS/linux-iw-3.1

patch -Np1 -- binary < ~/PATCH/iw/3.1/linux/000PATCH002-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_1.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/boot/dts/Makefile
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file arch/arm/mach-imx/mach-dilupe.c
	# patching file arch/arm/mach-imx/Makefile
	# patching file drivers/input/touchscreen/ft5x06_ts.c
	# patching file drivers/input/touchscreen/ft5x06_ts.h
	# patching file drivers/input/touchscreen/Kconfig
	# patching file drivers/input/touchscreen/Makefile
patch -Np1 < ~/PATCH/iw/3.1/linux/001PATCH003-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_MICRO_SD.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 < ~/PATCH/iw/3.1/linux/002PATCH004-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_TEMP_SENSOR.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/iw/3.1/linux/003PATCH005-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_I2C_EEPROM.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/iw/3.1/linux/004PATCH006-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BACKLIGHT.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig

patch -Np1 < ~/PATCH/iw/3.1/linux/005PATCH007-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_EEPROM.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file drivers/rtc/rtc-ds1307.c
patch -Np1 < ~/PATCH/iw/3.1/linux/006PATCH008-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_TRICKLE.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file drivers/rtc/rtc-ds1307.c
patch -Np1 < ~/PATCH/iw/3.1/linux/007PATCH009-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BARCODE_NODE.patch
	# patching file drivers/leds/led-class.c
patch -Np1 < ~/PATCH/iw/3.1/linux/008PATCH010-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_ETHERNET_WITHOUT_1_WIRE.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 < ~/PATCH/iw/3.1/linux/009PATCH011-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_CDC_ACM.patch
	# patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/iw/3.1/linux/011PATCH013-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SC_REV_update.patch
	# patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 < ~/PATCH/iw/3.1/linux/012PATCH014-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Buzzer.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file drivers/pwm/pwm-imx.c
patch -Np1 < ~/PATCH/iw/3.1/linux/013PATCH015-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SPI_FLASH.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 < ~/PATCH/iw/3.1/linux/014PATCH016-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Light_Sensor.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file drivers/input/misc/isl29023.c
patch -Np1 < ~/PATCH/iw/3.1/linux/015PATCH017-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_LCD_dot_issue_fix.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 < ~/PATCH/iw/3.1/linux/016PATCH018-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Dig_out_node.patch
	# patching file drivers/leds/led-class.c
patch -Np1 < ~/PATCH/iw/3.1/linux/017PATCH019-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_LM75_Interrupt_Fix.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file drivers/hwmon/lm75.c
	# patching file drivers/hwmon/lm75.h
patch -Np1 < ~/PATCH/iw/3.1/linux/018PATCH020-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Brightness_value_fix.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 < ~/PATCH/iw/3.1/linux/019PATCH021-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Audio.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file sound/soc/codecs/sgtl5000.c
	# patching file sound/soc/fsl/imx-sgtl5000.c
	# patching file sound/soc/Makefile
patch -Np1 < ~/PATCH/iw/3.1/linux/020PATCH022-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_GPIO_LED.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file drivers/leds/led-class.c
patch -Np1 < ~/PATCH/iw/3.1/linux/021PATCH023-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SAM.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# patching file arch/arm/configs/imx_dilupe_defconfig
	# patching file drivers/mxc/Kconfig
	# patching file drivers/mxc/Makefile
	# patching file drivers/mxc/sam/Kconfig
	# patching file drivers/mxc/sam/Makefile
	# patching file drivers/mxc/sam/nsam.c
	# patching file drivers/mxc/sam/nsam.h
	# patching file drivers/mxc/sam/nsam_sm.c
	# patching file drivers/mxc/sam/sam.h

patch -Np1 < ~/PATCH/iw/3.1/linux/010PATCH012-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_1_WIRE_eeprom.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# 	Hunk #1 succeeded at 421 with fuzz 2 (offset 325 lines).
	# 	Hunk #2 FAILED at 188.
	# 	Hunk #3 FAILED at 367.
	# 	2 out of 3 hunks FAILED -- saving rejects to file arch/arm/boot/dts/imx6ul-dilupe.dts.rej
	# patching file arch/arm/configs/imx_dilupe_defconfig

patch -Np1 < ~/PATCH/iw/3.1/linux/022PATCH024-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_1_WIRE_EEPROM_EXTERNAL.patch
	# patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	# 	Hunk #1 succeeded at 429 with fuzz 1 (offset 325 lines).
	# 	Hunk #2 FAILED at 190.
	# 	Hunk #3 FAILED at 381.
	# 	2 out of 3 hunks FAILED -- saving rejects to file arch/arm/boot/dts/imx6ul-dilupe.dts.rej



#=======================================================================================================================================
# Export the Cross Compiler and tool chain path
#=======================================================================================================================================
export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-
export PATH=$PATH:/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi


#----------------------------------------------------------------------------------------------------------
#Configure for iWave-G18M-SM platform
#----------------------------------------------
make imx_dilupe_defconfig

#----------------------------------------------------------------------------------------------------------
#Compile 
#----------------------------------------------
make 



#----------------------------------------------
# Show result
#----------------------------------------------
DD=$(date +'%Y%m%d%H%M%S')

rm arch/arm/boot/zImage-*
cp arch/arm/boot/zImage  arch/arm/boot/zImage-$DD
ls -la arch/arm/boot/zImage*


rm arch/arm/boot/dts/imx6ul-dilupe-*
cp arch/arm/boot/dts/imx6ul-dilupe.dtb arch/arm/boot/dts/imx6ul-dilupe-$DD.dtb
ls -la arch/arm/boot/dts/imx6ul-dilupe*


