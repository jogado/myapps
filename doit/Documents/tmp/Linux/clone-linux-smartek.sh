#!/bin/sh

mkdir -p ~/GITS

#=======================================================================================================================================
# Linux sources from smartek
#=======================================================================================================================================
cd ~/GITS
git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x linux_smartek
cd linux_smartek
git fetch --all
git fetch --tags
git branch -a










#=======================================================================================================================================
# Linux kernel Build
#=======================================================================================================================================
#cd ~/GITS/linux_smartek
. /opt/poky-emsyscon/2.2/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
cp arch/arm/configs/imx_v6_v7_emsyscon_defconfig .config 
make -j4
make 

uImage LOADADDR=0x10800000

ls -rtl arch/arm/boot/uImage 
ls -rtl arch/arm/boot/dts/imx6dl-esc*.dtb
ls -rtl arch/arm/boot/dts/*.dtb |grep -Ei '(esc00*|mv3000*|dmt2000*)'



