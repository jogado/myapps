#!/bin/sh

mkdir -p ~/GITS

#=======================================================================================================================================
# Linux sources NXP
#=======================================================================================================================================
cd ~/GITS
git clone git://git.freescale.com/imx/linux-imx.git -b imx_4.1.15_2.0.0_ga
git clone git://git.freescale.com/imx/linux-imx.git -b rel_imx_4.9.x_1.0.0_ga
#=======================================================================================================================================
# iwave patches revision 2.0
#=======================================================================================================================================
cd ~/GITS/linux
patch -Np1 --binary < ~/PATCH/000PATCH002-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_1.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/boot/dts/Makefile
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file arch/arm/mach-imx/mach-dilupe.c
	#patching file arch/arm/mach-imx/Makefile
	#patching file drivers/input/touchscreen/ft5x06_ts.c
	#patching file drivers/input/touchscreen/ft5x06_ts.h
	#patching file drivers/input/touchscreen/Kconfig
	#patching file drivers/input/touchscreen/Makefile
patch -Np1 < ~/PATCH/001PATCH003-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_MICRO_SD.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 < ~/PATCH/002PATCH004-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_TEMP_SENSOR.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/003PATCH005-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_I2C_EEPROM.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/004PATCH006-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BACKLIGHT.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 < ~/PATCH/005PATCH007-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_EEPROM.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file drivers/rtc/rtc-ds1307.c
patch -Np1 < ~/PATCH/006PATCH008-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_TRICKLE.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file drivers/rtc/rtc-ds1307.c
patch -Np1 < ~/PATCH/007PATCH009-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BARCODE_NODE.patch
	#patching file drivers/leds/led-class.c
patch -Np1 < ~/PATCH/008PATCH010-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_ETHERNET_WITHOUT_1_WIRE.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 < ~/PATCH/009PATCH011-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_CDC_ACM.patch
	#patching file arch/arm/configs/imx_dilupe_defconfig

patch -Np1 < ~/PATCH/010PATCH012-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_1_WIRE_eeprom.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig

#=======================================================================================================================================
# iwave Linux Audio patch
#=======================================================================================================================================
patch -Np1 < ~/PATCH/Audio_patch.patch
	#(Stripping trailing CRs from patch; use --binary to disable.)
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#Hunk #1 succeeded at 77 with fuzz 2.
	#Hunk #2 succeeded at 139 (offset 8 lines).
	#(Stripping trailing CRs from patch; use --binary to disable.)
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#(Stripping trailing CRs from patch; use --binary to disable.)
	#patching file sound/soc/codecs/sgtl5000.c
	#(Stripping trailing CRs from patch; use --binary to disable.)
	#patching file sound/soc/fsl/imx-sgtl5000.c
	#(Stripping trailing CRs from patch; use --binary to disable.)
	#patching file sound/soc/Makefile












#=======================================================================================================================================
# Linux kernel Build
#=======================================================================================================================================
cp ~/Documents/doit-linux-iw-abt3000.sh .

#=======================================================================================================================================
# Export the Cross Compiler and tool chain path
#=======================================================================================================================================
export ARCH=arm
export PATH=$PATH:/opt/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-

#----------------------------------------------------------------------------------------------------------
#Configure for iWave-G18M-SM platform
#----------------------------------------------
make imx_dilupe_defconfig

#----------------------------------------------------------------------------------------------------------
#Compile 
#----------------------------------------------
make 

# Show result
#----------------------------------------------
ls -la arch/arm/boot/zImage 
ls -la arch/arm/boot/dts/imx6ul-dilupe.dtb

