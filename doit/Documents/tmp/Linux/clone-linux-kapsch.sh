#!/bin/sh

mkdir -p ~/GITS

#==================================================================================
# Install SDK
#==================================================================================
sh ~/Downloads/poky-prodata-glibc-x86_64-v790-base-image-armv7a-vfp-neon-toolchain-1.8.sh 


#==================================================================================
# Clone linux sources
# jgd:jogado01
#==================================================================================
export GIT_SSL_NO_VERIFY=1
git clone https://opensource.kapschcarrier.com/os/linux-imx.git linux-kapsch
git clone https://jgd:jogado01@opensource.kapschcarrier.com/os/linux-imx.git linux-kapsch


git checkout imx-prodata-3.10.17
git branch
git fetch --all
git fetch --tags


#==================================================================================
# Build kernel
#==================================================================================
. /opt/poky-prodata/1.8/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi 

cd GITS/linux-kapsch
make imxsolo_prodata_defconfig
make -j4
make uImage LOADADDR=0x10800000


scp uImage root@192.168.1.134:/boot
scp dts/dmt2000_2_0.dtb root@192.168.1.134:/boot/dtb
