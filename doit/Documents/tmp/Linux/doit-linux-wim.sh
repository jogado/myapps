#!/bin/sh


if [ ! -d "~/GITS/linux-wim" ]; then
	cd GITS
  	git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x linux-wim  
fi



# Export the Cross Compiler and tool chain path
#----------------------------------------------
export ARCH=arm
export PATH=$PATH:/opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-

# . /opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

#Configure for iWave-G18M-SM platform
#----------------------------------------------
# make imx_dilupe_defconfig
make imx_v6_v7_emsyscon_defconfig


#Compile the u-boot source code
#----------------------------------------------
make 
make uImage LOADADDR=0x80800000

# Show result
#----------------------------------------------
ls -la arch/arm/boot/zImage 
ls -la arch/arm/boot/uImage
ls -la arch/arm/boot/dts/imx6ul-esc-abt3000_1_0.dtb


