/**
 *
 *  @file   LinuxSerial.h
 *  @date   01-10-2016
 *  @author SEHCON/FO
 *
 *  @brief  linux serial port implementation
 *
 */

#ifndef     _PROXIMA_LIB_CARD_LINUXSERIAL_
#define     _PROXIMA_LIB_CARD_LINUXSERIAL_

#include <string>

#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <termios.h>

#include "libs/utils/serial/SerialExcept.h"
#include "libs/utils/ByteStr.h"

#if 0
#if !defined(TRACE_ALL) && !defined(TRACE_MAIN)
# define NO_TRACE_MODULE
#endif
#include "libs/utils/log/Trace++.h"
// No includes below this point!
#endif

#define INFINITE_TIMEOUT unsigned(-1)

namespace proxima
{
    namespace lib
    {
        namespace smartcard
        {
            namespace utils
            {
                namespace detail
                {

                    ///
                    /// \brief get_speedt
                    /// \param speed
                    /// \return
                    ///
                    inline speed_t get_speedt(int speed)
                    {
                        static const struct
                        {
                            int speed;
                            speed_t code;
                        }
                        table [] =
                        {
                            { 0,      B0 },
                            { 1200,   B1200 },
                            { 2400,   B2400 },
                            { 4800,   B4800 },
                            { 9600,   B9600 },
                            { 19200,  B19200 },
                            { 38400,  B38400 },
                            { 57600,  B57600 },
                            { 115200, B115200 },
                            { 230400, B230400 },
                        };

                        for (size_t i = 0; i < sizeof(table) / sizeof(*table); i++)
                        {
                            if (table[i].speed == speed)
                            {
                                return table[i].code;
                            }
                        }

                        return 0;
                    }
                } // namespace detail


                class SerialPort
                {
                    public:
                        enum Parity
                        {
                            none = 'N',
                            odd  = 'O',
                            even = 'E'
                        };

                        enum Databits
                        {
                            five  = 5,
                            six   = 6,
                            seven = 7,
                            eight = 8
                        };

                        enum Stopbits
                        {
                            one = 1,
                            two = 2
                        };

                        /// Constructor
                        /// \brief SerialPort
                        /// \param device
                        /// \param speed
                        /// \param databits
                        /// \param parity
                        /// \param stopbits
                        ///
                        SerialPort(const std::string& device, int speed, Databits databits = eight, Parity parity = none, Stopbits stopbits = one)
                            : fd(-1)
                        {
                            // get bitrate
                            speed_t comspeed = detail::get_speedt(speed);

                            // open device
                            if ((fd = open(device.c_str(), O_RDWR | O_NDELAY)) < 0)
                            {
                                SCLIB_THROW_EX(SystemException( QString( "open(%1)" ).arg( QString::fromStdString( device ) ) ) );
                            }

                            try
                            {
                                struct termios termios;

                                // get current settings
                                if (tcgetattr(fd, &termios))
                                {
                                    SCLIB_THROW_EX( SystemException( "tcgetattr" ) );
                                }
                    #ifndef __CYGWIN__
                                // set to RAW mode
                                cfmakeraw(&termios);
                    #endif
                                // return from read when all available data is read
                                termios.c_cc[VTIME] = 0;
                                termios.c_cc[VMIN] = 0;

                                // ignore modem control lines.
                                termios.c_cflag |= CLOCAL;
                                termios.c_cflag &= ~HUPCL;

                                // set speed
                                if (cfsetospeed(&termios, comspeed))
                                {
                                    SCLIB_THROW_EX( SystemException( "cfsetospeed" ) );
                                }
                                if (cfsetispeed(&termios, comspeed))
                                {
                                    SCLIB_THROW_EX( SystemException( "cfsetispeed" ) );
                                }

                                // set databits
                                termios.c_cflag &= ~(CS5|CS6|CS7|CS8);
                                switch (databits)
                                {
                                    case five:
                                        termios.c_cflag |= CS5;
                                        break;
                                    case six:
                                        termios.c_cflag |= CS6;
                                        break;
                                    case seven:
                                        termios.c_cflag |= CS7;
                                        break;
                                    case eight:
                                        termios.c_cflag |= CS8;
                                        break;
                                }

                                // set parity
                                switch (parity)
                                {
                                    case none:
                                        termios.c_cflag &= ~PARENB;
                                        break;
                                    case odd:
                                        termios.c_cflag |= PARODD;
                                        termios.c_cflag |= PARENB;
                                        break;
                                    case even:
                                        termios.c_cflag &= ~PARODD;
                                        termios.c_cflag |= PARENB;
                                        break;
                                }

                                // set stopbits
                                switch (stopbits)
                                {
                                    case one:
                                        termios.c_cflag &= ~CSTOPB;
                                        break;
                                    case two:
                                        termios.c_cflag |= CSTOPB;
                                        break;
                                }

                                // activate the termio structure
                                if (tcsetattr(fd, TCSANOW, &termios))
                                {
                                    SCLIB_THROW_EX( SystemException( "tcsetattr" ) );
                                }

                                // flush read buffer
                                if (tcflush(fd, TCIOFLUSH))
                                {
                                    SCLIB_THROW_EX( SystemException( "tcflush" ) );
                                }

                            }
                            catch (...)
                            {
                                close(fd);
                                throw;
                            }
                        }

                        ///
                        ~SerialPort()
                        {
                            if (fd != -1)
                            {
                                close(fd);
                            }
                        }

                        ///
                        /// \brief Read serial port implementation, n bytes in byte buffer
                        /// \param buf
                        /// \param n
                        /// \param timeOutMs
                        /// \return
                        ///
                        size_t Read(byte buf[], size_t n, unsigned& timeOutMs)
                        {
                            ssize_t count = read(fd, buf, n);
                            if ( count > 0 ) 
                            {
                                return size_t(count);
                            }

                            struct timeval timeout, *pTimeout;
                            if (timeOutMs == INFINITE_TIMEOUT)
                            {
                                pTimeout = 0;
                            }
                            else
                            {
                                timeout.tv_sec = timeOutMs / 1000;
                                timeout.tv_usec = (timeOutMs % 1000) * 1000;
                                pTimeout = &timeout;
                            }

                            fd_set rfds;
                            FD_ZERO(&rfds);
                            FD_SET(fd, &rfds);
                            int rc = select(fd + 1, &rfds, 0, 0, pTimeout);
                            if ( rc < 0 )
                            {
                                SCLIB_THROW_EX(SystemException("select"));
                            }
                            else if (rc == 0)
                            {
                                timeOutMs = 0;
                                return 0;
                            }
                            else if (pTimeout)
                            {
                                timeOutMs = pTimeout->tv_sec * 1000;
                                timeOutMs += pTimeout->tv_usec / 1000;
                            }

                            count = read(fd, buf, n);
                            if (count < 0)
                            {
                                SCLIB_THROW_EX(SystemException("read"));
                            }
                            return size_t(count);
                        }

                        ///
                        /// \brief Read serial port implementation, n bytes in byte stream
                        /// \param buf
                        /// \param n
                        /// \param timeOutMs
                        /// \return
                        ///
                        size_t Read(utils::ByteStr& buf, size_t n, unsigned& timeOutMs)
                        {
                            byte tmpbuf[n];
                            size_t count = Read(tmpbuf, n, timeOutMs);
                            if (count)
                            {
                                buf.append(tmpbuf, count);
                            }
                            return count;
                        }

                        ///
                        /// \brief Read serial port implementation, all bytes in byte buffer
                        /// \param buf
                        /// \param n
                        /// \param timeOutMs
                        ///
                        void ReadAll(byte buf[], size_t n, unsigned& timeOutMs)
                        {
                            while (n)
                            {
                                size_t count = Read(buf, n, timeOutMs);
                                if (count)
                                {
                                    n -= count;
                                    buf += count;
                                }
                                else if (!timeOutMs)
                                {
                                    /*SCLIB_THROW_EX(IOException() << error_mesg("Read timed out"));*/
                                }
                            }
                        }

                        ///
                        /// \brief Read serial port implementation, all bytes in byte stream
                        /// \param buf
                        /// \param n
                        /// \param timeOutMs
                        ///
                        void ReadAll(utils::ByteStr& buf, size_t n, unsigned& timeOutMs) {
                            while (n)
                            {
                                size_t count = Read(buf, n, timeOutMs);
                                if (count)
                                {
                                    n -= count;
                                }
                                else if (!timeOutMs)
                                {
                                    /*SCLIB_THROW_EX(IOException() << error_mesg("Read timed out"));*/
                                }
                            }
                        }

                        ///
                        /// \brief Write n bytes to serial port
                        /// \param buf
                        /// \param n
                        /// \param timeOutMs
                        /// \return
                        ///
                        size_t Write(const byte buf[], size_t n, unsigned& timeOutMs)
                        {
                            struct timeval timeout, *pTimeout;
                            if (timeOutMs == INFINITE_TIMEOUT)
                            {
                                pTimeout = 0;
                            }
                            else
                            {
                                timeout.tv_sec = timeOutMs / 1000;
                                timeout.tv_usec = (timeOutMs % 1000) * 1000;
                                pTimeout = &timeout;
                            }

                            fd_set wfds;
                            FD_ZERO(&wfds);
                            FD_SET(fd, &wfds);
                            int rc = select(fd + 1, 0, &wfds, 0, pTimeout);
                            if (rc < 0)
                            {
                                SCLIB_THROW_EX(SystemException("select"));
                            }
                            else if (rc == 0)
                            {
                                timeOutMs = 0;
                                return 0;
                            }
                            else if (pTimeout)
                            {
                                timeOutMs = pTimeout->tv_sec * 1000;
                                timeOutMs += pTimeout->tv_usec / 1000;
                            }

                            ssize_t count = write(fd, buf, n);
                            if (count < 0)
                            {
                                SCLIB_THROW_EX(SystemException("write"));
                            }
                            return size_t(count);
                        }

                        ///
                        /// \brief Write n bytes from byte stream to serial port
                        /// \param buf
                        /// \param timeOutMs
                        /// \return
                        ///
                        size_t Write(const utils::ByteStr& buf, unsigned& timeOutMs)
                        {
                            return Write(buf.c_data(), buf.length(), timeOutMs);
                        }

                        ///
                        /// \brief Write n bytes from byte stream to serial port
                        /// \param buf
                        /// \param timeOutMs
                        /// \return
                        ///
                        void WriteAll(const byte buf[], size_t n, unsigned& timeOutMs)
                        {
                            size_t offset = 0;
                            while (offset < n)
                            {
                                size_t count = Write(&buf[offset], n - offset, timeOutMs);
                                if (count)
                                {
                                    offset += count;
                                }
                                else if (!timeOutMs)
                                {
                                    /*SCLIB_THROW_EX(IOException() << error_mesg("Write timed out"));*/
                                }
                            }
                        }

                        ///
                        /// \brief Write n bytes byte stream from byte stream to serial port
                        /// \param buf
                        /// \param timeOutMs
                        /// \return
                        ///
                        void WriteAll(const utils::ByteStr& buf, unsigned& timeOutMs)
                        {
                            WriteAll(buf.c_data(), buf.length(), timeOutMs);
                        }

                    protected:
                        int fd;

                }; /* class SerialPort */
            }
        }
    }
}

#endif /* _PROXIMA_LIB_CARD_LINUXSERIAL_ */

