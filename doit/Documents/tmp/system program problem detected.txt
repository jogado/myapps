==============================================================================
getting system program problem detected pops-up
ref:http://askubuntu.com/questions/133385/getting-system-program-problem-detected-pops-up-regularly-after-upgrade
==============================================================================
Open a terminal (Ctrl+Alt+T)

sudo rm /var/crash/*    //Then hit Enter.
you can close all open popups with:  killall system-crash-notification
	
Here's how to disable Apport, the system that reports errors to Canonical:

Open your terminal and type:

gksudo gedit /etc/default/apport
And hit Enter.

Change enabled=1 to enabled=0, then save and exit.
==============================================================================
