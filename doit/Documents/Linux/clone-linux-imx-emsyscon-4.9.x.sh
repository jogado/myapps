#!/bin/sh

mkdir -p ~/GITS

#=======================================================================================================================================
# Linux sources from emsyscon
#=======================================================================================================================================
cd ~/GITS
git clone git@git.emsyscon.com:os/linux.git -b imx-emsyscon-4.9.x linux-imx-emsyscon-4.9.x
cd linux-imx-emsyscon-4.9.x
git fetch --all
git fetch --tags
git branch -a



mkdir -p ~/GIT
cd GITS
git clone git@git.emsyscon.com:os/linux.git linux-imx6s-emsyscon-4.9.x
cd linux-imx6s-emsyscon-4.9.x/
git fetch --all
git fetch --tags
git checkout imx-emsyscon-4.9.x
git branch -a

