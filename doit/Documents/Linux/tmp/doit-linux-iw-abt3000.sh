#!/bin/sh

# Export the Cross Compiler and tool chain path
#----------------------------------------------
export ARCH=arm
export PATH=$PATH:/opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-

# . /opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

#Configure for iWave-G18M-SM platform
#----------------------------------------------
make imx_dilupe_defconfig

#Compile the u-boot source code
#----------------------------------------------
make 

# Show result
#----------------------------------------------
ls -la arch/arm/boot/zImage 
ls -la arch/arm/boot/dts/imx6ul-dilupe.dtb


