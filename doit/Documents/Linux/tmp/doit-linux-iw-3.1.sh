#!/bin/sh

# Export the Cross Compiler and tool chain path
#----------------------------------------------
export ARCH=arm
export CROSS_COMPILE=arm-poky-linux-gnueabi-
export PATH=$PATH:/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi

# . /opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

#Configure for iWave-G18M-SM platform
#----------------------------------------------

#make imx_dilupe_defconfig

#Compile the u-boot source code
#----------------------------------------------
make 

# Show result
#----------------------------------------------

DD=$(date +'%Y%m%d%H%M%S')
rm arch/arm/boot/zImage-*
cp arch/arm/boot/zImage  arch/arm/boot/zImage-$DD
ls -la arch/arm/boot/zImage*


rm arch/arm/boot/dts/imx6ul-dilupe-*
cp arch/arm/boot/dts/imx6ul-dilupe.dtb arch/arm/boot/dts/imx6ul-dilupe-$DD.dtb
ls -la arch/arm/boot/dts/imx6ul-dilupe*


