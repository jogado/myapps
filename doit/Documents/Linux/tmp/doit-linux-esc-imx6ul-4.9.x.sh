#!/bin/sh

# Export the Cross Compiler and tool chain path
env1()
{
	export ARCH=arm
	export PATH=$PATH:/opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
}

env2()
{
	export ARCH=arm
	export PATH=$PATH:/opt/poky-emsyscon/2.4.1/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
}

# . /opt/iwqt5/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

env3()
{
	export ARCH=arm
	export PATH=$PATH:/opt/poky-emsyscon/2.4.1/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi
}




env3
make imx_v6_v7_emsyscon_defconfig
make 
make uImage LOADADDR=0x80800000

# Show result
#----------------------------------------------
ls -la arch/arm/boot/zImage 
ls -la arch/arm/boot/uImage 
ls -la arch/arm/boot/dts/imx6ul-esc-abt3000_1_0.dtb


