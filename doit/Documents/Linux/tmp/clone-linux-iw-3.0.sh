#!/bin/sh

mkdir -p ~/GITS


#=======================================================================================================================================
# Linux sources from iWave deliverables 
#=======================================================================================================================================
cd ~/GITS
tar -xvzf ~/PATCH/iw/REL3.0/linux/linux.tar.gz 
mv linux linux-iw-tar
#=======================================================================================================================================



#=======================================================================================================================================
# Linux sources NXP
#=======================================================================================================================================
cd ~/GITS
git clone git://git.freescale.com/imx/linux-imx.git -b imx_4.1.15_2.0.0_ga linux-iw-git
#=======================================================================================================================================









#=======================================================================================================================================
# iwave patches 
#=======================================================================================================================================
cd ~/GITS/linux-iw-git
cd ~/GITS/linux-iw-tar

patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/000PATCH002-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_1.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/boot/dts/Makefile
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file arch/arm/mach-imx/mach-dilupe.c
	#patching file arch/arm/mach-imx/Makefile
	#patching file drivers/input/touchscreen/ft5x06_ts.c
	#patching file drivers/input/touchscreen/ft5x06_ts.h
	#patching file drivers/input/touchscreen/Kconfig
	#patching file drivers/input/touchscreen/Makefile
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/001PATCH003-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_MICRO_SD.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/002PATCH004-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_TEMP_SENSOR.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/003PATCH005-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_I2C_EEPROM.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/004PATCH006-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BACKLIGHT.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/005PATCH007-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_EEPROM.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file drivers/rtc/rtc-ds1307.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/006PATCH008-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_RTC_TRICKLE.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file drivers/rtc/rtc-ds1307.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/007PATCH009-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_BARCODE_NODE.patch 
	#patching file drivers/leds/led-class.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/008PATCH010-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_ETHERNET_WITHOUT_1_WIRE.patch
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/009PATCH011-iW-EMEQC-SC-01-R1.0-REL2.0_Linux_Intermediate_Release_2_CDC_ACM.patch 
	#patching file arch/arm/configs/imx_dilupe_defconfig
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/011PATCH013-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SC_REV_update.patch 
	#patching file arch/arm/mach-imx/mach-dilupe.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/012PATCH014-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Buzzer.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file drivers/pwm/pwm-imx.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/013PATCH015-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SPI_FLASH.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/014PATCH016-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Light_Sensor.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file drivers/input/misc/isl29023.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/015PATCH017-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_LCD_dot_issue_fix.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/016PATCH018-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Dig_out_node.patch 
	#patching file drivers/leds/led-class.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/017PATCH019-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_LM75_Interrupt_Fix.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file drivers/hwmon/lm75.c
	#patching file drivers/hwmon/lm75.h
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/018PATCH020-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Brightness_value_fix.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/019PATCH021-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_Audio.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file sound/soc/codecs/sgtl5000.c
	#patching file sound/soc/fsl/imx-sgtl5000.c
	#patching file sound/soc/Makefile
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/020PATCH022-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_GPIO_LED.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file drivers/leds/led-class.c
patch -Np1 --binary < ~/PATCH/iw/REL3.0/linux/Patches/021PATCH023-iW-EMEQC-SC-01-R1.0-REL3.0_Linux_Intermediate_Release_3_SAM.patch 
	#patching file arch/arm/boot/dts/imx6ul-dilupe.dts
	#patching file arch/arm/configs/imx_dilupe_defconfig
	#patching file drivers/mxc/Kconfig
	#patching file drivers/mxc/Makefile
	#patching file drivers/mxc/sam/Kconfig
	#patching file drivers/mxc/sam/Makefile
	#patching file drivers/mxc/sam/nsam.c
	#patching file drivers/mxc/sam/nsam.h
	#patching file drivers/mxc/sam/nsam_sm.c
	#patching file drivers/mxc/sam/sam.h



#=======================================================================================================================================
# Linux kernel Build
#=======================================================================================================================================
cd ~/GITS/linux-iw-git
cd ~/GITS/linux-iw-tar
#=======================================================================================================================================
# Export the Cross Compiler and tool chain path
#=======================================================================================================================================

------------------------------------------------------------------------------------------------------------
export ARCH=arm
export PATH=$PATH:/opt/iw/fsl-imx-x11/4.1.15-2.0.3/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi
export CROSS_COMPILE=arm-poky-linux-gnueabi-
make imx_dilupe_defconfig
make 
------------------------------------------------------------------------------------------------------------
. /opt/iw/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi 
make imx_dilupe_defconfig
make 
------------------------------------------------------------------------------------------------------------


ls -la arch/arm/boot/zImage 
ls -la arch/arm/boot/dts/imx6ul-dilupe.dtb

