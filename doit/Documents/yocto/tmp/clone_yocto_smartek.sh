==============================================================================================================================================
# iWave G18D
==============================================================================================================================================
mkdir -p ~/yocto/smartek
cd ~/yocto/smartek

repo init -u git@git.emsyscon.com:smartek/yocto-manifests.git -m smartek-default.xml
repo sync



cd ~/yocto/smartek
#DISTRO=fsl-imx-x11 MACHINE=imx6-iwg18 source setup-environment build
source ./setup-env build

bitbake mv3000-smartek-image

