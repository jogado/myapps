#==================================================================================================================
# EMSYSCON FILESYSTEM BASED ON ROCO
#=======================================================================================================================
mkdir -p ~/yocto/emsyscon_wim ; cd ~/yocto/emsyscon_wim

repo init -u git@git.emsyscon.com:os/yocto-manifests.git
repo sync

#=======================================================================================================================
#ls -la ../sources/meta-emsyscon/conf/machine/
# esc-at91-sodimm.conf
# esc-imx6s-sodimm.conf
# esc-imx6ul.conf
#=======================================================================================================================
source ./setup-env build esc-imx6ul





#=======================================================================================================================
# ls -la ../sources/meta-emsyscon/recipes-kernel/linux/
# 		linux-at91-esc_3.11.bb
# 		linux-imx-esc_4.9.bb
# 		linux-imxsolo_3.10.17.bb
#
#	check: PREFERRED_PROVIDER_virtual/kernel = "linux-imx-esc"
#=======================================================================================================================
gedit ../sources/meta-emsyscon/conf/machine/esc-imx6ul.conf


#=======================================================================================================================
# ls -la ../sources/meta-emsyscon/recipes-core/images/
# 		emsyscon-abt3000-demo.bb
# 		emsyscon-abt3000-image.bb
# 		emsyscon-base-image.bb
# 		emsyscon-full-image.bb
# 		emsyscon-qt4-sdk.bb
# 		emsyscon-qt5-sdk.bb
# 		emsyscon-ramdisk-image.bb
#=======================================================================================================================
bb emsyscon-abt3000-image
bb emsyscon-abt3000-demo

