#!/bin/sh

#==============================================================================
cd ~/yocto/thud
source ./setup-env build-imx6s  esc-imx6s-sodimm

	bitbake emsyscon-base-image
	bitbake emsyscon-full-image
	bitbake emsyscon-qt5-sdk

	bitbake emsyscon-mv3000-demo
	bitbake emsyscon-mg3000-demo

	bitbake emsyscon-qt5-sdk -c populate_sdk
#==============================================================================
cd ~/yocto/thud
source ./setup-env build-imx6ul esc-imx6ul

	bitbake emsyscon-base-image
	bitbake emsyscon-full-image
	bitbake emsyscon-qt5-sdk
	bitbake emsyscon-abt3000-demo

	bitbake emsyscon-qt5-sdk -c populate_sdk
#==============================================================================







