#!/bin/sh


IPK_FILE="eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh"
uImage="uImage.olivier.2"
dts="imx6dl-esc-dmt2000_2_0.dtb.2"

sync
sync
cp /home/root/$uImage   /boot/uImage
cp /home/root/$dts      /boot/dtb/imx6dl-esc-dmt2000_2_0.dtb
sync
sync
sh /home/root/$IPK_FILE
sync
sync
rm /home/root/$uImage  
rm /home/root/$dts  
rm /home/root/update2.sh
beep   
echo "rebooting ..."
sleep 3 
reboot
