How to upgrade device:

	IMAGE   : emsyscon-qt5-sdk-esc-imx6s-sodimm-20191211013118.rootfs.tar.bz2 
	PACKAGE : eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh


1) Startup DMT2000 and start a terminal session ( debug port )

2) Type ifconfig  (to retrieve the device Ip address )

3) copy files to dmt2000:/home/root ( use winscp )
	<IMAGE> 
	reflash-mydevice
	
4) On console type :

	sh ./reflash-mydevice -r emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2  -d imx6dl-esc-dmt2000_2_0
	sync
	reboot


5) On dmt2000 screen you can observe that the flashing process should going on. 
   Wait until you hear three beep (end of the process) and reboot
	

6) Break boot process
	Type:
		env default -a
		setenv fdt_high 0xffffffff	
		saveenv	
		boot

7) ifconfig check dmt2000 IP address

8) cp to dmt2000:/home/root

	eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh
	imx6dl-esc-dmt2000_2_0.dtb.2
	uImage.olivier.2
	update2.sh
	reboot











































1) Startup DMT2000 and start a terminal session ( debug port )
2) Type ifconfig  (to retrieve the device Ip address )

3) copy files to dmt2000:/home/root ( use winscp )
	<IMAGE> 
	reflash-mydevice
	
4) On console type :

	sh ./reflash-mydevice -r emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2  -d imx6dl-esc-dmt2000_2_0
	reboot

	#init 4
	#rootfs-install -p 2 <PACKAGE>
	#wait end of installation & reboot

5) 


5) Break boot
	env default -a
	setenv partition 2
	saveenv
	boot

6) ifconfig 
7) cp to dmt2000:/home/root

	eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh
	emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2
	imx6dl-esc-dmt2000_2_0.dtb.2
	uImage.olivier.2
	update.sh

8) sh ./update.sh
 



============================================================================================
	FLASH PARTITION 2
============================================================================================
rootfs-install -p 2 emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2






============================================================================================
	REMOVE OR REFLASH FIRST PARTITION
============================================================================================
mkdir /mnt/p1
mount /dev/mmcblk0p1 /mnt/p1
	--------------------------------------------
	rm -rf /mnt/p1/*
	--------------------------------------------
	or

	--------------------------------------------
	cd
	cp /mnt/p1/home/root/emsys* .
	umount /mnt/p1
	rootfs-install -p 1 emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2
	--------------------------------------------
============================================================================================

============================================================================================
	COPY & INSTALL PACKAGE
============================================================================================
package: eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh 
Transfer package to DMT2000 (use WinSCP) 

sh ./eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh
============================================================================================









- eks-dmt2000-ves_2017.03.03-1.0.0_armv5te.ipk
- - place anywhere  on the DMT2000 device
- - create /home/root/MOS for user root if not existing 
- - opkg install eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk --force-overwrite --force-downgrade
- - # reboot
