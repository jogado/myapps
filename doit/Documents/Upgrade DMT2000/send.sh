#!/bin/sh

IMAGE="emsyscon-dmt2000-demo-esc-imx6s-sodimm-20191129094721.rootfs.tar.bz2"
SCRIPT1="reflash-mydevice"
SCRIPT3="update.sh"

IPK_FILE="eks-dmt2002-ves_2019.12.05-6.0.93_cortexa9t2hf-neon.ipk.sfx.sh"
uImage="uImage.olivier.2"
dts="imx6dl-esc-dmt2000_2_0.dtb.2"
SCRIPT2="update2.sh"


if echo $1 | grep -q "1"; then
	if echo $2 | grep -q "192.168"; then
		scp  $SCRIPT1 	root@$2:
	        scp  $IMAGE 	root@$2:
	        scp  $SCRIPT3 	root@$2:
	fi
fi



if echo $1 | grep -q "2"; then
	if echo $2 | grep -q "192.168"; then
		scp  $SCRIPT2 	root@$2:
	        scp  $dts 	root@$2:
	        scp  $uImage 	root@$2:
	        scp  $IPK_FILE	root@$2:
	fi
fi


