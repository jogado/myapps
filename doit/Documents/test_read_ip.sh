#!/bin/sh

GOOD=0
BAD=0

while [ 1 ] 
do
	REF=$1

	val=`esc-read-dallas-ip | cut -c 1-13`

	if  echo "$val" | grep $REF; then
		GOOD=$((GOOD+1))
	else
		BAD=$((BAD+1))
	fi
	echo "Good read $GOOD / Bad read = $BAD"
done
