
#==============================================================================
source ./setup-env build-imx6s       esc-imx6s-sodimm
source ./setup-env build-imx6ul      esc-imx6ul
source ./setup-env build-imx6ul-sysd esc-imx6ul-sysd

#==============================================================================
cd ~/yocto/thud
source ./setup-env build-imx6s  esc-imx6s-sodimm
	bitbake emsyscon-mv3000-demo
	bitbake emsyscon-mg3000-demo
	bitbake emsyscon-dmt2000-demo
	bitbake emsyscon-qt5-sdk -c populate_sdk
#==============================================================================
cd ~/yocto/thud
source ./setup-env build-imx6ul esc-imx6ul
	bitbake emsyscon-abt3000-demo
	bitbake emsyscon-qt5-sdk -c populate_sdk
#==============================================================================

