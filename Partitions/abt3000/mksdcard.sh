#!/bin/sh

# wait for the SD/MMC device node ready
while [ ! -e $1 ]
do
sleep 1
echo “wait for $1 appear”
done

# call sfdisk to create partition table
# destroy the partition table
node=$1
dd if=/dev/zero of=${node} bs=1M count=2

sfdisk --force ${node} << EOF
2M,1024M,83
1027M,1024M,83
2052M,1024M,83
3077M,,83
EOF
