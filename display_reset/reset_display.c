#include <linux/fb.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

int fb_reset(const char* fb_path) {
    int fb = open(fb_path, O_RDWR);

    if (fb < 0) return -1;

    struct fb_var_screeninfo vinfo;
    if (ioctl(fb, FBIOGET_VSCREENINFO, &vinfo)) {
        close(fb);
        return -1;
    }

    // Trigger a mode change, which might reset the display
    ioctl(fb, FBIOPUT_VSCREENINFO, &vinfo);
    printf("xres: %u\n", vinfo.xres);
    printf("yres: %u\n", vinfo.yres);
    printf("xres_virtual: %u\n", vinfo.xres_virtual);
    printf("yres_virtual: %u\n", vinfo.yres_virtual);
    printf("xoffset: %u\n", vinfo.xoffset);
    printf("yoffset: %u\n", vinfo.yoffset);
    printf("bits_per_pixel: %u\n", vinfo.bits_per_pixel);
    printf("red.offset: %u\n", vinfo.red.offset);
    printf("red.length: %u\n", vinfo.red.length);
    printf("green.offset: %u\n", vinfo.green.offset);
    printf("green.length: %u\n", vinfo.green.length);
    printf("blue.offset: %u\n", vinfo.blue.offset);
    printf("blue.length: %u\n", vinfo.blue.length);
    printf("transp.offset: %u\n", vinfo.transp.offset);
    printf("transp.length: %u\n", vinfo.transp.length);
    printf("height: %d\n", vinfo.height);
    printf("width: %d\n", vinfo.width);
    printf("pixclock: %u\n", vinfo.pixclock);
    printf("left_margin: %u\n", vinfo.left_margin);
    printf("right_margin: %u\n", vinfo.right_margin);
    printf("upper_margin: %u\n", vinfo.upper_margin);
    printf("lower_margin: %u\n", vinfo.lower_margin);
    printf("hsync_len: %u\n", vinfo.hsync_len);
    printf("vsync_len: %u\n", vinfo.vsync_len);

    close(fb);

    return 0;
}

int main() {

    printf( "/dev/fb0 info's\n\r");
    printf( "================\n\r");

    fb_reset("/dev/fb0");
    return 0;
}

