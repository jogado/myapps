/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <linux/serial.h>

#ifndef TIOCSRS485
#define TIOCSRS485  0x542F
#endif

#define VERSION "test_rs485_232_loopback V00:00 14/03/2016"
#define MAX_LOOPBACK_SIZE 32

const char * port_1_path="/dev/rs232";
const char * port_2_path="/dev/rs485";
static int test = 0, count = 1, baud = 9600, verbose = 0, rs485_mode = 2, one_direction = 0;

enum TEST_IDS {
    TEST_ID_TWO_PORT_LOOPBACK,
    TEST_ID_ONE_PORT_LOOPBACK,
};


static int serial_open( const char * path, int baud, int rs485_mode ){
	int fd;
    static struct termios tios;
    //struct serial_rs485 rs485conf;
    
    printf("path %s  baud %d  mode %s\n", path, baud, rs485_mode ? "RS485" : "RS232" );	

	fd = open(path, O_RDWR | O_NOCTTY );
	if (fd < 0){
        perror("error open");
		return -errno;
	}
	
/*
    if( rs485_mode ){
        memset( & rs485conf, 0, sizeof( rs485conf ));
        rs485conf.flags = SER_RS485_ENABLED;
        if( ioctl( fd, TIOCSRS485, & rs485conf ) < 0 ){
            perror("error ioclt %s");
            close(fd);
            return -errno;
        }
    }
*/

    memset( & tios, 0x00, sizeof(tios) );
    tios.c_cflag = CS8 | CLOCAL | CREAD | PARENB | CMSPAR | PARODD;
    
    switch( baud ){
        case 4800:
            tios.c_cflag |= B4800;
            break;
        case 9600:
            tios.c_cflag |= B9600;
            break;
        case 19200:
            tios.c_cflag |= B19200;
            break;
        case 38400:
            tios.c_cflag |= B38400;
            break;
        case 57600:
            tios.c_cflag |= B57600;
            break;
        case 115200:
            tios.c_cflag |= B115200;
            break;
        default:
            tios.c_cflag |= B115200;
            break;
    }
    tios.c_lflag &= ~(ICANON | ECHO | ISIG);
	tios.c_iflag = IGNPAR;
    tios.c_oflag = 0;
	
	if(tcsetattr(fd, TCSANOW, &tios)){
        perror("error tcsetattr");
		close(fd);
		return -errno;
	}

    if( tcflush( fd, TCIFLUSH )){
        perror("error tcflush");
		close(fd);
		return -errno;
    }

	return fd;
}

static int port_read( int fd, unsigned char * buf, unsigned short len ){
    int res, rdlen;
    fd_set fds;
    struct timeval tv;
    unsigned short remlen, totlen;

    for( remlen = len, totlen = 0; remlen; ){
        tv.tv_sec = 1;
        tv.tv_usec = 0;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
		res = select( fd+1, &fds, NULL, NULL, & tv );
		if(res < 0){
			if(errno == EAGAIN) {
				continue;
			} else {
				perror("select");
				return -errno;
			}
        } 
        if( res == 0 ){
            printf("read timeout\n");
            return -1;
        }
        if( FD_ISSET( fd, & fds )){
            if( ( res = rdlen = read( fd, buf + totlen, remlen ) ) < 0 ){
                perror("read");
                break;
            }
            if( ! rdlen ){
                printf( "EOF\n" );
                break;
            }
            remlen -= rdlen;
            totlen += rdlen;
        }
    }
    return 0;
}


int RTS_flag;


static int test_two_port_loopback( int fd1, int fd2 ){
    int res;
    unsigned char tx_dat[ MAX_LOOPBACK_SIZE ];
    unsigned char rx_dat[ MAX_LOOPBACK_SIZE ];

    memset( tx_dat, 0x55, MAX_LOOPBACK_SIZE );
    memset( rx_dat, 0x00, MAX_LOOPBACK_SIZE );



    RTS_flag = TIOCM_RTS;
    if( (res=ioctl(fd2,TIOCMBIS,&RTS_flag))) { //Set RTS pin
        perror("ioctl error:" );
        return res;
    }
	

    if( ( res = write( fd1, tx_dat, MAX_LOOPBACK_SIZE ) ) != MAX_LOOPBACK_SIZE ){
        perror("Error write port 1" );
        return -errno;
    }

    if( ( res = port_read( fd2, rx_dat, MAX_LOOPBACK_SIZE ) ) ){
        printf( "Error read port 2: (%d)\n", res );
        return res;
    }


    if( memcmp( rx_dat, tx_dat, MAX_LOOPBACK_SIZE ) ){
        printf("Error data sent port 1 != data received port 2\n");
        return -1;
    }

    printf( "Write Port 1 , Read Port 2 : OK\n" );	

    if( one_direction ) {
        printf( "OK\n" );
    } else {

    	RTS_flag = TIOCM_RTS;
    	if( (res=ioctl(fd2,TIOCMBIC,&RTS_flag))) {//Clear RTS pin
            perror("ioctl error:" );
            return res;
        }

        memset( rx_dat, 0x00, MAX_LOOPBACK_SIZE );
        if( ( res = write( fd2, tx_dat, MAX_LOOPBACK_SIZE ) ) != MAX_LOOPBACK_SIZE ){
            perror("Error write port 2" );
            return -errno;
        }


        if( ( res = port_read( fd1, rx_dat, MAX_LOOPBACK_SIZE ) ) ){
            printf( "Error read port 1: (%d)\n", res );
            return res; 
        }
        if( memcmp( rx_dat, tx_dat, MAX_LOOPBACK_SIZE ) ){
            printf("Error data sent port 2 != data received port 1\n");
            return -1;
        }
        printf( "Write Port 2 , Read Port 1 : OK\n" );	
    }
    printf( "OK\n" );
    return 0;
}

static int test_one_port_loopback( int fd ){
    int res;
    unsigned char tx_dat[ MAX_LOOPBACK_SIZE ];
    unsigned char rx_dat[ MAX_LOOPBACK_SIZE ];

    memset( tx_dat, 0x55, MAX_LOOPBACK_SIZE );

    memset( rx_dat, 0x00, MAX_LOOPBACK_SIZE );
    if( ( res = write( fd, tx_dat, MAX_LOOPBACK_SIZE ) ) != MAX_LOOPBACK_SIZE ){
        perror("Error write" );
        return -errno;
    }
    if( ( res = port_read( fd, rx_dat, MAX_LOOPBACK_SIZE ) ) ){
        printf( "Error read : (%d)\n", res );
        return res;
    }
    if( memcmp( rx_dat, tx_dat, MAX_LOOPBACK_SIZE ) ){
        printf("Error data sent != data received\n");
        return -1;
    }
    printf( "OK\n");
    return 0;
}

static void usage(const char *pname)
{
    fprintf(stderr, VERSION "\n\n");
	fprintf(stderr, "usage: %s [-t TEST_ID][-p PATH_PORT_1][-P PATH_PORT_2][-b BAUD][-r RS485_MODE]\n", pname);
    fprintf(stderr, "\t-t TEST_ID : ( default = 0 )\n");
    fprintf(stderr, "\t\t%d = twoo port loopback test\n", TEST_ID_TWO_PORT_LOOPBACK );
    fprintf(stderr, "\t\t%d = one port loopback test\n", TEST_ID_ONE_PORT_LOOPBACK );
	fprintf(stderr, "\t-p PATH_PORT_1 (default: %s)\n", port_1_path);
    fprintf(stderr, "\t-P PATH_PORT_2 (default: %s)\n", port_2_path);
    fprintf(stderr, "\t-b BAUDRATE : ( default = %d  )\n", baud );
    fprintf(stderr, "\t-r RS485_ON_OFF ( RS485 enable / disable, b0 = port 1 / b1 = port 2,\n\t\tdefault = %d )\n", rs485_mode );
    fprintf(stderr, "\t-c COUNT ( repeat test COUNT times, default = %d )\n", count );
    fprintf(stderr, "\t-o two port one direction test ( default : bi directional )\n" );
    fprintf(stderr, "\t-v : verbose\n");
    fprintf(stderr, "\t-h show this info\n");
}

static int parse_cmdline(int argc, char *argv[])
{
	int opt;

	while((opt = getopt(argc, argv, "p:P:t:b:r:c:ovh")) != -1) {
		switch (opt){
			case 'p':
				port_1_path = optarg;
				break;
            case 'P':
				port_2_path = optarg;
				break;
            case 't':
                test = atoi( optarg );
                break;
            case 'c':
                count = atoi( optarg );
                break;
            case 'b':
                baud = atoi( optarg );
                break;
            case 'r':
                rs485_mode = atoi( optarg );
                break;
            case 'o':
                one_direction = 1;
                break;
            case 'v':
                verbose = 1;
                break;
			default:
				return -1;
		}
	}
	return 0;
}

int main(int argc, char *argv[])
{
	int fd1 = -1, fd2 = -1, err = 0, try;

	if (parse_cmdline(argc, argv)) {
		usage(argv[0]);
		return 1;
	}

    if( ( fd1 = serial_open( port_1_path, baud, rs485_mode & 0x01 ) ) < 0  )
    {
	err=-1;
        goto end;
    }
    if( test == TEST_ID_TWO_PORT_LOOPBACK ){
        if( ( fd2 = serial_open( port_2_path, baud, rs485_mode & 0x02 ) ) < 0 )
	{
	    err=-1;
            goto end;
	}
    }

    for( try = 0; try < count && err == 0; ++try ){
        switch (test) {
            case TEST_ID_TWO_PORT_LOOPBACK:
                err = test_two_port_loopback( fd1, fd2 );
                break;
            case TEST_ID_ONE_PORT_LOOPBACK:
                err = test_one_port_loopback( fd1 );
                break;
            default:
                printf( "Error : invalid selection : %d\n", test );
                usage(argv[0]);
                err = 1;
                break;
        }
        if( count > 1 && verbose )
            printf( "cnt = %d\n", try );
    }


end:
    if( fd1 >= 0 )
        close(fd1);
    if( fd2 >= 0 )
        close(fd2);

    return err;
}

