#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>


#include <sys/ioctl.h> //ioctl() call defenitions


const char * port_1_path="/dev/rs232";
const char * port_2_path="/dev/rs485";
static int test = 0, count = 1, baud = 9600, loopback_size = 0, verbose = 0, rs485_mode = 2, one_direction = 0;



static void usage(const char *pname)
{
    fprintf(stderr, "\t-h show this info\n");
}

static int parse_cmdline(int argc, char *argv[])
{
	int opt;

	while((opt = getopt(argc, argv, "p:P:t:b:r:c:ovh")) != -1) {
		switch (opt){
			case 'p':
				port_1_path = optarg;
				break;
            case 'P':
				port_2_path = optarg;
				break;
            case 't':
                test = atoi( optarg );
                break;
            case 'c':
                count = atoi( optarg );
                break;
            case 'b':
                baud = atoi( optarg );
                break;
            case 'r':
                rs485_mode = atoi( optarg );
                break;
            case 'o':
                one_direction = 1;
                break;
            case 'v':
                verbose = 1;
                break;
			default:
				return -1;
		}
	}
	return 0;
}

















int main(int argc, char *argv[])
{
	int fd;
	int RTS_flag;
	int ret;



	if (parse_cmdline(argc, argv)) {
		usage(argv[0]);
		return 1;
	}



	printf("Device : %s\n\r",port_1_path);


        fd = open(port_1_path,O_RDWR | O_NOCTTY );//Open Serial Port
	
	

   //========================================================
   RTS_flag = TIOCM_RTS;
   ret=ioctl(fd,TIOCMBIS,&RTS_flag);  //Set RTS pin
   if(ret) 
   {
           perror("ioclt %s");
            close(fd);
            return -errno;
   }
   printf("Set RTS pin       : OK\n\r");
   //========================================================

   sleep (1);

   //========================================================
   ret=ioctl(fd,TIOCMBIC,&RTS_flag);//Clear RTS pin
   if(ret) 
   {
           perror("ioclt %s");
            close(fd);
            return -errno;
   }
   printf("Reset RTS pin     : OK\n\r");
   //========================================================
	sleep(1);
   //========================================================
  RTS_flag = TIOCM_RTS;
   ret=ioctl(fd,TIOCSRS485,&RTS_flag);//Clear RTS pin
   if(ret) 
   {
           perror("ioclt %s");
            close(fd);
            return -errno;
   }
   printf("Reset RTS485 mode : OK\n\r");

  close(fd);
}
