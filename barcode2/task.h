#ifndef _BUILD_TASK_H
#define _BUILD_TASK_H


#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/time.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>


#define MAX_TASKS 5

extern void 	*Task0			( void *arg  );
extern void 	*Task1			( void *arg  );
extern void 	*Task2			( void *arg  );
extern void 	*Task3			( void *arg  );


extern pthread_t tid[];

extern int 	GetTaskID		( void       );

#endif
