#ifndef _BUILD_SOCKET_MASTER_H
#define _BUILD_SOCKET_MASTER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>
#include <time.h> 
#include <unistd.h>
#include <errno.h>


extern int init_master_socket       ( int master_port );
extern int list_master_socket       ( void );

#endif  
