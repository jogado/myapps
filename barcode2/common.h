#ifndef _BUILD_COMMON_H
#define _BUILD_COMMON_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ctype.h>

extern int hex                      ( char c);
extern int ReadDataOneLine          ( char *ifile , char *data , int MaxLen );
extern int WriteValue				( char *ifile , char *data );

extern int mystrcmp                 (char *s1, char *s2);

#endif
