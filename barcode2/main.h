#ifndef	__MAIN_H__
#define	__MAIN_H__

extern int  	main 					( int argc, char **argv ) ;
extern void 	BARCO_Info				( void );
extern void 	BARCO_Settings				( void );

extern void 	BARCO_Illumination_Read_on		( void );
extern void 	BARCO_Illumination_Keep_on		( void );
extern void 	BARCO_Illumination_off			( void );
extern void 	BARCO_Illumination_wink			( void );

extern void 	BARCO_Sensitivity			( void );
extern void 	BARCO_Enable_Timeout_between_Decodes	( void );
extern void 	BARCO_Illuminace			( void );

extern void 	to_barcode				( char *s,int len);
extern void 	logfile					( char *s,int len);
extern int  	set_interface_attribs 			( int fd, int speed, int parity);
extern void 	set_blocking 				( int fd, int should_block) ;
extern void 	store 					( unsigned char c ) ;

extern int 	IsRackindaPresent			( void );
extern void 	show_help				( void );

extern char 	*Build_dateTime				( void );
#endif



