 #include "socket_master.h"  



int                 serv_listenfd    = 0;
int                 serv_connfd      = 0;
struct sockaddr_in  serv_addr;
char                serv_sendBuff[1025];




int init_master_socket(int master_port )
{

    serv_listenfd = socket(AF_INET, SOCK_STREAM, 0);
    memset(&serv_addr, '0', sizeof(serv_addr));
    memset(serv_sendBuff, '0', sizeof(serv_sendBuff)); 

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(master_port); 


    bind(serv_listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); 

    listen(serv_listenfd, 10); 

    return(0);

}



char buffer[256];
struct sockaddr_in  cli_addr;
unsigned int clilen;

extern int ShowAlive;
extern char socket_cmd[];

int list_master_socket( void )
{
    time_t ticks; 
    int n;



    serv_connfd = accept(serv_listenfd, (struct sockaddr*)&cli_addr, &clilen); 

  
    puts("Connection accepted");

    bzero(buffer,256);
    n = read( serv_connfd,buffer,255 );

    if (n < 0) {
         perror("ERROR reading from socket");
         exit(1);
    }

    printf("Your request : %s\n",buffer);

    sprintf(socket_cmd,"%s",buffer);
    
    ticks = time(NULL);
    snprintf(serv_sendBuff, sizeof(serv_sendBuff), "My response : %.24s\r\n", ctime(&ticks));
    write(serv_connfd, serv_sendBuff, strlen(serv_sendBuff)); 

    close(serv_connfd);

    return(0);
}


