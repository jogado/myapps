#include "task.h"  


pthread_t tid[MAX_TASKS];


char *TaskNames[9]={
    (char *) "Clock          ",
    (char *) "BRC listner    ",
    (char *) "Socket Listner ",
    (char *) "Light Sensor   ",
    (char *) "task_5",
    (char *) "task_6",
    (char *) "task_7",
    (char *) "task_8",
    (char *) "task_9",
};

//=============================================================================
int GetTaskID(void)
{
	int Task_ID = -1;
	int i;

   	pthread_t id = pthread_self();

    	for(i=0; i < MAX_TASKS ; i++)
    	{
		if(pthread_equal(id,tid[i]))
		{
			Task_ID = i;
        	printf("Task[%d] :%s (started) \n\r",Task_ID,TaskNames[i]);
			break;
		}
	}
	return(Task_ID);	
}
//=============================================================================
