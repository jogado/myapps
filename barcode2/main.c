#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>

#include <ctype.h>

#include "buildate.h"
#include "main.h"
#include "common.h"
#include "comms.h"
#include "socket_master.h"  
#include "task.h"  




extern int  CheckHexaFile       ( void );
extern void to_barcode_hex      ( char *s , int len );
extern int  main 				( int argc, char **argv ) ;


char portname[]  = "/dev/barcode";
char output_path [200];
char hexcmd[200];
char strcmd[100];
char socket_cmd[512];


int  fd ;
int  Baudrate = B9600 ;
int  Debug    = 0;
int  DoBeep   = 1;


int Elapsed_ms=0;
int ShowAlive = 0;




void to_barcode(char *s,int len)
{

    printf("to_barcode(%s,%d)\n\r",s,len);
	write (fd, s , len);           // send 7 character greeting
	usleep (100000);
}


char aHex[100];


void to_barcode_hex(char *s,int len)
{
    int i   = 0;        
    int val1 = 0;
    int val2 = 0;

    printf("to_barcode_hex(%s,%d)\n\r",s,len);


    printf("TX[%d]:",len/2);
    for( i=0 ; i < (len/2); i++ )
    {
        val1 = hex( s[i*2+0] );
        val2 = hex( s[i*2+1] );
        if( val1 < 0 || val2 < 0) 
        {
            return;
        }
        aHex[i]= ( (val1<<4) + val2) &0x0ff ;
        printf(" %02X",aHex[i]);
    }   
    printf("\n\r");
    
    

	write (fd, aHex , len/2);           // send 7 character greeting
	usleep (100000);
}


//================================================================================================================================
void logfile(char *s,int len)
{
	FILE *ftp;
	ftp = fopen( output_path , "w" );
	fwrite(s , 1 , len , ftp );
	fwrite("\n\r" , 1 , 2 , ftp );
	fclose(ftp);
}
//================================================================================================================================

//=============================================================================================================================
#define MAX_LEN 2000
char buf [MAX_LEN+10];
int  Offset = 0 ;

void store (unsigned char c)
{
	if(Debug ) printf("store (0x%02X)\n\r",c);

	if( (c == 0x0a || c == 0x0d )  )
	{
		if(Offset)
		{	
			if(DoBeep) 
                system("beep &");

     		//if(Debug ) 
                printf("%s\n\r",buf);

     		logfile(buf,Offset);
    	 	buf[0]=0;
     		Offset=0;
		}
     	return;
	}

	if( Offset < MAX_LEN )
	{
		buf[Offset]=c;
		Offset++;
		buf[Offset]=0;
	}
}
//=============================================================================================================================




//=============================================================================================================================
void show_help(void)
{
	printf("\n\r");
	printf("barcode (%s)\n\r",Build_dateTime() );
	printf("-----------------------------\n\r");
	printf("usage: barcode <options> <...>\n\r");
	printf("Options:\n\r");
	printf("    -h      : This info\n\r");
	printf("    -dev    : change default device\n\r");
	printf("    -output : Output file\n\r");
	printf("    -bps    : Change baudrate\n\r");
	printf("    -debug  : Debug on\n\r");
	printf("    -beep   : Beep on valid read\n\r");
	printf("    -strcmd : Send a command to barcode (ascci string)\n\r");
	printf("    -hexcmd : Send a command to barcode (hexa  string)\n\r");
	exit(0);;
}
//=============================================================================================================================



int CheckHexaFile()
{
    int flen=strlen(hexcmd);
    int i;
    char c;

    if(flen %2)
    {
        printf("error : Odd string len\n\r");
        return(0);
    }

    for(i=0; i < flen ; i ++ )    
    {
        c= hexcmd[i];

        if( c >= '0' && c <= '9' ) continue;
        if( c >= 'a' && c <= 'f' ) continue;
        if( c >= 'A' && c <= 'F' ) continue;
     

        printf("error : wrong hexa character <%c>\n\r",c);

        return(0);
    }

    if(Debug) printf("CheckHexaFile(%s) OK\n\r",hexcmd);
    return (1);
}




//=============================================================================================================================
int main ( int argc, char **argv ) 
{
	int i;
	char *p;
	char barcode_path[200];
    int new_bps;
	int err;

    init_master_socket(5000);  // 5000: DEV_BARCODE
    

    memset(hexcmd,0x00,sizeof(hexcmd));
    memset(strcmd,0x00,sizeof(strcmd));

	sprintf(barcode_path,"%s",portname);
	sprintf(output_path ,"%s" , "/tmp/barcode.data");

	for(i=1 ; i < argc ; i++)
	{
		p=argv[i];

		if( p[0] != '-')break;

		if(memcmp(p,"-beep"   ,5)==0) { DoBeep   = 1; }
		if(memcmp(p,"-debug"  ,6)==0) { Debug    = 1; }
		if( memcmp(p,"-h"     ,2)==0 && strlen(p)==2 ) { show_help();  }


		if(memcmp(p,"-bps"    ,4)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
                new_bps = atoi(p);
                switch(new_bps)
                {
                    case 9600:
                            Baudrate=B9600;
                            break;
                    case 115200:
                            Baudrate=B115200;
                            break;
                    default:
                            printf("Incorrect baudrate value : %d\n\r",new_bps);
                            exit(0);;
                }
			}
		};



		if(memcmp(p,"-strcmd",7)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				sprintf(strcmd,"%s",p);
			}
		};


		if(memcmp(p,"-hexcmd",7)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				sprintf(hexcmd,"%s",p);
			}
		};



		if(memcmp(p,"-dev"    ,4)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				sprintf(barcode_path,"%s",p);
			}
		};


		if(memcmp(p,"-output"    ,7)==0)
		{
			if(argc > i+1 )
			{
				i++;
				p=argv[i];
				sprintf(output_path,"%s",p);
			}
		};
	}




    printf("Barcode device : %s\n\r",barcode_path);
	printf("Baudrate       : %d\n\r",Baudrate);
	printf("Output data    : %s\n\r",output_path);



    if(strlen(hexcmd))
    {
        if(CheckHexaFile() )
        {
    	    printf("hexcmd data    : %s\n\r",hexcmd);
        }
        else
        {
            exit(0);
        }
    }

    if(strlen(strcmd))
    {
    	printf("strcmd data    : %s\n\r",strcmd);
    }



    //=================================================================================================================
	fd = open (barcode_path, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname, strerror (errno));
	        return(-1);
	}
	set_interface_attribs (fd, Baudrate, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (fd, 0);                     // set no blocking

    flush(fd);

    //=================================================================================================================

   
    printf("READY\n\r");




    //=================================================================================================================
    // Start tasks
    //=================================================================================================================
    i=0;
    err = pthread_create(&(tid[i]), NULL, &Task0, NULL);
    if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));

    i=1;
    err = pthread_create(&(tid[i]), NULL, &Task1, NULL);
    if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));

    i=2;
    err = pthread_create(&(tid[i]), NULL, &Task2, NULL);
    if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));

    i=3;
    err = pthread_create(&(tid[i]), NULL, &Task3, NULL);
    if (err) printf("\ncan't create thread[%d] :[%s]", i,  strerror(err));


    //=================================================================================================================


    //=================================================================================================================
	while(1)
	{
        list_master_socket();
		usleep(100000);
	}
    //=================================================================================================================

	close(fd);
	return(0);
}
//=============================================================================================================================













//=============================================================================
// This task just count elapsed milliseconds since startup
//=============================================================================
void *Task0(void *arg)
{
	int Task_ID;
	static int InitDone=0;

    int min;
    int sec;
    int hours;
    int days;

	if(InitDone++==0)
   	{	
		Elapsed_ms  = 0;
        sec         = 0;
        min         = 0;
        hours       = 0;
        days        = 0;
 	}
	
	Task_ID = GetTaskID();
	if(Task_ID <0 )
    		return NULL;

	while(1)
   	{	
		Elapsed_ms ++ ;

        if(Elapsed_ms >= 10 )
        {
            Elapsed_ms=0;
            sec ++;
           
            if (sec >=60 )
            {
                sec=0;
                min++;

                if (min >=60 )
                {
                    min=0;
                    hours++;

                    if (hours >=24 )
                    {
                        days++;
                    }
                }
            }            
        }


        if( ShowAlive )
        {
            if(days)
        		printf("Barcode is alive: %03d-%02d:%02d:%02d\n\r", days, hours,min,sec );
            else
        		printf("Barcode is alive: %02d:%02d:%02d\n\r",hours,min,sec );
            ShowAlive=0;
        }

		usleep(100000);

 	}
   	return NULL;
}
//=============================================================================



//=============================================================================
// This task reads barcode outputs and store it
//=============================================================================
void *Task1(void *arg)
{
	int Task_ID;
	static int InitDone=0;

    int n;
	unsigned char c;
 

	if(InitDone++==0)
   	{	
        Offset =0;
 	}
	
	Task_ID = GetTaskID();
	if(Task_ID <0 )
    		return NULL;

    while(1)
	{
		n = read (fd, &c, 1); 
		if(n)
		{
			if (Debug ) printf("rx:%02X (%c)\n\r",c,c);
			store(c);
			continue;
		}

		usleep(10000);

	}
   	return NULL;
}
//=============================================================================


//=============================================================================
// This task reads sockets for command to execute
//=============================================================================
void *Task2(void *arg)
{
	int Task_ID;
	static int InitDone=0;


	if(InitDone++==0)
   	{	
        memset(hexcmd,0x00,sizeof(hexcmd));
        memset(strcmd,0x00,sizeof(strcmd));
        memset(socket_cmd,0x00,sizeof(socket_cmd));
 	}
	
	Task_ID = GetTaskID();
	if(Task_ID <0 )
    		return NULL;

    while(1)
	{
        if(strlen(socket_cmd) ==0 ) 
        {
            usleep(10000);
            continue;
        }

        if(strlen(hexcmd))
        {
            to_barcode_hex(hexcmd, strlen(hexcmd));
            memset(hexcmd,0x00,sizeof(hexcmd));
            usleep(1000000);
        }

        if(strlen(strcmd))
        {
            to_barcode(strcmd, strlen(strcmd));
            memset(strcmd,0x00,sizeof(strcmd));
            usleep(1000000);
        }

        if( mystrcmp ( socket_cmd , (char *)"ShowAlive")==0 )
        {
            ShowAlive=1;
            memset(socket_cmd,0x00,sizeof(socket_cmd));
            continue;
        }    

        if( mystrcmp ( socket_cmd , (char *)"beep")==0 )
        {
            memset(socket_cmd,0x00,sizeof(socket_cmd));
            system("beep");
            continue;
        }    
        to_barcode(socket_cmd, strlen(socket_cmd));
        memset(socket_cmd,0x00,sizeof(socket_cmd));
	
	}
   	return NULL;
}
//=============================================================================


#define BCR_IDLE   -1
#define BCR_LOW     20  // ON
#define BCR_NORMAL  100  // WINK
#define BCR_HIGH    200  //OFF




//=============================================================================
// This task monitor light sensor and set barcode ligths
//=============================================================================
void *Task3(void *arg)
{
	int Task_ID;
	static int InitDone=0;
    char aTmp[100];
    int cur_lux=0;

    static int state = BCR_IDLE;
    int New_state = BCR_IDLE;

    time_t t;
    struct tm tm;



	if(InitDone++==0)
   	{	
        cur_lux = -1;
        state   = 0;

        // Enable light sensor
    	WriteValue( (char * )"/sys/bus/i2c/devices/0-0044/power_state" , (char *)"1" );
 	}
	



	Task_ID = GetTaskID();
	if(Task_ID <0 )
    		return NULL;


    while(1)
	{

    	if( ReadDataOneLine((char *)"//sys/bus/i2c/devices/0-0044/lux",aTmp,100) <=0 )
        {
            cur_lux = -1;
        }
        else
        {
			cur_lux = atoi(aTmp) ;
        }


        while(1)
        {

            if( cur_lux == -1 ) break;

            if( cur_lux <= BCR_LOW  )
            {
                New_state = BCR_LOW ;
                break;
            }
            if( cur_lux <= BCR_NORMAL  )
            {
                New_state = BCR_NORMAL ;
                break;
            }
            New_state = BCR_HIGH ;
            break;
        }

        if( state != New_state )
        {

             t  = time(NULL);
             tm = *localtime(&t);    
             printf("%02d:%02d:%02d - lux: %d\n\r"
                        ,tm.tm_hour
                        , tm.tm_min
                        , tm.tm_sec
                        ,cur_lux);

            switch(New_state)
            {
                case  BCR_LOW:
                                sprintf(socket_cmd,"%s","nls0200010");
                                break;

                case  BCR_HIGH:
                                sprintf(socket_cmd,"%s","nls0200020");
                                break;

                case  BCR_NORMAL:
                default:
                                sprintf(socket_cmd,"%s","nls0200030");
                                break;
            }
            state = New_state;
        }       


		usleep(1000000);

	}
   	return NULL;
}
//=============================================================================
    
