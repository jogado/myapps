#include "common.h"  


//=====================================================================================================================
int hex(char c)
{
    if( c >= '0' && c <= '9' ) return( c - '0') ;
    if( c >= 'a' && c <= 'f' ) return( c - 'a' + 10) ;
    if( c >= 'A' && c <= 'F' ) return( c - 'A' + 10) ;
    printf("error: Bad Hexadecila character<%c>\n\r",c);
    return (-1);
}
//=====================================================================================================================
int mystrcmp(char *s1, char *s2)
{
    int i=0;

    for(i=0; i < 32 ; i++)
    {
        if( s1[i] != s2[i] ) return 1;
        if( s1[i] ==0      ) return 0;           
    }
    return 2;
}
//=====================================================================================================================







//=====================================================================================================================
int ReadDataOneLine( char *ifile , char *data , int MaxLen)
{
	FILE *fp;

 	fp = fopen(ifile,"r");

  	if(fp == NULL) 
	{
	    	perror("Error opening file");
      		return(-1);
   	}

	if( fgets (data, MaxLen, fp)!=NULL )
	{
		fclose(fp);
		return(strlen(data));
	}

	fclose(fp);
	return(0);
}
//=====================================================================================================================

//=====================================================================================================================
int WriteValue( char *ifile , char *data )
{
	FILE *fp;
	int len;

	len = strlen(data);

 	fp = fopen(ifile,"w");

  	if(fp == NULL) 
	{
	    	perror("Error opening file");
      		return(-1);
   	}

	if( fwrite( data , 1,len,  fp ) != (unsigned int) len )
	{
		printf("Error writting  <%s> to [%s]",data,ifile);
		perror("Write error");
		fclose(fp);
		return(-1);
	}

	fclose(fp);
	return(0);
}
//=====================================================================================================================



