#ifndef _BUILD_COMMS_H
#define _BUILD_COMMS_H


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <ctype.h>

#include "common.h"
#include "comms.h"



//----------------------------------
// Function prototypes
//----------------------------------
extern int  set_interface_attribs   ( int fd, int speed, int parity);
extern void set_blocking            ( int fd, int should_block);
extern void flush                   ( int fd );


#endif
