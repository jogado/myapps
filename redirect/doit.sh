#!/bin/sh

FILE="redirect"


if echo $1 | grep -q "imx6s"; then
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/poky-emsyscon/2.4.1/environment-setup-cortexa9hf-neon-poky-linux-gnueabi

elif  echo $1 | grep -q "imx6ul"; then
	export ARCH=arm
	export CROSS_COMPILE=arm-poky-linux-gnueabi-
	. /opt/fsl-imx-x11/4.1.15-2.0.3/environment-setup-cortexa7hf-neon-poky-linux-gnueabi

elif  echo $1 | grep -q "imx8mm"; then
	export ARCH=arm64
	export CROSS_COMPILE=aarch64-linux-gnu-

	. /opt/poky-emsyscon/3.0.2/environment-setup-aarch64-poky-linux

	unset LDFLAGS
	unset CFLAGS

else
	echo "syntax doit [ imx6s | imx6ul ] < 192.168.0.xxx >"
	exit
fi

make clean
make


file $FILE
if echo $2 | grep -q "192.168"; then

	scp $FILE root@$2:/usr/bin/$FILE
fi

