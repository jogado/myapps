#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>




extern int  main 			( int argc, char **argv ) ;
extern void to_barcode			( char *s,int len);
extern void logfile			( char *s,int len);
extern int  set_interface_attribs 	( int fd, int speed, int parity);
extern void set_blocking 		( int fd, int should_block) ;
extern void store 			( unsigned char c ) ;
extern void ShowHelp			( void );


char portname_1[100];
char portname_2[100];



char output_file[256];

int  fd_1 ;
int  fd_2 ;

int  Baudrate = 115200 ;
int  Debug    = 0;
int  DoBeep   = 0;

char aSeparator[10];



int GOT_A_COMMAND	= 0 ;
int GOT_A_DEV  		= 0 ;
int GOT_A_BAUDRATE	= 0 ;
int GOT_A_SEPARATOR	= 0 ;
int GOT_A_OUTPUT_FILE   = 0 ;


int OK_DETECTED		= 0 ;
int ERROR_DETECTED	= 0 ;




int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf ("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
//        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout
        tty.c_cc[VTIME] = 2;            // 0.2 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf ("error %d setting term attributes", errno);
}


void to_barcode(char *s,int len)
{
//	write (fd, s , len);           // send 7 character greeting
//	usleep (100000);
}



void logfile(char *s,int len)
{
	FILE *ftp;

	if ( GOT_A_OUTPUT_FILE == 0 ) return;

 	ftp = fopen( output_file , "w" );
	if (ftp == NULL)
	{	
        	printf ("error %d opening %s: %s", errno, output_file, strerror (errno));
	        return;
	}
	fwrite(s , 1 , len , ftp );
	fwrite("\n\r" , 1 , 2 , ftp );
	fclose(ftp);
}




#define MAX_LEN 1000
char buf [MAX_LEN+10];
int  Offset = 0 ;



void store (unsigned char c)
{

	if( (c == 0x0a || c == 0x0d)  )
	{
		if(Offset)
		{	

			if(GOT_A_SEPARATOR)
			{
	     			printf("%s%s",buf,aSeparator);
			}
			else
			{
	     			printf("%s\n\r",buf);
			}
		
	     		logfile(buf,Offset);

			if(memcmp(buf,"OK",2) == 0)
			{
				OK_DETECTED = 1 ;
			}

			if(memcmp(buf,"ERROR",5) == 0)
			{
				ERROR_DETECTED = 1 ;
			}
	    	 	buf[0]=0;
	     		Offset=0;
		}
     		return;
	}

	if( Offset < MAX_LEN )
	{
		buf[Offset]=c;
		Offset++;
		buf[Offset]=0;
	}
}







void ShowHelp(void)
{
	printf("atcom utility  v2020.01.16\n\r\n\r");
	printf("    Default Baudrate = 9600 bps\n\r");
	printf("    usage atcom <options> <...>\n\r");
	printf("    Options:\n\r");
	printf("        -c     : at command   ex: ati           \n\r");
	printf("        -d     : device       ex: /dev/ttyUSB2  \n\r");
	printf("        -b     : Baudrate     ex: 115200        \n\r");
	printf("        -s     : Separator    ex: ':'           \n\r");
	printf("        -o     : Output file  ex: ./atcom.log   \n\r");

	exit(1);
}
//==========================================================================================================



//==========================================================================================================
int main ( int argc, char **argv ) 
{
	int n;
	unsigned char c;
	int i;
	char *p;
	char aCmd[100];
	int to=1000000; // 2000000 uSec = 2sec
	int t=0; // 2000000 uSec = 2sec
	


	sprintf(portname_1,"%s","/dev/gps");
	sprintf(portname_2,"%s","/dev/rs232");


	//================================================
	fd_1 = open (portname_1, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd_1 < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname_1, strerror (errno));
	        return(-1);
	}
	set_interface_attribs (fd_1, 115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (fd_1, 0);                    // set no blocking
	//================================================
	
	//================================================
	fd_2 = open (portname_2, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd_2 < 0)
	{	
        	printf ("error %d opening %s: %s", errno, portname_2, strerror (errno));
			close(fd_1);
	        return(-1);
	}
	set_interface_attribs (fd_2, 115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
	set_blocking (fd_2, 0);                    // set no blocking
	//================================================


	while(1)
	{
		n = read (fd_1, &c, 1); 
		if(n)
		{
			//printf("%c",c);
			write (fd_2, &c , 1);  
			continue;
		}


		n = read (fd_2, &c, 1); 
		if(n)
		{
			//printf("%c",c);
			write (fd_1, &c , 1);  
			continue;
		}

		usleep(1000);
		continue;
	}





	/*
	//================================================
	// Flush Buffer
	//================================================
	while(1)
	{
		n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
		if(n)
		{
			continue;
		}	
		break;
	}

	write (fd, aCmd , strlen(aCmd));  
	write (fd, "\r" , 1);  

	while(1)
	{
		n = read (fd, &c, 1); 
		if(n)
		{
			t=0;

			store(c);
			if( OK_DETECTED) 
			{
				printf("\r\n");
				break;
			}
			if( ERROR_DETECTED) 
			{
				printf("\r\n");
				break;
			}
			continue;
			
		}

		if( t > to ) 
		{
			printf("TIMEOUT\n\r");
			break;
		}

		usleep(100000);
		t+=100000;
	}
		
	*/

	close(fd_1);
	close(fd_2);
	return(0);
}


















