#ifndef	__MAIN_H__
#define	__MAIN_H__



#define MAX_INDEX_TABLE 300

typedef struct entry_table {
	char valid;
	int  start;
	int  separator;
 	int  end;
}ENTRY_TABLE;





typedef enum {
   STATE_IDLE = 1    ,
   HUNTING_START     ,
   HUNTING_SEPARATOR ,
   HUNTING_END       ,
} HUNT_STATES;






//=============================================================================================================================
extern int  main 			( int argc, char **argv ) ;
extern void show_help		( void ) ;
extern int  FileSize		( char *s);
extern int  filecopy		( char *d, char *s);
extern void dump			( unsigned char *gBuf , int size);
extern int  add_key			( const char *aKey, const char* aVal);

extern int  datafile_to_mem	( char *s );
extern int 	mem_to_datafile	( char *s );

extern int 	Show_index		( void );
extern void Update_index	( void );
extern int 	FileExist		( char *s );
extern void Show_all_keyval	( void );
extern void Check_datafile	( void );


extern int  get_keyval 		( char *akey );
extern int 	delete_keyval 	( char *akey );
extern void show_info		( void );

extern char Compare			( char *p1 , const char *p2 );
extern int 	format_all		( void );

extern int eeprom_to_local	( void );
extern int local_to_eeprom	( void );
//=============================================================================================================================



#endif



