#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> 
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <termios.h>
#include <ctype.h>

#include "version.h"

//=============================================================================================================================
char completeVersion[50];

char *Build_dateTime(void)
{
	char aTmp[100];
	int Month = 0;
 	int Year  = 0;
 	int Day   = 0;

	sprintf(aTmp,"%s",__DATE__);

	if(memcmp(aTmp,"Jan",3)==0) Month=1;
	if(memcmp(aTmp,"F"  ,1)==0) Month=2;
	if(memcmp(aTmp,"Mar",3)==0) Month=3;
	if(memcmp(aTmp,"Ap" ,2)==0) Month=4;
	if(memcmp(aTmp,"May",3)==0) Month=5;
	if(memcmp(aTmp,"Jun",3)==0) Month=6;
	if(memcmp(aTmp,"Jul",3)==0) Month=7;
	if(memcmp(aTmp,"Au" ,2)==0) Month=8;
	if(memcmp(aTmp,"S"  ,1)==0) Month=9;
	if(memcmp(aTmp,"O"  ,1)==0) Month=10;
	if(memcmp(aTmp,"N"  ,1)==0) Month=11;
	if(memcmp(aTmp,"D"  ,1)==0) Month=13;
	

	aTmp[6]=0;
	Day  = atoi(&aTmp[4]);
	Year = atoi(&aTmp[7]);

	sprintf(completeVersion,"%02d/%02d/%04d %s"
		,Day
		,Month
		,Year
		,__TIME__);

	return(completeVersion);
}



