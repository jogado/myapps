#!/bin/bash

BASE_PATH="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo "Base PATH   : $BASE_PATH"
IMAGE_NAME=`ls emsysconImage |grep -i -m 1 emsyscon-mv3000`
echo "Source name : $IMAGE_NAME"
TARGET_NAME=`ls emsysconImage |grep -i -m 1 emsyscon-mv3000  | sed 's/emsyscon/snt/g' | sed 's/.tar.bz2//g'`
echo "Target name : $TARGET_NAME"
TAR_NAME=`ls emsysconImage |grep -i -m 1 emsyscon-mv3000  | sed 's/.bz2//g'`
#echo "Tar    name : $TAR_NAME"



# emsyscon image and where to unpack
#ORIGINAL_IMAGE="$BASE_PATH/emsysconImage/emsyscon-mv3000-dev-esc-imx6s-sodimm-3.1.3-20221102145521.rootfs.tar"
ORIGINAL_IMAGE="$BASE_PATH/emsysconImage/$TAR_NAME"


# ipks to add in image
IPKS_TO_ADD=("kGate-firstrun-0.01.25.ipk" "kGate-0.01.25.ipk")

ROOT_FS="$BASE_PATH/rootfs"

do_unpackEmsysconImage()
{
    echo "cleanning rootfs folder ..."
    if [ -d $ROOT_FS ]; then
        rm -rf $ROOT_FS
    fi
    mkdir -p $ROOT_FS

    echo "unpacking emsyscon image ..."
    bzip2 -d "$ORIGINAL_IMAGE.bz2"
    tar -xf $ORIGINAL_IMAGE -C $ROOT_FS
    rm -rf $ORIGINAL_IMAGE
}



do_installIPKs()
{
    #opkgApp="/opt/poky-emsyscon/3.1.3/sysroots/x86_64-pokysdk-linux/usr/bin/opkg"
    opkgApp="/opt/poky-emsyscon/3.1.3/cortexa9t2hf/sysroots/x86_64-pokysdk-linux/usr/bin/opkg"
    #opkgConfFile="$BASE_PATH/emsyscon-opkg.conf"
    opkgConfFile="$BASE_PATH/sources/mv3000/emsyscon-opkg.conf"

    # prepare opkg conf file
    if [ -f $opkgConfFile ]; then
        rm -rf $opkgConfFile
    fi
    cp $opkgConfFile.template $opkgConfFile
    echo "option offline_root $ROOT_FS" >> $opkgConfFile
    
    for ipk in ${IPKS_TO_ADD[*]}; do
        echo "Installing $ipk"
        #$opkgApp -f $opkgConfFile --prefer-arch-to-version --nodeps install "$BASE_PATH/ipk/$ipk"
        $opkgApp -f $opkgConfFile --prefer-arch-to-version --nodeps install "$BASE_PATH/sources/mv3000/$ipk"
    done

    cp $BASE_PATH/sources/mv3000/kapsch-gate.sh  $ROOT_FS/etc/init.d
    chmod +x $ROOT_FS/etc/init.d/kapsch-gate.sh
   
    # create symlinks, if is the case
    rxcdxLink="S99kapsch-gate.sh"
    
    cd $ROOT_FS/etc/rc3.d/
    if [ ! -f $rxcdxLink ]; then
        echo "create gate symlink in rc3.d ..."
        ln -s ../init.d/kapsch-gate.sh S99kapsch-gate.sh
    fi
    
    cd $ROOT_FS/etc/rc5.d/
    if [ ! -f $rxcdxLink ]; then
        echo "create gate symlink in rc5.d ..."
        ln -s ../init.d/kapsch-gate.sh S99kapsch-gate.sh
    fi
    cd $BASE_PATH

    echo "update opkg status ..."
#   WRONG_OPKG="$ROOT_FS/opt/poky-emsyscon/3.1.3/sysroots/x86_64-pokysdk-linux/var/lib/opkg"
    WRONG_OPKG="$ROOT_FS/opt/poky-emsyscon/3.1.3/sysroots/x86_64-pokysdk-linux/var/lib/opkg"
    OPKG_INFO="$ROOT_FS/var/lib/opkg"
    cp -rf $WRONG_OPKG/info/* $OPKG_INFO/info
    cat $WRONG_OPKG/status >> $OPKG_INFO/status
    #rm -rf $ROOT_FS/opt/abt-poky-emsyscon
    rm -rf $ROOT_FS/opt/poky-emsyscon
    
    rm -f $opkgConfFile
}

do_packSntImage()
{
    #newImage="$BASE_PATH/snt-mv3000-dev-esc-imx6s-sodimm-3.1.3-`date +%Y%m%d%H%M%S`.rootfs"
    newImage="$BASE_PATH/$TARGET_NAME"

    echo "packing new image ..."
    tar -cf $newImage.tar -C $ROOT_FS .
    bzip2 -f -k $newImage.tar
    rm -rf $newImage.tar
}

do_clean()
{
    echo "cleanning..."
    rm -rf $ROOT_FS
}

# main entry
do_unpackEmsysconImage
do_installIPKs
do_packSntImage
do_clean
