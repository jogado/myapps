#!/bin/sh

#for armBuild branch
DAEMON_PATH=/usr/local/kGate

#global
NAME=kapsch-gate.sh
DISPLAY=:0.0

DAEMON=$DAEMON_PATH/kGate
PIDFILE=/var/run/kGate.pid

# for install at startup
applicationLock=/tmp/kGate.lock
COMMIT_SCRIPT=$DAEMON_PATH/module/scripts/commit_version.sh

# flags
OTI_UPGRADE_FLAG=/tmp/oti_upgrade.done
OTICI_INFO_FLAG=/tmp/otici_info.done
INSTALL_FLAG=$DAEMON_PATH/module/installfile
UPGRADE_RUNNING_FLAG=/tmp/upgrading.lock

FOLDER_UD=/apps/kGate/ud
FOLDER_EMV1=/apps/kGate/emv/Config
FOLDER_EMV=/apps/kGate/emv/Transactions
FOLDER_LOGS=/apps/kGate/logs
FOLDER_MODULE=/apps/kGate/module

FOLDER_CRONIE=$DAEMON_PATH/crontab

DOM_TRANSFERED=/apps/dom-transfered

do_print_info()
{
    echo "$(</proc/uptime awk '{print $1}') : aptransGate => $1" >> /tmp/startup.log  
}

do_start_X()
{
    #start X
    /usr/bin/Xorg :0 -br -pn -noreset > /dev/null 2>&1 &
    sleep 1
    export DISPLAY
}

do_wait_for_oti()
{
    # test if all oti scripts are ready.
    do_print_info "wait for oti scripts"
    while [ ! -e "$OTI_UPGRADE_FLAG" ] && [ ! -e "$OTICI_INFO_FLAG" ]; do
	sleep 1
    done
    do_print_info "oti scripts ended."

}
do_start_app()
{
    rm -f ${PIDFILE}
    start-stop-daemon -bm -S -x "$DAEMON" -p "$PIDFILE"
}

do_start()
{
    do_print_info "start X"
    /etc/init.d/alignment.sh
    do_start_X
    do_print_info "start application"
    # for debug purpose do not start application
    do_start_app
    # for debug purpose do not validate cron script: if "applicationLock" is created, 
    #then cron will try to start the app if is not found in running tasks
    touch "$applicationLock"
    do_print_info "done"
}

do_stop()
{
    rm -f "$applicationLock" 2> /dev/null
    start-stop-daemon -K -p "$PIDFILE"
    sleep 1
    rm -f ${PIDFILE}
}

do_checkInstall()
{
    [ -f $UPGRADE_RUNNING_FLAG ] && exit 0
    rm -f "$applicationLock" 2> /dev/null
    if [ -f "$INSTALL_FLAG" ]; then
        if [ -f "$COMMIT_SCRIPT" ]; then
            $COMMIT_SCRIPT
        fi
    fi
}

do_checkFiles()
{
    if [ -f "$DOM_TRANSFERED" ]; then
        do_print_info "already checked. returning ..."
        return
    fi
    do_print_info "Start file check."

    #check and create folder structure.
    if [ ! -d $FOLDER_EMV1 ]; then
        do_print_info "$FOLDER_EMV1 does not exist. Creating..."
        mkdir -p $FOLDER_EMV1
    fi
    if [ ! -d $FOLDER_EMV ]; then
        do_print_info "$FOLDER_EMV does not exist. Creating..."
        mkdir -p $FOLDER_EMV
    fi
    if [ ! -d $FOLDER_MODULE ]; then
        do_print_info "$FOLDER_MODULE does not exist. Creating..."
        mkdir -p $FOLDER_MODULE
    fi
    if [ ! -d $FOLDER_UD ]; then
        do_print_info "$FOLDER_UD does not exist. Creating..."
        mkdir -p $FOLDER_UD
    fi
    if [ ! -d $FOLDER_LOGS ]; then
        do_print_info "$FOLDER_LOGS does not exist. Creating..."
        mkdir -p $FOLDER_LOGS
    fi
    # create links to existing config files; first remove existing link.
    do_print_info "create EOD link"
    rm -rf $DAEMON_PATH/logs
    ln -s $FOLDER_LOGS $DAEMON_PATH/logs

    do_print_info "create UD link"
    rm -rf $DAEMON_PATH/ud
    ln -s $FOLDER_UD $DAEMON_PATH/ud

    do_print_info "create MODULE link"
    rm -rf $DAEMON_PATH/module
    ln -s $FOLDER_MODULE $DAEMON_PATH/module

    do_print_info "create EMV link"
    rm -rf $DAEMON_PATH/emv/Transactions
    ln -s $FOLDER_EMV $DAEMON_PATH/emv/Transactions

    # mark the transfer
    touch $DOM_TRANSFERED
    sync
}

do_checkCronie()
{
    if [ ! -d "$AFOLDER_CRONIE" ]; then
      do_print_info "cronie already initialized. returning ..."
      return
    fi
    
    if [ ! -f /usr/bin/crontab ]; then
      do_print_info "cronie is not installed. returning ..."
      return
    fi
    
    do_print_info "install cron script"
    /usr/bin/crontab $FOLDER_CRONIE/cron_tab.txt
    
    do_print_info "remove $FOLDER_CRONIE folder"
    rm -rf $FOLDER_CRONIE
}

case "$1" in
    start)
        #do_wait_for_oti
        do_checkCronie
        do_checkInstall
        do_checkFiles
        do_start
        ;;

    stop)
        do_stop
        ;;

    restart|force-reload)
        do_stop
        do_start
        ;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|force-reload}" >&2
	exit 1
	;;
esac

exit 0
