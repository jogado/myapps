# Copyright 2018-2020 NXP
#
# This software is owned or controlled by NXP and may only be used
# strictly in accordance with the applicable license terms.  By expressly
# accepting such terms or by downloading, installing, activating and/or
# otherwise using the software, you are agreeing that you have read, and
# that you agree to comply with and are bound by, such license terms.  If
# you do not agree to be bound by the applicable license terms, then you
# may not retain, install, activate or otherwise use the software.
#
# #############################################################
# This file is generated using a script
# #############################################################
#

# The Secure Element Applet
#
# You can compile host library for different Applets listed below.
# Please note, some of these Applets may be for NXP Internal use only.
SSS_HAVE_APPLET_NONE := 0
SSS_HAVE_APPLET_A71CH := 0
SSS_HAVE_APPLET_A71CL := 0
SSS_HAVE_APPLET_A71CH_SIM := 0
SSS_HAVE_APPLET_SE05X_A := 0
SSS_HAVE_APPLET_SE05X_B := 0
SSS_HAVE_APPLET_SE05X_C := 1
SSS_HAVE_APPLET_SE05X_L := 0
SSS_HAVE_APPLET_LOOPBACK := 0

# SE50 Applet version.
#
# 03_XX would only enable features of version 03.XX version of applet.
# But, this would be compatibility would be added for newer versions of the Applet.
# When 04_XX is selected, it would expose features available in 04_XX at compile time.
SSS_HAVE_SE05X_VER_03_XX := 1

# Host where the software stack is running
#
# e.g. Windows, PC Linux, Embedded Linux, Kinetis like embedded platform
SSS_HAVE_HOST_DARWIN := 0
SSS_HAVE_HOST_PCLINUX32 := 0
SSS_HAVE_HOST_PCLINUX64 := 0
SSS_HAVE_HOST_PCWINDOWS := 0
SSS_HAVE_HOST_CYGWIN := 0
SSS_HAVE_HOST_FRDMK64F := 0
SSS_HAVE_HOST_EVKMIMXRT1060 := 0
SSS_HAVE_HOST_LPCXPRESSO55S := 0
SSS_HAVE_HOST_LPCXPRESSO55S_NS := 0
SSS_HAVE_HOST_LPCXPRESSO55S_S := 0
SSS_HAVE_HOST_IMXLINUX := 1
SSS_HAVE_HOST_RASPBIAN := 0
SSS_HAVE_HOST_ANDROID := 0
SSS_HAVE_HOST_WIN10IOT := 0

# Communication Interface
#
# How the host library communicates to the Secure Element.
# This may be directly over an I2C interface on embedded platform.
# Or sometimes over Remote protocol like JRCP_V1 / JRCP_V2 / VCOM from PC.
SSS_HAVE_SMCOM_NONE := 0
SSS_HAVE_SMCOM_JRCP_V2 := 0
SSS_HAVE_SMCOM_JRCP_V1 := 0
SSS_HAVE_SMCOM_VCOM := 0
SSS_HAVE_SMCOM_SCI2C := 0
SSS_HAVE_SMCOM_T1OI2C := 1
SSS_HAVE_SMCOM_T1OI2C_GP1_0 := 0
SSS_HAVE_SMCOM_RC663_VCOM := 0
SSS_HAVE_SMCOM_PN7150 := 0
SSS_HAVE_SMCOM_THREAD := 0
SSS_HAVE_SMCOM_PCSC := 0

# Counterpart Crypto on Host
#
# What is being used as a cryptographic library on the host.
# As of now only OpenSSL / mbedTLS is supported
SSS_HAVE_HOSTCRYPTO_MBEDTLS := 0
SSS_HAVE_HOSTCRYPTO_MBEDCRYPTO := 0
SSS_HAVE_HOSTCRYPTO_OPENSSL := 1
SSS_HAVE_HOSTCRYPTO_USER := 0
SSS_HAVE_HOSTCRYPTO_NONE := 0

# Choice of Operating system
#
# Default would mean nothing special.
# i.e. Without any RTOS on embedded system, or default APIs on PC/Linux
SSS_HAVE_RTOS_DEFAULT := 1
SSS_HAVE_RTOS_FREERTOS := 0

# ALT Engine implementation for mbedTLS
#
# When set to None, mbedTLS would not use ALT Implementation to connect to / use Secure Element.
# This needs to be set to SSS for Cloud Demos over SSS APIs
SSS_HAVE_MBEDTLS_ALT_SSS := 0
SSS_HAVE_MBEDTLS_ALT_A71CH := 0
SSS_HAVE_MBEDTLS_ALT_NONE := 1

# Secure Channel Protocol
#
# In case we enable secure channel to Secure Element, which interface to be used.
SSS_HAVE_SCP_NONE := 1
SSS_HAVE_SCP_SCP03_SSS := 0
SSS_HAVE_SCP_SCP03_HOSTCRYPTO := 0

# Enable or disable FIPS
#
# This selection mostly impacts tests, and generally not the actual Middleware
SSS_HAVE_FIPS_NONE := 1
SSS_HAVE_FIPS_SE050 := 0
SSS_HAVE_FIPS_140_2 := 0
SSS_HAVE_FIPS_140_3 := 0

# SE050 Authentication
#
# This settings is used by examples to connect using various options to authenticate to the Applet.
SSS_HAVE_SE05X_AUTH_NONE := 1
SSS_HAVE_SE05X_AUTH_USERID := 0
SSS_HAVE_SE05X_AUTH_PLATFSCP03 := 0
SSS_HAVE_SE05X_AUTH_AESKEY := 0
SSS_HAVE_SE05X_AUTH_ECKEY := 0
SSS_HAVE_SE05X_AUTH_USERID_PLATFSCP03 := 0
SSS_HAVE_SE05X_AUTH_AESKEY_PLATFSCP03 := 0
SSS_HAVE_SE05X_AUTH_ECKEY_PLATFSCP03 := 0

# A71CH Authentication
#
# This settings is used by SSS-API based examples to connect using either plain or authenticated to the A71CH.
SSS_HAVE_A71CH_AUTH_NONE := 1
SSS_HAVE_A71CH_AUTH_SCP03 := 0

# Logging
SSS_HAVE_LOG_DEFAULT := 1
SSS_HAVE_LOG_VERBOSE := 0
SSS_HAVE_LOG_SILENT := 0
SSS_HAVE_LOG_SEGGERRTT := 0

# See https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html
#
# For embedded builds, this choices sets optimization levels.
# For MSVC builds, build type is selected from IDE As well
SSS_HAVE_CMAKE_BUILD_TYPE_DEBUG := 1
SSS_HAVE_CMAKE_BUILD_TYPE_RELEASE := 0
SSS_HAVE_CMAKE_BUILD_TYPE_RELWITHDEBINFO := 0
SSS_HAVE_CMAKE_BUILD_TYPE_ := 0

# Compiling without any Applet Support
SSS_HAVE_APPLET_NONE := 0

# A71CH (ECC)
SSS_HAVE_APPLET_A71CH := 0

# A71CL (RSA)
SSS_HAVE_APPLET_A71CL := 0

# Similar to A71CH
SSS_HAVE_APPLET_A71CH_SIM := 0

# SE050 Type A (ECC)
SSS_HAVE_APPLET_SE05X_A := 0

# SE050 Type B (RSA)
SSS_HAVE_APPLET_SE05X_B := 0

# SE050 (Super set of A + B)
SSS_HAVE_APPLET_SE05X_C := 1

# SE050 (Similar to A71CL)
SSS_HAVE_APPLET_SE05X_L := 0

# NXP Internal testing Applet
SSS_HAVE_APPLET_LOOPBACK := 0

# SE050
SSS_HAVE_SE05X_VER_03_XX := 1

# OS X / Macintosh
SSS_HAVE_HOST_DARWIN := 0

# PC/Laptop Linux with 32bit libraries
SSS_HAVE_HOST_PCLINUX32 := 0

# PC/Laptop Linux with 64bit libraries
SSS_HAVE_HOST_PCLINUX64 := 0

# PC/Laptop Windows
SSS_HAVE_HOST_PCWINDOWS := 0

# Using Cygwin
SSS_HAVE_HOST_CYGWIN := 0

# Embedded Kinetis Freedom K64F
SSS_HAVE_HOST_FRDMK64F := 0

# Embedded Kinetis i.MX RT 1060
SSS_HAVE_HOST_EVKMIMXRT1060 := 0

# Embedded LPCXpresso55s (No demarcation of secure/non-secure world)
SSS_HAVE_HOST_LPCXPRESSO55S := 0

# Non Secure world of LPCXpresso55s
SSS_HAVE_HOST_LPCXPRESSO55S_NS := 0

# Secure world of LPCXpresso55s
SSS_HAVE_HOST_LPCXPRESSO55S_S := 0

# Embedded Linux on i.MX
SSS_HAVE_HOST_IMXLINUX := 1

# Embedded Linux on RaspBerry PI
SSS_HAVE_HOST_RASPBIAN := 0

# Android
SSS_HAVE_HOST_ANDROID := 0

# Windows 10 IoT Core
SSS_HAVE_HOST_WIN10IOT := 0

# Not using any Communication layer
SSS_HAVE_SMCOM_NONE := 0

# Socket Interface New Implementation
SSS_HAVE_SMCOM_JRCP_V2 := 0

# Socket Interface Old Implementation.
# This is the interface used from Host PC when when we run jrcpv1_server
# from the linux PC.
SSS_HAVE_SMCOM_JRCP_V1 := 0

# Virtual COM Port
SSS_HAVE_SMCOM_VCOM := 0

# Smart Card I2C for A71CH and A71CH
SSS_HAVE_SMCOM_SCI2C := 0

# T=1 over I2C for SE050
SSS_HAVE_SMCOM_T1OI2C := 1

# GP Spec
SSS_HAVE_SMCOM_T1OI2C_GP1_0 := 0

# Via RC663 Over VCOM Interface from Windows PC
SSS_HAVE_SMCOM_RC663_VCOM := 0

# NFC Interface using PN7150
SSS_HAVE_SMCOM_PN7150 := 0

# Thread Mode interface
SSS_HAVE_SMCOM_THREAD := 0

# CCID PC/SC reader interface
SSS_HAVE_SMCOM_PCSC := 0

# Use mbedTLS as host crypto
SSS_HAVE_HOSTCRYPTO_MBEDTLS := 0

# Use mbed-crypto as host crypto
# Required for ARM-PSA / TF-M
# NXP Internal

SSS_HAVE_HOSTCRYPTO_MBEDCRYPTO := 0

# Use OpenSSL as host crypto
SSS_HAVE_HOSTCRYPTO_OPENSSL := 1

# User Implementation of Host Crypto
# e.g. Files at ``sss/src/user/crypto`` have low level AES/CMAC primitives.
# The files at ``sss/src/user`` use those primitives.
# This becomes an example for users with their own AES Implementation
# This then becomes integration without mbedTLS/OpenSSL for SCP03 / AESKey.
#
# .. note:: ECKey abstraction is not implemented/available yet.
SSS_HAVE_HOSTCRYPTO_USER := 0

# NO Host Crypto
# Note, this is unsecure and only provided for experimentation
# on platforms that do not have an mbedTLS PORT
# Many :ref:`sssftr-control` have to be disabled to have a valid build.
SSS_HAVE_HOSTCRYPTO_NONE := 0

# No specific RTOS. Either bare matal on embedded system or native linux or Windows OS
SSS_HAVE_RTOS_DEFAULT := 1

# Free RTOS for embedded systems
SSS_HAVE_RTOS_FREERTOS := 0

# Use SSS Layer ALT implementation
SSS_HAVE_MBEDTLS_ALT_SSS := 0

# Legacy implementation
SSS_HAVE_MBEDTLS_ALT_A71CH := 0

# Not using any mbedTLS_ALT
#
# When this is selected, cloud demos can not work with mbedTLS
SSS_HAVE_MBEDTLS_ALT_NONE := 1
SSS_HAVE_SCP_NONE := 1

# Use SSS Layer for SCP.  Used for SE050 family.
SSS_HAVE_SCP_SCP03_SSS := 0

# Use Host Crypto Layer for SCP03. Legacy implementation. Used for older demos of A71CH Family.
SSS_HAVE_SCP_SCP03_HOSTCRYPTO := 0

# NO FIPS
SSS_HAVE_FIPS_NONE := 1

# SE050 IC FIPS
SSS_HAVE_FIPS_SE050 := 0

# FIPS 140-2
SSS_HAVE_FIPS_140_2 := 0

# FIPS 140-3
SSS_HAVE_FIPS_140_3 := 0

# Use the default session (i.e. session less) login
SSS_HAVE_SE05X_AUTH_NONE := 1

# Do User Authentication with UserID
SSS_HAVE_SE05X_AUTH_USERID := 0

# Use Platform SCP for connection to SE
SSS_HAVE_SE05X_AUTH_PLATFSCP03 := 0

# Do User Authentication with AES Key
# Earlier this was called AppletSCP03
SSS_HAVE_SE05X_AUTH_AESKEY := 0

# Do User Authentication with EC Key
# Earlier this was called FastSCP
SSS_HAVE_SE05X_AUTH_ECKEY := 0

# UserID and PlatfSCP03
SSS_HAVE_SE05X_AUTH_USERID_PLATFSCP03 := 0

# AESKey and PlatfSCP03
SSS_HAVE_SE05X_AUTH_AESKEY_PLATFSCP03 := 0

# ECKey and PlatfSCP03
SSS_HAVE_SE05X_AUTH_ECKEY_PLATFSCP03 := 0

# Plain communication, not authenticated or encrypted
SSS_HAVE_A71CH_AUTH_NONE := 1

# SCP03 enabled
SSS_HAVE_A71CH_AUTH_SCP03 := 0

# Default Logging
SSS_HAVE_LOG_DEFAULT := 1

# Very Verbose logging
SSS_HAVE_LOG_VERBOSE := 0

# Totally silent logging
SSS_HAVE_LOG_SILENT := 0

# Segger Real Time Transfer (For Test Automation, NXP Internal)
SSS_HAVE_LOG_SEGGERRTT := 0

# For developer
SSS_HAVE_CMAKE_BUILD_TYPE_DEBUG := 1

# Optimization enabled and debug symbols removed
SSS_HAVE_CMAKE_BUILD_TYPE_RELEASE := 0

# Optimization enabled but with debug symbols
SSS_HAVE_CMAKE_BUILD_TYPE_RELWITHDEBINFO := 0

# Empty Allowed
# SE05X Secure Element : Symmetric AES
SSSFTR_SE05X_AES := ON
# SE05X Secure Element : Elliptic Curve Cryptography
SSSFTR_SE05X_ECC := ON
# SE05X Secure Element : RSA
SSSFTR_SE05X_RSA := ON
# SE05X Secure Element : KEY operations : SET Key
SSSFTR_SE05X_KEY_SET := ON
# SE05X Secure Element : KEY operations : GET Key
SSSFTR_SE05X_KEY_GET := ON
# SE05X Secure Element : Authenticate via ECKey
SSSFTR_SE05X_AuthECKey := ON
# SE05X Secure Element : Allow creation of user/authenticated session.
#
# If the intended deployment only uses Platform SCP
# Or it is a pure session less integration, this can
# save some code size.
SSSFTR_SE05X_AuthSession := ON
# SE05X Secure Element : Allow creation/deletion of Crypto Objects
#
# If disabled, new Crytpo Objects are neither created and
# old/existing Crypto Objects are not deleted.
# It is assumed that during provisioning phase, the required
# Crypto Objects are pre-created or they are never going to
# be needed.
SSSFTR_SE05X_CREATE_DELETE_CRYPTOOBJ := ON
# Software : Symmetric AES
SSSFTR_SW_AES := ON
# Software : Elliptic Curve Cryptography
SSSFTR_SW_ECC := ON
# Software : RSA
SSSFTR_SW_RSA := ON
# Software : KEY operations : SET Key
SSSFTR_SW_KEY_SET := ON
# Software : KEY operations : GET Key
SSSFTR_SW_KEY_GET := ON
# Software : Used as a test counterpart
#
# e.g. Major part of the mebdTLS SSS layer is purely used for
# testing of Secure Element implementation, and can be avoided
# fully during many production scenarios.
SSSFTR_SW_TESTCOUNTERPART := ON



# Deprecated items. Used here for backwards compatibility.

WithApplet_SE05X := ON
WithApplet_SE050_A := 0
WithApplet_SE050_B := 0
WithApplet_SE050_C := 1
SSS_HAVE_SE050_A := 0
SSS_HAVE_SE050_B := 0
SSS_HAVE_SE050_C := 1
SSS_HAVE_SE05X := ON
SSS_HAVE_SE := ON
SSS_HAVE_LOOPBACK := 0
SSS_HAVE_ALT := OFF
WithApplet_None := 0
SSS_HAVE_None := 0
WithApplet_A71CH := 0
SSS_HAVE_A71CH := 0
WithApplet_A71CL := 0
SSS_HAVE_A71CL := 0
WithApplet_A71CH_SIM := 0
SSS_HAVE_A71CH_SIM := 0
WithApplet_SE05X_A := 0
SSS_HAVE_SE05X_A := 0
WithApplet_SE05X_B := 0
SSS_HAVE_SE05X_B := 0
WithApplet_SE05X_C := 1
SSS_HAVE_SE05X_C := 1
WithApplet_SE05X_L := 0
SSS_HAVE_SE05X_L := 0
WithApplet_LoopBack := 0
SSS_HAVE_LoopBack := 0
SSS_HAVE_MBEDTLS := 0
SSS_HAVE_MBEDCRYPTO := 0
SSS_HAVE_OPENSSL := 1
SSS_HAVE_USER := 0
SSS_HAVE_NONE := 0
SSS_HAVE_ALT_SSS := 0
SSS_HAVE_ALT_A71CH := 0
SSS_HAVE_ALT_NONE := 1
SSS_HAVE_SE05X_Auth_None := 1
SSS_HAVE_SE05X_Auth_UserID := 0
SSS_HAVE_SE05X_Auth_PlatfSCP03 := 0
SSS_HAVE_SE05X_Auth_AESKey := 0
SSS_HAVE_SE05X_Auth_ECKey := 0
SSS_HAVE_SE05X_Auth_UserID_PlatfSCP03 := 0
SSS_HAVE_SE05X_Auth_AESKey_PlatfSCP03 := 0
SSS_HAVE_SE05X_Auth_ECKey_PlatfSCP03 := 0
