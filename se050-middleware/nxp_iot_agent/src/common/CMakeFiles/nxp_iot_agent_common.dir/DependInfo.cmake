# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Agent.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Agent.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Apdu.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Apdu.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Datastore.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Datastore.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Dispatcher.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Dispatcher.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Hostcmd.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Hostcmd.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/RPC.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/RPC.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/ServiceDescriptor.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/ServiceDescriptor.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/Types.pb.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/Types.pb.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/pb_common.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/pb_common.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/pb_decode.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/pb_decode.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/protobuf/pb_encode.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/__/protobuf/pb_encode.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/nxp_iot_agent_dispatcher.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/nxp_iot_agent_dispatcher.c.o"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/nxp_iot_agent_utils_protobuf.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/nxp_iot_agent/src/common/CMakeFiles/nxp_iot_agent_common.dir/nxp_iot_agent_utils_protobuf.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "FTR_FILE_SYSTEM"
  "PB_FIELD_32BIT"
  "PLATFORM_IMX"
  "SSS_USE_FTR_FILE"
  "T1oI2C"
  "mqttconfigENABLE_METRICS=0"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/se050_middleware/simw-top/sss/port/default"
  "."
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/../../inc"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/../../platform"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/../../src/protobuf"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/../../../semslite/lib/inc"
  "/home/ubuntu/se050_middleware/simw-top/nxp_iot_agent/src/common/port/default"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/se05x/../se05x_03_xx_xx"
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/inc"
  "/home/ubuntu/se050_middleware/simw-top/sss/inc"
  "/home/ubuntu/se050_middleware/simw-top/sss/port/x86"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../platform/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/api/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/scp"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/a71ch/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/platform/ksdk"
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/se05x/CMakeFiles/se05x.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/CMakeFiles/SSS_APIs.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/ex/src/CMakeFiles/ex_common.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/CMakeFiles/a7x_utils.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
