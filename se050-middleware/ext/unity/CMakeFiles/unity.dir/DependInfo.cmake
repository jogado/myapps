# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/se050_middleware/simw-top/ext/unity/unity.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/ext/unity/CMakeFiles/unity.dir/unity.c.o"
  "/home/ubuntu/se050_middleware/simw-top/ext/unity/unity_fixture.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/ext/unity/CMakeFiles/unity.dir/unity_fixture.c.o"
  "/home/ubuntu/se050_middleware/simw-top/ext/unity/unity_fixture_addin.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/ext/unity/CMakeFiles/unity.dir/unity_fixture_addin.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "FTR_FILE_SYSTEM"
  "PLATFORM_IMX"
  "SSS_USE_FTR_FILE"
  "T1oI2C"
  "UNITY_INCLUDE_CONFIG_H"
  "UNITY_REPEAT_TEST_NAME"
  "mqttconfigENABLE_METRICS=0"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/se050_middleware/simw-top/sss/port/default"
  "."
  "/home/ubuntu/se050_middleware/simw-top/ext/unity/."
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../platform/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
