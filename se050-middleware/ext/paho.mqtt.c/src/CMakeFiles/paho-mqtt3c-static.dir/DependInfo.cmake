# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/se050_middleware/simw-top/ext/paho.mqtt.c/src/MQTTClient.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/ext/paho.mqtt.c/src/CMakeFiles/paho-mqtt3c-static.dir/MQTTClient.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "FTR_FILE_SYSTEM"
  "PAHO_MQTT_STATIC=1"
  "SSS_USE_FTR_FILE"
  "T1oI2C"
  "_GNU_SOURCE"
  "mqttconfigENABLE_METRICS=0"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/se050_middleware/simw-top/sss/port/default"
  "."
  "/home/ubuntu/se050_middleware/simw-top/ext/paho.mqtt.c/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
