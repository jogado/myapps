# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/platform/generic/sm_timer.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/__/platform/generic/sm_timer.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/platform/imx/se05x_reset.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/__/platform/imx/se05x_reset.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/platform/linux/i2c_a7.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/__/platform/linux/i2c_a7.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/tstUtil/tst_sm_time.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/__/tstUtil/tst_sm_time.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra/nxLog.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/infra/nxLog.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra/sm_apdu.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/infra/sm_apdu.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra/sm_errors.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/infra/sm_errors.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra/sm_printf.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/infra/sm_printf.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C/phNxpEsePal_i2c.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/smCom/T1oI2C/phNxpEsePal_i2c.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C/phNxpEseProto7816_3.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/smCom/T1oI2C/phNxpEseProto7816_3.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C/phNxpEse_Api.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/smCom/T1oI2C/phNxpEse_Api.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/smCom.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/smCom/smCom.c.o"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/smComT1oI2C.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/smCom/smComT1oI2C.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "FTR_FILE_SYSTEM"
  "PLATFORM_IMX"
  "SSS_USE_FTR_FILE"
  "T1oI2C"
  "T1oI2C_UM11225"
  "mqttconfigENABLE_METRICS=0"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/se050_middleware/simw-top/sss/port/default"
  "."
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../platform/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/scp"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
