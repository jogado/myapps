# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/attest_ecc/ex_sss_ecc_attest.c" "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/ex/attest_ecc/CMakeFiles/ex_attest_ecc.dir/ex_sss_ecc_attest.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "FTR_FILE_SYSTEM"
  "PLATFORM_IMX"
  "SSS_USE_FTR_FILE"
  "T1oI2C"
  "mqttconfigENABLE_METRICS=0"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/ubuntu/se050_middleware/simw-top/sss/port/default"
  "."
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc"
  "/home/ubuntu/se050_middleware/simw-top/sss/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/se05x/../se05x_03_xx_xx"
  "/home/ubuntu/se050_middleware/simw-top/sss/ex/inc"
  "/home/ubuntu/se050_middleware/simw-top/sss/port/x86"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/infra"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../platform/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/../tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/smCom/T1oI2C"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/tstUtil"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/api/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/libCommon/scp"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/a71ch/inc"
  "/home/ubuntu/se050_middleware/simw-top/hostlib/hostLib/platform/ksdk"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/ex/src/CMakeFiles/ex_common.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/CMakeFiles/SSS_APIs.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/se05x/CMakeFiles/se05x.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/CMakeFiles/a7x_utils.dir/DependInfo.cmake"
  "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/hostlib/hostLib/libCommon/CMakeFiles/smCom.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
