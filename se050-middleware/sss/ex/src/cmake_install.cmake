# Install script for directory: /home/ubuntu/se050_middleware/simw-top/sss/ex/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "TRUE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/sss/ex/src/libex_common.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/se05x" TYPE FILE FILES
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_boot.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_ports.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_main_inc.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_auth.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_objid.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_ports.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_scp03_keys.h"
    "/home/ubuntu/se050_middleware/simw-top/sss/ex/src/../inc/ex_sss_tp_scp03_keys.h"
    )
endif()

