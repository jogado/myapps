# Install script for directory: /home/ubuntu/se050_middleware/simw-top/demos/se05x

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "TRUE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_Minimal/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_export/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_import/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_Personalization/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_Delete_and_test_provision/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_MandatePlatformSCP/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_TransportLock/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_TransportUnLock/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/ex_se05x_WiFiKDF/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_GetInfo/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_PCR/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_I2cMaster/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_policy/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_ReadWithAttestation/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_TimeStamp/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/se05x_InjectCertificate/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/certificate_demo/cmake_install.cmake")
  include("/home/ubuntu/se050_middleware/simw-top_build/imx_cc_se050_t1oi2c/demos/se05x/seTool/cmake_install.cmake")

endif()

