# Copyright 2018-2020 NXP
#
# This software is owned or controlled by NXP and may only be used
# strictly in accordance with the applicable license terms.  By expressly
# accepting such terms or by downloading, installing, activating and/or
# otherwise using the software, you are agreeing that you have read, and
# that you agree to comply with and are bound by, such license terms.  If
# you do not agree to be bound by the applicable license terms, then you
# may not retain, install, activate or otherwise use the software.
#
# #############################################################
# This file is generated using a script
# #############################################################
#

# The Secure Element Applet
#
# You can compile host library for different Applets listed below.
# Please note, some of these Applets may be for NXP Internal use only.

# Compiling without any Applet Support
SET(SSS_HAVE_APPLET_NONE 0)

# A71CH (ECC)
SET(SSS_HAVE_APPLET_A71CH 0)

# A71CL (RSA)
SET(SSS_HAVE_APPLET_A71CL 0)

# Similar to A71CH
SET(SSS_HAVE_APPLET_A71CH_SIM 0)

# SE050 Type A (ECC)
SET(SSS_HAVE_APPLET_SE05X_A 0)

# SE050 Type B (RSA)
SET(SSS_HAVE_APPLET_SE05X_B 0)

# SE050 (Super set of A + B)
SET(SSS_HAVE_APPLET_SE05X_C 1)

# SE050 (Similar to A71CL)
SET(SSS_HAVE_APPLET_SE05X_L 0)

# NXP Internal testing Applet
SET(SSS_HAVE_APPLET_LOOPBACK 0)

# SE50 Applet version.
#
# 03_XX would only enable features of version 03.XX version of applet.
# But, this would be compatibility would be added for newer versions of the Applet.
# When 04_XX is selected, it would expose features available in 04_XX at compile time.

# SE050
SET(SSS_HAVE_SE05X_VER_03_XX 1)

# Host where the software stack is running
#
# e.g. Windows, PC Linux, Embedded Linux, Kinetis like embedded platform

# OS X / Macintosh
SET(SSS_HAVE_HOST_DARWIN 0)

# PC/Laptop Linux with 32bit libraries
SET(SSS_HAVE_HOST_PCLINUX32 0)

# PC/Laptop Linux with 64bit libraries
SET(SSS_HAVE_HOST_PCLINUX64 0)

# PC/Laptop Windows
SET(SSS_HAVE_HOST_PCWINDOWS 0)

# Using Cygwin
SET(SSS_HAVE_HOST_CYGWIN 0)

# Embedded Kinetis Freedom K64F
SET(SSS_HAVE_HOST_FRDMK64F 0)

# Embedded Kinetis i.MX RT 1060
SET(SSS_HAVE_HOST_EVKMIMXRT1060 0)

# Embedded LPCXpresso55s (No demarcation of secure/non-secure world)
SET(SSS_HAVE_HOST_LPCXPRESSO55S 0)

# Non Secure world of LPCXpresso55s
SET(SSS_HAVE_HOST_LPCXPRESSO55S_NS 0)

# Secure world of LPCXpresso55s
SET(SSS_HAVE_HOST_LPCXPRESSO55S_S 0)

# Embedded Linux on i.MX
SET(SSS_HAVE_HOST_IMXLINUX 1)

# Embedded Linux on RaspBerry PI
SET(SSS_HAVE_HOST_RASPBIAN 0)

# Android
SET(SSS_HAVE_HOST_ANDROID 0)

# Windows 10 IoT Core
SET(SSS_HAVE_HOST_WIN10IOT 0)

# Communication Interface
#
# How the host library communicates to the Secure Element.
# This may be directly over an I2C interface on embedded platform.
# Or sometimes over Remote protocol like JRCP_V1 / JRCP_V2 / VCOM from PC.

# Not using any Communication layer
SET(SSS_HAVE_SMCOM_NONE 0)

# Socket Interface New Implementation
SET(SSS_HAVE_SMCOM_JRCP_V2 0)

# Socket Interface Old Implementation.
# This is the interface used from Host PC when when we run jrcpv1_server
# from the linux PC.
SET(SSS_HAVE_SMCOM_JRCP_V1 0)

# Virtual COM Port
SET(SSS_HAVE_SMCOM_VCOM 0)

# Smart Card I2C for A71CH and A71CH
SET(SSS_HAVE_SMCOM_SCI2C 0)

# T=1 over I2C for SE050
SET(SSS_HAVE_SMCOM_T1OI2C 1)

# GP Spec
SET(SSS_HAVE_SMCOM_T1OI2C_GP1_0 0)

# Via RC663 Over VCOM Interface from Windows PC
SET(SSS_HAVE_SMCOM_RC663_VCOM 0)

# NFC Interface using PN7150
SET(SSS_HAVE_SMCOM_PN7150 0)

# Thread Mode interface
SET(SSS_HAVE_SMCOM_THREAD 0)

# CCID PC/SC reader interface
SET(SSS_HAVE_SMCOM_PCSC 0)

# Counterpart Crypto on Host
#
# What is being used as a cryptographic library on the host.
# As of now only OpenSSL / mbedTLS is supported

# Use mbedTLS as host crypto
SET(SSS_HAVE_HOSTCRYPTO_MBEDTLS 0)

# Use mbed-crypto as host crypto
# Required for ARM-PSA / TF-M
# NXP Internal

SET(SSS_HAVE_HOSTCRYPTO_MBEDCRYPTO 0)

# Use OpenSSL as host crypto
SET(SSS_HAVE_HOSTCRYPTO_OPENSSL 1)

# User Implementation of Host Crypto
# e.g. Files at ``sss/src/user/crypto`` have low level AES/CMAC primitives.
# The files at ``sss/src/user`` use those primitives.
# This becomes an example for users with their own AES Implementation
# This then becomes integration without mbedTLS/OpenSSL for SCP03 / AESKey.
#
# .. note:: ECKey abstraction is not implemented/available yet.
SET(SSS_HAVE_HOSTCRYPTO_USER 0)

# NO Host Crypto
# Note, this is unsecure and only provided for experimentation
# on platforms that do not have an mbedTLS PORT
# Many :ref:`sssftr-control` have to be disabled to have a valid build.
SET(SSS_HAVE_HOSTCRYPTO_NONE 0)

# Choice of Operating system
#
# Default would mean nothing special.
# i.e. Without any RTOS on embedded system, or default APIs on PC/Linux

# No specific RTOS. Either bare matal on embedded system or native linux or Windows OS
SET(SSS_HAVE_RTOS_DEFAULT 1)

# Free RTOS for embedded systems
SET(SSS_HAVE_RTOS_FREERTOS 0)

# ALT Engine implementation for mbedTLS
#
# When set to None, mbedTLS would not use ALT Implementation to connect to / use Secure Element.
# This needs to be set to SSS for Cloud Demos over SSS APIs

# Use SSS Layer ALT implementation
SET(SSS_HAVE_MBEDTLS_ALT_SSS 0)

# Legacy implementation
SET(SSS_HAVE_MBEDTLS_ALT_A71CH 0)

# Not using any mbedTLS_ALT
#
# When this is selected, cloud demos can not work with mbedTLS
SET(SSS_HAVE_MBEDTLS_ALT_NONE 1)

# Secure Channel Protocol
#
# In case we enable secure channel to Secure Element, which interface to be used.
SET(SSS_HAVE_SCP_NONE 1)

# Use SSS Layer for SCP.  Used for SE050 family.
SET(SSS_HAVE_SCP_SCP03_SSS 0)

# Use Host Crypto Layer for SCP03. Legacy implementation. Used for older demos of A71CH Family.
SET(SSS_HAVE_SCP_SCP03_HOSTCRYPTO 0)

# Enable or disable FIPS
#
# This selection mostly impacts tests, and generally not the actual Middleware

# NO FIPS
SET(SSS_HAVE_FIPS_NONE 1)

# SE050 IC FIPS
SET(SSS_HAVE_FIPS_SE050 0)

# FIPS 140-2
SET(SSS_HAVE_FIPS_140_2 0)

# FIPS 140-3
SET(SSS_HAVE_FIPS_140_3 0)

# SE050 Authentication
#
# This settings is used by examples to connect using various options to authenticate to the Applet.

# Use the default session (i.e. session less) login
SET(SSS_HAVE_SE05X_AUTH_NONE 1)

# Do User Authentication with UserID
SET(SSS_HAVE_SE05X_AUTH_USERID 0)

# Use Platform SCP for connection to SE
SET(SSS_HAVE_SE05X_AUTH_PLATFSCP03 0)

# Do User Authentication with AES Key
# Earlier this was called AppletSCP03
SET(SSS_HAVE_SE05X_AUTH_AESKEY 0)

# Do User Authentication with EC Key
# Earlier this was called FastSCP
SET(SSS_HAVE_SE05X_AUTH_ECKEY 0)

# UserID and PlatfSCP03
SET(SSS_HAVE_SE05X_AUTH_USERID_PLATFSCP03 0)

# AESKey and PlatfSCP03
SET(SSS_HAVE_SE05X_AUTH_AESKEY_PLATFSCP03 0)

# ECKey and PlatfSCP03
SET(SSS_HAVE_SE05X_AUTH_ECKEY_PLATFSCP03 0)

# A71CH Authentication
#
# This settings is used by SSS-API based examples to connect using either plain or authenticated to the A71CH.

# Plain communication, not authenticated or encrypted
SET(SSS_HAVE_A71CH_AUTH_NONE 1)

# SCP03 enabled
SET(SSS_HAVE_A71CH_AUTH_SCP03 0)

# Logging

# Default Logging
SET(SSS_HAVE_LOG_DEFAULT 1)

# Very Verbose logging
SET(SSS_HAVE_LOG_VERBOSE 0)

# Totally silent logging
SET(SSS_HAVE_LOG_SILENT 0)

# Segger Real Time Transfer (For Test Automation, NXP Internal)
SET(SSS_HAVE_LOG_SEGGERRTT 0)

# See https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html
#
# For embedded builds, this choices sets optimization levels.
# For MSVC builds, build type is selected from IDE As well

# For developer
SET(SSS_HAVE_CMAKE_BUILD_TYPE_DEBUG 1)

# Optimization enabled and debug symbols removed
SET(SSS_HAVE_CMAKE_BUILD_TYPE_RELEASE 0)

# Optimization enabled but with debug symbols
SET(SSS_HAVE_CMAKE_BUILD_TYPE_RELWITHDEBINFO 0)

# Empty Allowed
# SE05X Secure Element : Symmetric AES
SET(SSSFTR_SE05X_AES ON)
# SE05X Secure Element : Elliptic Curve Cryptography
SET(SSSFTR_SE05X_ECC ON)
# SE05X Secure Element : RSA
SET(SSSFTR_SE05X_RSA ON)
# SE05X Secure Element : KEY operations : SET Key
SET(SSSFTR_SE05X_KEY_SET ON)
# SE05X Secure Element : KEY operations : GET Key
SET(SSSFTR_SE05X_KEY_GET ON)
# SE05X Secure Element : Authenticate via ECKey
SET(SSSFTR_SE05X_AuthECKey ON)
# SE05X Secure Element : Allow creation of user/authenticated session.
#
# If the intended deployment only uses Platform SCP
# Or it is a pure session less integration, this can
# save some code size.
SET(SSSFTR_SE05X_AuthSession ON)
# SE05X Secure Element : Allow creation/deletion of Crypto Objects
#
# If disabled, new Crytpo Objects are neither created and
# old/existing Crypto Objects are not deleted.
# It is assumed that during provisioning phase, the required
# Crypto Objects are pre-created or they are never going to
# be needed.
SET(SSSFTR_SE05X_CREATE_DELETE_CRYPTOOBJ ON)
# Software : Symmetric AES
SET(SSSFTR_SW_AES ON)
# Software : Elliptic Curve Cryptography
SET(SSSFTR_SW_ECC ON)
# Software : RSA
SET(SSSFTR_SW_RSA ON)
# Software : KEY operations : SET Key
SET(SSSFTR_SW_KEY_SET ON)
# Software : KEY operations : GET Key
SET(SSSFTR_SW_KEY_GET ON)
# Software : Used as a test counterpart
#
# e.g. Major part of the mebdTLS SSS layer is purely used for
# testing of Secure Element implementation, and can be avoided
# fully during many production scenarios.
SET(SSSFTR_SW_TESTCOUNTERPART ON)


#
# Version checks GTE - Greater Than Or Equal To
IF(SSS_HAVE_APPLET_SE05X_IOT)
    IF(SSS_HAVE_SE05X_VER_03_XX)
         SET(SSS_HAVE_SE05X_VER_GTE_03_XX ON)
    ENDIF()

ELSE() #SSS_HAVE_APPLET_SE05X_IOT
     SET(SSS_HAVE_SE05X_VER_GTE_03_XX OFF)
ENDIF() #SSS_HAVE_APPLET_SE05X_IOT

# Deprecated items. Used here for backwards compatibility.

SET(WithApplet_SE05X SSS_HAVE_APPLET_SE05X_IOT)
SET(WithApplet_SE050_A SSS_HAVE_APPLET_SE05X_A)
SET(WithApplet_SE050_B SSS_HAVE_APPLET_SE05X_B)
SET(WithApplet_SE050_C SSS_HAVE_APPLET_SE05X_C)
SET(SSS_HAVE_SE050_A SSS_HAVE_APPLET_SE05X_A)
SET(SSS_HAVE_SE050_B SSS_HAVE_APPLET_SE05X_B)
SET(SSS_HAVE_SE050_C SSS_HAVE_APPLET_SE05X_C)
SET(SSS_HAVE_SE05X SSS_HAVE_APPLET_SE05X_IOT)
SET(SSS_HAVE_SE SSS_HAVE_APPLET)
SET(SSS_HAVE_LOOPBACK SSS_HAVE_APPLET_LOOPBACK)
SET(SSS_HAVE_ALT SSS_HAVE_MBEDTLS_ALT)
SET(WithApplet_None SSS_HAVE_APPLET_NONE)
SET(SSS_HAVE_None SSS_HAVE_APPLET_NONE)
SET(WithApplet_A71CH SSS_HAVE_APPLET_A71CH)
SET(SSS_HAVE_A71CH SSS_HAVE_APPLET_A71CH)
SET(WithApplet_A71CL SSS_HAVE_APPLET_A71CL)
SET(SSS_HAVE_A71CL SSS_HAVE_APPLET_A71CL)
SET(WithApplet_A71CH_SIM SSS_HAVE_APPLET_A71CH_SIM)
SET(SSS_HAVE_A71CH_SIM SSS_HAVE_APPLET_A71CH_SIM)
SET(WithApplet_SE05X_A SSS_HAVE_APPLET_SE05X_A)
SET(SSS_HAVE_SE05X_A SSS_HAVE_APPLET_SE05X_A)
SET(WithApplet_SE05X_B SSS_HAVE_APPLET_SE05X_B)
SET(SSS_HAVE_SE05X_B SSS_HAVE_APPLET_SE05X_B)
SET(WithApplet_SE05X_C SSS_HAVE_APPLET_SE05X_C)
SET(SSS_HAVE_SE05X_C SSS_HAVE_APPLET_SE05X_C)
SET(WithApplet_SE05X_L SSS_HAVE_APPLET_SE05X_L)
SET(SSS_HAVE_SE05X_L SSS_HAVE_APPLET_SE05X_L)
SET(WithApplet_LoopBack SSS_HAVE_APPLET_LOOPBACK)
SET(SSS_HAVE_LoopBack SSS_HAVE_APPLET_LOOPBACK)
SET(SSS_HAVE_MBEDTLS SSS_HAVE_HOSTCRYPTO_MBEDTLS)
SET(SSS_HAVE_MBEDCRYPTO SSS_HAVE_HOSTCRYPTO_MBEDCRYPTO)
SET(SSS_HAVE_OPENSSL SSS_HAVE_HOSTCRYPTO_OPENSSL)
SET(SSS_HAVE_USER SSS_HAVE_HOSTCRYPTO_USER)
SET(SSS_HAVE_NONE SSS_HAVE_HOSTCRYPTO_NONE)
SET(SSS_HAVE_ALT_SSS SSS_HAVE_MBEDTLS_ALT_SSS)
SET(SSS_HAVE_ALT_A71CH SSS_HAVE_MBEDTLS_ALT_A71CH)
SET(SSS_HAVE_ALT_NONE SSS_HAVE_MBEDTLS_ALT_NONE)
SET(SSS_HAVE_SE05X_Auth_None SSS_HAVE_SE05X_AUTH_NONE)
SET(SSS_HAVE_SE05X_Auth_UserID SSS_HAVE_SE05X_AUTH_USERID)
SET(SSS_HAVE_SE05X_Auth_PlatfSCP03 SSS_HAVE_SE05X_AUTH_PLATFSCP03)
SET(SSS_HAVE_SE05X_Auth_AESKey SSS_HAVE_SE05X_AUTH_AESKEY)
SET(SSS_HAVE_SE05X_Auth_ECKey SSS_HAVE_SE05X_AUTH_ECKEY)
SET(SSS_HAVE_SE05X_Auth_UserID_PlatfSCP03 SSS_HAVE_SE05X_AUTH_USERID_PLATFSCP03)
SET(SSS_HAVE_SE05X_Auth_AESKey_PlatfSCP03 SSS_HAVE_SE05X_AUTH_AESKEY_PLATFSCP03)
SET(SSS_HAVE_SE05X_Auth_ECKey_PlatfSCP03 SSS_HAVE_SE05X_AUTH_ECKEY_PLATFSCP03)
