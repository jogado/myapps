/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_settings.c
 *
 *  Created on: Oct 24, 2008
 *      Author: jvolckae
 */


#include "oti_settings.h"


static OtiTag tag[] = {
		OTI_RF_COLLISION_MSG_ENABLED,
		OTI_TRACKS_FOLLOWED_BY_AID,
		OTI_PUPI,
		OTI_UID,
		OTI_ATQB,
		OTI_ATS,
};



/**
 * This method will set/return the PCD settings of the OTI reader.
 *
 * @param reader	pointer to the reader control block.
 * @param code		operational code that needs to be performed (GET - SET - DO).
 * @param mask		mask of the settings that are to be changed or requested.
 * @param settings	mask of the settings that are enabled/disabled or need to be enabled/disabled.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_pcd_settings(POtiReaderCb reader, OpCode code, POtiPcdSettings settings)
{
	int result = 0;

	/*-- sanity check --*/
	if ( settings == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		int idx = 0;
		int bitmask = 0;

		char data[1] = { 0 };

		for ( idx=0, bitmask=0x1 ; bitmask <= settings->mask ; idx++, bitmask = (1 << idx) )
		{
			/*-- continue when the setting doesn't need to be changed --*/
			if ( (bitmask & settings->mask) == 0 ) continue;

			/*-- Get the current applications that are enabled --*/
			if ( code == GET )
			{
				result = oti_handle_tag( reader, code, tag[idx], NULL, 0, data, sizeof( data ), OtiTlvSizeAdd );

				/*-- on success copy result in appli buffer --*/
				if ( result > 0 )
				{
					settings->values |= (data[0] << idx);
				}
			}
			/*-- DO or SET the applications --*/
			else
			{
				data[0] = (settings->values >> idx) & 0x01;
				result = oti_handle_tag( reader, code, tag[idx], data, sizeof( data ), NULL, 0, OtiTlvSizeAdd );
			}
		}
	}

	return check_oti_handle_tag_answer(result);
}
