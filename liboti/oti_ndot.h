/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_ndot.h
 *
 *  Created on: Jan 12, 2011
 *      Author: wmeganck
 */

#ifndef _PRODATA_SYSTEMS_OTI_NDOT_H_
#define _PRODATA_SYSTEMS_OTI_NDOT_H_ 1

#include <oti.h>

#include "oti_reader.h"
#include "oti_command.h"
#include "oti_poll.h"

/*-- Function declarations --*/
int oti_ndot_poll( POtiReaderCb reader, NDoTOperatorId* opId, size_t txLen, POtiCardInfo cardinfo );
int oti_ndot_emv( POtiReaderCb reader, char* txData, size_t txLen, POtiCardInfo cardinfo );

#endif /* _PRODATA_SYSTEMS_OTI_NDOT_H_ */
 
