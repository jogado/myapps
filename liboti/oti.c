/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti.c
 *
 *  Created on: Oct 13, 2008
 *      Author: jvolckae
 *  Modification history.
 *        Date		Author	Description
 *        April 24, 2015 JB	Add DO version of _RF_PARAM_MAX_BIT_RATE, _SAK, _PUPI, _ATS, _ATQB, _UID
 *                              Add Set/Get of main timeouts
 */

#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <oti.h>

#include "oti_reader.h"
#include "oti_command.h"

#include "oti_technical.h"
#include "oti_poll.h"
#include "oti_ndot.h"
#include "oti_emv.h"
#include "oti_rf.h"
#include "oti_settings.h"
#include "oti_trace.h"
#include "oti_usb.h"

OtiFWCompatibility fwCompatibility = OtiFWCompatibilityNone;

/**
 * This method will initialise the OTI reader and prepare it for use.
 *
 * @return	The function will return a handle that must be used to open a reader.
 * 			When an error occurred the error will be NULL, check errno for more details.
 */
void* oti_init( void )
{
    // no effect since verbosity @ startup is set to OtiVerbosityNone :)
    dprintf("Starting OTI.\n");
    return ( (void *)oti_reader_init( ) );
}


/**
 * Deinitialise the reader.
 * This will close all readers that are opened with the initialisation handle.
 * After all readers are closed, the pointer will be freed.
 *
 * @param handle	Handle returned by oti_init.
 *
 * @return 	If the function fails -1 is returned, check errno for a more detailed error code.
 * 			If the function succeeds 0 is returned.
 */
int oti_deinit( void* handle )
{
        dprintf("Closing OTI.\n");
	return ( oti_reader_deinit( (POtiCb)handle ) );
}


/**
 * Open an OTI-Reader connection.
 * All settings to open the reader are stored in the OtiOpen structure.
 *
 * @param handle	Handle returned by oti_init.
 * @param open		reference to the OtiOpen object.
 *
 * @return 	The function will return a handle that must be used to read/write data
 * 			from/to the OTI reader.
 * 			If the function fails NULL is returned, check errno for a more detailed error code.
 */
void* oti_open( void* handle, POtiOpen open )
{
	int retries=0, reset_retries = 0, result = -1;
    unsigned char buf[ 128 ];

	/*-- sanity check --*/
	if ( open == NULL )
	{
		errno = EINVAL;
	}
	else
	{
		POtiReaderCb reader = NULL;
		OtiReaderOpen readerOpen;

		memcpy( readerOpen.path, open->path, sizeof( readerOpen.path ) );
		readerOpen.baudrate = open->baudrate;
		readerOpen.interfaceType = open->interfaceType;
		readerOpen.mode = OtiReaderModeDefault;
		if((open->mode >= OtiReaderModeDefault) && (open->mode <= OtiReaderModeCallback))
			readerOpen.mode = open->mode;

        do {
            do {
                if(reader==NULL) {
                    if( ( reader = oti_reader_open( (POtiCb)handle, &readerOpen ) ) != NULL )
                        reader->timeout.response = 1000;
                }
                if( reader != NULL ){
                    if( ( result = oti_get_version( reader, OTI_VERSION_FIRMWARE, ( char * ) buf, sizeof( buf ) ) ) >= 0 ){
                        reader->timeout.response = OTI_READER_RESPONSE_TIMEOUT;
                        return( ( void * )reader );
                    }
                }
                usleep(OTI_OPEN_INTERVAL*1000);
            } while (retries++ < OTI_OPEN_RETRIES);
            if( reader != NULL ){
                oti_close( reader );
                reader = NULL;
            }
            if( open->interfaceType == OtiReaderInterfaceTypeSerial ) {
                oti_usb_reset( open->path );
                usleep(OTI_OPEN_RESET_WAIT*1000);
            } else {
                break;
            }
        } while (reset_retries++ < OTI_OPEN_RESET_RETRIES);
	}
	return ( NULL );
}


/**
 * Close the reader.
 *
 * @param handle	handle to the reader that must be closed, returned by oti_open.
 *
 * @return If the functions fails, -1 is returned, check errno for more detailed error code.
 */
int oti_close( void* handle )
{
	return ( oti_reader_close( (POtiReaderCb)handle ) );
}


/**
 * Execute an IO control function.
 * This function can be used to control the communication channel, the PCD or the PICC.
 *
 * @param handle	handle to the reader, returned by oti_open.
 * @param command	command that must be executed.
 * @param tx		pointer to data used by the command.
 * @param txLen		length of the tx buffer.
 * @param rx		pointer to data used to store the reply of the command.
 * @param rxLen		length of the rx buffer.
 */
int oti_ioctl( void* handle, OtiIoctl command, const void* tx, size_t txLen, void* rx, size_t rxLen )
{
    dprintf("OtiIoctl command: %d (%08X) => ", (int)command, (unsigned)command);
	int result = -1;

	/*-- sanity check --*/
	if ( handle == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		POtiReaderCb reader = (POtiReaderCb)handle;

		switch ( command )
		{
			/* VBO add-on :    */
			case OTI_IOCTL_DO_APPLICATIONS:
			{
                            dprintf("OTI_IOCTL_DO_APPLICATIONS\n");
                            OtiKernelID kernel = OTI_KERNEL_ID_NOKERNEL;
                            OtiAppli_t* txData = (OtiAppli_t*)tx;
                            if (txLen == 5)
                            {
                                kernel = (OtiKernelID)(((char*)tx)[0]);
                                txData = (OtiAppli_t*)(&((char*)tx)[1]);
                            }
                            result = oti_enable_application(reader, DO, txData, kernel);
                            break;
			}

			case OTI_IOCTL_SET_APPLICATIONS:
			{
                            dprintf("OTI_IOCTL_SET_APPLICATIONS\n");
                            OtiKernelID kernel = OTI_KERNEL_ID_NOKERNEL;
                            OtiAppli_t* txData = (OtiAppli_t*)tx;
                            if (txLen == 5)
                            {
                                kernel = (OtiKernelID)(((char*)tx)[0]);
                                txData = (OtiAppli_t*)(&((char*)tx)[1]);
                            }
                            result = oti_enable_application(reader, SET, txData, kernel);
                            break;
			}

			case OTI_IOCTL_GET_APPLICATIONS:
			{
                            dprintf("OTI_IOCTL_GET_APPLICATIONS\n");
                            OtiKernelID kernel = OTI_KERNEL_ID_NOKERNEL;
                            if (tx != NULL)
                            {
                                kernel = *(OtiKernelID*)tx;
                            }
                            result = oti_enable_application(reader, GET, (OtiAppli_t*)rx, kernel);
                            break;
			}


			case OTI_IOCTL_MIFARE_COMMAND :
			{
                                dprintf("OTI_IOCTL_MIFARE_COMMAND\n");
				result = oti_mifare_command( reader, tx, txLen, rx, rxLen );
				break;
			}
			case OTI_IOCTL_MIFARE_KEY_REGION :
			{
                                dprintf("OTI_IOCTL_MIFARE_KEY_REGION\n");
				result = oti_mifare_key_region( reader, *(OtiKeyStoreRegion*)tx );
				break;
			}
			/* End of VBO add-on.    */

			case OTI_IOCTL_GET_VERSION:
			{
                dprintf("OTI_IOCTL_GET_VERSION\n");
				result = oti_get_version( reader, *(OtiVersion*)tx, (char*)rx, rxLen );
				sprintf(&rx[strlen(rx)]," Build:2021.09.22");

				break;
			}

			case OTI_IOCTL_RESTORE_FACTORY_SETTINGS:
			{
                                dprintf("OTI_IOCTL_RESTORE_FACTORY_SETTINGS\n");
				result = oti_restore_factory_setttings( reader );
				break;
			}

			case OTI_IOCTL_LOAD_DEFAULT_PARAMETERS:
			{
                                dprintf("OTI_IOCTL_LOAD_DEFAULT_PARAMETERS\n");
				result = oti_load_default_parameters( reader );
				break;
			}

			case OTI_IOCTL_POLL_START:
			{
                                dprintf("OTI_IOCTL_POLL_START\n");
				OtiPollMode pollMode ;
				memcpy( &pollMode, tx, sizeof(OtiPollMode ) );
				reader->pollMode = pollMode;
				if( (reader->pollMode >= OtiPollPendingOnceExt) && (reader->pollMode <= OtiPollImmediateExt) ) {
					pollMode = OtiPollImmediate;
				}
				result = oti_poll_start( reader, DO, &pollMode );
				break;
			}

			case OTI_IOCTL_POLL_STOP:
			{
                                dprintf("OTI_IOCTL_POLL_STOP\n");
				result = oti_poll_stop( reader );
				break;
			}

			case OTI_IOCTL_WAIT_FOR_CARD:
			{
                                dprintf("OTI_IOCTL_WAIT_FOR_CARD\n");

				if( (reader->pollMode >= OtiPollPendingOnceExt) && (reader->pollMode <= OtiPollImmediateExt) )
					result = oti_wait_for_card_extended( reader, (POtiCardInfo)rx, *(int*)tx );
				else
					result = oti_wait_for_card( reader, (POtiCardInfo)rx, *(int*)tx );
				break;
			}

			case OTI_IOCTL_REMOVE_PICC:
			{
                                dprintf("OTI_IOCTL_REMOVE_PICC\n");
				if( (reader->pollMode >= OtiPollPendingOnceExt) && (reader->pollMode <= OtiPollImmediateExt) && (reader->piccType == 'T') )
					result = oti_remove_picc_extended( reader );
				else
					result = oti_remove_picc( reader );
				break;
			}

			case OTI_IOCTL_WAIT_FOR_CARD_REMOVAL:
			{
                                dprintf("OTI_IOCTL_WAIT_FOR_CARD_REMOVAL\n");
				if( (reader->pollMode >= OtiPollPendingOnceExt) && (reader->pollMode <= OtiPollImmediateExt) && (reader->piccType == 'T') )
					result = oti_wait_for_card_removal_extended( reader, *(int*)tx );
				else
					result = oti_wait_for_card_removal( reader, *(int*)tx );
				break;
			}

			case OTI_IOCTL_GET_RF_ANTENNA:
			{
                                dprintf("OTI_IOCTL_GET_RF_ANTENNA\n");
				result = oti_rf_antenna( reader, GET, (OtiRfAntenna*)rx );
				break;
			}

			case OTI_IOCTL_SET_RF_ANTENNA:
			{
                                dprintf("OTI_IOCTL_SET_RF_ANTENNA\n");
				result = oti_rf_antenna( reader, SET, (OtiRfAntenna*)tx );
				break;
			}

			case OTI_IOCTL_DO_RF_ANTENNA:
			{
                                dprintf("OTI_IOCTL_DO_RF_ANTENNA\n");
				result = oti_rf_antenna( reader, DO, (OtiRfAntenna*)tx );
				break;
			}

			case OTI_IOCTL_DO_PCD_SETTINGS:
			{
                                dprintf("OTI_IOCTL_DO_PCD_SETTINGS\n");
				result = oti_pcd_settings( reader, DO, (POtiPcdSettings)tx );
				break;
			}

			case OTI_IOCTL_GET_PCD_SETTINGS:
			{
                                dprintf("OTI_IOCTL_GET_PCD_SETTINGS\n");
				OtiPcdSettings settings;

				settings.mask = ((POtiPcdSettings)tx)->mask;
				settings.values = 0;
				result = oti_pcd_settings( reader, GET, &settings );

				if ( result >= 0 )
				{
					((POtiPcdSettings)rx)->mask = settings.mask;
					((POtiPcdSettings)rx)->values = settings.values;
				}
				break;
			}

			case OTI_IOCTL_SET_PCD_SETTINGS:
			{
                                dprintf("OTI_IOCTL_SET_PCD_SETTINGS\n");
				result = oti_pcd_settings( reader, SET, (POtiPcdSettings)tx );
				break;
			}

			case OTI_IOCTL_RESET_RF:
			{
                                dprintf("OTI_IOCTL_RESET_RF\n");
				result = oti_rf_reset( reader, *(int*)tx );
				break;
			}

			case OTI_IOCTL_TRANSPARENT_DATA:
			{
                                dprintf("OTI_IOCTL_TRANSPARENT_DATA\n");
				result = oti_transparent_command( reader, tx, txLen, rx, rxLen );
				break;
			}

			case OTI_IOCTL_SR_COMMAND:
			{
                                dprintf("OTI_IOCTL_SR_COMMAND\n");
				result = oti_sr_command( reader, tx, txLen, rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_EXT_POLL_CONFIG:
			{
                                dprintf("OTI_IOCTL_GET_EXT_POLL_CONFIG\n");
				if( (rxLen < sizeof(ExtendedPollConfig)) || (rx == NULL) ) {
					errno = EINVAL;
					result = -1;
				} else {
					memcpy(rx,&reader->pollConfig,result=sizeof(ExtendedPollConfig));
				}
				break;
			}

			case OTI_IOCTL_SET_EXT_POLL_CONFIG:
			{
                                dprintf("OTI_IOCTL_SET_EXT_POLL_CONFIG\n");
				if( (txLen != sizeof(ExtendedPollConfig)) || (tx == NULL) ) {
					errno = EINVAL;
					result = -1;
				} else {
					memcpy(&reader->pollConfig,tx,sizeof(ExtendedPollConfig)-2);	/* avoid risk to change internal settings */
					result = 0;
				}
				break;
			}

			case OTI_IOCTL_RF_MODE:
			{
                                dprintf("OTI_IOCTL_RF_MODE\n");
				result = oti_rf_antenna_mode(reader, *(OtiRfAntennaMode*)tx);
				break;
			}

			case OTI_IOCTL_TRANSPARENT_LAYER_CONTROL:
			{
                                dprintf("OTI_IOCTL_TRANSPARENT_LAYER_CONTROL\n");
				reader->pollConfig.tlc_appli = reader->pollConfig.tlc = *(OtiTransparentLayer *)tx;	/* keep track of application settings for extended poll */
				result =  oti_transparent_layer_control( reader, *(OtiTransparentLayer*)tx );
				break;
			}

			case OTI_IOCTL_TRANSPARENT_CHANNEL:
			{
                                dprintf("OTI_IOCTL_TRANSPARENT_CHANNEL\n");
				result = oti_transparent_channel_select( reader, *(OtiTransparentChannel*)tx);
				break;
			}

			case OTI_IOCTL_TRANSPARENT_FRAME_SIZE:
			{
                                dprintf("OTI_IOCTL_TRANSPARENT_FRAME_SIZE\n");
				result =  oti_transparent_frame_size( reader ,*(OtiTransparentFrameSize*)tx );
				break;
			}

			case OTI_IOCTL_GET_POLL_DELAY:
			{
                                dprintf("OTI_IOCTL_GET_POLL_DELAY\n");
				result = oti_get_getdelay( reader, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_BOOT_LOADER:
			{
                                dprintf("OTI_IOCTL_BOOT_LOADER\n");
				result = oti_call_boot_loader( reader, (char*)rx, rxLen );
				break;
			}
			
			case OTI_IOCTL_FLASH_PACKET:
			{
                                dprintf("OTI_IOCTL_FLASH_PACKET\n");
				result = oti_flash_packet( reader, (char*)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_RUN_APPLICATION:
			{
                                dprintf("OTI_IOCTL_RUN_APPLICATION\n");
				result = oti_run_application( reader, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_NDOT_POLL:
			{
                                dprintf("OTI_IOCTL_NDOT_POLL\n");
				result = oti_ndot_poll( reader, (NDoTOperatorId*)tx, txLen, (POtiCardInfo)rx );
				break;
			}

			case OTI_IOCTL_NDOT_EMV:
			{
                                dprintf("OTI_IOCTL_NDOT_EMV\n");
				result = oti_ndot_emv( reader, (char*)tx, txLen, (POtiCardInfo)rx );
				break;
			}

			case OTI_IOCTL_GET_EMV_COUNTRY_CODE:
			{
                                dprintf("OTI_IOCTL_GET_EMV_COUNTRY_CODE\n");
				result = oti_terminal_country_code( reader, GET, (OtiTerminalCountryCode*)rx );
				break;
			}

			case OTI_IOCTL_SET_EMV_COUNTRY_CODE:
			{
                                dprintf("OTI_IOCTL_SET_EMV_COUNTRY_CODE\n");
				result = oti_terminal_country_code( reader, SET, (OtiTerminalCountryCode*)tx );
				break;
			}

			case OTI_IOCTL_SET_PAYMENT_SUBSCHEME:
			{
                                dprintf("OTI_IOCTL_SET_PAYMENT_SUBSCHEME\n");
				result = oti_emv_config( reader, SET, OTI_ENABLE_PAYMENT_SUBSCHEME, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_EMV_RESULT_TAG_LIST:
			{
                            dprintf("OTI_IOCTL_GET_EMV_RESULT_TAG_LIST\n");
                            if(fwCompatibility == OtiFWCompatibilityNone)
                                result = oti_result_tag_list(reader, GET, OTI_EMV_RESULT_TAG_LIST, (unsigned short*)tx, txLen, (unsigned short*)rx, rxLen);
                            else if(fwCompatibility == OtiFWCompatibility040506)
				result = oti_result_tag_list_new(reader, GET, OTI_EMV_RESULT_TAG_LIST, (unsigned long*)tx, txLen, (unsigned long*)rx, rxLen);
                            break;
			}

			case OTI_IOCTL_SET_EMV_RESULT_TAG_LIST:
			{
                            dprintf("OTI_IOCTL_SET_EMV_RESULT_TAG_LIST\n");
                            if(fwCompatibility == OtiFWCompatibilityNone)
                                result = oti_result_tag_list(reader, SET, OTI_EMV_RESULT_TAG_LIST, (unsigned short *)tx, txLen, rx, rxLen );
                            else if(fwCompatibility == OtiFWCompatibility040506)
                                result = oti_result_tag_list_new(reader, SET, OTI_EMV_RESULT_TAG_LIST, (unsigned long *)tx, txLen, rx, rxLen );
                            break;
			}

			case OTI_IOCTL_GET_MAGSTRIPE_RESULT_TAG_LIST:
			{
                            dprintf("OTI_IOCTL_GET_MAGSTRIPE_RESULT_TAG_LIST\n");
                            if(fwCompatibility == OtiFWCompatibilityNone)
                                result = oti_result_tag_list(reader, GET, OTI_MAGSTRIPE_RESULT_TAG_LIST, (unsigned short*)tx, txLen, (unsigned short*)rx, rxLen);
                            else if(fwCompatibility == OtiFWCompatibility040506)
                                result = oti_result_tag_list_new(reader, GET, OTI_MAGSTRIPE_RESULT_TAG_LIST, (unsigned long*)tx, txLen, (unsigned long*)rx, rxLen);
                            break;
			}

			case OTI_IOCTL_SET_MAGSTRIPE_RESULT_TAG_LIST:
			{
                            dprintf("OTI_IOCTL_SET_MAGSTRIPE_RESULT_TAG_LIST\n");
                            if(fwCompatibility == OtiFWCompatibilityNone)
                                result = oti_result_tag_list(reader, SET, OTI_MAGSTRIPE_RESULT_TAG_LIST, (unsigned short*)tx, txLen, rx, rxLen);
                            else if(fwCompatibility == OtiFWCompatibility040506)
                                result = oti_result_tag_list_new(reader, SET, OTI_MAGSTRIPE_RESULT_TAG_LIST, (unsigned long*)tx, txLen, rx, rxLen);
                            break;
			}

			case OTI_IOCTL_POLL_EMV:
			{
                                dprintf("OTI_IOCTL_POLL_EMV\n");
				result = oti_poll_emv( reader, (unsigned char *)tx, txLen, (POtiCardInfo)rx );
				break;
			}

			case OTI_IOCTL_EMV_TERM_ONLINE_STAT:
			{
                                dprintf("OTI_IOCTL_EMV_TERM_ONLINE_STAT\n");
				result = oti_reader_command( reader, DO, OTI_TERMINAL_ONLINE_STATUS, (char*)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_EMV_GET_TRANS_CURR_CODE:
			{
                                dprintf("OTI_IOCTL_EMV_GET_TRANS_CURR_CODE\n");
                                    result = oti_emv_config( reader, GET, OTI_TRANSACTION_CURRENCY_CODE, (OtiEmvConfig *)tx, txLen, rx, rxLen );
				break;
			}

			case OTI_IOCTL_EMV_SET_TRANS_CURR_CODE:
			{
                                dprintf("OTI_IOCTL_EMV_SET_TRANS_CURR_CODE\n");
                                    result = oti_emv_config( reader, SET, OTI_TRANSACTION_CURRENCY_CODE, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_EMV_GET_PUBLIC_KEYS:
			{
                                dprintf("OTI_IOCTL_EMV_GET_PUBLIC_KEYS\n");
				result = oti_emv_public_keys( reader, GET, (char*)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_EMV_SET_PUBLIC_KEYS:
			{
                                dprintf("OTI_IOCTL_EMV_SET_PUBLIC_KEYS\n");
				result = oti_emv_public_keys( reader, SET, (char*)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_EMV_GET_PUBLIC_KEYS_LIST:
			{
                                dprintf("OTI_IOCTL_EMV_GET_PUBLIC_KEYS_LIST\n");
				result = oti_emv_public_keys_list( reader, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TERMINAL_TYPE:
			{
                                dprintf("OTI_IOCTL_GET_TERMINAL_TYPE\n");
                                if(fwCompatibility == OtiFWCompatibilityNone)
                                    result = oti_reader_command( reader, GET, OTI_TERMINAL_TYPE, NULL, 0, (char*)rx, rxLen );
                                else if(fwCompatibility == OtiFWCompatibility040506)
                                    result = oti_emv_config( reader, GET, OTI_TERMINAL_TYPE, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_TYPE:
			{
                                dprintf("OTI_IOCTL_SET_TERMINAL_TYPE\n");
                                if(fwCompatibility == OtiFWCompatibilityNone)
                                    result = oti_reader_command( reader, SET, OTI_TERMINAL_TYPE, (char *)tx, txLen, (char*)rx, rxLen );
                                else if(fwCompatibility == OtiFWCompatibility040506)
                                    result = oti_emv_config( reader, SET, OTI_TERMINAL_TYPE, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}
			case OTI_IOCTL_GET_VISA_CRYPTO_SUBSCHEME:
			{
                                dprintf("OTI_IOCTL_GET_VISA_CRYPTO_SUBSCHEME\n");
				result = oti_reader_command( reader, GET, OTI_VISA_CRYPTO_SUBSCHEME, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_VISA_CRYPTO_SUBSCHEME:
			{
                                dprintf("OTI_IOCTL_SET_VISA_CRYPTO_SUBSCHEME\n");
				result = oti_reader_command( reader, SET, OTI_VISA_CRYPTO_SUBSCHEME, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_APP_VERSION_NBR:
			{
                                dprintf("OTI_IOCTL_GET_APP_VERSION_NBR\n");
				result = oti_emv_config( reader, GET, OTI_APP_VERSION_NUMBER, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TERMINAL_FLOOR_LIMIT:
			{
				/* Encoding: BCD � 12 digits encoded in 6 bytes. Last 2 digits are after the decimal point */
                                dprintf("OTI_IOCTL_GET_TERMINAL_FLOOR_LIMIT\n");
				result = oti_emv_config( reader, GET, OTI_TERMINAL_FLOOR_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_FLOOR_LIMIT:
			{
				/* Encoding: BCD � 12 digits encoded in 6 bytes. Last 2 digits are after the decimal point */
                                dprintf("OTI_IOCTL_SET_TERMINAL_FLOOR_LIMIT\n");
				result = oti_emv_config( reader, SET, OTI_TERMINAL_FLOOR_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TERMINAL_CAPABILITIES:
			{
                                dprintf("OTI_IOCTL_GET_TERMINAL_CAPABILITIES\n");
				result = oti_emv_config( reader, GET, OTI_TERMINAL_CAPABILITIES, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_CAPABILITIES:
			{
                                dprintf("OTI_IOCTL_SET_TERMINAL_CAPABILITIES\n");
				result = oti_emv_config( reader, SET, OTI_TERMINAL_CAPABILITIES, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TCC:
			{
                                dprintf("OTI_IOCTL_GET_TCC\n");
				result = oti_emv_transaction_category_code( reader, GET, tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TCC:
			{
                                dprintf("OTI_IOCTL_SET_TCC\n");
				result = oti_emv_transaction_category_code( reader, SET, tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TARGET_PERC_BAISED_RAND:
			{
                                dprintf("OTI_IOCTL_GET_TARGET_PERC_BAISED_RAND\n");
				result = oti_emv_config( reader, GET, OTI_TARGET_PERCENT_BAISED_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TARGET_PERC_BAISED_RAND:
			{
                                dprintf("OTI_IOCTL_SET_TARGET_PERC_BAISED_RAND\n");
				result = oti_emv_config( reader, SET, OTI_TARGET_PERCENT_BAISED_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_MAX_PERC_RAND:
			{
                                dprintf("OTI_IOCTL_GET_MAX_PERC_RAND\n");
				result = oti_emv_config( reader, GET, OTI_MAX_PERCENT_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_MAX_PERC_RAND:
			{
                                dprintf("OTI_IOCTL_SET_MAX_PERC_RAND\n");
				result = oti_emv_config( reader, SET, OTI_MAX_PERCENT_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_THRESHOLD_BAISED_RANDOM:
			{
                                dprintf("OTI_IOCTL_GET_THRESHOLD_BAISED_RANDOM\n");
				result = oti_emv_config( reader, GET, OTI_THRESHOLD_VALUE_BAISED_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_THRESHOLD_BAISED_RANDOM:
			{
                                dprintf("OTI_IOCTL_SET_THRESHOLD_BAISED_RANDOM\n");
				result = oti_emv_config( reader, SET, OTI_THRESHOLD_VALUE_BAISED_RANDOM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TAC_DEFAULT:
			{
                                dprintf("OTI_IOCTL_GET_TAC_DEFAULT\n");
				result = oti_emv_config( reader, GET, OTI_TAC_DEFAULT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}
			case OTI_IOCTL_SET_TAC_DEFAULT:
			{
                                dprintf("OTI_IOCTL_SET_TAC_DEFAULT\n");
				result = oti_emv_config( reader, SET, OTI_TAC_DEFAULT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TAC_DENIAL:
			{
                                dprintf("OTI_IOCTL_GET_TAC_DENIAL\n");
				result = oti_emv_config( reader, GET, OTI_TAC_DENIAL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}
			case OTI_IOCTL_SET_TAC_DENIAL:
			{
                                dprintf("OTI_IOCTL_SET_TAC_DENIAL\n");
				result = oti_emv_config( reader, SET, OTI_TAC_DENIAL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TAC_ONLINE:
			{
                                dprintf("OTI_IOCTL_GET_TAC_ONLINE\n");
				result = oti_emv_config( reader, GET, OTI_TAC_ONLINE, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TAC_ONLINE:
			{
                                dprintf("OTI_IOCTL_SET_TAC_ONLINE\n");
				result = oti_emv_config( reader, SET, OTI_TAC_ONLINE, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_DEFAULT_DDOL:
			{
                                dprintf("OTI_IOCTL_GET_DEFAULT_DDOL\n");
				result = oti_emv_config( reader, GET, OTI_DEFAULT_DDOL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_DEFAULT_DDOL:
			{
                                dprintf("OTI_IOCTL_SET_DEFAULT_DDOL\n");
				result = oti_emv_config( reader, SET, OTI_DEFAULT_DDOL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_DEFAULT_TDOL:
			{
                                dprintf("OTI_IOCTL_GET_DEFAULT_TDOL\n");
				result = oti_emv_config( reader, GET, OTI_DEFAULT_TDOL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_DEFAULT_TDOL:
			{
                                dprintf("OTI_IOCTL_SET_DEFAULT_TDOL\n");
				result = oti_emv_config( reader, SET, OTI_DEFAULT_TDOL, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_PCD_TRANSACTION_LIMIT:
			{
                                dprintf("OTI_IOCTL_GET_PCD_TRANSACTION_LIMIT\n");
				result = oti_emv_config( reader, GET, OTI_PCD_TRANSACTION_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_PCD_TRANSACTION_LIMIT:
			{
                                dprintf("OTI_IOCTL_SET_PCD_TRANSACTION_LIMIT\n");
				result = oti_emv_config( reader, SET, OTI_PCD_TRANSACTION_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_CVM_REQUIRED_LIMIT:
			{
                                dprintf("OTI_IOCTL_GET_CVM_REQUIRED_LIMIT\n");
				result = oti_emv_config( reader, GET, OTI_CVM_REQUIRED_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_CVM_REQUIRED_LIMIT:
			{
                                dprintf("OTI_IOCTL_SET_CVM_REQUIRED_LIMIT\n");
				result = oti_emv_config( reader, SET, OTI_CVM_REQUIRED_LIMIT, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_AMOUNT_ZERO_ONLINE_FLAG:
			{
                                dprintf("OTI_IOCTL_GET_AMOUNT_ZERO_ONLINE_FLAG\n");
				result = oti_emv_config( reader, GET, OTI_AMOUNT_ZERO_ONLINE_FLAG, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_AMOUNT_ZERO_ONLINE_FLAG:
			{
                                dprintf("OTI_IOCTL_SET_AMOUNT_ZERO_ONLINE_FLAG\n");
				result = oti_emv_config( reader, SET, OTI_AMOUNT_ZERO_ONLINE_FLAG, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_STATUS_CHECK_FLAG:
			{
                                dprintf("OTI_IOCTL_GET_STATUS_CHECK_FLAG\n");
				result = oti_emv_config( reader, GET, OTI_STATUS_CHECK_FLAG, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_STATUS_CHECK_FLAG:
			{
                                dprintf("OTI_IOCTL_SET_STATUS_CHECK_FLAG\n");
				result = oti_emv_config( reader, SET, OTI_STATUS_CHECK_FLAG, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_VISA_FDDA_VERS_0_ENABLE:
			{
                                dprintf("OTI_IOCTL_GET_VISA_FDDA_VERS_0_ENABLE\n");
				result = oti_reader_command( reader, GET, OTI_VISA_FDDA_VERS_0_ENABLE, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_VISA_FDDA_VERS_0_ENABLE:
			{
                                dprintf("OTI_IOCTL_SET_VISA_FDDA_VERS_0_ENABLE\n");
				result = oti_reader_command( reader, SET, OTI_VISA_FDDA_VERS_0_ENABLE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TERMINAL_COMM_CHECK:
			{
                                dprintf("OTI_IOCTL_GET_TERMINAL_COMM_CHECK\n");
				result = oti_reader_command( reader, GET, OTI_TERMINAL_COMM_CHECK, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_COMM_CHECK:
			{
                                dprintf("OTI_IOCTL_SET_TERMINAL_COMM_CHECK\n");
				result = oti_reader_command( reader, SET, OTI_TERMINAL_COMM_CHECK, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_VISA_WAVE_ENABLE:
			{
                                dprintf("OTI_IOCTL_GET_VISA_WAVE_ENABLE\n");
				result = oti_reader_command( reader, GET, OTI_VISA_WAVE_ENABLE, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_VISA_WAVE_ENABLE:
			{
                                dprintf("OTI_IOCTL_SET_VISA_WAVE_ENABLE\n");
				result = oti_reader_command( reader, SET, OTI_VISA_WAVE_ENABLE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_LOCK_PCD_PROTECTED:
			{
                                dprintf("OTI_IOCTL_GET_LOCK_PCD_PROTECTED\n");
				result = oti_reader_command( reader, GET, OTI_LOCK_PCD_PROTECTED_CMD, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_LOCK_PCD_PROTECTED:
			{
                                dprintf("OTI_IOCTL_SET_LOCK_PCD_PROTECTED\n");
				result = oti_reader_command( reader, SET, OTI_LOCK_PCD_PROTECTED_CMD, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_PCD_PROTOCOL_SELECTOR:
			{
                                dprintf("OTI_IOCTL_GET_PCD_PROTOCOL_SELECTOR\n");
				result = oti_pcd_protocol_selector( reader, GET, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_PCD_PROTOCOL_SELECTOR:
			{
                                dprintf("OTI_IOCTL_SET_PCD_PROTOCOL_SELECTOR\n");
				result = oti_pcd_protocol_selector( reader, SET, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_RF_COLLISION_MSG_ENABLED:
			{
                                dprintf("OTI_IOCTL_GET_RF_COLLISION_MSG_ENABLED\n");
				result = oti_rf_collision_msg_enabled( reader, GET, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_RF_COLLISION_MSG_ENABLED:
			{
                                dprintf("OTI_IOCTL_SET_RF_COLLISION_MSG_ENABLED\n");
				result = oti_rf_collision_msg_enabled( reader, SET, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TRACKS_FOLLOWED_BY_AID:
			{
                                dprintf("OTI_IOCTL_GET_TRACKS_FOLLOWED_BY_AID\n");
				result = oti_reader_command( reader, GET, OTI_TRACKS_FOLLOWED_BY_AID, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TRACKS_FOLLOWED_BY_AID:
			{
                                dprintf("OTI_IOCTL_SET_TRACKS_FOLLOWED_BY_AID\n");
				result = oti_reader_command( reader, SET, OTI_TRACKS_FOLLOWED_BY_AID, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_PUPI:
			{
                                dprintf("OTI_IOCTL_GET_PUPI\n");
				result = oti_reader_command( reader, GET, OTI_PUPI, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_PUPI:
			{
                                dprintf("OTI_IOCTL_SET_PUPI\n");
				result = oti_reader_command( reader, SET, OTI_PUPI, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_PUPI:
			{
                                dprintf("OTI_IOCTL_DO_PUPI\n");
				result = oti_reader_command( reader, DO, OTI_PUPI, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_UID:
			{
                                dprintf("OTI_IOCTL_GET_UID\n");
				result = oti_reader_command( reader, GET, OTI_UID, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_UID:
			{
                                dprintf("OTI_IOCTL_SET_UID\n");
				result = oti_reader_command( reader, SET, OTI_UID, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_UID:
			{
                                dprintf("OTI_IOCTL_DO_UID\n");
				result = oti_reader_command( reader, DO, OTI_UID, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_ATQB:
			{
                                dprintf("OTI_IOCTL_GET_ATQB\n");
				result = oti_reader_command( reader, GET, OTI_ATQB, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_ATQB:
			{
                                dprintf("OTI_IOCTL_SET_ATQB\n");
				result = oti_reader_command( reader, SET, OTI_ATQB, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_ATQB:
			{
                                dprintf("OTI_IOCTL_DO_ATQB\n");
				result = oti_reader_command( reader, DO, OTI_ATQB, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_ATS:
			{
                                dprintf("OTI_IOCTL_GET_ATS\n");
				result = oti_reader_command( reader, GET, OTI_ATS, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_ATS:
			{
                                dprintf("OTI_IOCTL_SET_ATS\n");
				result = oti_reader_command( reader, SET, OTI_ATS, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_ATS:
			{
                                dprintf("OTI_IOCTL_DO_ATS\n");
				result = oti_reader_command( reader, DO, OTI_ATS, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_SAK:
			{
                                dprintf("OTI_IOCTL_GET_SAK\n");
				result = oti_reader_command( reader, GET, OTI_SAK, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_SAK:
			{
                                dprintf("OTI_IOCTL_SET_SAK\n");
				result = oti_reader_command( reader, SET, OTI_SAK, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_SAK:
			{
                                dprintf("OTI_IOCTL_DO_SAK\n");
				result = oti_reader_command( reader, DO, OTI_SAK, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_TRANSP_AUTO_DESELECT:
			{
                                dprintf("OTI_IOCTL_DO_TRANSP_AUTO_DESELECT\n");
				result = oti_reader_command( reader, DO, OTI_TRANSPARENT_AUTO_DESELECT, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_RF_PARAM_BIT_RATE:
			{
                                dprintf("OTI_IOCTL_DO_RF_PARAM_BIT_RATE\n");
				result = oti_reader_command( reader, DO, OTI_RF_PARAM_BIT_RATE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_RF_PARAM_MAX_BIT_RATE:
			{
                                dprintf("OTI_IOCTL_GET_RF_PARAM_MAX_BIT_RATE\n");
				result = oti_reader_command( reader, GET, OTI_RF_PARAM_MAX_BIT_RATE, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_RF_PARAM_MAX_BIT_RATE:
			{
                                dprintf("OTI_IOCTL_SET_RF_PARAM_MAX_BIT_RATE\n");
				result = oti_reader_command( reader, SET, OTI_RF_PARAM_MAX_BIT_RATE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_RF_PARAM_MAX_BIT_RATE:
			{
                                dprintf("OTI_IOCTL_DO_RF_PARAM_MAX_BIT_RATE\n");
				result = oti_reader_command( reader, DO, OTI_RF_PARAM_MAX_BIT_RATE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_RF_PARAM_FWTI:
			{
                                dprintf("OTI_IOCTL_DO_RF_PARAM_FWTI\n");
				result = oti_reader_command( reader, DO, OTI_RF_PARAM_FWTI, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_DESELECT:
			{
                                dprintf("OTI_IOCTL_DO_DESELECT\n");
				result = oti_reader_command( reader, DO, OTI_DESELECT, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_PCD_SERIAL_NUMBER:
			{
                                dprintf("OTI_IOCTL_GET_PCD_SERIAL_NUMBER\n");
				result = oti_reader_command( reader, GET, OTI_PCD_SERIAL_NUMBER, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TYPE_B_MODULATION_INDEX:	/* Only GET implemeted, protected command */
			{
                                dprintf("OTI_IOCTL_GET_TYPE_B_MODULATION_INDEX\n");
				result = oti_reader_command( reader, GET, OTI_TYPE_B_MODULATION_INDEX, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_APP_SELECTION_INDICATOR:
			{
                                dprintf("OTI_IOCTL_GET_APP_SELECTION_INDICATOR\n");
				result = oti_emv_config( reader, GET, OTI_APP_SELECTION_INDICATOR, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_APP_SELECTION_INDICATOR:
			{
                                dprintf("OTI_IOCTL_SET_APP_SELECTION_INDICATOR\n");
				result = oti_emv_config( reader, SET, OTI_APP_SELECTION_INDICATOR, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_UNLOCK_PROTECTED:
			{
                                dprintf("OTI_IOCTL_UNLOCK_PROTECTED\n");
				result = oti_reader_command( reader, DO, OTI_UNLOCK_PROTECTED_CMD_TEMPLATE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_OUTPORT_CONTROL:
			{
                                dprintf("OTI_IOCTL_DO_OUTPORT_CONTROL\n");
				result = oti_reader_command( reader, DO, OTI_OUTPORT_CONTROL, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}
			
			case OTI_IOCTL_SET_OUTPORT_CONTROL:
			{
                                dprintf("OTI_IOCTL_SET_OUTPORT_CONTROL\n");
				result = oti_reader_command( reader, SET, OTI_OUTPORT_CONTROL, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}
			
			case OTI_IOCTL_DO_OUTPORT:
			{
                                dprintf("OTI_IOCTL_DO_OUTPORT\n");
				result = oti_reader_command( reader, DO, OTI_OUTPORT, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_RESET_PCD:
			{
                                dprintf("OTI_IOCTL_RESET_PCD\n");
			      result = oti_reader_command( reader, DO, OTI_RESET_PCD, NULL, 0, NULL, 0 );
			      break;
			}

			case OTI_IOCTL_SET_VERBOSITY:
			{
			    if (rxLen == 1){
				if ( ((char*)rx)[0] >= OtiVerbosityNone && ((char*)rx)[0] <= OtiVerbosityAll ) {
                                    verbosity = ((char*)rx)[0]; 
                                    result = 0;
                                    dprintf("OtiIoctl command: %d (%08X) => OTI_IOCTL_SET_VERBOSITY=%x\n", (int)command, (unsigned)command,verbosity);
                                    break;
				}
			      }
                              errno = EBADRQC;
                              result = -1;
			      break;
			}

			case OTI_IOCTL_GET_TIMEOUTS:
			{
                                dprintf("OTI_IOCTL_GET_TIMEOUTS\n");
				if( (rxLen < sizeof(OtiReaderTimeout)) || (rx == NULL) ) {
					errno = EINVAL;
					result = -1;
				} else {
					memcpy(rx,&reader->timeout,result=sizeof(OtiReaderTimeout));
				}
				break;
			}

			case OTI_IOCTL_SET_TIMEOUTS:
			{
                                dprintf("OTI_IOCTL_SET_TIMEOUTS\n");
				if( (txLen != sizeof(OtiReaderTimeout)) || (tx == NULL) ) {
					errno = EINVAL;
					result = -1;
				} else {
					memcpy(&reader->timeout,tx,sizeof(OtiReaderTimeout));
					result = 0;
				}
				break;
			}

                        case OTI_IOCTL_SET_FW_COMPATIBILITY:
                        {
                            dprintf("OTI_IOCTL_SET_FW_COMPATIBILITY\n");
			    if (rxLen == 1){
				if ( ((char*)rx)[0] >= OtiFWCompatibilityNone && ((char*)rx)[0] <= OtiFWCompatibilityLast ) {
                                    fwCompatibility = ((char*)rx)[0]; 
                                    result = 0;
                                    break;
				}
			      }
                              errno = EBADRQC;
                              result = -1;
			      break;
                        }
                        
			case OTI_IOCTL_GET_VISA_WAVE_OFFLINE_ENABLE:
			{
                                dprintf("OTI_IOCTL_GET_VISA_WAVE_OFFLINE_ENABLE\n");
				result = oti_reader_command( reader, GET, OTI_VISA_WAVE_OFFLINE_ENABLE, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_VISA_WAVE_OFFLINE_ENABLE:
			{
                                dprintf("OTI_IOCTL_SET_VISA_WAVE_OFFLINE_ENABLE\n");
				result = oti_reader_command( reader, SET, OTI_VISA_WAVE_OFFLINE_ENABLE, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}
                        
                        case OTI_IOCTL_GET_VISA_ENABLE_LIMIT:
                        {
                            dprintf("OTI_IOCTL_GET_VISA_ENABLE_LIMIT");
                            result = oti_reader_command(reader, GET, OTI_VISA_ENABLE_LIMIT, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_SET_VISA_ENABLE_LIMIT:
                        {
                            dprintf("OTI_IOCTL_SET_VISA_ENABLE_LIMIT");
                            result = oti_reader_command(reader, SET, OTI_VISA_ENABLE_LIMIT, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_GET_VISA_FORMAT_TRACK1_ENABLE:
                        {
                            dprintf("OTI_IOCTL_GET_VISA_FORMAT_TRACK1_ENABLE");
                            result = oti_reader_command(reader, GET, OTI_VISA_FORMAT_TRACK1_ENABLE, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_SET_VISA_FORMAT_TRACK1_ENABLE:
                        {
                            dprintf("OTI_IOCTL_SET_VISA_FORMAT_TRACK1_ENABLE");
                            result = oti_reader_command(reader, SET, OTI_VISA_FORMAT_TRACK1_ENABLE, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_GET_TRACK2_FORMAT:
                        {
                            dprintf("OTI_IOCTL_GET_TRACK2_FORMAT");
                            result = oti_reader_command(reader, GET, OTI_TRACK2_FORMAT, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_SET_TRACK2_FORMAT:
                        {
                            dprintf("OTI_IOCTL_SET_TRACK2_FORMAT");
                            result = oti_reader_command(reader, SET, OTI_TRACK2_FORMAT, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_GET_EXCAHNGE_DATA_FLAG:
                        {
                            dprintf("OTI_IOCTL_GET_EXCAHNGE_DATA_FLAG");
                            result = oti_reader_command(reader, GET, OTI_EXCAHNGE_DATA_FLAG, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_SET_EXCAHNGE_DATA_FLAG:
                        {
                            dprintf("OTI_IOCTL_SET_EXCAHNGE_DATA_FLAG");
                            result = oti_reader_command(reader, SET, OTI_EXCAHNGE_DATA_FLAG, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_DO_EXCAHNGE_DATA_FLAG:
                        {
                            dprintf("OTI_IOCTL_DO_EXCAHNGE_DATA_FLAG");
                            result = oti_reader_command(reader, DO, OTI_EXCAHNGE_DATA_FLAG, (char *)tx, txLen, (char*)rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_GET_KERNEL_CONFIGURATION:
                        {
                            dprintf("OTI_IOCTL_GET_KERNEL_CONFIGURATION");
                            result = oti_emv_config( reader, GET, OTI_KERNEL_CONFIGURATION, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
                            break;
                        }

                        case OTI_IOCTL_SET_KERNEL_CONFIGURATION:
                        {
                            dprintf("OTI_IOCTL_SET_KERNEL_CONFIGURATION");
                            result = oti_emv_config( reader, SET, OTI_KERNEL_CONFIGURATION, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
                            break;
                        }

                        case OTI_IOCTL_GET_TERMINATE_RESULT_TAG_LIST:
                        {
                            dprintf("OTI_IOCTL_GET_TERMINATE_RESULT_TAG_LIST");
                            result = oti_result_tag_list_new(reader, GET, OTI_TERMINATE_RESULT_TAG_LIST, (unsigned long*)tx, txLen, rx, rxLen);
                            break;
                        }

                        case OTI_IOCTL_SET_TERMINATE_RESULT_TAG_LIST:
                        {
                            dprintf("OTI_IOCTL_SET_TERMINATE_RESULT_TAG_LIST");
                            result = oti_result_tag_list_new(reader, SET, OTI_TERMINATE_RESULT_TAG_LIST, (unsigned long*)tx, txLen, rx, rxLen);
                            break;
                        }

			case OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM:
			{
                                dprintf("OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM\n");
				result = oti_emv_config( reader, GET, OTI_TERMINAL_CAPABILITIES_NO_CVM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM:
			{
                                dprintf("OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM\n");
				result = oti_emv_config( reader, SET, OTI_TERMINAL_CAPABILITIES_NO_CVM, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM_MC:
			{
                                dprintf("OTI_IOCTL_GET_TERMINAL_CAPABILITIES_NO_CVM_MC\n");
				result = oti_emv_config( reader, GET, OTI_TERMINAL_CAPABILITIES_NO_CVM_MC, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM_MC:
			{
                                dprintf("OTI_IOCTL_SET_TERMINAL_CAPABILITIES_NO_CVM_MC\n");
				result = oti_emv_config( reader, SET, OTI_TERMINAL_CAPABILITIES_NO_CVM_MC, (OtiEmvConfig *)tx, txLen, (char*)rx, rxLen );
				break;
			}

            case OTI_IOTCL_SAM_POWER_UP_RESET:
			{
                dprintf("OTI_IOTCL_SAM_POWER_UP_RESET\n");
				result = oti_reader_command( reader, DO, OTI_SAM_POWER_UP_RESET, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_RAW_COMMAND:
			{
                                dprintf("OTI_IOCTL_RAW_COMMAND\n");
				result = oti_reader_raw_command( reader, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_GET_EXTERNAL_DISPLAY_MESSAGES:
			{
                                dprintf("OTI_IOCTL_GET_EXTERNAL_DISPLAY_MESSAGES\n");
				result = oti_reader_command( reader, GET, OTI_EXTERNAL_DISPLAY, NULL, 0, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_SET_EXTERNAL_DISPLAY_MESSAGES:
			{
                                dprintf("OTI_IOCTL_SET_EXTERNAL_DISPLAY_MESSAGES\n");
				result = oti_reader_command( reader, SET, OTI_EXTERNAL_DISPLAY, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_DO_EXTERNAL_DISPLAY_MESSAGES:
			{
                                dprintf("OTI_IOCTL_DO_EXTERNAL_DISPLAY_MESSAGES\n");
				result = oti_reader_command( reader, DO, OTI_EXTERNAL_DISPLAY, (char *)tx, txLen, (char*)rx, rxLen );
				break;
			}

			case OTI_IOCTL_EXT_DISP_MESS_CALLBACK:
			{
                                dprintf("OTI_IOCTL_EXT_DISP_MESS_CALLBACK\n");
				reader->edm_callback = tx;
				result = 0;
				break;
			}

			case OTI_IOCTL_COUNT:
                                dprintf("OTI_IOCTL_COUNT ");
			default:
			{
                                dprintf("and default.\n");
				errno = EBADRQC;
				result = -1;
				break;
			}
		}
	}

	return ( result );
}

