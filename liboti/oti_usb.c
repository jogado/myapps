#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libudev.h>
#include <unistd.h>
#include <linux/usbdevice_fs.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "oti_usb.h"

#define USBBUS_PATH "/dev/bus/usb"

static int udev_get_bus_and_dev_number(const char *path,
				       unsigned int *busnum,
				       unsigned int *devnum)
{
	struct udev *udev;
	struct udev_device *device = NULL, *device_parent;
	struct stat statbuf;
	const char *value;
	char type;
	int rc = -1;

	udev = udev_new();
	if (!udev) {
		printf("Can't create udev\n");
		return 1;
	}

	if (stat(path, &statbuf) < 0) {
		fprintf(stderr, "device node not found\n");
		goto exit;
	}

	if (S_ISBLK(statbuf.st_mode)) {
		type = 'b';
	} else if (S_ISCHR(statbuf.st_mode)) {
		type = 'c';
	} else {
		fprintf(stderr, "device node has wrong file type\n");
		goto exit;
	}

	device = udev_device_new_from_devnum(udev, type, statbuf.st_rdev);
	if (device == NULL) {
		fprintf(stderr, "device node not found\n");
		goto exit;
	}

        device_parent = device;
        do {
                device_parent = udev_device_get_parent(device_parent);
                if (device_parent == NULL)
                        break;

                value = udev_device_get_subsystem(device_parent);
		if (strcmp(value, "usb"))
			continue;

		value = udev_device_get_sysattr_value(device_parent, "busnum");
		if (!value)
			continue;

		*busnum = strtoul(value, NULL, 10);

		value = udev_device_get_sysattr_value(device_parent, "devnum");
		if (!value)
			continue;

		*devnum = strtoul(value, NULL, 10);

		rc = 0;
		goto exit;
        } while (device_parent != NULL);

exit:
	if (device)
		udev_device_unref(device);
	udev_unref(udev);

	return rc;
}

static int usb_reset(const char *path)
{
	int fd;

	fd = open(path, O_WRONLY);
	if (fd < 0) {
		perror("open");
		return 1;
	}

	if (ioctl(fd, USBDEVFS_RESET, 0) < 0) {
		perror("ioctl");
		close(fd);
		return 1;
	}

	close(fd);
	return 0;
}

int oti_usb_reset(const char *devpath)
{
	unsigned int bus, devnum;
	char path[PATH_MAX];

	if (udev_get_bus_and_dev_number(devpath, &bus, &devnum)) {
		fprintf(stderr, "Cannot get usb bus and/or device number!\n");
		return 1;
	}

	snprintf(path, sizeof(path), "%s/%03d/%03d", USBBUS_PATH, bus, devnum);

	if (usb_reset(path)) {
		fprintf(stderr, "Cannot reset usb device!\n");
		return 1;
	}

	return 0;
}
