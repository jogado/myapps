/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_emv.c
 *
 *  Created on: Jan 12, 2011
 *      Author: wmeganck
 */

#include <errno.h>
#include <string.h>

#include "oti_emv.h"
/**
 * This function will get/set the country code on the terminal.
 *
 * @param reader				pointer to reader control block.
 * @param terminalCountryCode	county code according ISO 3166-1.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */
int oti_terminal_country_code( POtiReaderCb reader, OpCode code, OtiTerminalCountryCode *terminalCountryCode )
{
	int result = 0;
	char data[2];

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, OTI_TERMINAL_COUNTRY_CODE, NULL, 0, data, sizeof(data), OtiTlvSizeAdd );
		//printf(" >>>>data 0: %02X,  data 1: %02X\n", data[0], data[1]);
		*terminalCountryCode = data[0] << 8;
		*terminalCountryCode |= data[1];
	}	
	else if (code == SET)
	{
		data[0] = ((*terminalCountryCode >> 8) & 0xFF);
		data[1] = (*terminalCountryCode & 0xFF);
		result = oti_handle_tag( reader, SET, OTI_TERMINAL_COUNTRY_CODE, data, sizeof(data), NULL, 0, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This function will set the tag result list on the terminal.
 *
 * @param reader				pointer to reader control block.
 * @param tx					list of AID length, AID, setting.
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */

int oti_result_tag_list( POtiReaderCb reader, OpCode code, OtiTag otiTag, unsigned short* tagList, size_t txLen, unsigned short* rx, size_t rxLen )
{
	int result = 0;
	unsigned char data[200];
	unsigned char *pData;
	int nbrTags = txLen;
	int cnt;
	int i=0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, otiTag, NULL, 0, data, sizeof(data), OtiTlvSizeAdd );
		if (result > 0)
		{
			memset(rx, rxLen, sizeof(unsigned short));
			for( cnt=0; cnt<result; cnt++)
			{
				if (data[cnt] == 0x00)
					i++;
				rx[i] <<= 8;
				rx[i] |= data[cnt];
			}
			result = i;
		}
	}
	else if (code == SET)
	{
		pData = data;
		while( nbrTags-- )
		{
			if ( *tagList & 0xFF00)
				*(pData++) = (*tagList) >> 8 & 0xFF;

			*(pData++) = *(tagList++) & 0xFF;
			*(pData++) = 0x00;
		}
		result = oti_handle_tag( reader, SET, otiTag, data, pData - data, (char *)rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

int oti_result_tag_list_new( POtiReaderCb reader, OpCode code, OtiTag otiTag, unsigned long* tagList, size_t txLen, unsigned long* rx, size_t rxLen )
{
	int result = 0;
	unsigned char data[200];
	unsigned char *pData;
	int nbrTags = txLen;
	int cnt;
	int i=0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, otiTag, NULL, 0, data, sizeof(data), OtiTlvSizeAdd );
		if (result > 0)
		{
			memset(rx, 0, rxLen * sizeof(unsigned long));
			for (cnt=0; cnt<result; cnt++)
			{
				if (data[cnt] == 0x00)
					i++;
				rx[i] <<= 8;
				rx[i] |= data[cnt];
			}
			result = i;
		}
	}
	else if (code == SET)
	{
		pData = data;
		while( nbrTags-- )
		{
			if (*tagList & 0x00FF0000)
				*(pData++) = ((*tagList) >> 16) & 0xFF;
			if (*tagList & 0x0000FF00)
				*(pData++) = ((*tagList) >>  8) & 0xFF;

			*(pData++) = *(tagList++) & 0xFF;
			*(pData++) = 0x00;
		}
		result = oti_handle_tag( reader, SET, otiTag, data, pData - data, (char *)rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This function reads or writes the public keys.
 *
 * @param reader				pointer to reader control block.
 * @param code					DO, SET or GET.
 * @param tx					transmit data, the public keys.
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */

int oti_emv_public_keys( POtiReaderCb reader, OpCode code, char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, OTI_PUBLIC_KEYS, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	}	
	else if (code == SET)
	{
		result = oti_handle_tag( reader, SET, OTI_PUBLIC_KEYS, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This function gets a list of all available Public Keys (RID and index only) in the PCD.
 *
 * @param reader				pointer to reader control block.
 * @param tx					transmit data, RID of the CA, Public Key Index, Action(Add or Delete), Key Data.
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */

int oti_emv_public_keys_list( POtiReaderCb reader, char* rx, size_t rxLen )
{
	int result = 0;

	result = oti_handle_tag( reader, GET, OTI_PUBLIC_KEYS_LIST, NULL, 0, rx, rxLen, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}
				
/**
 * This function gets or sets the Transaction Category Code
 *
 * @param reader				pointer to reader control block.
 * @param code					SET, GET or DO
 * @param tx					transmit data, category code
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */
int oti_emv_transaction_category_code( POtiReaderCb reader, OpCode code, const char* tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;

	if (code == GET)
	{
		result = oti_handle_tag( reader, GET, OTI_TRANSACTION_CATEGORY_CODE, NULL, 0, rx, rxLen, OtiTlvSizeAdd );
	}	
	else if (code == SET)
	{
		result = oti_handle_tag( reader, SET, OTI_TRANSACTION_CATEGORY_CODE, tx, txLen, rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This function gets or sets EMV config parameters
 *
 * @param reader				pointer to reader control block.
 * @param code					SET, GET or DO
 * @param tx					pointer to the OtiEmvConfig struct
 * @param txLen					length of the transmit data buffer.
 * @param rxData				pointer to receive data buffer.
 * @param rxLen					length of the receive data buffer.
 */
int oti_emv_config( POtiReaderCb reader, OpCode code, OtiTag otiTag, POtiEmvConfig tx, size_t txLen, char* rx, size_t rxLen )
{
	int result = 0;
	unsigned char data[100];
	unsigned char rxData[100];
	int i = 0;

	data[i++] = tx->AIDLen;
	memcpy(&data[i], tx->AID, tx->AIDLen);
	i+= tx->AIDLen;

	if (code == GET)
	{
		switch(otiTag)
		{
		case OTI_TRANSACTION_CURRENCY_CODE:
			result = oti_handle_tag( reader, GET, otiTag, data, i, rxData, sizeof(rxData), OtiTlvSizeAdd );
			*(unsigned short *)rx = rxData[0] << 8; 
			*(unsigned short *)rx |= rxData[1]; 
			break;

		case OTI_TERMINAL_CAPABILITIES:
                case OTI_TERMINAL_CAPABILITIES_NO_CVM:
                case OTI_TERMINAL_CAPABILITIES_NO_CVM_MC:
			result = oti_handle_tag( reader, GET, otiTag, data, i, rxData, sizeof(rxData), OtiTlvSizeAdd );
			*(unsigned long *)rx = rxData[0] << 16; 
			*(unsigned long *)rx |= rxData[1] << 8; 
			*(unsigned long *)rx |= rxData[2]; 
			break;

                case OTI_TERMINAL_TYPE:
			result = oti_handle_tag( reader, GET, otiTag, data, i, rxData, sizeof(rxData), OtiTlvSizeAdd );
                        *(unsigned long*)rx = rxData[0];
                        break;
                    
		default:
			result = oti_handle_tag( reader, GET, otiTag, data, i, rx, rxLen, OtiTlvSizeAdd );
			break;
		}
	}	
	else if (code == SET)
	{
		switch(otiTag)
		{
		case OTI_ENABLE_PAYMENT_SUBSCHEME:
			data[i++] = (char)tx->paymentScheme;
			break;

		case OTI_TRANSACTION_CURRENCY_CODE:
			data[i++] = (tx->currencyCode & 0xFF00) >> 8;
			data[i++] = tx->currencyCode & 0x00FF;
			break;

		case OTI_TERMINAL_CAPABILITIES:
                case OTI_TERMINAL_CAPABILITIES_NO_CVM:
                case OTI_TERMINAL_CAPABILITIES_NO_CVM_MC:
			data[i++] = (tx->termCap & 0xFF0000) >> 16;
			data[i++] = (tx->termCap & 0x00FF00) >> 8;
			data[i++] = (tx->termCap & 0x0000FF);
			break;

                case OTI_TERMINAL_TYPE:
                        data[i++] = (unsigned char)(tx->termType);
                        break;
                    
		default:
			memcpy(&data[i], tx->data, tx->dataLen);
			i+= tx->dataLen;
		}

		result = oti_handle_tag( reader, SET, otiTag, data, i, rx, rxLen, OtiTlvSizeAdd );
	}
	else
	{
		errno = EINVAL;
		result = -1;
	}

	return check_oti_handle_tag_answer(result);
}
