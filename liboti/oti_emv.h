/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_emv.h
 *
 *  Created on: Jan 12, 2011
 *      Author: wmeganck
 */

#ifndef _PRODATA_SYSTEMS_OTI_EMV_H_
#define _PRODATA_SYSTEMS_OTI_EMV_H_ 1

#include <oti.h>

#include "oti_reader.h"
#include "oti_command.h"

/*-- Function declarations --*/
int oti_terminal_country_code( POtiReaderCb reader, OpCode code, OtiTerminalCountryCode *terminalCountryCode );
int oti_result_tag_list( POtiReaderCb reader, OpCode code, OtiTag otiTag, unsigned short* tagList, size_t txLen, unsigned short* rx, size_t rxLen );
int oti_result_tag_list_new( POtiReaderCb reader, OpCode code, OtiTag otiTag, unsigned long* tagList, size_t txLen, unsigned long* rx, size_t rxLen );
int oti_emv_public_keys( POtiReaderCb reader, OpCode code, char* tx, size_t txLen, char* rx, size_t rxLen );
int oti_emv_public_keys_list( POtiReaderCb reader, char* rx, size_t rxLen );
int oti_emv_transaction_category_code( POtiReaderCb reader, OpCode code, const char* tx, size_t txLen, char* rx, size_t rxLen );
int oti_emv_config( POtiReaderCb reader, OpCode code, OtiTag otiTag, POtiEmvConfig tx, size_t txLen, char* rx, size_t rxLen );

#endif /* _PRODATA_SYSTEMS_OTI_EMV_H_ */

	
