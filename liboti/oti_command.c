/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_command.c
 *
 *  Created on: Oct 14, 2008
 *      Author: jvolckae
 *  Modification history.
 *        Date		Author	Description
 *        April 24, 2015 JB	Add infinite blocking poll and way to stop it
 */
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/times.h>
#include <sys/select.h>

#include <oti.h>

#if defined INTERFACE_STM32
#include <stm32.h>
#include <stm32_api.h>
#endif

#include "oti_command.h"
#include "oti_reader.h"
#include "oti_rf.h"
#include "oti_trace.h"

static int _oti_transmit( POtiReaderCb reader, const char* data, size_t length );
static int _oti_transmit_serial( POtiReaderCb reader, const char* data, size_t length );
static size_t _oti_receive( POtiReaderCb reader, char** data, size_t length, int timeout );
static size_t _oti_receive_serial( POtiReaderCb reader, char** data, size_t length, int timeout );
static int _oti_parse_frame( POtiReaderCb reader, POtiTransaction trx, char** data, size_t length );
static int _oti_parse_tlv_response( POtiReaderCb reader, const char* tlv, char** data, size_t length );
static int _oti_check_lrc( const char* data, size_t length, char lrc );
static int _oti_check_reply_tag(OtiTag commandTag, char *rxBuf, size_t *offset);

#define isCbMode() (reader->otiOpen.mode == OtiReaderModeCallback)
//#define OTISHOWIO	1
#ifdef OTISHOWIO
#include <sys/time.h>
static int GetMicroSecStamp( void ){
	struct timeval tv;
	gettimeofday(&tv,NULL);
	return tv.tv_sec*1000000 + tv.tv_usec;
}
#endif

/**
 * 
 * @param answer If is a negative number is converted to -1.
 * @return corrected value for answer.
 */
int check_oti_handle_tag_answer(int answer)
{
	if (answer < 0)
	    answer = -1;
	return answer;
}

/**
 * This method will send a commond to the OTI reader and wait for its acknowledge message
 * and its response message.
 *
 * @param reader		pointer to reader control block.
 * @param code			OpCode of the command to send.
 * @param tag			tag of the command.
 * @param tx			pointer to the transmit buffer for the given tag.
 * @param txLength		length of the transmit buffer.
 * @param rx			pointer to the receive buffer for the given tag.
 * @param rxLength		maximum length of the transmit buffer.
 * @param EnableTlvLen	If true, TLV message contains length byte else not.
 *
 * @return 	If an error occurred -1 is returned, check errno for a more detailed error code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			When no error occurred the number of bytes written is returned.
 *              	Also, when Unsupported or Failed tag is encountered, the returned value is a
 * 			negative number, that contain the number of bytes for these two tags as absolute value.
 */
int oti_handle_tag( POtiReaderCb reader, OpCode code, OtiTag tag, const char* tx, size_t txLength, char* rx, size_t rxLength, char EnableTlvLen )
{
	int result = 0;

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		dprintf("  (oti_handle_tag) reader == NULL");
		result = -1;
	}
	else if ( (tx == NULL) && (txLength > 0 ) )
	{
		errno = EINVAL;
		dprintf("  (oti_handle_tag) (tx == NULL) && (txLength > 0 )");
		result = -1;
	}
	else if ( (rx == NULL) && (rxLength > 0 ) )
	{
		errno = EINVAL;
		dprintf("  (oti_handle_tag) (rx == NULL) && (rxLength > 0 )");
		result = -1;
	}
	else
	{
		char* rxBuf = reader->trx.rx;
		size_t rxLen = sizeof( reader->trx.rx );
		size_t off = 0;

		dprintf(" >>oti_handle_tag %X, length %d\n", tag, (int)txLength);
		result = oti_send_command( reader, code, tag, tx, txLength, EnableTlvLen );
		dprintf(" >> oti_send_command %d\n", result);

		/*-- on successful transmittal, get response --*/
		if ( result != -1 )
		{
			int go_on = 1;

			/*-- loop until we get a response for our tag --*/
			while ( go_on )
			{
				go_on = 0;
				off = 0;

				/*-- get response --*/
				memset( rxBuf, 0, rxLen );
				result = oti_wait_for_response( reader, &rxBuf, rxLen, reader->timeout.response );

				dprintf(" >> oti_wait_for_response %d\n", result);
				/*-- error or no error check the reply tag --*/
				if ( (result >= 2) || ((result < -1) && (errno == EPROTO) ))
				{
					/*-- check if this is a reply on our message --*/
					if ( _oti_check_reply_tag(tag, rxBuf, &off) == -1 )
					{
						errno = EBADRQC;
						dprintf("  (oti_handle_tag) replyTag != (unsigned short)tag");
						result = -1;

						go_on = 1;
					}
				}
			}
		}


		/*-- check received buffer, at least 1 byte tag and 1 byte length must be received --*/
		if ( result >= 2 || result <= -2)
		{
			/*-- calculate offset to length of data of the message --*/
			rxLen = oti_get_msg_length( rxBuf, &off );

			/*-- copy response into data buffer --*/
			if ( rxLen > rxLength ) rxLen = rxLength;
			if ( rxLen ) memcpy( rx, rxBuf+off, rxLen );

			if (result < 0)
				result = -rxLen;
			else
				result = rxLen;
		}
		/*-- if length is smaller then 2bytes but bigger or equal to 0, we didn't receive enough --*/
		else if ( result >= 0 )
		{
			errno = ENODATA;
			dprintf("  (oti_handle_tag) ( result >= 0 )");
			result = -1;
		}
	}

	dprintf("  (oti_handle_tag) returns %d \n", result);
	return ( result );
}


/**
 * This method will send a command to the OTI reader
 * and wait for the acknowledge message.
 *
 * @param reader		pointer to reader control block.
 * @param code			OpCode of the command to send.
 * @param tag			tag of the command.
 * @param data			pointer to data of the given command.
 * @param length		length of the data buffer.
 * @param EnableTlvLen	If true, TLV message contains length byte else not.
 *
 * @return 	If an error occurred -1 is returned, check errno for a more detailed error code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			When no error occurred the number of bytes written is returned.
 */
int oti_send_command( POtiReaderCb reader, OpCode code, OtiTag tag, const char* data, size_t length, char EnableTlvLen )
{
	POtiTransaction trx = NULL;
	int result = 0;

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( (data == NULL) && (length > 0) )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		int tlvLength = 0;
		int otiLength = 0;
		int len = 0;
		int tagLength = 0;
		int i;
		int retries = OTI_READER_RETRIES;
		char tagBuf[3];

		trx = &reader->trx;

		/*-- fillup tag buffer --*/
		if ( tag > 0x00FFFF ) 
			tagBuf[tagLength++] = ((tag & 0xFF0000) >> 16);
		if ( tag > 0x00FF ) 
			tagBuf[tagLength++] = ((tag & 0xFF00) >> 8);
		tagBuf[tagLength++] = (tag & 0xFF);

		while( retries )
		{
			/*-- build up tlv frame --*/
			tlvLength = oti_build_tlv_frame( trx->txTlv, tagBuf, tagLength, data, length, EnableTlvLen );
			otiLength = tlvLength + PROLOGUE_LENGTH + EPILOGUE_LENGTH;

			/*-- prepare the transmit buffer --*/
			BEGIN_TX( )

			/*-- set the message length --*/
			len = oti_set_msg_length( trx->tx+1, otiLength, 1 );
			for ( i=0 ; i<len ; i++ )
			{
				ADD_TX_BYTE( trx->tx[1+i] );
			}

			/*-- set unit and opcode --*/
			ADD_TX_BYTE( reader->unit )
			ADD_TX_BYTE( (char)code );

			for ( i=0 ; i<tlvLength ; i++ )
			{
				ADD_TX_BYTE( trx->txTlv[i] );
			}

			/*-- Round up the transaction buffer --*/
			END_TX( )

			/*-- send command to the OTI-reader --*/
#ifdef VBO_TESTS
{
int i;
for ( i = 0 ; i < trx->txLength ; i++ )
  printf( "%02x ", (unsigned char)trx->tx[i]);
printf( "\n");
}
#endif
			trx->txResult = _oti_transmit( reader, trx->tx, trx->txLength );

			/*-- all data has been send, wait for ack --*/
			if ( trx->txResult == trx->txLength )
			{
				char *data = trx->rx;
				memset( trx->rx, 0, sizeof( trx->rx) );
				trx->rxResult = _oti_receive( reader, &data, sizeof( trx->rx ), reader->timeout.ack );


				if ( trx->rxResult < 0 )
				{
					oti_perror( "ACK not received" );
					result = -1;
                                        trx->rxResult = -1;
					retries--;
				}
				else
				{
					result = trx->txResult;
					retries = 0;
				}
			}
			else
			{
				result = -1;
				retries--;
			}
		}
	}

	if( (result >= 0) && (code == DO) && (tag == OTI_POLL) && (*data >= OtiPollPendingAutoContinuously) &&
		(*data <= OtiPollPendingOnce) ) 
		reader->interruptible = INTERRUPTIBLE_PP;
	else
		reader->interruptible = 0;
	return ( result );
}




/**
 * This method will wait for the response of the last send command.
 *
 * @param reader	pointer to the reader control block.
 * @param data		pointer to pointer to the receival buffer.
 * @param length	length of the receivel buffer.
 * @param tmo		timeout value for the read action.
 *
 * @return 	This function will return -1 on error, check errno for a more detailed error code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			If this functions succeeds the length of the received data is returned.
 *		The data poiner is updated by the _oti_parse_tlv_response function
 */
int oti_wait_for_response( POtiReaderCb reader, char** data, size_t length, int tmo )
{
	int result = 0;

	/*-- sanity check --*/
	if (reader == NULL)
	{
		errno = EINVAL;
		dprintf(" >> (oti_wait_for_response)reader == NULL\n");
		result = -1;
	}
	else if ( (length > 0) && (data == NULL) )
	{
		errno = EINVAL;
		dprintf(" >> (oti_wait_for_response)(length > 0) && (data == NULL)\n");
		result = -1;
	}
	else
	{
		result = _oti_receive( reader, data, length, tmo );
		dprintf(" >> (oti_wait_for_response)_oti_receive returns %d\n", result);
	}

	return ( result );
}



/**
 * Helper function that will create the TLV frame for the OTI reader message.
 *
 * @param dest			pointer to destination buffer.
 * @param tag			pointer to TLV tag.
 * @param tagLength		length of the TLV tag.
 * @param data			pointer to data buffer.
 * @param length		length of the TLV data.
 * @param EnableTlvLen	If true, TLV message contains length byte else not.
 *
 * @return  On successfull completion the total length of the tlv frame is returned.
 * 			When an error occurred -1 is returned, check errno for a more detailed error code.
 */
int oti_build_tlv_frame( char* dest, const char* tag, size_t tagLength, const char* data, size_t length, char EnableTlvLen )
{
	int result = 0;
	int len = 0;

	/*-- sanity check --*/
	if ( dest == NULL || tag == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( (data == NULL) && (length > 0) )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		/*-- Copy Tag into destination buffer --*/
		memcpy( dest, tag, tagLength );
		len += tagLength;

		if (EnableTlvLen)
		{
			/*-- set the length of the TLV data --*/
			len += oti_set_msg_length( dest+len, length, 0 );
		}

		/*-- copy TLV into the destination buffer --*/
		memcpy( dest+len, data, length );
		len += length;

		result = len;
	}

	return ( result );
}




/**
 * Helper function that will set length in the given buffer<br>
 * <br>
 * <br>The length will be stored according to the following principle
 * <br>
 * <br>			 |   1st byte    |   2nd byte    |   3rd byte    |   4th byte    |   5th byte    |   N (decimal)
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br>	1 byte   | 0x00 to 0x7F  |               |               |               |               | 0 to 127
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 2 bytes  |     0x81      | 0x00 to 0xFF  |               |               |               | 0 to 255
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 3 bytes  |     0x82      |       0x0000 to 0xFFFF        |               |               | 0 to 65,535
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 4 bytes  |     0x83      |              0x000000 to 0xFFFFFF             |               | 0 to 16,777,215
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 5 bytes  |     0x84      |                   0x00000000 to 0xFFFFFFFF                    | 0 to 4,294,967,295
 * <br>
 *
 * @param dest		Pointer to the destination buffer, where the length must be stored.
 * 					Make sure the buffer is long enough !!!
 * @param value		Value that must be stored in buf.
 * @param include	0 when you don't want to add the number of bytes stored to the value field,
 * 					otherwise the number of bytes stored is added to the value field.
 *
 * @return 	In case of an error -1 is returned, check errno for a more detailed error code <br>
 * 			If no error occurred the number of bytes stored is returned.
 */
int oti_set_msg_length( char* dest, unsigned long value, char include )
{
	int length = 0;
	int result = 0;
	size_t total = value;

	/*-- sanity check --*/
	if ( dest == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		if ( include ) total++;
		if ( total <= 0x7F )
		{
			dest[length++] = (char)(total & 0x000000FF);

			return ( length );
		}

		if ( include ) total++;
		if ( total <= 0xFF )
		{
			dest[length++] = 0x81;
			dest[length++] = (char)(total & 0x000000FF);

			return ( length );
		}

		if ( include ) total++;
		if ( total <= 0xFFFF )
		{
			dest[length++] = 0x82;
			dest[length++] = (char)((total & 0x0000FF00) >> 8 );
			dest[length++] = (char)(total & 0x000000FF);

			return ( length );
		}

		if ( include ) total++;
		if ( total <= 0xFFFFFF )
		{
			dest[length++] = 0x83;
			dest[length++] = (char)((total & 0x00FF0000) >> 16 );
			dest[length++] = (char)((total & 0x0000FF00) >> 8 );
			dest[length++] = (char)(total & 0x000000FF);

			return ( length );
		}

		if ( include ) total++;
		if ( total <= 0xFFFFFFFF )
		{
			dest[length++] = 0x84;
			dest[length++] = (char)((total & 0xFF000000) >> 24 );
			dest[length++] = (char)((total & 0x00FF0000) >> 16 );
			dest[length++] = (char)((total & 0x0000FF00) >> 8 );
			dest[length++] = (char)(total & 0x000000FF);

			return ( length );
		}

		errno = E2BIG;
	}

	return ( result );
}



/**
 * Helper function that will get length from the given buffer<br>
 * <br>
 * <br>The length will be stored according to the following principle
 * <br>
 * <br>			 |   1st byte    |   2nd byte    |   3rd byte    |   4th byte    |   5th byte    |   N (decimal)
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br>	1 byte   | 0x00 to 0x7F  |               |               |               |               | 0 to 127
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 2 bytes  |     0x81      | 0x00 to 0xFF  |               |               |               | 0 to 255
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 3 bytes  |     0x82      |       0x0000 to 0xFFFF        |               |               | 0 to 65,535
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 4 bytes  |     0x83      |              0x000000 to 0xFFFFFF             |               | 0 to 16,777,215
 * <br> ---------|---------------|---------------|---------------|---------------|---------------|--------------------
 * <br> 5 bytes  |     0x84      |                   0x00000000 to 0xFFFFFFFF                    | 0 to 4,294,967,295
 * <br>
 *
 * @param value		pointer to buffer that contains the value.
 * @param offset	pointer to offset in the buffer where the value is located.
 * 					The offset will be updated for the length of the value field.
 *
 * @return 	In case of an error -1 is returned, check errno for a more detailed error code <br>
 * 			If no error occurred the number of bytes stored is returned.
 */
size_t oti_get_msg_length( const char* value, size_t* offset )
{
	size_t length = 0;
	int result = 0;

	/*-- sanity check --*/
	if ( value == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		size_t off = (offset == NULL) ? 0 : *offset;
		const char* val = value;

		switch( val[off] )
		{
			case (char)0x84:
			{
				off++;
				length |= ((val[off++]&0xFF) << 24);
				length |= ((val[off++]&0xFF) << 16);
				length |= ((val[off++]&0xFF) <<  8);
				length |= ((val[off++]&0xFF)      );

				break;
			}

			case (char)0x83:
			{
				off++;
				length |= ((val[off++]&0xFF) << 16);
				length |= ((val[off++]&0xFF) <<  8);
				length |= ((val[off++]&0xFF)      );

				break;
			}

			case (char)0x82:
			{
				off++;
				length |= ((val[off++]&0xFF) <<  8);
				length |= ((val[off++]&0xFF)      );

				break;
			}

			case (char)0x81:
			{
				off++;
				length = val[off++]&0xFF;

				break;
			}

			default:
			{
				length = val[off++]&0xFF;
				break;
			}
		}

		if ( offset != NULL ) *offset = off;
		result = length;
	}

	return ( result );
}



/**
 * Helper function that will send the data to the OTI reader.
 *
 * @param reader	pointer to the reader control block.
 * @param tx		pointer to the data that needs to be written.
 * @param txLen		length of the data that needs to be written.
 *
 * @return  If an error occurred while writing the data -1 is returned.
 * 			Otherwise the number of bytes written is returned.
 */


static int _oti_transmit_serial( POtiReaderCb reader, const char* data, size_t length )
{
	int result = 0;
	struct timeval tmo;

#ifdef OTISHOWIO
	size_t l;
	fprintf(stderr,"\x1b[31m%d:%s %d> ",GetMicroSecStamp(),__func__,length);
	for(l = 0 ; l < length ; l++ ) fprintf(stderr,"%02x",data[l]);
	fprintf(stderr,"\n\x1b[0m");
#endif
	pthread_mutex_lock( &reader->mutex );

	/*-- wait until device is ready to write to --*/
	if ( result >= 0 )
	{
		tmo.tv_sec = reader->timeout.write/1000;
		tmo.tv_usec = (reader->timeout.write%1000)*1000;

		FD_ZERO( &reader->wfds );
		FD_SET( reader->fd, &reader->wfds );
		result = select( reader->fd+1, NULL, &reader->wfds, NULL, &tmo );
		if ( FD_ISSET( reader->fd, &reader->wfds ) == 0 )
		{
			errno = ETIMEDOUT;
			oti_perror( "Unable to write to device" );
			result = -1;
		}
	}

	/*-- flush data that might be in te output stream --*/
	if ( result >= 0 )
	{
		result = tcflush( reader->fd, TCIOFLUSH );
	}

	/*-- write data to the output stream --*/
	if ( result != -1 )
	{
		result = write( reader->fd, data, length );
		oti_trace_hex(reader->oti, "write ==>", data, result);
	}

	pthread_mutex_unlock( &reader->mutex );

	return ( result );
}

#if defined INTERFACE_STM32
static int _oti_transmit_stm32( POtiReaderCb reader, const char* data, size_t length )
{
	int result = 0;

	pthread_mutex_lock( &reader->mutex );

	result = stm32_oti_write_data( reader->fd, ( unsigned char * )data, length );
    if( result >= 0 ) {
        result = length;
	oti_trace_hex(reader->oti, "stm32_write ==>", data, result);
    } else {
        dprintf( "stm32_oti_write_data : %d\n", result );
        errno = EIO;
        result = -1;
    }
		
	pthread_mutex_unlock( &reader->mutex );

	return ( result );
}
#endif

static int _oti_transmit( POtiReaderCb reader, const char* data, size_t length ){
#if defined INTERFACE_STM32
    if( reader->otiOpen.interfaceType == OtiReaderInterfaceTypeSerial )
        return _oti_transmit_serial( reader, data, length );
    else if ( reader->otiOpen.interfaceType == OtiReaderInterfaceTypeSTM32 ) {
        return _oti_transmit_stm32( reader, data, length );
    } else {
        dprintf( "Unknown interface type : %d\n", reader->otiOpen.interfaceType );
        return -1;
    }
#else
    return _oti_transmit_serial( reader, data, length );
#endif
}

/**
 * Helper function that will wait for data from the OTI-reader.
 *
 * @param reader	pointer to reader control block.
 * @param data		pointer to pointer to buffer where the data is stored.
 * @param length		size of receival buffer.
 * @param tmo		timeout to wait for the data in ms.
 *
 * @return  If an error occurred while receiving data -1 is returned, check errno for a more detailed code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			If a complete OTI message is received, the length of the message is returned.
 *                      The data pointer is updated by the _oti_parse_tlv_response function
 */


#if defined INTERFACE_STM32
static size_t _oti_receive_stm32( POtiReaderCb reader, char** data, size_t length, int timeout )
{
    POtiTransaction trx = NULL;
	int result = 0;
	size_t bytesToRead = 4;
	size_t bytesRead = 0;
    unsigned short rdLen;

	char buf[4];
	struct timeval tmo;
	int interval = (timeout >= 0 ) ? timeout : 30000;
	int t=0,remaining,interrupted=0;
	struct timespec ts;
	/*-- sanity check --*/
	if ( reader == NULL ){
		errno = EINVAL;
		result = -1;
	}
	else if ( (length > 0) && (data == NULL) ){
		errno = EINVAL;
		result = -1;
	} else {

		pthread_mutex_lock( &reader->mutex );

		tmo.tv_sec = interval / 1000;
		tmo.tv_usec = (interval % 1000) * 1000;

		trx = & reader->trx;

		for( ;; ) {

            if( trx->stmRxWr - trx->stmRxRd < bytesToRead ){

                /* not enough data in STM read buffer, read more data from STM OTI reader */

                if( timeout ){
                    for( ; trx->stmRxWr - trx->stmRxRd < bytesToRead; trx->stmRxWr += rdLen ){
                        result = stm32_oti_poll_request( reader->fd, interval );
                        if( result < 0 ){
                            dprintf( "stm32_oti_poll_request : %d\n", result );
                            result = -1;
                            errno = EIO;
                            break;
                        }

			FD_ZERO( &reader->rfds );
			FD_SET( reader->fd, &reader->rfds );
			if( reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) {
				FD_SET(reader->pfd[0],&reader->rfds);
			}
			result = select( reader->nfds, &reader->rfds, NULL, NULL, &tmo );
			remaining = tmo.tv_sec*1000 + tmo.tv_usec/1000;
			if ( FD_ISSET( reader->fd, &reader->rfds ) == 0 )
			{
				if( (reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) && 
					( FD_ISSET( reader->pfd[0], &reader->rfds))) {
					read(reader->pfd[0],buf,sizeof(buf));
					errno = ERESTART;
					/* Send stop and remove reply - ignore */
					result = stm32_oti_write_data( reader->fd, "\x02\x09\x00\x3e\xdf\x7d\x00\x97\x03", 9 );
					result = stm32_oti_poll_read( reader->fd, trx->stmRx+trx->stmRxWr, sizeof( trx->stmRx ) - trx->stmRxWr, & rdLen );
				} else
					errno = ETIMEDOUT;
				result = -1;
			}
			else
                        	result = stm32_oti_poll_read( reader->fd, trx->stmRx+trx->stmRxWr, sizeof( trx->stmRx ) - trx->stmRxWr, & rdLen );

                        if( result < 0 ){
                            dprintf( "stm32_oti_poll_read : %d\n", result );
                            result = -1;
                            errno = EIO;
                            break;
                        }
                    }
                } else {
                    result = stm32_oti_read_data( reader->fd, trx->stmRx+trx->stmRxWr, sizeof( trx->stmRx ) - trx->stmRxWr, & rdLen );
                    if( result < 0 ){
                        dprintf( "stm32_oti_read_data : %d\n", result );
                        result = -1;
                        errno = EIO;
                    } else
                        trx->stmRxWr += rdLen;
                }
                if( trx->stmRxWr - trx->stmRxRd < bytesToRead || result < 0 )
                    break;
            }
            
            memcpy( trx->rx+bytesRead, trx->stmRx+trx->stmRxRd, bytesToRead );
            trx->stmRxRd += bytesToRead;
            if( trx->stmRxRd >= trx->stmRxWr )
                    trx->stmRxRd = trx->stmRxWr = 0;
            if ( bytesRead == 0 ){
                /*-- check stx --*/
                if ( trx->rx[0] != STX ){
                    result = -1;
					errno = EINVAL;
					break;
                }
			}
            bytesRead += bytesToRead;
            bytesToRead = 0;

			/*-- if this is the prologue, calculate length of the field --*/
			if ( bytesRead == 4 ){
				/*-- get length --*/
				bytesToRead = oti_get_msg_length( trx->rx+1, NULL ) - 4;
			}

			/*-- Update result with the total amount of bytes read --*/
			if( bytesToRead == 0 ){
				/*-- update result code --*/
				result = bytesRead;
				trx->rxLength = bytesRead;

				/*-- check if return buffer is big enough --*/
				if ( bytesRead > length ){
					result = -1;
					errno = EOVERFLOW;
                    break;
				} else {
					/*-- copy data to return buffer if different from working buffer --*/
					result = _oti_parse_frame( reader, trx, data, length );
                    break;
				}
			}
		}

		if( reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) {
			reader->interruptible &= ~INTERRUPTIBLE_WAIT_CARD; 
			read(reader->pfd[0],buf,sizeof(buf));
		}
		if( result < 0 ){
			trx->stmRxRd = trx->stmRxWr = 0;
			stm32_oti_flush_data( reader->fd );
			/*-- This is need to keep the compatibility for STM devices 
			 * Last set for result can be done by "_oti_parse_frame" that 
			 * can return negative values less than -1. --*/
			result = -1;
		}
		pthread_mutex_unlock( &reader->mutex );
	}
	return ( result );
}
#endif

/* check basics of oti frame: stx, length, etx */
/* return: 
 * 0 : no frame in yet
 * size > 0 : frame in of size size
 * < 0 : error in input
 * 	-'S': start pattern wrong
 * 	-'F': frame error in size bytes
 * 	-'E': end pattern wrong ( may be size )
 * 	-'L': lrc error
 */
static int oti_framer(char *pD, unsigned int size, unsigned int *offsetTag, unsigned int *offsetContext)
{
	unsigned char l1, l2, l3, lrc=0;
	unsigned int i,j=3,flen=0;
	if( (size >= 1) && ((*pD) != 0x02) ) return -'S';
	if( size >= 2 )  {
		l1 = (unsigned char)(*(pD+1));
		l2 = (unsigned char)(*(pD+2));
		l3 = (unsigned char)(*(pD+3));
		if( (l1 == 0x80) || (l1 > 0x82) ) return -'F';
		else if( l1 < 0x80 ) flen = l1;
		else if( (l1 == 0x81) && (size >= 3) ) flen = l2;
		else if( (l1 == 0x82) && (size >= 4) ) flen = (l2<<8)|l3;
	}
	if( (flen > 0) && (size >= flen)) {
		if(*(pD+flen-1) == 0x03) {
			for(i = 0 ; i < (flen-2) ; i++ )
                                lrc ^= pD[i];
                       	if( lrc == (unsigned char)pD[i] ) {
				i=7;
				if( l1 > 0x80 ) {
					j += (l1&3);
					i = j+4+ ((pD[j+3] > 0x80) ? pD[j+3]&0x03 : 0);
				}
				*offsetContext=j;
				if( (i+5) <= flen ) *offsetTag=i;
				else *offsetTag = 0;
			        return flen;
			}
			return -'L';

		} else return -'E';
	}
	return 0;
} 

void * oti_thread (void * arg)
{
	struct timeval tmo;
	POtiReaderCb reader = (POtiReaderCb)arg;
	int result=0,tresult=0,timeout=100;	/* default idle timeout for now */
	fd_set rfds;
	int nfds = 1 +  ( (reader->fd > reader->wpfd[0]) ? reader->fd : reader->wpfd[0] );
	char buf[2048];
	char buf2[32];
	unsigned i=7, j=3, rin=0, rout=0, rcnt=0;
	int room=0;
#ifdef OTISHOWIO
	int r;
#endif

	for(;;) {
		while( rcnt > 0 ) {
			if( ((result=tresult) > 0) || (result = oti_framer(&buf[rout],rcnt,&i,&j)) > 0) {
				if( tresult > 0 ) tresult=0;
				/* need to check for external display messages */
#ifdef OTISHOWIO
				if( result > 8) fprintf(stderr,"%s Tag[%d]=%02x%02x Context[%d]=%02x\n",__func__,i,buf[rout+i],buf[rout+i+1],j,buf[rout+j]);
				else fprintf(stderr,"%s ACK Context[%d]=%02x\n",__func__,j,buf[rout+j]);
#endif
				if( ((result <= 8) || memcmp(&buf[rout+i],"\xdf\x46",2)) || (buf[rout+j] != DO) || (buf[rout+i+2] == 0) ) {
					if( room > 0 ) {
						room = 0;
						memcpy(reader->trx.rx,&buf[rout],result);				/* move frame to raw location */
						reader->trx.rxLength = result;
						buf2[0] = 'A';
						buf2[1] = result&0xff;
						buf2[2] = (result>>8)&0xff;
						write(reader->rpfd[1],buf2,3);
#ifdef OTISHOWIO
						fprintf(stderr,"\x1b[32m%d:%s %d tagofs=%d< ",GetMicroSecStamp(),__func__,result,i);
						for( r = 0 ; r < result ; r++ ) fprintf(stderr,"%02x",buf[rout+r]);
						fprintf(stderr,"\x1b[0m\n");
#endif
						rcnt -= result;
						rout += result;
					} else { 
						tresult=result; 
						break; 
					}
				} 
				else {
					if( reader->edm_callback != NULL ) {
						reader->edm_callback(&buf[rout+i],result-(i+2));
					}
#ifdef OTISHOWIO
					else {
						fprintf(stderr,"%d:%s _____ dumping external display message tag offset=%d ",GetMicroSecStamp(),__func__,i);
						for( i = 0 ; i < result ; i++ ) fprintf(stderr,"%02x",buf[rout+i]);
						fprintf(stderr,"\n");
					}
#endif
					rcnt -= result;
					rout += result;
				}
			} else if (result < 0) {
				/* trow away or pass down for further handling: the latter keeps things as before */
				for( i=1 ; (i < rcnt) && (buf[rout+i] != 0x02) ; i++ );		
				memcpy(reader->trx.rx,&buf[rout],i);
				reader->trx.rxLength = i;
				buf2[0] = -result;
				write(reader->rpfd[1],buf2,1);
				rcnt -= i;
				rout += i;
			}
			if( rcnt == 0 ) rin = rout = 0;
			if( result == 0 ) break;	
		}
		tmo.tv_sec = timeout / 1000;
		tmo.tv_usec = (timeout % 1000) * 1000;
		FD_ZERO( &rfds );
		FD_SET( reader->wpfd[0], &rfds);

		FD_SET( reader->fd, &rfds );
		result = select( nfds, &rfds, NULL, NULL, &tmo );
		if( result > 0 ) {
			if ( FD_ISSET( reader->fd, &rfds ) ) {
				if( (result = read(reader->fd,&buf[rin],sizeof(buf)-rin)) > 0) {
					rin += result;
					rcnt += result;
				}
			}
			if ( FD_ISSET( reader->wpfd[0], &rfds ) ) {
				result = read(reader->wpfd[0],buf2,sizeof(buf2));
				if( result > 0) {
					if( buf2[0] == 'S' ) break;
					if( buf2[0] == 'R' ) room = 1;
				}
			}
		}
	}
	dprintf("%s stopping...\n",__func__);
	return NULL;
}

static size_t _oti_receive_serial( POtiReaderCb reader, char** data, size_t length, int timeout )
{
	struct timeval tmo;

    POtiTransaction trx = NULL;
	int result = 0;
	int go_on = 1;
	size_t bytesToRead = 4;
	size_t bytesRead = 0;

	char buf[8];

	if( isCbMode() )
		write(reader->wpfd[1],"R",1);	/* barrier */

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		dprintf(" >>_oti_receive reader == NULL\n");
		result = -1;
	}
	else if ( (length > 0) && (data == NULL) )
	{
		errno = EINVAL;
		dprintf(" >>_oti_receive (length > 0) && (data == NULL)\n");
		result = -1;
	}
	else
	{
		dprintf(" >>_oti_receive length = %d\n", (int)length);
		pthread_mutex_lock( &reader->mutex );

		trx = &reader->trx;

		/*-- set the timeout and wait for data if wanted --*/
		if ( timeout > 0 )
		{
			tmo.tv_sec = timeout / 1000;
			tmo.tv_usec = (timeout % 1000) * 1000;

//			result = select( reader->nfds, &reader->rfds, NULL, NULL, &tmo );
//			if ( FD_ISSET( reader->fd, &reader->rfds ) == 0 )
//			{
//				errno = ETIMEDOUT;
//				result = -1;
//				go_on = 0;
//			}
		}

		/*-- enter loop that will read data from the communication line --*/
		while ( go_on )
		{
			/*-- read data from the OTI reader --*/
	if( isCbMode() ) {
			result = 0;
	} else {
//#if 0
			result = read( reader->fd, trx->rx+bytesRead, bytesToRead );

			/*-- parse newly received data --*/
			if ( result > 0 )
			{
				oti_trace_hex(reader->oti, "read <==", trx->rx+bytesRead, result);

				/*-- check validity start of frame --*/
				if ( bytesRead == 0 )
				{
					/*-- check stx --*/
					if ( trx->rx[0] != STX )
					{
						result = -1;
						errno = EINVAL;
						go_on = 0;
						dprintf(" >>_oti_receive go_on 0, no STX\n");
						break;
					}
				}

				bytesRead += result;
				bytesToRead -= result;

				/*-- if this is the prologue, calculate length of the field --*/
				if ( bytesRead == 4 ) {
					/*-- get length --*/
					bytesToRead = oti_get_msg_length( trx->rx+1, NULL ) - 4;
				}
			} else if (result == 0) {
				result = -1;
				break;
			}
	}
//#endif

			/*-- wait for new data if more is needed --*/
			if ( timeout != 0 )
			{
				if ( ((result == -1) && (errno == EAGAIN)) ||
						((result == 0) && (bytesToRead > 0)) )
				{

					FD_ZERO( &reader->rfds );
					FD_SET( isCbMode() ? reader->rpfd[0] : reader->fd, &reader->rfds );

					if( reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) {
						FD_SET(reader->pfd[0],&reader->rfds);
					}
					result = select( reader->nfds, &reader->rfds, NULL, NULL, (timeout < 0) ? NULL : &tmo );
					if( !isCbMode() ) dprintf(" >>_oti_receive select result %d\n", result);
					if ( FD_ISSET( isCbMode() ?  reader->rpfd[0] : reader->fd, &reader->rfds ) == 0 )
					{
						if( (reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) && 
							( FD_ISSET( reader->pfd[0], &reader->rfds))) {
							read(reader->pfd[0],buf,sizeof(buf));
							errno = ERESTART;
						} else
							errno = ETIMEDOUT;
						result = -1;
						go_on = 0;
						dprintf(" >>_oti_receive go_on 0, Timeout\n");

						break;
					} else if( isCbMode() && ((result = read( reader->rpfd[0],buf,sizeof(buf))) >= 3) ) {
						if( buf[0] == 'A' ) {
							bytesToRead = 0;
							bytesRead=buf[1]|(buf[2]<<8);
							errno = 0;
						} else {
							errno = EINVAL;
							go_on = 0;
						}
					}
				}
			}
			/*-- exit loop when the timeout is 0 --*/
			else
			{
				go_on = 0;
				dprintf(" >>_oti_receive go_on 0, timeout == 0\n");

				break;
			}

			/*-- Update result with the total amount of bytes read --*/
			if ( (bytesToRead == 0) && (result >= 0) )
			{
				/*-- update result code --*/
				result = bytesRead;
				if( isCbMode() )
					oti_trace_hex(reader->oti, "read <==", trx->rx, result);
				else
					trx->rxLength = bytesRead;

				/*-- check if return buffer is big enough --*/
				if ( bytesRead > length )
				{
					result = -1;
					dprintf(" >>_oti_receive bytesRead > length\n");
					errno = EOVERFLOW;
					break;
				}
				else
				{
					/*-- copy data to return buffer if different from working buffer --*/
					result = _oti_parse_frame( reader, trx, data, length );
					go_on = 0;
					dprintf(" >>_oti_receive go_on 0, after _oti_parse_frame, result = %d\n", result);

					break;
				}
			}
		}
		if( reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) {
			reader->interruptible &= ~INTERRUPTIBLE_WAIT_CARD; 
			read(reader->pfd[0],buf,sizeof(buf));
		}
/* this would never occur == -1 and >= 0 ??? Remove it for now */
#if 0
		if( result == -1 && errno == ETIMEDOUT )
		{
			/*-- flush data that might be in te output stream --*/
			if ( result >= 0 )
			{
				result = tcflush( reader->fd, TCIFLUSH );
				dprintf(" >>_oti_receive tcflush result %d\n", result);
			}
		}
#endif
		pthread_mutex_unlock( &reader->mutex );
	}
	return ( result );
}

static size_t _oti_receive( POtiReaderCb reader, char** data, size_t length, int timeout ){
    #if defined INTERFACE_STM32
    if( reader->otiOpen.interfaceType == OtiReaderInterfaceTypeSerial )
        return _oti_receive_serial( reader, data, length, timeout );
    else if ( reader->otiOpen.interfaceType == OtiReaderInterfaceTypeSTM32 ) {
        return _oti_receive_stm32( reader, data, length, timeout );
    } else {
        dprintf( "Unknown interface type : %d\n", reader->otiOpen.interfaceType );
        return -1;
    }
    #else
    return _oti_receive_serial( reader, data, length, timeout );
    #endif
}

/**
 * Helper function that will check the receival buffer and extract the data from it.
 * If the received block is valid, the extracted data will be copied into the data buffer.
 *
 * @param trx		pointer to transaction buffer.
 * @param data		pointer to pointer to data buffer.
 * @param length	maximum length of the data buffer
 *
 * @return 	When an error occurred -1 is returned check the errno code for a detailed error code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			If no error occurred the length of the data is stored.
 *                      The data pointer is updated by the _oti_parse_tlv_response function
 */
static int _oti_parse_frame( POtiReaderCb reader, POtiTransaction trx, char** data, size_t length )
{
	int result = 0;

	/*-- sanity check --*/
	if ( trx == NULL)
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( (length > 0) && (data == NULL) )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char* rx = trx->rx;
		int rxLen = trx->rxLength;
		char rxLrc = rx[rxLen-2];
		int offsetOpCode = 3;
		char opCode = 0;

		/*-- check the LRC --*/
		if ( result >= 0 )
		{
			result = _oti_check_lrc( rx, rxLen-2, rxLrc );
		}

		/*-- check the opcode of the message --*/
		if ( result >= 0 )
		{
			/*-- get the OpCode of the message --*/
			if ( ( rx[1] & 0x80 ) == 0x80 ) offsetOpCode += (rx[1] & 0x0F);
			opCode = rx[offsetOpCode];
			/*-- PCD to Host NAK --*/
			if ( opCode == NAK )
			{
				/*-- get OTI error code --*/
				errno = EOTI_NAK | rx[4];
				result = -1;
				dprintf("(_oti_parse_frame) NAK received\n");
				oti_perror( "NAK" );
			}
		}

		/*-- check the PCD th Host return message --*/
		if ( result >= 0 )
		{
			int offsetData = offsetOpCode + 1;
			size_t lengthData = rxLen - offsetData - EPILOGUE_LENGTH;

			if ( lengthData > length ) lengthData = length;

			/*-- PCD to Host Ack --*/
			if ( rx[1] == 0x08 )
			{
//				if ( lengthData ) memcpy( data, rx+offsetData, lengthData );
				if ( lengthData ) *data = rx+offsetData;
			}
			/*-- PCD to Host Response (After ACK) --*/
			else
			{
				/*-- Parse tlv response --*/
				result = _oti_parse_tlv_response( reader, rx+offsetData, data, length );
				dprintf("(_oti_parse_frame) _oti_parse_tlv_response return %d\n", result);
			}
		}
	}


	return ( result );
}


/**
 * Helper function that will check the TLV buffer and extract the data from it.
 * If the TLV block is valid, the extracted data will be copied into the data buffer.
 *
 * @param tlv		pointer to TLV buffer.
 * @param data		pointer to pointer to data buffer.
 * @param length	maximum length of the data buffer
 *
 * @return 	When an error occurred -1 is returned check the errno code for a detailed error code.
 * 			The errno variable can contain an OTI specific error code (0x1xxx).
 * 			If no error occurred the length of the data is stored.<br>
 * 			ENOTSUP : Tag and length of Unsupported instruction can be found in data buffer.<br>
 * 			EFAULT  : Tag and length of Failed instruction can be found in data buffer.<br>
 *                      The data pointer is updated by _oti_parse_tlv_response with the beginning of the frame content
 */
static int _oti_parse_tlv_response( POtiReaderCb reader, const char* tlv, char** data, size_t length )
{
	int result = -1;

	/*-- sanity check --*/
	if ( tlv == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( (length > 0) && (data == NULL) )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		size_t off = 2;
		unsigned short tlvCode = ( ((tlv[0] << 8) & 0xFF00) | tlv[1] );
		size_t tlvLength = oti_get_msg_length( tlv, &off );

		/*-- set length that needs to be copied --*/
		if ( tlvLength > length ) tlvLength = length;
		if ( tlvLength ) *data = (char *)tlv+off;

		/*-- check the message based on the rest of the message --*/
		switch ( tlvCode )
		{
			case TLV_RESPONSE_SUCCEEDED:
			{
//				if ( tlvLength ) memcpy( data, tlv+off, tlvLength );

				result = tlvLength;

				break;
			}

			case TLV_RESPONSE_UNSUPPORTED:
			{
				oti_trace_hex( reader->oti, "TLV Response Unsupported", tlv, tlvLength+off );

//				if ( tlvLength ) memcpy( data, tlv+off, tlvLength );

				result = -tlvLength;//-1;
				errno = ENOTSUP;

				break;
			}

			case TLV_RESPONSE_FAILED:
			{
				oti_trace_hex( reader->oti, "TLV Response Failed", tlv, tlvLength+off );

//				if ( tlvLength ) memcpy( data, tlv+off, tlvLength );

				result = -tlvLength;//-1;
				errno = EPROTO;
				break;
			}

			default:
			{
				//oti_trace_hex( reader->oti, "TLV Response default", tlv, tlvLength+off );
				result = -1;
				errno = ENOSYS;
				break;
			}
		}
	}

	return ( result );
}



/**
 * Helper function that will check the lrc over the data buffer.
 *
 * @param data		pointer to the data over which an LRC needs to be computed.
 * @param length	length of the data buffer.
 * @param lrc		LRC that is expected.
 *
 * @result 	-1 is returned when an LRC mismatch has occurred.
 * 			0 is returned when the LRC is correct.
 */
static int _oti_check_lrc( const char* data, size_t length, char lrc )
{
	int result = 0;
	size_t i;
	char calcLrc = 0;

	/*-- calculcate the lrc --*/
	for ( i=0 ; i<length ; i++ ) calcLrc ^= data[i];

	if ( lrc != calcLrc )
	{
		errno = EOTI | EOTI_NAK_LRC;
		result = -1;
	}

	return ( result );
}

static int _oti_check_reply_tag(OtiTag commandTag, char *rxBuf, size_t *offset)
{
	int err = 0;
	OtiTag replyTag = 0;
	replyTag |= rxBuf[(*offset)++]&0xFF;
	if ((replyTag & 0x1F) == 0x1F)
	{
		// tag length has at least 2 bytes:
		replyTag = (replyTag << 8) & 0x0000FF00;
		replyTag |= rxBuf[(*offset)++]&0xFF;    
		// if new added byte is 0x81, 0x82 or 0xa1, then tag has 3 bytes.
		if (((replyTag & 0x0000FF) == 0x81) || ((replyTag & 0x0000FF) == 0x82) || ((replyTag & 0x0000FF) == 0x83) || ((replyTag & 0x0000FF) == 0xA1)) 
		{
			replyTag = (replyTag << 8) & 0x00FFFF00;
			replyTag |= rxBuf[(*offset)++]&0xFF;    
		}
	}
	/*-- check if this is a reply on our message --*/
	/*-- the response tag is different in case of the NDoT & EMV calls --*/
	dprintf("tag = %08X\n", commandTag);
	dprintf("reply-tag = %08X\n", replyTag);
	if (commandTag == OTI_NDOT_POLL)        // 0xDF810A
	{
		if ((replyTag != OTI_RESULT_TREMINATE_TEMPLATE) &&  // 0xF2
		    (replyTag != OTI_NDOT_POLL_REPLY))              // 0xF4
		{
			err = -1;
		}
	}
	else if (commandTag == OTI_NDOT_EMV)    // 0xF4
	{
		if ((replyTag != OTI_RESULT_TREMINATE_TEMPLATE) &&  // 0xF2
		    (replyTag != OTI_NDOT_EMV_REPLY) &&             // 0xFC
		    (replyTag != OTI_NDOT_POLL_REPLY))              // 0xF4
		{
			err = -1;
		}
	}
	else if (commandTag == OTI_POLL_EMV)    // 0xFD
	{
		if ((replyTag != OTI_RESULT_TREMINATE_TEMPLATE) &&  // 0xF2
		    (replyTag != OTI_POLL_EMV_REPLY) &&             // 0xFC
		    (replyTag != OTI_NDOT_POLL_REPLY))              // 0xF4
		{
			err = -1;
		}
	}
#if 0
	else if (commandTag == 0xDF8157)
	{
		if( (replyTag != 0xDF8157) &&
			(replyTag != 0xDF8153) ) 
		{
			err = -1;
		}
	}
#endif
	else if (replyTag != commandTag)
	{
		err = -1;
	}

	return err;
}

