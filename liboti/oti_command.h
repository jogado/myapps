/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_command.h
 *
 *  Created on: Oct 14, 2008
 *      Author: jvolckae
 */

#ifndef _PRODATA_SYSTEMS_OTI_COMMAND_H_
#define _PRODATA_SYSTEMS_OTI_COMMAND_H_ 1

#include "oti_reader.h"

/*-- definitions --*/
#define	STX			0x02
#define ETX			0x03

#define	PROLOGUE_LENGTH		3	/*! Length of the prologue STX + Unit + OpCode */
#define EPILOGUE_LENGTH 	2	/*! Length of the epilogue LRC + ETX */

/*-- Macro's --*/
/**
 * @macro	BEGIN_TX
 *
 * Initialize the transmit variables to begin new transmital buffer.
 */
#define BEGIN_TX( )								\
	memset( trx->tx, 0, sizeof( trx->tx ) );	\
	trx->txLength = 0;							\
	trx->tx[trx->txLength++] = STX;				\
	trx->txLrc = STX;

/**
 * @macro	ADD_TX_BYTE
 *
 * Add new byte to the transmittal buffer.
 */
#define ADD_TX_BYTE( byte )					\
	trx->tx[trx->txLength++] = byte;		\
	trx->txLrc ^= byte;
/**
 * @macro	END_TX
 *
 * Round up the transmittal buffer preparing it to be sent.
 */
#define END_TX( )							\
	trx->tx[trx->txLength++] = trx->txLrc;	\
	trx->tx[trx->txLength++] = ETX;

#if 0
/**
 * @enum OpCode
 *
 * Enumeration of OpCodes known by the OTI reader.
 */
typedef enum
{
	SET	= 0x3C,
	GET = 0x3D,
	DO = 0x3E,
	NAK = 0x15
} OpCode;
#endif

typedef enum
{
	TLV_RESPONSE_SUCCEEDED		= 0xFF01 ,
	TLV_RESPONSE_UNSUPPORTED	= 0xFF02 ,
	TLV_RESPONSE_FAILED			= 0xFF03

} TlvCode;

/*-- Function declarations --*/
int oti_handle_tag( POtiReaderCb reader, OpCode code, OtiTag tag, const char* tx, size_t txLength, char* rx, size_t rxLength, char EnableTlvLen );
int oti_send_command( POtiReaderCb reader, OpCode code, OtiTag tag, const char* data, size_t length, char EnableTlvLen );
int oti_wait_for_response( POtiReaderCb reader, char** data, size_t length, int tmo );
int oti_build_tlv_frame( char* dest, const char* tag, size_t tagLength, const char* data, size_t length, char EnableTlvLen );
int oti_set_msg_length( char* dest, unsigned long value, char include );
size_t oti_get_msg_length( const char* value, size_t* offset );
int check_oti_handle_tag_answer(int answer);

#endif /* _PRODATA_SYSTEMS_OTI_COMMAND_H_ */
