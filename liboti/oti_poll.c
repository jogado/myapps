/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_poll.c
 *
 *  Created on: Oct 21, 2008
 *      Author: jvolckae
 */

#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "oti_poll.h"
#include "oti_trace.h"


/**
 * This method will initiate the polling for a card.
 *
 * @param reader	pointer to the reader control block.
 * @param code		operational code that needs to be performed (GET - SET - DO).
 * @param mode		pointer to the mode in which the polling will take place.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_poll_start(POtiReaderCb reader, OpCode code, OtiPollMode* mode)
{
	int result = 0;

	/*-- sanity check --*/
	if ( mode == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char data[1] = { 0 };

		/*-- Get the current applications that are enabled --*/
		if ( code == GET )
		{
			result = oti_handle_tag( reader, GET, OTI_POLL, NULL, 0, data, sizeof( data ), OtiTlvSizeAdd );
			result = check_oti_handle_tag_answer(result);
			/*-- on success copy result in appli buffer --*/
			if ( result != -1 )
			{
				*mode = (OtiPollMode)data[0];
			}
		}
		/*-- DO or SET the applications --*/
		else if ( code == SET )
		{
			data[0] = (char)*mode;
			result = oti_handle_tag( reader, SET, OTI_POLL, data, sizeof( data ), NULL, 0, OtiTlvSizeAdd );
			result = check_oti_handle_tag_answer(result);
		}
		else
		{
			/*-- start poll sequence --*/
			data[0] = (char)*mode;
			result = oti_send_command( reader, DO, OTI_POLL, data, sizeof( data ), OtiTlvSizeAdd );
		}
	}

	return ( result );
}

/**
 * This method will wait for a card within a given timeout.
 *
 * @param reader	pointer to the reader control block.
 * @param cardinfo	pointer to card information of the detected card.
 * @param timeout	maximum time to wait for a card.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			The number of bytes stored in the data buffer is returned
 * 			when the function succeeded.
 *
 * The _extended version of this routine handles polls OtiPollPendingOnceExt and OtiPollImmediateExt.
 * These polls were added to combine the default poll methods with extra polls for tickets like SRI.
 * These extra polls are implemented by using the TransparentData commands
 * The Immediate version is the simplest one: if the OtiPollImmediate is replying no card is detected,
 * then a poll for the SRI tickets is sent. The reply is build based on the outcome of either of these
 * two calls.
 * The PendingOnce version is a loop around the Immediate version for the timeout that is being passed
 * The activating_poll in the pollConfig defines which poll actually activates the card - if type A or B
 */
int oti_wait_for_card_extended( POtiReaderCb reader, POtiCardInfo cardinfo, int timeout )
{
	int result = 0;
	int elapsed = 0;
	int state=0;
        struct timespec time_start,time_now; 
	clock_gettime(CLOCK_MONOTONIC,&time_start);
	OtiPollMode pollMode = OtiPollImmediate;
	unsigned char tx[64],rx[64];
	int i,txLen,result2=0;
	int configured=0;	/* before sending first transparent command, need to set correct mode */

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( cardinfo == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		for( ; (elapsed < timeout) || (timeout == -1) ; ) {
			char* rxBuf = reader->trx.rx;
			size_t rxLen = sizeof( reader->trx.rx );

			memset(rxBuf,0,16); /* Make sure only new data is evaluated in case of error */

			/*-- wait for response --*/
			if( (timeout != 0) && (reader->interruptible == INTERRUPTIBLE_PP) )
				reader->interruptible |= INTERRUPTIBLE_WAIT_CARD;
			result = oti_wait_for_response( reader, &rxBuf, rxLen, (pollMode == OtiPollImmediate) ? timeout : 100 );
                	clock_gettime(CLOCK_MONOTONIC,&time_now);
                	elapsed = (time_now.tv_sec - time_start.tv_sec)*1000 + (time_now.tv_nsec - time_start.tv_nsec)/1000000;

			if (result < 0)
				result = -1;

			/*-- check received buffer --*/
			if ( result != -1 )
			{
				size_t off = 0;
				unsigned short replyTag = (rxBuf[off++]&0xff);

				/*-- check if this is a reply for card data --*/
				if ( replyTag != OTI_POLL_ANSWER )
				{
					errno = EBADRQC;
					result = -1;
				}
				else
				{
					/*-- calculate offset to length of data of the message --*/
					rxLen = oti_get_msg_length( rxBuf, &off );
	
					if (result != -1)
					{
						result = oti_copy_data_2_tag_struct( &rxBuf[off], rxLen, cardinfo);
					}
				}
			}  
			/* SD-890 if it is a poll answer with collision error - set EUSERS */
			else {
				 if( !memcmp(rxBuf,"\xdf\x7e\x01\x06",4) || !memcmp(rxBuf,"\xf2\x04\xdf\x68\x01\x06",6) )
					errno = EUSERS;	
			}
			/* break out if we already have a result or if time elapsed */
			if( result > 0 ) {
				if( (reader->pollMode == OtiPollImmediateExt) || (pollMode == reader->pollConfig.activating_poll) ) {
					if( (reader->pollConfig.tlc_appli == OtiTransparentLayerPcd) && (reader->pollConfig.tlc == OtiTransparentLayerHost)){
						result2 =  oti_transparent_layer_control( reader, reader->pollConfig.tlc = OtiTransparentLayerPcd );
					}
					reader->piccType = cardinfo->piccType;
			       		break;
				} else {
					pollMode = reader->pollConfig.activating_poll;
					if( (result2 = oti_poll_start( reader, DO, &pollMode )) < 0 ) break;
					continue;
				}
			} else pollMode = OtiPollImmediate;
			if( ( elapsed >= timeout ) && (timeout != -1) ) break;
			if( configured == 0 ) {
				/* make host responsible otherwise pcd adds prologue field */
				if( (reader->pollConfig.tlc_appli == OtiTransparentLayerPcd) && (reader->pollConfig.tlc == OtiTransparentLayerPcd) ) {
					if( (result2 =  oti_transparent_layer_control( reader, reader->pollConfig.tlc = OtiTransparentLayerHost )) < 0 ) break;
				}
				tx[0] = reader->pollConfig.fwti_poll; /* this should give ~ 5 msec frame waiting time - max. time in datasheet is 7 msec for write */
						/* but: write doesn't send reply so code does a read after the write */	
				if( (result2 = oti_reader_command( reader, DO, OTI_RF_PARAM_FWTI, tx, 1, rx, sizeof(rx))) < 0 ) break;

				configured = 1;
			}
			do {
				if( reader->pollConfig.pre_rf_on ) {
					tx[0] = 1;	/* activate antenna field is required with ARM_OPMT_040506_3h - unfortunately */
					if( (result2 = oti_rf_antenna( reader, DO, tx)) < 0 ) break;
				}
				/* transparent poll to SRI and ... other tickets ? */
				memcpy(tx,"\x06\x00",txLen=2);
				result2 = oti_transparent_command( reader, tx, txLen, rx, sizeof(rx) );
				if( result2 <= 0 ) break;
				tx[0] = SR_SELECT;	/* SR SELECT */
				tx[1] = rx[0];
				result2 = oti_transparent_command( reader, tx, 2, rx, sizeof(rx) );
				if( result2 <= 0 ) break;
				reader->chip_ID_SR = rx[0];	/* this is required for removal procedure */
				tx[0] = SR_GET_UID;	/* Get UID */
				result2 = oti_transparent_command( reader, tx, 1, rx, sizeof(rx) );
				if( result2 == 8 ) {
					result = result2;
					break;
				}
				/* If we got here but failed the get UID command, we may have a SR176 that doesn't know this command */
				/* Read the first 4 blocks is the same as getUID */
				txLen = 2;
				tx[0] = SR_READ_BLOCK; /* Read Block */
				for( i = 0 ; i < 4 ; i++ ) {
					tx[1] = i;
					if ((result2 = oti_transparent_command( reader, tx, txLen, &rx[i*2], 2 )) < 0 ) break;
				}
				if( result2 == 2 ) result = 8;
			} while ( 0 );	/* we could also do a couple of trial loops */
			if( result == 8 ) {
				memset( cardinfo, 0, sizeof(OtiCardInfo) );
				cardinfo->transactionType = -1;
				cardinfo->capki = -1;
				cardinfo->transactionResult = -1;
				cardinfo->dataChannel = -1;
				reader->piccType = cardinfo->piccType = 'T';	/* T for Ticket */
				cardinfo->length_pupi = result;
				memcpy(cardinfo->data,rx,result);
				cardinfo->pupi = cardinfo->data;
				result = sizeof( OtiCardInfo );
			} else {
				if( reader->pollConfig.post_rf_off ) {
					tx[0] = 0;	/* because we activated antenna field for ARM_OPMT_040506_3h we also need to deactivate it */
					oti_rf_antenna( reader, DO, tx);
				}
				if( reader->pollConfig.no_ticket_sleep > 0 )
					usleep(reader->pollConfig.no_ticket_sleep*1000);
			}
			if( result > 0 ) break;
			if( reader->pollMode == OtiPollImmediateExt ) break;	/* do a single cycle for the PollImmediate version */
			/* repeat oti_poll_start for pending polls */
			if( (result = oti_poll_start( reader, DO, &pollMode )) < 0 ) break;
		}
		if( (result < 0) && (reader->pollMode == OtiPollPendingOnceExt) && (errno == EPROTO) ) errno = EAGAIN;
	}
	return ( result );
}

/**
 * This method will wait for a card within a given timeout.
 *
 * @param reader	pointer to the reader control block.
 * @param cardinfo	pointer to card information of the detected card.
 * @param timeout	maximum time to wait for a card.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			The number of bytes stored in the data buffer is returned
 * 			when the function succeeded.
 */
int oti_wait_for_card( POtiReaderCb reader, POtiCardInfo cardinfo, int timeout )
{
	int result = 0;

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ( cardinfo == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char* rxBuf = reader->trx.rx;
		size_t rxLen = sizeof( reader->trx.rx );

		memset(rxBuf,0,16); /* Make sure only new data is evaluated in case of error */

		/*-- wait for response --*/
		if( (timeout != 0) && (reader->interruptible == INTERRUPTIBLE_PP) )
			reader->interruptible |= INTERRUPTIBLE_WAIT_CARD;
		result = oti_wait_for_response( reader, &rxBuf, rxLen, timeout );
		if (result < 0)
			result = -1;

		/*-- check received buffer --*/
		if ( result != -1 )
		{
			size_t off = 0;
			unsigned short replyTag = (rxBuf[off++]&0xff);

			/*-- check if this is a reply for card data --*/
			if ( replyTag != OTI_POLL_ANSWER )
			{
				errno = EBADRQC;
				result = -1;
			}
			else
			{
				/*-- calculate offset to length of data of the message --*/
				rxLen = oti_get_msg_length( rxBuf, &off );

				if (result != -1)
				{
					result = oti_copy_data_2_tag_struct( &rxBuf[off], rxLen, cardinfo);
				}
			}
		}  
		/* SD-890 if it is a poll answer with collision error - set EUSERS */
		else {
			 if( !memcmp(rxBuf,"\xdf\x7e\x01\x06",4) || !memcmp(rxBuf,"\xf2\x04\xdf\x68\x01\x06",6) )
				errno = EUSERS;	
		}

	}

	return ( result );
}

/**
 * This method will stop the polling for a card.
 *
 * @param reader	pointer to the reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_poll_stop(POtiReaderCb reader)
{
	int result = 0;

	if( reader->interruptible == (INTERRUPTIBLE_PP|INTERRUPTIBLE_WAIT_CARD) ) {
		result = write( reader->pfd[1], "S", 1);
	}
	result = oti_handle_tag( reader, DO, OTI_STOP_MACRO, NULL, 0, NULL, 0, OtiTlvSizeAdd );

	return check_oti_handle_tag_answer(result);
}



/**
 * This method will instruct that the card can be removed.
 *
 * @param reader	pointer to reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when the command succeeds
 */
int oti_remove_picc(POtiReaderCb reader)
{
	int result = 0;

	result = oti_send_command( reader, DO, OTI_REMOVE_PICC, NULL, 0, OtiTlvSizeAdd );

	return ( result );
}
/*
 * Remove picc version if SR ticket (or other in future) was detected
 * API: see oti_remove_picc
 */
int oti_remove_picc_extended(POtiReaderCb reader)
{
	int result = 0;
	unsigned char tx[4],rx[32];

#if 1
	tx[0] = reader->pollConfig.fwti_remove;
	oti_reader_command( reader, DO, OTI_RF_PARAM_FWTI, tx, 1, rx, sizeof(rx) );
#else
	tx[0] = SR_RESET_TO_INVENTORY;
	oti_transparent_command( reader, tx, 1, rx, sizeof(rx) );
#endif

	return ( result );
}



/**
 * This method will wait for a card to be removed from the RF.
 *
 * @param reader	pointer to the reader control block.
 * @param timeout	maximum time to wait for card removal.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when the card is removed.
 */
int oti_wait_for_card_removal( POtiReaderCb reader, int timeout )
{
	int result = 0;

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char* rxBuf = reader->trx.rx;
		size_t rxLen = sizeof( reader->trx.rx );

		/*-- wait for response --*/
		result = oti_wait_for_response( reader, &rxBuf, rxLen, timeout );
		if (result < 0)
			result = -1;

		/*-- check received buffer --*/
		if ( result != -1 )
		{
			unsigned short replyTag = ((rxBuf[0]&0xff)<<8) | (rxBuf[1]&0xFF);

			/*-- check if this is a reply for card data --*/
			if ( replyTag != OTI_REMOVE_PICC )
			{
				errno = EBADRQC;
				result = -1;
			}
		}
	}

	return ( result );
}
/*
 * Wait for card removal is SR ticket (or other in future) was detected
 * Same API as oti_wait_for_card_removal
 */
int oti_wait_for_card_removal_extended( POtiReaderCb reader, int timeout )
{
	int result = -1,result2;
	int elapsed = 0;
	int state=0;
        struct timespec time_start,time_now; 
	unsigned char tx[4],rx[32];
	int retries = 0;

	clock_gettime(CLOCK_MONOTONIC,&time_start);

	/*-- sanity check --*/
	if ( reader == NULL )
	{
		errno = EINVAL;
	}
	else
	{
		for( ; (elapsed < timeout) ; ) {
			tx[0] = SR_SELECT;
			tx[1] = reader->chip_ID_SR;
			result2 = oti_transparent_command( reader, tx, 2, rx, sizeof(rx) );
			if( result2 <= 0 ) {
				retries++;
				if( retries >= 2 ) {
			       		result = 3; 
					errno = EAGAIN;
					break;
				}
			} else {
				retries = 0;
				usleep(5000);	/* lower the CPU load of this loop */
			}
                	clock_gettime(CLOCK_MONOTONIC,&time_now);
                	elapsed = (time_now.tv_sec - time_start.tv_sec)*1000 + (time_now.tv_nsec - time_start.tv_nsec)/1000000;
			errno = ETIMEDOUT;
		}
	}
	return ( result );
}

/**
 * This method will initiate the polling for a EMV card and make a transaction if the card has been found.
 *
 * @param reader	pointer to the reader control block.
 * @param tx		pointer to transmit data.
 * @param txLen		length of the transmit data.
 * @param rx		pointer to store the recieve data.
 * @param rxLen		length of the recieve buffer.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_poll_emv(POtiReaderCb reader, unsigned char *tx, unsigned int txLen, POtiCardInfo cardinfo )
{
	int result = 0;
	char* rx = reader->trx.rx;
	size_t rxLen = sizeof( reader->trx.rx );


	/*-- sanity check --*/
	if ( tx == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		result = oti_handle_tag( reader, DO, OTI_POLL_EMV, tx, txLen, rx, rxLen, OtiTlvSizeAdd );

		if (result > 0 || result < -1)
		{
			result = oti_copy_data_2_tag_struct( rx, abs(result), cardinfo);
		}
	}

	return check_oti_handle_tag_answer(result);

}

/**
 * This method will set/get the applications enabled on the OTI reader.
 *
 * @param reader	pointer to the reader control block.
 * @param code		operational code that needs to be performed (GET - SET - DO).
 * @param appli		pointer to a bitmap structure that will represent the OTI Reader applications.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when function succeeded.
 */
int oti_enable_application(POtiReaderCb reader, OpCode code, OtiAppli_t* appli, OtiKernelID kernelId)
{
	int result = 0;

	/*-- sanity check --*/
	if ( appli == NULL )
	{
		errno = EINVAL;
		result = -1;
	}
	else if ((kernelId != OTI_KERNEL_ID_MASTERCARD) && 
		  (kernelId != OTI_KERNEL_ID_VISA) &&
		  (kernelId != OTI_KERNEL_ID_AMEX) && 
		  (kernelId != OTI_KERNEL_ID_NOKERNEL))
	{
		errno = EINVAL;
		result = -1;
	}
	else
	{
		char data[5] = { 0, 0, 0, 0, 0 };

		/*-- Get the current applications that are enabled 
		 *  we can ask for applications that are enabled under specified kernel, 
		 *  but always we get only 4 bytes as answer: the applications that are enabled --*/
		if ( code == GET )
		{
			char* tx = NULL;
			char kernel = (char)kernelId;
			size_t txSize = 0;
			if (kernelId != OTI_KERNEL_ID_NOKERNEL)
			{
				tx = &kernel;
				txSize = sizeof(kernel);
			}
			result = oti_handle_tag(reader, code, OTI_ENABLE_APPLICATION, tx, txSize, data, sizeof(data) - 1, OtiTlvSizeAdd);

			/*-- on success copy result in appli buffer --*/
			if ( result > 0 )
			{
				*appli  = ((data[0]&0xFF) << 24 );
				*appli |= ((data[1]&0xFF) << 16 );
				*appli |= ((data[2]&0xFF) << 8 );
				*appli |= (data[3]&0xFF );
			}
		}
		/*-- DO or SET the applications --*/
		else
		{
			size_t txSize = 0;
			if (kernelId != OTI_KERNEL_ID_NOKERNEL)
			{
				// first write the kernel id.
				data[txSize++] = (char)kernelId;
			}
			data[txSize++] = (((*appli) >> 24) & 0xFF);
			data[txSize++] = (((*appli) >> 16) & 0xFF);
			data[txSize++] = (((*appli) >> 8) & 0xFF);
			data[txSize++] = ((*appli) & 0xFF);

			result = oti_handle_tag(reader, code, OTI_ENABLE_APPLICATION, data, txSize, NULL, 0, OtiTlvSizeAdd);
		}
	}

	return check_oti_handle_tag_answer(result);
}

/**
 * This method will sent data in transparent mode.
 *
 * @param reader	pointer to the reader control block.
 * @param tx		Command to send starting at Mifare OpCode.
 * @param txlen		Command length.
 * @param rx		Response buffer.
 * @param rxlen		Response buffer length.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_transparent_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen )
{
	int result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_DATA, tx, txlen, rx, rxlen, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}

/**
 * This method is a special version of the oti_transparent_command to solve the issue of the no reply on write commands (9) to the SR family of tickets.
 *
 * @param reader	pointer to the reader control block.
 * @param tx		Command to send starting at Mifare OpCode.
 * @param txlen		Command length.
 * @param rx		Response buffer.
 * @param rxlen		Response buffer length.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_sr_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen )
{
	unsigned char txbuf[4];
	int result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_DATA, tx, txlen, rx, rxlen, OtiTlvSizeAdd );
	if( ((tx[0] == SR_WRITE_BLOCK)) && (result == -1) ) { /* sr tickets don't reply write commands cure this by adding a read command on same block = verify */
		txbuf[0] = SR_READ_BLOCK;
		txbuf[1] = tx[1];
		result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_DATA, txbuf, 2, rx, rxlen, OtiTlvSizeAdd );
	}
	return check_oti_handle_tag_answer(result);
}
/**
 * This method defines who is responsible for transparent communication, host or PCD
 *
 * @param reader	pointer to the reader control block.
 * @param device	host or PCD.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_transparent_layer_control( POtiReaderCb reader, OtiTransparentLayer device )
{
	char data[1];

	data[0] = (char)device;
	int result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_LAYER_CONTROL, data, sizeof(data), NULL, 0, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}

/**
 * This method defines who is responsible for transparent communication, host or PCD
 *
 * @param reader	pointer to the reader control block.
 * @param channel	channel to switch to transparent mode (RF, SAM).
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_transparent_channel_select( POtiReaderCb reader, OtiTransparentChannel channel )
{
	char data[1];

	data[0] = (char)channel;
	int result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_CHANNEL_SELECT, data, sizeof(data), NULL, 0, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}

/**
 * This method changes the frame size of the ISO14443 protocol
 *
 * @param reader	pointer to the reader control block.
 * @param device	host or PCD.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_transparent_frame_size( POtiReaderCb reader, OtiTransparentFrameSize size )
{
	char data = (char)size;
	int result = oti_handle_tag( reader, DO, OTI_TRANSPARENT_FRAME_SIZE, &data, sizeof(data), NULL, 0, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}


/* VBO add-on :    */
/**
 * This method will DO a Mifare command.
 *
 * @param reader	pointer to the reader control block.
 * @param tx		Command to send starting at Mifare OpCode.
 * @param txlen		Command length.
 * @param rx		Response buffer.
 * @param rxlen		Response buffer length.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			If ok : Length of received response and response data in rx.
 */
int oti_mifare_command( POtiReaderCb reader, const char *tx, size_t txlen, char *rx, size_t rxlen )
{
	/*-- VBO : NO SANITY CHECK ! Developers ***MUST*** know what they do ! --*/
	int result = oti_handle_tag( reader, DO, OTI_MIFARE_COMMAND, tx, txlen, rx, rxlen, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}

/**
 * This method will instruct that the card can be removed.
 *
 * @param reader	pointer to reader control block.
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when the command succeeds
 */
int oti_mifare_key_region( POtiReaderCb reader, OtiKeyStoreRegion keyStoreRegion )
{
	char tx[1] = { (char)keyStoreRegion };
	int result = oti_handle_tag( reader, DO, OTI_MIFARE_KEY_REGION, tx, sizeof(tx), NULL, 0, OtiTlvSizeAdd );
	return check_oti_handle_tag_answer(result);
}

/**
 * This method will copy the received data into the TAG struct and update the TAG pointers in the struct.
 *
 * @param	rxBuf		pointer to recieved data
 * @param	rxLen		size of recieved data
 * @param	cardinfo	pointer to the TAG struct
 *
 * @return	Returns -1 if the reader could not be set to its default settings,
 * 			check errno for more detailed errorcode.
 * 			Zero is returned when the command succeeds
 */

int oti_copy_data_2_tag_struct( char *rxBuf, size_t rxLen, POtiCardInfo cardinfo)
{
	size_t off = 0;
	unsigned short replyTag = (rxBuf[off++]&0xff);
	int result = 0;

	off = 0;
	/*-- copy response into data buffer --*/
	if ( rxLen > sizeof( cardinfo->data ) ) 
		rxLen = sizeof( cardinfo->data );
	if ( rxLen )
	{
		/*-- Be sure the stucture is emty --*/
		memset( cardinfo, 0, sizeof(OtiCardInfo) );

		memcpy( cardinfo->data, rxBuf+off, rxLen );
		cardinfo->length = rxLen;

		cardinfo->transactionType = -1;
		cardinfo->capki = -1;
		cardinfo->transactionResult = -1;
		cardinfo->dataChannel = -1;
	}

	/*-- set the pointers in the cardinfo structure --*/
	while ( off < cardinfo->length )
	{
		/*-- extract next tag --*/
		if ( (cardinfo->data[off]&0x1F) == 0x1F )
		{
			replyTag  = (cardinfo->data[off++]&0xFF) << 8;
			replyTag |= (cardinfo->data[off++]&0xFF);
		}
		else
		{
			replyTag = (cardinfo->data[off++]&0xFF);
		}

		dprintf(" replyTag = %04X\n", replyTag);

		/*-- parse tag --*/
		switch ( replyTag )
		{
		case OTI_TRACK1:
			{
				cardinfo->length_track1 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->track1 = cardinfo->data+off;
				off += cardinfo->length_track1;
				break;
			}

		case OTI_TRACK2:
			{
				cardinfo->length_track2 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->track2 = cardinfo->data+off;
				off += cardinfo->length_track2;
				break;
			}

		case OTI_AID:
			{
				cardinfo->length_aid = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->aid = cardinfo->data+off;
				off += cardinfo->length_aid;
				break;
			}

		case OTI_PICC_TYPE:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->piccType = (OtiPiccType)cardinfo->data[off++];
				break;
			}

		case OTI_POLL_EMV_ERROR:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->error = cardinfo->data[off++];
				break;
			}

		case OTI_PUPI:
			{
				cardinfo->length_pupi = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->pupi = cardinfo->data+off;
				off += cardinfo->length_pupi;
				break;
			}

		case OTI_UID:
			{
				cardinfo->length_uid = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->uid = cardinfo->data+off;
				off += cardinfo->length_uid;
				break;
			}

		case OTI_ATQB:
			{
				cardinfo->length_atqb = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->atqb = cardinfo->data+off;
				off += cardinfo->length_atqb;
				break;
			}

		case OTI_ATS:
			{
				cardinfo->length_ats = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ats = cardinfo->data+off;
				off += cardinfo->length_ats;
				break;
			}

		case OTI_SAK:
			{
				cardinfo->length_sak = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->sak = cardinfo->data+off;
				off += cardinfo->length_sak;
				break;
			}

		case OTI_AMOUNT_AUTHORIZED:
			{
				cardinfo->length_amountAuthorised = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->amountAuthorised = cardinfo->data+off;
				off += cardinfo->length_amountAuthorised;
				break;
			}

		case OTI_TRANSACTION_DATE:
			{
				cardinfo->length_transactionDate = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->transactionDate = cardinfo->data+off;
				off += cardinfo->length_transactionDate;
				break;
			}

		case OTI_TRANSACTION_TYPE:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->transactionType = (OtiTransactionType)cardinfo->data[off++];
				break;
			}

		case OTI_TRANSACTION_TIME:
			{
				cardinfo->length_transactionTime = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->transactionTime = cardinfo->data+off;
				off += cardinfo->length_transactionTime;
				break;
			}

		case OTI_CVM_RESULT:
			{
				cardinfo->length_cvmResult = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->cvmResult = cardinfo->data+off;
				off += cardinfo->length_cvmResult;
				break;
			}

		case OTI_TERMINAL_VERIFICATION_RESULT:
			{
				cardinfo->length_termVerification = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->termVerification = cardinfo->data+off;
				off += cardinfo->length_termVerification;
				break;
			}

		case OTI_TRANSACTION_STATUS_INFO:
			{
				cardinfo->length_transactionStatus = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->transactionStatus = cardinfo->data+off;
				off += cardinfo->length_transactionStatus;
				break;
			}

		case OTI_CERTIFICATION_AUTHORITY_PKI:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->capki = (OtiTransactionType)cardinfo->data[off++];
				break;
			}

		case OTI_ISSUER_APP_DATA:
			{
				cardinfo->length_issAppData = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->issAppData = cardinfo->data+off;
				off += cardinfo->length_issAppData;
				break;
			}

		case OTI_UNPREDICATABLE_NUMBER:
			{
				cardinfo->length_unpredictableNbr = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->unpredictableNbr = cardinfo->data+off;
				off += cardinfo->length_unpredictableNbr;
				break;
			}
			
		case OTI_APP_INTERCHANGE_PROFILE:
			{
				cardinfo->length_appInterchangeProf = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->appInterchangeProf = cardinfo->data+off;
				off += cardinfo->length_appInterchangeProf;
				break;
			}

		case OTI_DF_NAME:
			{
				cardinfo->length_DFName = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->DFName = cardinfo->data+off;
				off += cardinfo->length_DFName;
				break;
			}

		case OTI_CRYPTOGRAM_INFORMATION_DATA:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->cryptoInfoData = (OtiCryptoInfoData)cardinfo->data[off++];
				break;
			}

		case OTI_APP_CRYPTOGRAM:
			{
				cardinfo->length_appCrypto = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->appCrypto = cardinfo->data+off;
				off += cardinfo->length_appCrypto;
				break;
			}

		case OTI_APP_TRANSACTION_COUNTER:
			{
				cardinfo->length_appTransactionCnt = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->appTransactionCnt = cardinfo->data+off;
				off += cardinfo->length_appTransactionCnt;
				break;
			}

		case OTI_CVM_LIST:
			{
				cardinfo->length_cvmList = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->cvmList = cardinfo->data+off;
				off += cardinfo->length_cvmList;
				break;
			}

		case OTI_TRANSACTION_RESULT:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->transactionResult = (OtiTransactionRes)cardinfo->data[off++];
				break;
			}

		case OTI_DATA_AUTHENTICATION_CODE:
			{
				cardinfo->length_dataAuthCode = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->dataAuthCode = cardinfo->data+off;
				off += cardinfo->length_dataAuthCode;
				break;
			}

		case OTI_TERMINAL_TRANSACTION_QUALIFIERS:
			{
				cardinfo->length_termTransactQual = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->termTransactQual = cardinfo->data+off;
				off += cardinfo->length_termTransactQual;
				break;
			}

		case OTI_DATA_CHANNEL:
			{
				oti_get_msg_length( cardinfo->data, &off );
				cardinfo->dataChannel = (OtiTransactionRes)cardinfo->data[off++];
				break;
			}

		case OTI_CUSTOMER_EXCLUSIVE_DATA:
			{
				cardinfo->length_customerExclData = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->customerExclData = cardinfo->data+off;
				off += cardinfo->length_customerExclData;
				break;
			}

		case OTI_BANK_IDENTIFIER_CODE:
			{
				cardinfo->length_bankIdCode = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->bankIdCode = cardinfo->data+off;
				off += cardinfo->length_bankIdCode;
				break;
			}

		case OTI_CARDHOLDER_NAME_EXTENDED:
			{
				cardinfo->length_cardholderNameExt = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->cardholderNameExt = cardinfo->data+off;
				off += cardinfo->length_cardholderNameExt;
				break;
			}

		case OTI_APP_DISCRETIONARY_DATA:
			{
				cardinfo->length_appDiscrData = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->appDiscrData = cardinfo->data+off;
				off += cardinfo->length_appDiscrData;
				break;
			}

/*		case OTI_FROM_FACTOR_INDICATOR:
			{
				cardinfo->length_formFactorInd = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->formFactorInd = cardinfo->data+off;
				off += cardinfo->length_formFactorInd;
				break;
			}
*/
		case OTI_PAYPASS_THIRD_PARTY_DATA:
			{
				cardinfo->length_payPassThPartyData = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->payPassThPartyData = cardinfo->data+off;
				off += cardinfo->length_payPassThPartyData;
				break;
			}

		case OTI_TRANSACTION_SEQUENCE_COUNTER:
			{
				cardinfo->length_tranactionSeqCnt = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->tranactionSeqCnt = cardinfo->data+off;
				off += cardinfo->length_tranactionSeqCnt;
				break;
			}

		case OTI_DISCRETIONARY_DATA_TRACK1:
			{
				cardinfo->length_discrDataTrack1 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->discrDataTrack1 = cardinfo->data+off;
				off += cardinfo->length_discrDataTrack1;
				break;
			}

		case OTI_DISCRETIONARY_DATA_TRACK2:
			{
				cardinfo->length_discrDataTrack2 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->discrDataTrack2 = cardinfo->data+off;
				off += cardinfo->length_discrDataTrack2;
				break;
			}

		case OTI_NDOT_SECURED_FRAME:
			{
				cardinfo->length_ndotSecFrame = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotSecFrame = cardinfo->data+off;
				off += cardinfo->length_ndotSecFrame;
				break;
			}

		case OTI_NDOT_OFFLINE_BALANCE:
			{
				cardinfo->length_ndotOfflineBalance = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotOfflineBalance = cardinfo->data+off;
				off += cardinfo->length_ndotOfflineBalance;
				break;
			}

		case OTI_NDOT_SECURED_FIXED_FRAME_1:
			{
				cardinfo->length_ndotSecFixFrame1 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotSecFixFrame1 = cardinfo->data+off;
				off += cardinfo->length_ndotSecFixFrame1;
				break;
			}

		case OTI_NDOT_UNSECURED_FIXED_FRAME_1:
			{
				cardinfo->length_ndotUnsecFixFrame1 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotUnsecFixFrame1 = cardinfo->data+off;
				off += cardinfo->length_ndotUnsecFixFrame1;
				break;
			}

		case OTI_NDOT_SECURED_FIXED_FRAME_2:
			{
				cardinfo->length_ndotSecFixFrame2 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotSecFixFrame2 = cardinfo->data+off;
				off += cardinfo->length_ndotSecFixFrame2;
				break;
			}

		case OTI_NDOT_UNSECURED_FIXED_FRAME_2:
			{
				cardinfo->length_ndotUnsecFixFrame2 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotUnsecFixFrame2 = cardinfo->data+off;
				off += cardinfo->length_ndotUnsecFixFrame2;
				break;
			}

		case OTI_NDOT_SECURED_FIXED_FRAME_3:
			{
				cardinfo->length_ndotSecFixFrame3 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotSecFixFrame3 = cardinfo->data+off;
				off += cardinfo->length_ndotSecFixFrame3;
				break;
			}

		case OTI_NDOT_UNSECURED_FIXED_FRAME_3:
			{
				cardinfo->length_ndotUnsecFixFrame3 = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotUnsecFixFrame3 = cardinfo->data+off;
				off += cardinfo->length_ndotUnsecFixFrame3;
				break;
			}

		case OTI_NDOT_UNSECURED_GEN_VAR_FRAME:
			{
				cardinfo->length_ndotUnsecGenVarFrame = oti_get_msg_length( cardinfo->data, &off );
				cardinfo->ndotUnsecGenVarFrame = cardinfo->data+off;
				off += cardinfo->length_ndotUnsecGenVarFrame;
				break;
			}

		default:
			{
				off++;
				break;
			}
		}
	}

	result = sizeof( OtiCardInfo );

	return result;
}
