/*****************************************************************************/
/*
 *
 *      Copyright (C) 2010-2014 Prodata Mobility Systems
 *
 * All rights reserved.
 * 
 * Permission to use, copy, modify, and distribute this software for any purpose
 * with or without fee is hereby granted, provided that the above copyright
 * notice and this permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Except as contained in this notice, the name of a copyright holder shall not
 * be used in advertising or otherwise to promote the sale, use or other dealings
 * in this Software without prior written authorization of the copyright holder.
 * 
 */
/*****************************************************************************//*
 * oti_trace.h
 *
 *  Created on: Oct 24, 2008
 *      Author: jvolckae
 */

#ifndef OTI_TRACE_H_
#define OTI_TRACE_H_

#include <unistd.h>
#include <stdio.h>
#include <sys/times.h>

#include "oti.h"
#include "oti_reader.h"

extern OtiVerbosity verbosity;
#define dprintf 	if ( verbosity & OtiVerbosityCommands ) printf
#include <errno.h>
#define oti_perror(a)	if ( verbosity & OtiVerbosityError ) printf("%s - %s\n",a,strerror(errno));
/*-- function declaration --*/
void oti_trace_hex( POtiCb oti, const char* str, const char* data, size_t length);

#endif /* OTI_TRACE_H_ */
